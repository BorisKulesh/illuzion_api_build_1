<?php

namespace Illuzion\Common\Doctrine;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;

/**
 * Вспомогательный тип для строк
 * Приложение работает в UTF-8, но в базе строки должны храниться в windows-1251
 */
class Windows1251String extends StringType
{
    /**
     * {@inheritdoc}
     */
    public function convertToDatabaseValue($value, AbstractPlatform $p)
    {
        return $value !== null ? iconv('UTF-8', 'windows-1251//TRANSLIT//IGNORE', $value) : null;
    }

    /**
     * {@inheritdoc}
     */
    public function convertToPHPValue($value, AbstractPlatform $p)
    {
        return $value !== null ? iconv('windows-1251', 'UTF-8//TRANSLIT//IGNORE', $value) : null;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'string_win1251';
    }
}

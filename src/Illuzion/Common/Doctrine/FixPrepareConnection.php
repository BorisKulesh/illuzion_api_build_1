<?php

namespace Illuzion\Common\Doctrine;

use Doctrine\DBAL\Driver\Ibase\AbstractIbaseConnection;

/**
 * Используем FixPrepareStatement вместо обычного
 * Там есть правки по ibase_execute
 */
class FixPrepareConnection extends AbstractIbaseConnection
{
    /**
     * {@inheritDoc}
     *
     * @param string $prepareString SQL Statement
     * @return FixPrepareStatement
     */
    public function prepare($prepareString)
    {
        return new FixPrepareStatement($this, $prepareString);
    }

    /**
     * Better code encoding
     * @return int|null|string
     */
    public function errorCode()
    {
        return iconv('windows-1251', 'UTF-8//TRANSLIT//IGNORE', parent::errorCode());
    }

    /**
     * Better info encoding
     * @return array
     */
    public function errorInfo()
    {
        $info = parent::errorInfo();
        if (null !== $info['message']) {
            $info['code'] = iconv('windows-1251', 'UTF-8//TRANSLIT//IGNORE', $info['code']);
            $info['message'] = iconv('windows-1251', 'UTF-8//TRANSLIT//IGNORE', $info['message']);
        }
        return $info;
    }
}

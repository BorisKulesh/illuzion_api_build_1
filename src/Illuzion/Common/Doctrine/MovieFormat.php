<?php

namespace Illuzion\Common\Doctrine;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\IntegerType;

/**
 * Вспомогательный тип для формата фильмов
 * В базе форматы хранятся в виде маски, а у нас в виде массива
 */
class MovieFormat extends IntegerType
{
    const TYPE_2D = '2D';
    const TYPE_3D = '3D';
    const TYPE_IMAX_2D = 'IMAX_2D';
    const TYPE_IMAX_3D = 'IMAX_3D';

    protected static $MAP = [
        MovieFormat::TYPE_2D => 1,
        MovieFormat::TYPE_3D => 2,
        MovieFormat::TYPE_IMAX_2D => 4,
        MovieFormat::TYPE_IMAX_3D => 8,
    ];

    /**
     * @param array $formats
     * @return int
     */
    public static function toInteger(array $formats)
    {
        $result = 0;
        foreach ($formats as $format) {
            if (!array_key_exists($format, self::$MAP)) {
                continue;
            }

            $result += self::$MAP[$format];
        }
        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function convertToDatabaseValue($value, AbstractPlatform $p)
    {
        if (!is_array($value)) {
            return 0;
        }

        return self::toInteger($value);
    }

    /**
     * {@inheritdoc}
     */
    public function convertToPHPValue($value, AbstractPlatform $p)
    {
        $result = [];
        $value = parent::convertToPHPValue($value, $p);
        foreach (self::$MAP as $format => $bit) {
            if ($value & $bit) {
                $result[] = $format;
            }
        }

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'movie_format';
    }
}

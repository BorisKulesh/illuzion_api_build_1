<?php

namespace Illuzion\Common\Doctrine;

use Doctrine\DBAL\Driver\Ibase\AbstractIbaseStatement;

/**
 * У метода ibase_execute есть какие-то проблемы, и запросы выполняются некорректно
 * Поэтому мы используем вместо него ibase_query
 * Это менее безопасно, но необходимо, чтобы все хоть как-то работало
 */
class FixPrepareStatement extends AbstractIbaseStatement
{
    /**
     * Prepares the statement for further use and executes it
     * @result resource|boolean
     */
    protected function doExecPrepared()
    {
        $stm = explode('?', $this->statement);
        $result = [$stm[0]];
        for ($i = 0; $i < count($this->queryParamBindings); $i++) {
            $param = $this->queryParamBindings[$i];

            if (is_string($param)) {
                $param = str_replace("'", "''", $param);
                $param = "'{$param}'";
            } elseif (null === $param) {
                $param = 'NULL';
            } elseif ($param instanceof \DateTime) {
                $param = "'".$param->format('Y-m-d H:i:s')."'";
            } else {
                $param = (string) $param;
            }

            $result[] = $param;
            $result[] = $stm[$i + 1];
        }

        return @ibase_query($this->connection->getActiveTransactionIbaseRes(), implode('', $result));
    }

    /**
     * Better code encoding
     * @return int|null|string
     */
    public function errorCode()
    {
        return iconv('windows-1251', 'UTF-8//TRANSLIT//IGNORE', parent::errorCode());
    }

    /**
     * Better info encoding
     * @return array
     */
    public function errorInfo()
    {
        $info = parent::errorInfo();
        if (null !== $info['message']) {
            $info['code'] = iconv('windows-1251', 'UTF-8//TRANSLIT//IGNORE', $info['code']);
            $info['message'] = iconv('windows-1251', 'UTF-8//TRANSLIT//IGNORE', $info['message']);
        }
        return $info;
    }
}

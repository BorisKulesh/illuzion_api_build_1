<?php

namespace Illuzion\Common\Doctrine;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\IntegerType;

/**
 * Вспомогательный тип для типов мест
 * В базе форматы хранятся в числа, а у нас в виде строки
 */
class SeatType extends IntegerType
{
    const TYPE_SIMPLE = 'simple';
    const TYPE_SOFA = 'sofa';
    const TYPE_VIP = 'vip';
    const TYPE_BEATBOX = 'beatbox';

    protected static $MAP = [
        SeatType::TYPE_SIMPLE => 3,
        SeatType::TYPE_SOFA => 4,
        SeatType::TYPE_VIP => 1,
        SeatType::TYPE_BEATBOX => 2,
    ];

    /**
     * @param string $value
     * @return int
     */
    public static function toInteger($value)
    {
        if (!array_key_exists($value, self::$MAP)) {
            return null;
        }

        return self::$MAP[$value];
    }

    /**
     * {@inheritdoc}
     */
    public function convertToDatabaseValue($value, AbstractPlatform $p)
    {
        return self::toInteger($value);
    }

    /**
     * {@inheritdoc}
     */
    public function convertToPHPValue($value, AbstractPlatform $p)
    {
        $value = parent::convertToPHPValue($value, $p);
        foreach (self::$MAP as $format => $bit) {
            if ($bit === $value) {
                return $format;
            }
        }
        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'seat_type';
    }
}

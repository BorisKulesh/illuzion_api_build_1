<?php

namespace Illuzion\Common\Doctrine;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;
use Doctrine\DBAL\Types\TextType;

/**
 * Вспомогательный тип для текста
 * Приложение работает в UTF-8, но в базе тест должн храниться в windows-1251
 */
class Windows1251Text extends TextType
{
    /**
     * {@inheritdoc}
     */
    public function convertToDatabaseValue($value, AbstractPlatform $p)
    {
        return iconv('UTF-8', 'windows-1251//TRANSLIT//IGNORE', $value);
    }

    /**
     * {@inheritdoc}
     */
    public function convertToPHPValue($value, AbstractPlatform $p)
    {
        return iconv('windows-1251', 'UTF-8//TRANSLIT//IGNORE', $value);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'text_win1251';
    }
}

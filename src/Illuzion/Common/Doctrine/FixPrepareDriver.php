<?php

namespace Illuzion\Common\Doctrine;

use Doctrine\DBAL\Driver\Ibase\AbstractDriver;

/**
 * Используем FixPrepareConnection вместо обычного
 * Там есть правки по ibase_execute
 */
class FixPrepareDriver extends AbstractDriver
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'ibase_firebird_fixed_prepare';
    }

    /**
     * @return string Class name of the Connection object used in this driver
     */
    protected function getDriverConnectionClass()
    {
        return FixPrepareConnection::class;
    }
}

<?php

namespace Illuzion\Common;

use EasyCorp\EasyLog\EasyLogHandler;

class ExceptionLogHandler extends EasyLogHandler
{
    /**
     * @param array $records
     */
    public function handleBatch(array $records)
    {
        foreach ($records as &$record) {
            if (array_key_exists('context', $record) && array_key_exists('exception', $record['context'])) {
                /** @var \Exception $ex */
                $ex = $record['context']['exception'];
                $record['context']['exception'] = $ex->getMessage();
                $record['context']['stack'] = $ex->getTrace();
            }
        }

        parent::handleBatch($records);
    }
}

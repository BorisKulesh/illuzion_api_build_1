<?php

namespace Illuzion\ApiBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;

/**
 * Бонусная карта
 *
 * @ORM\Entity(repositoryClass="Illuzion\ApiBundle\Repository\DiscountCardRepository")
 * @ORM\Table(name="DISCOUNT_CARD")
 */
class DiscountCard
{
    /** Типы поддерживаемых карт */
    const TYPE_BASIC = 'basic';
    const TYPE_GOLD = 'gold';

    /** @var string[] Соответствия типов карт и префиксов */
    static $TYPE_PREFIX_MAP = [
        self::TYPE_BASIC => '636363',
        self::TYPE_GOLD => '555555',
    ];

    /**
     * @var integer Id карты
     * @ORM\Column(name="ID", type="integer")
     * @ORM\Id()
     */
    protected $id;

    /**
     * @var string Номер карты
     * @ORM\Column(name="SCAN_CODE", type="string_win1251")
     */
    protected $number;

    /**
     * @var integer Количество бонусов
     * @ORM\Column(name="BONUS_SUM", type="integer")
     */
    protected $bonus;

    /**
     * @var \DateTime Дата активации
     * @ORM\Column(name="DATETIME_REG", type="datetime")
     */
    protected $datetimeReg;

    /**
     * @var integer Состояние блокировки
     * @ORM\Column(name="ACTIVITY_LEVEL", type="integer")
     */
    protected $activityLevel;

    /**
     * @var Customer Пользователь
     * @ORM\JoinColumn(nullable=false, name="USER_ID", referencedColumnName="ID")
     * @ORM\ManyToOne(targetEntity="Illuzion\ApiBundle\Entity\Customer\Customer", inversedBy="discountCards")
     */
    protected $customer;

    /**
     * @return int Id карты
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id Id карты
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = (int) $id;
        return $this;
    }

    /**
     * @return string Номер карты
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param string $number Номер карты
     *
     * @return $this
     */
    public function setNumber($number)
    {
        $this->number = (string) $number;
        return $this;
    }

    /**
     * @return int Количество бонусов
     */
    public function getBonus()
    {
        return $this->bonus;
    }

    /**
     * @param int $bonus Количество бонусов
     *
     * @return $this
     */
    public function setBonus($bonus)
    {
        $this->bonus = (int) $bonus;
        return $this;
    }

    /**
     * @return Customer Пользователь
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param Customer $customer Пользователь
     *
     * @return $this
     */
    public function setCustomer(Customer $customer)
    {
        $this->customer = $customer;
        return $this;
    }

    /**
     * @return int Состояние блокировки
     */
    public function getActivityLevel()
    {
        return $this->activityLevel;
    }

    /**
     * @param int $activityLevel Состояние блокировки
     *
     * @return $this
     */
    public function setActivityLevel($activityLevel)
    {
        $this->activityLevel = $activityLevel;
        return $this;
    }

    /**
     * @return \DateTime Дата активации
     */
    public function getDatetimeReg()
    {
        return $this->datetimeReg;
    }

    /**
     * @param \DateTime $datetimeReg Дата активации
     *
     * @return $this
     */
    public function setDatetimeReg(\DateTime $datetimeReg = null)
    {
        $this->datetimeReg = $datetimeReg;
        return $this;
    }
}

<?php

namespace Illuzion\ApiBundle\Entity\Customer;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Пользователь
 *
 * @ORM\Entity(repositoryClass="Illuzion\ApiBundle\Repository\CustomerRepository")
 * @ORM\Table(name="USER_ENTITY")
 */
class Customer
{
    /**
     * @var integer Id в базе кинотеатра
     * @ORM\Column(name="ID", type="integer")
     * @ORM\Id()
     */
    protected $localId;

    /**
     * @var integer Id в базе уссури
     * @ORM\Column(name="GLOBAL_ID", type="integer")
     */
    protected $id;

    /**
     * @var string Почта
     * @ORM\Column(name="MAIL", type="string_win1251")
     */
    protected $email;

    /**
     * @var string Не подтвержденная почта
     * @ORM\Column(name="EMAILNEW", type="string_win1251")
     */
    protected $emailNew;

    /**
     * @var boolean Почта была обновленна
     */
    protected $emailUpdated = false;

    /**
     * @var \DateTime Дата отсылки подтверждения почты
     * @ORM\Column(name="EMAILCONFIRMATIONSENDDATE", type="datetime")
     */
    protected $emailConfirmationSentDate;

    /**
     * @var boolean Подписка на расслку
     * @ORM\Column(name="EMAILNEWSLETTER", type="boolean")
     */
    protected $emailNewsletter = true;

    /**
     * @var string Телефон
     * @ORM\Column(name="PHONE", type="string_win1251")
     */
    protected $phone;

    /**
     * @var string Не подтвержденный телефон
     * @ORM\Column(name="PHONENEW", type="string_win1251")
     */
    protected $phoneNew;

    /**
     * @var boolean Телефон был обновлен
     */
    protected $phoneUpdated = false;

    /**
     * @var \DateTime Дата отсылки подтверждения телефона
     * @ORM\Column(name="PHONECONFIRMATIONSENDDATE", type="datetime")
     */
    protected $phoneConfirmationSentDate;

    /**
     * @var string ФИО
     * @ORM\Column(name="NAME", type="string_win1251")
     */
    protected $name;

    /**
     * @var string[]|ArrayCollection ФИО разбитое по частям
     */
    protected $nameArr;

    /**
     * @var \DateTime Дата рождения
     * @ORM\Column(name="BIRTHDAY", type="datetime")
     */
    protected $birthday;

    /**
     * @var integer Набор различных флагов
     * @ORM\Column(name="USER_TYPE_EXT", type="integer")
     */
    protected $userTypeExt;

    /**
     * @var DiscountCard[]|ArrayCollection Список карт пользователя
     * @ORM\OneToMany(targetEntity="Illuzion\ApiBundle\Entity\Customer\DiscountCard", mappedBy="customer")
     */
    protected $discountCards;

    /**
     * @var string Хеш пароля
     * @ORM\Column(name="PASSWORDHASH", type="string_win1251")
     */
    protected $passwordHash;

    /**
     * @var string Соль пароля
     * @ORM\Column(name="PASSWORDSALT", type="string_win1251")
     */
    protected $passwordSalt;

    /**
     * @var \DateTime Дата последнего обновления
     * @ORM\Column(name="UPDATED_AT", type="datetime")
     */
    protected $updatedAt;

    /**
     * @return int Id в базе уссури
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string Почта
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email Почта
     *
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = (string) $email;
        return $this;
    }

    /**
     * @return string Телефон
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone Телефон
     *
     * @return $this
     */
    public function setPhone($phone)
    {
        $this->phone = (string) $phone;
        return $this;
    }

    /**
     * @return string ФИО
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return ArrayCollection|\string[] ФИО разбитое по частям
     */
    protected function getNameArr()
    {
        if (null === $this->nameArr) {
            $this->nameArr = new ArrayCollection(explode(' ', $this->getName() ?: ''));
        }

        return $this->nameArr;
    }

    /**
     * @return string Имя
     */
    public function getNameFirst()
    {
        return $this->getNameArr()->get(1);
    }

    /**
     * @param string $name Имя
     * @return $this
     */
    public function setNameFirst($name)
    {
        $arr = $this->getNameArr();
        $arr->set(1, $name);
        $this->name = implode(' ', $arr->toArray());
        return $this;
    }

    /**
     * @return string Отчество
     */
    public function getNameMiddle()
    {
        return $this->getNameArr()->get(2);
    }

    /**
     * @param string $name Отчество
     * @return $this
     */
    public function setNameMiddle($name)
    {
        $arr = $this->getNameArr();
        $arr->set(2, $name);
        $this->name = implode(' ', $arr->toArray());
        return $this;
    }

    /**
     * @return string Фамилия
     */
    public function getNameLast()
    {
        return $this->getNameArr()->get(0);
    }

    /**
     * @param string $name Фамилия
     * @return $this
     */
    public function setNameLast($name)
    {
        $arr = $this->getNameArr();
        $arr->set(0, $name);
        $this->name = implode(' ', $arr->toArray());
        return $this;
    }

    /**
     * @param string $name ФИО
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = (string) $name;
        return $this;
    }

    /**
     * @return \DateTime Дата рождения
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * @param \DateTime $birthday Дата рождения
     *
     * @return $this
     */
    public function setBirthday(\DateTime $birthday = null)
    {
        $this->birthday = $birthday;
        return $this;
    }

    /**
     * @return int Набор различных флагов
     */
    public function getUserTypeExt()
    {
        return $this->userTypeExt;
    }

    /**
     * @param int $userTypeExt Набор различных флагов
     *
     * @return $this
     */
    public function setUserTypeExt($userTypeExt)
    {
        $this->userTypeExt = (int) $userTypeExt;
        return $this;
    }

    /**
     * @return ArrayCollection|DiscountCard[] Список карт пользователя
     */
    public function getDiscountCards()
    {
        return $this->discountCards;
    }

    /**
     * @param ArrayCollection|DiscountCard[] $discountCards Список карт пользователя
     *
     * @return $this
     */
    public function setDiscountCards(array $discountCards)
    {
        $this->discountCards = new ArrayCollection($discountCards);
        return $this;
    }

    /**
     * @return boolean Подписка на смс уведомления
     */
    public function isSmsNotifications()
    {
        return ($this->getUserTypeExt() & 1024) === 1024;
    }

    /**
     * @param boolean $smsNotifications Подписка на смс уведомления
     *
     * @return $this
     */
    public function setSmsNotifications($smsNotifications)
    {
        if ($smsNotifications) {
            $this->userTypeExt = $this->userTypeExt | 1024;
        } else {
            $this->userTypeExt = $this->userTypeExt - ($this->userTypeExt & 1024);
        }
        return $this;
    }

    /**
     * @return boolean Подписка на email уведомления
     */
    public function isEmailNotifications()
    {
        return ($this->getUserTypeExt() & 2048) === 2048;
    }

    /**
     * @param boolean $emailNotifications Подписка на email уведомления
     *
     * @return $this
     */
    public function setEmailNotifications($emailNotifications)
    {
        if ($emailNotifications) {
            $this->userTypeExt = $this->userTypeExt | 2048;
        } else {
            $this->userTypeExt = $this->userTypeExt - ($this->userTypeExt & 2048);
        }
        return $this;
    }

    /**
     * @return boolean Подписка на рассылку
     */
    public function isEmailNewsletter()
    {
        return $this->emailNewsletter;
    }

    /**
     * @param boolean $emailNewsletter Подписка на рассылку
     */
    public function setEmailNewsletter($emailNewsletter)
    {
        $this->emailNewsletter = $emailNewsletter;
    }

    /**
     * @return string Не подтвержденная почта
     */
    public function getEmailNew()
    {
        return $this->emailNew;
    }

    /**
     * @param string $emailNew Не подтвержденная почта
     *
     * @return $this
     */
    public function setEmailNew($emailNew)
    {
        if (!$emailNew || $emailNew === $this->email) {
            return $this;
        }
        if ($emailNew === $this->emailNew && $this->emailConfirmationSentDate < new \DateTime('-1 day')) {
            return $this;
        }

        $this->emailUpdated = true;
        $this->emailNew = $emailNew;
        $this->emailConfirmationSentDate = new \DateTime();

        return $this;
    }

    /**
     * @return bool Почта была обновлена
     */
    public function isEmailUpdated()
    {
        return $this->emailUpdated;
    }

    /**
     * @return boolean Подтверждена ли почта
     */
    public function isEmailConfirmed()
    {
        return ($this->userTypeExt & 8192) === 8192;
    }

    /**
     * @param boolean $emailConfirmed Подтверждена ли почта
     *
     * @return $this
     */
    public function setEmailConfirmed($emailConfirmed)
    {
        if ($emailConfirmed) {
            $this->userTypeExt = $this->userTypeExt | 8192;
        } else {
            $this->userTypeExt = $this->userTypeExt - ($this->userTypeExt & 8192);
        }
        return $this;
    }

    /**
     * @return \DateTime Дата отсылки подтверждения почты
     */
    public function getEmailConfirmationSentDate()
    {
        return $this->emailConfirmationSentDate;
    }

    /**
     * @param \DateTime $emailConfirmationSentDate Дата отсылки подтверждения почты
     *
     * @return $this
     */
    public function setEmailConfirmationSentDate(\DateTime $emailConfirmationSentDate = null)
    {
        $this->emailConfirmationSentDate = $emailConfirmationSentDate;
        $this->emailUpdated = true;
        return $this;
    }

    /**
     * @return string Не подтвержденный телефон
     */
    public function getPhoneNew()
    {
        return $this->phoneNew;
    }

    /**
     * @param string $phoneNew Не подтвержденный телефон
     *
     * @return $this
     */
    public function setPhoneNew($phoneNew)
    {
        if (!$phoneNew || $phoneNew === $this->phone) {
            return $this;
        }
        if ($phoneNew === $this->phoneNew && $this->phoneConfirmationSentDate < new \DateTime('-1 day')) {
            return $this;
        }

        $this->phoneUpdated = true;
        $this->phoneNew = $phoneNew;
        $this->phoneConfirmationSentDate = new \DateTime();

        return $this;
    }

    /**
     * @return bool Телефон был обновлен
     */
    public function isPhoneUpdated()
    {
        return $this->phoneUpdated;
    }

    /**
     * @return boolean Подтвержден ли телефон
     */
    public function isPhoneConfirmed()
    {
        return ($this->userTypeExt & 4096) === 4096;
    }

    /**
     * @param boolean $phoneConfirmed Подтвержден ли телефон
     *
     * @return $this
     */
    public function setPhoneConfirmed($phoneConfirmed)
    {
        if ($phoneConfirmed) {
            $this->userTypeExt = $this->userTypeExt | 4096;
        } else {
            $this->userTypeExt = $this->userTypeExt - ($this->userTypeExt & 4096);
        }
        return $this;
    }

    /**
     * @return \DateTime Дата отсылки подтверждения телефона
     */
    public function getPhoneConfirmationSentDate()
    {
        return $this->phoneConfirmationSentDate;
    }

    /**
     * @param \DateTime $phoneConfirmationSentDate Дата отсылки подтверждения телефона
     *
     * @return $this
     */
    public function setPhoneConfirmationSentDate(\DateTime $phoneConfirmationSentDate = null)
    {
        $this->phoneConfirmationSentDate = $phoneConfirmationSentDate;
        $this->phoneUpdated = true;
        return $this;
    }

    /**
     * @return string Хеш пароля
     */
    public function getPasswordHash()
    {
        return $this->passwordHash;
    }

    /**
     * @param string $passwordHash Хеш пароля
     *
     * @return $this
     */
    public function setPasswordHash($passwordHash)
    {
        $this->passwordHash = (string) $passwordHash;
        return $this;
    }

    /**
     * @return string Соль пароля
     */
    public function getPasswordSalt()
    {
        return $this->passwordSalt;
    }

    /**
     * @param string $passwordSalt Соль пароля
     *
     * @return $this
     */
    public function setPasswordSalt($passwordSalt)
    {
        $this->passwordSalt = (string) $passwordSalt;
        return $this;
    }

    /**
     * @return \DateTime Дата последнего обновления
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt Дата последнего обновления
     *
     * @return $this
     */
    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * @return boolean Активирована ли карта
     */
    public function isUserActivate()
    {
        return ($this->userTypeExt & 12288) === 12288;
    }

    /**
     * @return DiscountCard|null Основная карта пользователя
     */
    public function getMainCard()
    {
        foreach ($this->getDiscountCards() as $card) {
            foreach (DiscountCard::$TYPE_PREFIX_MAP as $typePrefix) {
                if (0 === strpos($card->getNumber(), $typePrefix)) {
                    return $card;
                }
            }
        }
        return null;
    }
}

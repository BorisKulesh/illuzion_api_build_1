<?php

namespace Illuzion\ApiBundle\Entity\Main;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Фильм
 *
 * @ORM\Entity(repositoryClass="Illuzion\ApiBundle\Repository\MovieRepository")
 * @ORM\Table(name="FILMS")
 */
class Movie
{
    /**
     * @var integer Глобальный Id
     * @ORM\Column(name="NEWID", type="integer")
     */
    protected $id;

    /**
     * @var integer Id в базе кинотеатра
     * @ORM\Column(name="KEY_FILM", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Illuzion\ApiBundle\Entity\Generator\MovieIdGenerator")
     */
    protected $localId;

    /**
     * @var string Название
     * @ORM\Column(name="NAME_FILM", type="string_win1251")
     */
    protected $title;

    /**
     * @var string Алиас
     * @ORM\Column(name="ALIAS", type="string_win1251")
     */
    protected $alias;

    /**
     * @var boolean Архивный
     * @ORM\Column(name="ARCHIVED", type="boolean")
     */
    protected $archived;

    /**
     * @var boolean Опубликован
     * @ORM\Column(name="PUBLIC", type="boolean")
     */
    protected $published;

    /**
     * @var \DateTime Дата начала проката в кинотеатре
     * @ORM\Column(name="START_DATE", type="datetime")
     */
    protected $startDate;

    /**
     * @var string Режисер
     * @ORM\Column(name="DIRECTOR", type="string_win1251", length=50)
     */
    protected $director;

    /**
     * @var string Актреры
     * @ORM\Column(name="ACTORS", type="string_win1251")
     */
    protected $actors;

    /**
     * @var integer Продолжительность
     * @ORM\Column(name="FILM_TIME", type="integer")
     */
    protected $runtime;

    /**
     * @var integer Номер лицензии
     * @ORM\Column(name="PU", type="string_win1251")
     */
    protected $licenceNumber;

    /**
     * @var integer Год выхода
     * @ORM\Column(name="YEARS", type="integer")
     */
    protected $year;

    /**
     * @var string Страны
     * @ORM\Column(name="COUNTRY", type="string_win1251", length=50)
     */
    protected $countries;

    /**
     * @var string Возрастное ограничение
     * @ORM\Column(name="CENZ", type="string_win1251", length=3)
     */
    protected $pg;

    /**
     * @var string Описание
     * @ORM\Column(name="ABOUT", type="text_win1251")
     */
    protected $description;

    /**
     * @var string Язык
     * @ORM\Column(name="LANG", type="string_win1251", length=3)
     */
    protected $language = 'ru';

    /**
     * @var string Язык субтитров
     * @ORM\Column(name="SUBTITLES", type="string_win1251", length=3)
     */
    protected $subtitles;

    /**
     * @var string Урл трейлера
     * @ORM\Column(name="TRAILER", type="string_win1251")
     */
    protected $trailerUrl;

    /**
     * @var string Урл постера
     * @ORM\Column(name="POSTER", type="string_win1251")
     */
    protected $posterUrl;

    /**
     * @var string Урл обложки
     * @ORM\Column(name="COVER", type="string_win1251")
     */
    protected $coverUrl;

    /**
     * @var Distributor Прокатчик
     * @ORM\JoinColumn(nullable=false, name="OWNERID", referencedColumnName="ID")
     * @ORM\ManyToOne(targetEntity="Illuzion\ApiBundle\Entity\Main\Distributor")
     */
    protected $distributor;

    /**
     * @var Program Программа
     * @ORM\JoinColumn(nullable=false, name="PROGRAM_ID", referencedColumnName="ID")
     * @ORM\ManyToOne(targetEntity="Illuzion\ApiBundle\Entity\Main\Program")
     */
    protected $program;

    /**
     * @var Genre[]|ArrayCollection Список жанров
     * @ORM\ManyToMany(targetEntity="Illuzion\ApiBundle\Entity\Main\Genre")
     * @ORM\JoinTable(name="MOVIES_GENRES",
     *      joinColumns={@ORM\JoinColumn(name="KEY_FILM", referencedColumnName="KEY_FILM")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="KEY_GENRE", referencedColumnName="KEY_GENRE")}
     * )
     */
    protected $genres;

    /**
     * @var FilmFormat[]|ArrayCollection Список форматов
     * @ORM\ManyToMany(targetEntity="Illuzion\ApiBundle\Entity\Main\FilmFormat")
     * @ORM\JoinTable(name="SEANCESFILMSFORMATS",
     *      joinColumns={@ORM\JoinColumn(name="KEY_FILM", referencedColumnName="KEY_FILM")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="FILMSFORMATS_ID", referencedColumnName="ID")}
     * )
     */
    protected $formats;

    /**
     * @var \DateTime Дата последнего обновления
     * @ORM\Column(name="UPDATED_AT", type="datetime")
     */
    protected $updatedAt;

    public function __construct()
    {
        $this->genres = new ArrayCollection();
        $this->formats = new ArrayCollection();
    }

    /**
     * @return int Глобальный Id
     */
    public function getId()
    {
        return $this->id ?: $this->localId;
    }

    /**
     * @param int $id Глобальный Id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = (int) $id;
        return $this;
    }

    /**
     * @return int Id в базе кинотеатра
     */
    public function getLocalId()
    {
        return $this->localId;
    }

    /**
     * @param int $localId Id в базе кинотеатра
     *
     * @return $this
     */
    public function setLocalId($localId)
    {
        $this->localId = $localId;
        return $this;
    }

    /**
     * @return string Название
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title Название
     *
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = (string) $title;
        return $this;
    }

    /**
     * @return string Алиас
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * @param string $alias Алиас
     *
     * @return $this
     */
    public function setAlias($alias)
    {
        $this->alias = (string) $alias;
        return $this;
    }

    /**
     * @return boolean Архивный
     */
    public function isArchived()
    {
        return $this->archived;
    }

    /**
     * @param boolean $archived Архивный
     *
     * @return $this
     */
    public function setArchived($archived)
    {
        $this->archived = (boolean) $archived;
        return $this;
    }

    /**
     * @return \DateTime Дата начала проката в кинотеатре
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param \DateTime $startDate Дата начала проката в кинотеатре
     *
     * @return $this
     */
    public function setStartDate(\DateTime $startDate = null)
    {
        $this->startDate = $startDate;
        return $this;
    }

    /**
     * @return string Режисер
     */
    public function getDirector()
    {
        return $this->director;
    }

    /**
     * @param string $director Режисер
     *
     * @return $this
     */
    public function setDirector($director)
    {
        $this->director = (string) $director;
        return $this;
    }

    /**
     * @return string Актреры
     */
    public function getActors()
    {
        return $this->actors;
    }

    /**
     * @param string $actors Актреры
     */
    public function setActors($actors)
    {
        $this->actors = (string) $actors;
    }

    /**
     * @return int Продолжительность
     */
    public function getRuntime()
    {
        return $this->runtime;
    }

    /**
     * @param int $runtime Продолжительность
     *
     * @return $this
     */
    public function setRuntime($runtime)
    {
        $this->runtime = (int) $runtime;
        return $this;
    }

    /**
     * @return int Номер лицензии
     */
    public function getLicenceNumber()
    {
        return $this->licenceNumber;
    }

    /**
     * @param int $licenceNumber Номер лицензии
     *
     * @return $this
     */
    public function setLicenceNumber($licenceNumber)
    {
        $this->licenceNumber = (string) $licenceNumber;
        return $this;
    }

    /**
     * @return int Год выхода
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param int $year Год выхода
     *
     * @return $this
     */
    public function setYear($year)
    {
        $this->year = (int) $year;
        return $this;
    }

    /**
     * @return string Страны
     */
    public function getCountries()
    {
        return $this->countries;
    }

    /**
     * @param string $countries Страны
     *
     * @return $this
     */
    public function setCountries($countries)
    {
        $this->countries = (string) $countries;
        return $this;
    }

    /**
     * @return string Возрастное ограничение
     */
    public function getPg()
    {
        return $this->pg;
    }

    /**
     * @param string $pg Возрастное ограничение
     *
     * @return $this
     */
    public function setPg($pg)
    {
        // Добвляем '+' в конец если необходимо
        if (is_numeric($pg)) {
            $pg = $pg.'+';
        }

        $this->pg = (string) $pg;
        return $this;
    }

    /**
     * @return string Описание
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description Описание
     *
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = (string) $description;
        return $this;
    }

    /**
     * @return string Язык
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param string $language Язык
     *
     * @return $this
     */
    public function setLanguage($language)
    {
        $this->language = (string) $language;
        return $this;
    }

    /**
     * @return string Язык субтитров
     */
    public function getSubtitles()
    {
        return $this->subtitles;
    }

    /**
     * @param string $subtitles Язык субтитров
     *
     * @return $this
     */
    public function setSubtitles($subtitles)
    {
        $this->subtitles = (string) $subtitles;
        return $this;
    }

    /**
     * @return string Урл трейлера
     */
    public function getTrailerUrl()
    {
        return $this->trailerUrl;
    }

    /**
     * @param string $trailerUrl Урл трейлера
     *
     * @return $this
     */
    public function setTrailerUrl($trailerUrl)
    {
        $this->trailerUrl = (string) $trailerUrl;
        return $this;
    }

    /**
     * @return string Урл постера
     */
    public function getPosterUrl()
    {
        return $this->posterUrl;
    }

    /**
     * @param string $posterUrl Урл постера
     *
     * @return $this
     */
    public function setPosterUrl($posterUrl)
    {
        $this->posterUrl = (string) $posterUrl;
        return $this;
    }

    /**
     * @return string Урл обложки
     */
    public function getCoverUrl()
    {
        return $this->coverUrl;
    }

    /**
     * @param string $coverUrl Урл обложки
     *
     * @return $this
     */
    public function setCoverUrl($coverUrl)
    {
        $this->coverUrl = (string) $coverUrl;
        return $this;
    }

    /**
     * @return Distributor Прокатчик
     */
    public function getDistributor()
    {
        return $this->distributor;
    }

    /**
     * @param Distributor $distributor Прокатчик
     *
     * @return $this
     */
    public function setDistributor(Distributor $distributor = null)
    {
        $this->distributor = $distributor;
        return $this;
    }

    /**
     * @return Program Программа
     */
    public function getProgram()
    {
        return $this->program;
    }

    /**
     * @param Program $program Программа
     *
     * @return $this
     */
    public function setProgram(Program $program = null)
    {
        $this->program = $program;
        return $this;
    }

    /**
     * @return Genre[]|ArrayCollection Список жанров
     */
    public function getGenres()
    {
        return $this->genres;
    }

    /**
     * @param Genre[] $genres Список жанров
     *
     * @return $this
     */
    public function setGenres($genres)
    {
        $this->genres = new ArrayCollection($genres);
        return $this;
    }

    /**
     * @return ArrayCollection|FilmFormat[] Список форматов
     */
    public function getFormats()
    {
        return $this->formats;
    }

    /**
     * @return string[] Список названий форматов
     */
    public function getFormatsArr()
    {
        $result = [];
        foreach ($this->formats as $format) {
            $result[] = $format->getTitle();
        }
        return $result;
    }

    /**
     * @param ArrayCollection|FilmFormat[] $formats Список форматов
     */
    public function setFormats($formats)
    {
        $this->formats = new ArrayCollection($formats);
    }

    /**
     * @return \DateTime Дата последнего обновления
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @return boolean Опубликован
     */
    public function isPublished()
    {
        return $this->published;
    }

    /**
     * @param boolean $published Опубликован
     *
     * @return $this
     */
    public function setPublished($published)
    {
        $this->published = $published;
        return $this;
    }
}

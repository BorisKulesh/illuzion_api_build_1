<?php

namespace Illuzion\ApiBundle\Entity\Main;

use Doctrine\ORM\Mapping as ORM;
use Illuzion\ApiBundle\Entity\LocalEntity;

/**
 * Место сеанса
 *
 * @ORM\Entity(repositoryClass="Illuzion\ApiBundle\Repository\SeatRepository")
 * @ORM\Table(name="PLACES")
 */
class Seat
{
    /**
     * @var integer
     * @ORM\Column(name="KEY_PLACE", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue()
     */
    protected $id;

    /**
     * @var string Ряд
     * @ORM\Column(name="LINE_PL", type="integer")
     */
    protected $row;

    /**
     * @var string Номер места
     * @ORM\Column(name="PLACE_PL", type="integer")
     */
    protected $place;

    /**
     * @var integer X координата левого вехнего угла
     * @ORM\Column(name="X", type="integer")
     */
    protected $leftTopX;

    /**
     * @var integer Y координата левого вехнего угла
     * @ORM\Column(name="Y", type="integer")
     */
    protected $leftTopY;

    /**
     * @var integer X координата правого нижнего угла
     * @ORM\Column(name="X1", type="integer")
     */
    protected $rightBottomX;

    /**
     * @var integer Y координата правого нижнего угла
     * @ORM\Column(name="Y1", type="integer")
     */
    protected $rightBottomY;

    /**
     * @var string Тип места
     * @ORM\Column(name="KEY_ST", type="seat_type")
     */
    protected $type;

    /**
     * @var Hall Зал
     * @ORM\JoinColumn(name="KEY_HALL", nullable=false, referencedColumnName="KEY_HALL")
     * @ORM\ManyToOne(targetEntity="Illuzion\ApiBundle\Entity\Main\Hall")
     */
    protected $hall;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = (int) $id;
    }

    /**
     * @return int Ряд
     */
    public function getRow()
    {
        return $this->row;
    }

    /**
     * @param string $row Ряд
     */
    public function setRow($row)
    {
        $this->row = (string) $row;
    }

    /**
     * @return string Номер места
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * @param string $place Номер места
     */
    public function setPlace($place)
    {
        $this->place = (string) $place;
    }

    /**
     * @return int X координата левого вехнего угла
     */
    public function getLeftTopX()
    {
        return $this->leftTopX;
    }

    /**
     * @param int $leftTopX X координата левого вехнего угла
     */
    public function setLeftTopX($leftTopX)
    {
        $this->leftTopX = (int) $leftTopX;
    }

    /**
     * @return int Y координата левого вехнего угла
     */
    public function getLeftTopY()
    {
        return $this->leftTopY;
    }

    /**
     * @param int $leftTopY Y координата левого вехнего угла
     */
    public function setLeftTopY($leftTopY)
    {
        $this->leftTopY = (int) $leftTopY;
    }

    /**
     * @return int X координата правого нижнего угла
     */
    public function getRightBottomX()
    {
        return $this->rightBottomX;
    }

    /**
     * @param int $rightBottomX X координата правого нижнего угла
     */
    public function setRightBottomX($rightBottomX)
    {
        $this->rightBottomX = (int) $rightBottomX;
    }

    /**
     * @return int Y координата правого нижнего угла
     */
    public function getRightBottomY()
    {
        return $this->rightBottomY;
    }

    /**
     * @param int $rightBottomY Y координата правого нижнего угла
     */
    public function setRightBottomY($rightBottomY)
    {
        $this->rightBottomY = (int) $rightBottomY;
    }

    /**
     * @return string Тип места
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type Тип места
     */
    public function setType($type)
    {
        $this->type = (string) $type;
    }

    /**
     * @return Hall Зал
     */
    public function getHall()
    {
        return $this->hall;
    }

    /**
     * @param Hall $hall Зал
     */
    public function setHall(Hall $hall)
    {
        $this->hall = $hall;
    }
}

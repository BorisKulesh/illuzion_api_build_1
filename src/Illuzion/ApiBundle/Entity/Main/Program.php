<?php

namespace Illuzion\ApiBundle\Entity\Main;

use Doctrine\ORM\Mapping as ORM;

/**
 * Программа
 *
 * @ORM\Entity(repositoryClass="Illuzion\ApiBundle\Repository\ProgramRepository")
 * @ORM\Table(name="PROGRAMS")
 */
class Program
{
    /**
     * @var integer
     * @ORM\Column(name="ID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Illuzion\ApiBundle\Entity\Generator\ProgramIdGenerator")
     */
    protected $id;

    /**
     * @var string Названеи
     * @ORM\Column(name="TITLE", type="string_win1251")
     */
    protected $title;

    /**
     * @var string Алиас
     * @ORM\Column(name="ALIAS", type="string_win1251")
     */
    protected $alias;

    /**
     * @var string Дополнительное название
     * @ORM\Column(name="SUBTITLE", type="string_win1251")
     */
    protected $subtitle;

    /**
     * @var string Краткое описание
     * @ORM\Column(name="DESCRIPTION_SHORT", type="text_win1251")
     */
    protected $descriptionShort;

    /**
     * @var string Полное описание
     * @ORM\Column(name="DESCRIPTION_FULL", type="text_win1251")
     */
    protected $descriptionFull;

    /**
     * @var Program Родительская программа
     * @ORM\JoinColumn(nullable=false, name="PARENT_ID", referencedColumnName="ID")
     * @ORM\ManyToOne(targetEntity="Illuzion\ApiBundle\Entity\Main\Program")
     */
    protected $parent;

    /**
     * @var \DateTime Дата последнего обновления
     * @ORM\Column(name="UPDATED_AT", type="datetime")
     */
    protected $updatedAt;

    /**
     * @var string Урл постера
     * @ORM\Column(name="POSTER_URL", type="string_win1251")
     */
    protected $posterUrl;

    /**
     * @var string Урл обложки
     * @ORM\Column(name="COVER_URL", type="string_win1251")
     */
    protected $coverUrl;

    /**
     * @var boolean Опубликован
     * @ORM\Column(name="PUBLISHED", type="boolean")
     */
    protected $published;

    /**
     * @var string Id города
     * @ORM\Column(name="CITY_ID", type="string")
     */
    protected $cityId;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = (int) $id;
        return $this;
    }

    /**
     * @return string Названеи
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title Названеи
     *
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = (string) $title;
        return $this;
    }

    /**
     * @return string Алиас
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * @param string $alias Алиас
     *
     * @return $this
     */
    public function setAlias($alias)
    {
        $this->alias = (string) $alias;
        return $this;
    }

    /**
     * @return string Дополнительное название
     */
    public function getSubtitle()
    {
        return $this->subtitle;
    }

    /**
     * @param string $subtitle Дополнительное название
     *
     * @return $this
     */
    public function setSubtitle($subtitle)
    {
        $this->subtitle = (string) $subtitle;
        return $this;
    }

    /**
     * @return string Краткое описание
     */
    public function getDescriptionShort()
    {
        return $this->descriptionShort;
    }

    /**
     * @param string $descriptionShort Краткое описание
     *
     * @return $this
     */
    public function setDescriptionShort($descriptionShort)
    {
        $this->descriptionShort = (string) $descriptionShort;
        return $this;
    }

    /**
     * @return string Полное описание
     */
    public function getDescriptionFull()
    {
        return $this->descriptionFull;
    }

    /**
     * @param string $descriptionFull Полное описание
     *
     * @return $this
     */
    public function setDescriptionFull($descriptionFull)
    {
        $this->descriptionFull = (string) $descriptionFull;
        return $this;
    }

    /**
     * @return Program Родительская программа
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param Program $parent Родительская программа
     *
     * @return $this
     */
    public function setParent(Program $parent = null)
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     * @return \DateTime Дата последнего обновления
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @return string Урл постера
     */
    public function getPosterUrl()
    {
        return $this->posterUrl;
    }

    /**
     * @param string $posterUrl Урл постера
     *
     * @return $this
     */
    public function setPosterUrl($posterUrl)
    {
        $this->posterUrl = (string) $posterUrl;
        return $this;
    }

    /**
     * @return string Урл обложки
     */
    public function getCoverUrl()
    {
        return $this->coverUrl;
    }

    /**
     * @param string $coverUrl Урл обложки
     *
     * @return $this
     */
    public function setCoverUrl($coverUrl)
    {
        $this->coverUrl = (string) $coverUrl;
        return $this;
    }

    /**
     * @return bool Опубликован
     */
    public function isPublished()
    {
        return $this->published;
    }

    /**
     * @param bool $published Опубликован
     *
     * @return $this
     */
    public function setPublished($published)
    {
        $this->published = (boolean) $published;
        return $this;
    }

    /**
     * @return string Id города
     */
    public function getCityId()
    {
        return $this->cityId;
    }

    /**
     * @param string $cityId Id города
     *
     * @return $this
     */
    public function setCityId($cityId)
    {
        $this->cityId = $cityId;
        return $this;
    }
}

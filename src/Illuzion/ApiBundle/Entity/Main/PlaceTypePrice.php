<?php

namespace Illuzion\ApiBundle\Entity\Main;

use Doctrine\ORM\Mapping as ORM;

/**
 * Ценовые схемы сеанса
 *
 * @ORM\Table(name="TICKET_PRICE")
 * @ORM\Entity(repositoryClass="Illuzion\ApiBundle\Repository\PlaceTypePriceRepository")
 */
class PlaceTypePrice
{
    /**
     * @var integer Тип места
     * @ORM\Column(name="KEY_ST", type="integer")
     * @ORM\Id()
     */
    protected $seatType;

    /**
     * @var integer Id ценовой схемы
     * @ORM\Column(name="KEY_SCHEM", type="integer")
     * @ORM\Id()
     */
    protected $schemeId;

    /**
     * @var integer Цена со скидкой
     * @ORM\Column(name="WITH_DISC", type="integer")
     */
    protected $priceWithDiscount;

    /**
     * @var integer Цена
     * @ORM\Column(name="PRICE_TICK_SEANSE", type="integer")
     */
    protected $price;

    /**
     * @return int Тип места
     */
    public function getSeatType()
    {
        return $this->seatType;
    }

    /**
     * @param int $seatType Тип места
     *
     * @return $this
     */
    public function setSeatType($seatType)
    {
        $this->seatType = (int) $seatType;
        return $this;
    }

    /**
     * @return int Id ценовой схемы
     */
    public function getSchemeId()
    {
        return $this->schemeId;
    }

    /**
     * @param int $schemeId Id ценовой схемы
     *
     * @return $this
     */
    public function setSchemeId($schemeId)
    {
        $this->schemeId = (int) $schemeId;
        return $this;
    }

    /**
     * @return int Цена со скидкой
     */
    public function getPriceWithDiscount()
    {
        return $this->priceWithDiscount;
    }

    /**
     * @param int $priceWithDiscount Цена со скидкой
     *
     * @return $this
     */
    public function setPriceWithDiscount($priceWithDiscount)
    {
        $this->priceWithDiscount = (int) $priceWithDiscount;
        return $this;
    }

    /**
     * @return int Цена
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param int $price Цена
     *
     * @return $this
     */
    public function setPrice($price)
    {
        $this->price = (int) $price;
        return $this;
    }
}

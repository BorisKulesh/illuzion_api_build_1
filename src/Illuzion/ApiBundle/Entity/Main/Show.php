<?php

namespace Illuzion\ApiBundle\Entity\Main;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Illuzion\ApiBundle\Entity\LocalEntity;

/**
 * Сеанс
 *
 * @ORM\Entity(repositoryClass="Illuzion\ApiBundle\Repository\ShowRepository")
 * @ORM\Table(name="SEANCE")
 */
class Show extends LocalEntity
{
    /**
     * @var integer
     * @ORM\Column(name="KEY_SEAN", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue()
     */
    protected $id;

    /**
     * @var \DateTime Дата начала
     * @ORM\Column(name="DATETIME_SEAN", type="datetime")
     */
    protected $datetime;

    /**
     * @var integer Id ценовой схемы
     * @ORM\Column(name="KEY_SCHEM", type="integer")
     */
    protected $schemeId;

    /**
     * @var integer Набор флагов
     * @ORM\Column(name="ISACTIVE", type="integer")
     */
    protected $isActiveMask;

    /**
     * @var Hall Зал
     * @ORM\JoinColumn(nullable=false, name="KEY_HALL", referencedColumnName="KEY_HALL")
     * @ORM\ManyToOne(targetEntity="Illuzion\ApiBundle\Entity\Main\Hall", inversedBy="shows")
     */
    protected $hall;

    /**
     * @var FilmFormat Формат
     * @ORM\JoinColumn(nullable=false, name="SEANCE_FORMAT", referencedColumnName="ID")
     * @ORM\ManyToOne(targetEntity="Illuzion\ApiBundle\Entity\Main\FilmFormat")
     */
    protected $format;

    /**
     * @var Movie Фильм
     * @ORM\JoinColumn(nullable=false, name="KEY_FILM", referencedColumnName="KEY_FILM")
     * @ORM\ManyToOne(targetEntity="Illuzion\ApiBundle\Entity\Main\Movie")
     */
    protected $movie;

    /**
     * @var \DateTime Дата последнего обновления
     * @ORM\Column(name="UPDATED_AT", type="datetime")
     */
    protected $updatedAt;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = (int) $id;
        return $this;
    }

    /**
     * @return \DateTime Дата начала
     */
    public function getDatetime()
    {
        return $this->datetime;
    }

    /**
     * @return \DateTime Дата начала, с учетом смещения для ночных сеансов
     */
    public function getActualDatetime()
    {
        $dt = clone $this->datetime;
        if ($dt->format('H') < 9) {
            $dt->modify('+1 day');
        }
        return $dt;
    }

    /**
     * @param \DateTime $datetime Дата начала
     *
     * @return $this
     */
    public function setDatetime(\DateTime $datetime)
    {
        $this->datetime = $datetime;
        return $this;
    }

    /**
     * @return Hall Зал
     */
    public function getHall()
    {
        return $this->hall;
    }

    /**
     * @param Hall $hall Зал
     *
     * @return $this
     */
    public function setHall(Hall $hall)
    {
        $this->hall = $hall;
        return $this;
    }

    /**
     * @return Movie Фильм
     */
    public function getMovie()
    {
        return $this->movie;
    }

    /**
     * @param Movie $movie Фильм
     *
     * @return $this
     */
    public function setMovie(Movie $movie)
    {
        $this->movie = $movie;
        return $this;
    }

    /**
     * @return int Id ценовой схемы
     */
    public function getSchemeId()
    {
        return $this->schemeId;
    }

    /**
     * @param int $schemeId Id ценовой схемы
     *
     * @return $this
     */
    public function setSchemeId($schemeId)
    {
        $this->schemeId = (int) $schemeId;
        return $this;
    }

    /**
     * @return \DateTime Дата последнего обновления
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @return FilmFormat Формат
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * @param FilmFormat $format Формат
     */
    public function setFormat(FilmFormat $format)
    {
        $this->format = $format;
    }

    /**
     * @return bool Опубиликован
     */
    public function isPublished() {
        return ($this->isActiveMask & 0x040) === 0;
    }

    /**
     * @return bool Доступна ли покупка за бонусы
     */
    public function isBonusBuyAllowed() {
        return ($this->isActiveMask & 0x004) === 0;
    }
}

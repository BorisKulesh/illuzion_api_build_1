<?php

namespace Illuzion\ApiBundle\Entity\Main;

use Doctrine\ORM\Mapping as ORM;

/**
 * Жанр
 *
 * @ORM\Entity(repositoryClass="Illuzion\ApiBundle\Repository\GenreRepository")
 * @ORM\Table(name="GENRES")
 */
class Genre
{
    /**
     * @var integer
     * @ORM\Column(name="KEY_GENRE", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue()
     */
    protected $id;

    /**
     * @var string Название
     * @ORM\Column(name="TITLE", type="string_win1251")
     */
    protected $title;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = (int) $id;
    }

    /**
     * @return string Название
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title Название
     */
    public function setTitle($title)
    {
        $this->title = (string) $title;
    }
}

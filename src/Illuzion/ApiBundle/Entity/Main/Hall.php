<?php

namespace Illuzion\ApiBundle\Entity\Main;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Illuzion\ApiBundle\Entity\LocalEntity;

/**
 * Зал
 *
 * @ORM\Entity(repositoryClass="Illuzion\ApiBundle\Repository\HallRepository")
 * @ORM\Table(name="HALLS")
 */
class Hall extends LocalEntity
{
    /**
     * @var integer Id в базе кинотеатра
     * @ORM\Column(name="KEY_HALL", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue()
     */
    protected $id;

    /**
     * @var string Название
     * @ORM\Column(name="NAME_HALL", type="string_win1251")
     */
    protected $title;

    /**
     * @var Show[]|ArrayCollection Список сеансов
     * @ORM\OneToMany(targetEntity="Illuzion\ApiBundle\Entity\Main\Show", mappedBy="hall")
     */
    protected $shows;

    /**
     * @var \DateTime Дата последнего обновления
     * @ORM\Column(name="UPDATED_AT", type="datetime")
     */
    protected $updatedAt;

    /**
     * @var boolean Доступен ли зал
     * @ORM\Column(name="ACTIVITY", type="boolean")
     */
    protected $activity;

    /**
     * @return int Id в базе кинотеатра
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id Id в базе кинотеатра
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = (int) $id;
        return $this;
    }

    /**
     * @return string Название
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title Название
     *
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = (string) $title;
        return $this;
    }

    /**
     * @return \DateTime Дата последнего обновления
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}

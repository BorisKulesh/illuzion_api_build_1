<?php

namespace Illuzion\ApiBundle\Entity\Main;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Illuzion\ApiBundle\Entity\LocalEntity;

/**
 * Билеты
 *
 * @ORM\Entity(repositoryClass="Illuzion\ApiBundle\Repository\TicketRepository")
 * @ORM\Table(name="TICKET")
 */
class Ticket extends LocalEntity
{
    /**
     * @var integer
     * @ORM\Column(name="NOM_TICK", type="integer")
     * @ORM\GeneratedValue()
     * @ORM\Id()
     */
    protected $id;

    /**
     * @var boolean Возвращен
     * @ORM\Column(name="MONEY_BACK", type="boolean")
     */
    protected $returned;

    /**
     * @var Show Сеанс
     * @ORM\JoinColumn(nullable=false, name="KEY_SEAN", referencedColumnName="KEY_SEAN")
     * @ORM\ManyToOne(targetEntity="Illuzion\ApiBundle\Entity\Main\Show")
     */
    protected $show;

    /**
     * @var Seat Место
     * @ORM\JoinColumn(nullable=false, name="KEY_PLACE", referencedColumnName="KEY_PLACE")
     * @ORM\ManyToOne(targetEntity="Illuzion\ApiBundle\Entity\Main\Seat")
     */
    protected $seat;

    /**
     * @var Order Заказ, когда он еще не оплачен
     * @ORM\JoinColumn(nullable=false, name="RESERV_ID", referencedColumnName="RESERV_ID")
     * @ORM\ManyToOne(targetEntity="Illuzion\ApiBundle\Entity\Main\Order", inversedBy="tickets")
     */
    protected $order;

    /**
     * @var Ticket[]|ArrayCollection Заказ, когда он оплачен (берем первый)
     * @ORM\ManyToMany(targetEntity="Illuzion\ApiBundle\Entity\Main\Order", mappedBy="soldTickets")
     */
    protected $soldOrder;

    public function __construct()
    {
        $this->soldOrder = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = (int) $id;
    }

    /**
     * @return boolean Возвращен
     */
    public function getReturned()
    {
        return $this->returned;
    }

    /**
     * @param boolean $returned Возвращен
     */
    public function setReturned($returned)
    {
        $this->returned = (boolean) $returned;
    }

    /**
     * @return Show Сеанс
     */
    public function getShow()
    {
        return $this->show;
    }

    /**
     * @param Show $show Сеанс
     */
    public function setShow(Show $show)
    {
        $this->show = $show;
    }

    /**
     * @return Seat Место
     */
    public function getSeat()
    {
        return $this->seat;
    }

    /**
     * @param Seat $seat Место
     */
    public function setSeat(Seat $seat)
    {
        $this->seat = $seat;
    }

    /**
     * @return Order Заказ
     */
    public function getOrder()
    {
        return $this->order ?: $this->soldOrder->first();
    }
}

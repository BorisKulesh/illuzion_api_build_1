<?php

namespace Illuzion\ApiBundle\Entity\Main;

use Doctrine\ORM\Mapping as ORM;

/**
 * Прокатчик
 *
 * @ORM\Entity(repositoryClass="Illuzion\ApiBundle\Repository\DistributorRepository")
 * @ORM\Table(name="OWNERS")
 */
class Distributor
{
    /**
     * @var integer
     * @ORM\Column(name="ID", type="integer")
     * @ORM\Id
     */
    protected $id;

    /**
     * @var string Название
     * @ORM\Column(name="OWNER", type="string_win1251")
     */
    protected $title;

    /**
     * @var string Контакты
     * @ORM\Column(name="EMAILS", type="string_win1251")
     */
    protected $emails;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = (int) $id;
        return $this;
    }

    /**
     * @return string Название
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title Название
     *
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = (string) $title;
        return $this;
    }

    /**
     * @return string Контакты
     */
    public function getEmails()
    {
        return $this->emails;
    }

    /**
     * @param string $emails Контакты
     *
     * @return $this
     */
    public function setEmails($emails)
    {
        $this->emails = (string) $emails;
        return $this;
    }
}

<?php

namespace Illuzion\ApiBundle\Entity\Main;

use Doctrine\ORM\Mapping as ORM;

/**
 * Форматы сеансов (IMAX, 2D, 3D, ...)
 *
 * @ORM\Entity(repositoryClass="Illuzion\ApiBundle\Repository\FilmFormatRepository")
 * @ORM\Table(name="FILMFORMATS")
 */
class FilmFormat
{
    /**
     * @var integer
     * @ORM\Column(name="ID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue()
     */
    protected $id;

    /**
     * @var string Названеи
     * @ORM\Column(name="NAME_FORMAT", type="string_win1251")
     */
    protected $title;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = (int) $id;
    }

    /**
     * @return string Названеи
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title Названеи
     */
    public function setTitle($title)
    {
        $this->title = (string) $title;
    }
}

<?php

namespace Illuzion\ApiBundle\Entity\Main;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Illuzion\ApiBundle\Entity\LocalEntity;

/**
 * Заказ
 *
 * @ORM\Entity(repositoryClass="Illuzion\ApiBundle\Repository\OrderRepository")
 * @ORM\Table(name="PAYMENTS")
 */
class Order extends LocalEntity
{
    /** @var string[] Соответсвие статусов заказов в базе и в АПИ  */
    protected static $STATUS_TYPE_MAP = [
        0 => 'reserved',
        1 => 'paid',
        2 => 'reserve_released',
        3 => 'partial_returned',
        4 => 'full_returned',
        5 => 'paid',
        6 => 'full_returned',
    ];

    /**
     * Получить список статусов закатов базы по статусу из АПИ
     * @param string $status Статус заказа в АПИ
     * @return int[] Статусы заказов в базе
     */
    public static function getStatusId($status)
    {
        $result = [];
        foreach (self::$STATUS_TYPE_MAP as $id => $str) {
            if ($status === $str) {
                $result[] = $id;
            }
        }
        return $result;
    }

    /**
     * @var integer Id заказа в базе кинотеатра
     * @ORM\Column(name="RESERV_ID", type="integer")
     * @ORM\GeneratedValue()
     * @ORM\Id()
     */
    protected $id;

    /**
     * @var integer Id пользователя
     * @ORM\Column(name="GLOBAL_ID", type="integer")
     */
    protected $customerId;

    /**
     * @var integer Статус в базе
     * @ORM\Column(name="STATUS", type="integer")
     */
    protected $status;

    /**
     * @var integer Полная стоимость
     * @ORM\Column(name="TOTAL", type="integer")
     */
    protected $totalCost;

    /**
     * @var \DateTime Дата покупки
     * @ORM\Column(name="FIRST_BUY", type="datetime")
     */
    protected $purchaseDate;

    /**
     * @var string Почта пользователя
     * @ORM\Column(name="EMAIL", type="string_win1251")
     */
    protected $email;

    /**
     * @var string Телефон пользователя
     * @ORM\Column(name="PHONE", type="string_win1251")
     */
    protected $phone;

    /**
     * @var string Клиент АПИ
     * @ORM\Column(name="CLIENT", type="string_win1251")
     */
    protected $client;

    /**
     * @var string Тип покупки
     * @ORM\Column(name="PAYMENT_TYPE", type="string_win1251")
     */
    protected $paymentType;

    /**
     * @var string Используемая ценовая схема
     * @ORM\Column(name="PROMO", type="string_win1251")
     */
    protected $promo;

    /**
     * @var integer Id транзакции платежной системы
     * @ORM\Column(name="ID", type="integer")
     */
    protected $transactionId;

    /**
     * @var string Номер карты платежной системы
     * @ORM\Column(name="CREDITCARDNUMBER", type="string_win1251")
     */
    protected $creditCardNumber;

    /**
     * @var string Владелец карты латежной системы
     * @ORM\Column(name="CARDHOLDER", type="string_win1251")
     */
    protected $creditCardHolder;

    /**
     * @var string Код покупки
     * @ORM\Column(name="ACCESS_CODE", type="string_win1251")
     */
    protected $printCode;

    /**
     * @var \DateTime Дата распечатки билетов
     * @ORM\Column(name="PRINT_DATE", type="datetime")
     */
    protected $printed;

    /**
     * @var \DateTime Дата последнего изменеия
     * @ORM\Column(name="DATETIME", type="datetime")
     */
    protected $lastModifiedDate;

    /**
     * @var Show Сеанс
     * @ORM\JoinColumn(nullable=false, name="KEY_SEAN", referencedColumnName="KEY_SEAN")
     * @ORM\ManyToOne(targetEntity="Illuzion\ApiBundle\Entity\Main\Show")
     */
    protected $show;

    /**
     * @var Ticket[]|ArrayCollection Список билетов, если заказ еще не оплачен
     * @ORM\OneToMany(targetEntity="Illuzion\ApiBundle\Entity\Main\Ticket", mappedBy="order")
     */
    protected $tickets;

    /**
     * @var Ticket[]|ArrayCollection Списов билетов, если зака оплачен
     * @ORM\ManyToMany(targetEntity="Illuzion\ApiBundle\Entity\Main\Ticket", inversedBy="soldOrder")
     * @ORM\JoinTable(name="RESERVAT_SOLD",
     *      joinColumns={@ORM\JoinColumn(name="RESERV_ID", referencedColumnName="RESERV_ID")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="NOM_TICK", referencedColumnName="NOM_TICK")}
     * )
     */
    protected $soldTickets;

    public function __construct()
    {
        $this->tickets = new ArrayCollection();
        $this->soldTickets = new ArrayCollection();
    }

    /**
     * @return int Id заказа в базе кинотеатра
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id Id заказа в базе кинотеатра
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = (int) $id;
        return $this;
    }

    /**
     * @return int Id пользователя
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     * @param int $customerId Id пользователя
     *
     * @return $this
     */
    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;
        return $this;
    }

    /**
     * @return int Статус в базе
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return string Статус в АПИ
     */
    public function getStatusStr()
    {
        if (!array_key_exists($this->status, self::$STATUS_TYPE_MAP)) {
            return 'error';
        }
        return self::$STATUS_TYPE_MAP[$this->status];
    }

    /**
     * @param int $status Статус в базе
     *
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = (int) $status;
        return $this;
    }

    /**
     * @return int Полная стоимость
     */
    public function getTotalCost()
    {
        return $this->totalCost;
    }

    /**
     * @param int $totalCost Полная стоимость
     *
     * @return $this
     */
    public function setTotalCost($totalCost)
    {
        $this->totalCost = (int) $totalCost;
        return $this;
    }

    /**
     * @return \DateTime Дата покупки
     */
    public function getPurchaseDate()
    {
        return $this->purchaseDate;
    }

    /**
     * @param \DateTime $purchaseDate Дата покупки
     *
     * @return $this
     */
    public function setPurchaseDate(\DateTime $purchaseDate)
    {
        $this->purchaseDate = $purchaseDate;
        return $this;
    }

    /**
     * @return string Номер карты платежной системы
     */
    public function getCreditCardNumber()
    {
        return $this->creditCardNumber ? trim($this->creditCardNumber) : null;
    }

    /**
     * @param string $creditCardNumber Номер карты платежной системы
     *
     * @return $this
     */
    public function setCreditCardNumber($creditCardNumber)
    {
        $this->creditCardNumber = (string) $creditCardNumber;
        return $this;
    }

    /**
     * @return string Владелец карты латежной системы
     */
    public function getCreditCardHolder()
    {
        return $this->creditCardHolder;
    }

    /**
     * @param string $creditCardHolder Владелец карты латежной системы
     *
     * @return $this
     */
    public function setCreditCardHolder($creditCardHolder)
    {
        $this->creditCardHolder = $creditCardHolder;
        return $this;
    }

    /**
     * @return string Почта пользователя
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email Почта пользователя
     *
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = (string) $email;
        return $this;
    }

    /**
     * @return string Телефон пользователя
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone Телефон пользователя
     *
     * @return $this
     */
    public function setPhone($phone)
    {
        $this->phone = (string) $phone;
        return $this;
    }

    /**
     * @return string Клиент АПИ
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param string $client Клиент АПИ
     *
     * @return $this
     */
    public function setClient($client)
    {
        $this->client = (string) $client;
        return $this;
    }

    /**
     * @return integer Id транзакции платежной системы
     */
    public function getTransactionId()
    {
        return $this->transactionId;
    }

    /**
     * @param integer $transactionId Id транзакции платежной системы
     *
     * @return $this
     */
    public function setTransactionId($transactionId)
    {
        $this->transactionId = (integer) $transactionId;
        return $this;
    }

    /**
     * @return string Код покупки
     */
    public function getPrintCode()
    {
        return $this->printCode ? trim($this->printCode) : null;
    }

    /**
     * @param string $printCode Код покупки
     *
     * @return $this
     */
    public function setPrintCode($printCode)
    {
        $this->printCode = (string) $printCode;
        return $this;
    }

    /**
     * @return \DateTime Дата распечатки билетов
     */
    public function getPrinted()
    {
        return $this->printed;
    }

    /**
     * @param \DateTime $printed Дата распечатки билетов
     *
     * @return $this
     */
    public function setPrinted(\DateTime $printed)
    {
        $this->printed = $printed;
        return $this;
    }

    /**
     * @return \DateTime Дата последнего изменеия
     */
    public function getLastModifiedDate()
    {
        return $this->lastModifiedDate;
    }

    /**
     * @param \DateTime $lastModifiedDate Дата последнего изменеия
     *
     * @return $this
     */
    public function setLastModifiedDate(\DateTime $lastModifiedDate)
    {
        $this->lastModifiedDate = $lastModifiedDate;
        return $this;
    }

    /**
     * @return string Тип покупки
     */
    public function getPaymentType()
    {
        return $this->paymentType;
    }

    /**
     * @param string $paymentType Тип покупки
     *
     * @return $this
     */
    public function setPaymentType($paymentType)
    {
        $this->paymentType = $paymentType;
        return $this;
    }

    /**
     * @return string Используемая ценовая схема
     */
    public function getPromo()
    {
        return $this->promo;
    }

    /**
     * @param string $promo Используемая ценовая схема
     *
     * @return $this
     */
    public function setPromo($promo)
    {
        $this->promo = $promo;
        return $this;
    }

    /**
     * @return Show Сеанс
     */
    public function getShow()
    {
        return $this->show;
    }

    /**
     * @param Show $show Сеанс
     *
     * @return $this
     */
    public function setShow(Show $show)
    {
        $this->show = $show;
        return $this;
    }

    /**
     * @return ArrayCollection|Ticket[] Список билетов
     */
    public function getTickets()
    {
        return $this->getStatusStr() === 'paid' ? $this->soldTickets : $this->tickets;
    }
}

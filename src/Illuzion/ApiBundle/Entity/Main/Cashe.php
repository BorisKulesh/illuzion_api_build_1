<?php

namespace Illuzion\ApiBundle\Entity\Main;

use Doctrine\ORM\Mapping as ORM;

/**
 * Нечто, что нужно закрывать, чтобы работала покупка
 *
 * @ORM\Entity(repositoryClass="Illuzion\ApiBundle\Repository\CasheRepository")
 * @ORM\Table(name="CASHE")
 */
class Cashe
{
    /**
     * @var string
     * @ORM\Column(name="USER_OPERATION", type="string_win1251")
     * @ORM\Id
     */
    protected $operator;

    /**
     * @var \DateTime
     * @ORM\Column(name="DATE_SALE1", type="string_win1251")
     * @ORM\Id
     */
    protected $date;

    /**
     * @var boolean
     * @ORM\Column(name="ISACTIVE", type="boolean")
     */
    protected $active;

    /**
     * @var integer
     * @ORM\Column(name="START_TICK", type="integer")
     */
    protected $startTick;

    /**
     * @var integer
     * @ORM\Column(name="LAST_TICK", type="integer", nullable=true)
     */
    protected $lastTick;

    /**
     * @var string
     * @ORM\Column(name="MACHINENUM", type="string_win1251")
     */
    protected $machine;

    /**
     * @var string
     * @ORM\Column(name="IP", type="string_win1251")
     */
    protected $ip;

    /**
     * @return string
     */
    public function getOperator()
    {
        return $this->operator;
    }

    /**
     * @param string $operator
     *
     * @return $this
     */
    public function setOperator($operator)
    {
        $this->operator = $operator;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     *
     * @return $this
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param boolean $active
     *
     * @return $this
     */
    public function setActive($active)
    {
        $this->active = $active;
        return $this;
    }

    /**
     * @return int
     */
    public function getStartTick()
    {
        return $this->startTick;
    }

    /**
     * @param int $startTick
     *
     * @return $this
     */
    public function setStartTick($startTick)
    {
        $this->startTick = $startTick;
        return $this;
    }

    /**
     * @return int
     */
    public function getLastTick()
    {
        return $this->lastTick;
    }

    /**
     * @param int $lastTick
     *
     * @return $this
     */
    public function setLastTick($lastTick)
    {
        $this->lastTick = $lastTick;
        return $this;
    }

    /**
     * @return string
     */
    public function getMachine()
    {
        return $this->machine;
    }

    /**
     * @param string $machine
     *
     * @return $this
     */
    public function setMachine($machine)
    {
        $this->machine = $machine;
        return $this;
    }

    /**
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @param string $ip
     *
     * @return $this
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
        return $this;
    }
}

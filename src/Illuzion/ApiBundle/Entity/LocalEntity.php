<?php

namespace Illuzion\ApiBundle\Entity;

/**
 * Базовый класс для всех сущностей, которые хранятся только в базе своего кинотеатра (заказа, сеансы, ...)
 */
abstract class LocalEntity
{
    /** @var string Id кинотаетра */
    protected $cinemaId;

    /**
     * Id сущности в базе кинотеатра
     * @return mixed
     */
    abstract function getId();

    /**
     * @return string Id кинотаетра
     */
    public function getCinemaId()
    {
        return $this->cinemaId;
    }

    /**
     * @param string $cinemaId Id кинотаетра
     *
     * @return $this
     */
    public function setCinemaId($cinemaId)
    {
        $this->cinemaId = $cinemaId;
        return $this;
    }

    /**
     * @return string Id с которым сущность отдается через апи
     */
    public function getPublicId()
    {
        return $this->cinemaId.'_'.$this->getId();
    }
}

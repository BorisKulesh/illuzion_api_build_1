<?php

namespace Illuzion\ApiBundle\Entity\Shared;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Город
 *
 * @ORM\Entity(repositoryClass="Illuzion\ApiBundle\Repository\CityRepository")
 */
class City
{
    /**
     * @var string
     * @ORM\Id()
     * @ORM\Column(type="string")
     */
    protected $id;

    /**
     * @var string Название
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @var Cinema[]|ArrayCollection Список кинотеатров
     * @ORM\OneToMany(targetEntity="Illuzion\ApiBundle\Entity\Shared\Cinema", mappedBy="city")
     */
    protected $cinemas;

    public function __construct()
    {
        $this->cinemas = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = (string) $id;
        return $this;
    }

    /**
     * @return string Название
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name Название
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = (string) $name;
        return $this;
    }

    /**
     * @return ArrayCollection|Cinema[] Список кинотеатров
     */
    public function getCinemas()
    {
        return $this->cinemas;
    }
}

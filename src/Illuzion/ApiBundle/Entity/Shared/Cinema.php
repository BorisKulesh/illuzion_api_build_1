<?php

namespace Illuzion\ApiBundle\Entity\Shared;

use Doctrine\ORM\Mapping as ORM;

/**
 * Кинотеатр
 *
 * @ORM\Entity(repositoryClass="Illuzion\ApiBundle\Repository\CinemaRepository")
 * @ORM\Table(uniqueConstraints={})
 */
class Cinema
{
    /**
     * @var string
     * @ORM\Id()
     * @ORM\Column(type="string")
     */
    protected $id;

    /**
     * @var string Название
     * @ORM\Column(type="string")
     */
    protected $title;

    /**
     * @var string Текст страницы
     * @ORM\Column(type="text", nullable=true)
     */
    protected $page;

    /**
     * @var City Город
     * @ORM\JoinColumn(nullable=false)
     * @ORM\ManyToOne(targetEntity="Illuzion\ApiBundle\Entity\Shared\City", inversedBy="cinemas")
     */
    protected $city;

    /**
     * @var string Алиас
     * @ORM\Column(type="string")
     */
    protected $alias;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = (string) $id;
        return $this;
    }

    /**
     * @return string Название
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title Название
     *
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = (string) $title;
        return $this;
    }

    /**
     * @return string Текст страницы
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param string $page Текст страницы
     *
     * @return $this
     */
    public function setPage($page)
    {
        $this->page = (string) $page;
        return $this;
    }

    /**
     * @return City Город
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param City $city Город
     *
     * @return $this
     */
    public function setCity(City $city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return string Алиас
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * @param string $alias Алиас
     *
     * @return $this
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;
        return $this;
    }
}

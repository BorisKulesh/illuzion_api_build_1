<?php

namespace Illuzion\ApiBundle\Entity\Shared;

use Doctrine\ORM\Mapping as ORM;

/**
 * Дата старта продаж фильма в городе
 *
 * @ORM\Entity(repositoryClass="Illuzion\ApiBundle\Repository\ReleaseDateRepository")
 * @ORM\Table(uniqueConstraints={
 *     @ORM\UniqueConstraint(columns={"city_id", "movie_id"}, name="UNQ_CITY_MOVIE")
 * })
 */
class ReleaseDate
{
    /**
     * @var integer
     * @ORM\Id()
     * @ORM\Column(type="integer", options={"unsigned": true})
     * @ORM\GeneratedValue()
     */
    protected $id;

    /**
     * @var integer Id фильма
     * @ORM\Column(type="integer", options={"unsigned": true})
     */
    protected $movieId;

    /**
     * @var City Город
     * @ORM\JoinColumn(nullable=false)
     * @ORM\ManyToOne(targetEntity="Illuzion\ApiBundle\Entity\Shared\City")
     */
    protected $city;

    /**
     * @var \DateTime Дата старта продаж
     * @ORM\Column(type="datetime")
     */
    protected $datetime;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = (int) $id;
        return $this;
    }

    /**
     * @return int Id фильма
     */
    public function getMovieId()
    {
        return $this->movieId;
    }

    /**
     * @param int $movieId Id фильма
     *
     * @return $this
     */
    public function setMovieId($movieId)
    {
        $this->movieId = (int) $movieId;
        return $this;
    }

    /**
     * @return City Город
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param City $city Город
     *
     * @return $this
     */
    public function setCity(City $city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return \DateTime Дата старта продаж
     */
    public function getDatetime()
    {
        return $this->datetime;
    }

    /**
     * @param \DateTime $datetime Дата старта продаж
     *
     * @return $this
     */
    public function setDatetime(\DateTime $datetime)
    {
        $this->datetime = $datetime;
        return $this;
    }
}

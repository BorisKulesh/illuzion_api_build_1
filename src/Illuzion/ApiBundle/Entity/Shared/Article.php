<?php

namespace Illuzion\ApiBundle\Entity\Shared;

use Doctrine\ORM\Mapping as ORM;

/**
 * Новость
 *
 * @ORM\Entity(repositoryClass="Illuzion\ApiBundle\Repository\ArticleRepository")
 */
class Article
{
    /**
     * @var integer
     * @ORM\Id()
     * @ORM\Column(type="integer", options={"unsigned": true})
     * @ORM\GeneratedValue()
     */
    protected $id;

    /**
     * @var string Название
     * @ORM\Column(type="string")
     */
    protected $title;

    /**
     * @var string Алиас
     * @ORM\Column(type="string", unique=true)
     */
    protected $alias;

    /**
     * @var \DateTime Дата публикации
     * @ORM\Column(type="datetime")
     */
    protected $datetime;

    /**
     * @var string Краткое описание
     * @ORM\Column(type="text", nullable=true)
     */
    protected $textShort;

    /**
     * @var string Текст новости
     * @ORM\Column(type="text", nullable=true)
     */
    protected $textFull;

    /**
     * @var string Url обложки
     * @ORM\Column(type="string", nullable=true)
     */
    protected $coverUrl;

    /**
     * @var boolean Опубликован
     * @ORM\Column(type="boolean")
     */
    protected $published;

    /**
     * @var integer Кол-во просмотров
     * @ORM\Column(type="integer", options={"unsigned": true})
     */
    protected $views = 0;

    /**
     * @var City Город
     * @ORM\JoinColumn(nullable=false)
     * @ORM\ManyToOne(targetEntity="Illuzion\ApiBundle\Entity\Shared\City", inversedBy="cinemas")
     */
    protected $city;

    /**
     * @var \DateTime Дата последнего обновления
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $updatedAt;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param integer $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = (int) $id;
        return $this;
    }

    /**
     * @return string Название
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title Название
     *
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = (string) $title;
        return $this;
    }

    /**
     * @return string Алиас
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * @param string $alias Алиас
     *
     * @return $this
     */
    public function setAlias($alias)
    {
        $this->alias = (string) $alias;
        return $this;
    }

    /**
     * @return \DateTime Дата публикации
     */
    public function getDatetime()
    {
        return $this->datetime;
    }

    /**
     * @param \DateTime $datetime Дата публикации
     *
     * @return $this
     */
    public function setDatetime(\DateTime $datetime)
    {
        $this->datetime = $datetime;
        return $this;
    }

    /**
     * @return string Краткое описание
     */
    public function getTextShort()
    {
        return $this->textShort;
    }

    /**
     * @param string $textShort Краткое описание
     *
     * @return $this
     */
    public function setTextShort($textShort)
    {
        $this->textShort = (string) $textShort;
        return $this;
    }

    /**
     * @return string Текст новости
     */
    public function getTextFull()
    {
        return $this->textFull;
    }

    /**
     * @param string $textFull Текст новости
     *
     * @return $this
     */
    public function setTextFull($textFull)
    {
        $this->textFull = (string) $textFull;
        return $this;
    }

    /**
     * @return string Url обложки
     */
    public function getCoverUrl()
    {
        return $this->coverUrl;
    }

    /**
     * @param string $coverUrl Url обложки
     *
     * @return $this
     */
    public function setCoverUrl($coverUrl)
    {
        $this->coverUrl = (string) $coverUrl;
        return $this;
    }

    /**
     * @return boolean Опубликован
     */
    public function isPublished()
    {
        return $this->published;
    }

    /**
     * @param boolean $published Опубликован
     *
     * @return $this
     */
    public function setPublished($published)
    {
        $this->published = (boolean) $published;
        return $this;
    }

    /**
     * @return int Кол-во просмотров
     */
    public function getViews()
    {
        return $this->views;
    }

    /**
     * @param int $views Кол-во просмотров
     *
     * @return $this
     */
    public function setViews($views)
    {
        $this->views = (int) $views;
        return $this;
    }

    /**
     * @return City Город
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param City $city Город
     *
     * @return $this
     */
    public function setCity(City $city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return \DateTime Дата последнего обновления
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt Дата последнего обновления
     */
    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }
}

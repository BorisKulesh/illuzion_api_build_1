<?php

namespace Illuzion\ApiBundle\Entity\Generator;

use Doctrine\ORM\Id\IdentityGenerator;

/**
 * Генератор Id для фильмов
 */
class MovieIdGenerator extends IdentityGenerator
{
    public function __construct()
    {
        parent::__construct('GEN_KEY_FILM');
    }
}

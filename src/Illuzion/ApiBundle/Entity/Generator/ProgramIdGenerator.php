<?php

namespace Illuzion\ApiBundle\Entity\Generator;

use Doctrine\ORM\Id\IdentityGenerator;

/**
 * Генератор Id для программ
 */
class ProgramIdGenerator extends IdentityGenerator
{
    public function __construct()
    {
        parent::__construct('GEN_PROGRAMS_ID');
    }
}

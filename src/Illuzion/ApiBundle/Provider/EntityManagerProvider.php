<?php

namespace Illuzion\ApiBundle\Provider;

use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Провайдер EntityManager для необходимых баз
 */
class EntityManagerProvider
{
    const DEFAULT_EM = 'uss';

    /** @var ManagerRegistry */
    protected $managerRegistry;

    /** @var string[] */
    protected $loadedManagers = [];

    /** @var string[] Список доступных кинотеатров */
    protected $supportedCinemas;

    /**
     * @param ManagerRegistry $managerRegistry
     * @param string[] $supportedCinemas Список доступных кинотеатров
     */
    public function __construct(ManagerRegistry $managerRegistry, array $supportedCinemas)
    {
        $this->managerRegistry = $managerRegistry;
        $this->supportedCinemas = $supportedCinemas;
    }

    /**
     * Mysql база
     *
     * @return EntityManager
     */
    public function getSharedEm()
    {
        return $this->getManager('shared');
    }

    /**
     * Основная база уссури
     *
     * @return EntityManager
     */
    public function getMainEm()
    {
        return $this->getMainEmForCinema(self::DEFAULT_EM);
    }

    /**
     * Основная база для кинотеатра
     *
     * @param string $cinemaId Id кинотеатра
     * @return EntityManager
     */
    public function getMainEmForCinema($cinemaId)
    {
        $em = $this->getManager($cinemaId.'_main');
        $em->CINEMA_ID = $cinemaId;
        return $em;
    }

    /**
     * База лояльности уссури
     *
     * @return EntityManager
     */
    public function getCustomerEm()
    {
        return $this->getCustomerEmForCinema(self::DEFAULT_EM);
    }

    /**
     * База лояльности для кинотеатра
     *
     * @param string $cinemaId Id кинотеатра
     * @return EntityManager
     */
    public function getCustomerEmForCinema($cinemaId)
    {
        /** @var EntityManager $em */
        $em = $this->getManager($cinemaId.'_customer');
        $em->CINEMA_ID = $cinemaId;
        return $em;
    }

    /**
     * @return \string[] Список доступных кинотеатров
     */
    public function getSupportedCinemas()
    {
        return $this->supportedCinemas;
    }

    /**
     * Сбросить весь кеш в текущем запросе (стоит выполнять в демонах)
     */
    public function resetLoadedManagers()
    {
        foreach ($this->loadedManagers as $name => $val) {
            $em = $this->managerRegistry->getManager($name);
            $em->clear();

            $em = $this->managerRegistry->resetManager($name);
            if ($em instanceof EntityManagerInterface) {
                $conn = $em->getConnection();
                $conn->close();
                $conn->connect();
            }
        }
        $this->loadedManagers = [];
    }

    /**
     * @param string $name
     * @return EntityManager
     */
    protected function getManager($name)
    {
        $this->loadedManagers[$name] = 1;
        return $this->managerRegistry->getManager($name);
    }
}

<?php

namespace Illuzion\ApiBundle;

use Illuzion\ApiBundle\DependencyInjection\Compiler\SerializerContainerCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class ApiBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(new SerializerContainerCompilerPass());
    }
}

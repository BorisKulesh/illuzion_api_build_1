<?php

namespace Illuzion\ApiBundle\Model;

/**
 * Запрос на изменение пользователя
 */
class CustomerUpdateRequest
{
    /** @var string Новый пароль */
    protected $password;

    /** @var boolean Заблокировать */
    protected $blocked;

    /**
     * @return boolean Заблокировать
     */
    public function isBlocked()
    {
        return $this->blocked;
    }

    /**
     * @param boolean $blocked Заблокировать
     *
     * @return $this
     */
    public function setBlocked($blocked)
    {
        $this->blocked = (boolean) $blocked;
        return $this;
    }

    /**
     * @return string|null Новый пароль
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string|null $password Новый пароль
     *
     * @return $this
     */
    public function setPassword($password)
    {
        $this->password = null === $password ? null : (string) $password;
        return $this;
    }
}

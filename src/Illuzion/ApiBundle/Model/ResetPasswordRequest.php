<?php

namespace Illuzion\ApiBundle\Model;

/**
 * Запрос на сброс пароля
 */
class ResetPasswordRequest
{
    /** @var string */
    protected $userId;

    /** @var string */
    protected $sendTo;

    /**
     * @return string
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param string $userId
     *
     * @return $this
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return string
     */
    public function getSendTo()
    {
        return $this->sendTo;
    }

    /**
     * @param string $sendTo
     *
     * @return $this
     */
    public function setSendTo($sendTo)
    {
        $this->sendTo = $sendTo;
        return $this;
    }
}

<?php

namespace Illuzion\ApiBundle\Model;

/**
 * Запрос на обновления/оплату/отмену заказа
 */
class OrderUpdateRequest
{
    /** @var string Новый статус заказа */
    protected $status;

    /** @var string Тип оплаты */
    protected $paymentType;

    /** @var string Пользователь */
    protected $customer;

    /** @var string Почта пользователя */
    protected $email;

    /** @var string Телефон пользователя */
    protected $phone;

    /** @var string Id транзакции платежной системы */
    protected $transactionId;

    /** @var string Номер карты платежной системы */
    protected $creditCardNumber;

    /** @var string Владелец карты платежной системы */
    protected $creditCardHolder;

    /**
     * @return string Новый статус заказа
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status Новый статус заказа
     *
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string Номер карты платежной системы
     */
    public function getCreditCardNumber()
    {
        return $this->creditCardNumber;
    }

    /**
     * @param string $creditCardNumber Номер карты платежной системы
     *
     * @return $this
     */
    public function setCreditCardNumber($creditCardNumber)
    {
        $this->creditCardNumber = $creditCardNumber;
        return $this;
    }

    /**
     * @return string Владелец карты платежной системы
     */
    public function getCreditCardHolder()
    {
        return $this->creditCardHolder;
    }

    /**
     * @param string $creditCardHolder Владелец карты платежной системы
     *
     * @return $this
     */
    public function setCreditCardHolder($creditCardHolder)
    {
        $this->creditCardHolder = $creditCardHolder;
        return $this;
    }

    /**
     * @return string Пользователь
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param string $customer Пользователь
     *
     * @return $this
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;
        return $this;
    }

    /**
     * @return string Тип оплаты
     */
    public function getPaymentType()
    {
        return $this->paymentType;
    }

    /**
     * @param string $paymentType Тип оплаты
     *
     * @return $this
     */
    public function setPaymentType($paymentType)
    {
        $this->paymentType = $paymentType;
        return $this;
    }

    /**
     * @return string Почта пользователя
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email Почта пользователя
     *
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string Телефон пользователя
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone Телефон пользователя
     *
     * @return $this
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return string Id транзакции платежной системы
     */
    public function getTransactionId()
    {
        return $this->transactionId;
    }

    /**
     * @param string $transactionId Id транзакции платежной системы
     *
     * @return $this
     */
    public function setTransactionId($transactionId)
    {
        $this->transactionId = $transactionId;
        return $this;
    }
}

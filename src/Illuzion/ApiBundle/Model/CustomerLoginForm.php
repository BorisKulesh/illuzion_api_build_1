<?php

namespace Illuzion\ApiBundle\Model;

/**
 * Запрос пользователя на авторизацию
 */
class CustomerLoginForm
{
    /** @var string */
    protected $login;

    /** @var string */
    protected $password;

    /**
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @param string $login
     *
     * @return $this
     */
    public function setLogin($login)
    {
        $this->login = $login;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     *
     * @return $this
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }
}

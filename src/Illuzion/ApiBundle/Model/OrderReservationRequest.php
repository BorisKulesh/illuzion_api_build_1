<?php

namespace Illuzion\ApiBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Illuzion\ApiBundle\Entity\Main\Show;

/**
 * Запрос на создание заказа
 */
class OrderReservationRequest
{
    /** @var int Ожидаемая стоимость */
    protected $totalCost;

    /** @var string Почта пользователя */
    protected $email;

    /** @var string Телефон пользователя */
    protected $phone;

    /** @var string[] Список Id мест */
    protected $seats;

    /** @var Show Сеанс */
    protected $show;

    /** @var string Ценовая схема */
    protected $promo = 'default';

    /**
     * @return int Ожидаемая стоимость
     */
    public function getTotalCost()
    {
        return $this->totalCost;
    }

    /**
     * @param int $totalCost Ожидаемая стоимость
     */
    public function setTotalCost($totalCost)
    {
        $this->totalCost = (int) $totalCost;
    }

    /**
     * @return string Почта пользователя
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email Почта пользователя
     */
    public function setEmail($email)
    {
        $this->email = (string) $email;
    }

    /**
     * @return string Телефон пользователя
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone Телефон пользователя
     */
    public function setPhone($phone)
    {
        $this->phone = (string) $phone;
    }

    /**
     * @return \string[]|ArrayCollection Список Id мест
     */
    public function getSeats()
    {
        return $this->seats;
    }

    /**
     * @param \string[] $seats Список Id мест
     */
    public function setSeats($seats)
    {
        $this->seats = $seats;
    }

    /**
     * @return Show Сеанс
     */
    public function getShow()
    {
        return $this->show;
    }

    /**
     * @param Show $show Сеанс
     */
    public function setShow(Show $show)
    {
        $this->show = $show;
    }

    /**
     * @return string Ценовая схема
     */
    public function getPromo()
    {
        return $this->promo;
    }

    /**
     * @param string $promo Ценовая схема
     *
     * @return $this
     */
    public function setPromo($promo)
    {
        $this->promo = $promo;
        return $this;
    }
}

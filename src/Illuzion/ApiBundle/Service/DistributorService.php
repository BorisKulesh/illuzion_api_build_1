<?php

namespace Illuzion\ApiBundle\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Illuzion\ApiBundle\Entity\Main\Distributor;
use Illuzion\ApiBundle\Provider\EntityManagerProvider;
use Illuzion\ApiBundle\Repository\DistributorRepository;

/**
 * Сервис для работы с прокатчиками
 */
class DistributorService
{
    /** @var DistributorRepository */
    protected $distributorRepository;

    /**
     * @param EntityManagerProvider $entityManagerProvider
     */
    public function __construct(EntityManagerProvider $entityManagerProvider)
    {
        $this->distributorRepository = $entityManagerProvider
            ->getMainEm()
            ->getRepository('Main:Distributor');
    }

    /**
     * Получить всех прокатчиков
     *
     * @return ArrayCollection|Distributor[]
     */
    public function getAll()
    {
        return new ArrayCollection(
            $this->distributorRepository->findAll()
        );
    }

    /**
     * Получит прокатчика по id
     *
     * @param string $id id прокатчика
     * @return Distributor|null
     */
    public function get($id)
    {
        return $this->distributorRepository->find($id);
    }
}

<?php

namespace Illuzion\ApiBundle\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Illuzion\ApiBundle\Entity\Shared\City;
use Illuzion\ApiBundle\Provider\EntityManagerProvider;
use Illuzion\ApiBundle\Repository\CityRepository;

/**
 * Сервис для работы с городами
 */
class CityService
{
    /** @var CityRepository */
    protected $cityRepository;

    /** @var City[] Кеш городов */
    protected $cache = [];

    /**
     * @param EntityManagerProvider $entityManagerProvider
     */
    public function __construct(EntityManagerProvider $entityManagerProvider)
    {
        $this->cityRepository = $entityManagerProvider
            ->getSharedEm()
            ->getRepository('Shared:City');
    }

    /**
     * Получить список всех городов
     *
     * @return ArrayCollection|City[]
     */
    public function getAll()
    {
        return new ArrayCollection(
            $this->cityRepository->findAll()
        );
    }

    /**
     * Получить город по id
     *
     * @param string $id
     * @return City|null
     */
    public function get($id)
    {
        if (!array_key_exists($id, $this->cache)) {
            $this->cache[$id] = $this->cityRepository->find($id);
        }

        return $this->cache[$id];
    }
}

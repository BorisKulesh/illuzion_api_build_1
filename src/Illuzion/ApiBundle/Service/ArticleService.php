<?php

namespace Illuzion\ApiBundle\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Illuzion\ApiBundle\Entity\Shared\Article;
use Illuzion\ApiBundle\Filter\ArticleFilter;
use Illuzion\ApiBundle\Provider\EntityManagerProvider;
use Illuzion\ApiBundle\Repository\ArticleRepository;

/**
 * Сервис для работы с новостями
 */
class ArticleService
{
    /** @var ArticleRepository */
    protected $articleRepository;

    /**
     * @param EntityManagerProvider $entityManagerProvider
     */
    public function __construct(EntityManagerProvider $entityManagerProvider)
    {
        $this->articleRepository = $entityManagerProvider
            ->getSharedEm()
            ->getRepository('Shared:Article');
    }

    /**
     * Получения списка новостей по фильтру
     *
     * @param ArticleFilter $filter
     * @return ArrayCollection|Article[]
     */
    public function getAll(ArticleFilter $filter)
    {
        return new ArrayCollection(
            $this->articleRepository->findAllByFilter($filter)
        );
    }

    /**
     * Получения кол-ва новостей по фильтру
     * (игнорирует пагинацию)
     *
     * @param ArticleFilter $filter
     * @return int
     */
    public function count(ArticleFilter $filter)
    {
        return $this->articleRepository->countByFilter($filter);
    }

    /**
     * Получение новоси по Id или алиасу
     *
     * @param string $id
     * @return Article|null
     */
    public function get($id)
    {
        if (is_numeric($id)) {
            return $this->articleRepository->find($id);
        } else {
            return $this->articleRepository->findOneBy(['alias' => $id]);
        }
    }

    /**
     * Сохранить новость
     * @param Article $article
     */
    public function save(Article $article)
    {
        $article->setUpdatedAt(new \DateTime());
        $this->articleRepository->save($article);
    }

    /**
     * Удалить новость
     * @param Article $article
     */
    public function delete(Article $article)
    {
        $this->articleRepository->delete($article);
    }
}

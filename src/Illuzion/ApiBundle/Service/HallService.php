<?php

namespace Illuzion\ApiBundle\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Illuzion\ApiBundle\Entity\Main\Hall;
use Illuzion\ApiBundle\Filter\HallFilter;
use Illuzion\ApiBundle\Provider\EntityManagerProvider;

/**
 * Сервис для работы с залами
 */
class HallService extends LocalCinemaService
{
    /** @var EntityManagerProvider */
    protected $emProvider;

    /**
     * @param EntityManagerProvider $entityManagerProvider
     */
    public function __construct(EntityManagerProvider $entityManagerProvider)
    {
        $this->emProvider = $entityManagerProvider;
    }

    /**
     * Получить залы по фильтру
     *
     * @param HallFilter $filter фильтр
     * @return ArrayCollection|Hall[]
     */
    public function getAll(HallFilter $filter)
    {
        if (null !== ($cinemaId = $filter->getCinema())) {
            // Если запрашивают конкретный кинотеатр, то тянем из его базы
            $halls = $this->getAllForCinema($cinemaId)->toArray();
        } else {
            // В противном случае тянем из всех
            $halls = [];
            foreach ($this->emProvider->getSupportedCinemas() as $cinemaId) {
                $halls = array_merge($halls, $this->getAllForCinema($cinemaId)->toArray());
            }
        }

        return new ArrayCollection($halls);
    }

    /**
     * Получение всех залов для кинотеатра
     *
     * @param string $cinemaId Id кинотеатра
     * @return ArrayCollection|Hall[]
     */
    public function getAllForCinema($cinemaId)
    {
        if (!in_array($cinemaId, $this->emProvider->getSupportedCinemas())) {
            return new ArrayCollection();
        }

        $repository = $this->emProvider->getMainEmForCinema($cinemaId)->getRepository('Main:Hall');
        $halls = $repository->findAll();

        return new ArrayCollection($halls);
    }

    /**
     * Получение зала по Id
     *
     * @param string $id
     * @return Hall|null
     */
    public function get($id)
    {
        // Определяем id кинотеатра и зала в нем
        list($cinemaId, $hallId) = $this->getIdAndCinema($id);
        if (!$cinemaId || !$hallId || !in_array($cinemaId, $this->emProvider->getSupportedCinemas())) {
            return null;
        }

        $repository = $this->emProvider->getMainEmForCinema($cinemaId)->getRepository('Main:Hall');
        $hall = $repository->find($hallId);

        return $hall;
    }
}

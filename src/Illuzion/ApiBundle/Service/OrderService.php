<?php

namespace Illuzion\ApiBundle\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Illuzion\ApiBundle\Entity\Main\Order;
use Illuzion\ApiBundle\Entity\Main\Show;
use Illuzion\ApiBundle\Filter\OrderFilter;
use Illuzion\ApiBundle\Filter\SeatFilter;
use Illuzion\ApiBundle\Model\OrderReservationRequest;
use Illuzion\ApiBundle\Model\OrderUpdateRequest;
use Illuzion\ApiBundle\Provider\EntityManagerProvider;
use Illuzion\ApiBundle\Repository\CasheRepository;
use Illuzion\ApiBundle\Repository\OrderRepository;
use Illuzion\ApiBundle\Security\ApiClient;
use Illuzion\Common\Doctrine\SeatType;
use Neomerx\JsonApi\Document\Error;
use Neomerx\JsonApi\Exceptions\JsonApiException;
use Psr\Log\LoggerInterface;

/**
 * Сервис для работы с заказами
 */
class OrderService extends LocalCinemaService
{
    /** @var EntityManagerProvider */
    protected $emProvider;

    /** @var PlaceTypePriceService */
    protected $placePriceTypeService;

    /** @var SeatService */
    protected $seatService;

    /** @var CustomerService */
    protected $customerService;

    /** @var NotificationService */
    protected $notificationService;

    /** @var CinemaService */
    protected $cinemaService;

    /** @var LoggerInterface */
    protected $logger;

    /**
     * @param EntityManagerProvider $entityManagerProvider
     * @param PlaceTypePriceService $placeTypePriceService
     * @param SeatService $seatService
     * @param CustomerService $customerService
     * @param NotificationService $notificationService
     * @param CinemaService $cinemaService
     * @param LoggerInterface $logger
     */
    public function __construct(
        EntityManagerProvider $entityManagerProvider,
        PlaceTypePriceService $placeTypePriceService,
        SeatService $seatService,
        CustomerService $customerService,
        NotificationService $notificationService,
        CinemaService $cinemaService,
        LoggerInterface $logger
    ) {
        $this->emProvider = $entityManagerProvider;
        $this->placePriceTypeService = $placeTypePriceService;
        $this->seatService = $seatService;
        $this->customerService = $customerService;
        $this->notificationService = $notificationService;
        $this->cinemaService = $cinemaService;
        $this->logger = $logger;
    }

    /**
     * Получение списка заказов по фильтру
     *
     * @param OrderFilter $filter фильтр
     * @return ArrayCollection|Order[]
     */
    public function getAll(OrderFilter $filter)
    {
        if (null !== ($cinemaId = $filter->getCinema())) {
            // Если указан конкретный кинотеатр, то поучаем заказы оттуда
            $orders = $this->getAllForCinema($cinemaId, $filter)->toArray();
        } else {
            // В противном случае проходимся по всем
            $orders = [];
            foreach ($this->emProvider->getSupportedCinemas() as $cinemaId) {
                $orders = array_merge($orders, $this->getAllForCinema($cinemaId, $filter)->toArray());
            }
        }

        $result = new ArrayCollection($orders);
        return $result;
    }

    /**
     * Получение кол-ва заказов по фильтру
     * Работает только для конкретного кинотеатра
     *
     * @param OrderFilter $filter фильтр
     * @return integer Кол-во заказов
     */
    public function count(OrderFilter $filter)
    {
        if (null !== ($cinemaId = $filter->getCinema())) {
            return $this->countForCinema($cinemaId, $filter);
        }

        return null;
    }

    /**
     * Получение списка заказов по фильтру для конкретного кинотеатра
     *
     * @param string $cinemaId Id кинотеатра
     * @param OrderFilter $filter фильтр
     * @return ArrayCollection|Show[]
     */
    public function getAllForCinema($cinemaId, OrderFilter $filter)
    {
        if (!in_array($cinemaId, $this->emProvider->getSupportedCinemas())) {
            return new ArrayCollection();
        }

        /** @var OrderRepository $repository */
        $repository = $this->emProvider->getMainEmForCinema($cinemaId)->getRepository('Main:Order');
        $orders = $repository->findAllByFilter($filter);

        return new ArrayCollection($orders);
    }

    /**
     * Получение кол-ва заказов по фильтру для конкретного кинотеатра
     *
     * @param string $cinemaId
     * @param OrderFilter $filter
     * @return integer
     */
    public function countForCinema($cinemaId, OrderFilter $filter)
    {
        if (!in_array($cinemaId, $this->emProvider->getSupportedCinemas())) {
            return 0;
        }

        /** @var OrderRepository $repository */
        $repository = $this->emProvider->getMainEmForCinema($cinemaId)->getRepository('Main:Order');
        $count = $repository->countByFilter($filter);

        return $count;
    }

    /**
     * Получение заказа по Id
     *
     * @param string $id Id заказа
     * @return Order|null
     */
    public function get($id)
    {
        // Определяем id кинотеатра и заказа в нем
        list($cinemaId, $orderId) = $this->getIdAndCinema($id);
        if (!$cinemaId || !$orderId || !in_array($cinemaId, $this->emProvider->getSupportedCinemas())) {
            return null;
        }

        $repository = $this->emProvider->getMainEmForCinema($cinemaId)->getRepository('Main:Order');
        $order = $repository->find($orderId);

        return $order;
    }

    /**
     * Создание/бронирование заказа
     *
     * @param OrderReservationRequest $request Запрос на создание
     * @param ApiClient $client Клиент АПИ
     *
     * @return Order
     * @throws JsonApiException
     * @throws \Exception
     */
    public function createOrder(OrderReservationRequest $request, ApiClient $client)
    {
        // Нельзя купить за час до сеанса
        if ($request->getShow()->getActualDatetime() < new \DateTime($client->getSalesCloseTime())) {
            throw new JsonApiException(
                $this->createError('seance_passed', 'Сеанс уже прошел')
            );
        }

        // Проверка, что покупаемые места свободны
        $availableSeatsIds = $this->seatService->getAvailableSeatsIds($request->getShow());
        $busySeats = array_diff($request->getSeats()->toArray(), $availableSeatsIds);
        if (count($busySeats) > 0) {
            throw new JsonApiException(
                $this->createError('seats_busy', 'Некоторые места уже заняты')
            );
        }

        // Проверяем, что запрашиваемая ценовая схема доступна
        $priceSchemes = $this->placePriceTypeService->getPriceSchemes($request->getShow(), $client);
        if (!array_key_exists($request->getPromo(), $priceSchemes)) {
            throw new JsonApiException(
                $this->createError('promo_not_found', 'Ценовая схема не найдена')
            );
        }
        $scheme = $priceSchemes[$request->getPromo()];

        // Получаем список мест для подсчета цены
        $filter = new SeatFilter([
            'show' => $request->getShow()->getPublicId(),
            'seats' => $request->getSeats()->toArray(),
        ]);
        $seats = $this->seatService->getAll($filter);

        // Суммируем и проверяем стоимость заказа
        $expectedCost = 0;
        foreach ($seats as $seat) {
            $expectedCost += $scheme[$seat->getType()];
        }
        if ($expectedCost != $request->getTotalCost()) {
            throw new JsonApiException(
                $this->createError('invalid_total_cost', 'Неверная цена покупки')
            );
        }

        $repository = $this->emProvider
            ->getMainEmForCinema($request->getShow()->getCinemaId())
            ->getRepository('Main:Order');

        // Бронируем места по 1 штуке
        // Процедура бронирование также создаст заказ в базе
        $orderId = null;
        foreach ($request->getSeats() as $seat) {
            $orderId = $repository->blockSeat(
                $request->getShow()->getId(),
                $seat,
                $request->getPhone(),
                $request->getEmail(),
                $orderId
            );
        }

        // Генерируем код покупки
        $code = $this->generateCode($repository, $request->getShow()->getDatetime());

        // Обновляем дополнительную информацию о заказе
        $order = $repository->find($orderId);
        $order->setTotalCost($request->getTotalCost());
        $order->setClient($client->getUsername());
        $order->setPromo($request->getPromo());
        $order->setPrintCode($code);
        $repository->save($order);

        return $order;
    }

    public function updateOrder(Order $order, OrderUpdateRequest $orderUpdateRequest)
    {
        $repository = $this->emProvider
            ->getMainEmForCinema($order->getCinemaId())
            ->getRepository('Main:Order');

        // Обновляем дополнительную информацию о заказе
        $order
            ->setEmail($orderUpdateRequest->getEmail())
            ->setPhone($orderUpdateRequest->getPhone());
        $repository->save($order);
    }

    /**
     * Выкуп/оплата заказа
     *
     * @param Order $order Заказ
     * @param OrderUpdateRequest $orderUpdateRequest Запрос на выкуп
     * @param ApiClient $client
     *
     * @throws \Exception
     */
    public function purchaseOrder(Order $order, OrderUpdateRequest $orderUpdateRequest, ApiClient $client)
    {
        $repository = $this->emProvider
            ->getMainEmForCinema($order->getCinemaId())
            ->getRepository('Main:Order');

        $customerRepository = $this->emProvider
            ->getCustomerEmForCinema($order->getCinemaId())
            ->getRepository('Customer:Customer');

        // Получение и проверка существования пользователя из базы
        $customer = null;
        if (!empty($orderUpdateRequest->getCustomer())) {
            $customer = $this->customerService->get($orderUpdateRequest->getCustomer());
            if (null === $customer) {
                throw new JsonApiException(
                    $this->createError('customer_not_found', 'Пользователь не найден')
                );
            }
        }

        $hallId = $order->getShow()->getHall()->getPublicId();
        if ($orderUpdateRequest->getPaymentType() === PlaceTypePriceService::TYPE_BONUS) {
            // Покупка за бонусы

            // Проверяем доступность покупки для клиента
            if (!$client->isPurchaseByBonusAllowed($hallId)) {
                throw new JsonApiException(
                    $this->createError('purchase_by_bonus_disallowed', 'Покупка за бонусы отключена')
                );
            }

            // Пользователь обязателен в этой покупке
            if (null === $customer) {
                throw new JsonApiException(
                    $this->createError('customer_not_found', 'Для покупки за бонусы необходимо указать пользователя')
                );
            }

            // Проверка, что у пользователя достаточно бонусов
            $bonus = $customer->getMainCard()->getBonus();
            if ($bonus < $order->getTotalCost()) {
                throw new JsonApiException(
                    $this->createError('not_enough_bonus', 'Недостаточно бонусов для покупки')
                );
            }

            // Выкуп/оплата заказа бонусами
            $this->enshureCacheClosed($order->getCinemaId());
            $this->logger->notice("Sale bonus order: ".$order->getId().", card: ".$customer->getMainCard()->getNumber());
            $repository->internetSaleBonus($order->getId(), $customer->getMainCard()->getNumber());
            $repository->refresh($order);

            // Снятие бонусов с пользователя
            $customerRepository->changeBonus(
                $customer->getMainCard()->getNumber(),
                -$order->getTotalCost(),
                $order->getShow()->getDatetime()
            );
        } else {
            // Покупка за деньги

            $this->enshureCacheClosed($order->getCinemaId());
            if (null !== $customer) {
                // Если пользователь был указан, то выкупаем с начислением бонусов

                // Выкуп/оплата заказа
                $repository->internetCardSale($order->getId(), $customer->getMainCard()->getNumber());
                $repository->refresh($order);

                // Получение ценовой схемы для расчета бонусов
                $priceSchemes = $this->placePriceTypeService->getPriceSchemes($order->getShow(), $client);
                $scheme = array_key_exists($order->getPromo(), $priceSchemes)
                    ? $priceSchemes[$order->getPromo()]
                    : $priceSchemes[PlaceTypePriceService::TYPE_DEFAULT];

                // Проверяем возможность накопления бонусов при покупке через клиента
                if ($client->isBonusAccumulationAllowed($hallId)) {
                    // За каждый билет начисляем бонусы отдельно. Так сказал делать Кульков
                    foreach ($order->getTickets() as $ticket) {
                        $customerRepository->updateCardBonus(
                            $customer->getMainCard()->getNumber(),
                            $scheme[$ticket->getSeat()->getType()],
                            1,
                            $order->getShow()->getDatetime(),
                            SeatType::toInteger($ticket->getSeat()->getType())
                        );
                    }
                }
            } else {
                // Если пользователь не указан, то просто выкупаем заказ
                $repository->internetSale($order->getId());
                $repository->refresh($order);
            }
        }

        // Обновляем дополнительную информацию о заказе
        $order
            ->setPaymentType($orderUpdateRequest->getPaymentType())
            ->setCreditCardHolder($orderUpdateRequest->getCreditCardHolder())
            ->setCreditCardNumber($orderUpdateRequest->getCreditCardNumber())
            ->setTransactionId($orderUpdateRequest->getTransactionId())
            ->setEmail($orderUpdateRequest->getEmail())
            ->setPhone($orderUpdateRequest->getPhone())
            ->setCustomerId($customer ? $customer->getId() : null);
        $repository->save($order);

        // Посылаем уведомления о успешной покупке
        $this->sendOrderNotifications($order);
    }

    /**
     * Разбронирование заказа
     *
     * @param Order $order Заказ
     */
    public function unreserveOrder(Order $order)
    {
        $repository = $this->emProvider
            ->getMainEmForCinema($order->getCinemaId())
            ->getRepository('Main:Order');
        $repository->freeTempReserv($order->getId());
        $repository->refresh($order);
    }

    /**
     * Отмена заказа
     *
     * @param Order $order Заказ
     * @param ApiClient $client
     *
     * @throws \Exception
     */
    public function rollbackOrder(Order $order, ApiClient $client)
    {
        $repository = $this->emProvider
            ->getMainEmForCinema($order->getCinemaId())
            ->getRepository('Main:Order');

        $customerRepository = $this->emProvider
            ->getCustomerEmForCinema($order->getCinemaId())
            ->getRepository('Customer:Customer');

        // Получение пользователя, если он был
        $customer = null;
        if (!empty($order->getCustomerId())) {
            $customer = $this->customerService->get($order->getCustomerId());
        }

        $hallId = $order->getShow()->getHall()->getPublicId();
        if ($order->getPaymentType() === PlaceTypePriceService::TYPE_BONUS) {
            // В случае покупке за бонусы необходимо

            // Крайне странная ситуация, но возможна со старыми заказами
            if (null === $customer) {
                throw new JsonApiException(
                    $this->createError('unexpected_error', 'Покупка за бонусы но без пользователя')
                );
            }

            // Вызов процедуры отмены заказа
            try {
                $repository->moneybackInternet($order->getPublicId());
            } catch (\Exception $exception) {
                $this->logger->error('Cannot return purchase', [
                    'exception' => $exception,
                    'id' => $order->getPublicId(),
                ]);
                throw new JsonApiException(
                    $this->createError('cannot_return', $exception->getMessage())
                );
            }

            // Возврат бонусов пользователю
            $customerRepository->changeBonus(
                $customer->getMainCard()->getNumber(),
                $order->getTotalCost(),
                $order->getShow()->getDatetime()
            );

            // Обновление статуса заказа
            $repository->refresh($order);
            $order->setStatus(6);
            $repository->save($order, false);
        } else {
            // Покупка за деньги

            // Вызов процедуры отмены заказа
            try {
                $repository->moneybackInternet($order->getPublicId());
            } catch (\Exception $exception) {
                $this->logger->error('Cannot return purchase', [
                    'exception' => $exception,
                    'id' => $order->getPublicId(),
                ]);
                throw new JsonApiException(
                    $this->createError('cannot_return', $exception->getMessage())
                );
            }

            if (null !== $customer) {
                // Если был пользователь, то необходимо отменить начисление бонусов

                // Получение ценовой схемы для расчета бонусов
                $priceSchemes = $this->placePriceTypeService->getPriceSchemes($order->getShow(), $client);
                $scheme = array_key_exists($order->getPromo(), $priceSchemes)
                    ? $priceSchemes[$order->getPromo()]
                    : $priceSchemes[PlaceTypePriceService::TYPE_DEFAULT];

                // Проверяем возможность накопления бонусов при покупке через клиента
                if ($client->isBonusAccumulationAllowed($hallId)) {
                    // "Отмена начисления" - это начисление отрицательных бонусов. Опять по одному, по наставлению Кулькова
                    foreach ($order->getTickets() as $ticket) {
                        $customerRepository->updateCardBonus(
                            $customer->getMainCard()->getNumber(),
                            -$scheme[$ticket->getSeat()->getType()],
                            1,
                            $order->getShow()->getDatetime(),
                            SeatType::toInteger($ticket->getSeat()->getType())
                        );
                    }
                }
            }
            $repository->refresh($order);
        }
    }

    /**
     * Отсылка уведомлений о успешной покупке
     *
     * @param Order $order
     */
    protected function sendOrderNotifications(Order $order)
    {
        // Собираем и отсылаем письмо, если в заказе указана почта
        if ($order->getEmail()) {
            $seats = [];
            foreach ($order->getTickets() as $ticket) {
                $seats[] = [
                    'row' => $ticket->getSeat()->getRow(),
                    'number' => $ticket->getSeat()->getPlace(),
                ];
            }
            $cinema = $this->cinemaService->get($order->getCinemaId());
            $customerName = $order->getCustomerId() === null
                ? null
                : $this->customerService->get($order->getCustomerId())->getName();

            $this->notificationService->sendEmailOrderConfirmation(
                $order->getEmail(),
                $customerName,
                $order->getPrintCode(),
                $order->getId(),
                $order->getShow()->getMovie()->getTitle(),
                $cinema->getTitle(),
                $order->getShow()->getHall()->getTitle(),
                $seats
            );
        }

        // Отсылаем смс, если в заказе указан телефон
        if ($order->getPhone()) {
            $this->notificationService->sendSmsOrderConfirmation(
                $order->getPhone(),
                $order->getPrintCode()
            );
        }
    }

    /**
     * Генерация кода покупки
     *
     * @param OrderRepository $repository
     * @param \DateTime $datetime
     * @return string
     */
    protected function generateCode(OrderRepository $repository, \DateTime $datetime)
    {
        // Перебираем и проверяем коды покупки
        while (true) {
            $code = (string) mt_rand(10000000, 99999999);
            if ($repository->isPrintCodeValid($code, $datetime)) {
                return $code;
            }
        }
    }

    /**
     * Нечто, что нужно закрывать, чтобы работала покупка
     * Логика сделана со слов разработчика базы
     *
     * @param string $cinemaId
     */
    protected function enshureCacheClosed($cinemaId)
    {
        /** @var CasheRepository $repository */
        $repository = $this->emProvider
            ->getMainEmForCinema($cinemaId)
            ->getRepository('Main:Cashe');

        $repository->ensureCasheClosed();
    }

    /**
     * Вспомогательный метод для генерации ошибок в jsonapi формате
     *
     * @param string $code Код ошибки
     * @param string $details Описание ошибки
     *
     * @return Error
     */
    protected function createError($code, $details)
    {
        return new Error(null, null, 400, $code, null, $details);
    }
}

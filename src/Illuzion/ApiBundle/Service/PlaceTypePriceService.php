<?php

namespace Illuzion\ApiBundle\Service;

use Illuzion\ApiBundle\Entity\Main\Show;
use Illuzion\ApiBundle\Provider\EntityManagerProvider;
use Illuzion\ApiBundle\Security\ApiClient;

/**
 * Метод для работы с ценовыми схемами
 */
class PlaceTypePriceService
{
    // Id стандартной схемы
    const TYPE_DEFAULT = 'default';
    // Id схемы со скидкой по карте (например последние сеансы)
    const TYPE_BONUS = 'bonus';

    /** @var EntityManagerProvider */
    protected $entityManagerProvider;

    /**
     * @param EntityManagerProvider $entityManagerProvider
     */
    public function __construct(EntityManagerProvider $entityManagerProvider)
    {
        $this->entityManagerProvider = $entityManagerProvider;
    }

    /**
     * Получение доступных ценовых схем для сеанса
     *
     * @param Show $show
     * @param ApiClient|null $client
     *
     * @return array [scheme_name => [price_type => price]]
     */
    public function getPriceSchemes(Show $show, ApiClient $client = null)
    {
        $hallId = $show->getHall()->getPublicId();

        // Получение стандартной ценовой схемы для сеанса
        $defaultPriceScheme = $this->getPriceScheme($show);

        // Вычисляем скидку, если разрешена для клиента
        if (!$client || $client->isDiscountsAllowed($hallId)) {
            // Получение скидки по карте
            $bonusDiscount = $this->calculateBonusDiscount($show);
        } else {
            $bonusDiscount = 0;
        }

        $schemes[self::TYPE_DEFAULT] = $defaultPriceScheme;

        // Если есть скидка по карте, то заполняем дополнительную схему
        if ($bonusDiscount !== 0) {
            $bonusPriceScheme = [];
            foreach ($defaultPriceScheme as $key => $price) {
                // Расчет стоимости со скидкой из письма:
                // А можно еще повторить механизм округления, скидок.
                // Оказалось очень важный вопрос.
                // Код из паскаля:
                // price=F_TRUNCATE((price+9)/10)*10.
                $bonusPriceScheme[$key] = floor(($price * (1 - $bonusDiscount) + 9) / 10) * 10;
            }
            $schemes[self::TYPE_BONUS] = $bonusPriceScheme;
        }

        if ($client) {
            // Корректируем цены для клиента
            foreach ($schemes as &$scheme) {
                foreach ($scheme as $priceType => &$price) {
                    $adjustment = $client->getPriceAdjustment($hallId, $priceType);
                    if ($adjustment === 0) {
                        continue;
                    }

                    if (abs($adjustment) < 1) {
                        $price = round($price * (1 + $adjustment));
                    } else {
                        $price = $price + $adjustment;
                    }
                    if ($price < 0) {
                        $price = 0;
                    }
                }
            }
        }

        return $schemes;
    }

    /**
     * Получение стандартной ценовой схемы для сеанса
     *
     * @param Show $show
     * @return \int[]
     */
    protected function getPriceScheme(Show $show)
    {
        $repository = $this->entityManagerProvider
            ->getMainEmForCinema($show->getCinemaId())
            ->getRepository('Main:PlaceTypePrice');

        return $repository->getPriceMap($show->getSchemeId());
    }

    /**
     * Расчет скидки по карте
     *
     * @param Show $show
     * @return float
     */
    protected function calculateBonusDiscount(Show $show)
    {
        // Не отдавать скидки, если нельзя
        // Из письма: Скидки не должны предоставляться если bonus_allowed_fasle.
        //            bonus_allowed общая галочка и для скидок и для бонусов.
        if (!$show->isBonusBuyAllowed()) {
            return 0;
        }

        // Скидка может быть только на сеансы с 23:00 - 8:00
        $hour = $show->getDatetime()->format('H');
        if ($hour >= 8 && $hour < 23) {
            return 0;
        }

        $hall = $show->getHall();
        if (in_array($hall->getPublicId(), ['oc_1', 'uss_20', 'nmega_6', 'nmega_7'])) {
            // Особые правила для Океан IMAX, Уссури VIP, Находка Мега залы VIP 1 и VIP 2

            $showDayOfWeek = (int) $show->getDatetime()->format('w');
            if ($showDayOfWeek >= 1 && $showDayOfWeek <= 4) {
                // скидка 20% c пн по чт
                return .2;
            } else {
                // нет скидки в остальные дни
                return 0;
            }
        } else {
            // остальные залы 50%
            return .5;
        }
    }
}

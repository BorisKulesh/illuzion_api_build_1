<?php

namespace Illuzion\ApiBundle\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Illuzion\ApiBundle\Entity\Main\Movie;
use Illuzion\ApiBundle\Filter\MovieFilter;
use Illuzion\ApiBundle\Provider\EntityManagerProvider;
use Illuzion\ApiBundle\Repository\MovieRepository;

/**
 * Сервис для работы с фильмами
 */
class MovieService
{
    /** @var EntityManagerProvider */
    protected $entityManagerProvider;

    /** @var MovieRepository */
    protected $movieRepository;

    /** @var ReleaseDateService */
    protected $releaseDateService;

    /**
     * @param EntityManagerProvider $entityManagerProvider
     * @param ReleaseDateService $releaseDateService
     */
    public function __construct(EntityManagerProvider $entityManagerProvider, ReleaseDateService $releaseDateService)
    {
        $this->entityManagerProvider = $entityManagerProvider;
        $this->movieRepository = $entityManagerProvider
            ->getMainEm()
            ->getRepository('Main:Movie');
        $this->releaseDateService = $releaseDateService;
    }

    /**
     * Получение всех фильмов по фильтру
     *
     * @param MovieFilter $filter фильтр
     * @return ArrayCollection|Movie[]
     */
    public function getAll(MovieFilter $filter)
    {
        if (null !== ($cinema = $filter->getCinema())) {
            if (!in_array($cinema, $this->entityManagerProvider->getSupportedCinemas())) {
                return new ArrayCollection();
            }

            $repository = $this->entityManagerProvider
                ->getMainEmForCinema($filter->getCinema())
                ->getRepository('Main:Movie');
        } else {
            $repository = $this->movieRepository;
        }

        return new ArrayCollection(
            $repository->findAllByFilter($filter)
        );
    }

    /**
     * Получение кол-ва фильмов по фильтру
     *
     * @param MovieFilter $filter фильтр
     * @return int
     */
    public function count(MovieFilter $filter)
    {
        if (null !== ($cinema = $filter->getCinema())) {
            if (!in_array($cinema, $this->entityManagerProvider->getSupportedCinemas())) {
                return 0;
            }

            $repository = $this->entityManagerProvider
                ->getMainEmForCinema($filter->getCinema())
                ->getRepository('Main:Movie');
        } else {
            $repository = $this->movieRepository;
        }

        return $repository->countByFilter($filter);
    }

    /**
     * Получение фильма по id или алиасу
     *
     * @param string $id
     * @return Movie|null
     */
    public function get($id)
    {
        // В случае если передано число, то считаем что мы получили id
        if (is_numeric($id)) {
            return $this->movieRepository->findOneBy([
                'id' => $id
            ]);
        } else {
            return $this->movieRepository->findOneBy([
                'alias' => $id
            ]);
        }
    }

    /**
     * Сохранение изменений в фильме
     *
     * @param Movie $movie
     * @param ArrayCollection $releaseDates Даты старта. Обрабатываем отдельно, т.к. они пишутся в отдельную базу
     */
    public function save(Movie $movie, ArrayCollection $releaseDates = null)
    {
        if (null !== $releaseDates) {
            $cityDate = $releaseDates->get('vladivostok');
            if ($cityDate) {
                $movie->setStartDate($cityDate);
            }
        }
        $this->movieRepository->save($movie);
        if (null !== $releaseDates) {
            $this->releaseDateService->updateReleaseDates($movie->getId(), $releaseDates);
        }
    }

    /**
     * Удаление фильма
     *
     * @param Movie $movie
     */
    public function delete(Movie $movie)
    {
        try {
            $this->movieRepository->delete($movie);
        } catch (ForeignKeyConstraintViolationException $e) {
            // Отдельно обрабатываем ошибки базы связанные с внешними ключами
            // Приводим их к понятному для человека ответу
            if (strpos($e->getMessage(), 'FK_SP_R_MOVIETI_FILMS')) {
                throw new \InvalidArgumentException('Удаление невозможно! Вначале удалите связанные проекты сеансов');
            } else if (strpos($e->getMessage(), 'FK_SEANCE_R_MOVIETI_FILMS')) {
                throw new \InvalidArgumentException('Удаление невозможно! Вначале удалите связанные сеансы');
            }
        }
        $this->releaseDateService->deleteAllForMovie($movie);
    }
}

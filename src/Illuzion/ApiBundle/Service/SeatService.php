<?php

namespace Illuzion\ApiBundle\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Illuzion\ApiBundle\Entity\Main\Seat;
use Illuzion\ApiBundle\Entity\Main\Show;
use Illuzion\ApiBundle\Filter\SeatFilter;
use Illuzion\ApiBundle\Provider\EntityManagerProvider;
use Illuzion\ApiBundle\Repository\SeatRepository;
use Psr\Cache\CacheItemPoolInterface;

/**
 * Сервис для работы с местами
 */
class SeatService extends LocalCinemaService
{
    /** @var EntityManagerProvider */
    protected $emProvider;

    /** @var CacheItemPoolInterface */
    protected $cachePool;

    /** @var int|null */
    protected $seatsCacheInSecs;

    /**
     * @param EntityManagerProvider $entityManagerProvider
     * @param CacheItemPoolInterface $cacheItemPool
     * @param int|null $seatsCacheInSecs
     */
    public function __construct(EntityManagerProvider $entityManagerProvider, CacheItemPoolInterface $cacheItemPool, $seatsCacheInSecs = null)
    {
        $this->emProvider = $entityManagerProvider;
        $this->cachePool = $cacheItemPool;
        $this->seatsCacheInSecs = $seatsCacheInSecs;
    }

    /**
     * Получение списка мест по фильтру
     *
     * @param SeatFilter $filter
     * @return ArrayCollection|Seat[]
     */
    public function getAll(SeatFilter $filter)
    {
        list($cinemaId,) = $this->getIdAndCinema($filter->getShow());
        return $this->getAllForCinema($cinemaId, $filter);
    }

    /**
     * Получение списка мест по фильтру для кинотеатра
     *
     * @param string $cinemaId
     * @param SeatFilter $filter
     * @return ArrayCollection|Seat[]
     */
    protected function getAllForCinema($cinemaId, SeatFilter $filter)
    {
        if (!in_array($cinemaId, $this->emProvider->getSupportedCinemas())) {
            return new ArrayCollection();
        }

        $cacheItem = $this->cachePool->getItem($filter->getHash());
        if (!$cacheItem->isHit()) {
            /** @var SeatRepository $repository */
            $repository = $this->emProvider->getMainEmForCinema($cinemaId)->getRepository('Main:Seat');
            $seats = new ArrayCollection(
                $repository->findAllByFilter($filter)
            );
            if (!$this->seatsCacheInSecs) {
                return $seats;
            }

            $cacheItem->set($seats);
            $cacheItem->expiresAfter($this->seatsCacheInSecs);
            $this->cachePool->save($cacheItem);
        }

        return $cacheItem->get();
    }

    /**
     * Получение списка id доступных мест
     *
     * @param Show $show
     * @return \int[]
     */
    public function getAvailableSeatsIds(Show $show)
    {
        $repository = $this->emProvider
            ->getMainEmForCinema($show->getCinemaId())
            ->getRepository('Main:Seat');

        return $repository->getAvailableSeatsIds($show->getId());
    }
}

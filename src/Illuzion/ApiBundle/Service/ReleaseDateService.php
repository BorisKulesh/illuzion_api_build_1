<?php

namespace Illuzion\ApiBundle\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Illuzion\ApiBundle\Entity\Main\Movie;
use Illuzion\ApiBundle\Entity\Shared\City;
use Illuzion\ApiBundle\Entity\Shared\ReleaseDate;
use Illuzion\ApiBundle\Provider\EntityManagerProvider;
use Illuzion\ApiBundle\Repository\CityRepository;
use Illuzion\ApiBundle\Repository\ReleaseDateRepository;

/**
 * Сервис для работы с датами старта продаж
 */
class ReleaseDateService
{
    /** @var ReleaseDateRepository */
    protected $releaseDateRepository;

    /** @var CityRepository */
    protected $cityRepository;

    /**
     * @param EntityManagerProvider $entityManagerProvider
     */
    public function __construct(EntityManagerProvider $entityManagerProvider)
    {
        $this->releaseDateRepository = $entityManagerProvider
            ->getSharedEm()
            ->getRepository('Shared:ReleaseDate');

        $this->cityRepository = $entityManagerProvider
            ->getSharedEm()
            ->getRepository('Shared:City');
    }

    /**
     * Получение дат старта по городам
     *
     * @param int $movieId Id фильма
     * @return string[]
     */
    public function getCityDateMapForMovie($movieId)
    {
        /** @var ReleaseDate[] $dates */
        $dates = $this->releaseDateRepository->findBy([
            'movieId' => $movieId
        ]);

        $result = [];
        foreach ($dates as $date) {
            $result[$date->getCity()->getId()] = $date->getDatetime()->format('Y-m-d H:i:s');
        }

        return $result;
    }

    /**
     * Обновление дат старта для фильма
     *
     * @param string $movieId Id фильма
     * @param \DateTime[]|ArrayCollection $releaseDates Словарик [город => дата]
     */
    public function updateReleaseDates($movieId, ArrayCollection $releaseDates)
    {
        // Получаем список городов
        /** @var City[] $cities */
        $cities = $this->cityRepository->findAll();
        $cityMap = [];
        foreach ($cities as $city) {
            if (!$releaseDates->containsKey($city->getId())) {
                $releaseDates->set($city->getId(), null);
            }
            $cityMap[$city->getId()] = $city;
        }

        // Получаем текущие даты старта
        $currentDates = new ArrayCollection();
        foreach ($this->releaseDateRepository->findBy(['movieId' => $movieId]) as $date) {
            $currentDates->set($date->getCity()->getId(), $date);
        }

        $newDates = new ArrayCollection();
        foreach ($releaseDates as $cityId => $releaseDate) {
            /** @var ReleaseDate $currentDate */
            $currentDate = $currentDates->get($cityId);

            // Если для города пришел null, и у нас была дата, то надо ее удалить
            if (null === $releaseDate && null !== $currentDate) {
                $this->releaseDateRepository->remove($currentDate);
                continue;
            }

            // Если пришел не null, то создаем/обновляем дату
            if (null !== $releaseDate) {
                if (null === $currentDate) {
                    $currentDate = (new ReleaseDate())
                        ->setMovieId($movieId)
                        ->setCity($cityMap[$cityId]);
                }
                $currentDate->setDatetime($releaseDate);
                $newDates->add($currentDate);
            }
        }

        $this->releaseDateRepository->saveDates($newDates);
    }

    /**
     * Удаление дат старта для фильма
     *
     * @param Movie $movie фильм
     */
    public function deleteAllForMovie(Movie $movie)
    {
        $this->releaseDateRepository->removeAllForMovie($movie->getId());
    }
}

<?php

namespace Illuzion\ApiBundle\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Illuzion\ApiBundle\Entity\Main\Genre;
use Illuzion\ApiBundle\Filter\GenreFilter;
use Illuzion\ApiBundle\Provider\EntityManagerProvider;
use Illuzion\ApiBundle\Repository\GenreRepository;

/**
 * Сервис для работы с жанрами
 */
class GenreService
{
    /** @var GenreRepository */
    protected $genreRepository;

    /**
     * @param EntityManagerProvider $entityManagerProvider
     */
    public function __construct(EntityManagerProvider $entityManagerProvider)
    {
        $this->genreRepository = $entityManagerProvider
            ->getMainEm()
            ->getRepository('Main:Genre');
    }

    /**
     * Получить все жанры по фильтру
     *
     * @param GenreFilter $filter фильтр
     * @return ArrayCollection|Genre[]
     */
    public function getAll(GenreFilter $filter)
    {
        return new ArrayCollection(
            $this->genreRepository->findAllByFilter($filter)
        );
    }

    /**
     * Получить жанр по id
     *
     * @param string $id id жанра
     * @return Genre|null
     */
    public function get($id)
    {
        return $this->genreRepository->find($id);
    }
}

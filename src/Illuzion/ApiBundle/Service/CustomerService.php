<?php

namespace Illuzion\ApiBundle\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Illuzion\ApiBundle\Entity\Customer\Customer;
use Illuzion\ApiBundle\Filter\CustomerFilter;
use Illuzion\ApiBundle\Model\CustomerUpdateRequest;
use Illuzion\ApiBundle\Model\ResetPasswordRequest;
use Illuzion\ApiBundle\Provider\EntityManagerProvider;
use Illuzion\ApiBundle\Repository\CustomerRepository;
use Illuzion\ApiBundle\Repository\DiscountCardRepository;

/**
 * Сервис для работы с пользователями
 */
class CustomerService
{
    /** Символы, используемы для генерации пароля при сбросе */
    const PASS_CHARTERS = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

    /** @var CustomerRepository */
    protected $customerRepository;

    /** @var DiscountCardRepository */
    protected $discountCardRepository;

    /** @var NotificationService */
    protected $notificationService;

    /**
     * @param EntityManagerProvider $entityManagerProvider
     * @param NotificationService $notificationService
     */
    public function __construct(EntityManagerProvider $entityManagerProvider, NotificationService $notificationService)
    {
        $this->customerRepository = $entityManagerProvider
            ->getCustomerEm()
            ->getRepository('Customer:Customer');

        $this->discountCardRepository = $entityManagerProvider
            ->getCustomerEm()
            ->getRepository('Customer:DiscountCard');

        $this->notificationService = $notificationService;
    }

    /**
     * Получить пользователя оп Id или номеру карты
     *
     * @param string $id
     * @return Customer|null
     */
    public function get($id)
    {
        return $this->customerRepository->findOneByIdOrCardNumber($id);
    }

    /**
     * Получить пользователя по логину
     *
     * @param string $login
     * @return Customer|null
     */
    public function getByLogin($login)
    {
        return $this->customerRepository->findOneByEmailOrCardNumber($login);
    }

    /**
     * Получить список пользователей по фильтру
     *
     * @param CustomerFilter $filter
     * @return ArrayCollection|Customer[]
     */
    public function getAll(CustomerFilter $filter)
    {
        return new ArrayCollection(
            $this->customerRepository->findAllByFilter($filter)
        );
    }

    /**
     * Изменить кол-во бонусов после окончания сеанса
     *
     * @param Customer $customer Пользователя
     * @param string $delta Кол-во бонусов
     * @param \DateTime $seanceDatetime Дата начала сеанса
     */
    public function changeBonus(Customer $customer, $delta, \DateTime $seanceDatetime)
    {
        $this->customerRepository->changeBonus($customer->getMainCard()->getCustomer(), $delta, $seanceDatetime);
    }

    /**
     * Получения кол-ва пользователей по фильтру
     * (игнорирует пагинацию)
     *
     * @param CustomerFilter $filter
     * @return ArrayCollection|Customer[]
     */
    public function count(CustomerFilter $filter)
    {
        return $this->customerRepository->countByFilter($filter);
    }

    /**
     * Изменить данные пользователя
     * При изменении телефона или почты, отсылает письма для подтверждения
     *
     * @param Customer $customer
     * @param CustomerUpdateRequest $request
     */
    public function update(Customer $customer, CustomerUpdateRequest $request)
    {
        if ($request->isBlocked()) {
            $card = $customer->getMainCard();
            if (null !== $card) {
                $card->setActivityLevel(0);
            }
        }

        // Если был изменен пароль, то сначала кодируем его
        if ($request->getPassword()) {
            $this->changePassword($customer, $request->getPassword());
        } else {
            $this->customerRepository->save($customer);
        }

        // Отсылаем письма для подтверждения изменения почты
        if ($customer->isEmailUpdated()) {
            if ($customer->getEmail()) {
                $this->notificationService->sendEmailConfirmationOnChange(
                    $customer->getEmailNew(),
                    $customer->getName(),
                    $this->generateEmailHash($customer),
                    $customer->getId()
                );
            } else {
                $this->notificationService->sendEmailConfirmationOnRegistration(
                    $customer->getEmailNew(),
                    $customer->getName(),
                    $this->generateEmailHash($customer),
                    $customer->getId()
                );
            }
        }

        // Отсылаем смс для подтверждения изменения телефона
        if ($customer->isPhoneUpdated()) {
            $this->notificationService->sendPhoneChangeCode(
                $customer->getPhoneNew(),
                $customer->getName(),
                $this->generateSmsCode($customer)
            );
        }
    }

    /**
     * Подтвердить почту
     *
     * @param Customer $customer Пользователь
     * @param string $emailHash Код
     * @return bool Успешно ли прошло подтверждение
     */
    public function confirmEmail(Customer $customer, $emailHash)
    {
        if (!$customer->getEmailConfirmationSentDate() || !$customer->getEmailNew()) {
            return false;
        }

        // Код подтверждения валиден в течении дня
        if ($customer->getEmailConfirmationSentDate() < new \DateTime('-1 day')) {
            return false;
        }

        // Кто-то уже подтвердил раньше
        if (!$this->customerRepository->isEmailUnique($customer->getEmailNew())) {
            return false;
        }

        // Проверяем правильность хеша письма
        if ($this->generateEmailHash($customer) !== $emailHash) {
            return false;
        }

        $isNew = empty($customer->getEmail());
        $customer
            ->setEmail($customer->getEmailNew())
            ->setEmailConfirmed(true)
            ->setEmailConfirmationSentDate(null)
            ->setEmailNew(null);

        $card = $customer->getMainCard();
        if (null !== $card && null === $card->getDatetimeReg()) {
            $card->setDatetimeReg(new \DateTime());
            $this->discountCardRepository->save($card);
        }

        $this->customerRepository->save($customer);

        // Отсылаем письмо приветствия если это активация пользователя
        if ($isNew) {
            $this->notificationService->sendWelcomeLetter(
                $customer->getEmail(),
                $customer->getName()
            );
        }

        return true;
    }

    /**
     * Подтвердить телефон
     *
     * @param Customer $customer Пользователь
     * @param string $code Код
     * @return bool Успешно ли прошло подтверждение
     */
    public function confirmPhone(Customer $customer, $code)
    {
        if (!$customer->getPhoneConfirmationSentDate() || !$customer->getPhoneNew()) {
            return false;
        }

        if ($customer->getPhoneConfirmationSentDate() < new \DateTime('-1 day')) {
            return false;
        }

        // Кто-то уже подтвердил раньше
        if (!$this->customerRepository->isPhoneUnique($customer->getPhoneNew())) {
            return false;
        }

        if ($this->generateSmsCode($customer) !== $code) {
            return false;
        }

        $customer
            ->setPhone($customer->getPhoneNew())
            ->setPhoneConfirmed(true)
            ->setPhoneConfirmationSentDate(null)
            ->setPhoneNew(null);
        $this->customerRepository->save($customer);
        return true;
    }

    /**
     * Проверить пароль пользователя
     *
     * @param Customer $customer Пользователь
     * @param string $password Пароль
     * @return bool Верный ли пароль
     */
    public function validatePassword(Customer $customer, $password)
    {
        $hash = base64_decode($customer->getPasswordHash());
        $salt = substr($hash, 1, 16);
        return $customer->getPasswordHash() === $this->generatePasswordHash($password, $salt);
    }

    /**
     * Сброс пароля пользователя
     *
     * @param Customer $customer Пользователь
     * @param ResetPasswordRequest $request Запрос на сброс пароля
     */
    public function resetPassword(Customer $customer, ResetPasswordRequest $request)
    {
        $charactersLength = strlen(self::PASS_CHARTERS);
        $newPassword = '';
        for ($i = 0; $i < 10; $i++) {
            $newPassword .= self::PASS_CHARTERS[rand(0, $charactersLength - 1)];
        }

        $this->changePassword($customer, $newPassword);

        switch ($request->getSendTo()) {
            case 'phone':
                if (!$customer->getPhone()) {
                    break;
                }
                $this->notificationService->sendSmsResetPassword(
                    $customer->getPhone(),
                    $customer->getName(),
                    $newPassword
                );
                break;
            case 'email':
                if (!$customer->getEmail()) {
                    break;
                }
                $this->notificationService->sendEmailResetPassword(
                    $customer->getEmail(),
                    $customer->getName(),
                    $newPassword
                );
                break;
        }
    }

    /**
     * Сохраняет хеш и соль для пароля
     *
     * @param Customer $customer
     * @param string $password
     */
    public function changePassword(Customer $customer, $password)
    {
        $salt = random_bytes(16);
        $hash = $this->generatePasswordHash($password, $salt);
        $customer->setPasswordHash($hash);
        $this->customerRepository->save($customer);
    }

    /**
     * Генерация кода для подтверждения почты
     *
     * @param Customer $customer
     * @return string
     */
    protected function generateEmailHash(Customer $customer)
    {
        if (!$customer->getEmailConfirmationSentDate()) {
            return false;
        }

        return \md5($customer->getEmailConfirmationSentDate()->format('Y-m-d H:i:s'));
    }

    /**
     * Генерация кода для подтверждения телефона
     *
     * @param Customer $customer
     * @return string
     */
    protected function generateSmsCode(Customer $customer)
    {
        if (!$customer->getPhoneConfirmationSentDate()) {
            return false;
        }

        return $customer->getPhoneConfirmationSentDate()->format('is');
    }

    /**
     * Генерация хеша пароля
     *
     * @param string $password
     * @param string $salt
     * @return string
     */
    protected function generatePasswordHash($password, $salt)
    {
        $passwordHash = hash_pbkdf2('sha1', $password, $salt, 1000, 32, true);
        $passwordHash = chr(0) . $salt . $passwordHash;
        return base64_encode($passwordHash);
    }
}

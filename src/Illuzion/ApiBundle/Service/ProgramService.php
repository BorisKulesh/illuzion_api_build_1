<?php

namespace Illuzion\ApiBundle\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Illuzion\ApiBundle\Entity\Main\Program;
use Illuzion\ApiBundle\Filter\ProgramFilter;
use Illuzion\ApiBundle\Provider\EntityManagerProvider;
use Illuzion\ApiBundle\Repository\ProgramRepository;

/**
 * Сервис для работы с программами
 */
class ProgramService
{
    /** @var ProgramRepository */
    protected $programRepository;

    /**
     * @param EntityManagerProvider $entityManagerProvider
     */
    public function __construct(EntityManagerProvider $entityManagerProvider)
    {
        $this->programRepository = $entityManagerProvider
            ->getMainEm()
            ->getRepository('Main:Program');
    }

    /**
     * Получение списка программ по фильтру
     *
     * @param ProgramFilter $filter
     *
     * @return ArrayCollection|Program[]
     */
    public function getAll(ProgramFilter $filter)
    {
        return new ArrayCollection(
            $this->programRepository->findAllByFilter($filter)
        );
    }

    /**
     * Получение кол-ва программ по фильтру
     *
     * @param ProgramFilter $filter
     *
     * @return int
     */
    public function count(ProgramFilter $filter)
    {
        return $this->programRepository->countByFilter($filter);
    }

    /**
     * Получение программы по id
     *
     * @param string $id Id программы
     * @return Program|null
     */
    public function get($id)
    {
        if (is_numeric($id)) {
            return $this->programRepository->find($id);
        } else {
            return $this->programRepository->findOneBy(['alias' => $id]);
        }
    }

    /**
     * Сохранение программы
     *
     * @param Program $program
     */
    public function save(Program $program)
    {
        $this->programRepository->save($program);
    }

    /**
     * Удаление программы
     *
     * @param Program $program
     */
    public function delete(Program $program)
    {
        $this->programRepository->delete($program);
    }
}

<?php

namespace Illuzion\ApiBundle\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Illuzion\ApiBundle\Entity\Main\Show;
use Illuzion\ApiBundle\Filter\ShowFilter;
use Illuzion\ApiBundle\Provider\EntityManagerProvider;
use Illuzion\ApiBundle\Repository\ShowRepository;

/**
 * Сервис для работы с сеансами
 */
class ShowService extends LocalCinemaService
{
    /** @var EntityManagerProvider */
    protected $emProvider;

    /**
     * @param EntityManagerProvider $entityManagerProvider
     */
    public function __construct(EntityManagerProvider $entityManagerProvider)
    {
        $this->emProvider = $entityManagerProvider;
    }

    /**
     * Получение списка сеансов по фильтру
     *
     * @param ShowFilter $filter фильтр
     * @return ArrayCollection|Show[]
     */
    public function getAll(ShowFilter $filter)
    {
        if (null !== ($cinemaId = $filter->getCinema())) {
            // Если запрашивают конкретный кинотеатр, то тянем из его базы
            $shows = $this->getAllForCinema($cinemaId, $filter)->toArray();
        } else {
            // В противном случае тянем из всех
            $shows = [];
            foreach ($this->emProvider->getSupportedCinemas() as $cinemaId) {
                $shows = array_merge($shows, $this->getAllForCinema($cinemaId, $filter)->toArray());
            }
        }

        return new ArrayCollection($shows);
    }

    /**
     * Получение списка фильмов для кинотеатра по фильтру
     *
     * @param string $cinemaId Id кинотеатра
     * @param ShowFilter $filter фильтр
     * @return ArrayCollection|Show[]
     */
    protected function getAllForCinema($cinemaId, ShowFilter $filter)
    {
        if (!in_array($cinemaId, $this->emProvider->getSupportedCinemas())) {
            return new ArrayCollection();
        }

        /** @var ShowRepository $repository */
        $repository = $this->emProvider->getMainEmForCinema($cinemaId)->getRepository('Main:Show');
        $shows = $repository->findAllByFilter($filter);

        return new ArrayCollection($shows);
    }

    /**
     * Получение сеанса по Id
     *
     * @param string $id
     * @return Show|null
     */
    public function get($id)
    {
        // Определяем id кинотеатра и сеанса в нем
        list($cinemaId, $showId) = $this->getIdAndCinema($id);
        if (!$cinemaId || !$showId || !in_array($cinemaId, $this->emProvider->getSupportedCinemas())) {
            return null;
        }

        $repository = $this->emProvider->getMainEmForCinema($cinemaId)->getRepository('Main:Show');
        $show = $repository->find($showId);

        return $show;
    }
}

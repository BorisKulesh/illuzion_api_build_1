<?php

namespace Illuzion\ApiBundle\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Illuzion\ApiBundle\Entity\Shared\Cinema;
use Illuzion\ApiBundle\Filter\CinemaFilter;
use Illuzion\ApiBundle\Provider\EntityManagerProvider;
use Illuzion\ApiBundle\Repository\CinemaRepository;

/**
 * Сервис для работы с кинотеатрами
 */
class CinemaService
{
    /** @var CinemaRepository */
    protected $cinemaRepository;

    /**
     * @param EntityManagerProvider $entityManagerProvider
     */
    public function __construct(EntityManagerProvider $entityManagerProvider)
    {
        $this->cinemaRepository = $entityManagerProvider
            ->getSharedEm()
            ->getRepository('Shared:Cinema');
    }

    /**
     * Получить список кинотеатров по фильтру
     *
     * @param CinemaFilter $filter
     * @return ArrayCollection|Cinema[]
     */
    public function getAll(CinemaFilter $filter)
    {
        return new ArrayCollection(
            $this->cinemaRepository->findAllByFilter($filter)
        );
    }

    /**
     * Найти кинотеатра по id или алиасу
     *
     * @param string $id
     * @return Cinema|null
     */
    public function get($id)
    {
        return $this->cinemaRepository->findOneByIdOrAlias($id);
    }
}

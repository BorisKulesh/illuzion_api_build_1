<?php

namespace Illuzion\ApiBundle\Service;

/**
 * Базовый класс для сервисов, которые работают с сущностями от Illuzion\ApiBundle\Entity\LocalEntity (заказа, сеансы, ...)
 */
abstract class LocalCinemaService
{
    /**
     * @param string $id
     * @return array [$cinemaId, $localId]
     */
    protected function getIdAndCinema($id)
    {
        $data = explode('_', $id);
        if (count($data) === 1) {
            $data[1] = null;
        }
        return $data;
    }
}

<?php

namespace Illuzion\ApiBundle\Service;

use GuzzleHttp\Client;

/**
 * Сервис для работы с внешним сервисом уведомлений
 */
class NotificationService
{
    // Шаблон для генерации ссылки на активацию пользователя
    const ACTIVATION_LINK_PATTERN = 'http://illuzion.ru/confirm-account-activation/__customer__/__hash__';
    // Шаблон для генерации ссылки на смену почты
    const CONFIRMATION_LINK_PATTERN = 'http://illuzion.ru/confirm-email-change/__customer__/__hash__';

    /** @var Client http клиент */
    protected $client;

    /** @var CinemaService */
    protected $cinemaService;

    /**
     * @param string $host Хост сервиса
     * @param string $token Токен для авторизации
     */
    public function __construct($host, $token)
    {
        $this->client = new Client([
            'base_uri' => $host,
            'timeout' => 30,
            'auth' => [$token, ''],
        ]);
    }

    /**
     * Отослать письмо для подтверждения изменения email
     *
     * @param string $toEmail Адрест
     * @param string $toName Имя пользователя
     * @param string $hash Токен для подтверждения
     * @param string $customerId Id пользователя
     */
    public function sendEmailConfirmationOnChange($toEmail, $toName, $hash, $customerId)
    {
        $this->client->post('email/customer/confirm-email-on-change', [
            'json' => [
                'to_email' => $toEmail,
                'to_name' => $toName,
                'confirmation_link' => str_replace(
                    ['__hash__', '__customer__'],
                    [$hash, $customerId],
                    self::CONFIRMATION_LINK_PATTERN
                ),
            ],
            'http_errors' => true,
        ]);
    }

    /**
     * Отослать письмо для подтверждения регистрации
     *
     * @param string $toEmail Адресат
     * @param string $toName Имя пользователя
     * @param string $hash Токен для подтверждения
     * @param string $customerId Id пользователя
     */
    public function sendEmailConfirmationOnRegistration($toEmail, $toName, $hash, $customerId)
    {
        $this->client->post('email/customer/confirm-email-on-registration', [
            'json' => [
                'to_email' => $toEmail,
                'to_name' => $toName,
                'confirmation_link' => str_replace(
                    ['__hash__', '__customer__'],
                    [$hash, $customerId],
                    self::ACTIVATION_LINK_PATTERN
                ),
            ],
            'http_errors' => true,
        ]);

    }

    /**
     * Отослать письмо с уведомлением о успешной регистрации
     *
     * @param string $toEmail Адресат
     * @param string $toName Имя пользователя
     */
    public function sendWelcomeLetter($toEmail, $toName)
    {
        $this->client->post('customer/welcome-letter', [
            'json' => [
                'to_email' => $toEmail,
                'to_name' => $toName,
            ],
            'http_errors' => true,
        ]);
    }

    /**
     * Отослать письмо для уведомлении о сбросе пароля
     *
     * @param string $toEmail Адресат
     * @param string $toName Имя пользователя
     * @param string $newPassword Новый пароль
     */
    public function sendEmailResetPassword($toEmail, $toName, $newPassword)
    {
        $this->client->post('email/customer/reset-password', [
            'json' => [
                'to_email' => $toEmail,
                'to_name' => $toName,
                'new_password' => $newPassword,
            ],
            'http_errors' => true,
        ]);
    }

    /**
     * Отсылка смс для уведомления о сбросе пароля
     *
     * @param string $toPhone Адресат
     * @param string $toName Имя пользователя
     * @param string $newPassword Новый пароль
     */
    public function sendSmsResetPassword($toPhone, $toName, $newPassword)
    {
        $this->client->post('sms/customer/reset-password', [
            'json' => [
                'to_phone' => $toPhone,
                'to_name' => $toName,
                'new_password' => $newPassword,
            ],
            'http_errors' => true,
        ]);
    }

    /**
     * Отсылка смс для подтверждения о смене телефона
     *
     * @param string $toNumber Адресат
     * @param string $toName Имя пользователя
     * @param string $code Код подтверждения
     */
    public function sendPhoneChangeCode($toNumber, $toName, $code)
    {
        $this->client->post('sms/customer/confirm-phone-on-change', [
            'json' => [
                'to_phone' => $toNumber,
                'to_name' => $toName,
                'confirmation_code' => $code,
            ],
            'http_errors' => true,
        ]);
    }

    /**
     * Отсылка письма для уведомления о успешном заказе
     *
     * @param string $toEmail Адресат
     * @param string $toName Имя пользователя
     * @param string $bookingCode Код покупки
     * @param string $orderId Id заказа
     * @param string $movieTitle Название фильма
     * @param string $cinemaTitle Название кинотеатра
     * @param string $hallTitle Название зала
     * @param integer[][] $seats Массив мест [['row' => 1, 'place' => 1], ...]
     */
    public function sendEmailOrderConfirmation(
        $toEmail,
        $toName,
        $bookingCode,
        $orderId,
        $movieTitle,
        $cinemaTitle,
        $hallTitle,
        $seats
    ) {
        $this->client->post('email/order/confirmation', [
            'json' => [
                'to_email' => $toEmail,
                'to_name' => $toName,
                'booking_code' => $bookingCode,
                'order_id' => $orderId,
                'movie_title' => $movieTitle,
                'cinema_title' => $cinemaTitle,
                'hall_title' => $hallTitle,
                'seats' => $seats,
            ],
            'http_errors' => true,
        ]);
    }

    /**
     * Отсылка смс для уведомления о успешном заказе
     *
     * @param string $toPhone Адресат
     * @param string $bookingCode Код покупки
     */
    public function sendSmsOrderConfirmation($toPhone, $bookingCode)
    {
        $this->client->post('sms/order/confirmation', [
            'json' => [
                'to_phone' => $toPhone,
                'booking_code' => $bookingCode,
            ],
            'http_errors' => true,
        ]);
    }
}

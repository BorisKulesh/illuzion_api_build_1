<?php

namespace Illuzion\ApiBundle\Filter;

use Illuzion\ApiBundle\Request\BaseFilter;
use Illuzion\ApiBundle\Request\Pagination;

/**
 * Фильтр статей
 */
class ArticleFilter extends BaseFilter
{
    /**
     * @return string|null Поиск по названию или алиасу
     */
    public function getSearch()
    {
        return $this->get('search');
    }

    /**
     * @return boolean|null Опубликованные новости
     */
    public function getPublished()
    {
        return $this->get('published');
    }

    /**
     * @return Pagination|null Пагинации
     */
    public function getPagination()
    {
        return $this->get('pagination');
    }
}

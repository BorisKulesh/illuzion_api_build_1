<?php

namespace Illuzion\ApiBundle\Filter;

use Illuzion\ApiBundle\Request\BaseFilter;

/**
 * Фильтр жанров
 */
class GenreFilter extends BaseFilter
{
    /**
     * @return string|null Поиск по названию
     */
    public function getSearch()
    {
        return $this->get('search');
    }
}

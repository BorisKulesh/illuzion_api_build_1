<?php

namespace Illuzion\ApiBundle\Filter;

use Illuzion\ApiBundle\Request\BaseFilter;

/**
 * Фильтр сеансов
 */
class ShowFilter extends BaseFilter
{
    /**
     * @return string|null Id кинотеатра
     */
    public function getCinema()
    {
        return $this->get('cinema');
    }

    /**
     * @return int|null Id фильма
     */
    public function getMovie()
    {
        return $this->get('movie');
    }

    /**
     * @return \DateTime|null Дета проведения сеансов
     */
    public function getDate()
    {
        return $this->get('date');
    }

    /**
     * @return string[] Список форматов показа
     */
    public function getFormat()
    {
        return $this->get('format');
    }
}

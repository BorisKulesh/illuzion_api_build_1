<?php

namespace Illuzion\ApiBundle\Filter;

use Illuzion\ApiBundle\Request\BaseFilter;

/**
 * Фильтр мест
 */
class SeatFilter extends BaseFilter
{
    /**
     * @return int|null Id сеанса
     */
    public function getShow()
    {
        return $this->get('show');
    }

    /**
     * @return int|null Свободные места
     */
    public function getAvailable()
    {
        return $this->get('available');
    }

    /**
     * @return int[] Список id конкретных мест
     */
    public function getSeats()
    {
        return $this->get('seats');
    }
}

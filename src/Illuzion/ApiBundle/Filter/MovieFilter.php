<?php

namespace Illuzion\ApiBundle\Filter;

use Illuzion\ApiBundle\Request\BaseFilter;
use Illuzion\ApiBundle\Request\DatesRange;
use Illuzion\ApiBundle\Request\Pagination;

/**
 * Фильтр фильмов
 */
class MovieFilter extends BaseFilter
{
    /**
     * @return string|null Статус фильма (текущие, архивные, все) ["all", "current", "archived"]
     */
    public function getStatus()
    {
        return $this->get('status');
    }

    /**
     * @return string|null Поиск по id, названию или алиасу
     */
    public function getSearch()
    {
        return $this->get('search');
    }

    /**
     * @return int|null Id программы
     */
    public function getProgram()
    {
        return $this->get('program');
    }

    /**
     * @return string[] Список форматов
     */
    public function getFormat()
    {
        return $this->get('format');
    }

    /**
     * @return string|null Язык
     */
    public function getLanguage()
    {
        return $this->get('language');
    }

    /**
     * @return Pagination|null Пагинация
     */
    public function getPagination()
    {
        return $this->get('pagination');
    }

    /**
     * @return string|null
     */
    public function getCinema()
    {
        return $this->get('cinema');
    }

    /**
     * @return DatesRange|null
     */
    public function getDateStartRange()
    {
        return $this->get('sale_start_date');
    }
}

<?php

namespace Illuzion\ApiBundle\Filter;

use Illuzion\ApiBundle\Request\BaseFilter;

/**
 * Фильтр кинотеатров
 */
class CinemaFilter extends BaseFilter
{
    /**
     * @return string|null Id города
     */
    public function getCity()
    {
        return $this->get('city');
    }
}

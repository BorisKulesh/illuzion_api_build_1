<?php

namespace Illuzion\ApiBundle\Filter;

use Illuzion\ApiBundle\Request\BaseFilter;

/**
 * Фильтр залов
 */
class HallFilter extends BaseFilter
{
    /**
     * @return string|null Id кинотеатра
     */
    public function getCinema()
    {
        return $this->get('cinema');
    }
}

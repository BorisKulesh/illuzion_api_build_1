<?php

namespace Illuzion\ApiBundle\Filter;

use Illuzion\ApiBundle\Request\BaseFilter;
use Illuzion\ApiBundle\Request\Pagination;

/**
 * Фильтр программ
 */
class ProgramFilter extends BaseFilter
{
    /**
     * @return string|null Поиск по названию или алиасу
     */
    public function getSearch()
    {
        return $this->get('search');
    }

    /**
     * @return string|null Id города
     */
    public function getCity()
    {
        return $this->get('city');
    }

    /**
     * @return boolean|null Опубликованные программы
     */
    public function getPublished()
    {
        return $this->get('published');
    }

    /**
     * @return Pagination|null Пагинация
     */
    public function getPagination()
    {
        return $this->get('pagination');
    }
}

<?php

namespace Illuzion\ApiBundle\Filter;

use Illuzion\ApiBundle\Request\BaseFilter;
use Illuzion\ApiBundle\Request\Pagination;

/**
 * Фильтр заказов
 */
class OrderFilter extends BaseFilter
{
    /**
     * @return string|null Статус заказа
     */
    public function getStatus()
    {
        return $this->get('status');
    }

    /**
     * @return int|null Id пользователя
     */
    public function getCustomer()
    {
        return $this->get('customer');
    }

    /**
     * @return string|null Код покупки
     */
    public function getPrintCode()
    {
        return $this->get('print_code');
    }

    /**
     * @return Pagination|null Пагинация
     */
    public function getPagination()
    {
        return $this->get('pagination');
    }

    /**
     * @return string|null Id кинотеатра
     */
    public function getCinema()
    {
        return $this->get('cinema');
    }

    /**
     * @return string|null Имя клиента
     */
    public function getClient()
    {
        return $this->get('client');
    }

    /**
     * @param string|null $client Имя клиента
     *
     * @return $this
     */
    public function setClient($client)
    {
        $this->set('client', (string) $client);
        return $this;
    }
}

<?php

namespace Illuzion\ApiBundle\Filter;

use Illuzion\ApiBundle\Request\BaseFilter;
use Illuzion\ApiBundle\Request\Pagination;

/**
 * Фильтр пользователей
 */
class CustomerFilter extends BaseFilter
{
    /**
     * @return boolean|null
     */
    public function getBlocked()
    {
        return $this->get('blocked');
    }

    /**
     * @return string|null Поиск по имени, телефону, почте или номеру карты
     */
    public function getSearch()
    {
        return $this->get('search');
    }

    /**
     * @return string|null Тип пользователя
     */
    public function getType()
    {
        return $this->get('type');
    }

    /**
     * @return Pagination|null Пагинация
     */
    public function getPagination()
    {
        return $this->get('pagination');
    }
}

<?php

namespace Illuzion\ApiBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Обработка всех сервисов помеченных тегом serializer.schema
 * @link http://symfony.com/doc/current/service_container/tags.html
 */
class SerializerContainerCompilerPass implements CompilerPassInterface
{
    /**
     * @inheritdoc
     */
    public function process(ContainerBuilder $container)
    {
        $containerDefinition = $container->findDefinition('api.serializer.container');
        $providers = $container->findTaggedServiceIds('serializer.schema');

        foreach ($providers as $id => $tags) {
            foreach ($tags as $attrs) {
                $containerDefinition->addMethodCall('register', [new Reference($id)]);
            }
        }
    }
}

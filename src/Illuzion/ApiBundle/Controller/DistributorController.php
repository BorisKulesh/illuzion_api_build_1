<?php

namespace Illuzion\ApiBundle\Controller;
use Illuzion\ApiBundle\Service\DistributorService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Response;

/**
 * Контроллер для работы с прокатчиками
 *
 * @Route("/distributors", service="api.controller.distributor")
 */
class DistributorController extends BaseController
{
    /** @var DistributorService */
    protected $distributorService;

    /**
     * @param DistributorService $distributorService
     */
    public function __construct(DistributorService $distributorService)
    {
        $this->distributorService = $distributorService;
    }

    /**
     * Получение списка прокатчиков
     *
     * @Route("", name="api.v10.distributor.list")
     * @Method("GET")
     * @Security("has_role('DISTRIBUTOR_VIEW')")
     *
     * @return Response
     */
    public function listAction()
    {
        $distributors = $this->distributorService->getAll();
        return $this->jsonapi($distributors, [
            self::META_COUNT => $distributors->count(),
        ]);
    }

    /**
     * Получение прокатчика по Id
     *
     * @Route("/{id}", name="api.v10.distributor.get")
     * @Method("GET")
     * @Security("has_role('DISTRIBUTOR_VIEW')")
     *
     * @param string $id
     *
     * @return Response
     */
    public function getAction($id)
    {
        $distributor = $this->distributorService->get($id);
        if (null === $distributor) {
            return $this->jsonapiError("Прокатчик не найден", 'not_found', 404);
        }

        return $this->jsonapi($distributor);
    }
}

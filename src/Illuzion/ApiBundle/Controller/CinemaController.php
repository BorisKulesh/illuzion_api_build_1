<?php

namespace Illuzion\ApiBundle\Controller;

use Illuzion\ApiBundle\Configuration\Query as Query;
use Illuzion\ApiBundle\Filter\CinemaFilter;
use Illuzion\ApiBundle\Service\CinemaService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Response;

/**
 * Контроллер. Содержит методы для работы с кинотеатрами
 *
 * @Route("/cinemas", service="api.controller.cinema")
 */
class CinemaController extends BaseController
{
    /** @var CinemaService */
    protected $cinemaService;

    /**
     * @param CinemaService $cinemaService
     */
    public function __construct(CinemaService $cinemaService)
    {
        $this->cinemaService = $cinemaService;
    }

    /**
     * Получение списка кинотеатров
     *
     * @Route("", name="api.v10.cinema.list")
     * @Method("GET")
     * @Security("has_role('CINEMA_VIEW')")
     *
     * // Список фильтров
     * @Query\StringParam("city", desc="Id города")
     *
     * @param CinemaFilter $filter Фильтры переданные в запросе
     *
     * @return Response
     */
    public function listAction(CinemaFilter $filter)
    {
        $cinemas = $this->cinemaService->getAll($filter);
        return $this->jsonapi($cinemas, [
            self::META_COUNT => $cinemas->count(),
        ]);
    }

    /**
     * Получение кинотеатра по Id или алиасу
     *
     * @Route("/{id}", name="api.v10.cinema.get")
     * @Method("GET")
     * @Security("has_role('CINEMA_VIEW')")
     *
     * @param string $id
     *
     * @return Response
     */
    public function getAction($id)
    {
        $cinema = $this->cinemaService->get($id);
        if (null === $cinema) {
            return $this->jsonapiError("Кинотеатр не найден", 'not_found', 404);
        }

        return $this->jsonapi($cinema);
    }
}

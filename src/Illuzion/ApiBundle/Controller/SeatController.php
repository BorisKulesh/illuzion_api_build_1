<?php

namespace Illuzion\ApiBundle\Controller;

use Illuzion\ApiBundle\Configuration\Query as Query;
use Illuzion\ApiBundle\Filter\SeatFilter;
use Illuzion\ApiBundle\Service\SeatService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Response;

/**
 * Контроллер для работы с местами
 *
 * @Route("/seats", service="api.controller.seat")
 */
class SeatController extends BaseController
{
    /** @var SeatService */
    protected $seatService;

    /**
     * @param SeatService $seatService
     */
    public function __construct(SeatService $seatService)
    {
        $this->seatService = $seatService;
    }

    /**
     * Получение списка мест
     *
     * @Route("", name="api.v10.seat.list")
     * @Method("GET")
     * @Security("has_role('SEAT_VIEW')")
     *
     * // Список фильтров
     * @Query\StringParam("show", required=true, desc="Id сеанса")
     * @Query\BooleanParam("available", desc="Свободные места")
     * @Query\ScalarArrayParam("seats", items=@Query\IntegerParam(desc="Список id конкретных мест"))
     *
     * @param SeatFilter $filter Фильтры переданные в запросе
     *
     * @return Response
     */
    public function listAction(SeatFilter $filter)
    {
        $seats = $this->seatService->getAll($filter);
        return $this->jsonapi($seats, [
            self::META_COUNT => $seats->count(),
        ]);
    }
}

<?php

namespace Illuzion\ApiBundle\Controller;

use Illuzion\ApiBundle\Configuration\Query as Query;
use Illuzion\ApiBundle\Entity\Main\Movie;
use Illuzion\ApiBundle\Filter\MovieFilter;
use Illuzion\ApiBundle\Form\MovieType;
use Illuzion\ApiBundle\Service\MovieService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Контроллер для работы с фильмами
 *
 * @Route("/movies", service="api.controller.movie")
 */
class MovieController extends BaseController
{
    /** @var MovieService */
    protected $movieService;

    /**
     * @param MovieService $movieService
     */
    public function __construct(MovieService $movieService)
    {
        $this->movieService = $movieService;
    }

    /**
     * Получение списка фильмов
     *
     * @Route("", name="api.v10.movie.list")
     * @Method("GET")
     * @Security("has_role('MOVIE_VIEW')")
     *
     * // Список фильтров
     * @Query\StringParam("status", default="current", constraints={
     *     @Assert\Choice(choices={"all", "current", "archived"})
     * }, desc="Статус фильма (текущии, архивные, все)")
     * @Query\StringParam("search", desc="Поиск по id, названию или алиасу")
     * @Query\IntegerParam("program", min=0, desc="Id программы")
     * @Query\ScalarArrayParam("format", items=@Query\StringParam(constraints={
     *     @Assert\Choice(choices={"2D", "3D", "IMAX 2D", "IMAX 3D"})
     * }, desc="Список форматов"))
     * @Query\StringParam("language", desc="Язык")
     * @Query\StringParam("cinema", desc="Кинотеатр")
     * @Query\DatesRangeParam("sale_start_date", mapping={
     *     "date_start": "sales_start_from",
     *     "date_end": "sales_start_to",
     *     "year": "sales_year",
     *     "month": "sales_month"
     * }) // Дата старта продаж
     *
     * // Поддержка пагинации
     * @Query\PaginationParam("pagination")
     *
     * @param MovieFilter $filter Фильтры переданные в запросе
     *
     * @return Response
     */
    public function listAction(MovieFilter $filter)
    {
        $count = $this->movieService->count($filter);
        if ($count > 0) {
            $movies = $this->movieService->getAll($filter);
        } else {
            $movies = [];
        }

        return $this->jsonapi($movies, [
            self::META_COUNT => $count,
            self::META_LIMIT => $filter->getPagination()->getLimit(),
            self::META_OFFSET => $filter->getPagination()->getOffset(),
        ], [
            'distributor',
            'program',
            'genres',
            'programs.parent'
        ]);
    }

    /**
     * Получение фильма по Id
     *
     * @Route("/{id}", name="api.v10.movie.get")
     * @Method("GET")
     * @Security("has_role('MOVIE_VIEW')")
     *
     * @param string $id Id фильма
     *
     * @return Response
     */
    public function getAction($id)
    {
        $movie = $this->movieService->get($id);
        if (null === $movie) {
            return $this->jsonapiError("Фильм не найден", 'not_found', 404);
        }

        return $this->jsonapi($movie, null, [
            'distributor',
            'program',
            'genres',
            'programs.parent'
        ]);
    }

    /**
     * Создание фильма
     *
     * @Route("", name="api.v10.movie.create")
     * @Method("POST")
     * @Security("has_role('MOVIE_CREATE')")
     *
     * @param Request $request Запрос
     *
     * @return Response
     */
    public function createAction(Request $request)
    {
        $movie = new Movie();

        $form = $this->createForm(MovieType::class, $movie);
        $form->submit($request->request->get('data'));
        $this->throwErrorIfFormInvalid($form);

        // Даты старта мы обрабатываем отдельно, т.к. они пишутся в отдельную базу
        $this->movieService->save($movie, $form->get('attributes')->get('start_dates')->getData());

        return $this->jsonapi($movie, null, [], 201);
    }

    /**
     * Изменения фильма
     *
     * @Route("/{id}", name="api.v10.movie.update")
     * @Method("PATCH")
     * @Security("has_role('MOVIE_UPDATE')")
     *
     * @param string $id Id фильма
     * @param Request $request Запрос
     *
     * @return Response
     */
    public function updateAction($id, Request $request)
    {
        $movie = $this->movieService->get($id);
        if (null === $movie) {
            return $this->jsonapiError("Фильм не найден", 'not_found', 404);
        }

        $form = $this->createForm(MovieType::class, $movie);
        $form->submit($request->request->get('data'), false);
        $this->throwErrorIfFormInvalid($form);

        $this->movieService->save($movie, $form->get('attributes')->get('start_dates')->getData());

        return $this->jsonapi($movie);
    }

    /**
     * Удаление фильма
     *
     * @Route("/{id}", name="api.v10.movie.delete")
     * @Method("DELETE")
     * @Security("has_role('MOVIE_DELETE')")
     *
     * @param string $id Id фильма
     *
     * @return Response
     */
    public function deleteAction($id)
    {
        $movie = $this->movieService->get($id);
        if (null === $movie) {
            return $this->jsonapiError("Фильм не найден", 'not_found', 404);
        }

        try {
            $this->movieService->delete($movie);
        } catch (\InvalidArgumentException $e) {
            return $this->jsonapiError($e->getMessage(), 'associated_items');
        }

        return $this->json(null, 204);
    }
}

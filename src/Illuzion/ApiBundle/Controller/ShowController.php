<?php

namespace Illuzion\ApiBundle\Controller;

use Illuzion\ApiBundle\Configuration\Query as Query;
use Illuzion\ApiBundle\Filter\ShowFilter;
use Illuzion\ApiBundle\Service\ShowService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Контроллер для работы с сеансами
 *
 * @Route("/shows", service="api.controller.show")
 */
class ShowController extends BaseController
{
    /** @var ShowService */
    protected $showService;

    /**
     * @param ShowService $showService
     */
    public function __construct(ShowService $showService)
    {
        $this->showService = $showService;
    }

    /**
     * Получение списка сеансов
     *
     * @Route("", name="api.v10.show.list")
     * @Method("GET")
     * @Security("has_role('SHOW_VIEW')")
     *
     * // Списов фильтров
     * @Query\StringParam("cinema", desc="Id кинотеатра")
     * @Query\IntegerParam("movie", min=1, desc="Id фильма")
     * @Query\DateTimeParam("date", format="Y-m-d", desc="Дата проведения сеансов")
     * @Query\ScalarArrayParam("format", items=@Query\StringParam(constraints={
     *     @Assert\Choice(choices={"2D", "3D", "IMAX 2D", "IMAX 3D"})
     * }, desc="Список форматов показа"))
     *
     * @param ShowFilter $filter Фильтры переданные в запросе
     *
     * @return Response
     */
    public function listAction(ShowFilter $filter)
    {
        $shows = $this->showService->getAll($filter);

        return $this->jsonapi($shows, [
            self::META_COUNT => $shows->count(),
        ], [
            'hall.cinema',
            'movie.genres'
        ]);
    }

    /**
     * Получение сеанса по Id
     *
     * @Route("/{id}", name="api.v10.show.get")
     * @Method("GET")
     * @Security("has_role('SHOW_VIEW')")
     *
     * @param string $id Id сеанса
     *
     * @return Response
     */
    public function getAction($id)
    {
        $show = $this->showService->get($id);
        if (null === $show) {
            return $this->jsonapiError("Сеанс не найден", 'not_found', 404);
        }

        return $this->jsonapi($show, null, [
            'hall.cinema',
            'movie.genres'
        ]);
    }
}

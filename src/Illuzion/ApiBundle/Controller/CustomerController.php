<?php

namespace Illuzion\ApiBundle\Controller;

use Illuzion\ApiBundle\Configuration\Query as Query;
use Illuzion\ApiBundle\Filter\CustomerFilter;
use Illuzion\ApiBundle\Form\CustomerChangePasswordRequestType;
use Illuzion\ApiBundle\Form\ResetPasswordRequestType;
use Illuzion\ApiBundle\Form\CustomerLoginFormType;
use Illuzion\ApiBundle\Form\CustomerType;
use Illuzion\ApiBundle\Model\CustomerChangePasswordRequest;
use Illuzion\ApiBundle\Model\ResetPasswordRequest;
use Illuzion\ApiBundle\Model\CustomerLoginForm;
use Illuzion\ApiBundle\Model\CustomerUpdateRequest;
use Illuzion\ApiBundle\Service\CustomerService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Контроллер для работы с пользователями
 *
 * @Route("/customers", service="api.controller.customer")
 */
class CustomerController extends BaseController
{
    /** @var CustomerService */
    protected $customerService;

    /**
     * @param CustomerService $customerService
     */
    public function __construct(CustomerService $customerService)
    {
        $this->customerService = $customerService;
    }

    /**
     * Получение списка пользователей
     *
     * @Route("", name="api.v10.customer.list")
     * @Method("GET")
     * @Security("has_role('CUSTOMER_VIEW')")
     *
     * // Список фильтров
     * @Query\StringParam("search", desc="Поиск по имени, телефону, почте или номеру карты")
     * @Query\StringParam("type", constraints={
     *     @Assert\Choice(choices={"basic", "gold"})
     * }, desc="Тип пользователя")
     * @Query\BooleanParam("blocked", desc="Состояни блокировки")
     *
     * // Поддержка пагинации
     * @Query\PaginationParam("pagination")
     *
     * @param CustomerFilter $filter Фильтры переданные в запросе
     *
     * @return Response
     */
    public function listAction(CustomerFilter $filter)
    {
        $count = $this->customerService->count($filter);
        if ($count > 0) {
            $customers = $this->customerService->getAll($filter);
        } else {
            $customers = [];
        }
        return $this->jsonapi($customers, [
            self::META_COUNT => $count,
            self::META_LIMIT => $filter->getPagination()->getLimit(),
            self::META_OFFSET => $filter->getPagination()->getOffset(),
        ]);
    }

    /**
     * Получение пользователя по Id или номеру карты
     *
     * @Route("/{id}", name="api.v10.customer.get")
     * @Method("GET")
     * @Security("has_role('CUSTOMER_VIEW')")
     *
     * @param string $id
     *
     * @return Response
     */
    public function getAction($id)
    {
        $customer = $this->customerService->get($id);
        if (null === $customer) {
            return $this->jsonapiError("Пользователь не найден", 'not_found', 404);
        }

        return $this->jsonapi($customer);
    }

    /**
     * Метод для обновления пользователя
     * Так же используется для подтверждения изменения телефона или почты
     *
     * @Route("/{id}", name="api.v10.customer.update")
     * @Method("PATCH")
     * @Security("has_role('CUSTOMER_UPDATE')")
     *
     * @Query\StringParam("emailHash", mapping={"emailValidation"}, desc="Код для подтверждения почты")
     * @Query\StringParam("smsCode", mapping={"smsValidation"}, desc="Код для подтверждения телефона")
     *
     * @param string $id Id пользователя или номер карты
     * @param Request $request Запрос
     * @param string|null $emailHash Код для подтверждения почты
     * @param string|null $smsCode Код для подтверждения телефона
     *
     * @return Response
     */
    public function updateAction($id, Request $request, $emailHash = null, $smsCode = null)
    {
        $customer = $this->customerService->get($id);
        if (null === $customer) {
            return $this->jsonapiError("Пользователь не найден", 'not_found', 404);
        }

        // Если передан код подтверждения почты, то пытаемся это сделать и отдаем результат
        if (null !== $emailHash) {
            $result = $this->customerService->confirmEmail($customer, $emailHash);
            if (!$result) {
                return $this->jsonapiError("Provided token is invalid", 'email_hash_invalid', 401);
            }
            return $this->jsonapi($customer);
        }

        // Если передан код подтверждения телефона, то пытаемся это сделать и отдаем результат
        if (null !== $smsCode) {
            $result = $this->customerService->confirmPhone($customer, $smsCode);
            if (!$result) {
                return $this->jsonapiError("Provided token is invalid", 'phone_hash_invalid', 401);
            }
            return $this->jsonapi($customer);
        }

        // Если это не подтверждение, то обновляем данные пользователя
        $form = $this->createForm(CustomerType::class, $customer);
        $form->submit($request->request->get('data'), false);
        $this->throwErrorIfFormInvalid($form);

        $form = $form->get('attributes');
        $blocked = $form->get('blocked')->getData();
        $password = $form->get('password')->getData();

        if ($form->get('email')->isSubmitted()) {
            $customer->setEmailConfirmationSentDate(new \DateTime());
        }
        if ($form->get('phone')->isSubmitted()) {
            $customer->setPhoneConfirmationSentDate(new \DateTime());
        }

        $updateRequest = (new CustomerUpdateRequest())
            ->setBlocked($blocked)
            ->setPassword($password)
        ;

        $this->customerService->update($customer, $updateRequest);

        return $this->jsonapi($customer);
    }

    /**
     * Метод для проверки пароля пользователя (авторизации)
     *
     * @Route("/login", name="api.v10.customer.login")
     * @Method("POST")
     * @Security("has_role('CUSTOMER_LOGIN')")
     *
     * @param Request $request Запрос
     *
     * @return Response
     */
    public function loginAction(Request $request)
    {
        $loginData = new CustomerLoginForm();

        $form = $this->createForm(CustomerLoginFormType::class, $loginData);
        $form->submit($request->request->all());
        $this->throwErrorIfFormInvalid($form);

        $customer = $this->customerService->getByLogin($loginData->getLogin());
        if (
            null === $customer ||
            !$this->customerService->validatePassword($customer, $loginData->getPassword())
        ) {
            return $this->jsonapiError("Provided login or password is invalid", 'password_invalid', 401);
        }

        return $this->jsonapi($customer);
    }

    /**
     * Метод для сброса пароля
     *
     * @Route("/reset-password", name="api.v10.customer.reset-password")
     * @Method("POST")
     * @Security("has_role('CUSTOMER_UPDATE')")
     *
     * @param Request $request Запрос
     *
     * @return Response
     */
    public function resetPasswordAction(Request $request)
    {
        $data = new ResetPasswordRequest();

        $form = $this->createForm(ResetPasswordRequestType::class, $data);
        $form->submit($request->request->all());
        $this->throwErrorIfFormInvalid($form);

        $customer = $this->customerService->get($data->getUserId());
        if (null === $customer) {
            return $this->jsonapiError("Пользователь не найден", 'customer_not_found', 404);
        }

        $this->customerService->resetPassword($customer, $data);

        return $this->jsonapi($customer);
    }

    /**
     * Метод для изменения пароля
     * Выполняет валидацию старого пароля
     *
     * @Route("/changePassword", name="api.v10.customer.change-password")
     * @Method("POST")
     * @Security("has_role('CUSTOMER_UPDATE')")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function changePasswordAction(Request $request)
    {
        $data = new CustomerChangePasswordRequest();
        $form = $this->createForm(CustomerChangePasswordRequestType::class, $data);
        $form->submit($request->request->all());
        $this->throwErrorIfFormInvalid($form);

        $customer = $this->customerService->get($data->getUserId());
        if (
            null === $customer ||
            !$this->customerService->validatePassword($customer, $data->getOldPassword())
        ) {
            return $this->jsonapiError("Provided login or password is invalid", 'password_invalid', 401);
        }
        $this->customerService->changePassword($customer, $data->getNewPassword());

        return $this->jsonapi($customer);
    }
}

<?php

namespace Illuzion\ApiBundle\Controller;

use Illuzion\ApiBundle\Configuration\Query as Query;
use Illuzion\ApiBundle\Filter\HallFilter;
use Illuzion\ApiBundle\Service\HallService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Response;

/**
 * Контроллер для работы с залами кинотеатров
 *
 * @Route("/halls", service="api.controller.hall")
 */
class HallController extends BaseController
{
    /** @var HallService */
    protected $hallService;

    /**
     * @param HallService $hallService
     */
    public function __construct(HallService $hallService)
    {
        $this->hallService = $hallService;
    }

    /**
     * Список залов
     *
     * @Route("", name="api.v10.hall.list")
     * @Method("GET")
     * @Security("has_role('HALL_VIEW')")
     *
     * // Список фильтров
     * @Query\StringParam("cinema", desc="Id кинотеатра")
     *
     * @param HallFilter $filter Фильтры переданные в запросе
     *
     * @return Response
     */
    public function listAction(HallFilter $filter)
    {
        $halls = $this->hallService->getAll($filter);
        return $this->jsonapi($halls, [
            self::META_COUNT => $halls->count(),
        ]);
    }

    /**
     * Поиск зала по Id
     *
     * @Route("/{id}", name="api.v10.hall.get")
     * @Method("GET")
     * @Security("has_role('HALL_VIEW')")
     *
     * @param string $id Id зала
     *
     * @return Response
     */
    public function getAction($id)
    {
        $hall = $this->hallService->get($id);
        if (null === $hall) {
            return $this->jsonapiError("Зал не найден", 'not_found', 404);
        }

        return $this->jsonapi($hall);
    }
}

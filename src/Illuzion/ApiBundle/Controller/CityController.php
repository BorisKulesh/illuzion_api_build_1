<?php

namespace Illuzion\ApiBundle\Controller;

use Illuzion\ApiBundle\Service\CityService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Response;

/**
 * Контроллер. Содержит методы для работы с городами
 *
 * @Route("/cities", service="api.controller.city")
 */
class CityController extends BaseController
{
    /** @var CityService */
    protected $cityService;

    /**
     * @param CityService $cityService
     */
    public function __construct(CityService $cityService)
    {
        $this->cityService = $cityService;
    }

    /**
     * Получение списка городов
     *
     * @Route("", name="api.v10.city.list")
     * @Method("GET")
     * @Security("has_role('CITY_VIEW')")
     *
     * @return Response
     */
    public function listAction()
    {
        $cities = $this->cityService->getAll();
        return $this->jsonapi($cities, [
            self::META_COUNT => $cities->count(),
        ]);
    }

    /**
     * Получение города по Id
     *
     * @Route("/{id}", name="api.v10.city.get")
     * @Method("GET")
     * @Security("has_role('CITY_VIEW')")
     *
     * @param string $id
     *
     * @return Response
     */
    public function getAction($id)
    {
        $city = $this->cityService->get($id);
        if (null === $city) {
            return $this->jsonapiError("Город не найден", 'not_found', 404);
        }

        return $this->jsonapi($city);
    }
}

<?php

namespace Illuzion\ApiBundle\Controller;

use Illuzion\ApiBundle\Configuration\Query as Query;
use Illuzion\ApiBundle\Entity\Shared\Article;
use Illuzion\ApiBundle\Filter\ArticleFilter;
use Illuzion\ApiBundle\Form\ArticleType;
use Illuzion\ApiBundle\Service\ArticleService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Контроллер. Содержит методы для работы с новостями
 *
 * @Route("/news", service="api.controller.news")
 */
class NewsController extends BaseController
{
    /** @var ArticleService */
    protected $articleService;

    /**
     * @param ArticleService $articleService
     */
    public function __construct(ArticleService $articleService)
    {
        $this->articleService = $articleService;
    }

    /**
     * Получение списка новостей
     *
     * @Route("", name="api.v10.article.list")
     * @Method("GET")
     * @Security("has_role('NEWS_VIEW')")
     *
     * // Список фильтров
     * @Query\BooleanParam("published", desc="Опубликованные новости")
     * @Query\StringParam("search", desc="Поиск по названию или алиасу")
     *
     * // Поддержка пагинации
     * @Query\PaginationParam("pagination")
     *
     * @param ArticleFilter $filter Фильтры переданные в запросе
     *
     * @return Response
     */
    public function listAction(ArticleFilter $filter)
    {
        $count = $this->articleService->count($filter);
        if ($count > 0) {
            $articles = $this->articleService->getAll($filter);
        } else {
            $articles = [];
        }

        return $this->jsonapi($articles, [
            self::META_COUNT => $count,
            self::META_LIMIT => $filter->getPagination()->getLimit(),
            self::META_OFFSET => $filter->getPagination()->getOffset(),
        ]);
    }

    /**
     * Получение новости по Id
     *
     * @Route("/{id}", name="api.v10.article.get")
     * @Method("GET")
     * @Security("has_role('NEWS_VIEW')")
     *
     * @param string $id Id или алиас новости
     *
     * @return Response
     */
    public function getAction($id)
    {
        $article = $this->articleService->get($id);
        if (null === $article) {
            return $this->jsonapiError("Новость не найдена", 'not_found', 404);
        }

        return $this->jsonapi($article);
    }

    /**
     * Создание новости
     *
     * @Route("", name="api.v10.article.create")
     * @Method("POST")
     * @Security("has_role('NEWS_CREATE')")
     *
     * @param Request $request Запрос
     *
     * @return Response
     */
    public function createAction(Request $request)
    {
        $article = new Article();

        $form = $this->createForm(ArticleType::class, $article);
        $form->submit($request->request->get('data'));
        $this->throwErrorIfFormInvalid($form);

        $this->articleService->save($article);

        return $this->jsonapi($article, null, [], 201);
    }

    /**
     * Изменение новости
     *
     * @Route("/{id}", name="api.v10.article.update")
     * @Method("PATCH")
     * @Security("has_role('NEWS_UPDATE')")
     *
     * @param string $id Id или алиас новости
     * @param Request $request Запрос
     *
     * @return Response
     */
    public function updateAction($id, Request $request)
    {
        $article = $this->articleService->get($id);
        if (null === $article) {
            return $this->jsonapiError("Новость не найдена", 'not_found', 404);
        }

        $form = $this->createForm(ArticleType::class, $article);
        $form->submit($request->request->get('data'), false);
        $this->throwErrorIfFormInvalid($form);

        $this->articleService->save($article);

        return $this->jsonapi($article);
    }

    /**
     * Удаление новости
     *
     * @Route("/{id}", name="api.v10.article.delete")
     * @Method("DELETE")
     * @Security("has_role('NEWS_DELETE')")
     *
     * @param string $id Id или алиас новости
     *
     * @return Response
     */
    public function deleteAction($id)
    {
        $article = $this->articleService->get($id);
        if (null === $article) {
            return $this->jsonapiError("Новость не найдена", 'not_found', 404);
        }

        $this->articleService->delete($article);

        return $this->json(null, 204);
    }
}

<?php

namespace Illuzion\ApiBundle\Controller;

use Illuzion\ApiBundle\Configuration\Query as Query;
use Illuzion\ApiBundle\Entity\Main\Order;
use Illuzion\ApiBundle\Filter\OrderFilter;
use Illuzion\ApiBundle\Form\OrderReservationRequestType;
use Illuzion\ApiBundle\Form\OrderUpdateRequestType;
use Illuzion\ApiBundle\Model\OrderReservationRequest;
use Illuzion\ApiBundle\Model\OrderUpdateRequest;
use Illuzion\ApiBundle\Security\ApiClient;
use Illuzion\ApiBundle\Service\OrderService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

/**
 * Контроллер для управления заказами
 *
 * @Route("/orders", service="api.controller.order")
 */
class OrderController extends BaseController
{
    /** @var OrderService */
    protected $orderService;

    /**
     * @param OrderService $orderService
     */
    public function __construct(OrderService $orderService)
    {
        $this->orderService = $orderService;
    }

    /**
     * Список заказов
     *
     * @Route("", name="api.v10.order.list")
     * @Method("GET")
     * @Security("has_role('ORDER_VIEW')")
     *
     * // Список фильтров
     * @Query\StringParam("status", desc="Статус заказа")
     * @Query\IntegerParam("customer", desc="Id пользователя")
     * @Query\StringParam("cinema", desc="Id кинотеатра")
     * @Query\StringParam("print_code", desc="Код покупки")
     *
     * // Поддержка пагинации
     * @Query\PaginationParam("pagination")
     *
     * @param OrderFilter $filter Фильтры переданные в запросе
     *
     * @return Response
     */
    public function listAction(OrderFilter $filter)
    {
        if (null === $filter->getCustomer() && null === $filter->getCinema()) {
            return $this->jsonapiError('Необходимо указать или пользователя, или кинотеатр', 'invalid_request');
        }

        // Ограничиваем выборку только заказами клиента
        if (!$this->isGranted('ADMIN')) {
            /** @var ApiClient $client клиент сделавший запрос */
            $client = $this->getUser();
            $filter->setClient($client->getUsername());
        }

        $count = $this->orderService->count($filter);
        if (0 === $count) {
            $orders = [];
        } else {
            $orders = $this->orderService->getAll($filter);

            // Т.к. заказы разбросанные по куче баз,то пагинацию делаем тут
            if (null === $count) {
                $count = $orders->count();
                $orders = array_slice(
                    $orders->toArray(),
                    $filter->getPagination()->getOffset(),
                    $filter->getPagination()->getLimit()
                );
            }
        }

        return $this->jsonapi($orders, [
            self::META_COUNT => $count,
            self::META_LIMIT => $filter->getPagination()->getLimit(),
            self::META_OFFSET => $filter->getPagination()->getOffset(),
        ]);
    }

    /**
     * Получение заказа по Id
     *
     * @Route("/{id}", name="api.v10.order.get")
     * @Method("GET")
     * @Security("has_role('ORDER_VIEW')")
     *
     * @param string $id Id заказа
     *
     * @return Response
     */
    public function getAction($id)
    {
        $order = $this->orderService->get($id);
        if (null === $order || !$this->canCurrentClientViewOrder($order)) {
            return $this->jsonapiError("Заказ не найден", 'not_found', 404);
        }

        return $this->jsonapi($order);
    }

    /**
     * Создание заказа
     *
     * @Route("", name="api.v10.order.create")
     * @Method("POST")
     * @Security("has_role('ORDER_CREATE')")
     *
     * @param Request $request Запрос
     *
     * @return Response
     */
    public function createAction(Request $request)
    {
        $orderRequest = new OrderReservationRequest();

        $form = $this->createForm(OrderReservationRequestType::class, $orderRequest);
        $form->submit($request->request->get('data'));
        $this->throwErrorIfFormInvalid($form);

        /** @var ApiClient $client клиент сделавший запрос */
        $client = $this->getUser();
        $order = $this->orderService->createOrder($orderRequest, $client);

        return $this->jsonapi($order, null, [], 201);
    }

    /**
     * Изменение/оплата заказа
     *
     * @Route("/{id}", name="api.v10.order.update")
     * @Method("PATCH")
     * @Security("has_role('ORDER_UPDATE')")
     *
     * @param string $id Id заказа
     * @param Request $request Запрос
     *
     * @return Response
     */
    public function updateAction($id, Request $request)
    {
        $order = $this->orderService->get($id);
        if (null === $order || !$this->canCurrentClientViewOrder($order)) {
            return $this->jsonapiError("Заказ не найден", 'not_found', 404);
        }

        $orderRequest = (new OrderUpdateRequest())
            ->setEmail($order->getEmail())
            ->setPhone($order->getPhone());

        $form = $this->createForm(OrderUpdateRequestType::class, $orderRequest);
        $form->submit($request->request->get('data'), false);
        $this->throwErrorIfFormInvalid($form);

        /** @var ApiClient $client клиент сделавший запрос */
        $client = $this->getUser();

        // Проверям текущий и новый статус заказа, и выполняем необходимые действия
        $currentStatus = $order->getStatusStr();
        switch ($newStatus = $orderRequest->getStatus()) {
            case $currentStatus:
            case null:
                $this->orderService->updateOrder($order, $orderRequest);
                break;
            case 'paid':
                if ($currentStatus !== 'reserved') {
                    return $this->jsonapiError(
                        "Невозможно перевести заказ из состояния {$currentStatus} в состояние {$newStatus}",
                        'bad_request'
                    );
                }
                $this->orderService->purchaseOrder($order, $orderRequest, $client);
                break;
            case 'reserve_released':
                if ($currentStatus !== 'reserved') {
                    return $this->jsonapiError(
                        "Невозможно перевести заказ из состояния {$currentStatus} в состояние {$newStatus}",
                        'bad_request'
                    );
                }
                $this->orderService->unreserveOrder($order);
                break;
            case 'full_returned':
                if ($currentStatus !== 'paid') {
                    return $this->jsonapiError(
                        "Невозможно перевести заказ из состояния {$currentStatus} в состояние {$newStatus}",
                        'bad_request'
                    );
                }
                $this->orderService->rollbackOrder($order, $client);
                break;
            default:
                return $this->jsonapiError(
                    "Невозможно перевести заказ из состояния {$currentStatus} в состояние {$newStatus}",
                    'bad_request'
                );
        }

        return $this->jsonapi($order);
    }

    /**
     * Проверяет, имеет ли текущий клиент доступ к заказу
     *
     * @param Order $order заказ для проверки
     *
     * @return bool
     */
    protected function canCurrentClientViewOrder(Order $order)
    {
        /** @var ApiClient $client клиент сделавший запрос */
        $client = $this->getUser();
        return $this->isGranted('ADMIN')
            || $client->getUsername() === $order->getClient();
    }
}

<?php

namespace Illuzion\ApiBundle\Controller;

use Illuzion\ApiBundle\Exception\ApiException;
use Neomerx\JsonApi\Document\Error;
use Neomerx\JsonApi\Encoder\Parameters\EncodingParameters;
use Neomerx\JsonApi\Exceptions\ErrorCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\ConstraintViolation;

/**
 * Базовый контроллер
 * Содержит вспомогательные методы для всех контроллеров
 */
abstract class BaseController extends Controller
{
    // Название полей метоинформации для пагинации. Взяты с документации к json-api
    const META_COUNT = 'count';
    const META_LIMIT = 'limit';
    const META_OFFSET = 'offset';

    /**
     * Метод генерирует корректный json-api ответ
     *
     * @param object|array $data Объект или массив объектов, которые необходимо сериализовать
     * @param array $meta Метадата, которую необходима прикрепить к ответу
     * @param string[] $defaultIncludes Список связей(relations), которые отдаются в развернутом виде по умолчанию
     * @param int $status Статус код ответа
     * @return JsonResponse
     */
    protected function jsonapi($data, $meta = null, $defaultIncludes = [], $status = 200)
    {
        $encoder = $this->get('api.serializer.encoder');
        $encoder->withMeta($meta);

        return new JsonResponse(
            $encoder->encodeData($data, $this->getEncodingParameters($defaultIncludes)),
            $status,
            ['Content-Type' => 'application/vnd.api+json'],
            true
        );
    }

    /**
     * Метод генерирует корректную ошибку
     *
     * @param string $message Сообщение
     * @param string $code Код ошибки
     * @param int $statusCode Код ответа
     * @return JsonResponse
     */
    protected function jsonapiError($message, $code, $statusCode = 400)
    {
        $encoder = $this->get('api.serializer.encoder');
        $error = new Error(null, null, $statusCode, $code, null, $message);

        return new JsonResponse(
            $encoder->encodeError($error),
            $statusCode,
            ['Content-Type' => 'application/vnd.api+json'],
            true
        );
    }

    /**
     * Метод генериреут корректную jsonapi ошибку, если форма не валидна
     *
     * @param Form $form
     */
    protected function throwErrorIfFormInvalid(Form $form)
    {
        if ($form->isValid()) {
            return;
        }

        $errors = new ErrorCollection();
        foreach ($form->getErrors(true, true) as $error) {
            $cause = $error->getCause();
            if ($cause instanceof ConstraintViolation) {
                // Тут мы пытаемся преобразовать ошибки валидации форм в формат jsonapi
                // Необходимые манипуляции были получены опытным путем

                $path = $cause->getPropertyPath();
                $path = preg_replace('/children\[([^\]]+)\]/', '${1}', $path);
                $path = preg_replace('/\.data$/', '', $path);
                $path = explode('.', $path);

                // Если это ошибка уникальности (например не уникальное название фильма)
                if ($cause->getConstraint() instanceof UniqueEntity) {
                    $errors->addDataAttributeError(implode('/', array_slice($path, 1)), null, $cause->getMessage(), null, null, null, 'validation');
                    continue;
                }

                // Если это ошибка в атрибутах
                if ($path[0] === 'attributes') {
                    $errors->addDataAttributeError(implode('/', array_slice($path, 1)), null, $cause->getMessage(), null, null, null, 'validation');
                    continue;
                }

                // Если это ошибка в отношениях
                if ($path[0] === 'relationships') {
                    if (count($path) === 4) {
                        $errors->addRelationshipError($path[1].'/'.$path[3], null, $cause->getMessage(), null, null, null, 'validation');
                    } else {
                        $errors->addRelationshipError($path[1], null, $cause->getMessage(), null, null, null, 'validation');
                    }
                    continue;
                }

                $errors->addDataAttributeError(implode('/', $path), null, $cause->getMessage(), null, null, null, 'validation');
            }
        }
        throw new ApiException($errors);
    }

    /**
     * Достает из запроса параметры для сериализации
     * Метод проверяет параметр include из query запроса и добавляет его к $defaultIncludes
     * Так же метод обрабатывает параметр fields из query запроса
     *
     * @param string[] $defaultIncludes Список связей(relations), которые отдаются в развернутом виде по умолчанию
     * @return EncodingParameters
     */
    protected function getEncodingParameters($defaultIncludes)
    {
        $request = $this->get('request_stack')->getCurrentRequest();

        $include = $request->query->get('include', $defaultIncludes);
        if (!is_array($include)) {
            $include = explode(',', $include);
        }

        $fields = $request->query->get('fields', []);
        foreach ($fields as $type => &$fs) {
            if (null === $fs) {
                $fs = [];
                continue;
            }

            if (!is_array($fs)) {
                $fs = explode(',', $fs);
            }
        }

        return new EncodingParameters($include, $fields);
    }
}

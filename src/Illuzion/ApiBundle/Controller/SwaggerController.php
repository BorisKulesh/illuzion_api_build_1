<?php

namespace Illuzion\ApiBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Контроллер отвечающий за отдачу документации
 *
 * @Route("/swagger", service="api.controller.swagger")
 */
class SwaggerController extends Controller
{
    /**
     * @Route("/{path}", requirements={"path"=".*"}, name="swagger")
     * @Method("GET")
     *
     * @param Request $request
     * @param string $path
     *
     * @return Response
     */
    public function listAction(Request $request, $path)
    {
        if ($this->getParameter('kernel.environment') === 'prod'
            && strpos($request->getUri(), '/v4-dev/') === false) {
            throw new NotFoundHttpException('Route not found');
        }

        try {
            $filePath = $this->get('file_locator')->locate('swagger/'.($path ?: 'index.html'));
            return $this->file($filePath, null, ResponseHeaderBag::DISPOSITION_INLINE);
        } catch (\Exception $exception) {
            throw new NotFoundHttpException('Route not found', $exception);
        }
    }
}

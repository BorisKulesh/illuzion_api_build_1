<?php

namespace Illuzion\ApiBundle\Controller;

use Illuzion\ApiBundle\Configuration\Query as Query;
use Illuzion\ApiBundle\Filter\GenreFilter;
use Illuzion\ApiBundle\Service\GenreService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Response;

/**
 * Контроллер для работы с жанрами фильмов
 *
 * @Route("/genres", service="api.controller.genre")
 */
class GenreController extends BaseController
{
    /** @var GenreService */
    protected $genreService;

    /**
     * @param GenreService $genreService
     */
    public function __construct(GenreService $genreService)
    {
        $this->genreService = $genreService;
    }

    /**
     * Получение списка жанров
     *
     * @Route("", name="api.v10.genre.list")
     * @Method("GET")
     * @Security("has_role('GENRE_VIEW')")
     *
     * // Список фильтров
     * @Query\StringParam("search", desc="Поиск по названию")
     *
     * @param GenreFilter $filter Фильтры переданные в запросе
     *
     * @return Response
     */
    public function listAction(GenreFilter $filter)
    {
        $genres = $this->genreService->getAll($filter);
        return $this->jsonapi($genres, [
            self::META_COUNT => $genres->count(),
        ]);
    }

    /**
     * Получение жанра по Id
     *
     * @Route("/{id}", name="api.v10.genre.get")
     * @Method("GET")
     * @Security("has_role('GENRE_VIEW')")
     *
     * @param string $id Id жанра
     *
     * @return Response
     */
    public function getAction($id)
    {
        $genre = $this->genreService->get($id);
        if (null === $genre) {
            return $this->jsonapiError("Жанр не найдена", 'not_found', 404);
        }

        return $this->jsonapi($genre);
    }
}

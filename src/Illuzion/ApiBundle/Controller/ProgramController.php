<?php

namespace Illuzion\ApiBundle\Controller;

use Illuzion\ApiBundle\Configuration\Query as Query;
use Illuzion\ApiBundle\Entity\Main\Program;
use Illuzion\ApiBundle\Filter\ProgramFilter;
use Illuzion\ApiBundle\Form\ProgramType;
use Illuzion\ApiBundle\Service\ProgramService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Контроллер для управления программами
 *
 * @Route("/programs", service="api.controller.program")
 */
class ProgramController extends BaseController
{
    /** @var ProgramService */
    protected $programService;

    /**
     * @param ProgramService $programService
     */
    public function __construct(ProgramService $programService)
    {
        $this->programService = $programService;
    }

    /**
     * Получение списка программ
     *
     * @Route("", name="api.v10.program.list")
     * @Method("GET")
     * @Security("has_role('PROGRAM_VIEW')")
     *
     * // Список фильтров
     * @Query\StringParam("search", desc="Поиск по названию или алиасу")
     * @Query\StringParam("city", desc="Id города")
     * @Query\BooleanParam("published", desc="Опубликованные программы")
     *
     * // Поддержка пагинации
     * @Query\PaginationParam("pagination")
     *
     * @param ProgramFilter $filter Фильтры переданные в запросе
     *
     * @return Response
     */
    public function listAction(ProgramFilter $filter)
    {
        $count = $this->programService->count($filter);
        if ($count > 0) {
            $programs = $this->programService->getAll($filter);
        } else {
            $programs = [];
        }

        return $this->jsonapi($programs, [
            self::META_COUNT => $count,
            self::META_LIMIT => $filter->getPagination()->getLimit(),
            self::META_OFFSET => $filter->getPagination()->getOffset(),
        ]);
    }

    /**
     * Получение программы по id
     *
     * @Route("/{id}", name="api.v10.program.get")
     * @Method("GET")
     * @Security("has_role('PROGRAM_VIEW')")
     *
     * @param string $id Id программы
     *
     * @return Response
     */
    public function getAction($id)
    {
        $program = $this->programService->get($id);
        if (null === $program) {
            return $this->jsonapiError("Программа не найдена", 'not_found', 404);
        }

        return $this->jsonapi($program, null, ['parent']);
    }

    /**
     * Создание программы
     *
     * @Route("", name="api.v10.program.create")
     * @Method("POST")
     * @Security("has_role('PROGRAM_CREATE')")
     *
     * @param Request $request Запрос
     *
     * @return Response
     */
    public function createAction(Request $request)
    {
        $program = new Program();

        $form = $this->createForm(ProgramType::class, $program);
        $form->submit($request->request->get('data'));
        $this->throwErrorIfFormInvalid($form);

        $this->programService->save($program);

        return $this->jsonapi($program, null, [], 201);
    }

    /**
     * Изменение программы
     *
     * @Route("/{id}", name="api.v10.program.update")
     * @Method("PATCH")
     * @Security("has_role('PROGRAM_UPDATE')")
     *
     * @param string $id Id программы
     * @param Request $request Запрос
     *
     * @return Response
     */
    public function updateAction($id, Request $request)
    {
        // Программу с id=0 нельзя изменять
        if ($id == 0) {
            return $this->jsonapiError('You are not allowed to edit this program', 'forbidden', 403);
        }

        $program = $this->programService->get($id);
        if (null === $program) {
            return $this->jsonapiError("Программа не найдена", 'not_found', 404);
        }

        $form = $this->createForm(ProgramType::class, $program);
        $form->submit($request->request->get('data'), false);
        $this->throwErrorIfFormInvalid($form);

        $this->programService->save($program);

        return $this->jsonapi($program);
    }

    /**
     * Удаление программы
     *
     * @Route("/{id}", name="api.v10.program.delete")
     * @Method("DELETE")
     * @Security("has_role('PROGRAM_DELETE')")
     *
     * @param string $id Id программы
     *
     * @return Response
     */
    public function deleteAction($id)
    {
        // Программу с id=0 нельзя удалять
        if ($id == 0) {
            return $this->jsonapiError('You are not allowed to edit this program', 'forbidden', 403);
        }

        $program = $this->programService->get($id);
        if (null === $program) {
            return $this->jsonapiError("Программа не найдена", 'not_found', 404);
        }

        $this->programService->delete($program);

        return $this->json(null, 204);
    }
}

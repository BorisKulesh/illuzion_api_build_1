<?php

namespace Illuzion\ApiBundle\Request;

/**
 * Пагинация
 */
class Pagination
{
    // Кол-во записей на страницу по умолчанию
    const DEFAULT_PAGE_SIZE = 20;

    /** @var int */
    private $limit;

    /** @var int */
    private $offset;

    /**
     * @param int $offset
     * @param int $limit
     */
    public function __construct($offset = 0, $limit = self::DEFAULT_PAGE_SIZE)
    {
        $this->limit = $limit;
        $this->offset = $offset;
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @return int
     */
    public function getOffset()
    {
        return $this->offset;
    }
}

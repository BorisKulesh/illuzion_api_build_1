<?php

namespace Illuzion\ApiBundle\Request\Params;

use Doctrine\Common\Collections\ArrayCollection;
use Illuzion\ApiBundle\Exception\ApiException;
use Neomerx\JsonApi\Exceptions\ErrorCollection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Illuzion\ApiBundle\Request\Params\Metadata\ActionMetadata;

/**
 * Парсер параметров запроса
 * Обрабатывает фильтры, сортировоки, пагинацию и прочих query параметры
 */
class RequestParser
{
    /** @var ValidatorInterface */
    protected $validator;

    /** @var ParamTransformerFactory */
    protected $transformerFactory;

    /**
     * @param ValidatorInterface $validator
     * @param ParamTransformerFactory $transformerFactory
     */
    public function __construct(
        ValidatorInterface $validator,
        ParamTransformerFactory $transformerFactory
    ) {
        $this->validator = $validator;
        $this->transformerFactory = $transformerFactory;
    }


    /**
     * Парсинг фильтров
     *
     * @param Request $request Запрос
     * @param ActionMetadata $actionMetadata Метадата правил парсинга для действия котроллера
     * @throws ApiException
     * @return array Параметры фильтра
     */
    public function parseParameters(Request $request, ActionMetadata $actionMetadata)
    {
        $sourceData = $this->parseSourceData($request, $actionMetadata);
        $parameters = $this->transformSourceData($sourceData, $actionMetadata);

        return $parameters;
    }

    /**
     * Получение и валидация исходных данных для парсинга
     *
     * @param Request $request
     * @param ActionMetadata $actionMetadata
     * @throws ApiException
     * @return array
     */
    private function parseSourceData(Request $request, ActionMetadata $actionMetadata)
    {
        // Если есть фильтр в запросе, берем данные оттдуа
        if ($request->query->has('filter')) {
            $filter = $request->query->get('filter', []);
            if (!is_array($filter)) {
                $errors = (new ErrorCollection())->addQueryParameterError('filter', 'Bad filters provided');
                throw new ApiException($errors);
            }
            $filter = new ArrayCollection($filter);
        } else {
            $filter = new ArrayCollection();
        }

        // Если есть пагинация в запросе, берем данные оттдуа
        if ($actionMetadata->pagination && $request->query->has('page')) {
            $page = $request->query->get('page', []);
            if (!is_array($page)) {
                $errors = (new ErrorCollection())->addQueryParameterError('page', 'Bad page provided');
                throw new ApiException($errors);
            }
            foreach ($page as $key => $value) {
                $filter["page[{$key}]"] = $value;
            }
        }

        // Вализация данных запроса
        $sourceData = [];
        $validationContext = $this->validator->startContext();
        foreach ($actionMetadata->sourceConstraints as $key => $constraints) {
            $sourceData[$key] = $this->getSourceValue($filter, $key, in_array($key, $actionMetadata->arraySources));

            $validationContext->atPath($key);
            $validationContext->validate($sourceData[$key], $constraints);
        }

        $errors = $validationContext->getViolations();
        if ($errors->count() > 0) {
            $collection = new ErrorCollection();
            /** @var ConstraintViolationInterface $error */
            foreach ($errors as $error) {
                $collection->addQueryParameterError($error->getPropertyPath(), $error->getMessage());
            }
            throw new ApiException($collection);
        }

        return $sourceData;
    }

    /**
     * Получение даннах запроса, с учетом того, что мы можем запрашивать массив
     * Массивы парсятся согласно 'a[]=1&a[]=2' и 'a=1,2' паттернам
     * Массивы при отсутствии значения проводятся к пустому массиву, а не null
     *
     * @param ArrayCollection $filter Данные запроса
     * @param string $key Ключ
     * @param bool $isArray Работать как с массиовм
     * @return mixed Значение
     */
    private function getSourceValue(ArrayCollection $filter, $key, $isArray = false)
    {
        $value = $filter->get($key);

        if ($isArray) {
            if (null === $value) {
                $value = [];
            } elseif (!is_array($value)) {
                $value = explode(',', $value);
            }
        }

        return $value;
    }

    /**
     * Преобразование данных запроса в параметры фильтра
     *
     * @param array $sourceData
     * @param ActionMetadata $actionMetadata
     * @throws ApiException
     * @return array
     */
    private function transformSourceData(array $sourceData, ActionMetadata $actionMetadata)
    {
        $parameters = [];
        $validationContext = $this->validator->startContext();
        foreach ($actionMetadata->transformRules as $name => $rule) {
            $transformer = $this->transformerFactory->getInitializedTransformer($rule);
            $value = $transformer->transform($sourceData);

            $validationContext->atPath($name);
            $validationContext->validate($value, $actionMetadata->valueConstraints[$name]);

            $parameters[$name] = $value;
        }

        $errors = $validationContext->getViolations();
        if ($errors->count() > 0) {
            $collection = new ErrorCollection();
            /** @var ConstraintViolationInterface $error */
            foreach ($errors as $error) {
                $collection->addQueryParameterError($error->getPropertyPath(), $error->getMessage());
            }
            throw new ApiException($collection);
        }

        return $parameters;
    }
}

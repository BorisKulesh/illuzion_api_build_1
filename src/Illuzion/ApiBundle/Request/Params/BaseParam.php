<?php

namespace Illuzion\ApiBundle\Request\Params;

use Symfony\Component\Validator\Constraint;
use Illuzion\ApiBundle\Request\Params\Metadata\TransformRule;

/**
 * Базовый класс для аннотаций фильтров
 * Аннотаций фильтров используются для парсинга GET параметров запроса
 */
abstract class BaseParam
{
    /** @var string Название параметра в фильтре */
    private $name;

    /** @var string[] Соответсвие данных неопходимых параметров к данным запроса */
    private $mapping;

    /** @var Constraint[] Список проверок параметра фильтра */
    private $constraints;

    /** @var Constraint[][] Списки проверок данных запроса */
    private $sourceConstraints = [];

    /** @var array Дополнительны данные, которые могут использоваться при трансформации */
    private $ruleOptions = [];

    /**
     * @param array $options
     */
    public function __construct(array $options)
    {
        if (!isset($options['name'])) {
            if (isset($options['value'])) {
                $this->name = $options['value'];
                unset($options['value']);
            } else {
                $this->name = 0;
            }
        } else {
            $this->name = $options['name'];
            unset($options['name']);
        }

        if (array_key_exists('mapping', $options)) {
            $this->mapping = $options['mapping'];
            unset($options['mapping']);
        } else {
            $this->mapping = [$this->camelCaseToSnake($this->name)];
        }

        if (array_key_exists('constraints', $options)) {
            $this->constraints = $options['constraints'];
            unset($options['constraints']);
        } else {
            $this->constraints = [];
        }

        unset($options['sourceConstraints']);
        unset($options['ruleOptions']);

        foreach ($options as $key => $value) {
            if (property_exists($this, $key)) {
                $this->{$key} = $value;
            }
        }
    }

    /**
     * @return string Название параметра в фильтре
     */
    final public function getName()
    {
        return $this->name;
    }

    /**
     * @return Constraint[][] Списки проверок данных запроса
     */
    final public function getSourceConstraints()
    {
        return $this->sourceConstraints;
    }

    /**
     * @return Constraint[] Список проверок параметра фильтра
     */
    final public function getValueConstraints()
    {
        return $this->constraints;
    }

    /**
     * @return TransformRule Правило используемое для преобразования данных запроса в параметр фильтра
     */
    final public function getTransformRule()
    {
        $rule = new TransformRule();
        $rule->transformBy = $this->transformBy();
        $rule->mapping = $this->mapping;
        $rule->options = $this->ruleOptions;

        return $rule;
    }

    /**
     * @return string Навание класса или сервиса, который выполняет преобразования данных запроса в параметр фильтра
     */
    protected function transformBy()
    {
        return get_class($this) . 'Transformer';
    }

    /**
     * Добавляет проверки и соответсвие для данных запроса
     * @param string|int $sourceKey Ключ, по которому будут добавляться соответсвия и проверки
     * @return string
     */
    protected function registerSource($sourceKey = 0)
    {
        if (!array_key_exists($sourceKey, $this->mapping)) {
            $this->mapping[$sourceKey] = $sourceKey;
        }

        $mappedKey = $this->mapping[$sourceKey];

        if (!array_key_exists($mappedKey, $this->sourceConstraints)) {
            $this->sourceConstraints[$mappedKey] = [];
        }

        return $mappedKey;
    }

    /**
     * Добваляет проверку для данных запроса
     * @param Constraint $constraint Проверка
     * @param mixed $sourceKey Ключ проверяемых данных
     */
    protected function addSourceConstraint($constraint, $sourceKey = 0)
    {
        $mappedKey = $this->registerSource($sourceKey);
        $this->sourceConstraints[$mappedKey][] = $constraint;
    }

    /**
     * Добавляет проверку для параметра фильтра
     * @param Constraint $constraint
     */
    protected function addValueConstraint(Constraint $constraint)
    {
        $this->constraints[] = $constraint;
    }

    /**
     * Добавить данные к правилу преобразования данных запроса в параметр фильтра
     * @param string $key
     * @param mixed $value
     */
    protected function setTransformRuleOption($key, $value)
    {
        $this->ruleOptions[$key] = $value;
    }

    /**
     * Преобразовать строку из camelCase в snake_case
     * @param string $camelCasedString
     * @return string
     */
    protected function camelCaseToSnake($camelCasedString)
    {
        return ltrim(strtolower(preg_replace('/[A-Z]/', '_$0', $camelCasedString)), '_');
    }
}

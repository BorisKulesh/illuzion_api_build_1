<?php

namespace Illuzion\ApiBundle\Request\Params;

use Illuzion\ApiBundle\Request\Params\Metadata\TransformRule;

/**
 * Базоваый класс для сервисов, которые выполняют преобразования данных запроса в параметр фильтра
 */
abstract class ParamTransformer
{
    /** @var TransformRule Правило преобразования */
    private $rule;

    /**
     * @param TransformRule $rule Правило преобразования
     */
    public function initialize(TransformRule $rule)
    {
        $this->rule = $rule;
    }

    /**
     * Преобразовать данные запроса в параметр фильтра
     *
     * @param array $sourceData данные запроса
     * @return mixed
     */
    abstract public function transform(array $sourceData);

    /**
     * Получение данных запроса по ключу с учетом соответствий
     *
     * @param array $sourceData Данные запроса
     * @param string $valueKey Ключ необходимого значения
     * @return mixed
     */
    protected function getMappedValue(array $sourceData, $valueKey = null)
    {
        if (null === $valueKey) {
            $key = reset($this->rule->mapping);
        } else {
            $key = $this->rule->mapping[$valueKey];

            if (false === $key) {
                return null;
            }
        }

        return self::value($sourceData, $key);
    }

    /**
     * Получение опции преобразования
     *
     * @param string $key Ключ опции
     * @param mixed $defaultValue Значение по умолчанию
     * @return mixed Опция
     */
    protected function getOption($key, $defaultValue = null)
    {
        return self::value($this->rule->options, $key, $defaultValue);
    }

    /**
     * Вспомогательный метод для безопасного получения значения из массива
     *
     * @param array $array Массив
     * @param string $key Ключ
     * @param mixed $defaultValue Значение по умолчанию
     * @return mixed Значение
     */
    protected static function value(array $array, $key, $defaultValue = null)
    {
        return array_key_exists($key, $array) ? $array[$key] : $defaultValue;
    }
}

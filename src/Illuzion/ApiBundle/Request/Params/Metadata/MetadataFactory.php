<?php

namespace Illuzion\ApiBundle\Request\Params\Metadata;

use Doctrine\Common\Annotations\Reader;
use Doctrine\Common\Cache\Cache;
use Illuzion\ApiBundle\Request\Params\BaseParam;
use Illuzion\ApiBundle\Request\BaseFilter;

/**
 * Парсит аннотаци действий контроллеров и собирает метадату для фильтра
 */
class MetadataFactory
{
    /** @var Cache Кеш для метадаты */
    private $cache;

    /** @var Reader */
    private $annotationReader;

    /**
     * @param Reader $annotationReader
     */
    public function __construct(Reader $annotationReader)
    {
        $this->annotationReader = $annotationReader;
    }

    /**
     * @param Cache $cache
     */
    public function setCache(Cache $cache)
    {
        $this->cache = $cache;
    }

    /**
     * Получить металату для действия контроллера
     *
     * @param array $controller
     * @return ActionMetadata
     */
    public function getMetadataFor(array $controller)
    {
        $class = get_class($controller[0]);
        $method = $controller[1];
        $cacheKey = "ActionMetadata({$class}::{$method})";

        if (null !== $this->cache && false !== ($metadata = $this->cache->fetch($cacheKey))) {
            return $metadata;
        }

        $builder = new MetadataBuilder();
        $reflectionMethod = new \ReflectionMethod($class, $method);

        foreach ($this->annotationReader->getMethodAnnotations($reflectionMethod) as $annotation) {
            if ($annotation instanceof BaseParam) {
                $builder->addParamAnnotation($annotation);
            }
        }

        foreach ($reflectionMethod->getParameters() as $reflectionParameter) {
            $class = $reflectionParameter->getClass();
            if (null === $class) {
                continue;
            }

            if ($reflectionParameter->getName() === 'filter' && $class->isSubclassOf(BaseFilter::class)) {
                $builder->setFilterClass($class->getName());
            }
        }

        $metadata = $builder->build();

        if (null !== $this->cache) {
            $this->cache->save($cacheKey, $metadata);
        }

        return $metadata;
    }
}

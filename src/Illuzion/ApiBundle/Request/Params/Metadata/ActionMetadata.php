<?php

namespace Illuzion\ApiBundle\Request\Params\Metadata;

use Illuzion\ApiBundle\Request\BaseFilter;
use Symfony\Component\Validator\Constraint;

/**
 * Метадата действия контроллера
 */
class ActionMetadata
{
    /** @var Constraint[][] Списки проверок данных запроса */
    public $sourceConstraints = [];

    /** @var TransformRule[] Набор правил преобразования */
    public $transformRules = [];

    /** @var Constraint[] Список проверок параметров фильтра */
    public $valueConstraints = [];

    /** @var string Список параметров, которые являются массивом */
    public $arraySources = [];

    /** @var string Класс собираемого фильтра */
    public $filterClass = BaseFilter::class;

    /** @var bool Добавить пагинацию в фильтр */
    public $pagination = false;

    /** @var bool Добавить сортировку в фильтр */
    public $sorting = false;
}

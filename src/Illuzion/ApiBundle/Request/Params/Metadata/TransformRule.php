<?php

namespace Illuzion\ApiBundle\Request\Params\Metadata;

/**
 * Правило используемое для преобразования данных запроса в параметр фильтра
 */
class TransformRule
{
    /** @var string Навание класса или сервиса, который выполняет преобразования данных запроса в параметр фильтра */
    public $transformBy;

    /** @var string[] Соответсвие данных неопходимых параметров к данным запроса */
    public $mapping;

    /** @var array Дополнительны данные, которые могут использоваться при трансформации */
    public $options;
}

<?php

namespace Illuzion\ApiBundle\Request\Params;

use Symfony\Component\Validator\Constraints\NotNull;

/**
 * Базовы класс для аннотаций фильтров скалярных парамоетров (одно поле в запросе => один параметр в фильтре)
 */
abstract class ScalarParam extends BaseParam
{
    // Названия опции преобразования для значения по умолчанию
    const OPTION_DEFAULT = 'default';

    /** @var mixed Значение поу молчанию */
    public $default = null;

    /** @var bool Фильтр является обязательным */
    public $required = false;

    /** @var string Описание фильтра */
    private $desc;

    /**
     * @param array $options
     */
    public function __construct(array $options)
    {
        parent::__construct($options);
        $this->setTransformRuleOption(self::OPTION_DEFAULT, $this->default);
        if ($this->required) {
            $this->addSourceConstraint(new NotNull());
        } else {
            $this->registerSource();
        }
    }
}

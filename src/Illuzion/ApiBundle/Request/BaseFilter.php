<?php

namespace Illuzion\ApiBundle\Request;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Базовый класс для всех фильтров, которые поддерживают сборку из GET параметров запроса
 */
class BaseFilter extends ArrayCollection
{
    public function getHash()
    {
        $arr = $this->toArray();
        array_multisort($arr);
        return md5(json_encode($arr));
    }
}

<?php

namespace Illuzion\ApiBundle\Serializer\Schema;

use Illuzion\ApiBundle\Entity\Main\Hall;
use Illuzion\ApiBundle\Service\CinemaService;
use Neomerx\JsonApi\Contracts\Schema\SchemaFactoryInterface;

/**
 * Схема сериализации залов
 */
class HallSchema extends AbstractScheme
{
    /** @var CinemaService */
    protected $cinemaService;

    /**
     * @param SchemaFactoryInterface $factory
     * @param CinemaService $cinemaService
     */
    public function __construct(SchemaFactoryInterface $factory, CinemaService $cinemaService)
    {
        $this->resourceType = 'halls';
        $this->cinemaService = $cinemaService;
        parent::__construct($factory);
    }

    /**
     * @inheritdoc
     */
    public function getSupportedClass()
    {
        return Hall::class;
    }

    /**
     * @inheritdoc
     * @param Hall $resource
     */
    public function getId($resource)
    {
        return $resource->getPublicId();
    }

    /**
     * @inheritdoc
     * @param Hall $resource
     */
    public function getAttributes($resource)
    {
        return [
            'title' => $resource->getTitle(),
            'updated_at' => $this->formatDate($resource->getUpdatedAt()),
        ];
    }

    /**
     * @inheritdoc
     * @param Hall $resource
     */
    public function getRelationships($resource, $isPrimary, array $includeRelationships)
    {
        return [
            'cinema' => [
                self::DATA => $this->cinemaService->get($resource->getCinemaId()),
            ]
        ];
    }
}

<?php

namespace Illuzion\ApiBundle\Serializer\Schema;

use Illuzion\ApiBundle\Entity\Shared\Article;
use Neomerx\JsonApi\Contracts\Schema\SchemaFactoryInterface;

/**
 * Схема сериализации новостей
 */
class ArticleSchema extends AbstractScheme
{
    /** @var string */
    protected $imageUrlPrefix;

    /**
     * @param SchemaFactoryInterface $factory
     * @param string $imageUrlPrefix
     */
    public function __construct(SchemaFactoryInterface $factory, $imageUrlPrefix)
    {
        $this->resourceType = 'news';
        $this->imageUrlPrefix = $imageUrlPrefix;
        parent::__construct($factory);
    }

    /**
     * @inheritdoc
     */
    public function getSupportedClass()
    {
        return Article::class;
    }

    /**
     * @inheritdoc
     * @param Article $resource
     */
    public function getId($resource)
    {
        return $resource->getId();
    }

    /**
     * @inheritdoc
     * @param Article $resource
     */
    public function getAttributes($resource)
    {
        return [
            'title' => $resource->getTitle(),
            'alias' => $resource->getAlias(),
            'datetime' => $this->formatDate($resource->getDatetime()),
            'text_short' => $resource->getTextShort(),
            'text_full' => $resource->getTextFull(),
            'cover_url' => $this->formatUrl(
                $this->imageUrlPrefix,
                $resource->getCoverUrl()
            ),
            'published' => $resource->isPublished(),
            'views' => $resource->getViews(),
            'updated_at' => $this->formatDate($resource->getUpdatedAt()),
        ];
    }

    /**
     * @inheritdoc
     * @param Article $resource
     */
    public function getRelationships($resource, $isPrimary, array $includeRelationships)
    {
        return [
            'city' => [
                self::DATA => $resource->getCity(),
            ]
        ];
    }
}

<?php

namespace Illuzion\ApiBundle\Serializer\Schema;

use Illuzion\ApiBundle\Entity\Main\Ticket;
use Illuzion\ApiBundle\Service\CinemaService;
use Neomerx\JsonApi\Contracts\Schema\SchemaFactoryInterface;

/**
 * Схема сериализации билетов
 */
class TicketSchema extends AbstractScheme
{
    /** @var CinemaService */
    protected $cinemaService;

    /**
     * @param SchemaFactoryInterface $factory
     * @param CinemaService $cinemaService
     */
    public function __construct(SchemaFactoryInterface $factory, CinemaService $cinemaService)
    {
        $this->resourceType = 'tickets';
        $this->cinemaService = $cinemaService;
        parent::__construct($factory);
    }

    /**
     * @inheritdoc
     */
    public function getSupportedClass()
    {
        return Ticket::class;
    }

    /**
     * @inheritdoc
     * @param Ticket $resource
     */
    public function getId($resource)
    {
        return $resource->getPublicId();
    }

    /**
     * @inheritdoc
     * @param Ticket $resource
     */
    public function getAttributes($resource)
    {
        return [
            'row' => $resource->getSeat()->getRow(),
            'place' => $resource->getSeat()->getPlace(),
            'returned' => $resource->getReturned(),
            'printed' => $resource->getOrder()->getPrinted() !== null,
        ];
    }

    /**
     * @inheritdoc
     * @param Ticket $resource
     */
    public function getRelationships($resource, $isPrimary, array $includeRelationships)
    {
        return [
            'show' => [
                self::DATA => $resource->getShow(),
            ],
            'hall' => [
                self::DATA => $resource->getSeat()->getHall(),
            ],
            'cinema' => [
                self::DATA => $this->cinemaService->get($resource->getCinemaId()),
            ],
            'order' => [
                self::DATA => $resource->getOrder(),
            ],
        ];
    }
}

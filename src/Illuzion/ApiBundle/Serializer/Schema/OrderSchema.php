<?php

namespace Illuzion\ApiBundle\Serializer\Schema;

use Illuzion\ApiBundle\Entity\Main\Order;
use Neomerx\JsonApi\Contracts\Schema\SchemaFactoryInterface;

/**
 * Схема сериализации заказов
 */
class OrderSchema extends AbstractScheme
{
    /**
     * @param SchemaFactoryInterface $factory
     */
    public function __construct(SchemaFactoryInterface $factory)
    {
        $this->resourceType = 'orders';
        parent::__construct($factory);
    }

    /**
     * @inheritdoc
     */
    public function getSupportedClass()
    {
        return Order::class;
    }

    /**
     * @inheritdoc
     * @param Order $resource
     */
    public function getId($resource)
    {
        return $resource->getPublicId();
    }

    /**
     * @inheritdoc
     * @param Order $resource
     */
    public function getAttributes($resource)
    {
        return [
            'status' => $resource->getStatusStr(),
            'payment_type' => $resource->getPaymentType(),
            'total_cost' => $resource->getTotalCost(),
            'purchase_date' => $this->formatDate($resource->getPurchaseDate()),
            'email' => $resource->getEmail(),
            'phone' => $resource->getPhone(),
            'client' => $resource->getClient(),
            'transaction_id' => $resource->getTransactionId(),
            'print_code' => $resource->getPrintCode(),
            'printed' => $this->formatDate($resource->getPrinted()),
            'credit_card_number' => $resource->getCreditCardNumber(),
            'credit_card_holder' => $resource->getCreditCardHolder(),
            'last_modified_date' => $this->formatDate($resource->getLastModifiedDate()),
            'customer' => $resource->getCustomerId(),
        ];
    }

    /**
     * @inheritdoc
     * @param Order $resource
     */
    public function getRelationships($resource, $isPrimary, array $includeRelationships)
    {
        return [
            'tickets' => [
                self::DATA => $resource->getTickets(),
            ],
            'show' => [
                self::DATA => $resource->getShow(),
            ],
        ];
    }
}

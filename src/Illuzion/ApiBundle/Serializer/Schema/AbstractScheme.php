<?php

namespace Illuzion\ApiBundle\Serializer\Schema;

use Neomerx\JsonApi\Schema\SchemaProvider;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

/**
 * Базоваый класс для всех схем сериализации
 */
abstract class AbstractScheme extends SchemaProvider
{
    /** @var AuthorizationChecker */
    protected $authChecker;

    /**
     * @return string Имя класса, который эта схема обрабатывает
     */
    abstract public function getSupportedClass();

    /**
     * Вспомогательный метод для форматирование дат
     *
     * @param \DateTime|null $dateTime
     * @return null|string
     */
    protected function formatDate(\DateTime $dateTime = null) {
        return null !== $dateTime ? $dateTime->format('Y-m-d H:i:s') : null;
    }

    /**
     * Вспомогательный метод для форматирования урлов картинок
     *
     * @param string $base База пути до картинки
     * @param string $path Остаток пути до картинки
     *
     * @return string
     */
    protected function formatUrl($base, $path)
    {
        if (empty($path)) {
            return null;
        }

        $path = trim($path);

        if (stripos($path, 'http') === 0 || strpos($path, '//') === 0) {
            return $path;
        }

        if (strpos($path, '/') !== 0) {
            $path = '/'.$path;
        }

        return $base.$path;
    }

    public function setAuthChecker(AuthorizationChecker $authChecker)
    {
        $this->authChecker = $authChecker;
    }
}

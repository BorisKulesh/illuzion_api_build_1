<?php

namespace Illuzion\ApiBundle\Serializer\Schema;

use Illuzion\ApiBundle\Entity\Main\Distributor;
use Neomerx\JsonApi\Contracts\Schema\SchemaFactoryInterface;

/**
 * Схема сериализации прокатчиков
 */
class DistributorSchema extends AbstractScheme
{
    /**
     * @param SchemaFactoryInterface $factory
     */
    public function __construct(SchemaFactoryInterface $factory)
    {
        $this->resourceType = 'distributors';
        parent::__construct($factory);
    }

    /**
     * @inheritdoc
     */
    public function getSupportedClass()
    {
        return Distributor::class;
    }

    /**
     * @inheritdoc
     * @param Distributor $resource
     */
    public function getId($resource)
    {
        return $resource->getId();
    }

    /**
     * @inheritdoc
     * @param Distributor $resource
     */
    public function getAttributes($resource)
    {
        return [
            'title' => $resource->getTitle(),
            'emails' => $resource->getEmails(),
        ];
    }
}

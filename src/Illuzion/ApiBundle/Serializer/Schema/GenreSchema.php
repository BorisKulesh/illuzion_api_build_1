<?php

namespace Illuzion\ApiBundle\Serializer\Schema;

use Illuzion\ApiBundle\Entity\Main\Genre;
use Neomerx\JsonApi\Contracts\Schema\SchemaFactoryInterface;

/**
 * Схема сериализации жанров
 */
class GenreSchema extends AbstractScheme
{
    /**
     * @param SchemaFactoryInterface $factory
     */
    public function __construct(SchemaFactoryInterface $factory)
    {
        $this->resourceType = 'genres';
        parent::__construct($factory);
    }

    /**
     * @inheritdoc
     */
    public function getSupportedClass()
    {
        return Genre::class;
    }

    /**
     * @inheritdoc
     * @param Genre $resource
     */
    public function getId($resource)
    {
        return $resource->getId();
    }

    /**
     * @inheritdoc
     * @param Genre $resource
     */
    public function getAttributes($resource)
    {
        return [
            'title' => $resource->getTitle(),
        ];
    }
}

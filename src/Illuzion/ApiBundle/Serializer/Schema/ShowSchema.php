<?php

namespace Illuzion\ApiBundle\Serializer\Schema;

use Illuzion\ApiBundle\Entity\Main\Show;
use Illuzion\ApiBundle\Security\ApiClient;
use Illuzion\ApiBundle\Service\PlaceTypePriceService;
use Neomerx\JsonApi\Contracts\Schema\SchemaFactoryInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

/**
 * Схема сериализации сеансов
 */
class ShowSchema extends AbstractScheme
{
    /** @var PlaceTypePriceService */
    protected $placeTypePriceService;

    /** @var TokenStorage */
    protected $tokenStorage;

    /**
     * @param SchemaFactoryInterface $factory
     * @param PlaceTypePriceService $placeTypePriceService
     * @param TokenStorage $tokenStorage
     */
    public function __construct(
        SchemaFactoryInterface $factory,
        PlaceTypePriceService $placeTypePriceService,
        TokenStorage $tokenStorage
    ) {
        $this->resourceType = 'shows';
        $this->placeTypePriceService = $placeTypePriceService;
        $this->tokenStorage = $tokenStorage;
        parent::__construct($factory);
    }

    /**
     * @inheritdoc
     */
    public function getSupportedClass()
    {
        return Show::class;
    }

    /**
     * @inheritdoc
     * @param Show $resource
     */
    public function getId($resource)
    {
        return $resource->getPublicId();
    }

    /**
     * @inheritdoc
     * @param Show $resource
     */
    public function getAttributes($resource)
    {
        $priceSchemes = $this->placeTypePriceService->getPriceSchemes($resource, $this->getClient());
        $promos = [];
        if (array_key_exists(PlaceTypePriceService::TYPE_BONUS, $priceSchemes)) {
            $promos[] = PlaceTypePriceService::TYPE_BONUS;
        }

        return [
            'datetime' => $this->formatDate($resource->getDatetime()),
            'prices' => $priceSchemes,
            'format' => $resource->getFormat() ? $resource->getFormat()->getTitle() : null,
            'promos' => $promos,
            'bonus_buy_allowed' => $resource->isBonusBuyAllowed(),
            'published' => $resource->isPublished(),
            'updated_at' => $this->formatDate($resource->getUpdatedAt()),
        ];
    }



    /**
     * @return null|ApiClient
     */
    protected function getClient()
    {
        if (null === $token = $this->tokenStorage->getToken()) {
            return null;
        }

        if (!is_object($user = $token->getUser()) || !($user instanceof ApiClient)) {
            return null;
        }

        return $user;
    }

    /**
     * @inheritdoc
     * @param Show $resource
     */
    public function getRelationships($resource, $isPrimary, array $includeRelationships)
    {
        return [
            'hall' => [
                self::DATA => $resource->getHall(),
            ],
            'movie' => [
                self::DATA => $resource->getMovie(),
            ],
        ];
    }
}

<?php

namespace Illuzion\ApiBundle\Serializer\Schema;

use Illuzion\ApiBundle\Entity\Shared\City;
use Neomerx\JsonApi\Contracts\Schema\SchemaFactoryInterface;

/**
 * Схема сериализации городов
 */
class CitySchema extends AbstractScheme
{
    /**
     * @param SchemaFactoryInterface $factory
     */
    public function __construct(SchemaFactoryInterface $factory)
    {
        $this->resourceType = 'cities';
        parent::__construct($factory);
    }

    /**
     * @inheritdoc
     */
    public function getSupportedClass()
    {
        return City::class;
    }

    /**
     * @inheritdoc
     * @param City $resource
     */
    public function getId($resource)
    {
        return $resource->getId();
    }

    /**
     * @inheritdoc
     * @param City $resource
     */
    public function getAttributes($resource)
    {
        return [
            'name' => $resource->getName(),
        ];
    }

    /**
     * @inheritdoc
     * @param City $resource
     */
    public function getRelationships($resource, $isPrimary, array $includeRelationships)
    {
        return [
            'cinemas' => [
                self::DATA => $resource->getCinemas(),
            ]
        ];
    }
}

<?php

namespace Illuzion\ApiBundle\Serializer\Schema;

use Illuzion\ApiBundle\Entity\Shared\Cinema;
use Illuzion\ApiBundle\Service\HallService;
use Neomerx\JsonApi\Contracts\Schema\SchemaFactoryInterface;

/**
 * Схема сериализации кинотеатров
 */
class CinemaSchema extends AbstractScheme
{
    /** @var HallService */
    protected $hallService;

    /**
     * @param SchemaFactoryInterface $factory
     * @param HallService $hallService
     */
    public function __construct(SchemaFactoryInterface $factory, HallService $hallService)
    {
        $this->resourceType = 'cinemas';
        $this->hallService = $hallService;
        parent::__construct($factory);
    }

    /**
     * @inheritdoc
     */
    public function getSupportedClass()
    {
        return Cinema::class;
    }

    /**
     * @inheritdoc
     * @param Cinema $resource
     */
    public function getId($resource)
    {
        return $resource->getId();
    }

    /**
     * @inheritdoc
     * @param Cinema $resource
     */
    public function getAttributes($resource)
    {
        return [
            'title' => $resource->getTitle(),
            'page' => $resource->getPage(),
            'alias' => $resource->getAlias(),
        ];
    }

    /**
     * @inheritdoc
     * @param Cinema $resource
     */
    public function getRelationships($resource, $isPrimary, array $includeRelationships)
    {
        return [
            'city' => [
                self::DATA => $resource->getCity(),
            ],
            'halls' => [
                self::DATA => $this->hallService->getAllForCinema($resource->getId()),
            ],
        ];
    }
}

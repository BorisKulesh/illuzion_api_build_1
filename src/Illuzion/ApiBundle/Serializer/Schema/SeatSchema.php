<?php

namespace Illuzion\ApiBundle\Serializer\Schema;

use Illuzion\ApiBundle\Entity\Main\Seat;
use Neomerx\JsonApi\Contracts\Schema\SchemaFactoryInterface;

/**
 * Схема сериализации мест
 */
class SeatSchema extends AbstractScheme
{
    /**
     * @param SchemaFactoryInterface $factory
     */
    public function __construct(SchemaFactoryInterface $factory)
    {
        $this->resourceType = 'seats';
        parent::__construct($factory);
    }

    /**
     * @inheritdoc
     */
    public function getSupportedClass()
    {
        return Seat::class;
    }

    /**
     * @inheritdoc
     * @param Seat $resource
     */
    public function getId($resource)
    {
        return $resource->getId();
    }

    /**
     * @inheritdoc
     * @param Seat $resource
     */
    public function getAttributes($resource)
    {
        return [
            'row' => $resource->getRow(),
            'place' => $resource->getPlace(),
            'left_top_x' => $resource->getLeftTopX(),
            'left_top_y' => $resource->getLeftTopY(),
            'right_bottom_x' => $resource->getRightBottomX(),
            'right_bottom_y' => $resource->getRightBottomY(),
            'seat_type' => $resource->getType(),
        ];


    }

    /**
     * @inheritdoc
     */
    public function getResourceLinks($resource)
    {
        return [];
    }
}
<?php

namespace Illuzion\ApiBundle\Serializer\Schema;

use Illuzion\ApiBundle\Entity\Main\Movie;
use Illuzion\ApiBundle\Service\ReleaseDateService;
use Neomerx\JsonApi\Contracts\Schema\SchemaFactoryInterface;

/**
 * Схема сериализации фильмов
 */
class MovieSchema extends AbstractScheme
{
    /** @var ReleaseDateService */
    protected $releaseDateService;

    /** @var string */
    protected $imageUrlPrefix;

    /**
     * @param SchemaFactoryInterface $factory
     * @param ReleaseDateService $releaseDateService
     * @param string $imageUrlPrefix
     */
    public function __construct(
        SchemaFactoryInterface $factory,
        ReleaseDateService $releaseDateService,
        $imageUrlPrefix
    )  {
        $this->resourceType = 'movies';
        $this->releaseDateService = $releaseDateService;
        $this->imageUrlPrefix = $imageUrlPrefix;
        parent::__construct($factory);
    }

    /**
     * @inheritdoc
     */
    public function getSupportedClass()
    {
        return Movie::class;
    }

    /**
     * @inheritdoc
     * @param Movie $resource
     */
    public function getId($resource)
    {
        return $resource->getId();
    }

    /**
     * @inheritdoc
     * @param Movie $resource
     */
    public function getAttributes($resource)
    {
        return [
            'title' => $resource->getTitle(),
            'alias' => $resource->getAlias(),
            'archived' => $resource->isArchived(),
            'published' => $resource->isPublished(),
            'format' => $resource->getFormatsArr(),
            'start_dates' => $this->releaseDateService->getCityDateMapForMovie($resource->getId()),
            'director' => $resource->getDirector(),
            'runtime' => $resource->getRuntime(),
            'licence_number' => $resource->getLicenceNumber(),
            'year' => $resource->getYear(),
            'countries' => $resource->getCountries(),
            'subtitles' => $resource->getSubtitles(),
            'pg' => (int) str_replace('+', '', $resource->getPg()),
            'actors' => $resource->getActors(),
            'description' => $resource->getDescription(),
            'language' => $resource->getLanguage(),
            'trailer_url' => $resource->getTrailerUrl(),
            'poster_url' => $this->formatUrl(
                $this->imageUrlPrefix,
                $resource->getPosterUrl()
            ),
            'cover_url' => $this->formatUrl(
                $this->imageUrlPrefix,
                $resource->getCoverUrl()
            ),
            'updated_at' => $this->formatDate($resource->getUpdatedAt()),
        ];
    }

    /**
     * @inheritdoc
     * @param Movie $resource
     */
    public function getRelationships($resource, $isPrimary, array $includeRelationships)
    {
        return [
            'distributor' => [
                self::DATA => // $resource->getDistributor(),
                    $resource->getDistributor() !== null && $resource->getDistributor()->getId() !== 0
                        ? $resource->getDistributor()
                        : null,
            ],
            'program' => [
                self::DATA => $resource->getProgram(),
            ],
            'genres' => [
                self::DATA => $resource->getGenres(),
            ],
        ];
    }
}

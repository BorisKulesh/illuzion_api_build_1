<?php

namespace Illuzion\ApiBundle\Serializer\Schema;

use Illuzion\ApiBundle\Entity\Main\Program;
use Illuzion\ApiBundle\Service\CityService;
use Neomerx\JsonApi\Contracts\Schema\SchemaFactoryInterface;

/**
 * Схема сериализации программ
 */
class ProgramSchema extends AbstractScheme
{
    /** @var string */
    protected $imageUrlPrefix;

    /** @var CityService */
    protected $cityService;

    /**
     * @param SchemaFactoryInterface $factory
     * @param CityService $cityService
     * @param string $imageUrlPrefix
     */
    public function __construct(
        SchemaFactoryInterface $factory,
        CityService $cityService,
        $imageUrlPrefix
    ) {
        $this->resourceType = 'programs';
        $this->cityService = $cityService;
        $this->imageUrlPrefix = $imageUrlPrefix;
        parent::__construct($factory);
    }

    /**
     * @inheritdoc
     */
    public function getSupportedClass()
    {
        return Program::class;
    }

    /**
     * @inheritdoc
     * @param Program $resource
     */
    public function getId($resource)
    {
        return $resource->getId();
    }

    /**
     * @inheritdoc
     * @param Program $resource
     */
    public function getAttributes($resource)
    {
        return [
            'title' => $resource->getTitle(),
            'alias' => $resource->getAlias(),
            'published' => $resource->isPublished(),
            'subtitle' => $resource->getSubtitle(),
            'description_short' => $resource->getDescriptionShort(),
            'description_full' => $resource->getDescriptionFull(),
            'poster_url' => $this->formatUrl(
                $this->imageUrlPrefix,
                $resource->getPosterUrl()
            ),
            'cover_url' => $this->formatUrl(
                $this->imageUrlPrefix,
                $resource->getCoverUrl()
            ),
            'updated_at' => $this->formatDate($resource->getUpdatedAt()),
        ];
    }

    /**
     * @inheritdoc
     * @param Program $resource
     */
    public function getRelationships($resource, $isPrimary, array $includeRelationships)
    {
        $city = null !== $resource->getCityId()
            ? $this->cityService->get($resource->getCityId())
            : null;

        return [
            'parent' => [
                self::DATA => $resource->getParent(),
            ],
            'city' => [
                self::DATA => $city,
            ],
        ];
    }
}

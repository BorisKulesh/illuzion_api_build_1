<?php

namespace Illuzion\ApiBundle\Serializer\Schema;

use Illuzion\ApiBundle\Entity\Customer\Customer;
use Neomerx\JsonApi\Contracts\Schema\SchemaFactoryInterface;

/**
 * Схема сериализации пользователей
 */
class CustomerSchema extends AbstractScheme
{
    /**
     * @param SchemaFactoryInterface $factory
     */
    public function __construct(SchemaFactoryInterface $factory)
    {
        $this->resourceType = 'customers';
        parent::__construct($factory);
    }

    /**
     * @inheritdoc
     */
    public function getSupportedClass()
    {
        return Customer::class;
    }

    /**
     * @inheritdoc
     * @param Customer $resource
     */
    public function getId($resource)
    {
        return $resource->getId();
    }

    /**
     * @inheritdoc
     * @param Customer $resource
     */
    public function getAttributes($resource)
    {
        $result = [
            'email' => $resource->getEmail() ?: null,
            'email_new' => $resource->getEmailNew(),
            'email_confirmed' => $resource->isEmailConfirmed(),
            'email_confirmation_sent_date' => $this->formatDate($resource->getEmailConfirmationSentDate()),
            'email_newsletter' => $resource->isEmailNewsletter(),
            'phone' => $resource->getPhone() ?: null,
            'phone_new' => $resource->getPhoneNew(),
            'phone_confirmed' => $resource->isPhoneConfirmed(),
            'phone_confirmation_sent_date' => $this->formatDate($resource->getPhoneConfirmationSentDate()),
            'birthday' => $this->formatDate($resource->getBirthday()),
            'name_first' => $resource->getNameFirst(),
            'name_middle' => $resource->getNameMiddle(),
            'name_last' => $resource->getNameLast(),
            'sms_notifications' => $resource->isSmsNotifications(),
            'activated' => false,
            'blocked' => null,
            'card' => null,
            'bonus' => null,
            'updated_at' => $this->formatDate($resource->getUpdatedAt()),
        ];

        if (null !== ($card = $resource->getMainCard())) {
            $result['blocked'] = $card->getActivityLevel() !== 1;
            $result['activated'] = !$result['blocked'] && $resource->isUserActivate();
            $result['card'] = $card->getNumber();
            $result['bonus'] = $card->getBonus();
        }

        return $result;
    }
}

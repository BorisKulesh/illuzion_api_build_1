<?php

namespace Illuzion\ApiBundle\Serializer;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Util\ClassUtils;
use Illuzion\ApiBundle\Serializer\Schema\AbstractScheme;
use Neomerx\JsonApi\Contracts\Schema\ContainerInterface;
use Neomerx\JsonApi\Contracts\Schema\SchemaProviderInterface;
use \Neomerx\JsonApi\I18n\Translator as T;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

/**
 * Хранилище схем сериализации
 */
class Container implements ContainerInterface
{
    /** @var AuthorizationChecker */
    protected $authChecker;

    /** @var SchemaProviderInterface[]|ArrayCollection Соответствие класса сущности схеме сериализации */
    protected $providersByType;

    /** @var SchemaProviderInterface[]|ArrayCollection Соответствие типа сущности схеме сериализации */
    protected $providersByResource;

    public function __construct(AuthorizationChecker $authorizationChecker)
    {
        $this->authChecker = $authorizationChecker;
        $this->providersByType = new ArrayCollection();
        $this->providersByResource = new ArrayCollection();
    }

    /**
     * Добавить новую схему сериализации
     *
     * @param AbstractScheme $provider Схема сериализации
     */
    public function register(AbstractScheme $provider)
    {
        $provider->setAuthChecker($this->authChecker);
        $this->providersByType->set($provider->getSupportedClass(), $provider);
        $this->providersByResource->set($provider->getResourceType(), $provider);
    }

    /**
     * @inheritdoc
     */
    public function getSchema($resourceObject)
    {
        $type = ClassUtils::getRealClass(get_class($resourceObject));
        return $this->getSchemaByType($type);
    }

    /**
     * @inheritdoc
     */
    public function getSchemaByType($type)
    {
        if (!$this->providersByType->containsKey($type)) {
            throw new \InvalidArgumentException(T::t('Schema is not registered for type \'%s\'.', [$type]));
        }

        return $this->providersByType->get($type);
    }

    /**
     * @inheritdoc
     */
    public function getSchemaByResourceType($resourceType)
    {
        if (!$this->providersByResource->containsKey($resourceType)) {
            throw new \InvalidArgumentException(T::t(
                'Schema is not registered for resource type \'%s\'.',
                [$resourceType]
            ));
        }

        return $this->providersByResource->get($resourceType);
    }
}

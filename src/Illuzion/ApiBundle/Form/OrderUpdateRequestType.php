<?php

namespace Illuzion\ApiBundle\Form;

use Illuzion\ApiBundle\Form\Common\JsonApiFormType;
use Illuzion\ApiBundle\Model\OrderUpdateRequest;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

/**
 * Форма для изменения и оплаты заказа
 */
class OrderUpdateRequestType extends JsonApiFormType
{
    /**
     * @inheritdoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->attributesBuilder($builder)
            ->add('status', TextType::class, [
                'constraints' => [
                    new Choice([
                        'choices' => ['paid', 'reserve_released', 'full_returned', null, ''],
                        'message' => 'Некорректное значение поля',
                    ]),
                ]
            ])
            ->add('email', TextType::class, [
                'constraints' => [
                    new Email(['message' => 'Некорректный формат почты'])
                ],
            ])
            ->add('phone', TextType::class, [
                'constraints' => [new Regex([
                    'pattern' => '/^9\d{9}$/',
                    'message' => 'Некорректный формат телефона'
                ])]
            ])
            ->addEventListener(FormEvents::PRE_SUBMIT, [$this, 'handlePreSubmit']);
    }

    /**
     * Проверяем пришедший статус заказа и добавляем необзодимые поля в форму
     *
     * @param FormEvent $event
     */
    public function handlePreSubmit(FormEvent $event)
    {
        $data = $event->getData();
        if (!is_array($data) || !array_key_exists('status', $data)) {
            return;
        }

        if ($data['status'] === 'paid') {
            $event->getForm()
                ->add('payment_type', TextType::class, [
                    'constraints' => [
                        new NotBlank(),
                        new Choice([
                            'choices' => ['farpost', 'online-payment', 'card', 'bonus'],
                            'message' => 'Некорректное значение поля. Допустимые значения: farpost, online-payment, card, bonus',
                        ]),
                    ]
                ])
                ->add('customer', TextType::class)
                ->add('transaction_id', TextType::class)
                ->add('credit_card_number', TextType::class)
                ->add('credit_card_holder', TextType::class)
            ;
        }
    }

    /**
     * @inheritdoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => OrderUpdateRequest::class,
            'csrf_protection' => false,
            'allow_extra_fields' => true,
        ]);
    }
}

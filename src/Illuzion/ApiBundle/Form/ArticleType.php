<?php

namespace Illuzion\ApiBundle\Form;

use Illuzion\ApiBundle\Entity\Shared\Article;
use Illuzion\ApiBundle\Entity\Shared\City;
use Illuzion\ApiBundle\Form\Common\BooleanType;
use Illuzion\ApiBundle\Form\Common\DateTimeType;
use Illuzion\ApiBundle\Form\Common\JsonApiFormType;
use Illuzion\ApiBundle\Form\Common\RelationType;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Regex;

/**
 * Форма для создания и редактирования новостей
 */
class ArticleType extends JsonApiFormType
{
    /**
     * @inheritdoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->attributesBuilder($builder)
            ->add('title', TextType::class, [
                'constraints' => [
                    new NotBlank(['message' => 'Это поле не дожно быть пустым'])
                ]])
            ->add('alias', TextType::class, [
                'constraints' => [
                    new NotBlank(['message' => 'Это поле не дожно быть пустым']),
                    new Regex([
                        'pattern' => '/^[a-zA-Z0-9-]+$/',
                        'message' => 'Поле содержит некорректные символы'
                    ])
                ]
            ])
            ->add('datetime', DateTimeType::class, [
                'constraints' => [
                    new NotNull(['message' => 'Это поле не дожно быть пустым'])
                ]
            ])
            ->add('text_short', TextType::class)
            ->add('text_full', TextType::class)
            ->add('cover_url', TextType::class)
            ->add('published', BooleanType::class)
            ->add('views', IntegerType::class);

        $this->relationsBuilder($builder)
            ->add('city', RelationType::class, [
                'class' => City::class,
                'constraints' => [
                    new NotNull(['message' => 'Это поле не дожно быть пустым'])
                ],
            ]);
    }

    /**
     * @inheritdoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Article::class,
            'csrf_protection' => false,
            'allow_extra_fields' => true,
            'constraints' => [new UniqueEntity([
                'fields' => ['alias'],
                'message' => 'Новость с таким алиасов уже существует',
            ])],
        ]);
    }
}

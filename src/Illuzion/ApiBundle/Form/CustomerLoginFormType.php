<?php

namespace Illuzion\ApiBundle\Form;

use Illuzion\ApiBundle\Model\CustomerLoginForm;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Форма авторизации пользователя
 */
class CustomerLoginFormType extends AbstractType
{
    /**
     * @inheritdoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('login', TextType::class, [
                'constraints' => [new NotBlank(['message' => 'Это поле не дожно быть пустым'])]
            ])
            ->add('password', TextType::class, [
                'constraints' => [new NotBlank(['message' => 'Это поле не дожно быть пустым'])]
            ])
        ;
    }

    /**
     * @inheritdoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CustomerLoginForm::class,
            'csrf_protection' => false,
            'allow_extra_fields' => true,
        ]);
    }

}

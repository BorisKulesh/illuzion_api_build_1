<?php

namespace Illuzion\ApiBundle\Form\Common;

use Illuzion\ApiBundle\Form\Transformer\StringToDatetimeTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Форма для обработки полей с датой и временем
 */
class DateTimeType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addModelTransformer(new StringToDatetimeTransformer());
    }

    /**
     * @inheritDoc
     */
    public function getParent()
    {
        return TextType::class;
    }
}

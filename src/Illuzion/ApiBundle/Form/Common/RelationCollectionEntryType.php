<?php

namespace Illuzion\ApiBundle\Form\Common;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Вспомогательная форма для обработки массива однотипных связей
 */
class RelationCollectionEntryType extends AbstractType
{
    /**
     * @inheritdoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // Мапим стандартный EntityType на поле id отношения

        $class = $options['class'];
        $builder
            ->add('id', EntityType::class, ['class' => $class])
            ->addModelTransformer(new CallbackTransformer(
                function ($args) {
                    if (null !== $args) {
                        return ['id' => $args];
                    }

                    return null;
                },
                function ($args) use ($class) {
                    if (array_key_exists('id', $args) && $args['id'] instanceof $class) {
                        return $args['id'];
                    }

                    return null;
                }
            ));
    }

    /**
     * @inheritdoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('allow_extra_fields', true);
        // Класс сущности связи
        $resolver->setDefined('class');
    }
}

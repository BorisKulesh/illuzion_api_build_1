<?php

namespace Illuzion\ApiBundle\Form\Common;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Форма для обработки массива однотипных связей
 */
class RelationCollectionType extends AbstractType
{
    /**
     * @inheritdoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $class = $options['class'];
        $builder->add(
            $builder
                ->create('data', CollectionType::class, [
                    'entry_type' => RelationCollectionEntryType::class,
                    'entry_options' => ['class' => $class],
                    'data' => $builder->getData(),
                    'allow_add' => true,
                    'allow_delete' => true,
                ])
        )
        ->addModelTransformer(new CallbackTransformer(
            function ($args) {
                if (null !== $args) {
                    return ['data' => $args];
                }

                return null;
            },
            function ($args) use ($class) {
                if (array_key_exists('data', $args)) {
                    return $args['data'];
                }

                return null;
            }
        ));
    }

    /**
     * @inheritdoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('allow_extra_fields', true);
        // Класс сущности связи
        $resolver->setDefined('class');
    }
}

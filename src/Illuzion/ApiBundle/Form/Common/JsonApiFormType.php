<?php

namespace Illuzion\ApiBundle\Form\Common;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Базовый класс для json-api форм
 */
abstract class JsonApiFormType extends AbstractType
{
    /**
     * Конфигурация атрибутов сущности
     *
     * @param FormBuilderInterface $builder
     * @return FormBuilderInterface
     */
    protected function attributesBuilder(FormBuilderInterface $builder)
    {
        if ($builder->has('attributes')) {
            $attrBuilder = $builder->get('attributes');
        } else {
            $attrBuilder = $builder
                ->create('attributes', FormType::class, [
                    'data' => $builder->getData(),
                    'mapped' => false,
                    'allow_extra_fields' => true,
                ]);
            $builder->add($attrBuilder);
        }
        return $attrBuilder;
    }

    /**
     * Конфигурация связей сущности
     *
     * @param FormBuilderInterface $builder
     * @return FormBuilderInterface
     */
    protected function relationsBuilder(FormBuilderInterface $builder)
    {
        if ($builder->has('relationships')) {
            $attrBuilder = $builder->get('relationships');
        } else {
            $attrBuilder = $builder
                ->create('relationships', FormType::class, [
                    'data' => $builder->getData(),
                    'mapped' => false,
                    'allow_extra_fields' => true,
                ]);
            $builder->add($attrBuilder);
        }
        return $attrBuilder;
    }
}

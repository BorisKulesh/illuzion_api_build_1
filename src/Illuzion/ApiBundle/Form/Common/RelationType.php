<?php

namespace Illuzion\ApiBundle\Form\Common;

use Doctrine\ORM\EntityRepository;
use Illuzion\ApiBundle\Entity\LocalEntity;
use Illuzion\ApiBundle\Provider\EntityManagerProvider;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Форма для обработки связей
 */
class RelationType extends AbstractType
{
    /** @var EntityManagerProvider */
    protected $entityManagerProvider;

    /**
     * @param EntityManagerProvider $entityManagerProvider
     */
    public function __construct(EntityManagerProvider $entityManagerProvider)
    {
        $this->entityManagerProvider = $entityManagerProvider;
    }

    /**
     * @inheritdoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // Класс сущности связи
        $class = $options['class'];
        // Искать сущность в общей базе (Mysql)
        $isSharedEm = $options['shared_em'];

        $builder->add(
            $builder
                ->create('data', FormType::class, [
                    'data' => $builder->getData(),
                    'allow_extra_fields' => true
                ])
                ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) use ($class, $isSharedEm) {
                    // Мапим стандартный EntityType на поле id отношения

                    $data = $event->getData();
                    if (!$data || !array_key_exists('id', $data)) {
                        $event->getForm()->add('id', EntityType::class, [
                            'class' => $class,
                        ]);
                        return;
                    }

                    $em = null;
                    $id = $data['id'];
                    if ($isSharedEm) {
                        $em = $this->entityManagerProvider->getSharedEm();
                    } else {
                        if (in_array(LocalEntity::class, class_parents($class))) {
                            list($cinemaId, $id) = explode('_', $id);
                            $em = $this->entityManagerProvider
                                ->getMainEmForCinema($cinemaId);
                        }
                    }

                    $event->setData(['id' => $id]);
                    $event->getForm()->add('id', EntityType::class, [
                        'class' => $class,
                        'invalid_message' => 'Указанная сущность не существует',
                        'em' => $em,
                    ]);
                })
        )
        ->addModelTransformer(new CallbackTransformer(
            function ($args) {
                if (null !== $args) {
                    return ['data' => ['id' => $args]];
                }

                return null;
            },
            function ($args) use ($class) {
                if (array_key_exists('id', $args['data']) && $args['data']['id'] instanceof $class) {
                    return $args['data']['id'];
                }

                return null;
            }
        ));
    }

    /**
     * @inheritdoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('allow_extra_fields', true);
        // Искать сущность в общей базе (Mysql)
        $resolver->setDefault('shared_em', false);
        // Класс сущности связи
        $resolver->setDefined('class');
    }
}

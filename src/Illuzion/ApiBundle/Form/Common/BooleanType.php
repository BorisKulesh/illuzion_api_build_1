<?php

namespace Illuzion\ApiBundle\Form\Common;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Форма для обработки boolean полей
 */
class BooleanType extends AbstractType
{
    /**
     * @inheritdoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addViewTransformer(new CallbackTransformer(
            // Transforms a bool into a string.
            function ($value) {
                return $value ? 'true' : 'false';
            },
            // Transforms a string into a bool.
            function ($value) {
                if ($value === "") {
                    return null;
                }

                if ($value === null) {
                    return false;
                }

                if ('true' === $value) {
                    return true;
                }

                if ('false' === $value) {
                    return false;
                }

                return (bool) $value;
            }
        ));
    }

    /**
     * @inheritdoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'empty_data' => null,
            'compound' => false,
        ));
    }
}

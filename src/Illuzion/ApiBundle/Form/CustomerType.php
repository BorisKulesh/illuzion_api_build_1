<?php

namespace Illuzion\ApiBundle\Form;

use Illuzion\ApiBundle\Entity\Customer\Customer;
use Illuzion\ApiBundle\Form\Common\BooleanType;
use Illuzion\ApiBundle\Form\Common\DateTimeType;
use Illuzion\ApiBundle\Form\Common\JsonApiFormType;
use Illuzion\ApiBundle\Provider\EntityManagerProvider;
use Illuzion\ApiBundle\Repository\CustomerRepository;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Форма для редактирования пользователя
 */
class CustomerType extends JsonApiFormType
{
    /** @var CustomerRepository */
    protected $customerRepository;

    /**
     * @param EntityManagerProvider $entityManagerProvider
     */
    public function __construct(EntityManagerProvider $entityManagerProvider)
    {
        $this->customerRepository = $entityManagerProvider
            ->getCustomerEm()
            ->getRepository('Customer:Customer');
    }

    /**
     * @inheritdoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->attributesBuilder($builder)
            ->add('phone', TextType::class, [
                'property_path' => 'phoneNew',
                'constraints' => [
                    new Regex([
                        'pattern' => '/^9\d{9}$/',
                        'message' => 'Некорректный формат телефона'
                    ]),
                    new Callback([
                        'callback' => [static::class, 'validateUniquePhone'],
                        'payload' => [
                            'repository' => $this->customerRepository,
                            'customer' => $builder->getData(),
                        ],
                    ]),
                ]
            ])
            ->add('email', TextType::class, [
                'property_path' => 'emailNew',
                'constraints' => [
                    new Email(['message' => 'Некорректный формат почты']),
                    new Callback([
                        'callback' => [static::class, 'validateUniqueEmail'],
                        'payload' => [
                            'repository' => $this->customerRepository,
                            'customer' => $builder->getData(),
                        ],
                    ]),
                ],
            ])
            ->add('password', TextType::class, [
                'mapped' => false,
                'constraints' => [
                    new Length(['min' => 8, 'minMessage' => 'Пароль должен содержать минимум 8 символов']),
                    new Regex(['pattern' => '/\d+/', 'message' => 'Пароль должен содержать хотябы одну цифру']),
                    new Regex(['pattern' => '/\D+/', 'message' => 'Пароль должен содержать хотябы одну букву']),
                ],
            ])
            ->add('name_first', TextType::class)
            ->add('name_middle', TextType::class)
            ->add('name_last', TextType::class)
            ->add('birthday', DateTimeType::class)
            ->add('sms_notifications', BooleanType::class)
            ->add('email_notifications', BooleanType::class)
            ->add('email_newsletter', BooleanType::class)
            ->add('blocked', BooleanType::class, [
                'mapped' => false,
            ]);
    }

    /**
     * Проверка на уникальность почты
     *
     * @param string $object
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public static function validateUniqueEmail($object, ExecutionContextInterface $context, $payload)
    {
        /** @var CustomerRepository $customerRepository */
        $customerRepository = $payload['repository'];

        /** @var Customer $customer */
        $customer = $payload['customer'];

        if (null === $object || !$customer->isEmailUpdated() || $customerRepository->isEmailUnique($object)) {
            return;
        }

        $context
            ->buildViolation('Пользователь с такой почтой уже зарегистрирован')
            ->atPath('')
            ->addViolation();
    }

    /**
     * Проверка на уникальность номера телефона
     *
     * @param string $object
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public static function validateUniquePhone($object, ExecutionContextInterface $context, $payload)
    {
        /** @var CustomerRepository $customerRepository */
        $customerRepository = $payload['repository'];

        /** @var Customer $customer */
        $customer = $payload['customer'];

        if (null === $object || !$customer->isPhoneUpdated() || $customerRepository->isPhoneUnique($object)) {
            return;
        }

        $context
            ->buildViolation('Пользователь с таким номером телефона уже зарегистрирован')
            ->atPath('')
            ->addViolation();
    }

    /**
     * @inheritdoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Customer::class,
            'csrf_protection' => false,
            'allow_extra_fields' => true,
        ]);
    }
}

<?php

namespace Illuzion\ApiBundle\Form;

use Illuzion\ApiBundle\Entity\Main\Program;
use Illuzion\ApiBundle\Entity\Shared\City;
use Illuzion\ApiBundle\Form\Common\BooleanType;
use Illuzion\ApiBundle\Form\Common\JsonApiFormType;
use Illuzion\ApiBundle\Form\Common\RelationType;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Форма для создания и изменения программ
 */
class ProgramType extends JsonApiFormType
{
    /**
     * @inheritdoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->attributesBuilder($builder)
            ->add('title', TextType::class, ['constraints' => [
                new NotBlank(['message' => 'Это поле не дожно быть пустым'])
            ]])
            ->add('alias', TextType::class, [
                'constraints' => [
                    new NotBlank(['message' => 'Это поле не дожно быть пустым']),
                    new Regex([
                        'pattern' => '/^[a-zA-Z0-9-]+$/',
                        'message' => 'Поле содержит некорректные символы'
                    ])
                ]
            ])
            ->add('subtitle', TextType::class, [
                'constraints' => [
                    new NotBlank(['message' => 'Это поле не дожно быть пустым'])
                ]
            ])
            ->add('published', BooleanType::class)
            ->add('description_short', TextType::class)
            ->add('description_full', TextType::class)
            ->add('poster_url', TextType::class)
            ->add('cover_url', TextType::class);

        $this->relationsBuilder($builder)
            ->add('parent', RelationType::class, [
                'class' => Program::class,
            ])
            ->add('city', RelationType::class, [
                'class' => City::class,
                'shared_em' => true,
                'mapped' => false,
            ]);

        $builder->addEventListener(FormEvents::POST_SUBMIT, [$this, 'handlePostSubmitEvent']);
    }

    /**
     * После обработки формы необходимо выставить прокграмме id города
     * т.к. они лежат в разных базах и каких-то базовых механизмов для этого нет
     *
     * @param FormEvent $event
     */
    public function handlePostSubmitEvent(FormEvent $event)
    {
        /**
         * @var Program $program
         * @var City $city
         */
        $program = $event->getData();
        $city = $event->getForm()->get('relationships')->get('city')->getData();
        if (null !== $city) {
            $program->setCityId($city->getId());
        }
    }

    /**
     * Проверяем что родителем программы не является она сама
     *
     * @param Program $obj
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public static function validateParent(Program $obj, ExecutionContextInterface $context, $payload)
    {
        if (null !== $obj->getId() && $obj->getParent() && $obj->getParent()->getId() === $obj->getId()) {
            $context
                ->buildViolation('Программа не может быть дочерней к самой себе')
                ->atPath('relationships.parent')
                ->addViolation();
        }
    }

    /**
     * @inheritdoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Program::class,
            'csrf_protection' => false,
            'allow_extra_fields' => true,
            'constraints' => [
                new UniqueEntity([
                    'fields' => ['alias'],
                    'message' => 'Программа с таким алиасом уже существует',
                ]),
                new Callback([static::class, 'validateParent'])
            ],
        ]);
    }
}

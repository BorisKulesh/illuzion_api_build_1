<?php

namespace Illuzion\ApiBundle\Form;

use Doctrine\Common\Collections\ArrayCollection;
use Illuzion\ApiBundle\Entity\Main\Distributor;
use Illuzion\ApiBundle\Entity\Main\Genre;
use Illuzion\ApiBundle\Entity\Main\Movie;
use Illuzion\ApiBundle\Entity\Main\Program;
use Illuzion\ApiBundle\Form\Common\BooleanType;
use Illuzion\ApiBundle\Form\Common\DateTimeType;
use Illuzion\ApiBundle\Form\Common\JsonApiFormType;
use Illuzion\ApiBundle\Form\Common\RelationCollectionType;
use Illuzion\ApiBundle\Form\Common\RelationType;
use Illuzion\ApiBundle\Provider\EntityManagerProvider;
use Illuzion\ApiBundle\Repository\CityRepository;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Range;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Форма для создания и редактирования фильмов
 */
class MovieType extends JsonApiFormType
{
    /** @var CityRepository */
    protected $cityRepository;

    /**
     * @param EntityManagerProvider $entityManagerProvider
     */
    public function __construct(EntityManagerProvider $entityManagerProvider)
    {
        $this->cityRepository = $entityManagerProvider
            ->getSharedEm()
            ->getRepository('Shared:City');
    }

    /**
     * @inheritdoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->attributesBuilder($builder)
            ->add('title', TextType::class, [
                'constraints' => [
                    new NotBlank(['message' => 'Это поле не дожно быть пустым'])
                ]
            ])
            ->add('alias', TextType::class, [
                'constraints' => [
                    new NotBlank(['message' => 'Это поле не дожно быть пустым']),
                    new Regex([
                        'pattern' => '/^[a-zA-Z0-9-]+$/',
                        'message' => 'Поле содержит некорректные символы'
                    ])
                ]
            ])
            ->add($this->createStartDateForm($builder))
            ->add('director', TextType::class)
            ->add('runtime', IntegerType::class)
            ->add('licence_number', TextType::class, [
                'constraints' => [
                    new Length([
                        'max' => 9,
                        'maxMessage' => 'Поле не может быть длиннее 9-ти символов',
                    ])
                ]
            ])
            ->add('subtitles', TextType::class, [
                'constraints' => [
                    new Length([
                        'max' => 2,
                        'min' => 2,
                        'exactMessage' => 'Поле должно быть длинной 2 символа',
                    ])
                ]
            ])
            ->add('year', IntegerType::class, [
                'constraints' => [
                    new Range([
                        'min' => 1900,
                        'max' => 9999,
                        'minMessage' => 'Год должен быть больше 1900',
                        'maxMessage' => 'Год должен быть меньше 9999',
                    ])
                ]
            ])
            ->add('countries', TextType::class)
            ->add('pg', ChoiceType::class, [
                'choices' => [0, 6, 12, 16, 18],
                'invalid_message' => 'Недопустимое значение поля'
            ])
            ->add('actors', TextType::class)
            ->add('description', TextType::class)
            ->add('language', TextType::class, [
                'constraints' => [
                    new NotBlank(['message' => 'Это поле не дожно быть пустым']),
                    new Length([
                        'max' => 2,
                        'min' => 2,
                        'exactMessage' => 'Поле должно быть длинной 2 символа',
                    ])
                ]
            ])
            ->add('published', BooleanType::class)
            ->add('archived', BooleanType::class)
            ->add('trailer_url', TextType::class)
            ->add('poster_url', TextType::class)
            ->add('cover_url', TextType::class);

        $this->relationsBuilder($builder)
            ->add('distributor', RelationType::class, [
                'class' => Distributor::class,
            ])
            ->add('program', RelationType::class, [
                'class' => Program::class,
            ])
            ->add('genres', RelationCollectionType::class, [
                'class' => Genre::class,
            ]);
    }

    /**
     * Создает форму для дат старта
     *
     * @param FormBuilderInterface $builder
     * @return FormBuilderInterface
     */
    protected function createStartDateForm(FormBuilderInterface $builder)
    {
        /** @var Movie $movie */
        $movie = $builder->getData();
        $cities = $this->cityRepository->findAll();

        $constrains = [];
        if (null === $movie || null === $movie->getId()) {
            $constrains[] = new Callback([static::class, 'validateStartDates']);
        }

        $releaseDateForm = $builder
            ->create('start_dates', FormType::class, [
                'mapped' => false,
                'data_class' => ArrayCollection::class,
                'allow_extra_fields' => true,
                'constraints' => $constrains,
            ]);

        foreach ($cities as $city) {
            $releaseDateForm->add($city->getId(), DateTimeType::class, [
                'property_path' => '['.$city->getId().']',
            ]);
        }

        return $releaseDateForm;
    }

    /**
     * Проверяет что дата старта есть хотябы в одном городе
     *
     * @param ArrayCollection $obj
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public static function validateStartDates($obj, ExecutionContextInterface $context, $payload)
    {
        foreach ($obj as $city => $date) {
            if ($date !== null) {
                return;
            }
        }

        $context
            ->buildViolation('Необходимо указать дату старта хотя бы в одном городе')
            ->atPath('')
            ->addViolation();
    }

    /**
     * @inheritdoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Movie::class,
            'csrf_protection' => false,
            'allow_extra_fields' => true,
            'constraints' => [
                new UniqueEntity([
                    'fields' => ['title'],
                    'message' => 'Фильм с таким названием уже существует',
                ]),
                new UniqueEntity([
                    'fields' => ['alias'],
                    'message' => 'Фильм с таким алиасом уже существует',
                ]),
            ],
        ]);
    }
}

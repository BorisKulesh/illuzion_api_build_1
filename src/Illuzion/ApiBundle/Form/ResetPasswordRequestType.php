<?php

namespace Illuzion\ApiBundle\Form;

use Illuzion\ApiBundle\Model\ResetPasswordRequest;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

/**
 * Форма для сброса пароля
 */
class ResetPasswordRequestType extends AbstractType
{
    /**
     * @inheritdoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user_id', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                ]
            ])
            ->add('send_to', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new Choice([
                        'choices' => ['email', 'phone'],
                        'message' => 'Допустимые значения email или phone'
                    ])
                ]
            ])
        ;
    }

    /**
     * @inheritdoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ResetPasswordRequest::class,
            'csrf_protection' => false,
            'allow_extra_fields' => true,
        ]);
    }
}

<?php

namespace Illuzion\ApiBundle\Form;

use Doctrine\Common\Collections\ArrayCollection;
use Illuzion\ApiBundle\Entity\Main\Show;
use Illuzion\ApiBundle\Form\Common\JsonApiFormType;
use Illuzion\ApiBundle\Form\Common\RelationType;
use Illuzion\ApiBundle\Model\OrderReservationRequest;
use Illuzion\ApiBundle\Service\PlaceTypePriceService;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Count;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Regex;

/**
 * Форма для создания заказа
 */
class OrderReservationRequestType extends JsonApiFormType
{
    /**
     * @inheritdoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->attributesBuilder($builder)
            ->add('total_cost', IntegerType::class, [
                'constraints' => [
                    new NotNull(['message' => 'Это поле не дожно быть пустым'])
                ]
            ])
            ->add('email', TextType::class, [
                'constraints' => [
                    new Email(['message' => 'Некорректный формат почты'])
                ],
            ])
            ->add('phone', TextType::class, [
                'constraints' => [new Regex([
                    'pattern' => '/^9\d{9}$/',
                    'message' => 'Некорректный формат телефона. Необходимо указать 10 цифр, начиная с 9ки'
                ])]
            ])
            ->add('promo', TextType::class, [
                'constraints' => [
                    new NotBlank(['message' => 'Это поле не дожно быть пустым'])
                ],
                'empty_data' => PlaceTypePriceService::TYPE_DEFAULT,
            ])
            ->add('seats', CollectionType::class, [
                'entry_type' => TextType::class,
                'constraints' => [
                    new Count(['min' => 1, 'minMessage' => 'Это поле не дожно быть пустым']),
                    new NotNull(['message' => 'Это поле не дожно быть пустым'])
                ],
                'allow_add' => true,
                'allow_delete' => true,
                'empty_data' => new ArrayCollection(),
            ]);

        $this->relationsBuilder($builder)
            ->add('show', RelationType::class, [
                'class' => Show::class,
                'constraints' => [
                    new NotNull(['message' => 'Это поле не дожно быть пустым'])
                ],
            ]);
    }

    /**
     * @inheritdoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => OrderReservationRequest::class,
            'csrf_protection' => false,
            'allow_extra_fields' => true,
        ]);
    }
}

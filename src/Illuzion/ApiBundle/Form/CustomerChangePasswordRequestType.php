<?php

namespace Illuzion\ApiBundle\Form;

use Illuzion\ApiBundle\Model\CustomerChangePasswordRequest;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

/**
 * Форма смены пароля
 */
class CustomerChangePasswordRequestType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user_id', TextType::class, [
                'constraints' => [new NotBlank()]
            ])
            ->add('old_password', TextType::class, [
                'constraints' => [new NotBlank()]
            ])
            ->add('new_password', TextType::class, [
                'constraints' => [
                    new NotBlank(['message' => 'Это поле не дожно быть пустым']),
                    new Length(['min' => 8, 'minMessage' => 'Пароль должен содержать минимум 8 символов']),
                    new Regex(['pattern' => '/\d+/', 'message' => 'Пароль должен содержать хотябы одну цифру']),
                    new Regex(['pattern' => '/\D+/', 'message' => 'Пароль должен содержать хотябы одну букву']),
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CustomerChangePasswordRequest::class,
            'csrf_protection' => false,
            'allow_extra_fields' => true,
        ]);
    }

}

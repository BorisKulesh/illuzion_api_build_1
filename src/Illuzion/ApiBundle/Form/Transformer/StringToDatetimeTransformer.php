<?php


namespace Illuzion\ApiBundle\Form\Transformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

/**
 * Правило преобразование строки в дату при парсинге форм
 */
class StringToDatetimeTransformer implements DataTransformerInterface
{
    /** @var string */
    protected $format;

    /**
     * @param string $format
     */
    public function __construct($format = "Y-m-d H:i:s")
    {
        $this->format = $format;
    }

    /**
     * Get timestamp from DateTime object
     *
     * @param \DateTime $datetime
     * @return string|null
     */
    public function transform($datetime)
    {
        if (null === $datetime) {
            return null;
        }

        return $datetime->format($this->format);
    }

    /**
     * Transform datetime string to DateTime object
     *
     * @param string $str
     * @return \DateTime
     */
    public function reverseTransform($str)
    {
        if (!$str) {
            return null;
        }

        $date = \DateTime::createFromFormat($this->format, $str);

        if (false === $date) {
            throw new TransformationFailedException("Bad datetime format");
        }

        return $date;
    }
}

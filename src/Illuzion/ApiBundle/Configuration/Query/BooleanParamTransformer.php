<?php

namespace Illuzion\ApiBundle\Configuration\Query;

use Illuzion\ApiBundle\Request\Params\ParamTransformer;

/**
 * Обработчик аннотации BooleanParam
 */
class BooleanParamTransformer extends ParamTransformer
{
    /**
     * @inheritdoc
     */
    public function transform(array $sourceData)
    {
        $value = $this->getMappedValue($sourceData);

        if (null === $value) {
            return $this->getOption(BooleanParam::OPTION_DEFAULT);
        } elseif (is_bool($value)) {
            return $value;
        } elseif (true === in_array(strtolower($value), ['', 'true', '1'])) {
            return true;
        } elseif (true === in_array(strtolower($value), ['false', '0'])) {
            return false;
        } else {
            return (bool) $value;
        }
    }
}

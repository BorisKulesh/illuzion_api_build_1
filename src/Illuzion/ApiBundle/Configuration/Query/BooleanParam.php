<?php

namespace Illuzion\ApiBundle\Configuration\Query;

use Illuzion\ApiBundle\Request\Params\ScalarParam;

/**
 * @Annotation
 * @Target({"METHOD", "ANNOTATION"})
 *
 * Данную аннотацию можно прописать методу контроллера
 * Она добавит поддержку фильтра по флагу
 *
 * Например - BooleanParam("published")
 * Означает, что в метод поддерживает параметр filter[published]
 */
class BooleanParam extends ScalarParam
{
}

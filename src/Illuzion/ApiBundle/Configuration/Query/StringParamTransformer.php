<?php

namespace Illuzion\ApiBundle\Configuration\Query;

use Illuzion\ApiBundle\Request\Params\ParamTransformer;

/**
 * Обработчик аннотации StringParam
 */
class StringParamTransformer extends ParamTransformer
{
    /**
     * @param array $sourceData
     * @return mixed
     */
    public function transform(array $sourceData)
    {
        $value = $this->getMappedValue($sourceData);

        return null === $value ? $this->getOption(StringParam::OPTION_DEFAULT) : (string) $value;
    }
}

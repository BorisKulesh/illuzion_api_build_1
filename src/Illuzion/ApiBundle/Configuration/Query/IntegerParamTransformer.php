<?php

namespace Illuzion\ApiBundle\Configuration\Query;

use Illuzion\ApiBundle\Request\Params\ParamTransformer;

/**
 * Обработчик аннотации IntegerParam
 */
class IntegerParamTransformer extends ParamTransformer
{
    /**
     * @param array $sourceData
     * @return mixed
     */
    public function transform(array $sourceData)
    {
        $value = $this->getMappedValue($sourceData);

        if (null === $value) {
            return $this->getOption(IntegerParam::OPTION_DEFAULT);
        }

        return (int) $value;
    }
}

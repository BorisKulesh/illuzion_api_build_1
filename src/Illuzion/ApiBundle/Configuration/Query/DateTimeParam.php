<?php

namespace Illuzion\ApiBundle\Configuration\Query;

use Symfony\Component\Validator\Constraints\DateTime;
use Illuzion\ApiBundle\Request\Params\ScalarParam;

/**
 * @Annotation
 * @Target({"METHOD", "ANNOTATION"})
 *
 * Данную аннотацию можно прописать методу контроллера
 * Она добавит поддержку фильтра по дате и времени
 *
 * Например - DateTimeParam("date")
 * Означает, что в метод поддерживает параметр filter[date]
 */
class DateTimeParam extends ScalarParam
{
    public $format = 'Y-m-d H:i:s';

    /**
     * @param array $options
     */
    public function __construct(array $options)
    {
        parent::__construct($options);

        $this->addSourceConstraint(new DateTime(['format' => $this->format]));
    }
}

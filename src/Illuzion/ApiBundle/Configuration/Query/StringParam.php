<?php

namespace Illuzion\ApiBundle\Configuration\Query;

use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\Regex;
use Illuzion\ApiBundle\Request\Params\ScalarParam;

/**
 * @Annotation
 * @Target({"METHOD", "ANNOTATION"})
 *
 * Данную аннотацию можно прописать методу контроллера
 * Она добавит поддержку фильтра по строке
 *
 * Например - StringParam("search")
 * Означает, что в метод поддерживает параметр filter[search]
 */
class StringParam extends ScalarParam
{
    /** @var string */
    public $pattern;

    /** @var int */
    public $minLength;

    /** @var int */
    public $maxLength;

    /**
     * @param array $options
     */
    public function __construct(array $options)
    {
        parent::__construct($options);

        if (null !== $this->pattern) {
            $this->addValueConstraint(new Regex([
                'pattern' => "#^{$this->pattern}$#ixsu",
                'message' => "Value doesn't match pattern '$this->pattern'"
            ]));
        }

        if ((null !== $this->minLength) || (null !== $this->maxLength)) {
            $this->addValueConstraint(new Length([
                'min' => $this->minLength,
                'max' => $this->maxLength
            ]));
        }
    }
}

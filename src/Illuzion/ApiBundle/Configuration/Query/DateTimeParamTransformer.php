<?php

namespace Illuzion\ApiBundle\Configuration\Query;

use Illuzion\ApiBundle\Request\Params\ParamTransformer;

/**
 * Обработчик аннотации DateTimeParam
 */
class DateTimeParamTransformer extends ParamTransformer
{
    /**
     * @param array $sourceData
     * @return mixed
     */
    public function transform(array $sourceData)
    {
        $value = $this->getMappedValue($sourceData);

        if (null === $value) {
            $value = $this->getOption(DateTimeParam::OPTION_DEFAULT);

            if (null === $value) {
                return null;
            }
        }

        return new \DateTime($value);
    }
}

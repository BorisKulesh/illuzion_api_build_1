<?php

namespace Illuzion\ApiBundle\Configuration\Query;

use Illuzion\ApiBundle\Configuration\Constraint\IsNumber;
use Symfony\Component\Validator\Constraints\Range;
use Illuzion\ApiBundle\Request\Params\ScalarParam;

/**
 * @Annotation
 * @Target({"METHOD", "ANNOTATION"})
 *
 * Данную аннотацию можно прописать методу контроллера
 * Она добавит поддержку фильтра по числу
 *
 * Например - IntegerParam("maxCount")
 * Означает, что в метод поддерживает параметр filter[maxCount]
 */
class IntegerParam extends ScalarParam
{
    /** @var int */
    public $min;

    /** @var int */
    public $max;

    /**
     * @param array $options
     */
    public function __construct(array $options)
    {
        parent::__construct($options);

        $this->addSourceConstraint(new IsNumber());

        if ((null !== $this->min) || (null !== $this->max)) {
            $this->addValueConstraint(new Range([
                'min' => $this->min,
                'max' => $this->max
            ]));
        }
    }
}

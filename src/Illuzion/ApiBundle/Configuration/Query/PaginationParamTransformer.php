<?php

namespace Illuzion\ApiBundle\Configuration\Query;

use Illuzion\ApiBundle\Request\Pagination;
use Illuzion\ApiBundle\Request\Params\ParamTransformer;

/**
 * Обработчик аннотации PaginationParam
 */
class PaginationParamTransformer extends ParamTransformer
{
    /**
     * @param array $sourceData
     * @return mixed
     */
    public function transform(array $sourceData)
    {
        $offset = $this->getMappedValue($sourceData, PaginationParam::PARAM_OFFSET);
        if (null === $offset) {
            $offset = 0;
        }

        $limit = $this->getMappedValue($sourceData, PaginationParam::PARAM_LIMIT);
        if (null == $limit) {
            $limit = $this->getOption(PaginationParam::OPTION_DEFAULT_PAGE_SIZE);
        }

        return new Pagination((int) $offset, (int) $limit);
    }
}

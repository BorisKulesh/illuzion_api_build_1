<?php

namespace Illuzion\ApiBundle\Configuration\Query;

use Symfony\Component\Validator\Constraints\Range;
use Symfony\Component\Validator\Constraints\Type;
use Illuzion\ApiBundle\Request\Pagination;
use Illuzion\ApiBundle\Request\Params\BaseParam;

/**
 * @Annotation
 * @Target({"METHOD"})
 *
 * Данную аннотацию можно прописать методу контроллера
 * Она добавит поддержку пагинации
 *
 * Например - PaginationParam("pagination")
 * Означает, что в метод поддерживает параметр page[limit] page[offset]
 */
class PaginationParam extends BaseParam
{
    const PARAM_LIMIT = 'page[limit]';
    const PARAM_OFFSET = 'page[offset]';
    const OPTION_DEFAULT_PAGE_SIZE = 'default_ps';

    /** @var int */
    public $defaultPageSize = Pagination::DEFAULT_PAGE_SIZE;

    /**
     * @param array $options
     */
    public function __construct(array $options)
    {
        parent::__construct($options);

        $this->addSourceConstraint(new Type(['type' => 'digit']), self::PARAM_LIMIT);
        $this->addSourceConstraint(new Range(['min' => 1]), self::PARAM_LIMIT);

        $this->addSourceConstraint(new Type(['type' => 'digit']), self::PARAM_OFFSET);
        $this->addSourceConstraint(new Range(['min' => 0]), self::PARAM_OFFSET);

        $this->setTransformRuleOption(self::OPTION_DEFAULT_PAGE_SIZE, $this->defaultPageSize);
    }
}

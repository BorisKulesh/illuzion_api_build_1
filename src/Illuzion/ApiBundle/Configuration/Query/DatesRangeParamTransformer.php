<?php

namespace Illuzion\ApiBundle\Configuration\Query;

use Illuzion\ApiBundle\Request\DatesRange;
use Illuzion\ApiBundle\Request\Params\ParamTransformer;

/**
 * Обработчик аннотации DatesRangeParam
 */
class DatesRangeParamTransformer extends ParamTransformer
{
    /**
     * @param array $sourceData
     * @return mixed
     */
    public function transform(array $sourceData)
    {
        $year      = $this->getMappedValue($sourceData, DatesRangeParam::PARAM_YEAR);
        $month     = $this->getMappedValue($sourceData, DatesRangeParam::PARAM_MONTH);
        $dateStart = $this->getMappedValue($sourceData, DatesRangeParam::PARAM_DATE_START);
        $dateEnd   = $this->getMappedValue($sourceData, DatesRangeParam::PARAM_DATE_END);

        if (null !== $dateStart || null !== $dateEnd) {
            $useTimestamps = $this->getOption(DatesRangeParam::OPTION_USE_TIMESTAMPS, false);
            if ($useTimestamps) {
                return new DatesRange(
                    $this->createDateTimeFromTimestamp($dateStart),
                    $this->createDateTimeFromTimestamp($dateEnd)
                );
            } else {
                return new DatesRange(
                    $dateStart ? new \DateTime($dateStart) : null,
                    $dateEnd ? new \DateTime($dateEnd) : null
                );
            }
        } elseif (null !== $year) {
            if (null !== $month) {
                $month = str_pad($month, 2, '0', STR_PAD_LEFT);
                $start = new \DateTime("{$year}-{$month}-01 00:00:00");
                $end   = clone $start;
                $end
                    ->modify('+1 month')
                    ->modify('-1 days')
                    ->setTime(23, 59, 59);
            } else {
                $start = new \DateTime("{$year}-01-01 00:00:00");
                $end = new \DateTime("{$year}-12-31 23:59:59");
            }

            return new DatesRange($start, $end);
        }

        return null;
    }

    /**
     * @param int|null $timestamp
     *
     * @return \DateTime|null
     */
    protected function createDateTimeFromTimestamp($timestamp)
    {
        if (null === $timestamp) {
            return null;
        }

        return (new \DateTime())
            ->setTimestamp($timestamp);
    }
}

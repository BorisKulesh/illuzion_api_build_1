<?php

namespace Illuzion\ApiBundle\Configuration\Query;

use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints\Range;
use Symfony\Component\Validator\Constraints\Type;
use Illuzion\ApiBundle\Request\Params\BaseParam;

/**
 * @Annotation
 * @Target({"METHOD", "ANNOTATION"})
 *
 * Данную аннотацию можно прописать методу контроллера
 * Она добавит поддержку фильтра по диапазону дат
 *
 * Например - DatesRangeParam("range")
 * Означает, что в метод поддерживает параметры filter[date_start] filter[date_end] filter[year] filter[month]
 */
class DatesRangeParam extends BaseParam
{
    const OPTION_USE_TIMESTAMPS = 'ut';

    const PARAM_YEAR = 'year';
    const PARAM_MONTH = 'month';
    const PARAM_DATE_START = 'date_start';
    const PARAM_DATE_END = 'date_end';

    /** @var bool */
    public $useTimestamps = false;

    /**
     * @param array $options
     */
    public function __construct(array $options)
    {
        parent::__construct($options);

        $this->addSourceConstraint(new Type(['type' => 'digit']), self::PARAM_YEAR);
        $this->addSourceConstraint(new Range(['min' => 1970]), self::PARAM_YEAR);

        $this->addSourceConstraint(new Type(['type' => 'digit']), self::PARAM_MONTH);
        $this->addSourceConstraint(new Range(['min' => 1, 'max' => 12]), self::PARAM_MONTH);

        $this->setTransformRuleOption(self::OPTION_USE_TIMESTAMPS, $this->useTimestamps);
        if ($this->useTimestamps) {
            $this->addSourceConstraint(new Type(['type' => 'digit']), self::PARAM_DATE_START);
            $this->addSourceConstraint(new Range(['min' => 1]), self::PARAM_DATE_START);

            $this->addSourceConstraint(new Type(['type' => 'digit']), self::PARAM_DATE_END);
            $this->addSourceConstraint(new Range(['min' => 1]), self::PARAM_DATE_END);
        } else {
            $this->addSourceConstraint(new DateTime(), self::PARAM_DATE_START);
            $this->addSourceConstraint(new DateTime(), self::PARAM_DATE_END);
        }
    }
}

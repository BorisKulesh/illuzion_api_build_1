<?php

namespace Illuzion\ApiBundle\Configuration\Query;

use Illuzion\ApiBundle\Request\Params\Metadata\TransformRule;
use Illuzion\ApiBundle\Request\Params\ParamTransformer;
use Illuzion\ApiBundle\Request\Params\ParamTransformerFactory;

/**
 * Обработчик аннотации ScalarArrayParam
 */
class ScalarArrayParamTransformer extends ParamTransformer
{
    /** @var ParamTransformerFactory */
    private $transformersFactory;

    /**
     * @param ParamTransformerFactory $transformersFactory
     */
    public function __construct(ParamTransformerFactory $transformersFactory)
    {
        $this->transformersFactory = $transformersFactory;
    }

    /**
     * @param array $sourceData
     * @return mixed
     */
    public function transform(array $sourceData)
    {
        $sourceArray = $this->getMappedValue($sourceData);

        if (null == $sourceArray) {
            return [];
        }

        /** @var TransformRule $itemsRule */
        $itemsRule = $this->getOption(ScalarArrayParam::OPTION_RULE);
        $itemsMappingKey = reset($itemsRule->mapping);
        $itemsTransformer = $this->transformersFactory->getInitializedTransformer($itemsRule);

        $transformedArray = [];
        foreach ($sourceArray as $key => $value) {
            $transformedValue = $itemsTransformer->transform([$itemsMappingKey => $value]);
            $transformedArray[$key] = $transformedValue;
        }

        return $transformedArray;
    }
}

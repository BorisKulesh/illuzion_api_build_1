<?php

namespace Illuzion\ApiBundle\Configuration\Query;

use Doctrine\Common\Annotations\Annotation\Required;
use Illuzion\ApiBundle\Request\Params\BaseParam;
use Illuzion\ApiBundle\Request\Params\ScalarParam;
use Symfony\Component\Validator\Constraints\All;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * @Annotation
 * @Target({"METHOD"})
 *
 * Данную аннотацию можно прописать методу контроллера
 * Она добавит поддержку фильтра по массиву скаляров
 *
 * Например - ScalarArrayParam("seats", items=IntegerParam())
 * Означает, что в метод поддерживает параметр filter[seats][]
 */
class ScalarArrayParam extends BaseParam
{
    const OPTION_RULE = 'items_rule';

    /**
     * @Required
     * @var ScalarParam
     */
    public $items;

    /** @var bool */
    public $required = false;

    /**
     * @param array $options
     */
    public function __construct(array $options)
    {
        parent::__construct($options);

        if ($this->required) {
            $this->addSourceConstraint(new NotBlank());
        }

        $itemsSourceConstraints = $this->items->getSourceConstraints();
        $this->addSourceConstraint(new All(['constraints' => reset($itemsSourceConstraints)]));

        $itemValidationConstraints = $this->items->getValueConstraints();
        $this->addValueConstraint(new All(['constraints' => $itemValidationConstraints]));

        $itemsTransformRule = $this->items->getTransformRule();
        $this->setTransformRuleOption(self::OPTION_RULE, $itemsTransformRule);
    }

    protected function transformBy()
    {
        return 'api.params.transformer.scalar_array';
    }
}

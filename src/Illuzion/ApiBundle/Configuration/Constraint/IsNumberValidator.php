<?php

namespace Illuzion\ApiBundle\Configuration\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * Валидатор для ограничения IsNumber
 */
class IsNumberValidator extends ConstraintValidator
{
    /**
     * @inheritdoc
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof IsNumber) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__.'\IsNumber');
        }

        if (null === $value) {
            return;
        }

        if (!is_numeric($value)) {
            $this->context->addViolation($constraint->message);
        }
    }
}

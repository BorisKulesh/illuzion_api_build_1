<?php

namespace Illuzion\ApiBundle\Configuration\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Ограничение, что значение должно состоять только из цифр
 */
class IsNumber extends Constraint
{
    public $message = 'Value is not a number';

    public function validatedBy()
    {
        return get_class($this).'Validator';
    }
}

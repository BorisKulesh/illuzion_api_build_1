<?php

namespace Illuzion\ApiBundle\Exception;

use Neomerx\JsonApi\Exceptions\ErrorCollection;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Это исключение будет сериализована в корректный json-api ответ
 */
class ApiException extends HttpException
{
    /** @var ErrorCollection */
    protected $errorCollection;

    /**
     * @param ErrorCollection $errorCollection Список ошибок
     * @param int $statusCode Статус ответа
     * @param \Exception $previous Предыдущее исключение
     */
    public function __construct(ErrorCollection $errorCollection, $statusCode = 400, \Exception $previous = null)
    {
        parent::__construct($statusCode, 'Error during api call', $previous);
        $this->errorCollection = $errorCollection;
    }

    /**
     * @return ErrorCollection Список ошибок
     */
    public function getErrors()
    {
        return $this->errorCollection;
    }
}

<?php

namespace Illuzion\ApiBundle\Security;

use Neomerx\JsonApi\Document\Error;
use Neomerx\JsonApi\Exceptions\JsonApiException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

/**
 * Отвечает за авторизацию клиентов АПИ
 */
class JwsGuardAuthenticator extends AbstractGuardAuthenticator
{
    /** @var JwsPayloadEncoder */
    protected $jwsPayloadEncoder;

    /** @var boolean */
    protected $isDebug;

    /**
     * @param JwsPayloadEncoder $jwsPayloadEncoder
     * @param $isDebug
     */
    public function __construct(JwsPayloadEncoder $jwsPayloadEncoder, $isDebug)
    {
        $this->jwsPayloadEncoder = $jwsPayloadEncoder;
        $this->isDebug = $isDebug;
    }

    /**
     * @inheritdoc
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        return new JsonResponse([
            'errors' => [[
                'status' => '401',
                'details' => 'Auth header required',
            ]]
        ], 401);
    }

    /**
     * @inheritdoc
     */
    public function getCredentials(Request $request)
    {
        if (!$request->headers->has('Authorization')) {
            throw new JsonApiException(
                new Error(null, null, 401, null, null, 'Auth header required')
            );
        }

        return $request->headers->get('Authorization');
    }

    /**
     * @inheritdoc
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        try {
            $payload = $this->jwsPayloadEncoder->decode($credentials);
        } catch (\Exception $e) {
            throw new BadCredentialsException('Provided token is invalid or expired', 0, $e);
        }

        return $userProvider->loadUserByUsername($payload['username']);
    }

    /**
     * @inheritdoc
     */
    public function checkCredentials($credentials, UserInterface $user)
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        if ($this->isDebug) {
            $message = $exception->getMessage() ?: $exception->getMessageKey();
        } else {
            $message = 'Недостаточно прав';
        }

        return new JsonResponse([
            'errors' => [[
                'status' => '403',
                'details' => $message,
            ]]
        ], 403);
    }

    /**
     * @inheritdoc
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        return null;
    }

    /**
     * @inheritdoc
     */
    public function supportsRememberMe()
    {
        return false;
    }
}

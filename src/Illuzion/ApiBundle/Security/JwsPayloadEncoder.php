<?php

namespace Illuzion\ApiBundle\Security;

use Namshi\JOSE\SimpleJWS;

/**
 * Генератов токенов авторизации
 */
class JwsPayloadEncoder
{
    const NONE  = 'None';
    public static $HMAC  = [256 => 'HS256', 384 => 'HS384', 512 => 'HS512'];
    public static $ECDSA = [256 => 'ES256', 384 => 'ES384', 512 => 'ES512'];
    public static $RSA   = [256 => 'RS256', 384 => 'RS384', 512 => 'RS512'];

    /** @var string Алгоритм подписи */
    protected $algo;

    /** @var string[] Ключи подписи */
    protected $keyChain;

    /**
     * @param string $algo
     * @param string[] $keyChain
     */
    public function __construct($algo, array $keyChain)
    {
        $this->algo = $algo;
        $this->keyChain = $keyChain;
    }

    /**
     * Создания токена
     *
     * @param array $payload Данные
     * @return string
     */
    public function encode(array $payload)
    {
        $jwsToken = new SimpleJWS([
            'alg' => $this->algo
        ]);
        $jwsToken->setPayload($payload);
        $jwsToken->sign($this->getSignKey());

        return $jwsToken->getTokenString();
    }

    /**
     * Парсинг и проверка токена
     *
     * @param string $rawToken Токен
     * @return array Данные
     * @throws \InvalidArgumentException
     */
    public function decode($rawToken)
    {
        /** @var SimpleJws $jwsToken */
        $jwsToken = SimpleJWS::load($rawToken, $this->algo === self::NONE);

        if (!$jwsToken->isValid($this->getVerifyKey(), $this->algo)) {
            throw new \InvalidArgumentException(sprintf('The token "%s" is invalid or expired', $rawToken));
        }

        return $jwsToken->getPayload();
    }

    /**
     * @return resource|string
     */
    private function getVerifyKey()
    {
        if ($this->algo === self::NONE) {
            return 'test';
        }

        if (false !== array_search($this->algo, self::$HMAC)) {
            return $this->keyChain['key'];
        }

        return openssl_pkey_get_public('file://'.$this->keyChain['public_key']);
    }

    /**
     * @return resource|string
     */
    private function getSignKey()
    {
        if ($this->algo === self::NONE) {
            return 'test';
        }

        if (false !== array_search($this->algo, self::$HMAC)) {
            return $this->keyChain['key'];
        }

        return openssl_pkey_get_private('file://'.$this->keyChain['private_key'], $this->keyChain['password']);
    }
}

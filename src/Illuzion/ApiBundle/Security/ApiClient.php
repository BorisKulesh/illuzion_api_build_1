<?php

namespace Illuzion\ApiBundle\Security;

use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Модель клиента API
 */
class ApiClient implements UserInterface, EquatableInterface
{
    /** @var string */
    protected $username;

    /** @var string[] */
    protected $roles;

    /** @var string[] */
    protected $priceAdjustment;

    /** @var bool|string[] */
    protected $disableDiscounts;

    /** @var bool|string[] */
    protected $disableBonusAccumulation;

    /** @var bool|string[] */
    protected $disablePurchaseByBonus;

    /** @var string */
    protected $salesCloseTime;

    /**
     * @param string $username
     * @param string[] $roles
     * @param string[] $priceAdjustments
     * @param bool|string[] $disableDiscounts
     * @param bool|string[] $disableBonusAccumulation
     * @param bool|string[] $disablePurchaseByBonus
     * @param string $salesCloseTime
     */
    public function __construct(
        $username,
        array $roles,
        array $priceAdjustments,
        $disableDiscounts,
        $disableBonusAccumulation,
        $disablePurchaseByBonus,
        $salesCloseTime
    ) {
        $this->username = $username;
        $this->roles = $roles;
        $this->priceAdjustment = $priceAdjustments;
        $this->disableDiscounts = $disableDiscounts;
        $this->disableBonusAccumulation = $disableBonusAccumulation;
        $this->disablePurchaseByBonus = $disablePurchaseByBonus;
        $this->salesCloseTime = $salesCloseTime;
    }

    /**
     * @inheritdoc
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @inheritdoc
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Возвращает корректировку цены для клиента
     * Если корректировка < 1, то это процентный модификатор
     *
     * @param string $hall
     * @param string $placeType
     *
     * @return int|float
     */
    public function getPriceAdjustment($hall, $placeType)
    {
        if (array_key_exists($hall.'/'.$placeType, $this->priceAdjustment)) {
            return $this->priceAdjustment[$hall.'/'.$placeType];
        }

        if (array_key_exists($hall, $this->priceAdjustment)) {
            return $this->priceAdjustment[$hall];
        }

        return 0;
    }

    /**
     * Разрешены ли скидки для зала
     *
     * @param string $hall
     *
     * @return bool
     */
    public function isDiscountsAllowed($hall)
    {
        if (is_array($this->disableDiscounts)) {
            return !in_array($hall, $this->disableDiscounts);
        } else {
            return !$this->disableDiscounts;
        }
    }

    /**
     * Разрешено ли накопление бонусов для зала
     *
     * @param string $hall
     *
     * @return bool
     */
    public function isBonusAccumulationAllowed($hall)
    {
        if (is_array($this->disableBonusAccumulation)) {
            return !in_array($hall, $this->disableBonusAccumulation);
        } else {
            return !$this->disableBonusAccumulation;
        }
    }

    /**
     * Разрешена ли покупка за бонусы для зала
     *
     * @param string $hall
     *
     * @return bool
     */
    public function isPurchaseByBonusAllowed($hall)
    {
        if (is_array($this->disablePurchaseByBonus)) {
            return !in_array($hall, $this->disablePurchaseByBonus);
        } else {
            return !$this->disablePurchaseByBonus;
        }
    }

    /**
     * Период до начала сеанся, когда закрываются продажи для клиента
     * Формат как для метода \DateTime::modify
     *
     * @return string
     */
    public function getSalesCloseTime()
    {
        return $this->salesCloseTime;
    }

    /**
     * @inheritdoc
     */
    public function getPassword()
    {
        return '';
    }

    /**
     * @inheritdoc
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * @inheritdoc
     */
    public function eraseCredentials()
    {
    }

    /**
     * @inheritdoc
     */
    public function isEqualTo(UserInterface $user)
    {
        if (!$user instanceof ApiClient) {
            return false;
        }

        if ($this->username !== $user->getUsername()) {
            return false;
        }

        return true;
    }
}

<?php

namespace Illuzion\ApiBundle\Security;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * Провайдер клиентов АПИ
 */
class ApiClientProvider implements UserProviderInterface
{
    /** @var array[] */
    protected $usersData;

    /** @var string */
    protected $defaultSalesCloseTime;

    /**
     * @param \array[] $usersData
     * @param $defaultSalesCloseTime
     */
    public function __construct(array $usersData, $defaultSalesCloseTime)
    {
        $this->usersData = $usersData;
        $this->defaultSalesCloseTime = $defaultSalesCloseTime;
    }

    /**
     * @inheritdoc
     */
    public function loadUserByUsername($username)
    {
        if (!array_key_exists($username, $this->usersData)) {
            throw new UsernameNotFoundException(
                sprintf('Username "%s" does not exist.', $username)
            );
        }

        $userData = new ArrayCollection($this->usersData[$username]);

        $priceAdjustment = [];
        foreach ($userData->get('price_adjustment') ?: [] as $hall => $data) {
            if (!is_array($data)) {
                $priceAdjustment[$hall] = $this->normalizeAdjustment($data);
                continue;
            }

            foreach ($data as $priceType => $adjustment) {
                $priceAdjustment[$hall.'/'.$priceType] = $this->normalizeAdjustment($adjustment);
            }
        }

        return new ApiClient(
            $username,
            $userData->get('roles') ?: [],
            $priceAdjustment,
            $userData->get('disable_discounts') ?: false,
            $userData->get('disable_bonus_accumulation') ?: false,
            $userData->get('disable_purchase_by_bonus') ?: false,
            $userData->get('sales_close_time') ?: $this->defaultSalesCloseTime
        );
    }

    /**
     * @inheritdoc
     */
    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof ApiClient) {
            throw new UnsupportedUserException(
                sprintf('Instances of "%s" are not supported.', get_class($user))
            );
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    /**
     * @inheritdoc
     */
    public function supportsClass($class)
    {
        return ApiClient::class === $class;
    }

    /**
     * @param string $adjustment
     *
     * @return float|int
     */
    protected function normalizeAdjustment($adjustment)
    {
        $adjustment = trim($adjustment);
        if (strpos('%', $adjustment) > 0) {
            return ((int) $adjustment) / 100;
        } else {
            return (int) $adjustment;
        }
    }
}

<?php

namespace Illuzion\ApiBundle\Listener;

use Illuzion\ApiBundle\Exception\ApiException;
use Neomerx\JsonApi\Document\Error;
use Neomerx\JsonApi\Encoder\Encoder;
use Neomerx\JsonApi\Exceptions\JsonApiException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Обрабатывает исключения, которые не были отловлены
 * Старается привести их к корректному json-api ответу
 */
class ApiExceptionListener
{
    /** @var Encoder */
    protected $encoder;

    /** @var LoggerInterface */
    protected $log;

    /** @var boolean */
    protected $isDebug;

    /**
     * @param Encoder $encoder
     * @param LoggerInterface $log
     * @param bool $isDebug
     */
    public function __construct(Encoder $encoder, LoggerInterface $log, $isDebug)
    {
        $this->encoder = $encoder;
        $this->log = $log;
        $this->isDebug = $isDebug;
    }

    /**
     * Приводит исключения к корректному json-api ответу
     *
     * @param GetResponseForExceptionEvent $event
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();
        if ($exception instanceof JsonApiException) {
            $response = new JsonResponse(
                $this->encoder->encodeErrors($exception->getErrors()),
                $exception->getHttpCode(),
                ['Content-Type' => 'application/vnd.api+json'],
                true
            );
        } elseif (!($exception instanceof HttpException)) {
            $this->log->error(
                'Unexpected exception during api call',
                ['exception' => $exception]
            );
            $error = new Error(null, null, 500, 'unexpected_exception', null, $this->isDebug ? $exception->getMessage() : 'Неизвестная ошибка');
            $response = new JsonResponse(
                $this->encoder->encodeError($error),
                500,
                ['Content-Type' => 'application/vnd.api+json'],
                true
            );
        } elseif ($exception instanceof ApiException) {
            $response = new JsonResponse(
                $this->encoder->encodeErrors($exception->getErrors()),
                $exception->getStatusCode(),
                ['Content-Type' => 'application/vnd.api+json'],
                true
            );
        } else {
            if ($exception->getStatusCode() == 403 && !$this->isDebug) {
                $message = 'Доступ запрещен';
            } else {
                $message = $exception->getMessage();
            }

            $error = new Error(null, null, $exception->getStatusCode(), 'error', null, $message);
            $response = new JsonResponse(
                $this->encoder->encodeError($error),
                $exception->getStatusCode(),
                ['Content-Type' => 'application/vnd.api+json'],
                true
            );
        }

        $event->setResponse($response);
    }
}

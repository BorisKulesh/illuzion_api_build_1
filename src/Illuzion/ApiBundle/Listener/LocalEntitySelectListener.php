<?php

namespace Illuzion\ApiBundle\Listener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Illuzion\ApiBundle\Entity\LocalEntity;

/**
 * Проставляет всем загруженным сущностям Id кинотеатра из которого они были загружены
 * Это необходимо для того, чтобы мы могли потом им их правильно обработать, а так же отдать правильны ID в АПИ
 */
class LocalEntitySelectListener implements EventSubscriber
{
    /**
     * @inheritdoc
     */
    public function getSubscribedEvents()
    {
        return ['postLoad'];
    }

    /**
     * Проставляет всем зогруженым сущностям Id кинотеатра из которого они были загружены
     *
     * @param LifecycleEventArgs $args
     */
    public function postLoad(LifecycleEventArgs $args)
    {
        $em = $args->getEntityManager();
        if (!property_exists($em, 'CINEMA_ID')) {
            return;
        }

        $obj = $args->getEntity();
        if (!($obj instanceof LocalEntity)) {
            return;
        }

        $obj->setCinemaId($em->CINEMA_ID);
    }
}

<?php

namespace Illuzion\ApiBundle\Listener;

use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Проверяет входящие запросы на наличие корректного content-type
 * Парсит json входящих POST и PATCH запросов
 */
class JsonRequestTransformerListener
{
    /** @var LoggerInterface */
    protected $logger;

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Проверят входящие запросы на наличие корректного content-type
     * Парсит json входящих POST и PATCH запросов
     *
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        
        if (strpos($request->getUri(), '/v4') === false
            || strpos($request->getUri(), '/swagger/') > 0
        ) {
            return;
        }

        if ((
                in_array($request->getMethod(), ['PATCH', 'POST']) &&
                'jsonapi' !== $request->getContentType()
            ) || (
                count($request->getAcceptableContentTypes()) > 0 &&
                !in_array('application/vnd.api+json', $request->getAcceptableContentTypes())
            )
        ) {
            throw new HttpException(415, 'Unsupported Media Type');
        }

        $data = json_decode($request->getContent(), true);
        if (JSON_ERROR_NONE !== ($error = json_last_error())) {
            $this->logger->debug('Bad json request', [
                'request_content' => $request->getContent(),
                'json_decode_error' => $error
            ]);
            throw new BadRequestHttpException('Unable to parse json request');
        }

        if ((null !== $data) && (is_array($data))) {
            $request->request->replace($data);
        }
    }
}

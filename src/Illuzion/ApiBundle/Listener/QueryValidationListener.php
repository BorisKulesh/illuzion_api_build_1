<?php

namespace Illuzion\ApiBundle\Listener;

use Illuzion\ApiBundle\Controller\BaseController;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Illuzion\ApiBundle\Request\Params\Metadata\MetadataFactory;
use Illuzion\ApiBundle\Request\Params\RequestParser;

/**
 * Обрабатывает входящие запросы
 * Запускает обработку фильтров, сортировок, пагинации и прочих query параметров
 */
class QueryValidationListener
{
    /** @var MetadataFactory */
    protected $metadataFactory;

    /** @var RequestParser */
    protected $requestParser;

    /**
     * @param MetadataFactory $metadataFactory
     * @param RequestParser $requestParser
     */
    public function __construct(
        MetadataFactory $metadataFactory,
        RequestParser $requestParser
    ) {
        $this->metadataFactory = $metadataFactory;
        $this->requestParser = $requestParser;
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        // Получаем метод контроллера
        $controller = $event->getController();
        if (!is_array($controller) || !($controller[0] instanceof BaseController)) {
            return;
        }

        // Получаем метадату действия и парсим запрос
        $request = $event->getRequest();
        $actionMetadata = $this->metadataFactory->getMetadataFor($controller);
        $parameters = $this->requestParser->parseParameters($request, $actionMetadata);

        // Добавляем результаты парсинга в качестве атрибутов запроса
        foreach ($parameters as $name => $value) {
            if ($request->attributes->has($name) && !is_null($request->attributes->get($name))) {
                $route = $request->attributes->get('_route');
                throw new \InvalidArgumentException(
                    "Parameter '$name' conflicts with path parameters from route '$route'"
                );
            }

            $request->attributes->set($name, $value);
        }

        // Собираем и добавляем фильтр
        $filterClass = $actionMetadata->filterClass;
        $request->attributes->set('filter', new $filterClass($parameters));
    }
}

<?php

namespace Illuzion\ApiBundle\Command;

use Illuzion\ApiBundle\Security\JwsPayloadEncoder;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Команда для генерации авторизационных токенов
 */
class GenerateJwsTokenCommand extends Command
{
    /** @var JwsPayloadEncoder */
    protected $payloadEncoder;

    /**
     * @param JwsPayloadEncoder $payloadEncoder
     */
    public function __construct(JwsPayloadEncoder $payloadEncoder)
    {
        $this->payloadEncoder = $payloadEncoder;
        parent::__construct('security:jws:generate-user-token');
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setDescription('Generate jws token for user by username')
            ->addArgument('username', InputArgument::REQUIRED, 'Username')
            ->addArgument('expire', InputArgument::OPTIONAL, 'Timestamp. Token will be valid until this date')
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $username = $input->getArgument('username');
        $expire = $input->getArgument('expire');

        $payload = ['username' => $username];
        if (null !== $expire) {
            $payload['exp'] = $expire;
        }

        $token = $this->payloadEncoder->encode($payload);

        $output->writeln($token);
    }
}

<?php

namespace Illuzion\ApiBundle\Command;

use Illuzion\ApiBundle\Provider\EntityManagerProvider;
use Illuzion\ApiBundle\Repository\OrderRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Команда для закрытия не оплаченных броней
 */
class CancelTempReservationsCommand extends Command
{
    /** @var EntityManagerProvider */
    protected $emProvider;

    /**
     * @param EntityManagerProvider $emProvider
     */
    public function __construct(EntityManagerProvider $emProvider)
    {
        $this->emProvider = $emProvider;
        parent::__construct('api:orders:cancel-temp-reservations');
    }

    /**
     * @inheritDoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        foreach ($this->emProvider->getSupportedCinemas() as $cinema) {
            $output->write($cinema.': ');
            /** @var OrderRepository $orderRepository */
            $orderRepository = $this->emProvider->getMainEmForCinema($cinema)->getRepository('Main:Order');
            $ids = $orderRepository->cancelTempReserv();
            $output->writeln(join(', ', $ids) ?: 'no reservations');
        }
    }
}

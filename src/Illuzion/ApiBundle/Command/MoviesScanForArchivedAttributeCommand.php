<?php

namespace Illuzion\ApiBundle\Command;

use Illuzion\ApiBundle\Provider\EntityManagerProvider;
use Illuzion\ApiBundle\Repository\ShowRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Команда для выставления атрибута archived у фильмов
 */
class MoviesScanForArchivedAttributeCommand extends Command
{
    /** @var EntityManagerProvider */
    protected $emProvider;

    /**
     * @param EntityManagerProvider $emProvider
     */
    public function __construct(EntityManagerProvider $emProvider)
    {
        $this->emProvider = $emProvider;
        parent::__construct('api:movies:scan-for-archived');
    }

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->addOption(
            'scan-all',
            null,
            InputOption::VALUE_NONE,
            'Scam all films, not only last month'
        );
    }

    /**
     * @inheritDoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $scanAll = $input->getOption('scan-all');
        $mainEm = $this->emProvider->getMainEm();
        $movieRepository = $mainEm->getRepository('Main:Movie');

        $movies = $movieRepository->getAllForArchivation($scanAll);
        $progressBar = new ProgressBar($output, count($movies));

        $idx = 0;
        foreach ($movies as $movie) {
            // Дата считается архивной, если начала показа уже прошла
            if ($movie->getStartDate() > new \DateTime()) {
                $movie->setArchived(false);
                $progressBar->advance();
                continue;
            }

            // и если нет сеансов ни в одном кинотеатре
            $movie->setArchived(true);
            foreach ($this->emProvider->getSupportedCinemas() as $cinemaId) {
                $em = $this->emProvider->getMainEmForCinema($cinemaId);
                /** @var ShowRepository $repository */
                $repository = $em->getRepository('Main:Show');
                $futureShowsCount = $repository->checkMovieHasFutureShows($movie->getId());
                if ($futureShowsCount > 0) {
                    $movie->setArchived(false);
                    break;
                }
            }

            $idx++;
            if ($idx % 10 === 0) {
                $idx = 0;
                $mainEm->flush();
            }

            $progressBar->advance();
        }

        $output->writeln('');
        $mainEm->flush();
    }
}

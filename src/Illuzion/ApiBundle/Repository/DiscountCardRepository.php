<?php

namespace Illuzion\ApiBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Illuzion\ApiBundle\Entity\Customer\DiscountCard;
use Illuzion\ApiBundle\Filter\CustomerFilter;

/**
 * Репозиторий бонусных карт
 */
class DiscountCardRepository extends EntityRepository
{
    /**
     * Сохрантиь карту
     * @param DiscountCard $card
     */
    public function save(DiscountCard $card)
    {
        $this->getEntityManager()->persist($card);
        $this->getEntityManager()->flush($card);
    }
}

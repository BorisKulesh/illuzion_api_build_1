<?php

namespace Illuzion\ApiBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Illuzion\ApiBundle\Entity\Shared\Article;
use Illuzion\ApiBundle\Filter\ArticleFilter;

/**
 * Репозиторий новостей
 */
class ArticleRepository extends EntityRepository
{
    /**
     * Кол-во новостей для фильтра
     * (игнорирует пагинацию)
     *
     * @param ArticleFilter $filter
     * @return int
     */
    public function countByFilter(ArticleFilter $filter)
    {
        $qb = $this->createQueryBuilderWithFilter($filter);
        $qb->select('COUNT(article)');

        return (int) $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * Найти все новости по фильтру
     *
     * @param ArticleFilter $filter
     * @return Article[]
     */
    public function findAllByFilter(ArticleFilter $filter)
    {
        $qb = $this->createQueryBuilderWithFilter($filter);

        if (null !== ($pagination = $filter->getPagination())) {
            $qb ->setMaxResults($pagination->getLimit())
                ->setFirstResult($pagination->getOffset());
        }

        $qb->orderBy('article.datetime', 'DESC');
        return $qb->getQuery()->getResult();
    }

    /**
     * Сохранить новость
     * @param Article $article
     */
    public function save(Article $article)
    {
        $this->getEntityManager()->persist($article);
        $this->getEntityManager()->flush($article);
    }

    /**
     * Удалить новость
     * @param Article $article
     */
    public function delete(Article $article)
    {
        $this->getEntityManager()->remove($article);
        $this->getEntityManager()->flush();
    }

    /**
     * Преобразовывает фильтр в запрос
     *
     * @param ArticleFilter $filter
     * @return \Doctrine\ORM\QueryBuilder
     */
    protected function createQueryBuilderWithFilter(ArticleFilter $filter)
    {
        $qb = $this->createQueryBuilder('article');

        if (null !== ($published = $filter->getPublished())) {
            $qb ->andWhere('article.published= :pub')
                ->setParameter('pub', $published);
        }

        if (null !== ($search = $filter->getSearch())) {
            $qb->andWhere($qb->expr()->orX(
                $qb->expr()->like('article.alias', "'%{$search}%'"),
                $qb->expr()->like('article.title', "'%{$search}%'")
            ));
        }

        return $qb;
    }
}

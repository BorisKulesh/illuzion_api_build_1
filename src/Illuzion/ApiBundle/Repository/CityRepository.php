<?php

namespace Illuzion\ApiBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Illuzion\ApiBundle\Entity\Shared\City;

/**
 * Репозиторий городов
 */
class CityRepository extends EntityRepository
{
    /**
     * @param string $cinemaId
     *
     * @return City|null
     */
    public function findOneByCinema($cinemaId)
    {
        $qb = $this->createQueryBuilder('city');
        $qb ->join('city.cinemas', 'cinema')
            ->andWhere('cinema.id = :id')
            ->setParameter('id', $cinemaId);

        return $qb->getQuery()->getOneOrNullResult();
    }
}

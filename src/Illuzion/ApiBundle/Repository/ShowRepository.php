<?php

namespace Illuzion\ApiBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Illuzion\ApiBundle\Entity\Main\Show;
use Illuzion\ApiBundle\Filter\ShowFilter;
use Illuzion\Common\Doctrine\MovieFormat;

/**
 * Репозиторий для работы с сеансами
 */
class ShowRepository extends EntityRepository
{
    /**
     * @inheritdoc
     */
    public function find($id, $lockMode = null, $lockVersion = null)
    {
        // Убираем префикс кинотеатра, если он сюда как-то попал
        if (!is_numeric($id)) {
            list(,$id) = explode('_', $id);
        }
        return parent::find($id, $lockMode, $lockVersion);
    }

    /**
     * Поиск мест по фильтру
     *
     * @param ShowFilter $filter
     * @return Show[]
     */
    public function findAllByFilter(ShowFilter $filter)
    {
        $qb = $this->createQueryBuilder('show');
        $qb ->join('show.movie', 'movie')
            ->addSelect('movie')
        ;

        if (null !== ($movie = $filter->getMovie())) {
            $qb ->andWhere('movie.id = :movie')
                ->setParameter('movie', $movie);
        }

        if (count($format = $filter->getFormat()) > 0) {
            $qb ->join('show.format', 'format')
                ->andWhere($qb->expr()->in('format.title', $format));
        }

        $date = $filter->getDate();
        if (null === $date) {
            // Т.к. в базе время хранится со смещением для ночных сеансов, то все сеансы за вчера, а потом фильруем
            $dateStart = new \DateTime('yesterday midnight');

            $qb ->andWhere('show.datetime >= :date_start')
                ->setParameter('date_start', $dateStart);
        } else {
            $dateStart = clone $date;
            $dateStart->setTime(0, 0, 0);
            $dateEnd = clone $date;
            $dateEnd->setTime(23, 59, 59);

            $qb ->andWhere('show.datetime >= :date_start')
                ->andWhere('show.datetime <= :date_end')
                ->setParameter('date_start', $dateStart)
                ->setParameter('date_end', $dateEnd);
        }

        $result = $qb->getQuery()->getResult();

        $showIds = [];
        $filteredResult = [];
        $now = new \DateTime();
        foreach ($result as $show)
        {
            /** @var Show $show */
            if (null === $date && $show->getActualDatetime() < $now) {
                continue;
            }
            $showIds[] = $show->getId();
            $filteredResult[] = $show;
        }

        // Сеансы запрашиваем отдельным запросом для оптимизации
        if (count($showIds) > 0) {
            $this->createQueryBuilder('show')
                ->leftJoin('show.format', 'format')
                ->select('PARTIAL show.{id}', 'format')
                ->where($qb->expr()->in('show.id', $showIds))
                ->getQuery()
                ->getResult();
        }

        return $filteredResult;
    }

    /**
     * Проверяем, есть ли у фильма сеансы в будуцем
     *
     * @param integer $movieId
     * @return integer
     */
    public function checkMovieHasFutureShows($movieId)
    {
        $qb = $this->createQueryBuilder('show');

        $qb ->select('COUNT(show)')
            ->join('show.movie', 'movie')
            ->andWhere('movie.id = :id')
            ->andWhere('show.datetime > :date')
            ->setParameter('date', new \DateTime())
            ->setParameter('id', $movieId);

        return (int) $qb->getQuery()->getSingleScalarResult();
    }
}

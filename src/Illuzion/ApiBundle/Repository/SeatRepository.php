<?php

namespace Illuzion\ApiBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Illuzion\ApiBundle\Entity\Main\Seat;
use Illuzion\ApiBundle\Filter\SeatFilter;

/**
 * Репозиторий мест
 */
class SeatRepository extends EntityRepository
{
    /**
     * Поиск мест по фильтру
     *
     * @param SeatFilter $filter
     * @return Seat[]
     */
    public function findAllByFilter(SeatFilter $filter)
    {
        $qb = $this->createQueryBuilder('seat');

        list(,$show) = explode('_', $filter->getShow());
        $qb ->join('seat.hall', 'hall')
            ->join('hall.shows', 'show')
            ->andWhere('show.id = :id')
            ->setParameter('id', $show);

        if (count($seats = $filter->getSeats()) > 0) {
            $qb->andWhere($qb->expr()->in('seat.id', $seats));
        }

        if (null !== ($available = $filter->getAvailable())) {
            $availableSeatsIds = $this->getAvailableSeatsIds($show);

            $condition = $qb->expr()->in('seat.id', $availableSeatsIds);
            if ($available) {
                if (count($availableSeatsIds) === 0) {
                    return [];
                }

                $qb->andWhere($condition);
            } else {
                if (count($availableSeatsIds) > 0) {
                    $qb->andWhere($qb->expr()->not($condition));
                }
            }
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * Процедура для получения списка Id свободных места сеанса
     *
     * @param string $showId Id сеанса
     * @return int[]
     */
    public function getAvailableSeatsIds($showId)
    {
        $conn = $this->getEntityManager()->getConnection();

        $query = $conn->prepare('select * from GETAVAILABLESEATS(:show)');
        $query->execute([
            ':show' => $showId,
        ]);

        $result = [];
        foreach ($query->fetchAll(\PDO::FETCH_ASSOC) as $row) {
            if (array_key_exists('KP', $row)) {
                $result[] = (int) $row['KP'];
            }
        };

        return $result;
    }
}

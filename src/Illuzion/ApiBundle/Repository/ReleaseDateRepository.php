<?php

namespace Illuzion\ApiBundle\Repository;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Illuzion\ApiBundle\Entity\Shared\ReleaseDate;

/**
 * Репозиторий дат старта продаж
 */
class ReleaseDateRepository extends EntityRepository
{
    /**
     * Удалить дату старта
     * @param ReleaseDate $date
     */
    public function remove(ReleaseDate $date)
    {
        $this->getEntityManager()->remove($date);
    }

    /**
     * Удалить все даты старта для фильма
     * @param string $movieId id фильма
     */
    public function removeAllForMovie($movieId)
    {
        $this->createQueryBuilder('release_date')
            ->delete()
            ->where('release_date.movieId = :id')
            ->setParameter('id', $movieId)
            ->getQuery()
            ->execute();
    }

    /**
     * Сохранить список дат старта
     * @param ArrayCollection|ReleaseDate[] $collection
     */
    public function saveDates(ArrayCollection $collection)
    {
        foreach ($collection as $date) {
            $this->getEntityManager()->persist($date);
        }
        $this->getEntityManager()->flush();
    }
}

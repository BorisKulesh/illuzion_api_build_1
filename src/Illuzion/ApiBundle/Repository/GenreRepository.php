<?php

namespace Illuzion\ApiBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Illuzion\ApiBundle\Entity\Main\Genre;
use Illuzion\ApiBundle\Filter\GenreFilter;

/**
 * Репозиторий жанров
 */
class GenreRepository extends EntityRepository
{
    /**
     * Поиск всех жанров по фильтру
     *
     * @param GenreFilter $filter
     * @return Genre[]
     */
    public function findAllByFilter(GenreFilter $filter)
    {
        $qb = $this->createQueryBuilder('genre');

        if (null !== ($search = $filter->getSearch())) {
            $search = mb_strtoupper($search);
            $search = iconv('UTF-8', 'windows-1251//TRANSLIT//IGNORE', $search);
            $qb->andWhere(
                $qb->expr()->like('RUPPER(genre.title)', "'%{$search}%'")
            );
        }

        return $qb->getQuery()->getResult();
    }
}

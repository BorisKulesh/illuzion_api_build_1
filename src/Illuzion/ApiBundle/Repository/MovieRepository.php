<?php

namespace Illuzion\ApiBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Illuzion\ApiBundle\Entity\Main\Movie;
use Illuzion\ApiBundle\Filter\MovieFilter;

/**
 * Репозиторий фильмов
 */
class MovieRepository extends EntityRepository
{
    /**
     * Кол-во фильмов для фильтра
     * (игнорирует пагинацию)
     *
     * @param MovieFilter $filter
     * @return int
     */
    public function countByFilter(MovieFilter $filter)
    {
        $qb = $this->createQueryBuilderWithFilter($filter);
        $qb->select('COUNT(movie)');

        return (int) $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * Найти все фильмы по фильтру
     *
     * @param MovieFilter $filter
     * @return Movie[]
     */
    public function findAllByFilter(MovieFilter $filter)
    {
        $qb = $this->createQueryBuilderWithFilter($filter);

        if (null !== ($pagination = $filter->getPagination())) {
            $qb ->setMaxResults($pagination->getLimit())
                ->setFirstResult($pagination->getOffset());
        }

        $result = $qb->getQuery()->getResult();

        $ids = [];
        foreach ($result as $movie)
        {
            /** @var Movie $movie */
            $ids[] = $movie->getId();
        }

        // Жанры запрашиваем отдельным запросом для оптимизации
        $this->createQueryBuilder('movie')
            ->leftJoin('movie.genres', 'genre')
            ->select('PARTIAL movie.{id, localId}', 'genre')
            ->where($qb->expr()->in('movie.id', $ids))
            ->getQuery()
            ->getResult();

        // Форматы запрашиваем отдельным запросом для оптимизации
        $this->createQueryBuilder('movie')
            ->leftJoin('movie.formats', 'format')
            ->select('PARTIAL movie.{id, localId}', 'format')
            ->where($qb->expr()->in('movie.id', $ids))
            ->getQuery()
            ->getResult();

        return $result;
    }

    /**
     * Сохранить фильм
     * @param Movie $movie
     */
    public function save(Movie $movie)
    {
        if ($movie->getId()) {
            // Измененик
            $this->getEntityManager()->flush($movie);
            $this->getEntityManager()->refresh($movie);
        } else {
            // Сохранение. Происходит в 2 этапа, чтобы правильно отрабатывал триггер в базе
            $genres = $movie->getGenres()->toArray();

            $movie->setGenres([]);
            $this->getEntityManager()->persist($movie);
            $this->getEntityManager()->flush($movie);
            $this->getEntityManager()->refresh($movie);

            $movie->setGenres($genres);
            $this->getEntityManager()->flush($movie);
            $this->getEntityManager()->refresh($movie);
        }
    }

    /**
     * Удаить фильм
     * @param Movie $movie
     */
    public function delete(Movie $movie)
    {
        $this->getEntityManager()->remove($movie);
        $this->getEntityManager()->flush();
    }

    /**
     * Получить список фильмов для обновления поля archived
     *
     * @param bool $scanAll Получаем все или только за последний месяц
     * @return Movie[]
     */
    public function getAllForArchivation($scanAll = false)
    {
        $qb = $this->createQueryBuilder('movie');

        if (!$scanAll) {
            $qb ->andWhere('movie.startDate > :date')
                ->setParameter('date', new \DateTime('-1 month'));
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * Преобразовывает фильтр в запрос
     *
     * @param MovieFilter $filter
     * @return \Doctrine\ORM\QueryBuilder
     */
    protected function createQueryBuilderWithFilter(MovieFilter $filter)
    {
        $qb = $this->createQueryBuilder('movie');

        if (null !== ($status = $filter->getStatus())) {
            switch ($status) {
                case 'current':
                    $qb->andWhere($qb->expr()->orX(
                        'movie.archived = false',
                        $qb->expr()->isNull('movie.archived')
                    ));
                    break;
                case 'archived':
                    $qb->andWhere('movie.archived = true');
                    break;
            }
        }

        if (null !== ($search = $filter->getSearch())) {
            if (is_numeric($search)) {
                $qb ->andWhere('movie.id = :search')
                    ->setParameter('search', $search);
            } else {
                $search = mb_strtoupper($search);
                $search = iconv('UTF-8', 'windows-1251//TRANSLIT//IGNORE', $search);
                $qb->andWhere($qb->expr()->orX(
                    $qb->expr()->like('RUPPER(movie.alias)', "'%{$search}%'"),
                    $qb->expr()->like('RUPPER(movie.title)', "'%{$search}%'")
                ));
            }
        }

        if (null !== ($language = $filter->getLanguage())) {
            $qb ->andWhere('movie.language = :lang')
                ->setParameter('lang', $language);
        }

        if (null !== ($programId = $filter->getProgram())) {
            $qb ->andWhere('movie.program = :program')
                ->setParameter('program', $programId);
        }

        if (count($format = $filter->getFormat()) > 0) {
            $qb ->innerJoin('movie.formats', 'format')
                ->andWhere($qb->expr()->in('format.title', $format));
        }

        if (null !== ($range = $filter->getDateStartRange())) {
            if (null !== ($start = $range->getStart())) {
                $qb ->andWhere('movie.startDate >= :start_start')
                    ->setParameter('start_start', $start);
            }
            if (null !== ($end = $range->getEnd())) {
                $qb ->andWhere('movie.startDate <= :start_end')
                    ->setParameter('start_end', $end);
            }
        }

        return $qb;
    }
}

<?php

namespace Illuzion\ApiBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Illuzion\ApiBundle\Entity\Main\Order;
use Illuzion\ApiBundle\Filter\OrderFilter;

/**
 * Репозиторий для работы с заказами
 */
class OrderRepository extends EntityRepository
{
    /**
     * Кол-во заказов для фильтра
     * (игнорирует пагинацию)
     *
     * @param OrderFilter $filter
     * @return int
     */
    public function countByFilter(OrderFilter $filter)
    {
        $qb = $this->createQueryBuilderWithFilter($filter);
        $qb->select('COUNT(o)');

        return (int) $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * Найти все заказы по фильтру
     *
     * @param OrderFilter $filter
     * @return Order[]
     */
    public function findAllByFilter(OrderFilter $filter)
    {
        $qb = $this->createQueryBuilderWithFilter($filter);

        if (null !== ($pagination = $filter->getPagination())) {
            $qb ->setMaxResults($pagination->getLimit())
                ->setFirstResult($pagination->getOffset());
        }

        $qb->addOrderBy('o.purchaseDate', 'DESC');
        $result = $qb->getQuery()->getResult();

        // У оплаченных и не оплаченных заказов билеты подтягиваются из разных мест
        $idsPaid = [];
        $idsReserved = [];
        $ids = [];
        foreach ($result as $order)
        {
            /** @var Order $order */
            if ($order->getStatusStr() === 'paid') {
                $idsPaid[] = $order->getId();
            } else {
                $idsReserved[] = $order->getId();
            }
            $ids[] = $order->getId();
        }

        if (count($idsReserved) > 0) {
            $this->createQueryBuilder('o')
                ->leftJoin('o.tickets', 't')
                ->select('PARTIAL o.{id}', 't')
                ->where($qb->expr()->in('o.id', $idsReserved))
                ->getQuery()
                ->getResult();
        }

        if (count($idsPaid) > 0) {
            $this->createQueryBuilder('o')
                ->leftJoin('o.soldTickets', 't')
                ->select('PARTIAL o.{id}', 't')
                ->where($qb->expr()->in('o.id', $idsPaid))
                ->getQuery()
                ->getResult();
        }

        // Сеансы запрашиваем отдельным запросом для оптимизации
        if (count($ids) > 0) {
            $this->createQueryBuilder('o')
                ->leftJoin('o.show', 's')
                ->select('PARTIAL o.{id}', 's')
                ->where($qb->expr()->in('o.id', $ids))
                ->getQuery()
                ->getResult();
        }

        return $result;
    }

    /**
     * Проверяем занятость кода покупки
     *
     * @param string $code Проверяемый код
     * @param \DateTime $seanceDatetime Дата начала сеанса, на который будет покупка
     * @return bool
     */
    public function isPrintCodeValid($code, \DateTime $seanceDatetime)
    {
        $qb = $this
            ->createQueryBuilder('o')
            ->join('o.show', 'show')
            ->andWhere('o.printCode = :code')
            ->andWhere('show.datetime <= :date')
            ->setParameter('code', $code)
            ->setParameter('date', $seanceDatetime);

        return null === $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * Процедура для бронирования мест и создания заказа
     *
     * @param string $showId Id сеанса
     * @param string $seatId Id места
     * @param string $phone Телефон пользователя
     * @param string $email Почта пользователя
     * @param string|null $orderId Заказ к которому прикрепить бронь. Создает новый если null
     *
     * @return int Id заказа в базе кинотеатра
     *
     * @throws \Exception
     */
    public function blockSeat($showId, $seatId, $phone, $email, $orderId = null)
    {
        $conn = $this->getEntityManager()->getConnection();

        $query = $conn->prepare('select * from BLOCKSEATS_WC(:key_sean, :key_place, :res, :phone, :email)');
        $query->execute([
            ':key_sean' => (int) $showId,
            ':key_place' => (int) $seatId,
            ':res' => null === $orderId ? null : (int) $orderId,
            ':phone' => $phone,
            ':email' => $email,
        ]);
        $result = $query->fetch(\PDO::FETCH_ASSOC);
        if (false === $result) {
            $info = $conn->errorInfo();
            $data = array_key_exists('message', $info) ? $info['message'] : $conn->errorCode();
            throw new \Exception($data);
        }
        $orderId = $result['IDRESERV'];

        if ($orderId === 0) {
            throw new \Exception('Reservation not created');
        }

        return $orderId;
    }

    /**
     * Процедура для оплаты заказа
     *
     * @param string $orderId Id заказа в базе кинотеатра
     *
     * @return mixed
     * @throws \Exception
     */
    public function internetSale($orderId)
    {
        $conn = $this->getEntityManager()->getConnection();

        $query = $conn->prepare('select * from internet_sale(:res)');
        $query->execute([
            ':res' => $orderId,
        ]);

        $result = $query->fetch(\PDO::FETCH_ASSOC);
        if (false === $result) {
            $info = $conn->errorInfo();
            $data = array_key_exists('message', $info) ? $info['message'] : $conn->errorCode();
            throw new \Exception($data);
        }
        return $result;
    }

    /**
     * Процедура для оплаты заказа бонусами
     *
     * @param string $orderId Id заказа в базе кинотеатра
     * @param string $cardNumber Номер карты пользователя
     * @return mixed
     * @throws \Exception
     */
    public function internetSaleBonus($orderId, $cardNumber)
    {
        $conn = $this->getEntityManager()->getConnection();

        $query = $conn->prepare('select * from internet_sale_bonus(:res, :sc)');
        $query->execute([
            ':res' => $orderId,
            ':sc' => $cardNumber,
        ]);

        $result = $query->fetch(\PDO::FETCH_ASSOC);
        if (false === $result) {
            $info = $conn->errorInfo();
            $data = array_key_exists('message', $info) ? $info['message'] : $conn->errorCode();
            throw new \Exception($data);
        }
        return $result;
    }

    /**
     * Процедура для оплаты заказа при наличии карты пользователя
     *
     * @param string $orderId Id заказа в базе кинотеатра
     * @param string $cardNumber Номер карты пользователя
     * @throws \Exception
     */
    public function internetCardSale($orderId, $cardNumber)
    {
        $conn = $this->getEntityManager()->getConnection();

        $query = $conn->prepare('select * from internet_card_sale(:res, :kpz)');
        $query->execute([
            ':res' => $orderId,
            ':kpz' => $cardNumber,
        ]);

        $result = $query->fetch(\PDO::FETCH_ASSOC);
        if (false === $result) {
            $info = $conn->errorInfo();
            $data = array_key_exists('message', $info) ? $info['message'] : $conn->errorCode();
            throw new \Exception($data);
        }
    }

    /**
     * Процедура для снятия брони
     *
     * @param string $orderId Id заказа в базе кинотеатра
     * @return mixed
     * @throws \Exception
     */
    public function freeTempReserv($orderId)
    {
        $conn = $this->getEntityManager()->getConnection();

        $query = $conn->prepare('select * from free_temp_reserv(:res)');
        $query->execute([
            ':res' => $orderId,
        ]);

        $result = $query->fetch(\PDO::FETCH_ASSOC);
        if (false === $result) {
            $info = $conn->errorInfo();
            $data = array_key_exists('message', $info) ? $info['message'] : $conn->errorCode();
            throw new \Exception($data);
        }
        return $result;
    }

    /**
     * Процедура для отмены заказа
     *
     * @param string $orderId Код покупки
     *
     * @throws \Exception
     */
    public function moneybackInternet($orderId)
    {
        $conn = $this->getEntityManager()->getConnection();

        $query = $conn->prepare('select * from MONEYBACK_INTERNET_FOR_API(:orderId)');
        $query->execute([
            ':orderId' => $orderId,
        ]);

        $result = $query->fetch(\PDO::FETCH_ASSOC);
        if (false === $result) {
            $info = $conn->errorInfo();
            $data = array_key_exists('message', $info) ? $info['message'] : $conn->errorCode();
            throw new \Exception($data);
        }
    }

    /**
     * Процедура для снятия неоплаченных броней
     *
     * @return int[] список отмененных броней
     * @throws \Exception
     */
    public function cancelTempReserv()
    {
        $conn = $this->getEntityManager()->getConnection();

        $query = $conn->prepare('select * from CANCEL_TEMP_RESERV');
        $query->execute();

        $result = $query->fetchAll(\PDO::FETCH_ASSOC);
        if (false === $result) {
            $info = $conn->errorInfo();
            $data = array_key_exists('message', $info) ? $info['message'] : $conn->errorCode();
            throw new \Exception($data);
        }

        $arr = [];
        foreach ($result as $row) {
            if (array_key_exists('RESID', $row)) {
                $arr[] = $row['RESID'];
            }
        }

        return $arr;
    }

    /**
     * Обновить данных о заказе из базы
     * @param Order $order Заказ
     */
    public function refresh(Order $order)
    {
        $this->getEntityManager()->refresh($order);
    }

    /**
     * Сохранить заказ
     * @param Order $order Заказ
     * @param bool $refresh Обновить данные после сохранения
     */
    public function save(Order $order, $refresh = true)
    {
        $this->getEntityManager()->flush($order);
        if ($refresh) {
            $this->getEntityManager()->refresh($order);
        }
    }

    /**
     * Преобразовывает фильтр в запрос
     *
     * @param OrderFilter $filter
     * @return array|\Doctrine\ORM\QueryBuilder
     */
    protected function createQueryBuilderWithFilter(OrderFilter $filter)
    {
        $qb = $this->createQueryBuilder('o');

        if (null !== ($status = $filter->getStatus())) {
            $ids = Order::getStatusId($status);
            $qb->andWhere($qb->expr()->in('o.status', $ids));
        }

        if (null !== ($customer = $filter->getCustomer())) {
            $qb ->andWhere('o.customerId = :customer')
                ->setParameter('customer', $customer);
        }

        // Коды покупки актуальны в течении 2х месяцев
        if (null !== ($code = $filter->getPrintCode())) {
            $qb ->andWhere('o.printCode = :code')
                ->andWhere('o.purchaseDate > :date')
                ->setParameter('code', $code)
                ->setParameter('date', new \DateTime('-2 month'));
        }

        if (null !== ($client = $filter->getClient())) {
            $qb ->andWhere('o.client = :client')
                ->setParameter('client', $client);
        }

        return $qb;
    }
}

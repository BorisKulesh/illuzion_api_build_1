<?php

namespace Illuzion\ApiBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Illuzion\ApiBundle\Entity\Main\Program;
use Illuzion\ApiBundle\Filter\ProgramFilter;

/**
 * Репозиторий для программ
 */
class ProgramRepository extends EntityRepository
{
    /**
     * Поиск программ по фильтру
     *
     * @param ProgramFilter $filter
     *
     * @return Program[]
     */
    public function findAllByFilter(ProgramFilter $filter)
    {
        $qb = $this->createQueryBuilderWithFilter($filter);

        if (null !== ($pagination = $filter->getPagination())) {
            $qb ->setMaxResults($pagination->getLimit())
                ->setFirstResult($pagination->getOffset());
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * Кол-во программ для фильтра
     * (игнорирует пагинацию)
     *
     * @param ProgramFilter $filter
     *
     * @return int
     */
    public function countByFilter(ProgramFilter $filter)
    {
        $qb = $this->createQueryBuilderWithFilter($filter);
        $qb->select('COUNT(program)');

        return (int) $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * Сохранить программу
     * @param Program $program
     */
    public function save(Program $program)
    {
        $this->getEntityManager()->persist($program);
        $this->getEntityManager()->flush($program);
        $this->getEntityManager()->refresh($program);
    }

    /**
     * Удалить программу
     * @param Program $program
     */
    public function delete(Program $program)
    {
        $this->getEntityManager()->remove($program);
        $this->getEntityManager()->flush();
    }

    /**
     * Преобразовывает фильтр в запрос
     *
     * @param ProgramFilter $filter
     * @return \Doctrine\ORM\QueryBuilder
     */
    protected function createQueryBuilderWithFilter(ProgramFilter $filter)
    {
        $qb = $this->createQueryBuilder('program');

        if (null !== ($published = $filter->getPublished())) {
            $qb ->andWhere('program.published= :pub')
                ->setParameter('pub', $published);
        }

        if (null !== ($city = $filter->getCity())) {
            $qb ->andWhere('program.city = :city')
                ->setParameter('city', $city);
        }

        if (null !== ($search = $filter->getSearch())) {
            $search = mb_strtoupper($search);
            $search = iconv('UTF-8', 'windows-1251//TRANSLIT//IGNORE', $search);
            $qb->andWhere($qb->expr()->orX(
                $qb->expr()->like('RUPPER(program.alias)', "'%{$search}%'"),
                $qb->expr()->like('RUPPER(program.title)', "'%{$search}%'")
            ));
        }

        return $qb;
    }
}

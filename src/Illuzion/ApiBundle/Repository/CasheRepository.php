<?php

namespace Illuzion\ApiBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Illuzion\ApiBundle\Entity\Main\Cashe;

/**
 * Нечто, что нужно закрывать, чтобы работала покупка
 */
class CasheRepository extends EntityRepository
{
    /**
     * Нечто, что нужно закрывать, чтобы работала покупка
     * Логика сделана со слов разработчика базы
     */
    public function ensureCasheClosed()
    {
        /** @var Cashe[] $cashe */
        $cashe = $this->findBy([
            'active' => true,
            'operator' => 'INTERNET',
        ]);

        $date = (new \DateTime())
            ->setTime(0, 0, 0)
            ->format('Y-m-d H:i:s');

        foreach ($cashe as $item) {
            if ($item->getDate() !== $date) {
                $item
                    ->setActive(false)
                    ->setLastTick(1);
            }
        }
        $this->getEntityManager()->flush();
    }
}

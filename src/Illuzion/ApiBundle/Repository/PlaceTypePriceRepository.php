<?php

namespace Illuzion\ApiBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Illuzion\ApiBundle\Entity\Main\PlaceTypePrice;

/**
 * Репозиторий ценовых схем
 */
class PlaceTypePriceRepository extends EntityRepository
{
    static $TYPE_NAMES = [
        1 => 'vip',
        2 => 'beatbox',
        3 => 'simple',
        4 => 'sofa',
    ];

    /**
     * Получить ценовую схему
     *
     * @param int $schemeId Id ценовой схемы
     * @return int[] Тип места  => цена
     */
    public function getPriceMap($schemeId)
    {
        /** @var PlaceTypePrice[] $prices */
        $prices = $this
            ->createQueryBuilder('p')
            ->andWhere('p.schemeId = :id')
            ->setParameter('id', $schemeId)
            ->getQuery()
            ->useResultCache(true, 600)
            ->getResult();

        $map = [];

        foreach ($prices as $price) {
            if ($price->getSeatType() > 4) {
                continue;
            }

            $map[self::$TYPE_NAMES[$price->getSeatType()]] = $price->getPrice();
        }

        return $map;
    }
}

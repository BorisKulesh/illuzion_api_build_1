<?php

namespace Illuzion\ApiBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Репозиторий затов
 */
class HallRepository extends EntityRepository
{
    /**
     * @inheritdoc
     */
    public function findAll()
    {
        // Не достаем не активные залы
        return $this->createQueryBuilder('hall')
            ->andWhere('hall.activity = true')
            ->getQuery()
            ->useResultCache(true)
            ->getResult();
    }
}

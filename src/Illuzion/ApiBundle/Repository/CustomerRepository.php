<?php

namespace Illuzion\ApiBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Illuzion\ApiBundle\Entity\Customer\Customer;
use Illuzion\ApiBundle\Entity\Customer\DiscountCard;
use Illuzion\ApiBundle\Filter\CustomerFilter;

/**
 * Репозиторий пользователей
 */
class CustomerRepository extends EntityRepository
{
    /**
     * Кол-во пользователей для фильтра
     * (игнорирует пагинацию)
     *
     * @param CustomerFilter $filter
     * @return int
     */
    public function countByFilter(CustomerFilter $filter)
    {
        $qb = $this->createQueryBuilderWithFilter($filter);
        $qb->select('COUNT(customer)');

        return (int) $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * Найти всех пользователей по фильтру
     *
     * @param CustomerFilter $filter
     * @return Customer[]
     */
    public function findAllByFilter(CustomerFilter $filter)
    {
        $qb = $this->createQueryBuilderWithFilter($filter);
        $qb->select('customer', 'card');

        if (null !== ($pagination = $filter->getPagination())) {
            $qb ->setMaxResults($pagination->getLimit())
                ->setFirstResult($pagination->getOffset());
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * Найти пользователя по id или номеру карты
     *
     * @param $id
     * @return Customer
     */
    public function findOneByIdOrCardNumber($id)
    {
        $qb = $this->createQueryBuilder('customer');
        $qb ->innerJoin( // Мы работаетм только с поддеживаемыми картами
                'customer.discountCards',
                'card',
                Join::WITH,
                $this->getCardPrefixFilter()
            )
            ->select('customer', 'card')
            ->orWhere('customer.id = :id')
            ->orWhere('card.number = :id')
            ->setParameter('id', $id);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * Найти пользователя по почте или номеру карты
     *
     * @param $login
     * @return Customer
     */
    public function findOneByEmailOrCardNumber($login)
    {
        $qb = $this->createQueryBuilder('customer');
        $qb ->innerJoin( // Мы работаетм только с поддеживаемыми картами
                'customer.discountCards',
                'card',
                Join::WITH,
                $this->getCardPrefixFilter()
            )
            ->select('customer', 'card')
            ->orWhere('customer.email = :login')
            ->orWhere('card.number = :login')
            ->setParameter('login', $login);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * Проверка уникальности почты
     *
     * @param string $email
     * @return bool
     */
    public function isEmailUnique($email)
    {
        $qb = $this->createQueryBuilder('customer');
        $qb ->select('count(customer)')
            ->orWhere('customer.email = :email')
            ->setParameter('email', $email);

        $result = (int) $qb->getQuery()->getSingleScalarResult();
        return $result === 0;
    }

    /**
     * Проверка уникальности телефона
     *
     * @param string $phone
     * @return bool
     */
    public function isPhoneUnique($phone)
    {
        $qb = $this->createQueryBuilder('customer');
        $qb ->select('count(customer)')
            ->orWhere('customer.phone = :phone')
            ->setParameter('phone', $phone);

        $result = (int) $qb->getQuery()->getSingleScalarResult();
        return $result === 0;
    }

    /**
     * Сохранить пользователя
     * @param Customer $customer
     */
    public function save(Customer $customer)
    {
        $this->getEntityManager()->persist($customer);
        $this->getEntityManager()->flush($customer);
        $this->getEntityManager()->refresh($customer);
    }

    /**
     * Вызов процедуры изменения бонусов после окончания сеанса
     *
     * @param string $cardNumber
     * @param string $delta
     * @param \DateTime $seanceDatetime
     * @return bool
     * @throws \Exception
     */
    public function changeBonus($cardNumber, $delta, \DateTime $seanceDatetime)
    {
        $conn = $this->getEntityManager()->getConnection();

        $query = $conn->prepare('select * from change_bonus(:kpz, :bonus, 0, :dts)');
        $query->execute([
            ':kpz' => $cardNumber,
            ':bonus' => $delta,
            ':dts' => $seanceDatetime->format('Y-m-d H:i:s'),
        ]);
        $result = $query->fetch(\PDO::FETCH_ASSOC);
        if (false === $result) {
            $info = $conn->errorInfo();
            $data = array_key_exists('message', $info) ? $info['message'] : $conn->errorCode();
            throw new \Exception($data);
        }
        return $result;
    }

    /**
     * Вызов процедуры пополнения бонусов за покупку
     *
     * @param string $cardNumber
     * @param float $price
     * @param int $ticketsCount
     * @param \DateTime $seanceDatetime
     * @param int $seatType
     * @return bool
     * @throws \Exception
     */
    public function updateCardBonus($cardNumber, $price, $ticketsCount, \DateTime $seanceDatetime, $seatType)
    {
        $conn = $this->getEntityManager()->getConnection();

        $query = $conn->prepare('select * from update_card_bonus(:kpz, :dsc, :dscnt, :dts, :key_st)');
        $query->execute([
            ':kpz' => $cardNumber,
            ':dsc' => $price,
            ':dscnt' => $ticketsCount,
            ':dts' => $seanceDatetime->format('Y-m-d H:i:s'),
            ':key_st' => $seatType,
        ]);
        $result = $query->fetch(\PDO::FETCH_ASSOC);
        if (false === $result) {
            $info = $conn->errorInfo();
            $data = array_key_exists('message', $info) ? $info['message'] : $conn->errorCode();
            throw new \Exception($data);
        }
        return $result;
    }

    /**
     * Преобразовывает фильтр в запрос
     *
     * @param CustomerFilter $filter
     * @return \Doctrine\ORM\QueryBuilder
     */
    protected function createQueryBuilderWithFilter(CustomerFilter $filter)
    {
        $qb = $this->createQueryBuilder('customer');
        $qb ->innerJoin(
            'customer.discountCards',
            'card',
            Join::WITH,
            $this->getCardPrefixFilter($filter->getType())
        )
            ->andWhere('card.datetimeReg IS NOT NULL')
        ;

        if (null !== ($search = $filter->getSearch())) {
            if (is_numeric($search)) {
                $qb->andWhere($qb->expr()->orX(
                    $qb->expr()->like('customer.name', "'%{$search}%'"),
                    $qb->expr()->like('customer.phone', "'%{$search}%'"),
                    $qb->expr()->eq('card.number', "'$search'")
                ));
            } else {
                $search = mb_strtoupper($search);
                $search = iconv('UTF-8', 'windows-1251//TRANSLIT//IGNORE', $search);
                $qb->andWhere($qb->expr()->orX(
                    $qb->expr()->like('RUPPER(customer.email)', "'%{$search}%'"),
                    $qb->expr()->like('RUPPER(customer.name)', "'%{$search}%'")
                ));
            }
        }

        if (null !== ($blocked = $filter->getBlocked())) {
            if ($blocked) {
                $qb->andWhere('NOT card.activityLevel = 1');
            } else {
                $qb->andWhere('card.activityLevel = 1');
            }
        }

        return $qb;
    }

    /**
     * Генерирует условие, что карат имеет поддерживаемый тип
     *
     * @param string|null $type Конкретный типа. Все типы, если null
     * @return string Запрос
     */
    protected function getCardPrefixFilter($type = null)
    {
        $result = [];
        if ($type !== null && array_key_exists($type, DiscountCard::$TYPE_PREFIX_MAP)) {
            $typePrefix = DiscountCard::$TYPE_PREFIX_MAP[$type];
            $result[] = "card.number like '{$typePrefix}%'";
        } else {
            foreach (DiscountCard::$TYPE_PREFIX_MAP as $typePrefix) {
                $result[] = "card.number like '{$typePrefix}%'";
            }
        }

        return implode(' or ', $result);
    }
}

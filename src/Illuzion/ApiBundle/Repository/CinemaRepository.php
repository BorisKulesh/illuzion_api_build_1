<?php

namespace Illuzion\ApiBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Illuzion\ApiBundle\Entity\Shared\Cinema;
use Illuzion\ApiBundle\Filter\CinemaFilter;

/**
 * Репозиторий кинотеатров
 */
class CinemaRepository extends EntityRepository
{
    /**
     * Найти все кинотеатры по фильтру
     *
     * @param CinemaFilter $filter
     * @return Cinema[]
     */
    public function findAllByFilter(CinemaFilter $filter)
    {
        $qb = $this->createQueryBuilder('cinema');

        if (null !== ($city = $filter->getCity())) {
            $qb ->andWhere('cinema.city = :city')
                ->setParameter('city', $city);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * Найти кинотеатра по id или алиасу
     *
     * @param string $str id или алиас
     * @return Cinema
     */
    public function findOneByIdOrAlias($str)
    {
        $qb = $this->createQueryBuilder('cinema');

        $qb ->orWhere('cinema.id = :str')
            ->orWhere('cinema.alias = :str')
            ->setParameter('str', $str);

        return $qb->getQuery()->getOneOrNullResult();
    }
}

<?php

namespace Illuzion\SynchronizationBundle\Hanlder;

use Illuzion\SynchronizationBundle\Service\SyncHandler;

/**
 * Интерфейс для сервиса синхронизации сущности
 */
interface EntitySyncHandlerInterface
{
    /**
     * Сущность, которую сервис обрабатывает
     *
     * @return string
     */
    public function getSupportedEntityClass();

    /**
     * Получить запрос, по которому нужно будет искать сущность
     *
     * @param object $entity сущность для запроса
     * @return array запрос
     */
    public function getEntityQuery($entity);

    /**
     * Синхронизировать создание или сущности
     *
     * @param string $cinemaId Id кинотеатра
     * @param array $entityQuery запрос для сущности
     * @param bool $flush отправлять изменения в базу
     *
     * @return mixed Сущность или null в случае нейдачи
     */
    public function handleEntityPersist($cinemaId, $entityQuery, $flush = true);

    /**
     * Синхронизировать удаление сущности
     *
     * @param string $cinemaId Id кинотеатра
     * @param array $entityQuery запрос для сущности
     * @return boolean
     */
    public function handleEntityRemove($cinemaId, $entityQuery);

    /**
     * @param SyncHandler $handler
     */
    public function setSyncHandler(SyncHandler $handler);
}

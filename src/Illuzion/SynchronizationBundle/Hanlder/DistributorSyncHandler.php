<?php

namespace Illuzion\SynchronizationBundle\Hanlder;

use Illuzion\ApiBundle\Entity\Main\Distributor;
use Illuzion\ApiBundle\Provider\EntityManagerProvider;
use Illuzion\SynchronizationBundle\Service\SyncHandler;
use Psr\Log\LoggerInterface;

/**
 * Сервис для синхронизации программ
 */
class DistributorSyncHandler implements EntitySyncHandlerInterface
{
    /** @var EntityManagerProvider */
    protected $emProvider;

    /** @var SyncHandler */
    protected $syncHandler;

    /** @var LoggerInterface */
    protected $logger;

    /**
     * @param EntityManagerProvider $emProvider
     * @param LoggerInterface $logger
     */
    public function __construct(EntityManagerProvider $emProvider, LoggerInterface $logger)
    {
        $this->emProvider = $emProvider;
        $this->logger = $logger;
    }

    /**
     * @inheritDoc
     */
    public function getSupportedEntityClass()
    {
        return Distributor::class;
    }

    /**
     * @inheritDoc
     * @param Distributor $entity
     */
    public function getEntityQuery($entity)
    {
        return [
            'id' => $entity->getId(),
        ];
    }

    /**
     * @inheritDoc
     */
    public function handleEntityPersist($cinemaId, $entityQuery, $flush = true)
    {
        $mainEm = $this->emProvider->getMainEm();
        $cinemaEm = $this->emProvider->getMainEmForCinema($cinemaId);

        try {
            $distributor = $mainEm->getRepository('Main:Distributor')->findOneBy($entityQuery);
            $cinemaDistributor = $cinemaEm->getRepository('Main:Distributor')->findOneBy($entityQuery);

            if (!$cinemaDistributor) {
                $cinemaEm->persist($distributor);
                $cinemaDistributor = $distributor;
            } else {
                $cinemaEm->merge($distributor);
            }

            if ($flush) {
                $cinemaEm->flush($cinemaDistributor);
            }

            return $cinemaDistributor;
        } catch (\Exception $e) {
            $this->logger->error('Failed to persist Distributor', [
                'cinema' => $cinemaId,
                'query' => $entityQuery,
                'exception' => $e,
            ]);

            return null;
        }
    }

    /**
     * @inheritDoc
     */
    public function handleEntityRemove($cinemaId, $entityQuery)
    {
        $cinemaEm = $this->emProvider->getMainEmForCinema($cinemaId);

        try {
            $distributor = $cinemaEm->getRepository('Main:Distributor')->findOneBy($entityQuery);
            if ($distributor) {
                $cinemaEm->remove($distributor);
                $cinemaEm->flush();
            }
        } catch (\Exception $e) {
            $this->logger->error('Failed to remove Distributor', [
                'cinema' => $cinemaId,
                'query' => $entityQuery,
                'exception' => $e,
            ]);
            return false;
        } finally {
            $cinemaEm->clear();
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function setSyncHandler(SyncHandler $handler)
    {
        $this->syncHandler = $handler;
    }
}

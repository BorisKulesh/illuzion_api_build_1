<?php

namespace Illuzion\SynchronizationBundle\Hanlder;

use Illuzion\ApiBundle\Entity\Main\Program;
use Illuzion\ApiBundle\Provider\EntityManagerProvider;
use Illuzion\SynchronizationBundle\Service\SyncHandler;
use Psr\Log\LoggerInterface;

/**
 * Сервис для синхронизации программ
 */
class ProgramSyncHandler implements EntitySyncHandlerInterface
{
    /** @var EntityManagerProvider */
    protected $emProvider;

    /** @var SyncHandler */
    protected $syncHandler;

    /** @var LoggerInterface */
    protected $logger;

    /**
     * @param EntityManagerProvider $emProvider
     * @param LoggerInterface $logger
     */
    public function __construct(EntityManagerProvider $emProvider, LoggerInterface $logger)
    {
        $this->emProvider = $emProvider;
        $this->logger = $logger;
    }

    /**
     * @inheritDoc
     */
    public function getSupportedEntityClass()
    {
        return Program::class;
    }

    /**
     * @inheritDoc
     * @param Program $entity
     */
    public function getEntityQuery($entity)
    {
        return [
            'id' => $entity->getId(),
        ];
    }

    /**
     * @inheritDoc
     */
    public function handleEntityPersist($cinemaId, $entityQuery, $flush = true)
    {
        $mainEm = $this->emProvider->getMainEm();
        $cinemaEm = $this->emProvider->getMainEmForCinema($cinemaId);

        try {
            $program = $mainEm->getRepository('Main:Program')->findOneBy($entityQuery);
            $cinemaProgram = $cinemaEm->getRepository('Main:Program')->findOneBy($entityQuery);

            if (!$cinemaProgram) {
                $cinemaEm->persist($program);
                $cinemaProgram = $program;
            } else {
                $cinemaEm->merge($program);
            }

            $cinemaProgram->setParent(
                $this->syncHandler->loadCinemaEntity($cinemaId, $program->getParent())
            );

            if ($flush) {
                $cinemaEm->flush($cinemaProgram);
            }

            return $cinemaProgram;
        } catch (\Exception $e) {
            $this->logger->error('Failed to persist Program', [
                'cinema' => $cinemaId,
                'query' => $entityQuery,
                'exception' => $e,
            ]);

            return null;
        }
    }

    /**
     * @inheritDoc
     */
    public function handleEntityRemove($cinemaId, $entityQuery)
    {
        $cinemaEm = $this->emProvider->getMainEmForCinema($cinemaId);

        try {
            $program = $cinemaEm->getRepository('Main:Program')->findOneBy($entityQuery);
            if ($program) {
                $cinemaEm->remove($program);
                $cinemaEm->flush();
            }
        } catch (\Exception $e) {
            $this->logger->error('Failed to remove Program', [
                'cinema' => $cinemaId,
                'query' => $entityQuery,
                'exception' => $e,
            ]);
            return false;
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function setSyncHandler(SyncHandler $handler)
    {
        $this->syncHandler = $handler;
    }
}

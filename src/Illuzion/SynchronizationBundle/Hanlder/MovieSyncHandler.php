<?php

namespace Illuzion\SynchronizationBundle\Hanlder;

use Illuzion\ApiBundle\Entity\Main\Movie;
use Illuzion\ApiBundle\Entity\Shared\ReleaseDate;
use Illuzion\ApiBundle\Provider\EntityManagerProvider;
use Illuzion\SynchronizationBundle\Service\SyncHandler;
use Psr\Log\LoggerInterface;

/**
 * Сервис для синхронизации программ
 */
class MovieSyncHandler implements EntitySyncHandlerInterface
{
    /** @var EntityManagerProvider */
    protected $emProvider;

    /** @var SyncHandler */
    protected $syncHandler;

    /** @var LoggerInterface */
    protected $logger;

    /**
     * @param EntityManagerProvider $emProvider
     * @param LoggerInterface $logger
     */
    public function __construct(EntityManagerProvider $emProvider, LoggerInterface $logger)
    {
        $this->emProvider = $emProvider;
        $this->logger = $logger;
    }

    /**
     * @inheritDoc
     */
    public function getSupportedEntityClass()
    {
        return Movie::class;
    }

    /**
     * @inheritDoc
     * @param Movie $entity
     */
    public function getEntityQuery($entity)
    {
        return [
            'id' => $entity->getId(),
        ];
    }

    /**
     * @inheritDoc
     */
    public function handleEntityPersist($cinemaId, $entityQuery, $flush = true)
    {
        $mainEm = $this->emProvider->getMainEm();
        $cinemaEm = $this->emProvider->getMainEmForCinema($cinemaId);
        $sharedEm = $this->emProvider->getSharedEm();

        try {
            $movie = $mainEm->getRepository('Main:Movie')->findOneBy($entityQuery);
            $city = $sharedEm->getRepository('Shared:City')->findOneByCinema($cinemaId);
            if ($city) {
                /** @var ReleaseDate $releaseDate */
                $releaseDate = $sharedEm
                    ->getRepository('Shared:ReleaseDate')
                    ->findOneBy([
                        'movieId' => $movie->getId(),
                        'city' => $city,
                    ]);

                if ($releaseDate) {
                    $movie->setStartDate($releaseDate->getDatetime());
                } else {
                    $movie->setStartDate(null);
                }
            }

            $cinemaMovie = $cinemaEm->getRepository('Main:Movie')->findOneBy($entityQuery);
            if (!$cinemaMovie) {
                $movie->setLocalId(null);
                $cinemaEm->persist($movie);
                $cinemaMovie = $movie;
            } else {
                $movie->setLocalId($cinemaMovie->getLocalId());
                $cinemaEm->merge($movie);
            }

            $cinemaMovie->setProgram(
                $this->syncHandler->loadCinemaEntity($cinemaId, $movie->getProgram())
            );
            $cinemaMovie->setDistributor(
                $this->syncHandler->loadCinemaEntity($cinemaId, $movie->getDistributor())
            );

            if ($flush) {
                $cinemaEm->flush($cinemaMovie);
            }

            return $cinemaMovie;
        } catch (\Exception $e) {
            $this->logger->error('Failed to update Movie', [
                'cinema' => $cinemaId,
                'query' => $entityQuery,
                'exception' => $e,
            ]);
            return null;
        }
    }

    /**
     * @inheritDoc
     */
    public function handleEntityRemove($cinemaId, $entityQuery)
    {
        $cinemaEm = $this->emProvider->getMainEmForCinema($cinemaId);

        try {
            $movie = $cinemaEm->getRepository('Main:Movie')->findOneBy($entityQuery);
            if ($movie) {
                $cinemaEm->remove($movie);
                $cinemaEm->flush();
            }
        } catch (\Exception $e) {
            $this->logger->error('Failed to remove Movie', [
                'cinema' => $cinemaId,
                'query' => $entityQuery,
                'exception' => $e,
            ]);
            return false;
        } finally {
            $cinemaEm->clear();
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function setSyncHandler(SyncHandler $handler)
    {
        $this->syncHandler = $handler;
    }
}

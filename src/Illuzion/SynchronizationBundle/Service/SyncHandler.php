<?php

namespace Illuzion\SynchronizationBundle\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Util\ClassUtils;
use Illuzion\SynchronizationBundle\Hanlder\EntitySyncHandlerInterface;

/**
 * Сервис синхронизации сущности
 */
class SyncHandler
{
    const CLASS_PROPERTY = '__cls__';

    /** @var ArrayCollection|EntitySyncHandlerInterface[] словарь обработчиков */
    protected $entityHandlersMap;

    public function __construct()
    {
        $this->entityHandlersMap = new ArrayCollection();
    }

    /**
     * Получить запрос, по которому нужно будет искать сущность
     *
     * @param object $entity сущность для запроса
     * @return array запрос
     */
    public function getEntityQuery($entity)
    {
        $class = ClassUtils::getClass($entity);

        if ($handler = $this->entityHandlersMap->get($class)) {
            $query = $handler->getEntityQuery($entity);
            $query[self::CLASS_PROPERTY] = $class;
            return $query;
        }

        return null;
    }

    /**
     * Синхронизировать создание сущности
     *
     * @param string $cinemaId Id кинотеатра
     * @param array $entityQuery запрос для сущности
     * @return boolean
     */
    public function handleEntityPersist($cinemaId, $entityQuery)
    {
        if (!array_key_exists(self::CLASS_PROPERTY, $entityQuery)) {
            return false;
        }

        /** @var EntitySyncHandlerInterface $handler */
        $handler = $this->entityHandlersMap->get($entityQuery[self::CLASS_PROPERTY]);
        if (!$handler) {
            return false;
        }
        unset($entityQuery[self::CLASS_PROPERTY]);

        return $handler->handleEntityPersist($cinemaId, $entityQuery);
    }

    /**
     * Синхронизировать обновление
     *
     * @param string $cinemaId Id кинотеатра
     * @param array $entityQuery запрос для сущности
     * @return boolean
     */
    public function handleEntityUpdate($cinemaId, $entityQuery)
    {
        if (!array_key_exists(self::CLASS_PROPERTY, $entityQuery)) {
            return false;
        }

        /** @var EntitySyncHandlerInterface $handler */
        $handler = $this->entityHandlersMap->get($entityQuery[self::CLASS_PROPERTY]);
        if (!$handler) {
            return false;
        }
        unset($entityQuery[self::CLASS_PROPERTY]);

        return $handler->handleEntityPersist($cinemaId, $entityQuery);
    }

    /**
     * Синхронизировать удаление сущности
     *
     * @param string $cinemaId Id кинотеатра
     * @param array $entityQuery запрос для сущности
     * @return boolean
     */
    public function handleEntityRemove($cinemaId, $entityQuery)
    {
        if (!array_key_exists(self::CLASS_PROPERTY, $entityQuery)) {
            return false;
        }

        /** @var EntitySyncHandlerInterface $handler */
        $handler = $this->entityHandlersMap->get($entityQuery[self::CLASS_PROPERTY]);
        if (!$handler) {
            return false;
        }
        unset($entityQuery[self::CLASS_PROPERTY]);

        return $handler->handleEntityRemove($cinemaId, $entityQuery);
    }

    /**
     * Загрузить вариант сущности из кинотеатра, без изменений в базе
     *
     * @param int $cinemaId Id кинотеатра
     * @param object $entity Сущность
     *
     * @return mixed|null
     */
    public function loadCinemaEntity($cinemaId, $entity)
    {
        if (null === $entity) {
            return null;
        }

        $class = ClassUtils::getClass($entity);

        /** @var EntitySyncHandlerInterface $handler */
        if ($handler = $this->entityHandlersMap->get($class)) {
            $query = $handler->getEntityQuery($entity);
            return $handler->handleEntityPersist($cinemaId, $query, false);
        }

        return null;
    }

    /**
     * Регистрация нового обработчика
     *
     * @param EntitySyncHandlerInterface $handler
     */
    public function registerEntityHandler(EntitySyncHandlerInterface $handler)
    {
        $this->entityHandlersMap->set($handler->getSupportedEntityClass(), $handler);
        $handler->setSyncHandler($this);
    }
}

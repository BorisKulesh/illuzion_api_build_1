<?php

namespace Illuzion\SynchronizationBundle\Service;

use Illuzion\ApiBundle\Provider\EntityManagerProvider;
use Sonata\NotificationBundle\Consumer\ConsumerEvent;
use Sonata\NotificationBundle\Consumer\ConsumerInterface;

/**
 * Обработчик отложенных сообщений по синхронизации сущностей
 */
class SyncConsumer implements ConsumerInterface
{
    CONST ACTION_PERSIST = 'persist';
    CONST ACTION_UPDATE = 'update';
    CONST ACTION_REMOVE = 'remove';

    /** @var SyncHandler */
    protected $syncHandler;

    /** @var EntityManagerProvider */
    protected $emProvider;

    /**
     * @param SyncHandler $syncHandler
     * @param EntityManagerProvider $emProvider
     */
    public function __construct(SyncHandler $syncHandler, EntityManagerProvider $emProvider)
    {
        $this->syncHandler = $syncHandler;
        $this->emProvider = $emProvider;
    }

    /**
     * @inheritdoc
     */
    public function process(ConsumerEvent $event)
    {
        $message = $event->getMessage();

        $cinemaId = $message->getValue('cinema');
        $action = $message->getValue('action');
        $query = $message->getValue('query');

        if (!$cinemaId || !$action || !$query) {
            throw new \InvalidArgumentException('Message is invalid');
        }

        $this->emProvider->resetLoadedManagers();

        $result = true;
        switch ($action) {
            case self::ACTION_PERSIST:
                $result = $this->syncHandler->handleEntityPersist($cinemaId, $query);
                break;
            case self::ACTION_UPDATE:
                $result = $this->syncHandler->handleEntityUpdate($cinemaId, $query);
                break;
            case self::ACTION_REMOVE:
                $result = $this->syncHandler->handleEntityRemove($cinemaId, $query);
                break;
        }

        if (!$result) {
            throw new \Exception('Failed to sync entity');
        }
    }
}

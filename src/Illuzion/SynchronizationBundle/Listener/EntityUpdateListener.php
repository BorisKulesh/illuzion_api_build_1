<?php

namespace Illuzion\SynchronizationBundle\Listener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Illuzion\ApiBundle\Provider\EntityManagerProvider;
use Illuzion\SynchronizationBundle\Service\SyncConsumer;
use Illuzion\SynchronizationBundle\Service\SyncHandler;
use Sonata\NotificationBundle\Backend\BackendInterface;

/**
 * Здесь мы слушаем все запросы к базе и посылам отложенные сообщения об изменениях
 */
class EntityUpdateListener
{
    /** @var SyncHandler */
    protected $syncHandler;

    /** @var BackendInterface */
    protected $backend;

    /** @var string[] */
    protected $supportedCinemas;

    /**
     * @param SyncHandler $syncHandler
     * @param BackendInterface $backend
     * @param \string[] $supportedCinemas
     */
    public function __construct(SyncHandler $syncHandler, BackendInterface $backend, array $supportedCinemas)
    {
        $this->syncHandler = $syncHandler;
        $this->backend = $backend;
        $this->supportedCinemas = $supportedCinemas;
    }

    /**
     * Если была создана новая сущность
     *
     * @param LifecycleEventArgs $event
     */
    public function postPersist(LifecycleEventArgs $event)
    {
        $entity = $event->getEntity();
        $this->publish($entity, SyncConsumer::ACTION_PERSIST);
    }

    /**
     * Если была изменена сущность
     *
     * @param LifecycleEventArgs $event
     */
    public function postUpdate(LifecycleEventArgs $event)
    {
        $entity = $event->getEntity();
        $this->publish($entity, SyncConsumer::ACTION_UPDATE);
    }

    /**
     * Если была удалена сущность
     *
     * @param LifecycleEventArgs $event
     */
    public function postRemove(LifecycleEventArgs $event)
    {
        $entity = $event->getEntity();
        $this->publish($entity, SyncConsumer::ACTION_REMOVE);
    }

    /**
     * Создать и отправить отложенное сообщение для сущности
     *
     * @param object $entity сущность
     * @param string $action тип сообщения - создание/изменение/удаление
     */
    protected function publish($entity, $action)
    {
        $query = $this->syncHandler->getEntityQuery($entity);
        if (null === $query) {
            return;
        }

        $msg = [
            'action' => $action,
            'query' => $query,
        ];

        foreach ($this->supportedCinemas as $cinema) {
            if ($cinema === EntityManagerProvider::DEFAULT_EM) {
                continue;
            }
            $msg['cinema'] = $cinema;
            $this->backend->createAndPublish('sync', $msg);
        }
    }
}

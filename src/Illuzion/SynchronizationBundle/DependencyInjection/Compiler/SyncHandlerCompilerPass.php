<?php

namespace Illuzion\SynchronizationBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class SyncHandlerCompilerPass implements CompilerPassInterface
{
    /**
     * You can modify the container here before it is dumped to PHP code.
     *
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        $containerDefinition = $container->findDefinition('synchronization.handler');
        $providers = $container->findTaggedServiceIds('synchronization.entity_handler');

        foreach ($providers as $id => $tags) {
            $containerDefinition->addMethodCall('registerEntityHandler', [new Reference($id)]);
        }
    }
}

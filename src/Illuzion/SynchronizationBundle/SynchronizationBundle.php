<?php

namespace Illuzion\SynchronizationBundle;

use Illuzion\SynchronizationBundle\DependencyInjection\Compiler\SyncHandlerCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class SynchronizationBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(new SyncHandlerCompilerPass());
    }
}

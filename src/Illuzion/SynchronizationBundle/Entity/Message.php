<?php

namespace Illuzion\SynchronizationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Sonata\NotificationBundle\Entity\BaseMessage;

/**
 * Сущность для отложенного сообщения
 *
 * @ORM\Entity()
 */
class Message extends BaseMessage
{
    /**
     * @var int $id
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }
}

<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;
use Symfony\Component\DependencyInjection\Exception\LogicException;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;
use Symfony\Component\DependencyInjection\ParameterBag\FrozenParameterBag;

/**
 * appSyncDebugProjectContainer.
 *
 * This class has been auto-generated
 * by the Symfony Dependency Injection Component.
 *
 * @final since Symfony 3.3
 */
class appSyncDebugProjectContainer extends Container
{
    private $parameters;
    private $targetDirs = array();

    /**
     * Constructor.
     */
    public function __construct()
    {
        $dir = __DIR__;
        for ($i = 1; $i <= 5; ++$i) {
            $this->targetDirs[$i] = $dir = dirname($dir);
        }
        $this->parameters = $this->getDefaultParameters();

        $this->services = array();
        $this->normalizedIds = array(
            'symfony\\component\\security\\core\\authorization\\authorizationcheckerinterface' => 'Symfony\\Component\\Security\\Core\\Authorization\\AuthorizationCheckerInterface',
            'symfony\\component\\security\\core\\encoder\\userpasswordencoderinterface' => 'Symfony\\Component\\Security\\Core\\Encoder\\UserPasswordEncoderInterface',
        );
        $this->methodMap = array(
            '1_5bc971da440047f3dbeaf372c109d1b5c64861934e6dbd155bbae3c432cf847d' => 'get15bc971da440047f3dbeaf372c109d1b5c64861934e6dbd155bbae3c432cf847dService',
            '2_5bc971da440047f3dbeaf372c109d1b5c64861934e6dbd155bbae3c432cf847d' => 'get25bc971da440047f3dbeaf372c109d1b5c64861934e6dbd155bbae3c432cf847dService',
            '8464c6758298cf75d30c4f689fb7886d' => 'get8464c6758298cf75d30c4f689fb7886dService',
            'annotation_reader' => 'getAnnotationReaderService',
            'annotations.reader' => 'getAnnotations_ReaderService',
            'api.command.cancel_temp_reservations' => 'getApi_Command_CancelTempReservationsService',
            'api.command.generate_jws_token' => 'getApi_Command_GenerateJwsTokenService',
            'api.command.movie_scan_for_archived' => 'getApi_Command_MovieScanForArchivedService',
            'api.controller.cinema' => 'getApi_Controller_CinemaService',
            'api.controller.city' => 'getApi_Controller_CityService',
            'api.controller.customer' => 'getApi_Controller_CustomerService',
            'api.controller.distributor' => 'getApi_Controller_DistributorService',
            'api.controller.genre' => 'getApi_Controller_GenreService',
            'api.controller.hall' => 'getApi_Controller_HallService',
            'api.controller.movie' => 'getApi_Controller_MovieService',
            'api.controller.news' => 'getApi_Controller_NewsService',
            'api.controller.order' => 'getApi_Controller_OrderService',
            'api.controller.program' => 'getApi_Controller_ProgramService',
            'api.controller.seat' => 'getApi_Controller_SeatService',
            'api.controller.show' => 'getApi_Controller_ShowService',
            'api.controller.swagger' => 'getApi_Controller_SwaggerService',
            'api.form.customer_type' => 'getApi_Form_CustomerTypeService',
            'api.form.movie_type' => 'getApi_Form_MovieTypeService',
            'api.form.relation_type' => 'getApi_Form_RelationTypeService',
            'api.listener.local_entity' => 'getApi_Listener_LocalEntityService',
            'api.listner.api_exception' => 'getApi_Listner_ApiExceptionService',
            'api.listner.json_transformer' => 'getApi_Listner_JsonTransformerService',
            'api.listner.params_validation' => 'getApi_Listner_ParamsValidationService',
            'api.params.transformer.scalar_array' => 'getApi_Params_Transformer_ScalarArrayService',
            'api.params.transformer_factory' => 'getApi_Params_TransformerFactoryService',
            'api.provider.em' => 'getApi_Provider_EmService',
            'api.security.jws_guard_authenticator' => 'getApi_Security_JwsGuardAuthenticatorService',
            'api.security.jws_payload_encoder' => 'getApi_Security_JwsPayloadEncoderService',
            'api.security.user_provider' => 'getApi_Security_UserProviderService',
            'api.serializer.container' => 'getApi_Serializer_ContainerService',
            'api.serializer.ecoder_options' => 'getApi_Serializer_EcoderOptionsService',
            'api.serializer.encoder' => 'getApi_Serializer_EncoderService',
            'api.serializer.factory' => 'getApi_Serializer_FactoryService',
            'api.serializer.schema.article' => 'getApi_Serializer_Schema_ArticleService',
            'api.serializer.schema.cinema' => 'getApi_Serializer_Schema_CinemaService',
            'api.serializer.schema.city' => 'getApi_Serializer_Schema_CityService',
            'api.serializer.schema.customer' => 'getApi_Serializer_Schema_CustomerService',
            'api.serializer.schema.distributor' => 'getApi_Serializer_Schema_DistributorService',
            'api.serializer.schema.genre' => 'getApi_Serializer_Schema_GenreService',
            'api.serializer.schema.hall' => 'getApi_Serializer_Schema_HallService',
            'api.serializer.schema.movie' => 'getApi_Serializer_Schema_MovieService',
            'api.serializer.schema.order' => 'getApi_Serializer_Schema_OrderService',
            'api.serializer.schema.program' => 'getApi_Serializer_Schema_ProgramService',
            'api.serializer.schema.seat' => 'getApi_Serializer_Schema_SeatService',
            'api.serializer.schema.show' => 'getApi_Serializer_Schema_ShowService',
            'api.serializer.schema.ticket' => 'getApi_Serializer_Schema_TicketService',
            'api.service.article' => 'getApi_Service_ArticleService',
            'api.service.cinema' => 'getApi_Service_CinemaService',
            'api.service.city' => 'getApi_Service_CityService',
            'api.service.customer' => 'getApi_Service_CustomerService',
            'api.service.distributor' => 'getApi_Service_DistributorService',
            'api.service.emailer' => 'getApi_Service_EmailerService',
            'api.service.genre' => 'getApi_Service_GenreService',
            'api.service.hall' => 'getApi_Service_HallService',
            'api.service.movie' => 'getApi_Service_MovieService',
            'api.service.order' => 'getApi_Service_OrderService',
            'api.service.place_type_price' => 'getApi_Service_PlaceTypePriceService',
            'api.service.program' => 'getApi_Service_ProgramService',
            'api.service.release_date' => 'getApi_Service_ReleaseDateService',
            'api.service.seat' => 'getApi_Service_SeatService',
            'api.service.show' => 'getApi_Service_ShowService',
            'argument_resolver.default' => 'getArgumentResolver_DefaultService',
            'argument_resolver.request' => 'getArgumentResolver_RequestService',
            'argument_resolver.request_attribute' => 'getArgumentResolver_RequestAttributeService',
            'argument_resolver.service' => 'getArgumentResolver_ServiceService',
            'argument_resolver.session' => 'getArgumentResolver_SessionService',
            'argument_resolver.variadic' => 'getArgumentResolver_VariadicService',
            'assets.context' => 'getAssets_ContextService',
            'assets.packages' => 'getAssets_PackagesService',
            'cache.annotations' => 'getCache_AnnotationsService',
            'cache.app' => 'getCache_AppService',
            'cache.default_clearer' => 'getCache_DefaultClearerService',
            'cache.global_clearer' => 'getCache_GlobalClearerService',
            'cache.system' => 'getCache_SystemService',
            'cache.validator' => 'getCache_ValidatorService',
            'cache_clearer' => 'getCacheClearerService',
            'cache_warmer' => 'getCacheWarmerService',
            'config_cache_factory' => 'getConfigCacheFactoryService',
            'console.command.symfony_bundle_securitybundle_command_userpasswordencodercommand' => 'getConsole_Command_SymfonyBundleSecuritybundleCommandUserpasswordencodercommandService',
            'console.error_listener' => 'getConsole_ErrorListenerService',
            'controller_name_converter' => 'getControllerNameConverterService',
            'debug.argument_resolver' => 'getDebug_ArgumentResolverService',
            'debug.controller_resolver' => 'getDebug_ControllerResolverService',
            'debug.debug_handlers_listener' => 'getDebug_DebugHandlersListenerService',
            'debug.event_dispatcher' => 'getDebug_EventDispatcherService',
            'debug.security.access.decision_manager' => 'getDebug_Security_Access_DecisionManagerService',
            'debug.stopwatch' => 'getDebug_StopwatchService',
            'deprecated.form.registry' => 'getDeprecated_Form_RegistryService',
            'doctrine' => 'getDoctrineService',
            'doctrine.dbal.che_customer_connection' => 'getDoctrine_Dbal_CheCustomerConnectionService',
            'doctrine.dbal.che_main_connection' => 'getDoctrine_Dbal_CheMainConnectionService',
            'doctrine.dbal.connection_factory' => 'getDoctrine_Dbal_ConnectionFactoryService',
            'doctrine.dbal.ill_customer_connection' => 'getDoctrine_Dbal_IllCustomerConnectionService',
            'doctrine.dbal.ill_main_connection' => 'getDoctrine_Dbal_IllMainConnectionService',
            'doctrine.dbal.logger' => 'getDoctrine_Dbal_LoggerService',
            'doctrine.dbal.nmega_customer_connection' => 'getDoctrine_Dbal_NmegaCustomerConnectionService',
            'doctrine.dbal.nmega_main_connection' => 'getDoctrine_Dbal_NmegaMainConnectionService',
            'doctrine.dbal.oc_customer_connection' => 'getDoctrine_Dbal_OcCustomerConnectionService',
            'doctrine.dbal.oc_main_connection' => 'getDoctrine_Dbal_OcMainConnectionService',
            'doctrine.dbal.shared_connection' => 'getDoctrine_Dbal_SharedConnectionService',
            'doctrine.dbal.uss_customer_connection' => 'getDoctrine_Dbal_UssCustomerConnectionService',
            'doctrine.dbal.uss_main_connection' => 'getDoctrine_Dbal_UssMainConnectionService',
            'doctrine.orm.che_customer_entity_listener_resolver' => 'getDoctrine_Orm_CheCustomerEntityListenerResolverService',
            'doctrine.orm.che_customer_entity_manager' => 'getDoctrine_Orm_CheCustomerEntityManagerService',
            'doctrine.orm.che_customer_entity_manager.property_info_extractor' => 'getDoctrine_Orm_CheCustomerEntityManager_PropertyInfoExtractorService',
            'doctrine.orm.che_customer_listeners.attach_entity_listeners' => 'getDoctrine_Orm_CheCustomerListeners_AttachEntityListenersService',
            'doctrine.orm.che_customer_manager_configurator' => 'getDoctrine_Orm_CheCustomerManagerConfiguratorService',
            'doctrine.orm.che_main_entity_listener_resolver' => 'getDoctrine_Orm_CheMainEntityListenerResolverService',
            'doctrine.orm.che_main_entity_manager' => 'getDoctrine_Orm_CheMainEntityManagerService',
            'doctrine.orm.che_main_entity_manager.property_info_extractor' => 'getDoctrine_Orm_CheMainEntityManager_PropertyInfoExtractorService',
            'doctrine.orm.che_main_listeners.attach_entity_listeners' => 'getDoctrine_Orm_CheMainListeners_AttachEntityListenersService',
            'doctrine.orm.che_main_manager_configurator' => 'getDoctrine_Orm_CheMainManagerConfiguratorService',
            'doctrine.orm.ill_customer_entity_listener_resolver' => 'getDoctrine_Orm_IllCustomerEntityListenerResolverService',
            'doctrine.orm.ill_customer_entity_manager' => 'getDoctrine_Orm_IllCustomerEntityManagerService',
            'doctrine.orm.ill_customer_entity_manager.property_info_extractor' => 'getDoctrine_Orm_IllCustomerEntityManager_PropertyInfoExtractorService',
            'doctrine.orm.ill_customer_listeners.attach_entity_listeners' => 'getDoctrine_Orm_IllCustomerListeners_AttachEntityListenersService',
            'doctrine.orm.ill_customer_manager_configurator' => 'getDoctrine_Orm_IllCustomerManagerConfiguratorService',
            'doctrine.orm.ill_main_entity_listener_resolver' => 'getDoctrine_Orm_IllMainEntityListenerResolverService',
            'doctrine.orm.ill_main_entity_manager' => 'getDoctrine_Orm_IllMainEntityManagerService',
            'doctrine.orm.ill_main_entity_manager.property_info_extractor' => 'getDoctrine_Orm_IllMainEntityManager_PropertyInfoExtractorService',
            'doctrine.orm.ill_main_listeners.attach_entity_listeners' => 'getDoctrine_Orm_IllMainListeners_AttachEntityListenersService',
            'doctrine.orm.ill_main_manager_configurator' => 'getDoctrine_Orm_IllMainManagerConfiguratorService',
            'doctrine.orm.naming_strategy.default' => 'getDoctrine_Orm_NamingStrategy_DefaultService',
            'doctrine.orm.nmega_customer_entity_listener_resolver' => 'getDoctrine_Orm_NmegaCustomerEntityListenerResolverService',
            'doctrine.orm.nmega_customer_entity_manager' => 'getDoctrine_Orm_NmegaCustomerEntityManagerService',
            'doctrine.orm.nmega_customer_entity_manager.property_info_extractor' => 'getDoctrine_Orm_NmegaCustomerEntityManager_PropertyInfoExtractorService',
            'doctrine.orm.nmega_customer_listeners.attach_entity_listeners' => 'getDoctrine_Orm_NmegaCustomerListeners_AttachEntityListenersService',
            'doctrine.orm.nmega_customer_manager_configurator' => 'getDoctrine_Orm_NmegaCustomerManagerConfiguratorService',
            'doctrine.orm.nmega_main_entity_listener_resolver' => 'getDoctrine_Orm_NmegaMainEntityListenerResolverService',
            'doctrine.orm.nmega_main_entity_manager' => 'getDoctrine_Orm_NmegaMainEntityManagerService',
            'doctrine.orm.nmega_main_entity_manager.property_info_extractor' => 'getDoctrine_Orm_NmegaMainEntityManager_PropertyInfoExtractorService',
            'doctrine.orm.nmega_main_listeners.attach_entity_listeners' => 'getDoctrine_Orm_NmegaMainListeners_AttachEntityListenersService',
            'doctrine.orm.nmega_main_manager_configurator' => 'getDoctrine_Orm_NmegaMainManagerConfiguratorService',
            'doctrine.orm.oc_customer_entity_listener_resolver' => 'getDoctrine_Orm_OcCustomerEntityListenerResolverService',
            'doctrine.orm.oc_customer_entity_manager' => 'getDoctrine_Orm_OcCustomerEntityManagerService',
            'doctrine.orm.oc_customer_entity_manager.property_info_extractor' => 'getDoctrine_Orm_OcCustomerEntityManager_PropertyInfoExtractorService',
            'doctrine.orm.oc_customer_listeners.attach_entity_listeners' => 'getDoctrine_Orm_OcCustomerListeners_AttachEntityListenersService',
            'doctrine.orm.oc_customer_manager_configurator' => 'getDoctrine_Orm_OcCustomerManagerConfiguratorService',
            'doctrine.orm.oc_main_entity_listener_resolver' => 'getDoctrine_Orm_OcMainEntityListenerResolverService',
            'doctrine.orm.oc_main_entity_manager' => 'getDoctrine_Orm_OcMainEntityManagerService',
            'doctrine.orm.oc_main_entity_manager.property_info_extractor' => 'getDoctrine_Orm_OcMainEntityManager_PropertyInfoExtractorService',
            'doctrine.orm.oc_main_listeners.attach_entity_listeners' => 'getDoctrine_Orm_OcMainListeners_AttachEntityListenersService',
            'doctrine.orm.oc_main_manager_configurator' => 'getDoctrine_Orm_OcMainManagerConfiguratorService',
            'doctrine.orm.quote_strategy.default' => 'getDoctrine_Orm_QuoteStrategy_DefaultService',
            'doctrine.orm.shared_entity_listener_resolver' => 'getDoctrine_Orm_SharedEntityListenerResolverService',
            'doctrine.orm.shared_entity_manager' => 'getDoctrine_Orm_SharedEntityManagerService',
            'doctrine.orm.shared_entity_manager.property_info_extractor' => 'getDoctrine_Orm_SharedEntityManager_PropertyInfoExtractorService',
            'doctrine.orm.shared_listeners.attach_entity_listeners' => 'getDoctrine_Orm_SharedListeners_AttachEntityListenersService',
            'doctrine.orm.shared_manager_configurator' => 'getDoctrine_Orm_SharedManagerConfiguratorService',
            'doctrine.orm.uss_customer_entity_listener_resolver' => 'getDoctrine_Orm_UssCustomerEntityListenerResolverService',
            'doctrine.orm.uss_customer_entity_manager' => 'getDoctrine_Orm_UssCustomerEntityManagerService',
            'doctrine.orm.uss_customer_entity_manager.property_info_extractor' => 'getDoctrine_Orm_UssCustomerEntityManager_PropertyInfoExtractorService',
            'doctrine.orm.uss_customer_listeners.attach_entity_listeners' => 'getDoctrine_Orm_UssCustomerListeners_AttachEntityListenersService',
            'doctrine.orm.uss_customer_manager_configurator' => 'getDoctrine_Orm_UssCustomerManagerConfiguratorService',
            'doctrine.orm.uss_main_entity_listener_resolver' => 'getDoctrine_Orm_UssMainEntityListenerResolverService',
            'doctrine.orm.uss_main_entity_manager' => 'getDoctrine_Orm_UssMainEntityManagerService',
            'doctrine.orm.uss_main_entity_manager.property_info_extractor' => 'getDoctrine_Orm_UssMainEntityManager_PropertyInfoExtractorService',
            'doctrine.orm.uss_main_listeners.attach_entity_listeners' => 'getDoctrine_Orm_UssMainListeners_AttachEntityListenersService',
            'doctrine.orm.uss_main_manager_configurator' => 'getDoctrine_Orm_UssMainManagerConfiguratorService',
            'doctrine.orm.validator.unique' => 'getDoctrine_Orm_Validator_UniqueService',
            'doctrine.orm.validator_initializer' => 'getDoctrine_Orm_ValidatorInitializerService',
            'doctrine_cache.providers.doctrine.orm.che_customer_metadata_cache' => 'getDoctrineCache_Providers_Doctrine_Orm_CheCustomerMetadataCacheService',
            'doctrine_cache.providers.doctrine.orm.che_customer_query_cache' => 'getDoctrineCache_Providers_Doctrine_Orm_CheCustomerQueryCacheService',
            'doctrine_cache.providers.doctrine.orm.che_customer_result_cache' => 'getDoctrineCache_Providers_Doctrine_Orm_CheCustomerResultCacheService',
            'doctrine_cache.providers.doctrine.orm.che_main_metadata_cache' => 'getDoctrineCache_Providers_Doctrine_Orm_CheMainMetadataCacheService',
            'doctrine_cache.providers.doctrine.orm.che_main_query_cache' => 'getDoctrineCache_Providers_Doctrine_Orm_CheMainQueryCacheService',
            'doctrine_cache.providers.doctrine.orm.che_main_result_cache' => 'getDoctrineCache_Providers_Doctrine_Orm_CheMainResultCacheService',
            'doctrine_cache.providers.doctrine.orm.ill_customer_metadata_cache' => 'getDoctrineCache_Providers_Doctrine_Orm_IllCustomerMetadataCacheService',
            'doctrine_cache.providers.doctrine.orm.ill_customer_query_cache' => 'getDoctrineCache_Providers_Doctrine_Orm_IllCustomerQueryCacheService',
            'doctrine_cache.providers.doctrine.orm.ill_customer_result_cache' => 'getDoctrineCache_Providers_Doctrine_Orm_IllCustomerResultCacheService',
            'doctrine_cache.providers.doctrine.orm.ill_main_metadata_cache' => 'getDoctrineCache_Providers_Doctrine_Orm_IllMainMetadataCacheService',
            'doctrine_cache.providers.doctrine.orm.ill_main_query_cache' => 'getDoctrineCache_Providers_Doctrine_Orm_IllMainQueryCacheService',
            'doctrine_cache.providers.doctrine.orm.ill_main_result_cache' => 'getDoctrineCache_Providers_Doctrine_Orm_IllMainResultCacheService',
            'doctrine_cache.providers.doctrine.orm.nmega_customer_metadata_cache' => 'getDoctrineCache_Providers_Doctrine_Orm_NmegaCustomerMetadataCacheService',
            'doctrine_cache.providers.doctrine.orm.nmega_customer_query_cache' => 'getDoctrineCache_Providers_Doctrine_Orm_NmegaCustomerQueryCacheService',
            'doctrine_cache.providers.doctrine.orm.nmega_customer_result_cache' => 'getDoctrineCache_Providers_Doctrine_Orm_NmegaCustomerResultCacheService',
            'doctrine_cache.providers.doctrine.orm.nmega_main_metadata_cache' => 'getDoctrineCache_Providers_Doctrine_Orm_NmegaMainMetadataCacheService',
            'doctrine_cache.providers.doctrine.orm.nmega_main_query_cache' => 'getDoctrineCache_Providers_Doctrine_Orm_NmegaMainQueryCacheService',
            'doctrine_cache.providers.doctrine.orm.nmega_main_result_cache' => 'getDoctrineCache_Providers_Doctrine_Orm_NmegaMainResultCacheService',
            'doctrine_cache.providers.doctrine.orm.oc_customer_metadata_cache' => 'getDoctrineCache_Providers_Doctrine_Orm_OcCustomerMetadataCacheService',
            'doctrine_cache.providers.doctrine.orm.oc_customer_query_cache' => 'getDoctrineCache_Providers_Doctrine_Orm_OcCustomerQueryCacheService',
            'doctrine_cache.providers.doctrine.orm.oc_customer_result_cache' => 'getDoctrineCache_Providers_Doctrine_Orm_OcCustomerResultCacheService',
            'doctrine_cache.providers.doctrine.orm.oc_main_metadata_cache' => 'getDoctrineCache_Providers_Doctrine_Orm_OcMainMetadataCacheService',
            'doctrine_cache.providers.doctrine.orm.oc_main_query_cache' => 'getDoctrineCache_Providers_Doctrine_Orm_OcMainQueryCacheService',
            'doctrine_cache.providers.doctrine.orm.oc_main_result_cache' => 'getDoctrineCache_Providers_Doctrine_Orm_OcMainResultCacheService',
            'doctrine_cache.providers.doctrine.orm.shared_metadata_cache' => 'getDoctrineCache_Providers_Doctrine_Orm_SharedMetadataCacheService',
            'doctrine_cache.providers.doctrine.orm.shared_query_cache' => 'getDoctrineCache_Providers_Doctrine_Orm_SharedQueryCacheService',
            'doctrine_cache.providers.doctrine.orm.shared_result_cache' => 'getDoctrineCache_Providers_Doctrine_Orm_SharedResultCacheService',
            'doctrine_cache.providers.doctrine.orm.uss_customer_metadata_cache' => 'getDoctrineCache_Providers_Doctrine_Orm_UssCustomerMetadataCacheService',
            'doctrine_cache.providers.doctrine.orm.uss_customer_query_cache' => 'getDoctrineCache_Providers_Doctrine_Orm_UssCustomerQueryCacheService',
            'doctrine_cache.providers.doctrine.orm.uss_customer_result_cache' => 'getDoctrineCache_Providers_Doctrine_Orm_UssCustomerResultCacheService',
            'doctrine_cache.providers.doctrine.orm.uss_main_metadata_cache' => 'getDoctrineCache_Providers_Doctrine_Orm_UssMainMetadataCacheService',
            'doctrine_cache.providers.doctrine.orm.uss_main_query_cache' => 'getDoctrineCache_Providers_Doctrine_Orm_UssMainQueryCacheService',
            'doctrine_cache.providers.doctrine.orm.uss_main_result_cache' => 'getDoctrineCache_Providers_Doctrine_Orm_UssMainResultCacheService',
            'easycorp.easylog.handler' => 'getEasycorp_Easylog_HandlerService',
            'file_locator' => 'getFileLocatorService',
            'filesystem' => 'getFilesystemService',
            'form.factory' => 'getForm_FactoryService',
            'form.registry' => 'getForm_RegistryService',
            'form.resolved_type_factory' => 'getForm_ResolvedTypeFactoryService',
            'form.type.birthday' => 'getForm_Type_BirthdayService',
            'form.type.button' => 'getForm_Type_ButtonService',
            'form.type.checkbox' => 'getForm_Type_CheckboxService',
            'form.type.choice' => 'getForm_Type_ChoiceService',
            'form.type.collection' => 'getForm_Type_CollectionService',
            'form.type.country' => 'getForm_Type_CountryService',
            'form.type.currency' => 'getForm_Type_CurrencyService',
            'form.type.date' => 'getForm_Type_DateService',
            'form.type.datetime' => 'getForm_Type_DatetimeService',
            'form.type.email' => 'getForm_Type_EmailService',
            'form.type.entity' => 'getForm_Type_EntityService',
            'form.type.file' => 'getForm_Type_FileService',
            'form.type.form' => 'getForm_Type_FormService',
            'form.type.hidden' => 'getForm_Type_HiddenService',
            'form.type.integer' => 'getForm_Type_IntegerService',
            'form.type.language' => 'getForm_Type_LanguageService',
            'form.type.locale' => 'getForm_Type_LocaleService',
            'form.type.money' => 'getForm_Type_MoneyService',
            'form.type.number' => 'getForm_Type_NumberService',
            'form.type.password' => 'getForm_Type_PasswordService',
            'form.type.percent' => 'getForm_Type_PercentService',
            'form.type.radio' => 'getForm_Type_RadioService',
            'form.type.range' => 'getForm_Type_RangeService',
            'form.type.repeated' => 'getForm_Type_RepeatedService',
            'form.type.reset' => 'getForm_Type_ResetService',
            'form.type.search' => 'getForm_Type_SearchService',
            'form.type.submit' => 'getForm_Type_SubmitService',
            'form.type.text' => 'getForm_Type_TextService',
            'form.type.textarea' => 'getForm_Type_TextareaService',
            'form.type.time' => 'getForm_Type_TimeService',
            'form.type.timezone' => 'getForm_Type_TimezoneService',
            'form.type.url' => 'getForm_Type_UrlService',
            'form.type_extension.form.http_foundation' => 'getForm_TypeExtension_Form_HttpFoundationService',
            'form.type_extension.form.validator' => 'getForm_TypeExtension_Form_ValidatorService',
            'form.type_extension.repeated.validator' => 'getForm_TypeExtension_Repeated_ValidatorService',
            'form.type_extension.submit.validator' => 'getForm_TypeExtension_Submit_ValidatorService',
            'form.type_extension.upload.validator' => 'getForm_TypeExtension_Upload_ValidatorService',
            'form.type_guesser.doctrine' => 'getForm_TypeGuesser_DoctrineService',
            'form.type_guesser.validator' => 'getForm_TypeGuesser_ValidatorService',
            'fragment.handler' => 'getFragment_HandlerService',
            'fragment.listener' => 'getFragment_ListenerService',
            'fragment.renderer.esi' => 'getFragment_Renderer_EsiService',
            'fragment.renderer.hinclude' => 'getFragment_Renderer_HincludeService',
            'fragment.renderer.inline' => 'getFragment_Renderer_InlineService',
            'fragment.renderer.ssi' => 'getFragment_Renderer_SsiService',
            'http_kernel' => 'getHttpKernelService',
            'kernel.class_cache.cache_warmer' => 'getKernel_ClassCache_CacheWarmerService',
            'locale_listener' => 'getLocaleListenerService',
            'logger' => 'getLoggerService',
            'monolog.handler.buffered' => 'getMonolog_Handler_BufferedService',
            'monolog.handler.main' => 'getMonolog_Handler_MainService',
            'monolog.handler.null_internal' => 'getMonolog_Handler_NullInternalService',
            'monolog.logger.cache' => 'getMonolog_Logger_CacheService',
            'monolog.logger.console' => 'getMonolog_Logger_ConsoleService',
            'monolog.logger.doctrine' => 'getMonolog_Logger_DoctrineService',
            'monolog.logger.event' => 'getMonolog_Logger_EventService',
            'monolog.logger.php' => 'getMonolog_Logger_PhpService',
            'monolog.logger.request' => 'getMonolog_Logger_RequestService',
            'monolog.logger.router' => 'getMonolog_Logger_RouterService',
            'monolog.logger.security' => 'getMonolog_Logger_SecurityService',
            'monolog.processor.psr_log_message' => 'getMonolog_Processor_PsrLogMessageService',
            'property_accessor' => 'getPropertyAccessorService',
            'request.add_request_formats_listener' => 'getRequest_AddRequestFormatsListenerService',
            'request_stack' => 'getRequestStackService',
            'response_listener' => 'getResponseListenerService',
            'router' => 'getRouterService',
            'router.request_context' => 'getRouter_RequestContextService',
            'router_listener' => 'getRouterListenerService',
            'routing.loader' => 'getRouting_LoaderService',
            'security.access.authenticated_voter' => 'getSecurity_Access_AuthenticatedVoterService',
            'security.access.expression_voter' => 'getSecurity_Access_ExpressionVoterService',
            'security.access.role_hierarchy_voter' => 'getSecurity_Access_RoleHierarchyVoterService',
            'security.authentication.guard_handler' => 'getSecurity_Authentication_GuardHandlerService',
            'security.authentication.manager' => 'getSecurity_Authentication_ManagerService',
            'security.authentication.provider.guard.api' => 'getSecurity_Authentication_Provider_Guard_ApiService',
            'security.authentication.trust_resolver' => 'getSecurity_Authentication_TrustResolverService',
            'security.authentication_utils' => 'getSecurity_AuthenticationUtilsService',
            'security.authorization_checker' => 'getSecurity_AuthorizationCheckerService',
            'security.encoder_factory' => 'getSecurity_EncoderFactoryService',
            'security.firewall' => 'getSecurity_FirewallService',
            'security.firewall.map.context.api' => 'getSecurity_Firewall_Map_Context_ApiService',
            'security.firewall.map.context.dev' => 'getSecurity_Firewall_Map_Context_DevService',
            'security.firewall.map.context.swagger' => 'getSecurity_Firewall_Map_Context_SwaggerService',
            'security.logout_url_generator' => 'getSecurity_LogoutUrlGeneratorService',
            'security.password_encoder' => 'getSecurity_PasswordEncoderService',
            'security.rememberme.response_listener' => 'getSecurity_Rememberme_ResponseListenerService',
            'security.request_matcher.5233d5f86c1e2076eac36f9cc15152586d6330c2cd3da54fdb761a92d2bb3b59b468e25d' => 'getSecurity_RequestMatcher_5233d5f86c1e2076eac36f9cc15152586d6330c2cd3da54fdb761a92d2bb3b59b468e25dService',
            'security.request_matcher.5314eeb91110adf24b9b678372bb11bbe00e8858c519c088bfb65f525181ad3bf573fd1d' => 'getSecurity_RequestMatcher_5314eeb91110adf24b9b678372bb11bbe00e8858c519c088bfb65f525181ad3bf573fd1dService',
            'security.request_matcher.c9679edbbb26b9639a697ddca8184f648b310f5623db2680a8f22b3b5426f010053cae6b' => 'getSecurity_RequestMatcher_C9679edbbb26b9639a697ddca8184f648b310f5623db2680a8f22b3b5426f010053cae6bService',
            'security.role_hierarchy' => 'getSecurity_RoleHierarchyService',
            'security.token_storage' => 'getSecurity_TokenStorageService',
            'security.user_value_resolver' => 'getSecurity_UserValueResolverService',
            'security.validator.user_password' => 'getSecurity_Validator_UserPasswordService',
            'sensio_framework_extra.cache.listener' => 'getSensioFrameworkExtra_Cache_ListenerService',
            'sensio_framework_extra.controller.listener' => 'getSensioFrameworkExtra_Controller_ListenerService',
            'sensio_framework_extra.converter.datetime' => 'getSensioFrameworkExtra_Converter_DatetimeService',
            'sensio_framework_extra.converter.doctrine.orm' => 'getSensioFrameworkExtra_Converter_Doctrine_OrmService',
            'sensio_framework_extra.converter.listener' => 'getSensioFrameworkExtra_Converter_ListenerService',
            'sensio_framework_extra.converter.manager' => 'getSensioFrameworkExtra_Converter_ManagerService',
            'sensio_framework_extra.security.listener' => 'getSensioFrameworkExtra_Security_ListenerService',
            'sensio_framework_extra.view.guesser' => 'getSensioFrameworkExtra_View_GuesserService',
            'sensio_framework_extra.view.listener' => 'getSensioFrameworkExtra_View_ListenerService',
            'service_locator.f3e4af76f32830cd048a9a0d4662e709' => 'getServiceLocator_F3e4af76f32830cd048a9a0d4662e709Service',
            'sonata.notification.backend.doctrine' => 'getSonata_Notification_Backend_DoctrineService',
            'sonata.notification.backend.postpone' => 'getSonata_Notification_Backend_PostponeService',
            'sonata.notification.backend.runtime' => 'getSonata_Notification_Backend_RuntimeService',
            'sonata.notification.consumer.metadata' => 'getSonata_Notification_Consumer_MetadataService',
            'sonata.notification.dispatcher' => 'getSonata_Notification_DispatcherService',
            'sonata.notification.erroneous_messages_selector' => 'getSonata_Notification_ErroneousMessagesSelectorService',
            'sonata.notification.event.doctrine_backend_optimize' => 'getSonata_Notification_Event_DoctrineBackendOptimizeService',
            'sonata.notification.event.doctrine_optimize' => 'getSonata_Notification_Event_DoctrineOptimizeService',
            'sonata.notification.manager.message.default' => 'getSonata_Notification_Manager_Message_DefaultService',
            'streamed_response_listener' => 'getStreamedResponseListenerService',
            'swiftmailer.email_sender.listener' => 'getSwiftmailer_EmailSender_ListenerService',
            'swiftmailer.mailer.default' => 'getSwiftmailer_Mailer_DefaultService',
            'swiftmailer.mailer.default.plugin.messagelogger' => 'getSwiftmailer_Mailer_Default_Plugin_MessageloggerService',
            'swiftmailer.mailer.default.transport' => 'getSwiftmailer_Mailer_Default_TransportService',
            'synchronization.consumer' => 'getSynchronization_ConsumerService',
            'synchronization.handler' => 'getSynchronization_HandlerService',
            'synchronization.handler.distributor' => 'getSynchronization_Handler_DistributorService',
            'synchronization.handler.movie' => 'getSynchronization_Handler_MovieService',
            'synchronization.handler.program' => 'getSynchronization_Handler_ProgramService',
            'synchronization.listener.entity_update' => 'getSynchronization_Listener_EntityUpdateService',
            'templating.helper.logout_url' => 'getTemplating_Helper_LogoutUrlService',
            'templating.helper.security' => 'getTemplating_Helper_SecurityService',
            'translator' => 'getTranslatorService',
            'uri_signer' => 'getUriSignerService',
            'validate_request_listener' => 'getValidateRequestListenerService',
            'validator' => 'getValidatorService',
            'validator.builder' => 'getValidator_BuilderService',
            'validator.email' => 'getValidator_EmailService',
            'validator.expression' => 'getValidator_ExpressionService',
        );
        $this->privates = array(
            '1_5bc971da440047f3dbeaf372c109d1b5c64861934e6dbd155bbae3c432cf847d' => true,
            '2_5bc971da440047f3dbeaf372c109d1b5c64861934e6dbd155bbae3c432cf847d' => true,
            '8464c6758298cf75d30c4f689fb7886d' => true,
            'annotations.reader' => true,
            'api.params.transformer_factory' => true,
            'argument_resolver.default' => true,
            'argument_resolver.request' => true,
            'argument_resolver.request_attribute' => true,
            'argument_resolver.service' => true,
            'argument_resolver.session' => true,
            'argument_resolver.variadic' => true,
            'cache.annotations' => true,
            'cache.validator' => true,
            'console.error_listener' => true,
            'controller_name_converter' => true,
            'debug.security.access.decision_manager' => true,
            'doctrine.dbal.logger' => true,
            'doctrine.orm.naming_strategy.default' => true,
            'doctrine.orm.quote_strategy.default' => true,
            'form.type.choice' => true,
            'form.type.form' => true,
            'form.type_extension.form.http_foundation' => true,
            'form.type_extension.form.validator' => true,
            'form.type_extension.repeated.validator' => true,
            'form.type_extension.submit.validator' => true,
            'form.type_extension.upload.validator' => true,
            'form.type_guesser.validator' => true,
            'monolog.processor.psr_log_message' => true,
            'router.request_context' => true,
            'security.access.authenticated_voter' => true,
            'security.access.expression_voter' => true,
            'security.access.role_hierarchy_voter' => true,
            'security.authentication.manager' => true,
            'security.authentication.provider.guard.api' => true,
            'security.authentication.trust_resolver' => true,
            'security.logout_url_generator' => true,
            'security.request_matcher.5233d5f86c1e2076eac36f9cc15152586d6330c2cd3da54fdb761a92d2bb3b59b468e25d' => true,
            'security.request_matcher.5314eeb91110adf24b9b678372bb11bbe00e8858c519c088bfb65f525181ad3bf573fd1d' => true,
            'security.request_matcher.c9679edbbb26b9639a697ddca8184f648b310f5623db2680a8f22b3b5426f010053cae6b' => true,
            'security.role_hierarchy' => true,
            'security.user_value_resolver' => true,
            'service_locator.f3e4af76f32830cd048a9a0d4662e709' => true,
        );
        $this->aliases = array(
            'Symfony\\Component\\Security\\Core\\Authorization\\AuthorizationCheckerInterface' => 'security.authorization_checker',
            'Symfony\\Component\\Security\\Core\\Encoder\\UserPasswordEncoderInterface' => 'security.password_encoder',
            'cache.app_clearer' => 'cache.default_clearer',
            'database_connection' => 'doctrine.dbal.shared_connection',
            'doctrine.orm.che_customer_metadata_cache' => 'doctrine_cache.providers.doctrine.orm.che_customer_metadata_cache',
            'doctrine.orm.che_customer_query_cache' => 'doctrine_cache.providers.doctrine.orm.che_customer_query_cache',
            'doctrine.orm.che_customer_result_cache' => 'doctrine_cache.providers.doctrine.orm.che_customer_result_cache',
            'doctrine.orm.che_main_metadata_cache' => 'doctrine_cache.providers.doctrine.orm.che_main_metadata_cache',
            'doctrine.orm.che_main_query_cache' => 'doctrine_cache.providers.doctrine.orm.che_main_query_cache',
            'doctrine.orm.che_main_result_cache' => 'doctrine_cache.providers.doctrine.orm.che_main_result_cache',
            'doctrine.orm.entity_manager' => 'doctrine.orm.shared_entity_manager',
            'doctrine.orm.ill_customer_metadata_cache' => 'doctrine_cache.providers.doctrine.orm.ill_customer_metadata_cache',
            'doctrine.orm.ill_customer_query_cache' => 'doctrine_cache.providers.doctrine.orm.ill_customer_query_cache',
            'doctrine.orm.ill_customer_result_cache' => 'doctrine_cache.providers.doctrine.orm.ill_customer_result_cache',
            'doctrine.orm.ill_main_metadata_cache' => 'doctrine_cache.providers.doctrine.orm.ill_main_metadata_cache',
            'doctrine.orm.ill_main_query_cache' => 'doctrine_cache.providers.doctrine.orm.ill_main_query_cache',
            'doctrine.orm.ill_main_result_cache' => 'doctrine_cache.providers.doctrine.orm.ill_main_result_cache',
            'doctrine.orm.nmega_customer_metadata_cache' => 'doctrine_cache.providers.doctrine.orm.nmega_customer_metadata_cache',
            'doctrine.orm.nmega_customer_query_cache' => 'doctrine_cache.providers.doctrine.orm.nmega_customer_query_cache',
            'doctrine.orm.nmega_customer_result_cache' => 'doctrine_cache.providers.doctrine.orm.nmega_customer_result_cache',
            'doctrine.orm.nmega_main_metadata_cache' => 'doctrine_cache.providers.doctrine.orm.nmega_main_metadata_cache',
            'doctrine.orm.nmega_main_query_cache' => 'doctrine_cache.providers.doctrine.orm.nmega_main_query_cache',
            'doctrine.orm.nmega_main_result_cache' => 'doctrine_cache.providers.doctrine.orm.nmega_main_result_cache',
            'doctrine.orm.oc_customer_metadata_cache' => 'doctrine_cache.providers.doctrine.orm.oc_customer_metadata_cache',
            'doctrine.orm.oc_customer_query_cache' => 'doctrine_cache.providers.doctrine.orm.oc_customer_query_cache',
            'doctrine.orm.oc_customer_result_cache' => 'doctrine_cache.providers.doctrine.orm.oc_customer_result_cache',
            'doctrine.orm.oc_main_metadata_cache' => 'doctrine_cache.providers.doctrine.orm.oc_main_metadata_cache',
            'doctrine.orm.oc_main_query_cache' => 'doctrine_cache.providers.doctrine.orm.oc_main_query_cache',
            'doctrine.orm.oc_main_result_cache' => 'doctrine_cache.providers.doctrine.orm.oc_main_result_cache',
            'doctrine.orm.shared_metadata_cache' => 'doctrine_cache.providers.doctrine.orm.shared_metadata_cache',
            'doctrine.orm.shared_query_cache' => 'doctrine_cache.providers.doctrine.orm.shared_query_cache',
            'doctrine.orm.shared_result_cache' => 'doctrine_cache.providers.doctrine.orm.shared_result_cache',
            'doctrine.orm.uss_customer_metadata_cache' => 'doctrine_cache.providers.doctrine.orm.uss_customer_metadata_cache',
            'doctrine.orm.uss_customer_query_cache' => 'doctrine_cache.providers.doctrine.orm.uss_customer_query_cache',
            'doctrine.orm.uss_customer_result_cache' => 'doctrine_cache.providers.doctrine.orm.uss_customer_result_cache',
            'doctrine.orm.uss_main_metadata_cache' => 'doctrine_cache.providers.doctrine.orm.uss_main_metadata_cache',
            'doctrine.orm.uss_main_query_cache' => 'doctrine_cache.providers.doctrine.orm.uss_main_query_cache',
            'doctrine.orm.uss_main_result_cache' => 'doctrine_cache.providers.doctrine.orm.uss_main_result_cache',
            'event_dispatcher' => 'debug.event_dispatcher',
            'mailer' => 'swiftmailer.mailer.default',
            'monolog.handler.easylog' => 'easycorp.easylog.handler',
            'sonata.notification.backend' => 'sonata.notification.backend.doctrine',
            'sonata.notification.manager.message' => 'sonata.notification.manager.message.default',
            'swiftmailer.mailer' => 'swiftmailer.mailer.default',
            'swiftmailer.plugin.messagelogger' => 'swiftmailer.mailer.default.plugin.messagelogger',
            'swiftmailer.transport' => 'swiftmailer.mailer.default.transport',
        );
    }

    /**
     * {@inheritdoc}
     */
    public function compile()
    {
        throw new LogicException('You cannot compile a dumped container that was already compiled.');
    }

    /**
     * {@inheritdoc}
     */
    public function isCompiled()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isFrozen()
    {
        @trigger_error(sprintf('The %s() method is deprecated since version 3.3 and will be removed in 4.0. Use the isCompiled() method instead.', __METHOD__), E_USER_DEPRECATED);

        return true;
    }

    /**
     * Gets the 'annotation_reader' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Common\Annotations\CachedReader A Doctrine\Common\Annotations\CachedReader instance
     */
    protected function getAnnotationReaderService()
    {
        return $this->services['annotation_reader'] = new \Doctrine\Common\Annotations\CachedReader(${($_ = isset($this->services['annotations.reader']) ? $this->services['annotations.reader'] : $this->getAnnotations_ReaderService()) && false ?: '_'}, new \Symfony\Component\Cache\DoctrineProvider(\Symfony\Component\Cache\Adapter\PhpArrayAdapter::create((__DIR__.'/annotations.php'), ${($_ = isset($this->services['cache.annotations']) ? $this->services['cache.annotations'] : $this->getCache_AnnotationsService()) && false ?: '_'})), true);
    }

    /**
     * Gets the 'api.command.cancel_temp_reservations' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Command\CancelTempReservationsCommand A Illuzion\ApiBundle\Command\CancelTempReservationsCommand instance
     */
    protected function getApi_Command_CancelTempReservationsService()
    {
        return $this->services['api.command.cancel_temp_reservations'] = new \Illuzion\ApiBundle\Command\CancelTempReservationsCommand(${($_ = isset($this->services['api.provider.em']) ? $this->services['api.provider.em'] : $this->get('api.provider.em')) && false ?: '_'});
    }

    /**
     * Gets the 'api.command.generate_jws_token' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Command\GenerateJwsTokenCommand A Illuzion\ApiBundle\Command\GenerateJwsTokenCommand instance
     */
    protected function getApi_Command_GenerateJwsTokenService()
    {
        return $this->services['api.command.generate_jws_token'] = new \Illuzion\ApiBundle\Command\GenerateJwsTokenCommand(${($_ = isset($this->services['api.security.jws_payload_encoder']) ? $this->services['api.security.jws_payload_encoder'] : $this->get('api.security.jws_payload_encoder')) && false ?: '_'});
    }

    /**
     * Gets the 'api.command.movie_scan_for_archived' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Command\MoviesScanForArchivedAttributeCommand A Illuzion\ApiBundle\Command\MoviesScanForArchivedAttributeCommand instance
     */
    protected function getApi_Command_MovieScanForArchivedService()
    {
        return $this->services['api.command.movie_scan_for_archived'] = new \Illuzion\ApiBundle\Command\MoviesScanForArchivedAttributeCommand(${($_ = isset($this->services['api.provider.em']) ? $this->services['api.provider.em'] : $this->get('api.provider.em')) && false ?: '_'});
    }

    /**
     * Gets the 'api.controller.cinema' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Controller\CinemaController A Illuzion\ApiBundle\Controller\CinemaController instance
     */
    protected function getApi_Controller_CinemaService()
    {
        $this->services['api.controller.cinema'] = $instance = new \Illuzion\ApiBundle\Controller\CinemaController(${($_ = isset($this->services['api.service.cinema']) ? $this->services['api.service.cinema'] : $this->get('api.service.cinema')) && false ?: '_'});

        $instance->setContainer($this);

        return $instance;
    }

    /**
     * Gets the 'api.controller.city' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Controller\CityController A Illuzion\ApiBundle\Controller\CityController instance
     */
    protected function getApi_Controller_CityService()
    {
        $this->services['api.controller.city'] = $instance = new \Illuzion\ApiBundle\Controller\CityController(${($_ = isset($this->services['api.service.city']) ? $this->services['api.service.city'] : $this->get('api.service.city')) && false ?: '_'});

        $instance->setContainer($this);

        return $instance;
    }

    /**
     * Gets the 'api.controller.customer' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Controller\CustomerController A Illuzion\ApiBundle\Controller\CustomerController instance
     */
    protected function getApi_Controller_CustomerService()
    {
        $this->services['api.controller.customer'] = $instance = new \Illuzion\ApiBundle\Controller\CustomerController(${($_ = isset($this->services['api.service.customer']) ? $this->services['api.service.customer'] : $this->get('api.service.customer')) && false ?: '_'});

        $instance->setContainer($this);

        return $instance;
    }

    /**
     * Gets the 'api.controller.distributor' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Controller\DistributorController A Illuzion\ApiBundle\Controller\DistributorController instance
     */
    protected function getApi_Controller_DistributorService()
    {
        $this->services['api.controller.distributor'] = $instance = new \Illuzion\ApiBundle\Controller\DistributorController(${($_ = isset($this->services['api.service.distributor']) ? $this->services['api.service.distributor'] : $this->get('api.service.distributor')) && false ?: '_'});

        $instance->setContainer($this);

        return $instance;
    }

    /**
     * Gets the 'api.controller.genre' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Controller\GenreController A Illuzion\ApiBundle\Controller\GenreController instance
     */
    protected function getApi_Controller_GenreService()
    {
        $this->services['api.controller.genre'] = $instance = new \Illuzion\ApiBundle\Controller\GenreController(${($_ = isset($this->services['api.service.genre']) ? $this->services['api.service.genre'] : $this->get('api.service.genre')) && false ?: '_'});

        $instance->setContainer($this);

        return $instance;
    }

    /**
     * Gets the 'api.controller.hall' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Controller\HallController A Illuzion\ApiBundle\Controller\HallController instance
     */
    protected function getApi_Controller_HallService()
    {
        $this->services['api.controller.hall'] = $instance = new \Illuzion\ApiBundle\Controller\HallController(${($_ = isset($this->services['api.service.hall']) ? $this->services['api.service.hall'] : $this->get('api.service.hall')) && false ?: '_'});

        $instance->setContainer($this);

        return $instance;
    }

    /**
     * Gets the 'api.controller.movie' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Controller\MovieController A Illuzion\ApiBundle\Controller\MovieController instance
     */
    protected function getApi_Controller_MovieService()
    {
        $this->services['api.controller.movie'] = $instance = new \Illuzion\ApiBundle\Controller\MovieController(${($_ = isset($this->services['api.service.movie']) ? $this->services['api.service.movie'] : $this->get('api.service.movie')) && false ?: '_'});

        $instance->setContainer($this);

        return $instance;
    }

    /**
     * Gets the 'api.controller.news' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Controller\NewsController A Illuzion\ApiBundle\Controller\NewsController instance
     */
    protected function getApi_Controller_NewsService()
    {
        $this->services['api.controller.news'] = $instance = new \Illuzion\ApiBundle\Controller\NewsController(${($_ = isset($this->services['api.service.article']) ? $this->services['api.service.article'] : $this->get('api.service.article')) && false ?: '_'});

        $instance->setContainer($this);

        return $instance;
    }

    /**
     * Gets the 'api.controller.order' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Controller\OrderController A Illuzion\ApiBundle\Controller\OrderController instance
     */
    protected function getApi_Controller_OrderService()
    {
        $this->services['api.controller.order'] = $instance = new \Illuzion\ApiBundle\Controller\OrderController(${($_ = isset($this->services['api.service.order']) ? $this->services['api.service.order'] : $this->get('api.service.order')) && false ?: '_'});

        $instance->setContainer($this);

        return $instance;
    }

    /**
     * Gets the 'api.controller.program' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Controller\ProgramController A Illuzion\ApiBundle\Controller\ProgramController instance
     */
    protected function getApi_Controller_ProgramService()
    {
        $this->services['api.controller.program'] = $instance = new \Illuzion\ApiBundle\Controller\ProgramController(${($_ = isset($this->services['api.service.program']) ? $this->services['api.service.program'] : $this->get('api.service.program')) && false ?: '_'});

        $instance->setContainer($this);

        return $instance;
    }

    /**
     * Gets the 'api.controller.seat' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Controller\SeatController A Illuzion\ApiBundle\Controller\SeatController instance
     */
    protected function getApi_Controller_SeatService()
    {
        $this->services['api.controller.seat'] = $instance = new \Illuzion\ApiBundle\Controller\SeatController(${($_ = isset($this->services['api.service.seat']) ? $this->services['api.service.seat'] : $this->get('api.service.seat')) && false ?: '_'});

        $instance->setContainer($this);

        return $instance;
    }

    /**
     * Gets the 'api.controller.show' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Controller\ShowController A Illuzion\ApiBundle\Controller\ShowController instance
     */
    protected function getApi_Controller_ShowService()
    {
        $this->services['api.controller.show'] = $instance = new \Illuzion\ApiBundle\Controller\ShowController(${($_ = isset($this->services['api.service.show']) ? $this->services['api.service.show'] : $this->get('api.service.show')) && false ?: '_'});

        $instance->setContainer($this);

        return $instance;
    }

    /**
     * Gets the 'api.controller.swagger' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Controller\SwaggerController A Illuzion\ApiBundle\Controller\SwaggerController instance
     */
    protected function getApi_Controller_SwaggerService()
    {
        $this->services['api.controller.swagger'] = $instance = new \Illuzion\ApiBundle\Controller\SwaggerController();

        $instance->setContainer($this);

        return $instance;
    }

    /**
     * Gets the 'api.form.customer_type' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Form\CustomerType A Illuzion\ApiBundle\Form\CustomerType instance
     */
    protected function getApi_Form_CustomerTypeService()
    {
        return $this->services['api.form.customer_type'] = new \Illuzion\ApiBundle\Form\CustomerType(${($_ = isset($this->services['api.provider.em']) ? $this->services['api.provider.em'] : $this->get('api.provider.em')) && false ?: '_'});
    }

    /**
     * Gets the 'api.form.movie_type' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Form\MovieType A Illuzion\ApiBundle\Form\MovieType instance
     */
    protected function getApi_Form_MovieTypeService()
    {
        return $this->services['api.form.movie_type'] = new \Illuzion\ApiBundle\Form\MovieType(${($_ = isset($this->services['api.provider.em']) ? $this->services['api.provider.em'] : $this->get('api.provider.em')) && false ?: '_'});
    }

    /**
     * Gets the 'api.form.relation_type' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Form\Common\RelationType A Illuzion\ApiBundle\Form\Common\RelationType instance
     */
    protected function getApi_Form_RelationTypeService()
    {
        return $this->services['api.form.relation_type'] = new \Illuzion\ApiBundle\Form\Common\RelationType(${($_ = isset($this->services['api.provider.em']) ? $this->services['api.provider.em'] : $this->get('api.provider.em')) && false ?: '_'});
    }

    /**
     * Gets the 'api.listener.local_entity' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Listener\LocalEntitySelectListener A Illuzion\ApiBundle\Listener\LocalEntitySelectListener instance
     */
    protected function getApi_Listener_LocalEntityService()
    {
        return $this->services['api.listener.local_entity'] = new \Illuzion\ApiBundle\Listener\LocalEntitySelectListener();
    }

    /**
     * Gets the 'api.listner.api_exception' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Listener\ApiExceptionListener A Illuzion\ApiBundle\Listener\ApiExceptionListener instance
     */
    protected function getApi_Listner_ApiExceptionService()
    {
        return $this->services['api.listner.api_exception'] = new \Illuzion\ApiBundle\Listener\ApiExceptionListener(${($_ = isset($this->services['api.serializer.encoder']) ? $this->services['api.serializer.encoder'] : $this->get('api.serializer.encoder')) && false ?: '_'}, ${($_ = isset($this->services['logger']) ? $this->services['logger'] : $this->get('logger')) && false ?: '_'}, true);
    }

    /**
     * Gets the 'api.listner.json_transformer' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Listener\JsonRequestTransformerListener A Illuzion\ApiBundle\Listener\JsonRequestTransformerListener instance
     */
    protected function getApi_Listner_JsonTransformerService()
    {
        return $this->services['api.listner.json_transformer'] = new \Illuzion\ApiBundle\Listener\JsonRequestTransformerListener(${($_ = isset($this->services['logger']) ? $this->services['logger'] : $this->get('logger')) && false ?: '_'});
    }

    /**
     * Gets the 'api.listner.params_validation' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Listener\QueryValidationListener A Illuzion\ApiBundle\Listener\QueryValidationListener instance
     */
    protected function getApi_Listner_ParamsValidationService()
    {
        return $this->services['api.listner.params_validation'] = new \Illuzion\ApiBundle\Listener\QueryValidationListener(new \Illuzion\ApiBundle\Request\Params\Metadata\MetadataFactory(${($_ = isset($this->services['annotations.reader']) ? $this->services['annotations.reader'] : $this->getAnnotations_ReaderService()) && false ?: '_'}), new \Illuzion\ApiBundle\Request\Params\RequestParser(${($_ = isset($this->services['validator']) ? $this->services['validator'] : $this->get('validator')) && false ?: '_'}, ${($_ = isset($this->services['api.params.transformer_factory']) ? $this->services['api.params.transformer_factory'] : $this->getApi_Params_TransformerFactoryService()) && false ?: '_'}));
    }

    /**
     * Gets the 'api.params.transformer.scalar_array' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Configuration\Query\ScalarArrayParamTransformer A Illuzion\ApiBundle\Configuration\Query\ScalarArrayParamTransformer instance
     */
    protected function getApi_Params_Transformer_ScalarArrayService()
    {
        return $this->services['api.params.transformer.scalar_array'] = new \Illuzion\ApiBundle\Configuration\Query\ScalarArrayParamTransformer(${($_ = isset($this->services['api.params.transformer_factory']) ? $this->services['api.params.transformer_factory'] : $this->getApi_Params_TransformerFactoryService()) && false ?: '_'});
    }

    /**
     * Gets the 'api.provider.em' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Provider\EntityManagerProvider A Illuzion\ApiBundle\Provider\EntityManagerProvider instance
     */
    protected function getApi_Provider_EmService()
    {
        return $this->services['api.provider.em'] = new \Illuzion\ApiBundle\Provider\EntityManagerProvider(${($_ = isset($this->services['doctrine']) ? $this->services['doctrine'] : $this->get('doctrine')) && false ?: '_'}, array(0 => 'uss', 1 => 'oc', 2 => 'ill', 3 => 'che', 4 => 'nmega'));
    }

    /**
     * Gets the 'api.security.jws_guard_authenticator' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Security\JwsGuardAuthenticator A Illuzion\ApiBundle\Security\JwsGuardAuthenticator instance
     */
    protected function getApi_Security_JwsGuardAuthenticatorService()
    {
        return $this->services['api.security.jws_guard_authenticator'] = new \Illuzion\ApiBundle\Security\JwsGuardAuthenticator(${($_ = isset($this->services['api.security.jws_payload_encoder']) ? $this->services['api.security.jws_payload_encoder'] : $this->get('api.security.jws_payload_encoder')) && false ?: '_'}, true);
    }

    /**
     * Gets the 'api.security.jws_payload_encoder' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Security\JwsPayloadEncoder A Illuzion\ApiBundle\Security\JwsPayloadEncoder instance
     */
    protected function getApi_Security_JwsPayloadEncoderService()
    {
        return $this->services['api.security.jws_payload_encoder'] = new \Illuzion\ApiBundle\Security\JwsPayloadEncoder('HS512', array('key' => 'da08e547r7319955bg3d7465866619dfd0fbe478'));
    }

    /**
     * Gets the 'api.security.user_provider' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Security\ApiClientProvider A Illuzion\ApiBundle\Security\ApiClientProvider instance
     */
    protected function getApi_Security_UserProviderService()
    {
        return $this->services['api.security.user_provider'] = new \Illuzion\ApiBundle\Security\ApiClientProvider(array('base' => array('roles' => array(0 => 'admin_role')), 'tysa' => array('roles' => array(0 => 'tysa_role')), 'vlru' => array('roles' => array(0 => 'vlru_role')), 'rpi' => array('roles' => array(0 => 'rpi_role')), 'erp' => array('roles' => array(0 => 'admin_role')), 'illru' => array('roles' => array(0 => 'admin_role'))), '30 minutes');
    }

    /**
     * Gets the 'api.serializer.container' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Serializer\Container A Illuzion\ApiBundle\Serializer\Container instance
     */
    protected function getApi_Serializer_ContainerService()
    {
        $this->services['api.serializer.container'] = $instance = new \Illuzion\ApiBundle\Serializer\Container(${($_ = isset($this->services['security.authorization_checker']) ? $this->services['security.authorization_checker'] : $this->get('security.authorization_checker')) && false ?: '_'});

        $instance->register(${($_ = isset($this->services['api.serializer.schema.city']) ? $this->services['api.serializer.schema.city'] : $this->get('api.serializer.schema.city')) && false ?: '_'});
        $instance->register(${($_ = isset($this->services['api.serializer.schema.cinema']) ? $this->services['api.serializer.schema.cinema'] : $this->get('api.serializer.schema.cinema')) && false ?: '_'});
        $instance->register(${($_ = isset($this->services['api.serializer.schema.hall']) ? $this->services['api.serializer.schema.hall'] : $this->get('api.serializer.schema.hall')) && false ?: '_'});
        $instance->register(${($_ = isset($this->services['api.serializer.schema.distributor']) ? $this->services['api.serializer.schema.distributor'] : $this->get('api.serializer.schema.distributor')) && false ?: '_'});
        $instance->register(${($_ = isset($this->services['api.serializer.schema.program']) ? $this->services['api.serializer.schema.program'] : $this->get('api.serializer.schema.program')) && false ?: '_'});
        $instance->register(${($_ = isset($this->services['api.serializer.schema.genre']) ? $this->services['api.serializer.schema.genre'] : $this->get('api.serializer.schema.genre')) && false ?: '_'});
        $instance->register(${($_ = isset($this->services['api.serializer.schema.movie']) ? $this->services['api.serializer.schema.movie'] : $this->get('api.serializer.schema.movie')) && false ?: '_'});
        $instance->register(${($_ = isset($this->services['api.serializer.schema.show']) ? $this->services['api.serializer.schema.show'] : $this->get('api.serializer.schema.show')) && false ?: '_'});
        $instance->register(${($_ = isset($this->services['api.serializer.schema.seat']) ? $this->services['api.serializer.schema.seat'] : $this->get('api.serializer.schema.seat')) && false ?: '_'});
        $instance->register(${($_ = isset($this->services['api.serializer.schema.article']) ? $this->services['api.serializer.schema.article'] : $this->get('api.serializer.schema.article')) && false ?: '_'});
        $instance->register(${($_ = isset($this->services['api.serializer.schema.ticket']) ? $this->services['api.serializer.schema.ticket'] : $this->get('api.serializer.schema.ticket')) && false ?: '_'});
        $instance->register(${($_ = isset($this->services['api.serializer.schema.order']) ? $this->services['api.serializer.schema.order'] : $this->get('api.serializer.schema.order')) && false ?: '_'});
        $instance->register(${($_ = isset($this->services['api.serializer.schema.customer']) ? $this->services['api.serializer.schema.customer'] : $this->get('api.serializer.schema.customer')) && false ?: '_'});

        return $instance;
    }

    /**
     * Gets the 'api.serializer.ecoder_options' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Neomerx\JsonApi\Encoder\EncoderOptions A Neomerx\JsonApi\Encoder\EncoderOptions instance
     */
    protected function getApi_Serializer_EcoderOptionsService()
    {
        return $this->services['api.serializer.ecoder_options'] = new \Neomerx\JsonApi\Encoder\EncoderOptions(320, 'http://ussuri.illuzion.loc:7023/v4', 512);
    }

    /**
     * Gets the 'api.serializer.encoder' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Neomerx\JsonApi\Encoder\Encoder A Neomerx\JsonApi\Encoder\Encoder instance
     */
    protected function getApi_Serializer_EncoderService()
    {
        return $this->services['api.serializer.encoder'] = new \Neomerx\JsonApi\Encoder\Encoder(${($_ = isset($this->services['api.serializer.factory']) ? $this->services['api.serializer.factory'] : $this->get('api.serializer.factory')) && false ?: '_'}, ${($_ = isset($this->services['api.serializer.container']) ? $this->services['api.serializer.container'] : $this->get('api.serializer.container')) && false ?: '_'}, ${($_ = isset($this->services['api.serializer.ecoder_options']) ? $this->services['api.serializer.ecoder_options'] : $this->get('api.serializer.ecoder_options')) && false ?: '_'});
    }

    /**
     * Gets the 'api.serializer.factory' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Neomerx\JsonApi\Factories\Factory A Neomerx\JsonApi\Factories\Factory instance
     */
    protected function getApi_Serializer_FactoryService()
    {
        $this->services['api.serializer.factory'] = $instance = new \Neomerx\JsonApi\Factories\Factory();

        $instance->setLogger(${($_ = isset($this->services['logger']) ? $this->services['logger'] : $this->get('logger')) && false ?: '_'});

        return $instance;
    }

    /**
     * Gets the 'api.serializer.schema.article' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Serializer\Schema\ArticleSchema A Illuzion\ApiBundle\Serializer\Schema\ArticleSchema instance
     */
    protected function getApi_Serializer_Schema_ArticleService()
    {
        return $this->services['api.serializer.schema.article'] = new \Illuzion\ApiBundle\Serializer\Schema\ArticleSchema(${($_ = isset($this->services['api.serializer.factory']) ? $this->services['api.serializer.factory'] : $this->get('api.serializer.factory')) && false ?: '_'}, 'http://service.illuzion.ru/media');
    }

    /**
     * Gets the 'api.serializer.schema.cinema' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Serializer\Schema\CinemaSchema A Illuzion\ApiBundle\Serializer\Schema\CinemaSchema instance
     */
    protected function getApi_Serializer_Schema_CinemaService()
    {
        return $this->services['api.serializer.schema.cinema'] = new \Illuzion\ApiBundle\Serializer\Schema\CinemaSchema(${($_ = isset($this->services['api.serializer.factory']) ? $this->services['api.serializer.factory'] : $this->get('api.serializer.factory')) && false ?: '_'}, ${($_ = isset($this->services['api.service.hall']) ? $this->services['api.service.hall'] : $this->get('api.service.hall')) && false ?: '_'});
    }

    /**
     * Gets the 'api.serializer.schema.city' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Serializer\Schema\CitySchema A Illuzion\ApiBundle\Serializer\Schema\CitySchema instance
     */
    protected function getApi_Serializer_Schema_CityService()
    {
        return $this->services['api.serializer.schema.city'] = new \Illuzion\ApiBundle\Serializer\Schema\CitySchema(${($_ = isset($this->services['api.serializer.factory']) ? $this->services['api.serializer.factory'] : $this->get('api.serializer.factory')) && false ?: '_'});
    }

    /**
     * Gets the 'api.serializer.schema.customer' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Serializer\Schema\CustomerSchema A Illuzion\ApiBundle\Serializer\Schema\CustomerSchema instance
     */
    protected function getApi_Serializer_Schema_CustomerService()
    {
        return $this->services['api.serializer.schema.customer'] = new \Illuzion\ApiBundle\Serializer\Schema\CustomerSchema(${($_ = isset($this->services['api.serializer.factory']) ? $this->services['api.serializer.factory'] : $this->get('api.serializer.factory')) && false ?: '_'});
    }

    /**
     * Gets the 'api.serializer.schema.distributor' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Serializer\Schema\DistributorSchema A Illuzion\ApiBundle\Serializer\Schema\DistributorSchema instance
     */
    protected function getApi_Serializer_Schema_DistributorService()
    {
        return $this->services['api.serializer.schema.distributor'] = new \Illuzion\ApiBundle\Serializer\Schema\DistributorSchema(${($_ = isset($this->services['api.serializer.factory']) ? $this->services['api.serializer.factory'] : $this->get('api.serializer.factory')) && false ?: '_'});
    }

    /**
     * Gets the 'api.serializer.schema.genre' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Serializer\Schema\GenreSchema A Illuzion\ApiBundle\Serializer\Schema\GenreSchema instance
     */
    protected function getApi_Serializer_Schema_GenreService()
    {
        return $this->services['api.serializer.schema.genre'] = new \Illuzion\ApiBundle\Serializer\Schema\GenreSchema(${($_ = isset($this->services['api.serializer.factory']) ? $this->services['api.serializer.factory'] : $this->get('api.serializer.factory')) && false ?: '_'});
    }

    /**
     * Gets the 'api.serializer.schema.hall' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Serializer\Schema\HallSchema A Illuzion\ApiBundle\Serializer\Schema\HallSchema instance
     */
    protected function getApi_Serializer_Schema_HallService()
    {
        return $this->services['api.serializer.schema.hall'] = new \Illuzion\ApiBundle\Serializer\Schema\HallSchema(${($_ = isset($this->services['api.serializer.factory']) ? $this->services['api.serializer.factory'] : $this->get('api.serializer.factory')) && false ?: '_'}, ${($_ = isset($this->services['api.service.cinema']) ? $this->services['api.service.cinema'] : $this->get('api.service.cinema')) && false ?: '_'});
    }

    /**
     * Gets the 'api.serializer.schema.movie' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Serializer\Schema\MovieSchema A Illuzion\ApiBundle\Serializer\Schema\MovieSchema instance
     */
    protected function getApi_Serializer_Schema_MovieService()
    {
        return $this->services['api.serializer.schema.movie'] = new \Illuzion\ApiBundle\Serializer\Schema\MovieSchema(${($_ = isset($this->services['api.serializer.factory']) ? $this->services['api.serializer.factory'] : $this->get('api.serializer.factory')) && false ?: '_'}, ${($_ = isset($this->services['api.service.release_date']) ? $this->services['api.service.release_date'] : $this->get('api.service.release_date')) && false ?: '_'}, 'http://service.illuzion.ru/media');
    }

    /**
     * Gets the 'api.serializer.schema.order' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Serializer\Schema\OrderSchema A Illuzion\ApiBundle\Serializer\Schema\OrderSchema instance
     */
    protected function getApi_Serializer_Schema_OrderService()
    {
        return $this->services['api.serializer.schema.order'] = new \Illuzion\ApiBundle\Serializer\Schema\OrderSchema(${($_ = isset($this->services['api.serializer.factory']) ? $this->services['api.serializer.factory'] : $this->get('api.serializer.factory')) && false ?: '_'});
    }

    /**
     * Gets the 'api.serializer.schema.program' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Serializer\Schema\ProgramSchema A Illuzion\ApiBundle\Serializer\Schema\ProgramSchema instance
     */
    protected function getApi_Serializer_Schema_ProgramService()
    {
        return $this->services['api.serializer.schema.program'] = new \Illuzion\ApiBundle\Serializer\Schema\ProgramSchema(${($_ = isset($this->services['api.serializer.factory']) ? $this->services['api.serializer.factory'] : $this->get('api.serializer.factory')) && false ?: '_'}, ${($_ = isset($this->services['api.service.city']) ? $this->services['api.service.city'] : $this->get('api.service.city')) && false ?: '_'}, 'http://service.illuzion.ru/media');
    }

    /**
     * Gets the 'api.serializer.schema.seat' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Serializer\Schema\SeatSchema A Illuzion\ApiBundle\Serializer\Schema\SeatSchema instance
     */
    protected function getApi_Serializer_Schema_SeatService()
    {
        return $this->services['api.serializer.schema.seat'] = new \Illuzion\ApiBundle\Serializer\Schema\SeatSchema(${($_ = isset($this->services['api.serializer.factory']) ? $this->services['api.serializer.factory'] : $this->get('api.serializer.factory')) && false ?: '_'});
    }

    /**
     * Gets the 'api.serializer.schema.show' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Serializer\Schema\ShowSchema A Illuzion\ApiBundle\Serializer\Schema\ShowSchema instance
     */
    protected function getApi_Serializer_Schema_ShowService()
    {
        return $this->services['api.serializer.schema.show'] = new \Illuzion\ApiBundle\Serializer\Schema\ShowSchema(${($_ = isset($this->services['api.serializer.factory']) ? $this->services['api.serializer.factory'] : $this->get('api.serializer.factory')) && false ?: '_'}, ${($_ = isset($this->services['api.service.place_type_price']) ? $this->services['api.service.place_type_price'] : $this->get('api.service.place_type_price')) && false ?: '_'}, ${($_ = isset($this->services['security.token_storage']) ? $this->services['security.token_storage'] : $this->get('security.token_storage')) && false ?: '_'});
    }

    /**
     * Gets the 'api.serializer.schema.ticket' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Serializer\Schema\TicketSchema A Illuzion\ApiBundle\Serializer\Schema\TicketSchema instance
     */
    protected function getApi_Serializer_Schema_TicketService()
    {
        return $this->services['api.serializer.schema.ticket'] = new \Illuzion\ApiBundle\Serializer\Schema\TicketSchema(${($_ = isset($this->services['api.serializer.factory']) ? $this->services['api.serializer.factory'] : $this->get('api.serializer.factory')) && false ?: '_'}, ${($_ = isset($this->services['api.service.cinema']) ? $this->services['api.service.cinema'] : $this->get('api.service.cinema')) && false ?: '_'});
    }

    /**
     * Gets the 'api.service.article' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Service\ArticleService A Illuzion\ApiBundle\Service\ArticleService instance
     */
    protected function getApi_Service_ArticleService()
    {
        return $this->services['api.service.article'] = new \Illuzion\ApiBundle\Service\ArticleService(${($_ = isset($this->services['api.provider.em']) ? $this->services['api.provider.em'] : $this->get('api.provider.em')) && false ?: '_'});
    }

    /**
     * Gets the 'api.service.cinema' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Service\CinemaService A Illuzion\ApiBundle\Service\CinemaService instance
     */
    protected function getApi_Service_CinemaService()
    {
        return $this->services['api.service.cinema'] = new \Illuzion\ApiBundle\Service\CinemaService(${($_ = isset($this->services['api.provider.em']) ? $this->services['api.provider.em'] : $this->get('api.provider.em')) && false ?: '_'});
    }

    /**
     * Gets the 'api.service.city' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Service\CityService A Illuzion\ApiBundle\Service\CityService instance
     */
    protected function getApi_Service_CityService()
    {
        return $this->services['api.service.city'] = new \Illuzion\ApiBundle\Service\CityService(${($_ = isset($this->services['api.provider.em']) ? $this->services['api.provider.em'] : $this->get('api.provider.em')) && false ?: '_'});
    }

    /**
     * Gets the 'api.service.customer' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Service\CustomerService A Illuzion\ApiBundle\Service\CustomerService instance
     */
    protected function getApi_Service_CustomerService()
    {
        return $this->services['api.service.customer'] = new \Illuzion\ApiBundle\Service\CustomerService(${($_ = isset($this->services['api.provider.em']) ? $this->services['api.provider.em'] : $this->get('api.provider.em')) && false ?: '_'}, ${($_ = isset($this->services['api.service.emailer']) ? $this->services['api.service.emailer'] : $this->get('api.service.emailer')) && false ?: '_'});
    }

    /**
     * Gets the 'api.service.distributor' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Service\DistributorService A Illuzion\ApiBundle\Service\DistributorService instance
     */
    protected function getApi_Service_DistributorService()
    {
        return $this->services['api.service.distributor'] = new \Illuzion\ApiBundle\Service\DistributorService(${($_ = isset($this->services['api.provider.em']) ? $this->services['api.provider.em'] : $this->get('api.provider.em')) && false ?: '_'});
    }

    /**
     * Gets the 'api.service.emailer' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Service\NotificationService A Illuzion\ApiBundle\Service\NotificationService instance
     */
    protected function getApi_Service_EmailerService()
    {
        return $this->services['api.service.emailer'] = new \Illuzion\ApiBundle\Service\NotificationService('http://service.illuzion.ru/mailer/web/', 19901990);
    }

    /**
     * Gets the 'api.service.genre' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Service\GenreService A Illuzion\ApiBundle\Service\GenreService instance
     */
    protected function getApi_Service_GenreService()
    {
        return $this->services['api.service.genre'] = new \Illuzion\ApiBundle\Service\GenreService(${($_ = isset($this->services['api.provider.em']) ? $this->services['api.provider.em'] : $this->get('api.provider.em')) && false ?: '_'});
    }

    /**
     * Gets the 'api.service.hall' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Service\HallService A Illuzion\ApiBundle\Service\HallService instance
     */
    protected function getApi_Service_HallService()
    {
        return $this->services['api.service.hall'] = new \Illuzion\ApiBundle\Service\HallService(${($_ = isset($this->services['api.provider.em']) ? $this->services['api.provider.em'] : $this->get('api.provider.em')) && false ?: '_'});
    }

    /**
     * Gets the 'api.service.movie' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Service\MovieService A Illuzion\ApiBundle\Service\MovieService instance
     */
    protected function getApi_Service_MovieService()
    {
        return $this->services['api.service.movie'] = new \Illuzion\ApiBundle\Service\MovieService(${($_ = isset($this->services['api.provider.em']) ? $this->services['api.provider.em'] : $this->get('api.provider.em')) && false ?: '_'}, ${($_ = isset($this->services['api.service.release_date']) ? $this->services['api.service.release_date'] : $this->get('api.service.release_date')) && false ?: '_'});
    }

    /**
     * Gets the 'api.service.order' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Service\OrderService A Illuzion\ApiBundle\Service\OrderService instance
     */
    protected function getApi_Service_OrderService()
    {
        return $this->services['api.service.order'] = new \Illuzion\ApiBundle\Service\OrderService(${($_ = isset($this->services['api.provider.em']) ? $this->services['api.provider.em'] : $this->get('api.provider.em')) && false ?: '_'}, ${($_ = isset($this->services['api.service.place_type_price']) ? $this->services['api.service.place_type_price'] : $this->get('api.service.place_type_price')) && false ?: '_'}, ${($_ = isset($this->services['api.service.seat']) ? $this->services['api.service.seat'] : $this->get('api.service.seat')) && false ?: '_'}, ${($_ = isset($this->services['api.service.customer']) ? $this->services['api.service.customer'] : $this->get('api.service.customer')) && false ?: '_'}, ${($_ = isset($this->services['api.service.emailer']) ? $this->services['api.service.emailer'] : $this->get('api.service.emailer')) && false ?: '_'}, ${($_ = isset($this->services['api.service.cinema']) ? $this->services['api.service.cinema'] : $this->get('api.service.cinema')) && false ?: '_'}, ${($_ = isset($this->services['logger']) ? $this->services['logger'] : $this->get('logger')) && false ?: '_'});
    }

    /**
     * Gets the 'api.service.place_type_price' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Service\PlaceTypePriceService A Illuzion\ApiBundle\Service\PlaceTypePriceService instance
     */
    protected function getApi_Service_PlaceTypePriceService()
    {
        return $this->services['api.service.place_type_price'] = new \Illuzion\ApiBundle\Service\PlaceTypePriceService(${($_ = isset($this->services['api.provider.em']) ? $this->services['api.provider.em'] : $this->get('api.provider.em')) && false ?: '_'});
    }

    /**
     * Gets the 'api.service.program' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Service\ProgramService A Illuzion\ApiBundle\Service\ProgramService instance
     */
    protected function getApi_Service_ProgramService()
    {
        return $this->services['api.service.program'] = new \Illuzion\ApiBundle\Service\ProgramService(${($_ = isset($this->services['api.provider.em']) ? $this->services['api.provider.em'] : $this->get('api.provider.em')) && false ?: '_'});
    }

    /**
     * Gets the 'api.service.release_date' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Service\ReleaseDateService A Illuzion\ApiBundle\Service\ReleaseDateService instance
     */
    protected function getApi_Service_ReleaseDateService()
    {
        return $this->services['api.service.release_date'] = new \Illuzion\ApiBundle\Service\ReleaseDateService(${($_ = isset($this->services['api.provider.em']) ? $this->services['api.provider.em'] : $this->get('api.provider.em')) && false ?: '_'});
    }

    /**
     * Gets the 'api.service.seat' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Service\SeatService A Illuzion\ApiBundle\Service\SeatService instance
     */
    protected function getApi_Service_SeatService()
    {
        return $this->services['api.service.seat'] = new \Illuzion\ApiBundle\Service\SeatService(${($_ = isset($this->services['api.provider.em']) ? $this->services['api.provider.em'] : $this->get('api.provider.em')) && false ?: '_'}, ${($_ = isset($this->services['cache.app']) ? $this->services['cache.app'] : $this->get('cache.app')) && false ?: '_'}, 0);
    }

    /**
     * Gets the 'api.service.show' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\ApiBundle\Service\ShowService A Illuzion\ApiBundle\Service\ShowService instance
     */
    protected function getApi_Service_ShowService()
    {
        return $this->services['api.service.show'] = new \Illuzion\ApiBundle\Service\ShowService(${($_ = isset($this->services['api.provider.em']) ? $this->services['api.provider.em'] : $this->get('api.provider.em')) && false ?: '_'});
    }

    /**
     * Gets the 'assets.context' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\Asset\Context\RequestStackContext A Symfony\Component\Asset\Context\RequestStackContext instance
     */
    protected function getAssets_ContextService()
    {
        return $this->services['assets.context'] = new \Symfony\Component\Asset\Context\RequestStackContext(${($_ = isset($this->services['request_stack']) ? $this->services['request_stack'] : $this->get('request_stack')) && false ?: '_'});
    }

    /**
     * Gets the 'assets.packages' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\Asset\Packages A Symfony\Component\Asset\Packages instance
     */
    protected function getAssets_PackagesService()
    {
        return $this->services['assets.packages'] = new \Symfony\Component\Asset\Packages(new \Symfony\Component\Asset\PathPackage('', new \Symfony\Component\Asset\VersionStrategy\EmptyVersionStrategy(), ${($_ = isset($this->services['assets.context']) ? $this->services['assets.context'] : $this->get('assets.context')) && false ?: '_'}), array());
    }

    /**
     * Gets the 'cache.app' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\Cache\Adapter\MemcachedAdapter A Symfony\Component\Cache\Adapter\MemcachedAdapter instance
     */
    protected function getCache_AppService()
    {
        $this->services['cache.app'] = $instance = new \Symfony\Component\Cache\Adapter\MemcachedAdapter(${($_ = isset($this->services['8464c6758298cf75d30c4f689fb7886d']) ? $this->services['8464c6758298cf75d30c4f689fb7886d'] : $this->get8464c6758298cf75d30c4f689fb7886dService()) && false ?: '_'}, 'YgXWCXFKcK', 0);

        if ($this->has('monolog.logger.cache')) {
            $instance->setLogger(${($_ = isset($this->services['monolog.logger.cache']) ? $this->services['monolog.logger.cache'] : $this->get('monolog.logger.cache', ContainerInterface::NULL_ON_INVALID_REFERENCE)) && false ?: '_'});
        }

        return $instance;
    }

    /**
     * Gets the 'cache.default_clearer' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\HttpKernel\CacheClearer\Psr6CacheClearer A Symfony\Component\HttpKernel\CacheClearer\Psr6CacheClearer instance
     */
    protected function getCache_DefaultClearerService()
    {
        return $this->services['cache.default_clearer'] = new \Symfony\Component\HttpKernel\CacheClearer\Psr6CacheClearer(array('cache.app' => ${($_ = isset($this->services['cache.app']) ? $this->services['cache.app'] : $this->get('cache.app')) && false ?: '_'}, 'cache.system' => ${($_ = isset($this->services['cache.system']) ? $this->services['cache.system'] : $this->get('cache.system')) && false ?: '_'}, 'cache.validator' => ${($_ = isset($this->services['cache.validator']) ? $this->services['cache.validator'] : $this->getCache_ValidatorService()) && false ?: '_'}, 'cache.annotations' => ${($_ = isset($this->services['cache.annotations']) ? $this->services['cache.annotations'] : $this->getCache_AnnotationsService()) && false ?: '_'}));
    }

    /**
     * Gets the 'cache.global_clearer' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\HttpKernel\CacheClearer\Psr6CacheClearer A Symfony\Component\HttpKernel\CacheClearer\Psr6CacheClearer instance
     */
    protected function getCache_GlobalClearerService()
    {
        return $this->services['cache.global_clearer'] = new \Symfony\Component\HttpKernel\CacheClearer\Psr6CacheClearer(array('cache.app' => ${($_ = isset($this->services['cache.app']) ? $this->services['cache.app'] : $this->get('cache.app')) && false ?: '_'}, 'cache.system' => ${($_ = isset($this->services['cache.system']) ? $this->services['cache.system'] : $this->get('cache.system')) && false ?: '_'}, 'cache.validator' => ${($_ = isset($this->services['cache.validator']) ? $this->services['cache.validator'] : $this->getCache_ValidatorService()) && false ?: '_'}, 'cache.annotations' => ${($_ = isset($this->services['cache.annotations']) ? $this->services['cache.annotations'] : $this->getCache_AnnotationsService()) && false ?: '_'}));
    }

    /**
     * Gets the 'cache.system' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\Cache\Adapter\MemcachedAdapter A Symfony\Component\Cache\Adapter\MemcachedAdapter instance
     */
    protected function getCache_SystemService()
    {
        $this->services['cache.system'] = $instance = new \Symfony\Component\Cache\Adapter\MemcachedAdapter(${($_ = isset($this->services['8464c6758298cf75d30c4f689fb7886d']) ? $this->services['8464c6758298cf75d30c4f689fb7886d'] : $this->get8464c6758298cf75d30c4f689fb7886dService()) && false ?: '_'}, 'TEjAGVM1W0', 0);

        if ($this->has('monolog.logger.cache')) {
            $instance->setLogger(${($_ = isset($this->services['monolog.logger.cache']) ? $this->services['monolog.logger.cache'] : $this->get('monolog.logger.cache', ContainerInterface::NULL_ON_INVALID_REFERENCE)) && false ?: '_'});
        }

        return $instance;
    }

    /**
     * Gets the 'cache_clearer' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\HttpKernel\CacheClearer\ChainCacheClearer A Symfony\Component\HttpKernel\CacheClearer\ChainCacheClearer instance
     */
    protected function getCacheClearerService()
    {
        return $this->services['cache_clearer'] = new \Symfony\Component\HttpKernel\CacheClearer\ChainCacheClearer(array(0 => ${($_ = isset($this->services['cache.default_clearer']) ? $this->services['cache.default_clearer'] : $this->get('cache.default_clearer')) && false ?: '_'}));
    }

    /**
     * Gets the 'cache_warmer' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\HttpKernel\CacheWarmer\CacheWarmerAggregate A Symfony\Component\HttpKernel\CacheWarmer\CacheWarmerAggregate instance
     */
    protected function getCacheWarmerService()
    {
        return $this->services['cache_warmer'] = new \Symfony\Component\HttpKernel\CacheWarmer\CacheWarmerAggregate(array(0 => ${($_ = isset($this->services['kernel.class_cache.cache_warmer']) ? $this->services['kernel.class_cache.cache_warmer'] : $this->get('kernel.class_cache.cache_warmer')) && false ?: '_'}, 1 => new \Symfony\Bundle\FrameworkBundle\CacheWarmer\ValidatorCacheWarmer(${($_ = isset($this->services['validator.builder']) ? $this->services['validator.builder'] : $this->get('validator.builder')) && false ?: '_'}, (__DIR__.'/validation.php'), ${($_ = isset($this->services['cache.validator']) ? $this->services['cache.validator'] : $this->getCache_ValidatorService()) && false ?: '_'}), 2 => new \Symfony\Bundle\FrameworkBundle\CacheWarmer\RouterCacheWarmer(${($_ = isset($this->services['router']) ? $this->services['router'] : $this->get('router')) && false ?: '_'}), 3 => new \Symfony\Bundle\FrameworkBundle\CacheWarmer\AnnotationsCacheWarmer(${($_ = isset($this->services['annotations.reader']) ? $this->services['annotations.reader'] : $this->getAnnotations_ReaderService()) && false ?: '_'}, (__DIR__.'/annotations.php'), ${($_ = isset($this->services['cache.annotations']) ? $this->services['cache.annotations'] : $this->getCache_AnnotationsService()) && false ?: '_'}), 4 => new \Symfony\Bridge\Doctrine\CacheWarmer\ProxyCacheWarmer(${($_ = isset($this->services['doctrine']) ? $this->services['doctrine'] : $this->get('doctrine')) && false ?: '_'})));
    }

    /**
     * Gets the 'config_cache_factory' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\Config\ResourceCheckerConfigCacheFactory A Symfony\Component\Config\ResourceCheckerConfigCacheFactory instance
     */
    protected function getConfigCacheFactoryService()
    {
        return $this->services['config_cache_factory'] = new \Symfony\Component\Config\ResourceCheckerConfigCacheFactory(new RewindableGenerator(function () {
            yield 0 => ${($_ = isset($this->services['1_5bc971da440047f3dbeaf372c109d1b5c64861934e6dbd155bbae3c432cf847d']) ? $this->services['1_5bc971da440047f3dbeaf372c109d1b5c64861934e6dbd155bbae3c432cf847d'] : $this->get15bc971da440047f3dbeaf372c109d1b5c64861934e6dbd155bbae3c432cf847dService()) && false ?: '_'};
            yield 1 => ${($_ = isset($this->services['2_5bc971da440047f3dbeaf372c109d1b5c64861934e6dbd155bbae3c432cf847d']) ? $this->services['2_5bc971da440047f3dbeaf372c109d1b5c64861934e6dbd155bbae3c432cf847d'] : $this->get25bc971da440047f3dbeaf372c109d1b5c64861934e6dbd155bbae3c432cf847dService()) && false ?: '_'};
        }, 2));
    }

    /**
     * Gets the 'console.command.symfony_bundle_securitybundle_command_userpasswordencodercommand' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Bundle\SecurityBundle\Command\UserPasswordEncoderCommand A Symfony\Bundle\SecurityBundle\Command\UserPasswordEncoderCommand instance
     */
    protected function getConsole_Command_SymfonyBundleSecuritybundleCommandUserpasswordencodercommandService()
    {
        return $this->services['console.command.symfony_bundle_securitybundle_command_userpasswordencodercommand'] = new \Symfony\Bundle\SecurityBundle\Command\UserPasswordEncoderCommand(${($_ = isset($this->services['security.encoder_factory']) ? $this->services['security.encoder_factory'] : $this->get('security.encoder_factory')) && false ?: '_'}, array());
    }

    /**
     * Gets the 'debug.argument_resolver' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\HttpKernel\Controller\TraceableArgumentResolver A Symfony\Component\HttpKernel\Controller\TraceableArgumentResolver instance
     */
    protected function getDebug_ArgumentResolverService()
    {
        return $this->services['debug.argument_resolver'] = new \Symfony\Component\HttpKernel\Controller\TraceableArgumentResolver(new \Symfony\Component\HttpKernel\Controller\ArgumentResolver(new \Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadataFactory(), new RewindableGenerator(function () {
            yield 0 => ${($_ = isset($this->services['argument_resolver.request_attribute']) ? $this->services['argument_resolver.request_attribute'] : $this->getArgumentResolver_RequestAttributeService()) && false ?: '_'};
            yield 1 => ${($_ = isset($this->services['argument_resolver.request']) ? $this->services['argument_resolver.request'] : $this->getArgumentResolver_RequestService()) && false ?: '_'};
            yield 2 => ${($_ = isset($this->services['argument_resolver.session']) ? $this->services['argument_resolver.session'] : $this->getArgumentResolver_SessionService()) && false ?: '_'};
            yield 3 => ${($_ = isset($this->services['security.user_value_resolver']) ? $this->services['security.user_value_resolver'] : $this->getSecurity_UserValueResolverService()) && false ?: '_'};
            yield 4 => ${($_ = isset($this->services['argument_resolver.service']) ? $this->services['argument_resolver.service'] : $this->getArgumentResolver_ServiceService()) && false ?: '_'};
            yield 5 => ${($_ = isset($this->services['argument_resolver.default']) ? $this->services['argument_resolver.default'] : $this->getArgumentResolver_DefaultService()) && false ?: '_'};
            yield 6 => ${($_ = isset($this->services['argument_resolver.variadic']) ? $this->services['argument_resolver.variadic'] : $this->getArgumentResolver_VariadicService()) && false ?: '_'};
        }, 7)), ${($_ = isset($this->services['debug.stopwatch']) ? $this->services['debug.stopwatch'] : $this->get('debug.stopwatch')) && false ?: '_'});
    }

    /**
     * Gets the 'debug.controller_resolver' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\HttpKernel\Controller\TraceableControllerResolver A Symfony\Component\HttpKernel\Controller\TraceableControllerResolver instance
     */
    protected function getDebug_ControllerResolverService()
    {
        return $this->services['debug.controller_resolver'] = new \Symfony\Component\HttpKernel\Controller\TraceableControllerResolver(new \Symfony\Bundle\FrameworkBundle\Controller\ControllerResolver($this, ${($_ = isset($this->services['controller_name_converter']) ? $this->services['controller_name_converter'] : $this->getControllerNameConverterService()) && false ?: '_'}, ${($_ = isset($this->services['monolog.logger.request']) ? $this->services['monolog.logger.request'] : $this->get('monolog.logger.request', ContainerInterface::NULL_ON_INVALID_REFERENCE)) && false ?: '_'}), ${($_ = isset($this->services['debug.stopwatch']) ? $this->services['debug.stopwatch'] : $this->get('debug.stopwatch')) && false ?: '_'}, ${($_ = isset($this->services['debug.argument_resolver']) ? $this->services['debug.argument_resolver'] : $this->get('debug.argument_resolver')) && false ?: '_'});
    }

    /**
     * Gets the 'debug.debug_handlers_listener' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\HttpKernel\EventListener\DebugHandlersListener A Symfony\Component\HttpKernel\EventListener\DebugHandlersListener instance
     */
    protected function getDebug_DebugHandlersListenerService()
    {
        return $this->services['debug.debug_handlers_listener'] = new \Symfony\Component\HttpKernel\EventListener\DebugHandlersListener(NULL, ${($_ = isset($this->services['monolog.logger.php']) ? $this->services['monolog.logger.php'] : $this->get('monolog.logger.php', ContainerInterface::NULL_ON_INVALID_REFERENCE)) && false ?: '_'}, -1, -1, true, new \Symfony\Component\HttpKernel\Debug\FileLinkFormatter(NULL), true);
    }

    /**
     * Gets the 'debug.event_dispatcher' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\HttpKernel\Debug\TraceableEventDispatcher A Symfony\Component\HttpKernel\Debug\TraceableEventDispatcher instance
     */
    protected function getDebug_EventDispatcherService()
    {
        $this->services['debug.event_dispatcher'] = $instance = new \Symfony\Component\HttpKernel\Debug\TraceableEventDispatcher(new \Symfony\Component\EventDispatcher\ContainerAwareEventDispatcher($this), ${($_ = isset($this->services['debug.stopwatch']) ? $this->services['debug.stopwatch'] : $this->get('debug.stopwatch')) && false ?: '_'}, ${($_ = isset($this->services['monolog.logger.event']) ? $this->services['monolog.logger.event'] : $this->get('monolog.logger.event', ContainerInterface::NULL_ON_INVALID_REFERENCE)) && false ?: '_'});

        $instance->addListener('kernel.terminate', /** @closure-proxy Sonata\NotificationBundle\Backend\PostponeRuntimeBackend::onEvent */ function (\Symfony\Component\EventDispatcher\Event $event = NULL) {
            return ${($_ = isset($this->services['sonata.notification.backend.postpone']) ? $this->services['sonata.notification.backend.postpone'] : $this->get('sonata.notification.backend.postpone')) && false ?: '_'}->onEvent($event);
        }, 0);
        $instance->addListener('sonata.notification.event.message_iterate_event', /** @closure-proxy Sonata\NotificationBundle\Event\DoctrineBackendOptimizeListener::iterate */ function (\Sonata\NotificationBundle\Event\IterateEvent $event) {
            return ${($_ = isset($this->services['sonata.notification.event.doctrine_backend_optimize']) ? $this->services['sonata.notification.event.doctrine_backend_optimize'] : $this->get('sonata.notification.event.doctrine_backend_optimize')) && false ?: '_'}->iterate($event);
        }, 0);
        $instance->addListener('kernel.controller', /** @closure-proxy Illuzion\ApiBundle\Listener\QueryValidationListener::onKernelController */ function (\Symfony\Component\HttpKernel\Event\FilterControllerEvent $event) {
            return ${($_ = isset($this->services['api.listner.params_validation']) ? $this->services['api.listner.params_validation'] : $this->get('api.listner.params_validation')) && false ?: '_'}->onKernelController($event);
        }, 0);
        $instance->addListener('kernel.exception', /** @closure-proxy Illuzion\ApiBundle\Listener\ApiExceptionListener::onKernelException */ function (\Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent $event) {
            return ${($_ = isset($this->services['api.listner.api_exception']) ? $this->services['api.listner.api_exception'] : $this->get('api.listner.api_exception')) && false ?: '_'}->onKernelException($event);
        }, 0);
        $instance->addListener('kernel.request', /** @closure-proxy Illuzion\ApiBundle\Listener\JsonRequestTransformerListener::onKernelRequest */ function (\Symfony\Component\HttpKernel\Event\GetResponseEvent $event) {
            return ${($_ = isset($this->services['api.listner.json_transformer']) ? $this->services['api.listner.json_transformer'] : $this->get('api.listner.json_transformer')) && false ?: '_'}->onKernelRequest($event);
        }, -10);
        $instance->addListener('kernel.response', /** @closure-proxy Symfony\Component\HttpKernel\EventListener\ResponseListener::onKernelResponse */ function (\Symfony\Component\HttpKernel\Event\FilterResponseEvent $event) {
            return ${($_ = isset($this->services['response_listener']) ? $this->services['response_listener'] : $this->get('response_listener')) && false ?: '_'}->onKernelResponse($event);
        }, 0);
        $instance->addListener('kernel.response', /** @closure-proxy Symfony\Component\HttpKernel\EventListener\StreamedResponseListener::onKernelResponse */ function (\Symfony\Component\HttpKernel\Event\FilterResponseEvent $event) {
            return ${($_ = isset($this->services['streamed_response_listener']) ? $this->services['streamed_response_listener'] : $this->get('streamed_response_listener')) && false ?: '_'}->onKernelResponse($event);
        }, -1024);
        $instance->addListener('kernel.request', /** @closure-proxy Symfony\Component\HttpKernel\EventListener\LocaleListener::onKernelRequest */ function (\Symfony\Component\HttpKernel\Event\GetResponseEvent $event) {
            return ${($_ = isset($this->services['locale_listener']) ? $this->services['locale_listener'] : $this->get('locale_listener')) && false ?: '_'}->onKernelRequest($event);
        }, 16);
        $instance->addListener('kernel.finish_request', /** @closure-proxy Symfony\Component\HttpKernel\EventListener\LocaleListener::onKernelFinishRequest */ function (\Symfony\Component\HttpKernel\Event\FinishRequestEvent $event) {
            return ${($_ = isset($this->services['locale_listener']) ? $this->services['locale_listener'] : $this->get('locale_listener')) && false ?: '_'}->onKernelFinishRequest($event);
        }, 0);
        $instance->addListener('kernel.request', /** @closure-proxy Symfony\Component\HttpKernel\EventListener\ValidateRequestListener::onKernelRequest */ function (\Symfony\Component\HttpKernel\Event\GetResponseEvent $event) {
            return ${($_ = isset($this->services['validate_request_listener']) ? $this->services['validate_request_listener'] : $this->get('validate_request_listener')) && false ?: '_'}->onKernelRequest($event);
        }, 256);
        $instance->addListener('console.error', /** @closure-proxy Symfony\Component\Console\EventListener\ErrorListener::onConsoleError */ function (\Symfony\Component\Console\Event\ConsoleErrorEvent $event) {
            return ${($_ = isset($this->services['console.error_listener']) ? $this->services['console.error_listener'] : $this->getConsole_ErrorListenerService()) && false ?: '_'}->onConsoleError($event);
        }, -128);
        $instance->addListener('console.terminate', /** @closure-proxy Symfony\Component\Console\EventListener\ErrorListener::onConsoleTerminate */ function (\Symfony\Component\Console\Event\ConsoleTerminateEvent $event) {
            return ${($_ = isset($this->services['console.error_listener']) ? $this->services['console.error_listener'] : $this->getConsole_ErrorListenerService()) && false ?: '_'}->onConsoleTerminate($event);
        }, -128);
        $instance->addListener('kernel.request', /** @closure-proxy Symfony\Component\HttpKernel\EventListener\AddRequestFormatsListener::onKernelRequest */ function (\Symfony\Component\HttpKernel\Event\GetResponseEvent $event) {
            return ${($_ = isset($this->services['request.add_request_formats_listener']) ? $this->services['request.add_request_formats_listener'] : $this->get('request.add_request_formats_listener')) && false ?: '_'}->onKernelRequest($event);
        }, 1);
        $instance->addListener('kernel.request', /** @closure-proxy Symfony\Component\HttpKernel\EventListener\FragmentListener::onKernelRequest */ function (\Symfony\Component\HttpKernel\Event\GetResponseEvent $event) {
            return ${($_ = isset($this->services['fragment.listener']) ? $this->services['fragment.listener'] : $this->get('fragment.listener')) && false ?: '_'}->onKernelRequest($event);
        }, 48);
        $instance->addListener('kernel.request', /** @closure-proxy Symfony\Component\HttpKernel\EventListener\DebugHandlersListener::configure */ function (\Symfony\Component\EventDispatcher\Event $event = NULL) {
            return ${($_ = isset($this->services['debug.debug_handlers_listener']) ? $this->services['debug.debug_handlers_listener'] : $this->get('debug.debug_handlers_listener')) && false ?: '_'}->configure($event);
        }, 2048);
        $instance->addListener('console.command', /** @closure-proxy Symfony\Component\HttpKernel\EventListener\DebugHandlersListener::configure */ function (\Symfony\Component\EventDispatcher\Event $event = NULL) {
            return ${($_ = isset($this->services['debug.debug_handlers_listener']) ? $this->services['debug.debug_handlers_listener'] : $this->get('debug.debug_handlers_listener')) && false ?: '_'}->configure($event);
        }, 2048);
        $instance->addListener('kernel.request', /** @closure-proxy Symfony\Component\HttpKernel\EventListener\RouterListener::onKernelRequest */ function (\Symfony\Component\HttpKernel\Event\GetResponseEvent $event) {
            return ${($_ = isset($this->services['router_listener']) ? $this->services['router_listener'] : $this->get('router_listener')) && false ?: '_'}->onKernelRequest($event);
        }, 32);
        $instance->addListener('kernel.finish_request', /** @closure-proxy Symfony\Component\HttpKernel\EventListener\RouterListener::onKernelFinishRequest */ function (\Symfony\Component\HttpKernel\Event\FinishRequestEvent $event) {
            return ${($_ = isset($this->services['router_listener']) ? $this->services['router_listener'] : $this->get('router_listener')) && false ?: '_'}->onKernelFinishRequest($event);
        }, 0);
        $instance->addListener('kernel.request', /** @closure-proxy Symfony\Bundle\SecurityBundle\EventListener\FirewallListener::onKernelRequest */ function (\Symfony\Component\HttpKernel\Event\GetResponseEvent $event) {
            return ${($_ = isset($this->services['security.firewall']) ? $this->services['security.firewall'] : $this->get('security.firewall')) && false ?: '_'}->onKernelRequest($event);
        }, 8);
        $instance->addListener('kernel.finish_request', /** @closure-proxy Symfony\Bundle\SecurityBundle\EventListener\FirewallListener::onKernelFinishRequest */ function (\Symfony\Component\HttpKernel\Event\FinishRequestEvent $event) {
            return ${($_ = isset($this->services['security.firewall']) ? $this->services['security.firewall'] : $this->get('security.firewall')) && false ?: '_'}->onKernelFinishRequest($event);
        }, 0);
        $instance->addListener('kernel.response', /** @closure-proxy Symfony\Component\Security\Http\RememberMe\ResponseListener::onKernelResponse */ function (\Symfony\Component\HttpKernel\Event\FilterResponseEvent $event) {
            return ${($_ = isset($this->services['security.rememberme.response_listener']) ? $this->services['security.rememberme.response_listener'] : $this->get('security.rememberme.response_listener')) && false ?: '_'}->onKernelResponse($event);
        }, 0);
        $instance->addListener('kernel.controller', /** @closure-proxy Sensio\Bundle\FrameworkExtraBundle\EventListener\ControllerListener::onKernelController */ function (\Symfony\Component\HttpKernel\Event\FilterControllerEvent $event) {
            return ${($_ = isset($this->services['sensio_framework_extra.controller.listener']) ? $this->services['sensio_framework_extra.controller.listener'] : $this->get('sensio_framework_extra.controller.listener')) && false ?: '_'}->onKernelController($event);
        }, 0);
        $instance->addListener('kernel.controller', /** @closure-proxy Sensio\Bundle\FrameworkExtraBundle\EventListener\ParamConverterListener::onKernelController */ function (\Symfony\Component\HttpKernel\Event\FilterControllerEvent $event) {
            return ${($_ = isset($this->services['sensio_framework_extra.converter.listener']) ? $this->services['sensio_framework_extra.converter.listener'] : $this->get('sensio_framework_extra.converter.listener')) && false ?: '_'}->onKernelController($event);
        }, 0);
        $instance->addListener('kernel.controller', /** @closure-proxy Sensio\Bundle\FrameworkExtraBundle\EventListener\TemplateListener::onKernelController */ function (\Symfony\Component\HttpKernel\Event\FilterControllerEvent $event) {
            return ${($_ = isset($this->services['sensio_framework_extra.view.listener']) ? $this->services['sensio_framework_extra.view.listener'] : $this->get('sensio_framework_extra.view.listener')) && false ?: '_'}->onKernelController($event);
        }, -128);
        $instance->addListener('kernel.view', /** @closure-proxy Sensio\Bundle\FrameworkExtraBundle\EventListener\TemplateListener::onKernelView */ function (\Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent $event) {
            return ${($_ = isset($this->services['sensio_framework_extra.view.listener']) ? $this->services['sensio_framework_extra.view.listener'] : $this->get('sensio_framework_extra.view.listener')) && false ?: '_'}->onKernelView($event);
        }, 0);
        $instance->addListener('kernel.controller', /** @closure-proxy Sensio\Bundle\FrameworkExtraBundle\EventListener\HttpCacheListener::onKernelController */ function (\Symfony\Component\HttpKernel\Event\FilterControllerEvent $event) {
            return ${($_ = isset($this->services['sensio_framework_extra.cache.listener']) ? $this->services['sensio_framework_extra.cache.listener'] : $this->get('sensio_framework_extra.cache.listener')) && false ?: '_'}->onKernelController($event);
        }, 0);
        $instance->addListener('kernel.response', /** @closure-proxy Sensio\Bundle\FrameworkExtraBundle\EventListener\HttpCacheListener::onKernelResponse */ function (\Symfony\Component\HttpKernel\Event\FilterResponseEvent $event) {
            return ${($_ = isset($this->services['sensio_framework_extra.cache.listener']) ? $this->services['sensio_framework_extra.cache.listener'] : $this->get('sensio_framework_extra.cache.listener')) && false ?: '_'}->onKernelResponse($event);
        }, 0);
        $instance->addListener('kernel.controller', /** @closure-proxy Sensio\Bundle\FrameworkExtraBundle\EventListener\SecurityListener::onKernelController */ function (\Symfony\Component\HttpKernel\Event\FilterControllerEvent $event) {
            return ${($_ = isset($this->services['sensio_framework_extra.security.listener']) ? $this->services['sensio_framework_extra.security.listener'] : $this->get('sensio_framework_extra.security.listener')) && false ?: '_'}->onKernelController($event);
        }, 0);
        $instance->addListener('kernel.exception', /** @closure-proxy Symfony\Bundle\SwiftmailerBundle\EventListener\EmailSenderListener::onException */ function () {
            return ${($_ = isset($this->services['swiftmailer.email_sender.listener']) ? $this->services['swiftmailer.email_sender.listener'] : $this->get('swiftmailer.email_sender.listener')) && false ?: '_'}->onException();
        }, 0);
        $instance->addListener('kernel.terminate', /** @closure-proxy Symfony\Bundle\SwiftmailerBundle\EventListener\EmailSenderListener::onTerminate */ function () {
            return ${($_ = isset($this->services['swiftmailer.email_sender.listener']) ? $this->services['swiftmailer.email_sender.listener'] : $this->get('swiftmailer.email_sender.listener')) && false ?: '_'}->onTerminate();
        }, 0);
        $instance->addListener('console.exception', /** @closure-proxy Symfony\Bundle\SwiftmailerBundle\EventListener\EmailSenderListener::onException */ function () {
            return ${($_ = isset($this->services['swiftmailer.email_sender.listener']) ? $this->services['swiftmailer.email_sender.listener'] : $this->get('swiftmailer.email_sender.listener')) && false ?: '_'}->onException();
        }, 0);
        $instance->addListener('console.terminate', /** @closure-proxy Symfony\Bundle\SwiftmailerBundle\EventListener\EmailSenderListener::onTerminate */ function () {
            return ${($_ = isset($this->services['swiftmailer.email_sender.listener']) ? $this->services['swiftmailer.email_sender.listener'] : $this->get('swiftmailer.email_sender.listener')) && false ?: '_'}->onTerminate();
        }, 0);

        return $instance;
    }

    /**
     * Gets the 'debug.stopwatch' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\Stopwatch\Stopwatch A Symfony\Component\Stopwatch\Stopwatch instance
     */
    protected function getDebug_StopwatchService()
    {
        return $this->services['debug.stopwatch'] = new \Symfony\Component\Stopwatch\Stopwatch();
    }

    /**
     * Gets the 'deprecated.form.registry' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \stdClass A stdClass instance
     *
     * @deprecated The service "deprecated.form.registry" is internal and deprecated since Symfony 3.3 and will be removed in Symfony 4.0
     */
    protected function getDeprecated_Form_RegistryService()
    {
        @trigger_error('The service "deprecated.form.registry" is internal and deprecated since Symfony 3.3 and will be removed in Symfony 4.0', E_USER_DEPRECATED);

        $this->services['deprecated.form.registry'] = $instance = new \stdClass();

        $instance->registry = array(0 => ${($_ = isset($this->services['form.type_guesser.validator']) ? $this->services['form.type_guesser.validator'] : $this->getForm_TypeGuesser_ValidatorService()) && false ?: '_'}, 1 => ${($_ = isset($this->services['form.type.choice']) ? $this->services['form.type.choice'] : $this->getForm_Type_ChoiceService()) && false ?: '_'}, 2 => ${($_ = isset($this->services['form.type.form']) ? $this->services['form.type.form'] : $this->getForm_Type_FormService()) && false ?: '_'}, 3 => ${($_ = isset($this->services['form.type_extension.form.http_foundation']) ? $this->services['form.type_extension.form.http_foundation'] : $this->getForm_TypeExtension_Form_HttpFoundationService()) && false ?: '_'}, 4 => ${($_ = isset($this->services['form.type_extension.form.validator']) ? $this->services['form.type_extension.form.validator'] : $this->getForm_TypeExtension_Form_ValidatorService()) && false ?: '_'}, 5 => ${($_ = isset($this->services['form.type_extension.repeated.validator']) ? $this->services['form.type_extension.repeated.validator'] : $this->getForm_TypeExtension_Repeated_ValidatorService()) && false ?: '_'}, 6 => ${($_ = isset($this->services['form.type_extension.submit.validator']) ? $this->services['form.type_extension.submit.validator'] : $this->getForm_TypeExtension_Submit_ValidatorService()) && false ?: '_'}, 7 => ${($_ = isset($this->services['form.type_extension.upload.validator']) ? $this->services['form.type_extension.upload.validator'] : $this->getForm_TypeExtension_Upload_ValidatorService()) && false ?: '_'});

        return $instance;
    }

    /**
     * Gets the 'doctrine' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Bundle\DoctrineBundle\Registry A Doctrine\Bundle\DoctrineBundle\Registry instance
     */
    protected function getDoctrineService()
    {
        return $this->services['doctrine'] = new \Doctrine\Bundle\DoctrineBundle\Registry($this, array('shared' => 'doctrine.dbal.shared_connection', 'uss_main' => 'doctrine.dbal.uss_main_connection', 'uss_customer' => 'doctrine.dbal.uss_customer_connection', 'oc_main' => 'doctrine.dbal.oc_main_connection', 'oc_customer' => 'doctrine.dbal.oc_customer_connection', 'ill_main' => 'doctrine.dbal.ill_main_connection', 'ill_customer' => 'doctrine.dbal.ill_customer_connection', 'che_main' => 'doctrine.dbal.che_main_connection', 'che_customer' => 'doctrine.dbal.che_customer_connection', 'nmega_main' => 'doctrine.dbal.nmega_main_connection', 'nmega_customer' => 'doctrine.dbal.nmega_customer_connection'), array('shared' => 'doctrine.orm.shared_entity_manager', 'uss_main' => 'doctrine.orm.uss_main_entity_manager', 'uss_customer' => 'doctrine.orm.uss_customer_entity_manager', 'oc_main' => 'doctrine.orm.oc_main_entity_manager', 'oc_customer' => 'doctrine.orm.oc_customer_entity_manager', 'ill_main' => 'doctrine.orm.ill_main_entity_manager', 'ill_customer' => 'doctrine.orm.ill_customer_entity_manager', 'che_main' => 'doctrine.orm.che_main_entity_manager', 'che_customer' => 'doctrine.orm.che_customer_entity_manager', 'nmega_main' => 'doctrine.orm.nmega_main_entity_manager', 'nmega_customer' => 'doctrine.orm.nmega_customer_entity_manager'), 'shared', 'shared');
    }

    /**
     * Gets the 'doctrine.dbal.che_customer_connection' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\DBAL\Connection A Doctrine\DBAL\Connection instance
     */
    protected function getDoctrine_Dbal_CheCustomerConnectionService()
    {
        $a = new \Doctrine\DBAL\Logging\LoggerChain();
        $a->addLogger(${($_ = isset($this->services['doctrine.dbal.logger']) ? $this->services['doctrine.dbal.logger'] : $this->getDoctrine_Dbal_LoggerService()) && false ?: '_'});
        $a->addLogger(new \Doctrine\DBAL\Logging\DebugStack());

        $b = new \Doctrine\DBAL\Configuration();
        $b->setSQLLogger($a);

        $c = new \Symfony\Bridge\Doctrine\ContainerAwareEventManager($this);
        $c->addEventSubscriber(${($_ = isset($this->services['api.listener.local_entity']) ? $this->services['api.listener.local_entity'] : $this->get('api.listener.local_entity')) && false ?: '_'});
        $c->addEventListener(array(0 => 'loadClassMetadata'), ${($_ = isset($this->services['doctrine.orm.che_customer_listeners.attach_entity_listeners']) ? $this->services['doctrine.orm.che_customer_listeners.attach_entity_listeners'] : $this->get('doctrine.orm.che_customer_listeners.attach_entity_listeners')) && false ?: '_'});

        return $this->services['doctrine.dbal.che_customer_connection'] = ${($_ = isset($this->services['doctrine.dbal.connection_factory']) ? $this->services['doctrine.dbal.connection_factory'] : $this->get('doctrine.dbal.connection_factory')) && false ?: '_'}->createConnection(array('host' => '192.168.26.4', 'port' => NULL, 'dbname' => 'c:\\cher\\people_cinema.gdb', 'user' => 'internet', 'password' => 12345678, 'driver' => 'pdo_mysql', 'driverOptions' => array('doctrineTransactionWait' => -1, 'doctrineTransactionIsolationLevel' => 3), 'driverClass' => 'Illuzion\\Common\\Doctrine\\FixPrepareDriver', 'defaultTableOptions' => array()), $b, $c, array());
    }

    /**
     * Gets the 'doctrine.dbal.che_main_connection' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\DBAL\Connection A Doctrine\DBAL\Connection instance
     */
    protected function getDoctrine_Dbal_CheMainConnectionService()
    {
        $a = new \Doctrine\DBAL\Logging\LoggerChain();
        $a->addLogger(${($_ = isset($this->services['doctrine.dbal.logger']) ? $this->services['doctrine.dbal.logger'] : $this->getDoctrine_Dbal_LoggerService()) && false ?: '_'});
        $a->addLogger(new \Doctrine\DBAL\Logging\DebugStack());

        $b = new \Doctrine\DBAL\Configuration();
        $b->setSQLLogger($a);

        $c = new \Symfony\Bridge\Doctrine\ContainerAwareEventManager($this);
        $c->addEventSubscriber(${($_ = isset($this->services['api.listener.local_entity']) ? $this->services['api.listener.local_entity'] : $this->get('api.listener.local_entity')) && false ?: '_'});
        $c->addEventListener(array(0 => 'loadClassMetadata'), ${($_ = isset($this->services['doctrine.orm.che_main_listeners.attach_entity_listeners']) ? $this->services['doctrine.orm.che_main_listeners.attach_entity_listeners'] : $this->get('doctrine.orm.che_main_listeners.attach_entity_listeners')) && false ?: '_'});

        return $this->services['doctrine.dbal.che_main_connection'] = ${($_ = isset($this->services['doctrine.dbal.connection_factory']) ? $this->services['doctrine.dbal.connection_factory'] : $this->get('doctrine.dbal.connection_factory')) && false ?: '_'}->createConnection(array('host' => '192.168.26.4', 'port' => NULL, 'dbname' => 'c:\\cher\\cher_db.gdb', 'user' => 'internet', 'password' => 12345678, 'driver' => 'pdo_mysql', 'driverOptions' => array('doctrineTransactionWait' => -1, 'doctrineTransactionIsolationLevel' => 3), 'driverClass' => 'Illuzion\\Common\\Doctrine\\FixPrepareDriver', 'defaultTableOptions' => array()), $b, $c, array());
    }

    /**
     * Gets the 'doctrine.dbal.connection_factory' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Bundle\DoctrineBundle\ConnectionFactory A Doctrine\Bundle\DoctrineBundle\ConnectionFactory instance
     */
    protected function getDoctrine_Dbal_ConnectionFactoryService()
    {
        return $this->services['doctrine.dbal.connection_factory'] = new \Doctrine\Bundle\DoctrineBundle\ConnectionFactory(array('string_win1251' => array('class' => 'Illuzion\\Common\\Doctrine\\Windows1251String', 'commented' => true), 'text_win1251' => array('class' => 'Illuzion\\Common\\Doctrine\\Windows1251Text', 'commented' => true), 'movie_format' => array('class' => 'Illuzion\\Common\\Doctrine\\MovieFormat', 'commented' => true), 'seat_type' => array('class' => 'Illuzion\\Common\\Doctrine\\SeatType', 'commented' => true), 'json' => array('class' => 'Sonata\\Doctrine\\Types\\JsonType', 'commented' => true)));
    }

    /**
     * Gets the 'doctrine.dbal.ill_customer_connection' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\DBAL\Connection A Doctrine\DBAL\Connection instance
     */
    protected function getDoctrine_Dbal_IllCustomerConnectionService()
    {
        $a = new \Doctrine\DBAL\Logging\LoggerChain();
        $a->addLogger(${($_ = isset($this->services['doctrine.dbal.logger']) ? $this->services['doctrine.dbal.logger'] : $this->getDoctrine_Dbal_LoggerService()) && false ?: '_'});
        $a->addLogger(new \Doctrine\DBAL\Logging\DebugStack());

        $b = new \Doctrine\DBAL\Configuration();
        $b->setSQLLogger($a);

        $c = new \Symfony\Bridge\Doctrine\ContainerAwareEventManager($this);
        $c->addEventSubscriber(${($_ = isset($this->services['api.listener.local_entity']) ? $this->services['api.listener.local_entity'] : $this->get('api.listener.local_entity')) && false ?: '_'});
        $c->addEventListener(array(0 => 'loadClassMetadata'), ${($_ = isset($this->services['doctrine.orm.ill_customer_listeners.attach_entity_listeners']) ? $this->services['doctrine.orm.ill_customer_listeners.attach_entity_listeners'] : $this->get('doctrine.orm.ill_customer_listeners.attach_entity_listeners')) && false ?: '_'});

        return $this->services['doctrine.dbal.ill_customer_connection'] = ${($_ = isset($this->services['doctrine.dbal.connection_factory']) ? $this->services['doctrine.dbal.connection_factory'] : $this->get('doctrine.dbal.connection_factory')) && false ?: '_'}->createConnection(array('host' => '192.168.20.1', 'port' => NULL, 'dbname' => 'c:\\illuzion\\db\\people_cinema.gdb', 'user' => 'internet', 'password' => 12345678, 'driver' => 'pdo_mysql', 'driverOptions' => array('doctrineTransactionWait' => -1, 'doctrineTransactionIsolationLevel' => 3), 'driverClass' => 'Illuzion\\Common\\Doctrine\\FixPrepareDriver', 'defaultTableOptions' => array()), $b, $c, array());
    }

    /**
     * Gets the 'doctrine.dbal.ill_main_connection' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\DBAL\Connection A Doctrine\DBAL\Connection instance
     */
    protected function getDoctrine_Dbal_IllMainConnectionService()
    {
        $a = new \Doctrine\DBAL\Logging\LoggerChain();
        $a->addLogger(${($_ = isset($this->services['doctrine.dbal.logger']) ? $this->services['doctrine.dbal.logger'] : $this->getDoctrine_Dbal_LoggerService()) && false ?: '_'});
        $a->addLogger(new \Doctrine\DBAL\Logging\DebugStack());

        $b = new \Doctrine\DBAL\Configuration();
        $b->setSQLLogger($a);

        $c = new \Symfony\Bridge\Doctrine\ContainerAwareEventManager($this);
        $c->addEventSubscriber(${($_ = isset($this->services['api.listener.local_entity']) ? $this->services['api.listener.local_entity'] : $this->get('api.listener.local_entity')) && false ?: '_'});
        $c->addEventListener(array(0 => 'loadClassMetadata'), ${($_ = isset($this->services['doctrine.orm.ill_main_listeners.attach_entity_listeners']) ? $this->services['doctrine.orm.ill_main_listeners.attach_entity_listeners'] : $this->get('doctrine.orm.ill_main_listeners.attach_entity_listeners')) && false ?: '_'});

        return $this->services['doctrine.dbal.ill_main_connection'] = ${($_ = isset($this->services['doctrine.dbal.connection_factory']) ? $this->services['doctrine.dbal.connection_factory'] : $this->get('doctrine.dbal.connection_factory')) && false ?: '_'}->createConnection(array('host' => '192.168.20.1', 'port' => NULL, 'dbname' => 'c:\\illuzion\\db\\illusion_db.gdb', 'user' => 'internet', 'password' => 12345678, 'driver' => 'pdo_mysql', 'driverOptions' => array('doctrineTransactionWait' => -1, 'doctrineTransactionIsolationLevel' => 3), 'driverClass' => 'Illuzion\\Common\\Doctrine\\FixPrepareDriver', 'defaultTableOptions' => array()), $b, $c, array());
    }

    /**
     * Gets the 'doctrine.dbal.nmega_customer_connection' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\DBAL\Connection A Doctrine\DBAL\Connection instance
     */
    protected function getDoctrine_Dbal_NmegaCustomerConnectionService()
    {
        $a = new \Doctrine\DBAL\Logging\LoggerChain();
        $a->addLogger(${($_ = isset($this->services['doctrine.dbal.logger']) ? $this->services['doctrine.dbal.logger'] : $this->getDoctrine_Dbal_LoggerService()) && false ?: '_'});
        $a->addLogger(new \Doctrine\DBAL\Logging\DebugStack());

        $b = new \Doctrine\DBAL\Configuration();
        $b->setSQLLogger($a);

        $c = new \Symfony\Bridge\Doctrine\ContainerAwareEventManager($this);
        $c->addEventSubscriber(${($_ = isset($this->services['api.listener.local_entity']) ? $this->services['api.listener.local_entity'] : $this->get('api.listener.local_entity')) && false ?: '_'});
        $c->addEventListener(array(0 => 'loadClassMetadata'), ${($_ = isset($this->services['doctrine.orm.nmega_customer_listeners.attach_entity_listeners']) ? $this->services['doctrine.orm.nmega_customer_listeners.attach_entity_listeners'] : $this->get('doctrine.orm.nmega_customer_listeners.attach_entity_listeners')) && false ?: '_'});

        return $this->services['doctrine.dbal.nmega_customer_connection'] = ${($_ = isset($this->services['doctrine.dbal.connection_factory']) ? $this->services['doctrine.dbal.connection_factory'] : $this->get('doctrine.dbal.connection_factory')) && false ?: '_'}->createConnection(array('host' => '192.168.40.8', 'port' => NULL, 'dbname' => 'c:\\nmega\\people_cinema.gdb', 'user' => 'internet', 'password' => 12345678, 'driver' => 'pdo_mysql', 'driverOptions' => array('doctrineTransactionWait' => -1, 'doctrineTransactionIsolationLevel' => 3), 'driverClass' => 'Illuzion\\Common\\Doctrine\\FixPrepareDriver', 'defaultTableOptions' => array()), $b, $c, array());
    }

    /**
     * Gets the 'doctrine.dbal.nmega_main_connection' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\DBAL\Connection A Doctrine\DBAL\Connection instance
     */
    protected function getDoctrine_Dbal_NmegaMainConnectionService()
    {
        $a = new \Doctrine\DBAL\Logging\LoggerChain();
        $a->addLogger(${($_ = isset($this->services['doctrine.dbal.logger']) ? $this->services['doctrine.dbal.logger'] : $this->getDoctrine_Dbal_LoggerService()) && false ?: '_'});
        $a->addLogger(new \Doctrine\DBAL\Logging\DebugStack());

        $b = new \Doctrine\DBAL\Configuration();
        $b->setSQLLogger($a);

        $c = new \Symfony\Bridge\Doctrine\ContainerAwareEventManager($this);
        $c->addEventSubscriber(${($_ = isset($this->services['api.listener.local_entity']) ? $this->services['api.listener.local_entity'] : $this->get('api.listener.local_entity')) && false ?: '_'});
        $c->addEventListener(array(0 => 'loadClassMetadata'), ${($_ = isset($this->services['doctrine.orm.nmega_main_listeners.attach_entity_listeners']) ? $this->services['doctrine.orm.nmega_main_listeners.attach_entity_listeners'] : $this->get('doctrine.orm.nmega_main_listeners.attach_entity_listeners')) && false ?: '_'});

        return $this->services['doctrine.dbal.nmega_main_connection'] = ${($_ = isset($this->services['doctrine.dbal.connection_factory']) ? $this->services['doctrine.dbal.connection_factory'] : $this->get('doctrine.dbal.connection_factory')) && false ?: '_'}->createConnection(array('host' => '192.168.40.8', 'port' => NULL, 'dbname' => 'c:\\nmega\\nmega_db.gdb', 'user' => 'internet', 'password' => 12345678, 'driver' => 'pdo_mysql', 'driverOptions' => array('doctrineTransactionWait' => -1, 'doctrineTransactionIsolationLevel' => 3), 'driverClass' => 'Illuzion\\Common\\Doctrine\\FixPrepareDriver', 'defaultTableOptions' => array()), $b, $c, array());
    }

    /**
     * Gets the 'doctrine.dbal.oc_customer_connection' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\DBAL\Connection A Doctrine\DBAL\Connection instance
     */
    protected function getDoctrine_Dbal_OcCustomerConnectionService()
    {
        $a = new \Doctrine\DBAL\Logging\LoggerChain();
        $a->addLogger(${($_ = isset($this->services['doctrine.dbal.logger']) ? $this->services['doctrine.dbal.logger'] : $this->getDoctrine_Dbal_LoggerService()) && false ?: '_'});
        $a->addLogger(new \Doctrine\DBAL\Logging\DebugStack());

        $b = new \Doctrine\DBAL\Configuration();
        $b->setSQLLogger($a);

        $c = new \Symfony\Bridge\Doctrine\ContainerAwareEventManager($this);
        $c->addEventSubscriber(${($_ = isset($this->services['api.listener.local_entity']) ? $this->services['api.listener.local_entity'] : $this->get('api.listener.local_entity')) && false ?: '_'});
        $c->addEventListener(array(0 => 'loadClassMetadata'), ${($_ = isset($this->services['doctrine.orm.oc_customer_listeners.attach_entity_listeners']) ? $this->services['doctrine.orm.oc_customer_listeners.attach_entity_listeners'] : $this->get('doctrine.orm.oc_customer_listeners.attach_entity_listeners')) && false ?: '_'});

        return $this->services['doctrine.dbal.oc_customer_connection'] = ${($_ = isset($this->services['doctrine.dbal.connection_factory']) ? $this->services['doctrine.dbal.connection_factory'] : $this->get('doctrine.dbal.connection_factory')) && false ?: '_'}->createConnection(array('host' => '192.168.21.59', 'port' => NULL, 'dbname' => 'c:\\cinema\\db\\people_cinema.gdb', 'user' => 'internet', 'password' => 12345678, 'driver' => 'pdo_mysql', 'driverOptions' => array('doctrineTransactionWait' => -1, 'doctrineTransactionIsolationLevel' => 3), 'driverClass' => 'Illuzion\\Common\\Doctrine\\FixPrepareDriver', 'defaultTableOptions' => array()), $b, $c, array());
    }

    /**
     * Gets the 'doctrine.dbal.oc_main_connection' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\DBAL\Connection A Doctrine\DBAL\Connection instance
     */
    protected function getDoctrine_Dbal_OcMainConnectionService()
    {
        $a = new \Doctrine\DBAL\Logging\LoggerChain();
        $a->addLogger(${($_ = isset($this->services['doctrine.dbal.logger']) ? $this->services['doctrine.dbal.logger'] : $this->getDoctrine_Dbal_LoggerService()) && false ?: '_'});
        $a->addLogger(new \Doctrine\DBAL\Logging\DebugStack());

        $b = new \Doctrine\DBAL\Configuration();
        $b->setSQLLogger($a);

        $c = new \Symfony\Bridge\Doctrine\ContainerAwareEventManager($this);
        $c->addEventSubscriber(${($_ = isset($this->services['api.listener.local_entity']) ? $this->services['api.listener.local_entity'] : $this->get('api.listener.local_entity')) && false ?: '_'});
        $c->addEventListener(array(0 => 'loadClassMetadata'), ${($_ = isset($this->services['doctrine.orm.oc_main_listeners.attach_entity_listeners']) ? $this->services['doctrine.orm.oc_main_listeners.attach_entity_listeners'] : $this->get('doctrine.orm.oc_main_listeners.attach_entity_listeners')) && false ?: '_'});

        return $this->services['doctrine.dbal.oc_main_connection'] = ${($_ = isset($this->services['doctrine.dbal.connection_factory']) ? $this->services['doctrine.dbal.connection_factory'] : $this->get('doctrine.dbal.connection_factory')) && false ?: '_'}->createConnection(array('host' => '192.168.21.59', 'port' => NULL, 'dbname' => 'c:\\cinema\\db\\ocean_db.gdb', 'user' => 'internet', 'password' => 12345678, 'driver' => 'pdo_mysql', 'driverOptions' => array('doctrineTransactionWait' => -1, 'doctrineTransactionIsolationLevel' => 3), 'driverClass' => 'Illuzion\\Common\\Doctrine\\FixPrepareDriver', 'defaultTableOptions' => array()), $b, $c, array());
    }

    /**
     * Gets the 'doctrine.dbal.shared_connection' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\DBAL\Connection A Doctrine\DBAL\Connection instance
     */
    protected function getDoctrine_Dbal_SharedConnectionService()
    {
        $a = new \Doctrine\DBAL\Logging\LoggerChain();
        $a->addLogger(${($_ = isset($this->services['doctrine.dbal.logger']) ? $this->services['doctrine.dbal.logger'] : $this->getDoctrine_Dbal_LoggerService()) && false ?: '_'});
        $a->addLogger(new \Doctrine\DBAL\Logging\DebugStack());

        $b = new \Doctrine\DBAL\Configuration();
        $b->setSQLLogger($a);

        $c = new \Symfony\Bridge\Doctrine\ContainerAwareEventManager($this);
        $c->addEventSubscriber(${($_ = isset($this->services['api.listener.local_entity']) ? $this->services['api.listener.local_entity'] : $this->get('api.listener.local_entity')) && false ?: '_'});
        $c->addEventListener(array(0 => 'loadClassMetadata'), ${($_ = isset($this->services['doctrine.orm.shared_listeners.attach_entity_listeners']) ? $this->services['doctrine.orm.shared_listeners.attach_entity_listeners'] : $this->get('doctrine.orm.shared_listeners.attach_entity_listeners')) && false ?: '_'});

        return $this->services['doctrine.dbal.shared_connection'] = ${($_ = isset($this->services['doctrine.dbal.connection_factory']) ? $this->services['doctrine.dbal.connection_factory'] : $this->get('doctrine.dbal.connection_factory')) && false ?: '_'}->createConnection(array('driver' => 'pdo_mysql', 'host' => 'localhost', 'port' => NULL, 'dbname' => 'api_production', 'user' => 'api', 'password' => '1990api', 'driverOptions' => array(), 'defaultTableOptions' => array()), $b, $c, array());
    }

    /**
     * Gets the 'doctrine.dbal.uss_customer_connection' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\DBAL\Connection A Doctrine\DBAL\Connection instance
     */
    protected function getDoctrine_Dbal_UssCustomerConnectionService()
    {
        $a = new \Doctrine\DBAL\Logging\LoggerChain();
        $a->addLogger(${($_ = isset($this->services['doctrine.dbal.logger']) ? $this->services['doctrine.dbal.logger'] : $this->getDoctrine_Dbal_LoggerService()) && false ?: '_'});
        $a->addLogger(new \Doctrine\DBAL\Logging\DebugStack());

        $b = new \Doctrine\DBAL\Configuration();
        $b->setSQLLogger($a);

        $c = new \Symfony\Bridge\Doctrine\ContainerAwareEventManager($this);
        $c->addEventSubscriber(${($_ = isset($this->services['api.listener.local_entity']) ? $this->services['api.listener.local_entity'] : $this->get('api.listener.local_entity')) && false ?: '_'});
        $c->addEventListener(array(0 => 'loadClassMetadata'), ${($_ = isset($this->services['doctrine.orm.uss_customer_listeners.attach_entity_listeners']) ? $this->services['doctrine.orm.uss_customer_listeners.attach_entity_listeners'] : $this->get('doctrine.orm.uss_customer_listeners.attach_entity_listeners')) && false ?: '_'});

        return $this->services['doctrine.dbal.uss_customer_connection'] = ${($_ = isset($this->services['doctrine.dbal.connection_factory']) ? $this->services['doctrine.dbal.connection_factory'] : $this->get('doctrine.dbal.connection_factory')) && false ?: '_'}->createConnection(array('host' => '192.168.22.90', 'port' => NULL, 'dbname' => 'c:\\kassa\\people_cinema.gdb', 'user' => 'internet', 'password' => 12345678, 'driver' => 'pdo_mysql', 'driverOptions' => array('doctrineTransactionWait' => -1, 'doctrineTransactionIsolationLevel' => 3), 'driverClass' => 'Illuzion\\Common\\Doctrine\\FixPrepareDriver', 'defaultTableOptions' => array()), $b, $c, array());
    }

    /**
     * Gets the 'doctrine.dbal.uss_main_connection' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\DBAL\Connection A Doctrine\DBAL\Connection instance
     */
    protected function getDoctrine_Dbal_UssMainConnectionService()
    {
        $a = new \Doctrine\DBAL\Logging\LoggerChain();
        $a->addLogger(${($_ = isset($this->services['doctrine.dbal.logger']) ? $this->services['doctrine.dbal.logger'] : $this->getDoctrine_Dbal_LoggerService()) && false ?: '_'});
        $a->addLogger(new \Doctrine\DBAL\Logging\DebugStack());

        $b = new \Doctrine\DBAL\Configuration();
        $b->setSQLLogger($a);

        $c = new \Symfony\Bridge\Doctrine\ContainerAwareEventManager($this);
        $c->addEventSubscriber(${($_ = isset($this->services['api.listener.local_entity']) ? $this->services['api.listener.local_entity'] : $this->get('api.listener.local_entity')) && false ?: '_'});
        $c->addEventListener(array(0 => 'loadClassMetadata'), ${($_ = isset($this->services['doctrine.orm.uss_main_listeners.attach_entity_listeners']) ? $this->services['doctrine.orm.uss_main_listeners.attach_entity_listeners'] : $this->get('doctrine.orm.uss_main_listeners.attach_entity_listeners')) && false ?: '_'});
        $c->addEventListener(array(0 => 'postPersist', 1 => 'postUpdate', 2 => 'postRemove'), ${($_ = isset($this->services['synchronization.listener.entity_update']) ? $this->services['synchronization.listener.entity_update'] : $this->get('synchronization.listener.entity_update')) && false ?: '_'});

        return $this->services['doctrine.dbal.uss_main_connection'] = ${($_ = isset($this->services['doctrine.dbal.connection_factory']) ? $this->services['doctrine.dbal.connection_factory'] : $this->get('doctrine.dbal.connection_factory')) && false ?: '_'}->createConnection(array('host' => '192.168.22.90', 'port' => NULL, 'dbname' => 'c:\\kassa\\uss_db.gdb', 'user' => 'internet', 'password' => 12345678, 'driver' => 'pdo_mysql', 'driverOptions' => array('doctrineTransactionWait' => -1, 'doctrineTransactionIsolationLevel' => 3), 'driverClass' => 'Illuzion\\Common\\Doctrine\\FixPrepareDriver', 'defaultTableOptions' => array()), $b, $c, array());
    }

    /**
     * Gets the 'doctrine.orm.che_customer_entity_listener_resolver' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Bundle\DoctrineBundle\Mapping\ContainerAwareEntityListenerResolver A Doctrine\Bundle\DoctrineBundle\Mapping\ContainerAwareEntityListenerResolver instance
     */
    protected function getDoctrine_Orm_CheCustomerEntityListenerResolverService()
    {
        return $this->services['doctrine.orm.che_customer_entity_listener_resolver'] = new \Doctrine\Bundle\DoctrineBundle\Mapping\ContainerAwareEntityListenerResolver($this);
    }

    /**
     * Gets the 'doctrine.orm.che_customer_entity_manager' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @param bool    $lazyLoad whether to try lazy-loading the service with a proxy
     *
     * @return \Doctrine\ORM\EntityManager A Doctrine\ORM\EntityManager instance
     */
    public function getDoctrine_Orm_CheCustomerEntityManagerService($lazyLoad = true)
    {
        if ($lazyLoad) {

            return $this->services['doctrine.orm.che_customer_entity_manager'] = new DoctrineORMEntityManager_000000002084059000000000634d02687471c441876336872902bf356897607e(
                function (&$wrappedInstance, \ProxyManager\Proxy\LazyLoadingInterface $proxy) {
                    $wrappedInstance = $this->getDoctrine_Orm_CheCustomerEntityManagerService(false);

                    $proxy->setProxyInitializer(null);

                    return true;
                }
            );
        }

        $a = new \Doctrine\Common\Persistence\Mapping\Driver\MappingDriverChain();
        $a->addDriver(new \Doctrine\ORM\Mapping\Driver\AnnotationDriver(${($_ = isset($this->services['annotation_reader']) ? $this->services['annotation_reader'] : $this->get('annotation_reader')) && false ?: '_'}, array(0 => ($this->targetDirs[3].'/src/Illuzion/ApiBundle/Entity'))), 'Illuzion\\ApiBundle\\Entity\\Customer');

        $b = new \Doctrine\ORM\Configuration();
        $b->setEntityNamespaces(array('Customer' => 'Illuzion\\ApiBundle\\Entity\\Customer'));
        $b->setMetadataCacheImpl(${($_ = isset($this->services['doctrine_cache.providers.doctrine.orm.che_customer_metadata_cache']) ? $this->services['doctrine_cache.providers.doctrine.orm.che_customer_metadata_cache'] : $this->get('doctrine_cache.providers.doctrine.orm.che_customer_metadata_cache')) && false ?: '_'});
        $b->setQueryCacheImpl(${($_ = isset($this->services['doctrine_cache.providers.doctrine.orm.che_customer_query_cache']) ? $this->services['doctrine_cache.providers.doctrine.orm.che_customer_query_cache'] : $this->get('doctrine_cache.providers.doctrine.orm.che_customer_query_cache')) && false ?: '_'});
        $b->setResultCacheImpl(${($_ = isset($this->services['doctrine_cache.providers.doctrine.orm.che_customer_result_cache']) ? $this->services['doctrine_cache.providers.doctrine.orm.che_customer_result_cache'] : $this->get('doctrine_cache.providers.doctrine.orm.che_customer_result_cache')) && false ?: '_'});
        $b->setMetadataDriverImpl($a);
        $b->setProxyDir((__DIR__.'/doctrine/orm/Proxies'));
        $b->setProxyNamespace('Proxies');
        $b->setAutoGenerateProxyClasses(true);
        $b->setClassMetadataFactoryName('Doctrine\\ORM\\Mapping\\ClassMetadataFactory');
        $b->setDefaultRepositoryClassName('Doctrine\\ORM\\EntityRepository');
        $b->setNamingStrategy(${($_ = isset($this->services['doctrine.orm.naming_strategy.default']) ? $this->services['doctrine.orm.naming_strategy.default'] : $this->getDoctrine_Orm_NamingStrategy_DefaultService()) && false ?: '_'});
        $b->setQuoteStrategy(${($_ = isset($this->services['doctrine.orm.quote_strategy.default']) ? $this->services['doctrine.orm.quote_strategy.default'] : $this->getDoctrine_Orm_QuoteStrategy_DefaultService()) && false ?: '_'});
        $b->setEntityListenerResolver(${($_ = isset($this->services['doctrine.orm.che_customer_entity_listener_resolver']) ? $this->services['doctrine.orm.che_customer_entity_listener_resolver'] : $this->get('doctrine.orm.che_customer_entity_listener_resolver')) && false ?: '_'});
        $b->addCustomNumericFunction('bitand', 'Illuzion\\Common\\Doctrine\\BitAndFunction');
        $b->addCustomNumericFunction('rupper', 'Illuzion\\Common\\Doctrine\\RupperFunction');

        $instance = \Doctrine\ORM\EntityManager::create(${($_ = isset($this->services['doctrine.dbal.che_customer_connection']) ? $this->services['doctrine.dbal.che_customer_connection'] : $this->get('doctrine.dbal.che_customer_connection')) && false ?: '_'}, $b);

        ${($_ = isset($this->services['doctrine.orm.che_customer_manager_configurator']) ? $this->services['doctrine.orm.che_customer_manager_configurator'] : $this->get('doctrine.orm.che_customer_manager_configurator')) && false ?: '_'}->configure($instance);

        return $instance;
    }

    /**
     * Gets the 'doctrine.orm.che_customer_entity_manager.property_info_extractor' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Bridge\Doctrine\PropertyInfo\DoctrineExtractor A Symfony\Bridge\Doctrine\PropertyInfo\DoctrineExtractor instance
     */
    protected function getDoctrine_Orm_CheCustomerEntityManager_PropertyInfoExtractorService()
    {
        return $this->services['doctrine.orm.che_customer_entity_manager.property_info_extractor'] = new \Symfony\Bridge\Doctrine\PropertyInfo\DoctrineExtractor(${($_ = isset($this->services['doctrine.orm.che_customer_entity_manager']) ? $this->services['doctrine.orm.che_customer_entity_manager'] : $this->get('doctrine.orm.che_customer_entity_manager')) && false ?: '_'}->getMetadataFactory());
    }

    /**
     * Gets the 'doctrine.orm.che_customer_listeners.attach_entity_listeners' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\ORM\Tools\AttachEntityListenersListener A Doctrine\ORM\Tools\AttachEntityListenersListener instance
     */
    protected function getDoctrine_Orm_CheCustomerListeners_AttachEntityListenersService()
    {
        return $this->services['doctrine.orm.che_customer_listeners.attach_entity_listeners'] = new \Doctrine\ORM\Tools\AttachEntityListenersListener();
    }

    /**
     * Gets the 'doctrine.orm.che_customer_manager_configurator' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Bundle\DoctrineBundle\ManagerConfigurator A Doctrine\Bundle\DoctrineBundle\ManagerConfigurator instance
     */
    protected function getDoctrine_Orm_CheCustomerManagerConfiguratorService()
    {
        return $this->services['doctrine.orm.che_customer_manager_configurator'] = new \Doctrine\Bundle\DoctrineBundle\ManagerConfigurator(array(), array());
    }

    /**
     * Gets the 'doctrine.orm.che_main_entity_listener_resolver' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Bundle\DoctrineBundle\Mapping\ContainerAwareEntityListenerResolver A Doctrine\Bundle\DoctrineBundle\Mapping\ContainerAwareEntityListenerResolver instance
     */
    protected function getDoctrine_Orm_CheMainEntityListenerResolverService()
    {
        return $this->services['doctrine.orm.che_main_entity_listener_resolver'] = new \Doctrine\Bundle\DoctrineBundle\Mapping\ContainerAwareEntityListenerResolver($this);
    }

    /**
     * Gets the 'doctrine.orm.che_main_entity_manager' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @param bool    $lazyLoad whether to try lazy-loading the service with a proxy
     *
     * @return \Doctrine\ORM\EntityManager A Doctrine\ORM\EntityManager instance
     */
    public function getDoctrine_Orm_CheMainEntityManagerService($lazyLoad = true)
    {
        if ($lazyLoad) {

            return $this->services['doctrine.orm.che_main_entity_manager'] = new DoctrineORMEntityManager_000000002084059e00000000634d02687471c441876336872902bf356897607e(
                function (&$wrappedInstance, \ProxyManager\Proxy\LazyLoadingInterface $proxy) {
                    $wrappedInstance = $this->getDoctrine_Orm_CheMainEntityManagerService(false);

                    $proxy->setProxyInitializer(null);

                    return true;
                }
            );
        }

        $a = new \Doctrine\Common\Persistence\Mapping\Driver\MappingDriverChain();
        $a->addDriver(new \Doctrine\ORM\Mapping\Driver\AnnotationDriver(${($_ = isset($this->services['annotation_reader']) ? $this->services['annotation_reader'] : $this->get('annotation_reader')) && false ?: '_'}, array(0 => ($this->targetDirs[3].'/src/Illuzion/ApiBundle/Entity'))), 'Illuzion\\ApiBundle\\Entity\\Main');

        $b = new \Doctrine\ORM\Configuration();
        $b->setEntityNamespaces(array('Main' => 'Illuzion\\ApiBundle\\Entity\\Main'));
        $b->setMetadataCacheImpl(${($_ = isset($this->services['doctrine_cache.providers.doctrine.orm.che_main_metadata_cache']) ? $this->services['doctrine_cache.providers.doctrine.orm.che_main_metadata_cache'] : $this->get('doctrine_cache.providers.doctrine.orm.che_main_metadata_cache')) && false ?: '_'});
        $b->setQueryCacheImpl(${($_ = isset($this->services['doctrine_cache.providers.doctrine.orm.che_main_query_cache']) ? $this->services['doctrine_cache.providers.doctrine.orm.che_main_query_cache'] : $this->get('doctrine_cache.providers.doctrine.orm.che_main_query_cache')) && false ?: '_'});
        $b->setResultCacheImpl(${($_ = isset($this->services['doctrine_cache.providers.doctrine.orm.che_main_result_cache']) ? $this->services['doctrine_cache.providers.doctrine.orm.che_main_result_cache'] : $this->get('doctrine_cache.providers.doctrine.orm.che_main_result_cache')) && false ?: '_'});
        $b->setMetadataDriverImpl($a);
        $b->setProxyDir((__DIR__.'/doctrine/orm/Proxies'));
        $b->setProxyNamespace('Proxies');
        $b->setAutoGenerateProxyClasses(true);
        $b->setClassMetadataFactoryName('Doctrine\\ORM\\Mapping\\ClassMetadataFactory');
        $b->setDefaultRepositoryClassName('Doctrine\\ORM\\EntityRepository');
        $b->setNamingStrategy(${($_ = isset($this->services['doctrine.orm.naming_strategy.default']) ? $this->services['doctrine.orm.naming_strategy.default'] : $this->getDoctrine_Orm_NamingStrategy_DefaultService()) && false ?: '_'});
        $b->setQuoteStrategy(${($_ = isset($this->services['doctrine.orm.quote_strategy.default']) ? $this->services['doctrine.orm.quote_strategy.default'] : $this->getDoctrine_Orm_QuoteStrategy_DefaultService()) && false ?: '_'});
        $b->setEntityListenerResolver(${($_ = isset($this->services['doctrine.orm.che_main_entity_listener_resolver']) ? $this->services['doctrine.orm.che_main_entity_listener_resolver'] : $this->get('doctrine.orm.che_main_entity_listener_resolver')) && false ?: '_'});
        $b->addCustomNumericFunction('bitand', 'Illuzion\\Common\\Doctrine\\BitAndFunction');
        $b->addCustomNumericFunction('rupper', 'Illuzion\\Common\\Doctrine\\RupperFunction');

        $instance = \Doctrine\ORM\EntityManager::create(${($_ = isset($this->services['doctrine.dbal.che_main_connection']) ? $this->services['doctrine.dbal.che_main_connection'] : $this->get('doctrine.dbal.che_main_connection')) && false ?: '_'}, $b);

        ${($_ = isset($this->services['doctrine.orm.che_main_manager_configurator']) ? $this->services['doctrine.orm.che_main_manager_configurator'] : $this->get('doctrine.orm.che_main_manager_configurator')) && false ?: '_'}->configure($instance);

        return $instance;
    }

    /**
     * Gets the 'doctrine.orm.che_main_entity_manager.property_info_extractor' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Bridge\Doctrine\PropertyInfo\DoctrineExtractor A Symfony\Bridge\Doctrine\PropertyInfo\DoctrineExtractor instance
     */
    protected function getDoctrine_Orm_CheMainEntityManager_PropertyInfoExtractorService()
    {
        return $this->services['doctrine.orm.che_main_entity_manager.property_info_extractor'] = new \Symfony\Bridge\Doctrine\PropertyInfo\DoctrineExtractor(${($_ = isset($this->services['doctrine.orm.che_main_entity_manager']) ? $this->services['doctrine.orm.che_main_entity_manager'] : $this->get('doctrine.orm.che_main_entity_manager')) && false ?: '_'}->getMetadataFactory());
    }

    /**
     * Gets the 'doctrine.orm.che_main_listeners.attach_entity_listeners' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\ORM\Tools\AttachEntityListenersListener A Doctrine\ORM\Tools\AttachEntityListenersListener instance
     */
    protected function getDoctrine_Orm_CheMainListeners_AttachEntityListenersService()
    {
        return $this->services['doctrine.orm.che_main_listeners.attach_entity_listeners'] = new \Doctrine\ORM\Tools\AttachEntityListenersListener();
    }

    /**
     * Gets the 'doctrine.orm.che_main_manager_configurator' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Bundle\DoctrineBundle\ManagerConfigurator A Doctrine\Bundle\DoctrineBundle\ManagerConfigurator instance
     */
    protected function getDoctrine_Orm_CheMainManagerConfiguratorService()
    {
        return $this->services['doctrine.orm.che_main_manager_configurator'] = new \Doctrine\Bundle\DoctrineBundle\ManagerConfigurator(array(), array());
    }

    /**
     * Gets the 'doctrine.orm.ill_customer_entity_listener_resolver' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Bundle\DoctrineBundle\Mapping\ContainerAwareEntityListenerResolver A Doctrine\Bundle\DoctrineBundle\Mapping\ContainerAwareEntityListenerResolver instance
     */
    protected function getDoctrine_Orm_IllCustomerEntityListenerResolverService()
    {
        return $this->services['doctrine.orm.ill_customer_entity_listener_resolver'] = new \Doctrine\Bundle\DoctrineBundle\Mapping\ContainerAwareEntityListenerResolver($this);
    }

    /**
     * Gets the 'doctrine.orm.ill_customer_entity_manager' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @param bool    $lazyLoad whether to try lazy-loading the service with a proxy
     *
     * @return \Doctrine\ORM\EntityManager A Doctrine\ORM\EntityManager instance
     */
    public function getDoctrine_Orm_IllCustomerEntityManagerService($lazyLoad = true)
    {
        if ($lazyLoad) {

            return $this->services['doctrine.orm.ill_customer_entity_manager'] = new DoctrineORMEntityManager_000000002084058400000000634d02687471c441876336872902bf356897607e(
                function (&$wrappedInstance, \ProxyManager\Proxy\LazyLoadingInterface $proxy) {
                    $wrappedInstance = $this->getDoctrine_Orm_IllCustomerEntityManagerService(false);

                    $proxy->setProxyInitializer(null);

                    return true;
                }
            );
        }

        $a = new \Doctrine\Common\Persistence\Mapping\Driver\MappingDriverChain();
        $a->addDriver(new \Doctrine\ORM\Mapping\Driver\AnnotationDriver(${($_ = isset($this->services['annotation_reader']) ? $this->services['annotation_reader'] : $this->get('annotation_reader')) && false ?: '_'}, array(0 => ($this->targetDirs[3].'/src/Illuzion/ApiBundle/Entity'))), 'Illuzion\\ApiBundle\\Entity\\Customer');

        $b = new \Doctrine\ORM\Configuration();
        $b->setEntityNamespaces(array('Customer' => 'Illuzion\\ApiBundle\\Entity\\Customer'));
        $b->setMetadataCacheImpl(${($_ = isset($this->services['doctrine_cache.providers.doctrine.orm.ill_customer_metadata_cache']) ? $this->services['doctrine_cache.providers.doctrine.orm.ill_customer_metadata_cache'] : $this->get('doctrine_cache.providers.doctrine.orm.ill_customer_metadata_cache')) && false ?: '_'});
        $b->setQueryCacheImpl(${($_ = isset($this->services['doctrine_cache.providers.doctrine.orm.ill_customer_query_cache']) ? $this->services['doctrine_cache.providers.doctrine.orm.ill_customer_query_cache'] : $this->get('doctrine_cache.providers.doctrine.orm.ill_customer_query_cache')) && false ?: '_'});
        $b->setResultCacheImpl(${($_ = isset($this->services['doctrine_cache.providers.doctrine.orm.ill_customer_result_cache']) ? $this->services['doctrine_cache.providers.doctrine.orm.ill_customer_result_cache'] : $this->get('doctrine_cache.providers.doctrine.orm.ill_customer_result_cache')) && false ?: '_'});
        $b->setMetadataDriverImpl($a);
        $b->setProxyDir((__DIR__.'/doctrine/orm/Proxies'));
        $b->setProxyNamespace('Proxies');
        $b->setAutoGenerateProxyClasses(true);
        $b->setClassMetadataFactoryName('Doctrine\\ORM\\Mapping\\ClassMetadataFactory');
        $b->setDefaultRepositoryClassName('Doctrine\\ORM\\EntityRepository');
        $b->setNamingStrategy(${($_ = isset($this->services['doctrine.orm.naming_strategy.default']) ? $this->services['doctrine.orm.naming_strategy.default'] : $this->getDoctrine_Orm_NamingStrategy_DefaultService()) && false ?: '_'});
        $b->setQuoteStrategy(${($_ = isset($this->services['doctrine.orm.quote_strategy.default']) ? $this->services['doctrine.orm.quote_strategy.default'] : $this->getDoctrine_Orm_QuoteStrategy_DefaultService()) && false ?: '_'});
        $b->setEntityListenerResolver(${($_ = isset($this->services['doctrine.orm.ill_customer_entity_listener_resolver']) ? $this->services['doctrine.orm.ill_customer_entity_listener_resolver'] : $this->get('doctrine.orm.ill_customer_entity_listener_resolver')) && false ?: '_'});
        $b->addCustomNumericFunction('bitand', 'Illuzion\\Common\\Doctrine\\BitAndFunction');
        $b->addCustomNumericFunction('rupper', 'Illuzion\\Common\\Doctrine\\RupperFunction');

        $instance = \Doctrine\ORM\EntityManager::create(${($_ = isset($this->services['doctrine.dbal.ill_customer_connection']) ? $this->services['doctrine.dbal.ill_customer_connection'] : $this->get('doctrine.dbal.ill_customer_connection')) && false ?: '_'}, $b);

        ${($_ = isset($this->services['doctrine.orm.ill_customer_manager_configurator']) ? $this->services['doctrine.orm.ill_customer_manager_configurator'] : $this->get('doctrine.orm.ill_customer_manager_configurator')) && false ?: '_'}->configure($instance);

        return $instance;
    }

    /**
     * Gets the 'doctrine.orm.ill_customer_entity_manager.property_info_extractor' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Bridge\Doctrine\PropertyInfo\DoctrineExtractor A Symfony\Bridge\Doctrine\PropertyInfo\DoctrineExtractor instance
     */
    protected function getDoctrine_Orm_IllCustomerEntityManager_PropertyInfoExtractorService()
    {
        return $this->services['doctrine.orm.ill_customer_entity_manager.property_info_extractor'] = new \Symfony\Bridge\Doctrine\PropertyInfo\DoctrineExtractor(${($_ = isset($this->services['doctrine.orm.ill_customer_entity_manager']) ? $this->services['doctrine.orm.ill_customer_entity_manager'] : $this->get('doctrine.orm.ill_customer_entity_manager')) && false ?: '_'}->getMetadataFactory());
    }

    /**
     * Gets the 'doctrine.orm.ill_customer_listeners.attach_entity_listeners' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\ORM\Tools\AttachEntityListenersListener A Doctrine\ORM\Tools\AttachEntityListenersListener instance
     */
    protected function getDoctrine_Orm_IllCustomerListeners_AttachEntityListenersService()
    {
        return $this->services['doctrine.orm.ill_customer_listeners.attach_entity_listeners'] = new \Doctrine\ORM\Tools\AttachEntityListenersListener();
    }

    /**
     * Gets the 'doctrine.orm.ill_customer_manager_configurator' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Bundle\DoctrineBundle\ManagerConfigurator A Doctrine\Bundle\DoctrineBundle\ManagerConfigurator instance
     */
    protected function getDoctrine_Orm_IllCustomerManagerConfiguratorService()
    {
        return $this->services['doctrine.orm.ill_customer_manager_configurator'] = new \Doctrine\Bundle\DoctrineBundle\ManagerConfigurator(array(), array());
    }

    /**
     * Gets the 'doctrine.orm.ill_main_entity_listener_resolver' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Bundle\DoctrineBundle\Mapping\ContainerAwareEntityListenerResolver A Doctrine\Bundle\DoctrineBundle\Mapping\ContainerAwareEntityListenerResolver instance
     */
    protected function getDoctrine_Orm_IllMainEntityListenerResolverService()
    {
        return $this->services['doctrine.orm.ill_main_entity_listener_resolver'] = new \Doctrine\Bundle\DoctrineBundle\Mapping\ContainerAwareEntityListenerResolver($this);
    }

    /**
     * Gets the 'doctrine.orm.ill_main_entity_manager' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @param bool    $lazyLoad whether to try lazy-loading the service with a proxy
     *
     * @return \Doctrine\ORM\EntityManager A Doctrine\ORM\EntityManager instance
     */
    public function getDoctrine_Orm_IllMainEntityManagerService($lazyLoad = true)
    {
        if ($lazyLoad) {

            return $this->services['doctrine.orm.ill_main_entity_manager'] = new DoctrineORMEntityManager_000000002084058200000000634d02687471c441876336872902bf356897607e(
                function (&$wrappedInstance, \ProxyManager\Proxy\LazyLoadingInterface $proxy) {
                    $wrappedInstance = $this->getDoctrine_Orm_IllMainEntityManagerService(false);

                    $proxy->setProxyInitializer(null);

                    return true;
                }
            );
        }

        $a = new \Doctrine\Common\Persistence\Mapping\Driver\MappingDriverChain();
        $a->addDriver(new \Doctrine\ORM\Mapping\Driver\AnnotationDriver(${($_ = isset($this->services['annotation_reader']) ? $this->services['annotation_reader'] : $this->get('annotation_reader')) && false ?: '_'}, array(0 => ($this->targetDirs[3].'/src/Illuzion/ApiBundle/Entity'))), 'Illuzion\\ApiBundle\\Entity\\Main');

        $b = new \Doctrine\ORM\Configuration();
        $b->setEntityNamespaces(array('Main' => 'Illuzion\\ApiBundle\\Entity\\Main'));
        $b->setMetadataCacheImpl(${($_ = isset($this->services['doctrine_cache.providers.doctrine.orm.ill_main_metadata_cache']) ? $this->services['doctrine_cache.providers.doctrine.orm.ill_main_metadata_cache'] : $this->get('doctrine_cache.providers.doctrine.orm.ill_main_metadata_cache')) && false ?: '_'});
        $b->setQueryCacheImpl(${($_ = isset($this->services['doctrine_cache.providers.doctrine.orm.ill_main_query_cache']) ? $this->services['doctrine_cache.providers.doctrine.orm.ill_main_query_cache'] : $this->get('doctrine_cache.providers.doctrine.orm.ill_main_query_cache')) && false ?: '_'});
        $b->setResultCacheImpl(${($_ = isset($this->services['doctrine_cache.providers.doctrine.orm.ill_main_result_cache']) ? $this->services['doctrine_cache.providers.doctrine.orm.ill_main_result_cache'] : $this->get('doctrine_cache.providers.doctrine.orm.ill_main_result_cache')) && false ?: '_'});
        $b->setMetadataDriverImpl($a);
        $b->setProxyDir((__DIR__.'/doctrine/orm/Proxies'));
        $b->setProxyNamespace('Proxies');
        $b->setAutoGenerateProxyClasses(true);
        $b->setClassMetadataFactoryName('Doctrine\\ORM\\Mapping\\ClassMetadataFactory');
        $b->setDefaultRepositoryClassName('Doctrine\\ORM\\EntityRepository');
        $b->setNamingStrategy(${($_ = isset($this->services['doctrine.orm.naming_strategy.default']) ? $this->services['doctrine.orm.naming_strategy.default'] : $this->getDoctrine_Orm_NamingStrategy_DefaultService()) && false ?: '_'});
        $b->setQuoteStrategy(${($_ = isset($this->services['doctrine.orm.quote_strategy.default']) ? $this->services['doctrine.orm.quote_strategy.default'] : $this->getDoctrine_Orm_QuoteStrategy_DefaultService()) && false ?: '_'});
        $b->setEntityListenerResolver(${($_ = isset($this->services['doctrine.orm.ill_main_entity_listener_resolver']) ? $this->services['doctrine.orm.ill_main_entity_listener_resolver'] : $this->get('doctrine.orm.ill_main_entity_listener_resolver')) && false ?: '_'});
        $b->addCustomNumericFunction('bitand', 'Illuzion\\Common\\Doctrine\\BitAndFunction');
        $b->addCustomNumericFunction('rupper', 'Illuzion\\Common\\Doctrine\\RupperFunction');

        $instance = \Doctrine\ORM\EntityManager::create(${($_ = isset($this->services['doctrine.dbal.ill_main_connection']) ? $this->services['doctrine.dbal.ill_main_connection'] : $this->get('doctrine.dbal.ill_main_connection')) && false ?: '_'}, $b);

        ${($_ = isset($this->services['doctrine.orm.ill_main_manager_configurator']) ? $this->services['doctrine.orm.ill_main_manager_configurator'] : $this->get('doctrine.orm.ill_main_manager_configurator')) && false ?: '_'}->configure($instance);

        return $instance;
    }

    /**
     * Gets the 'doctrine.orm.ill_main_entity_manager.property_info_extractor' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Bridge\Doctrine\PropertyInfo\DoctrineExtractor A Symfony\Bridge\Doctrine\PropertyInfo\DoctrineExtractor instance
     */
    protected function getDoctrine_Orm_IllMainEntityManager_PropertyInfoExtractorService()
    {
        return $this->services['doctrine.orm.ill_main_entity_manager.property_info_extractor'] = new \Symfony\Bridge\Doctrine\PropertyInfo\DoctrineExtractor(${($_ = isset($this->services['doctrine.orm.ill_main_entity_manager']) ? $this->services['doctrine.orm.ill_main_entity_manager'] : $this->get('doctrine.orm.ill_main_entity_manager')) && false ?: '_'}->getMetadataFactory());
    }

    /**
     * Gets the 'doctrine.orm.ill_main_listeners.attach_entity_listeners' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\ORM\Tools\AttachEntityListenersListener A Doctrine\ORM\Tools\AttachEntityListenersListener instance
     */
    protected function getDoctrine_Orm_IllMainListeners_AttachEntityListenersService()
    {
        return $this->services['doctrine.orm.ill_main_listeners.attach_entity_listeners'] = new \Doctrine\ORM\Tools\AttachEntityListenersListener();
    }

    /**
     * Gets the 'doctrine.orm.ill_main_manager_configurator' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Bundle\DoctrineBundle\ManagerConfigurator A Doctrine\Bundle\DoctrineBundle\ManagerConfigurator instance
     */
    protected function getDoctrine_Orm_IllMainManagerConfiguratorService()
    {
        return $this->services['doctrine.orm.ill_main_manager_configurator'] = new \Doctrine\Bundle\DoctrineBundle\ManagerConfigurator(array(), array());
    }

    /**
     * Gets the 'doctrine.orm.nmega_customer_entity_listener_resolver' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Bundle\DoctrineBundle\Mapping\ContainerAwareEntityListenerResolver A Doctrine\Bundle\DoctrineBundle\Mapping\ContainerAwareEntityListenerResolver instance
     */
    protected function getDoctrine_Orm_NmegaCustomerEntityListenerResolverService()
    {
        return $this->services['doctrine.orm.nmega_customer_entity_listener_resolver'] = new \Doctrine\Bundle\DoctrineBundle\Mapping\ContainerAwareEntityListenerResolver($this);
    }

    /**
     * Gets the 'doctrine.orm.nmega_customer_entity_manager' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @param bool    $lazyLoad whether to try lazy-loading the service with a proxy
     *
     * @return \Doctrine\ORM\EntityManager A Doctrine\ORM\EntityManager instance
     */
    public function getDoctrine_Orm_NmegaCustomerEntityManagerService($lazyLoad = true)
    {
        if ($lazyLoad) {

            return $this->services['doctrine.orm.nmega_customer_entity_manager'] = new DoctrineORMEntityManager_00000000208405ac00000000634d02687471c441876336872902bf356897607e(
                function (&$wrappedInstance, \ProxyManager\Proxy\LazyLoadingInterface $proxy) {
                    $wrappedInstance = $this->getDoctrine_Orm_NmegaCustomerEntityManagerService(false);

                    $proxy->setProxyInitializer(null);

                    return true;
                }
            );
        }

        $a = new \Doctrine\Common\Persistence\Mapping\Driver\MappingDriverChain();
        $a->addDriver(new \Doctrine\ORM\Mapping\Driver\AnnotationDriver(${($_ = isset($this->services['annotation_reader']) ? $this->services['annotation_reader'] : $this->get('annotation_reader')) && false ?: '_'}, array(0 => ($this->targetDirs[3].'/src/Illuzion/ApiBundle/Entity'))), 'Illuzion\\ApiBundle\\Entity\\Customer');

        $b = new \Doctrine\ORM\Configuration();
        $b->setEntityNamespaces(array('Customer' => 'Illuzion\\ApiBundle\\Entity\\Customer'));
        $b->setMetadataCacheImpl(${($_ = isset($this->services['doctrine_cache.providers.doctrine.orm.nmega_customer_metadata_cache']) ? $this->services['doctrine_cache.providers.doctrine.orm.nmega_customer_metadata_cache'] : $this->get('doctrine_cache.providers.doctrine.orm.nmega_customer_metadata_cache')) && false ?: '_'});
        $b->setQueryCacheImpl(${($_ = isset($this->services['doctrine_cache.providers.doctrine.orm.nmega_customer_query_cache']) ? $this->services['doctrine_cache.providers.doctrine.orm.nmega_customer_query_cache'] : $this->get('doctrine_cache.providers.doctrine.orm.nmega_customer_query_cache')) && false ?: '_'});
        $b->setResultCacheImpl(${($_ = isset($this->services['doctrine_cache.providers.doctrine.orm.nmega_customer_result_cache']) ? $this->services['doctrine_cache.providers.doctrine.orm.nmega_customer_result_cache'] : $this->get('doctrine_cache.providers.doctrine.orm.nmega_customer_result_cache')) && false ?: '_'});
        $b->setMetadataDriverImpl($a);
        $b->setProxyDir((__DIR__.'/doctrine/orm/Proxies'));
        $b->setProxyNamespace('Proxies');
        $b->setAutoGenerateProxyClasses(true);
        $b->setClassMetadataFactoryName('Doctrine\\ORM\\Mapping\\ClassMetadataFactory');
        $b->setDefaultRepositoryClassName('Doctrine\\ORM\\EntityRepository');
        $b->setNamingStrategy(${($_ = isset($this->services['doctrine.orm.naming_strategy.default']) ? $this->services['doctrine.orm.naming_strategy.default'] : $this->getDoctrine_Orm_NamingStrategy_DefaultService()) && false ?: '_'});
        $b->setQuoteStrategy(${($_ = isset($this->services['doctrine.orm.quote_strategy.default']) ? $this->services['doctrine.orm.quote_strategy.default'] : $this->getDoctrine_Orm_QuoteStrategy_DefaultService()) && false ?: '_'});
        $b->setEntityListenerResolver(${($_ = isset($this->services['doctrine.orm.nmega_customer_entity_listener_resolver']) ? $this->services['doctrine.orm.nmega_customer_entity_listener_resolver'] : $this->get('doctrine.orm.nmega_customer_entity_listener_resolver')) && false ?: '_'});
        $b->addCustomNumericFunction('bitand', 'Illuzion\\Common\\Doctrine\\BitAndFunction');
        $b->addCustomNumericFunction('rupper', 'Illuzion\\Common\\Doctrine\\RupperFunction');

        $instance = \Doctrine\ORM\EntityManager::create(${($_ = isset($this->services['doctrine.dbal.nmega_customer_connection']) ? $this->services['doctrine.dbal.nmega_customer_connection'] : $this->get('doctrine.dbal.nmega_customer_connection')) && false ?: '_'}, $b);

        ${($_ = isset($this->services['doctrine.orm.nmega_customer_manager_configurator']) ? $this->services['doctrine.orm.nmega_customer_manager_configurator'] : $this->get('doctrine.orm.nmega_customer_manager_configurator')) && false ?: '_'}->configure($instance);

        return $instance;
    }

    /**
     * Gets the 'doctrine.orm.nmega_customer_entity_manager.property_info_extractor' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Bridge\Doctrine\PropertyInfo\DoctrineExtractor A Symfony\Bridge\Doctrine\PropertyInfo\DoctrineExtractor instance
     */
    protected function getDoctrine_Orm_NmegaCustomerEntityManager_PropertyInfoExtractorService()
    {
        return $this->services['doctrine.orm.nmega_customer_entity_manager.property_info_extractor'] = new \Symfony\Bridge\Doctrine\PropertyInfo\DoctrineExtractor(${($_ = isset($this->services['doctrine.orm.nmega_customer_entity_manager']) ? $this->services['doctrine.orm.nmega_customer_entity_manager'] : $this->get('doctrine.orm.nmega_customer_entity_manager')) && false ?: '_'}->getMetadataFactory());
    }

    /**
     * Gets the 'doctrine.orm.nmega_customer_listeners.attach_entity_listeners' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\ORM\Tools\AttachEntityListenersListener A Doctrine\ORM\Tools\AttachEntityListenersListener instance
     */
    protected function getDoctrine_Orm_NmegaCustomerListeners_AttachEntityListenersService()
    {
        return $this->services['doctrine.orm.nmega_customer_listeners.attach_entity_listeners'] = new \Doctrine\ORM\Tools\AttachEntityListenersListener();
    }

    /**
     * Gets the 'doctrine.orm.nmega_customer_manager_configurator' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Bundle\DoctrineBundle\ManagerConfigurator A Doctrine\Bundle\DoctrineBundle\ManagerConfigurator instance
     */
    protected function getDoctrine_Orm_NmegaCustomerManagerConfiguratorService()
    {
        return $this->services['doctrine.orm.nmega_customer_manager_configurator'] = new \Doctrine\Bundle\DoctrineBundle\ManagerConfigurator(array(), array());
    }

    /**
     * Gets the 'doctrine.orm.nmega_main_entity_listener_resolver' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Bundle\DoctrineBundle\Mapping\ContainerAwareEntityListenerResolver A Doctrine\Bundle\DoctrineBundle\Mapping\ContainerAwareEntityListenerResolver instance
     */
    protected function getDoctrine_Orm_NmegaMainEntityListenerResolverService()
    {
        return $this->services['doctrine.orm.nmega_main_entity_listener_resolver'] = new \Doctrine\Bundle\DoctrineBundle\Mapping\ContainerAwareEntityListenerResolver($this);
    }

    /**
     * Gets the 'doctrine.orm.nmega_main_entity_manager' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @param bool    $lazyLoad whether to try lazy-loading the service with a proxy
     *
     * @return \Doctrine\ORM\EntityManager A Doctrine\ORM\EntityManager instance
     */
    public function getDoctrine_Orm_NmegaMainEntityManagerService($lazyLoad = true)
    {
        if ($lazyLoad) {

            return $this->services['doctrine.orm.nmega_main_entity_manager'] = new DoctrineORMEntityManager_00000000208405aa00000000634d02687471c441876336872902bf356897607e(
                function (&$wrappedInstance, \ProxyManager\Proxy\LazyLoadingInterface $proxy) {
                    $wrappedInstance = $this->getDoctrine_Orm_NmegaMainEntityManagerService(false);

                    $proxy->setProxyInitializer(null);

                    return true;
                }
            );
        }

        $a = new \Doctrine\Common\Persistence\Mapping\Driver\MappingDriverChain();
        $a->addDriver(new \Doctrine\ORM\Mapping\Driver\AnnotationDriver(${($_ = isset($this->services['annotation_reader']) ? $this->services['annotation_reader'] : $this->get('annotation_reader')) && false ?: '_'}, array(0 => ($this->targetDirs[3].'/src/Illuzion/ApiBundle/Entity'))), 'Illuzion\\ApiBundle\\Entity\\Main');

        $b = new \Doctrine\ORM\Configuration();
        $b->setEntityNamespaces(array('Main' => 'Illuzion\\ApiBundle\\Entity\\Main'));
        $b->setMetadataCacheImpl(${($_ = isset($this->services['doctrine_cache.providers.doctrine.orm.nmega_main_metadata_cache']) ? $this->services['doctrine_cache.providers.doctrine.orm.nmega_main_metadata_cache'] : $this->get('doctrine_cache.providers.doctrine.orm.nmega_main_metadata_cache')) && false ?: '_'});
        $b->setQueryCacheImpl(${($_ = isset($this->services['doctrine_cache.providers.doctrine.orm.nmega_main_query_cache']) ? $this->services['doctrine_cache.providers.doctrine.orm.nmega_main_query_cache'] : $this->get('doctrine_cache.providers.doctrine.orm.nmega_main_query_cache')) && false ?: '_'});
        $b->setResultCacheImpl(${($_ = isset($this->services['doctrine_cache.providers.doctrine.orm.nmega_main_result_cache']) ? $this->services['doctrine_cache.providers.doctrine.orm.nmega_main_result_cache'] : $this->get('doctrine_cache.providers.doctrine.orm.nmega_main_result_cache')) && false ?: '_'});
        $b->setMetadataDriverImpl($a);
        $b->setProxyDir((__DIR__.'/doctrine/orm/Proxies'));
        $b->setProxyNamespace('Proxies');
        $b->setAutoGenerateProxyClasses(true);
        $b->setClassMetadataFactoryName('Doctrine\\ORM\\Mapping\\ClassMetadataFactory');
        $b->setDefaultRepositoryClassName('Doctrine\\ORM\\EntityRepository');
        $b->setNamingStrategy(${($_ = isset($this->services['doctrine.orm.naming_strategy.default']) ? $this->services['doctrine.orm.naming_strategy.default'] : $this->getDoctrine_Orm_NamingStrategy_DefaultService()) && false ?: '_'});
        $b->setQuoteStrategy(${($_ = isset($this->services['doctrine.orm.quote_strategy.default']) ? $this->services['doctrine.orm.quote_strategy.default'] : $this->getDoctrine_Orm_QuoteStrategy_DefaultService()) && false ?: '_'});
        $b->setEntityListenerResolver(${($_ = isset($this->services['doctrine.orm.nmega_main_entity_listener_resolver']) ? $this->services['doctrine.orm.nmega_main_entity_listener_resolver'] : $this->get('doctrine.orm.nmega_main_entity_listener_resolver')) && false ?: '_'});
        $b->addCustomNumericFunction('bitand', 'Illuzion\\Common\\Doctrine\\BitAndFunction');
        $b->addCustomNumericFunction('rupper', 'Illuzion\\Common\\Doctrine\\RupperFunction');

        $instance = \Doctrine\ORM\EntityManager::create(${($_ = isset($this->services['doctrine.dbal.nmega_main_connection']) ? $this->services['doctrine.dbal.nmega_main_connection'] : $this->get('doctrine.dbal.nmega_main_connection')) && false ?: '_'}, $b);

        ${($_ = isset($this->services['doctrine.orm.nmega_main_manager_configurator']) ? $this->services['doctrine.orm.nmega_main_manager_configurator'] : $this->get('doctrine.orm.nmega_main_manager_configurator')) && false ?: '_'}->configure($instance);

        return $instance;
    }

    /**
     * Gets the 'doctrine.orm.nmega_main_entity_manager.property_info_extractor' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Bridge\Doctrine\PropertyInfo\DoctrineExtractor A Symfony\Bridge\Doctrine\PropertyInfo\DoctrineExtractor instance
     */
    protected function getDoctrine_Orm_NmegaMainEntityManager_PropertyInfoExtractorService()
    {
        return $this->services['doctrine.orm.nmega_main_entity_manager.property_info_extractor'] = new \Symfony\Bridge\Doctrine\PropertyInfo\DoctrineExtractor(${($_ = isset($this->services['doctrine.orm.nmega_main_entity_manager']) ? $this->services['doctrine.orm.nmega_main_entity_manager'] : $this->get('doctrine.orm.nmega_main_entity_manager')) && false ?: '_'}->getMetadataFactory());
    }

    /**
     * Gets the 'doctrine.orm.nmega_main_listeners.attach_entity_listeners' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\ORM\Tools\AttachEntityListenersListener A Doctrine\ORM\Tools\AttachEntityListenersListener instance
     */
    protected function getDoctrine_Orm_NmegaMainListeners_AttachEntityListenersService()
    {
        return $this->services['doctrine.orm.nmega_main_listeners.attach_entity_listeners'] = new \Doctrine\ORM\Tools\AttachEntityListenersListener();
    }

    /**
     * Gets the 'doctrine.orm.nmega_main_manager_configurator' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Bundle\DoctrineBundle\ManagerConfigurator A Doctrine\Bundle\DoctrineBundle\ManagerConfigurator instance
     */
    protected function getDoctrine_Orm_NmegaMainManagerConfiguratorService()
    {
        return $this->services['doctrine.orm.nmega_main_manager_configurator'] = new \Doctrine\Bundle\DoctrineBundle\ManagerConfigurator(array(), array());
    }

    /**
     * Gets the 'doctrine.orm.oc_customer_entity_listener_resolver' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Bundle\DoctrineBundle\Mapping\ContainerAwareEntityListenerResolver A Doctrine\Bundle\DoctrineBundle\Mapping\ContainerAwareEntityListenerResolver instance
     */
    protected function getDoctrine_Orm_OcCustomerEntityListenerResolverService()
    {
        return $this->services['doctrine.orm.oc_customer_entity_listener_resolver'] = new \Doctrine\Bundle\DoctrineBundle\Mapping\ContainerAwareEntityListenerResolver($this);
    }

    /**
     * Gets the 'doctrine.orm.oc_customer_entity_manager' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @param bool    $lazyLoad whether to try lazy-loading the service with a proxy
     *
     * @return \Doctrine\ORM\EntityManager A Doctrine\ORM\EntityManager instance
     */
    public function getDoctrine_Orm_OcCustomerEntityManagerService($lazyLoad = true)
    {
        if ($lazyLoad) {

            return $this->services['doctrine.orm.oc_customer_entity_manager'] = new DoctrineORMEntityManager_000000002084058800000000634d02687471c441876336872902bf356897607e(
                function (&$wrappedInstance, \ProxyManager\Proxy\LazyLoadingInterface $proxy) {
                    $wrappedInstance = $this->getDoctrine_Orm_OcCustomerEntityManagerService(false);

                    $proxy->setProxyInitializer(null);

                    return true;
                }
            );
        }

        $a = new \Doctrine\Common\Persistence\Mapping\Driver\MappingDriverChain();
        $a->addDriver(new \Doctrine\ORM\Mapping\Driver\AnnotationDriver(${($_ = isset($this->services['annotation_reader']) ? $this->services['annotation_reader'] : $this->get('annotation_reader')) && false ?: '_'}, array(0 => ($this->targetDirs[3].'/src/Illuzion/ApiBundle/Entity'))), 'Illuzion\\ApiBundle\\Entity\\Customer');

        $b = new \Doctrine\ORM\Configuration();
        $b->setEntityNamespaces(array('Customer' => 'Illuzion\\ApiBundle\\Entity\\Customer'));
        $b->setMetadataCacheImpl(${($_ = isset($this->services['doctrine_cache.providers.doctrine.orm.oc_customer_metadata_cache']) ? $this->services['doctrine_cache.providers.doctrine.orm.oc_customer_metadata_cache'] : $this->get('doctrine_cache.providers.doctrine.orm.oc_customer_metadata_cache')) && false ?: '_'});
        $b->setQueryCacheImpl(${($_ = isset($this->services['doctrine_cache.providers.doctrine.orm.oc_customer_query_cache']) ? $this->services['doctrine_cache.providers.doctrine.orm.oc_customer_query_cache'] : $this->get('doctrine_cache.providers.doctrine.orm.oc_customer_query_cache')) && false ?: '_'});
        $b->setResultCacheImpl(${($_ = isset($this->services['doctrine_cache.providers.doctrine.orm.oc_customer_result_cache']) ? $this->services['doctrine_cache.providers.doctrine.orm.oc_customer_result_cache'] : $this->get('doctrine_cache.providers.doctrine.orm.oc_customer_result_cache')) && false ?: '_'});
        $b->setMetadataDriverImpl($a);
        $b->setProxyDir((__DIR__.'/doctrine/orm/Proxies'));
        $b->setProxyNamespace('Proxies');
        $b->setAutoGenerateProxyClasses(true);
        $b->setClassMetadataFactoryName('Doctrine\\ORM\\Mapping\\ClassMetadataFactory');
        $b->setDefaultRepositoryClassName('Doctrine\\ORM\\EntityRepository');
        $b->setNamingStrategy(${($_ = isset($this->services['doctrine.orm.naming_strategy.default']) ? $this->services['doctrine.orm.naming_strategy.default'] : $this->getDoctrine_Orm_NamingStrategy_DefaultService()) && false ?: '_'});
        $b->setQuoteStrategy(${($_ = isset($this->services['doctrine.orm.quote_strategy.default']) ? $this->services['doctrine.orm.quote_strategy.default'] : $this->getDoctrine_Orm_QuoteStrategy_DefaultService()) && false ?: '_'});
        $b->setEntityListenerResolver(${($_ = isset($this->services['doctrine.orm.oc_customer_entity_listener_resolver']) ? $this->services['doctrine.orm.oc_customer_entity_listener_resolver'] : $this->get('doctrine.orm.oc_customer_entity_listener_resolver')) && false ?: '_'});
        $b->addCustomNumericFunction('bitand', 'Illuzion\\Common\\Doctrine\\BitAndFunction');
        $b->addCustomNumericFunction('rupper', 'Illuzion\\Common\\Doctrine\\RupperFunction');

        $instance = \Doctrine\ORM\EntityManager::create(${($_ = isset($this->services['doctrine.dbal.oc_customer_connection']) ? $this->services['doctrine.dbal.oc_customer_connection'] : $this->get('doctrine.dbal.oc_customer_connection')) && false ?: '_'}, $b);

        ${($_ = isset($this->services['doctrine.orm.oc_customer_manager_configurator']) ? $this->services['doctrine.orm.oc_customer_manager_configurator'] : $this->get('doctrine.orm.oc_customer_manager_configurator')) && false ?: '_'}->configure($instance);

        return $instance;
    }

    /**
     * Gets the 'doctrine.orm.oc_customer_entity_manager.property_info_extractor' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Bridge\Doctrine\PropertyInfo\DoctrineExtractor A Symfony\Bridge\Doctrine\PropertyInfo\DoctrineExtractor instance
     */
    protected function getDoctrine_Orm_OcCustomerEntityManager_PropertyInfoExtractorService()
    {
        return $this->services['doctrine.orm.oc_customer_entity_manager.property_info_extractor'] = new \Symfony\Bridge\Doctrine\PropertyInfo\DoctrineExtractor(${($_ = isset($this->services['doctrine.orm.oc_customer_entity_manager']) ? $this->services['doctrine.orm.oc_customer_entity_manager'] : $this->get('doctrine.orm.oc_customer_entity_manager')) && false ?: '_'}->getMetadataFactory());
    }

    /**
     * Gets the 'doctrine.orm.oc_customer_listeners.attach_entity_listeners' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\ORM\Tools\AttachEntityListenersListener A Doctrine\ORM\Tools\AttachEntityListenersListener instance
     */
    protected function getDoctrine_Orm_OcCustomerListeners_AttachEntityListenersService()
    {
        return $this->services['doctrine.orm.oc_customer_listeners.attach_entity_listeners'] = new \Doctrine\ORM\Tools\AttachEntityListenersListener();
    }

    /**
     * Gets the 'doctrine.orm.oc_customer_manager_configurator' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Bundle\DoctrineBundle\ManagerConfigurator A Doctrine\Bundle\DoctrineBundle\ManagerConfigurator instance
     */
    protected function getDoctrine_Orm_OcCustomerManagerConfiguratorService()
    {
        return $this->services['doctrine.orm.oc_customer_manager_configurator'] = new \Doctrine\Bundle\DoctrineBundle\ManagerConfigurator(array(), array());
    }

    /**
     * Gets the 'doctrine.orm.oc_main_entity_listener_resolver' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Bundle\DoctrineBundle\Mapping\ContainerAwareEntityListenerResolver A Doctrine\Bundle\DoctrineBundle\Mapping\ContainerAwareEntityListenerResolver instance
     */
    protected function getDoctrine_Orm_OcMainEntityListenerResolverService()
    {
        return $this->services['doctrine.orm.oc_main_entity_listener_resolver'] = new \Doctrine\Bundle\DoctrineBundle\Mapping\ContainerAwareEntityListenerResolver($this);
    }

    /**
     * Gets the 'doctrine.orm.oc_main_entity_manager' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @param bool    $lazyLoad whether to try lazy-loading the service with a proxy
     *
     * @return \Doctrine\ORM\EntityManager A Doctrine\ORM\EntityManager instance
     */
    public function getDoctrine_Orm_OcMainEntityManagerService($lazyLoad = true)
    {
        if ($lazyLoad) {

            return $this->services['doctrine.orm.oc_main_entity_manager'] = new DoctrineORMEntityManager_000000002084057600000000634d02687471c441876336872902bf356897607e(
                function (&$wrappedInstance, \ProxyManager\Proxy\LazyLoadingInterface $proxy) {
                    $wrappedInstance = $this->getDoctrine_Orm_OcMainEntityManagerService(false);

                    $proxy->setProxyInitializer(null);

                    return true;
                }
            );
        }

        $a = new \Doctrine\Common\Persistence\Mapping\Driver\MappingDriverChain();
        $a->addDriver(new \Doctrine\ORM\Mapping\Driver\AnnotationDriver(${($_ = isset($this->services['annotation_reader']) ? $this->services['annotation_reader'] : $this->get('annotation_reader')) && false ?: '_'}, array(0 => ($this->targetDirs[3].'/src/Illuzion/ApiBundle/Entity'))), 'Illuzion\\ApiBundle\\Entity\\Main');

        $b = new \Doctrine\ORM\Configuration();
        $b->setEntityNamespaces(array('Main' => 'Illuzion\\ApiBundle\\Entity\\Main'));
        $b->setMetadataCacheImpl(${($_ = isset($this->services['doctrine_cache.providers.doctrine.orm.oc_main_metadata_cache']) ? $this->services['doctrine_cache.providers.doctrine.orm.oc_main_metadata_cache'] : $this->get('doctrine_cache.providers.doctrine.orm.oc_main_metadata_cache')) && false ?: '_'});
        $b->setQueryCacheImpl(${($_ = isset($this->services['doctrine_cache.providers.doctrine.orm.oc_main_query_cache']) ? $this->services['doctrine_cache.providers.doctrine.orm.oc_main_query_cache'] : $this->get('doctrine_cache.providers.doctrine.orm.oc_main_query_cache')) && false ?: '_'});
        $b->setResultCacheImpl(${($_ = isset($this->services['doctrine_cache.providers.doctrine.orm.oc_main_result_cache']) ? $this->services['doctrine_cache.providers.doctrine.orm.oc_main_result_cache'] : $this->get('doctrine_cache.providers.doctrine.orm.oc_main_result_cache')) && false ?: '_'});
        $b->setMetadataDriverImpl($a);
        $b->setProxyDir((__DIR__.'/doctrine/orm/Proxies'));
        $b->setProxyNamespace('Proxies');
        $b->setAutoGenerateProxyClasses(true);
        $b->setClassMetadataFactoryName('Doctrine\\ORM\\Mapping\\ClassMetadataFactory');
        $b->setDefaultRepositoryClassName('Doctrine\\ORM\\EntityRepository');
        $b->setNamingStrategy(${($_ = isset($this->services['doctrine.orm.naming_strategy.default']) ? $this->services['doctrine.orm.naming_strategy.default'] : $this->getDoctrine_Orm_NamingStrategy_DefaultService()) && false ?: '_'});
        $b->setQuoteStrategy(${($_ = isset($this->services['doctrine.orm.quote_strategy.default']) ? $this->services['doctrine.orm.quote_strategy.default'] : $this->getDoctrine_Orm_QuoteStrategy_DefaultService()) && false ?: '_'});
        $b->setEntityListenerResolver(${($_ = isset($this->services['doctrine.orm.oc_main_entity_listener_resolver']) ? $this->services['doctrine.orm.oc_main_entity_listener_resolver'] : $this->get('doctrine.orm.oc_main_entity_listener_resolver')) && false ?: '_'});
        $b->addCustomNumericFunction('bitand', 'Illuzion\\Common\\Doctrine\\BitAndFunction');
        $b->addCustomNumericFunction('rupper', 'Illuzion\\Common\\Doctrine\\RupperFunction');

        $instance = \Doctrine\ORM\EntityManager::create(${($_ = isset($this->services['doctrine.dbal.oc_main_connection']) ? $this->services['doctrine.dbal.oc_main_connection'] : $this->get('doctrine.dbal.oc_main_connection')) && false ?: '_'}, $b);

        ${($_ = isset($this->services['doctrine.orm.oc_main_manager_configurator']) ? $this->services['doctrine.orm.oc_main_manager_configurator'] : $this->get('doctrine.orm.oc_main_manager_configurator')) && false ?: '_'}->configure($instance);

        return $instance;
    }

    /**
     * Gets the 'doctrine.orm.oc_main_entity_manager.property_info_extractor' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Bridge\Doctrine\PropertyInfo\DoctrineExtractor A Symfony\Bridge\Doctrine\PropertyInfo\DoctrineExtractor instance
     */
    protected function getDoctrine_Orm_OcMainEntityManager_PropertyInfoExtractorService()
    {
        return $this->services['doctrine.orm.oc_main_entity_manager.property_info_extractor'] = new \Symfony\Bridge\Doctrine\PropertyInfo\DoctrineExtractor(${($_ = isset($this->services['doctrine.orm.oc_main_entity_manager']) ? $this->services['doctrine.orm.oc_main_entity_manager'] : $this->get('doctrine.orm.oc_main_entity_manager')) && false ?: '_'}->getMetadataFactory());
    }

    /**
     * Gets the 'doctrine.orm.oc_main_listeners.attach_entity_listeners' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\ORM\Tools\AttachEntityListenersListener A Doctrine\ORM\Tools\AttachEntityListenersListener instance
     */
    protected function getDoctrine_Orm_OcMainListeners_AttachEntityListenersService()
    {
        return $this->services['doctrine.orm.oc_main_listeners.attach_entity_listeners'] = new \Doctrine\ORM\Tools\AttachEntityListenersListener();
    }

    /**
     * Gets the 'doctrine.orm.oc_main_manager_configurator' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Bundle\DoctrineBundle\ManagerConfigurator A Doctrine\Bundle\DoctrineBundle\ManagerConfigurator instance
     */
    protected function getDoctrine_Orm_OcMainManagerConfiguratorService()
    {
        return $this->services['doctrine.orm.oc_main_manager_configurator'] = new \Doctrine\Bundle\DoctrineBundle\ManagerConfigurator(array(), array());
    }

    /**
     * Gets the 'doctrine.orm.shared_entity_listener_resolver' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Bundle\DoctrineBundle\Mapping\ContainerAwareEntityListenerResolver A Doctrine\Bundle\DoctrineBundle\Mapping\ContainerAwareEntityListenerResolver instance
     */
    protected function getDoctrine_Orm_SharedEntityListenerResolverService()
    {
        return $this->services['doctrine.orm.shared_entity_listener_resolver'] = new \Doctrine\Bundle\DoctrineBundle\Mapping\ContainerAwareEntityListenerResolver($this);
    }

    /**
     * Gets the 'doctrine.orm.shared_entity_manager' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @param bool    $lazyLoad whether to try lazy-loading the service with a proxy
     *
     * @return \Doctrine\ORM\EntityManager A Doctrine\ORM\EntityManager instance
     */
    public function getDoctrine_Orm_SharedEntityManagerService($lazyLoad = true)
    {
        if ($lazyLoad) {

            return $this->services['doctrine.orm.shared_entity_manager'] = new DoctrineORMEntityManager_000000002084056000000000634d02687471c441876336872902bf356897607e(
                function (&$wrappedInstance, \ProxyManager\Proxy\LazyLoadingInterface $proxy) {
                    $wrappedInstance = $this->getDoctrine_Orm_SharedEntityManagerService(false);

                    $proxy->setProxyInitializer(null);

                    return true;
                }
            );
        }

        $a = ${($_ = isset($this->services['annotation_reader']) ? $this->services['annotation_reader'] : $this->get('annotation_reader')) && false ?: '_'};

        $b = new \Doctrine\ORM\Mapping\Driver\AnnotationDriver($a, array(0 => ($this->targetDirs[3].'/src/Illuzion/ApiBundle/Entity'), 1 => ($this->targetDirs[3].'/src/Illuzion/SynchronizationBundle/Entity')));

        $c = new \Doctrine\ORM\Mapping\Driver\SimplifiedXmlDriver(array(($this->targetDirs[3].'/vendor/sonata-project/notification-bundle/Resources/config/doctrine') => 'Sonata\\NotificationBundle\\Entity'));
        $c->setGlobalBasename('mapping');

        $d = new \Doctrine\Common\Persistence\Mapping\Driver\MappingDriverChain();
        $d->addDriver($b, 'Illuzion\\ApiBundle\\Entity\\Shared');
        $d->addDriver($b, 'Illuzion\\SynchronizationBundle\\Entity');
        $d->addDriver($c, 'Sonata\\NotificationBundle\\Entity');

        $e = new \Doctrine\ORM\Configuration();
        $e->setEntityNamespaces(array('Shared' => 'Illuzion\\ApiBundle\\Entity\\Shared', 'SonataNotificationBundle' => 'Sonata\\NotificationBundle\\Entity', 'SynchronizationBundle' => 'Illuzion\\SynchronizationBundle\\Entity'));
        $e->setMetadataCacheImpl(${($_ = isset($this->services['doctrine_cache.providers.doctrine.orm.shared_metadata_cache']) ? $this->services['doctrine_cache.providers.doctrine.orm.shared_metadata_cache'] : $this->get('doctrine_cache.providers.doctrine.orm.shared_metadata_cache')) && false ?: '_'});
        $e->setQueryCacheImpl(${($_ = isset($this->services['doctrine_cache.providers.doctrine.orm.shared_query_cache']) ? $this->services['doctrine_cache.providers.doctrine.orm.shared_query_cache'] : $this->get('doctrine_cache.providers.doctrine.orm.shared_query_cache')) && false ?: '_'});
        $e->setResultCacheImpl(${($_ = isset($this->services['doctrine_cache.providers.doctrine.orm.shared_result_cache']) ? $this->services['doctrine_cache.providers.doctrine.orm.shared_result_cache'] : $this->get('doctrine_cache.providers.doctrine.orm.shared_result_cache')) && false ?: '_'});
        $e->setMetadataDriverImpl($d);
        $e->setProxyDir((__DIR__.'/doctrine/orm/Proxies'));
        $e->setProxyNamespace('Proxies');
        $e->setAutoGenerateProxyClasses(true);
        $e->setClassMetadataFactoryName('Doctrine\\ORM\\Mapping\\ClassMetadataFactory');
        $e->setDefaultRepositoryClassName('Doctrine\\ORM\\EntityRepository');
        $e->setNamingStrategy(new \Doctrine\ORM\Mapping\UnderscoreNamingStrategy());
        $e->setQuoteStrategy(${($_ = isset($this->services['doctrine.orm.quote_strategy.default']) ? $this->services['doctrine.orm.quote_strategy.default'] : $this->getDoctrine_Orm_QuoteStrategy_DefaultService()) && false ?: '_'});
        $e->setEntityListenerResolver(${($_ = isset($this->services['doctrine.orm.shared_entity_listener_resolver']) ? $this->services['doctrine.orm.shared_entity_listener_resolver'] : $this->get('doctrine.orm.shared_entity_listener_resolver')) && false ?: '_'});

        $instance = \Doctrine\ORM\EntityManager::create(${($_ = isset($this->services['doctrine.dbal.shared_connection']) ? $this->services['doctrine.dbal.shared_connection'] : $this->get('doctrine.dbal.shared_connection')) && false ?: '_'}, $e);

        ${($_ = isset($this->services['doctrine.orm.shared_manager_configurator']) ? $this->services['doctrine.orm.shared_manager_configurator'] : $this->get('doctrine.orm.shared_manager_configurator')) && false ?: '_'}->configure($instance);

        return $instance;
    }

    /**
     * Gets the 'doctrine.orm.shared_entity_manager.property_info_extractor' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Bridge\Doctrine\PropertyInfo\DoctrineExtractor A Symfony\Bridge\Doctrine\PropertyInfo\DoctrineExtractor instance
     */
    protected function getDoctrine_Orm_SharedEntityManager_PropertyInfoExtractorService()
    {
        return $this->services['doctrine.orm.shared_entity_manager.property_info_extractor'] = new \Symfony\Bridge\Doctrine\PropertyInfo\DoctrineExtractor(${($_ = isset($this->services['doctrine.orm.shared_entity_manager']) ? $this->services['doctrine.orm.shared_entity_manager'] : $this->get('doctrine.orm.shared_entity_manager')) && false ?: '_'}->getMetadataFactory());
    }

    /**
     * Gets the 'doctrine.orm.shared_listeners.attach_entity_listeners' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\ORM\Tools\AttachEntityListenersListener A Doctrine\ORM\Tools\AttachEntityListenersListener instance
     */
    protected function getDoctrine_Orm_SharedListeners_AttachEntityListenersService()
    {
        return $this->services['doctrine.orm.shared_listeners.attach_entity_listeners'] = new \Doctrine\ORM\Tools\AttachEntityListenersListener();
    }

    /**
     * Gets the 'doctrine.orm.shared_manager_configurator' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Bundle\DoctrineBundle\ManagerConfigurator A Doctrine\Bundle\DoctrineBundle\ManagerConfigurator instance
     */
    protected function getDoctrine_Orm_SharedManagerConfiguratorService()
    {
        return $this->services['doctrine.orm.shared_manager_configurator'] = new \Doctrine\Bundle\DoctrineBundle\ManagerConfigurator(array(), array());
    }

    /**
     * Gets the 'doctrine.orm.uss_customer_entity_listener_resolver' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Bundle\DoctrineBundle\Mapping\ContainerAwareEntityListenerResolver A Doctrine\Bundle\DoctrineBundle\Mapping\ContainerAwareEntityListenerResolver instance
     */
    protected function getDoctrine_Orm_UssCustomerEntityListenerResolverService()
    {
        return $this->services['doctrine.orm.uss_customer_entity_listener_resolver'] = new \Doctrine\Bundle\DoctrineBundle\Mapping\ContainerAwareEntityListenerResolver($this);
    }

    /**
     * Gets the 'doctrine.orm.uss_customer_entity_manager' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @param bool    $lazyLoad whether to try lazy-loading the service with a proxy
     *
     * @return \Doctrine\ORM\EntityManager A Doctrine\ORM\EntityManager instance
     */
    public function getDoctrine_Orm_UssCustomerEntityManagerService($lazyLoad = true)
    {
        if ($lazyLoad) {

            return $this->services['doctrine.orm.uss_customer_entity_manager'] = new DoctrineORMEntityManager_000000002084057c00000000634d02687471c441876336872902bf356897607e(
                function (&$wrappedInstance, \ProxyManager\Proxy\LazyLoadingInterface $proxy) {
                    $wrappedInstance = $this->getDoctrine_Orm_UssCustomerEntityManagerService(false);

                    $proxy->setProxyInitializer(null);

                    return true;
                }
            );
        }

        $a = new \Doctrine\Common\Persistence\Mapping\Driver\MappingDriverChain();
        $a->addDriver(new \Doctrine\ORM\Mapping\Driver\AnnotationDriver(${($_ = isset($this->services['annotation_reader']) ? $this->services['annotation_reader'] : $this->get('annotation_reader')) && false ?: '_'}, array(0 => ($this->targetDirs[3].'/src/Illuzion/ApiBundle/Entity'))), 'Illuzion\\ApiBundle\\Entity\\Customer');

        $b = new \Doctrine\ORM\Configuration();
        $b->setEntityNamespaces(array('Customer' => 'Illuzion\\ApiBundle\\Entity\\Customer'));
        $b->setMetadataCacheImpl(${($_ = isset($this->services['doctrine_cache.providers.doctrine.orm.uss_customer_metadata_cache']) ? $this->services['doctrine_cache.providers.doctrine.orm.uss_customer_metadata_cache'] : $this->get('doctrine_cache.providers.doctrine.orm.uss_customer_metadata_cache')) && false ?: '_'});
        $b->setQueryCacheImpl(${($_ = isset($this->services['doctrine_cache.providers.doctrine.orm.uss_customer_query_cache']) ? $this->services['doctrine_cache.providers.doctrine.orm.uss_customer_query_cache'] : $this->get('doctrine_cache.providers.doctrine.orm.uss_customer_query_cache')) && false ?: '_'});
        $b->setResultCacheImpl(${($_ = isset($this->services['doctrine_cache.providers.doctrine.orm.uss_customer_result_cache']) ? $this->services['doctrine_cache.providers.doctrine.orm.uss_customer_result_cache'] : $this->get('doctrine_cache.providers.doctrine.orm.uss_customer_result_cache')) && false ?: '_'});
        $b->setMetadataDriverImpl($a);
        $b->setProxyDir((__DIR__.'/doctrine/orm/Proxies'));
        $b->setProxyNamespace('Proxies');
        $b->setAutoGenerateProxyClasses(true);
        $b->setClassMetadataFactoryName('Doctrine\\ORM\\Mapping\\ClassMetadataFactory');
        $b->setDefaultRepositoryClassName('Doctrine\\ORM\\EntityRepository');
        $b->setNamingStrategy(${($_ = isset($this->services['doctrine.orm.naming_strategy.default']) ? $this->services['doctrine.orm.naming_strategy.default'] : $this->getDoctrine_Orm_NamingStrategy_DefaultService()) && false ?: '_'});
        $b->setQuoteStrategy(${($_ = isset($this->services['doctrine.orm.quote_strategy.default']) ? $this->services['doctrine.orm.quote_strategy.default'] : $this->getDoctrine_Orm_QuoteStrategy_DefaultService()) && false ?: '_'});
        $b->setEntityListenerResolver(${($_ = isset($this->services['doctrine.orm.uss_customer_entity_listener_resolver']) ? $this->services['doctrine.orm.uss_customer_entity_listener_resolver'] : $this->get('doctrine.orm.uss_customer_entity_listener_resolver')) && false ?: '_'});
        $b->addCustomNumericFunction('bitand', 'Illuzion\\Common\\Doctrine\\BitAndFunction');
        $b->addCustomNumericFunction('rupper', 'Illuzion\\Common\\Doctrine\\RupperFunction');

        $instance = \Doctrine\ORM\EntityManager::create(${($_ = isset($this->services['doctrine.dbal.uss_customer_connection']) ? $this->services['doctrine.dbal.uss_customer_connection'] : $this->get('doctrine.dbal.uss_customer_connection')) && false ?: '_'}, $b);

        ${($_ = isset($this->services['doctrine.orm.uss_customer_manager_configurator']) ? $this->services['doctrine.orm.uss_customer_manager_configurator'] : $this->get('doctrine.orm.uss_customer_manager_configurator')) && false ?: '_'}->configure($instance);

        return $instance;
    }

    /**
     * Gets the 'doctrine.orm.uss_customer_entity_manager.property_info_extractor' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Bridge\Doctrine\PropertyInfo\DoctrineExtractor A Symfony\Bridge\Doctrine\PropertyInfo\DoctrineExtractor instance
     */
    protected function getDoctrine_Orm_UssCustomerEntityManager_PropertyInfoExtractorService()
    {
        return $this->services['doctrine.orm.uss_customer_entity_manager.property_info_extractor'] = new \Symfony\Bridge\Doctrine\PropertyInfo\DoctrineExtractor(${($_ = isset($this->services['doctrine.orm.uss_customer_entity_manager']) ? $this->services['doctrine.orm.uss_customer_entity_manager'] : $this->get('doctrine.orm.uss_customer_entity_manager')) && false ?: '_'}->getMetadataFactory());
    }

    /**
     * Gets the 'doctrine.orm.uss_customer_listeners.attach_entity_listeners' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\ORM\Tools\AttachEntityListenersListener A Doctrine\ORM\Tools\AttachEntityListenersListener instance
     */
    protected function getDoctrine_Orm_UssCustomerListeners_AttachEntityListenersService()
    {
        return $this->services['doctrine.orm.uss_customer_listeners.attach_entity_listeners'] = new \Doctrine\ORM\Tools\AttachEntityListenersListener();
    }

    /**
     * Gets the 'doctrine.orm.uss_customer_manager_configurator' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Bundle\DoctrineBundle\ManagerConfigurator A Doctrine\Bundle\DoctrineBundle\ManagerConfigurator instance
     */
    protected function getDoctrine_Orm_UssCustomerManagerConfiguratorService()
    {
        return $this->services['doctrine.orm.uss_customer_manager_configurator'] = new \Doctrine\Bundle\DoctrineBundle\ManagerConfigurator(array(), array());
    }

    /**
     * Gets the 'doctrine.orm.uss_main_entity_listener_resolver' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Bundle\DoctrineBundle\Mapping\ContainerAwareEntityListenerResolver A Doctrine\Bundle\DoctrineBundle\Mapping\ContainerAwareEntityListenerResolver instance
     */
    protected function getDoctrine_Orm_UssMainEntityListenerResolverService()
    {
        return $this->services['doctrine.orm.uss_main_entity_listener_resolver'] = new \Doctrine\Bundle\DoctrineBundle\Mapping\ContainerAwareEntityListenerResolver($this);
    }

    /**
     * Gets the 'doctrine.orm.uss_main_entity_manager' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @param bool    $lazyLoad whether to try lazy-loading the service with a proxy
     *
     * @return \Doctrine\ORM\EntityManager A Doctrine\ORM\EntityManager instance
     */
    public function getDoctrine_Orm_UssMainEntityManagerService($lazyLoad = true)
    {
        if ($lazyLoad) {

            return $this->services['doctrine.orm.uss_main_entity_manager'] = new DoctrineORMEntityManager_000000002084057a00000000634d02687471c441876336872902bf356897607e(
                function (&$wrappedInstance, \ProxyManager\Proxy\LazyLoadingInterface $proxy) {
                    $wrappedInstance = $this->getDoctrine_Orm_UssMainEntityManagerService(false);

                    $proxy->setProxyInitializer(null);

                    return true;
                }
            );
        }

        $a = new \Doctrine\Common\Persistence\Mapping\Driver\MappingDriverChain();
        $a->addDriver(new \Doctrine\ORM\Mapping\Driver\AnnotationDriver(${($_ = isset($this->services['annotation_reader']) ? $this->services['annotation_reader'] : $this->get('annotation_reader')) && false ?: '_'}, array(0 => ($this->targetDirs[3].'/src/Illuzion/ApiBundle/Entity'))), 'Illuzion\\ApiBundle\\Entity\\Main');

        $b = new \Doctrine\ORM\Configuration();
        $b->setEntityNamespaces(array('Main' => 'Illuzion\\ApiBundle\\Entity\\Main'));
        $b->setMetadataCacheImpl(${($_ = isset($this->services['doctrine_cache.providers.doctrine.orm.uss_main_metadata_cache']) ? $this->services['doctrine_cache.providers.doctrine.orm.uss_main_metadata_cache'] : $this->get('doctrine_cache.providers.doctrine.orm.uss_main_metadata_cache')) && false ?: '_'});
        $b->setQueryCacheImpl(${($_ = isset($this->services['doctrine_cache.providers.doctrine.orm.uss_main_query_cache']) ? $this->services['doctrine_cache.providers.doctrine.orm.uss_main_query_cache'] : $this->get('doctrine_cache.providers.doctrine.orm.uss_main_query_cache')) && false ?: '_'});
        $b->setResultCacheImpl(${($_ = isset($this->services['doctrine_cache.providers.doctrine.orm.uss_main_result_cache']) ? $this->services['doctrine_cache.providers.doctrine.orm.uss_main_result_cache'] : $this->get('doctrine_cache.providers.doctrine.orm.uss_main_result_cache')) && false ?: '_'});
        $b->setMetadataDriverImpl($a);
        $b->setProxyDir((__DIR__.'/doctrine/orm/Proxies'));
        $b->setProxyNamespace('Proxies');
        $b->setAutoGenerateProxyClasses(true);
        $b->setClassMetadataFactoryName('Doctrine\\ORM\\Mapping\\ClassMetadataFactory');
        $b->setDefaultRepositoryClassName('Doctrine\\ORM\\EntityRepository');
        $b->setNamingStrategy(${($_ = isset($this->services['doctrine.orm.naming_strategy.default']) ? $this->services['doctrine.orm.naming_strategy.default'] : $this->getDoctrine_Orm_NamingStrategy_DefaultService()) && false ?: '_'});
        $b->setQuoteStrategy(${($_ = isset($this->services['doctrine.orm.quote_strategy.default']) ? $this->services['doctrine.orm.quote_strategy.default'] : $this->getDoctrine_Orm_QuoteStrategy_DefaultService()) && false ?: '_'});
        $b->setEntityListenerResolver(${($_ = isset($this->services['doctrine.orm.uss_main_entity_listener_resolver']) ? $this->services['doctrine.orm.uss_main_entity_listener_resolver'] : $this->get('doctrine.orm.uss_main_entity_listener_resolver')) && false ?: '_'});
        $b->addCustomNumericFunction('bitand', 'Illuzion\\Common\\Doctrine\\BitAndFunction');
        $b->addCustomNumericFunction('rupper', 'Illuzion\\Common\\Doctrine\\RupperFunction');

        $instance = \Doctrine\ORM\EntityManager::create(${($_ = isset($this->services['doctrine.dbal.uss_main_connection']) ? $this->services['doctrine.dbal.uss_main_connection'] : $this->get('doctrine.dbal.uss_main_connection')) && false ?: '_'}, $b);

        ${($_ = isset($this->services['doctrine.orm.uss_main_manager_configurator']) ? $this->services['doctrine.orm.uss_main_manager_configurator'] : $this->get('doctrine.orm.uss_main_manager_configurator')) && false ?: '_'}->configure($instance);

        return $instance;
    }

    /**
     * Gets the 'doctrine.orm.uss_main_entity_manager.property_info_extractor' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Bridge\Doctrine\PropertyInfo\DoctrineExtractor A Symfony\Bridge\Doctrine\PropertyInfo\DoctrineExtractor instance
     */
    protected function getDoctrine_Orm_UssMainEntityManager_PropertyInfoExtractorService()
    {
        return $this->services['doctrine.orm.uss_main_entity_manager.property_info_extractor'] = new \Symfony\Bridge\Doctrine\PropertyInfo\DoctrineExtractor(${($_ = isset($this->services['doctrine.orm.uss_main_entity_manager']) ? $this->services['doctrine.orm.uss_main_entity_manager'] : $this->get('doctrine.orm.uss_main_entity_manager')) && false ?: '_'}->getMetadataFactory());
    }

    /**
     * Gets the 'doctrine.orm.uss_main_listeners.attach_entity_listeners' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\ORM\Tools\AttachEntityListenersListener A Doctrine\ORM\Tools\AttachEntityListenersListener instance
     */
    protected function getDoctrine_Orm_UssMainListeners_AttachEntityListenersService()
    {
        return $this->services['doctrine.orm.uss_main_listeners.attach_entity_listeners'] = new \Doctrine\ORM\Tools\AttachEntityListenersListener();
    }

    /**
     * Gets the 'doctrine.orm.uss_main_manager_configurator' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Bundle\DoctrineBundle\ManagerConfigurator A Doctrine\Bundle\DoctrineBundle\ManagerConfigurator instance
     */
    protected function getDoctrine_Orm_UssMainManagerConfiguratorService()
    {
        return $this->services['doctrine.orm.uss_main_manager_configurator'] = new \Doctrine\Bundle\DoctrineBundle\ManagerConfigurator(array(), array());
    }

    /**
     * Gets the 'doctrine.orm.validator.unique' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntityValidator A Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntityValidator instance
     */
    protected function getDoctrine_Orm_Validator_UniqueService()
    {
        return $this->services['doctrine.orm.validator.unique'] = new \Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntityValidator(${($_ = isset($this->services['doctrine']) ? $this->services['doctrine'] : $this->get('doctrine')) && false ?: '_'});
    }

    /**
     * Gets the 'doctrine.orm.validator_initializer' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Bridge\Doctrine\Validator\DoctrineInitializer A Symfony\Bridge\Doctrine\Validator\DoctrineInitializer instance
     */
    protected function getDoctrine_Orm_ValidatorInitializerService()
    {
        return $this->services['doctrine.orm.validator_initializer'] = new \Symfony\Bridge\Doctrine\Validator\DoctrineInitializer(${($_ = isset($this->services['doctrine']) ? $this->services['doctrine'] : $this->get('doctrine')) && false ?: '_'});
    }

    /**
     * Gets the 'doctrine_cache.providers.doctrine.orm.che_customer_metadata_cache' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Common\Cache\ArrayCache A Doctrine\Common\Cache\ArrayCache instance
     */
    protected function getDoctrineCache_Providers_Doctrine_Orm_CheCustomerMetadataCacheService()
    {
        $this->services['doctrine_cache.providers.doctrine.orm.che_customer_metadata_cache'] = $instance = new \Doctrine\Common\Cache\ArrayCache();

        $instance->setNamespace('sf2orm_che_customer_1d80ffce729c3770350641b6aba39da08a13de62240f7ed985fc0377eaa53fdc');

        return $instance;
    }

    /**
     * Gets the 'doctrine_cache.providers.doctrine.orm.che_customer_query_cache' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Common\Cache\VoidCache A Doctrine\Common\Cache\VoidCache instance
     */
    protected function getDoctrineCache_Providers_Doctrine_Orm_CheCustomerQueryCacheService()
    {
        $this->services['doctrine_cache.providers.doctrine.orm.che_customer_query_cache'] = $instance = new \Doctrine\Common\Cache\VoidCache();

        $instance->setNamespace('sf2orm_che_customer_1d80ffce729c3770350641b6aba39da08a13de62240f7ed985fc0377eaa53fdc');

        return $instance;
    }

    /**
     * Gets the 'doctrine_cache.providers.doctrine.orm.che_customer_result_cache' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Common\Cache\VoidCache A Doctrine\Common\Cache\VoidCache instance
     */
    protected function getDoctrineCache_Providers_Doctrine_Orm_CheCustomerResultCacheService()
    {
        $this->services['doctrine_cache.providers.doctrine.orm.che_customer_result_cache'] = $instance = new \Doctrine\Common\Cache\VoidCache();

        $instance->setNamespace('sf2orm_che_customer_1d80ffce729c3770350641b6aba39da08a13de62240f7ed985fc0377eaa53fdc');

        return $instance;
    }

    /**
     * Gets the 'doctrine_cache.providers.doctrine.orm.che_main_metadata_cache' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Common\Cache\ArrayCache A Doctrine\Common\Cache\ArrayCache instance
     */
    protected function getDoctrineCache_Providers_Doctrine_Orm_CheMainMetadataCacheService()
    {
        $this->services['doctrine_cache.providers.doctrine.orm.che_main_metadata_cache'] = $instance = new \Doctrine\Common\Cache\ArrayCache();

        $instance->setNamespace('sf2orm_che_main_1d80ffce729c3770350641b6aba39da08a13de62240f7ed985fc0377eaa53fdc');

        return $instance;
    }

    /**
     * Gets the 'doctrine_cache.providers.doctrine.orm.che_main_query_cache' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Common\Cache\VoidCache A Doctrine\Common\Cache\VoidCache instance
     */
    protected function getDoctrineCache_Providers_Doctrine_Orm_CheMainQueryCacheService()
    {
        $this->services['doctrine_cache.providers.doctrine.orm.che_main_query_cache'] = $instance = new \Doctrine\Common\Cache\VoidCache();

        $instance->setNamespace('sf2orm_che_main_1d80ffce729c3770350641b6aba39da08a13de62240f7ed985fc0377eaa53fdc');

        return $instance;
    }

    /**
     * Gets the 'doctrine_cache.providers.doctrine.orm.che_main_result_cache' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Common\Cache\VoidCache A Doctrine\Common\Cache\VoidCache instance
     */
    protected function getDoctrineCache_Providers_Doctrine_Orm_CheMainResultCacheService()
    {
        $this->services['doctrine_cache.providers.doctrine.orm.che_main_result_cache'] = $instance = new \Doctrine\Common\Cache\VoidCache();

        $instance->setNamespace('sf2orm_che_main_1d80ffce729c3770350641b6aba39da08a13de62240f7ed985fc0377eaa53fdc');

        return $instance;
    }

    /**
     * Gets the 'doctrine_cache.providers.doctrine.orm.ill_customer_metadata_cache' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Common\Cache\ArrayCache A Doctrine\Common\Cache\ArrayCache instance
     */
    protected function getDoctrineCache_Providers_Doctrine_Orm_IllCustomerMetadataCacheService()
    {
        $this->services['doctrine_cache.providers.doctrine.orm.ill_customer_metadata_cache'] = $instance = new \Doctrine\Common\Cache\ArrayCache();

        $instance->setNamespace('sf2orm_ill_customer_1d80ffce729c3770350641b6aba39da08a13de62240f7ed985fc0377eaa53fdc');

        return $instance;
    }

    /**
     * Gets the 'doctrine_cache.providers.doctrine.orm.ill_customer_query_cache' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Common\Cache\VoidCache A Doctrine\Common\Cache\VoidCache instance
     */
    protected function getDoctrineCache_Providers_Doctrine_Orm_IllCustomerQueryCacheService()
    {
        $this->services['doctrine_cache.providers.doctrine.orm.ill_customer_query_cache'] = $instance = new \Doctrine\Common\Cache\VoidCache();

        $instance->setNamespace('sf2orm_ill_customer_1d80ffce729c3770350641b6aba39da08a13de62240f7ed985fc0377eaa53fdc');

        return $instance;
    }

    /**
     * Gets the 'doctrine_cache.providers.doctrine.orm.ill_customer_result_cache' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Common\Cache\VoidCache A Doctrine\Common\Cache\VoidCache instance
     */
    protected function getDoctrineCache_Providers_Doctrine_Orm_IllCustomerResultCacheService()
    {
        $this->services['doctrine_cache.providers.doctrine.orm.ill_customer_result_cache'] = $instance = new \Doctrine\Common\Cache\VoidCache();

        $instance->setNamespace('sf2orm_ill_customer_1d80ffce729c3770350641b6aba39da08a13de62240f7ed985fc0377eaa53fdc');

        return $instance;
    }

    /**
     * Gets the 'doctrine_cache.providers.doctrine.orm.ill_main_metadata_cache' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Common\Cache\ArrayCache A Doctrine\Common\Cache\ArrayCache instance
     */
    protected function getDoctrineCache_Providers_Doctrine_Orm_IllMainMetadataCacheService()
    {
        $this->services['doctrine_cache.providers.doctrine.orm.ill_main_metadata_cache'] = $instance = new \Doctrine\Common\Cache\ArrayCache();

        $instance->setNamespace('sf2orm_ill_main_1d80ffce729c3770350641b6aba39da08a13de62240f7ed985fc0377eaa53fdc');

        return $instance;
    }

    /**
     * Gets the 'doctrine_cache.providers.doctrine.orm.ill_main_query_cache' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Common\Cache\VoidCache A Doctrine\Common\Cache\VoidCache instance
     */
    protected function getDoctrineCache_Providers_Doctrine_Orm_IllMainQueryCacheService()
    {
        $this->services['doctrine_cache.providers.doctrine.orm.ill_main_query_cache'] = $instance = new \Doctrine\Common\Cache\VoidCache();

        $instance->setNamespace('sf2orm_ill_main_1d80ffce729c3770350641b6aba39da08a13de62240f7ed985fc0377eaa53fdc');

        return $instance;
    }

    /**
     * Gets the 'doctrine_cache.providers.doctrine.orm.ill_main_result_cache' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Common\Cache\VoidCache A Doctrine\Common\Cache\VoidCache instance
     */
    protected function getDoctrineCache_Providers_Doctrine_Orm_IllMainResultCacheService()
    {
        $this->services['doctrine_cache.providers.doctrine.orm.ill_main_result_cache'] = $instance = new \Doctrine\Common\Cache\VoidCache();

        $instance->setNamespace('sf2orm_ill_main_1d80ffce729c3770350641b6aba39da08a13de62240f7ed985fc0377eaa53fdc');

        return $instance;
    }

    /**
     * Gets the 'doctrine_cache.providers.doctrine.orm.nmega_customer_metadata_cache' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Common\Cache\ArrayCache A Doctrine\Common\Cache\ArrayCache instance
     */
    protected function getDoctrineCache_Providers_Doctrine_Orm_NmegaCustomerMetadataCacheService()
    {
        $this->services['doctrine_cache.providers.doctrine.orm.nmega_customer_metadata_cache'] = $instance = new \Doctrine\Common\Cache\ArrayCache();

        $instance->setNamespace('sf2orm_nmega_customer_1d80ffce729c3770350641b6aba39da08a13de62240f7ed985fc0377eaa53fdc');

        return $instance;
    }

    /**
     * Gets the 'doctrine_cache.providers.doctrine.orm.nmega_customer_query_cache' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Common\Cache\VoidCache A Doctrine\Common\Cache\VoidCache instance
     */
    protected function getDoctrineCache_Providers_Doctrine_Orm_NmegaCustomerQueryCacheService()
    {
        $this->services['doctrine_cache.providers.doctrine.orm.nmega_customer_query_cache'] = $instance = new \Doctrine\Common\Cache\VoidCache();

        $instance->setNamespace('sf2orm_nmega_customer_1d80ffce729c3770350641b6aba39da08a13de62240f7ed985fc0377eaa53fdc');

        return $instance;
    }

    /**
     * Gets the 'doctrine_cache.providers.doctrine.orm.nmega_customer_result_cache' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Common\Cache\VoidCache A Doctrine\Common\Cache\VoidCache instance
     */
    protected function getDoctrineCache_Providers_Doctrine_Orm_NmegaCustomerResultCacheService()
    {
        $this->services['doctrine_cache.providers.doctrine.orm.nmega_customer_result_cache'] = $instance = new \Doctrine\Common\Cache\VoidCache();

        $instance->setNamespace('sf2orm_nmega_customer_1d80ffce729c3770350641b6aba39da08a13de62240f7ed985fc0377eaa53fdc');

        return $instance;
    }

    /**
     * Gets the 'doctrine_cache.providers.doctrine.orm.nmega_main_metadata_cache' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Common\Cache\ArrayCache A Doctrine\Common\Cache\ArrayCache instance
     */
    protected function getDoctrineCache_Providers_Doctrine_Orm_NmegaMainMetadataCacheService()
    {
        $this->services['doctrine_cache.providers.doctrine.orm.nmega_main_metadata_cache'] = $instance = new \Doctrine\Common\Cache\ArrayCache();

        $instance->setNamespace('sf2orm_nmega_main_1d80ffce729c3770350641b6aba39da08a13de62240f7ed985fc0377eaa53fdc');

        return $instance;
    }

    /**
     * Gets the 'doctrine_cache.providers.doctrine.orm.nmega_main_query_cache' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Common\Cache\VoidCache A Doctrine\Common\Cache\VoidCache instance
     */
    protected function getDoctrineCache_Providers_Doctrine_Orm_NmegaMainQueryCacheService()
    {
        $this->services['doctrine_cache.providers.doctrine.orm.nmega_main_query_cache'] = $instance = new \Doctrine\Common\Cache\VoidCache();

        $instance->setNamespace('sf2orm_nmega_main_1d80ffce729c3770350641b6aba39da08a13de62240f7ed985fc0377eaa53fdc');

        return $instance;
    }

    /**
     * Gets the 'doctrine_cache.providers.doctrine.orm.nmega_main_result_cache' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Common\Cache\VoidCache A Doctrine\Common\Cache\VoidCache instance
     */
    protected function getDoctrineCache_Providers_Doctrine_Orm_NmegaMainResultCacheService()
    {
        $this->services['doctrine_cache.providers.doctrine.orm.nmega_main_result_cache'] = $instance = new \Doctrine\Common\Cache\VoidCache();

        $instance->setNamespace('sf2orm_nmega_main_1d80ffce729c3770350641b6aba39da08a13de62240f7ed985fc0377eaa53fdc');

        return $instance;
    }

    /**
     * Gets the 'doctrine_cache.providers.doctrine.orm.oc_customer_metadata_cache' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Common\Cache\ArrayCache A Doctrine\Common\Cache\ArrayCache instance
     */
    protected function getDoctrineCache_Providers_Doctrine_Orm_OcCustomerMetadataCacheService()
    {
        $this->services['doctrine_cache.providers.doctrine.orm.oc_customer_metadata_cache'] = $instance = new \Doctrine\Common\Cache\ArrayCache();

        $instance->setNamespace('sf2orm_oc_customer_1d80ffce729c3770350641b6aba39da08a13de62240f7ed985fc0377eaa53fdc');

        return $instance;
    }

    /**
     * Gets the 'doctrine_cache.providers.doctrine.orm.oc_customer_query_cache' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Common\Cache\VoidCache A Doctrine\Common\Cache\VoidCache instance
     */
    protected function getDoctrineCache_Providers_Doctrine_Orm_OcCustomerQueryCacheService()
    {
        $this->services['doctrine_cache.providers.doctrine.orm.oc_customer_query_cache'] = $instance = new \Doctrine\Common\Cache\VoidCache();

        $instance->setNamespace('sf2orm_oc_customer_1d80ffce729c3770350641b6aba39da08a13de62240f7ed985fc0377eaa53fdc');

        return $instance;
    }

    /**
     * Gets the 'doctrine_cache.providers.doctrine.orm.oc_customer_result_cache' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Common\Cache\VoidCache A Doctrine\Common\Cache\VoidCache instance
     */
    protected function getDoctrineCache_Providers_Doctrine_Orm_OcCustomerResultCacheService()
    {
        $this->services['doctrine_cache.providers.doctrine.orm.oc_customer_result_cache'] = $instance = new \Doctrine\Common\Cache\VoidCache();

        $instance->setNamespace('sf2orm_oc_customer_1d80ffce729c3770350641b6aba39da08a13de62240f7ed985fc0377eaa53fdc');

        return $instance;
    }

    /**
     * Gets the 'doctrine_cache.providers.doctrine.orm.oc_main_metadata_cache' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Common\Cache\ArrayCache A Doctrine\Common\Cache\ArrayCache instance
     */
    protected function getDoctrineCache_Providers_Doctrine_Orm_OcMainMetadataCacheService()
    {
        $this->services['doctrine_cache.providers.doctrine.orm.oc_main_metadata_cache'] = $instance = new \Doctrine\Common\Cache\ArrayCache();

        $instance->setNamespace('sf2orm_oc_main_1d80ffce729c3770350641b6aba39da08a13de62240f7ed985fc0377eaa53fdc');

        return $instance;
    }

    /**
     * Gets the 'doctrine_cache.providers.doctrine.orm.oc_main_query_cache' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Common\Cache\VoidCache A Doctrine\Common\Cache\VoidCache instance
     */
    protected function getDoctrineCache_Providers_Doctrine_Orm_OcMainQueryCacheService()
    {
        $this->services['doctrine_cache.providers.doctrine.orm.oc_main_query_cache'] = $instance = new \Doctrine\Common\Cache\VoidCache();

        $instance->setNamespace('sf2orm_oc_main_1d80ffce729c3770350641b6aba39da08a13de62240f7ed985fc0377eaa53fdc');

        return $instance;
    }

    /**
     * Gets the 'doctrine_cache.providers.doctrine.orm.oc_main_result_cache' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Common\Cache\VoidCache A Doctrine\Common\Cache\VoidCache instance
     */
    protected function getDoctrineCache_Providers_Doctrine_Orm_OcMainResultCacheService()
    {
        $this->services['doctrine_cache.providers.doctrine.orm.oc_main_result_cache'] = $instance = new \Doctrine\Common\Cache\VoidCache();

        $instance->setNamespace('sf2orm_oc_main_1d80ffce729c3770350641b6aba39da08a13de62240f7ed985fc0377eaa53fdc');

        return $instance;
    }

    /**
     * Gets the 'doctrine_cache.providers.doctrine.orm.shared_metadata_cache' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Common\Cache\ArrayCache A Doctrine\Common\Cache\ArrayCache instance
     */
    protected function getDoctrineCache_Providers_Doctrine_Orm_SharedMetadataCacheService()
    {
        $this->services['doctrine_cache.providers.doctrine.orm.shared_metadata_cache'] = $instance = new \Doctrine\Common\Cache\ArrayCache();

        $instance->setNamespace('sf2orm_shared_1d80ffce729c3770350641b6aba39da08a13de62240f7ed985fc0377eaa53fdc');

        return $instance;
    }

    /**
     * Gets the 'doctrine_cache.providers.doctrine.orm.shared_query_cache' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Common\Cache\ArrayCache A Doctrine\Common\Cache\ArrayCache instance
     */
    protected function getDoctrineCache_Providers_Doctrine_Orm_SharedQueryCacheService()
    {
        $this->services['doctrine_cache.providers.doctrine.orm.shared_query_cache'] = $instance = new \Doctrine\Common\Cache\ArrayCache();

        $instance->setNamespace('sf2orm_shared_1d80ffce729c3770350641b6aba39da08a13de62240f7ed985fc0377eaa53fdc');

        return $instance;
    }

    /**
     * Gets the 'doctrine_cache.providers.doctrine.orm.shared_result_cache' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Common\Cache\ArrayCache A Doctrine\Common\Cache\ArrayCache instance
     */
    protected function getDoctrineCache_Providers_Doctrine_Orm_SharedResultCacheService()
    {
        $this->services['doctrine_cache.providers.doctrine.orm.shared_result_cache'] = $instance = new \Doctrine\Common\Cache\ArrayCache();

        $instance->setNamespace('sf2orm_shared_1d80ffce729c3770350641b6aba39da08a13de62240f7ed985fc0377eaa53fdc');

        return $instance;
    }

    /**
     * Gets the 'doctrine_cache.providers.doctrine.orm.uss_customer_metadata_cache' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Common\Cache\ArrayCache A Doctrine\Common\Cache\ArrayCache instance
     */
    protected function getDoctrineCache_Providers_Doctrine_Orm_UssCustomerMetadataCacheService()
    {
        $this->services['doctrine_cache.providers.doctrine.orm.uss_customer_metadata_cache'] = $instance = new \Doctrine\Common\Cache\ArrayCache();

        $instance->setNamespace('sf2orm_uss_customer_1d80ffce729c3770350641b6aba39da08a13de62240f7ed985fc0377eaa53fdc');

        return $instance;
    }

    /**
     * Gets the 'doctrine_cache.providers.doctrine.orm.uss_customer_query_cache' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Common\Cache\VoidCache A Doctrine\Common\Cache\VoidCache instance
     */
    protected function getDoctrineCache_Providers_Doctrine_Orm_UssCustomerQueryCacheService()
    {
        $this->services['doctrine_cache.providers.doctrine.orm.uss_customer_query_cache'] = $instance = new \Doctrine\Common\Cache\VoidCache();

        $instance->setNamespace('sf2orm_uss_customer_1d80ffce729c3770350641b6aba39da08a13de62240f7ed985fc0377eaa53fdc');

        return $instance;
    }

    /**
     * Gets the 'doctrine_cache.providers.doctrine.orm.uss_customer_result_cache' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Common\Cache\VoidCache A Doctrine\Common\Cache\VoidCache instance
     */
    protected function getDoctrineCache_Providers_Doctrine_Orm_UssCustomerResultCacheService()
    {
        $this->services['doctrine_cache.providers.doctrine.orm.uss_customer_result_cache'] = $instance = new \Doctrine\Common\Cache\VoidCache();

        $instance->setNamespace('sf2orm_uss_customer_1d80ffce729c3770350641b6aba39da08a13de62240f7ed985fc0377eaa53fdc');

        return $instance;
    }

    /**
     * Gets the 'doctrine_cache.providers.doctrine.orm.uss_main_metadata_cache' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Common\Cache\ArrayCache A Doctrine\Common\Cache\ArrayCache instance
     */
    protected function getDoctrineCache_Providers_Doctrine_Orm_UssMainMetadataCacheService()
    {
        $this->services['doctrine_cache.providers.doctrine.orm.uss_main_metadata_cache'] = $instance = new \Doctrine\Common\Cache\ArrayCache();

        $instance->setNamespace('sf2orm_uss_main_1d80ffce729c3770350641b6aba39da08a13de62240f7ed985fc0377eaa53fdc');

        return $instance;
    }

    /**
     * Gets the 'doctrine_cache.providers.doctrine.orm.uss_main_query_cache' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Common\Cache\VoidCache A Doctrine\Common\Cache\VoidCache instance
     */
    protected function getDoctrineCache_Providers_Doctrine_Orm_UssMainQueryCacheService()
    {
        $this->services['doctrine_cache.providers.doctrine.orm.uss_main_query_cache'] = $instance = new \Doctrine\Common\Cache\VoidCache();

        $instance->setNamespace('sf2orm_uss_main_1d80ffce729c3770350641b6aba39da08a13de62240f7ed985fc0377eaa53fdc');

        return $instance;
    }

    /**
     * Gets the 'doctrine_cache.providers.doctrine.orm.uss_main_result_cache' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Doctrine\Common\Cache\VoidCache A Doctrine\Common\Cache\VoidCache instance
     */
    protected function getDoctrineCache_Providers_Doctrine_Orm_UssMainResultCacheService()
    {
        $this->services['doctrine_cache.providers.doctrine.orm.uss_main_result_cache'] = $instance = new \Doctrine\Common\Cache\VoidCache();

        $instance->setNamespace('sf2orm_uss_main_1d80ffce729c3770350641b6aba39da08a13de62240f7ed985fc0377eaa53fdc');

        return $instance;
    }

    /**
     * Gets the 'easycorp.easylog.handler' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\Common\ExceptionLogHandler A Illuzion\Common\ExceptionLogHandler instance
     */
    protected function getEasycorp_Easylog_HandlerService()
    {
        return $this->services['easycorp.easylog.handler'] = new \Illuzion\Common\ExceptionLogHandler(($this->targetDirs[5].'/logs/sync.log'));
    }

    /**
     * Gets the 'file_locator' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\HttpKernel\Config\FileLocator A Symfony\Component\HttpKernel\Config\FileLocator instance
     */
    protected function getFileLocatorService()
    {
        return $this->services['file_locator'] = new \Symfony\Component\HttpKernel\Config\FileLocator(${($_ = isset($this->services['kernel']) ? $this->services['kernel'] : $this->get('kernel')) && false ?: '_'}, ($this->targetDirs[3].'/app/Resources'), array(0 => ($this->targetDirs[3].'/app')));
    }

    /**
     * Gets the 'filesystem' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\Filesystem\Filesystem A Symfony\Component\Filesystem\Filesystem instance
     */
    protected function getFilesystemService()
    {
        return $this->services['filesystem'] = new \Symfony\Component\Filesystem\Filesystem();
    }

    /**
     * Gets the 'form.factory' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\Form\FormFactory A Symfony\Component\Form\FormFactory instance
     */
    protected function getForm_FactoryService()
    {
        return $this->services['form.factory'] = new \Symfony\Component\Form\FormFactory(${($_ = isset($this->services['form.registry']) ? $this->services['form.registry'] : $this->get('form.registry')) && false ?: '_'}, ${($_ = isset($this->services['form.resolved_type_factory']) ? $this->services['form.resolved_type_factory'] : $this->get('form.resolved_type_factory')) && false ?: '_'});
    }

    /**
     * Gets the 'form.registry' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\Form\FormRegistry A Symfony\Component\Form\FormRegistry instance
     */
    protected function getForm_RegistryService()
    {
        return $this->services['form.registry'] = new \Symfony\Component\Form\FormRegistry(array(0 => new \Symfony\Component\Form\Extension\DependencyInjection\DependencyInjectionExtension(new \Symfony\Component\DependencyInjection\ServiceLocator(array('Illuzion\\ApiBundle\\Form\\Common\\RelationType' => function () {
            return ${($_ = isset($this->services['api.form.relation_type']) ? $this->services['api.form.relation_type'] : $this->get('api.form.relation_type')) && false ?: '_'};
        }, 'Illuzion\\ApiBundle\\Form\\CustomerType' => function () {
            return ${($_ = isset($this->services['api.form.customer_type']) ? $this->services['api.form.customer_type'] : $this->get('api.form.customer_type')) && false ?: '_'};
        }, 'Illuzion\\ApiBundle\\Form\\MovieType' => function () {
            return ${($_ = isset($this->services['api.form.movie_type']) ? $this->services['api.form.movie_type'] : $this->get('api.form.movie_type')) && false ?: '_'};
        }, 'Symfony\\Bridge\\Doctrine\\Form\\Type\\EntityType' => function () {
            return ${($_ = isset($this->services['form.type.entity']) ? $this->services['form.type.entity'] : $this->get('form.type.entity')) && false ?: '_'};
        }, 'Symfony\\Component\\Form\\Extension\\Core\\Type\\ChoiceType' => function () {
            return ${($_ = isset($this->services['form.type.choice']) ? $this->services['form.type.choice'] : $this->getForm_Type_ChoiceService()) && false ?: '_'};
        }, 'Symfony\\Component\\Form\\Extension\\Core\\Type\\FormType' => function () {
            return ${($_ = isset($this->services['form.type.form']) ? $this->services['form.type.form'] : $this->getForm_Type_FormService()) && false ?: '_'};
        })), array('Symfony\\Component\\Form\\Extension\\Core\\Type\\FormType' => new RewindableGenerator(function () {
            yield 0 => ${($_ = isset($this->services['form.type_extension.form.http_foundation']) ? $this->services['form.type_extension.form.http_foundation'] : $this->getForm_TypeExtension_Form_HttpFoundationService()) && false ?: '_'};
            yield 1 => ${($_ = isset($this->services['form.type_extension.form.validator']) ? $this->services['form.type_extension.form.validator'] : $this->getForm_TypeExtension_Form_ValidatorService()) && false ?: '_'};
            yield 2 => ${($_ = isset($this->services['form.type_extension.upload.validator']) ? $this->services['form.type_extension.upload.validator'] : $this->getForm_TypeExtension_Upload_ValidatorService()) && false ?: '_'};
        }, 3), 'Symfony\\Component\\Form\\Extension\\Core\\Type\\RepeatedType' => new RewindableGenerator(function () {
            yield 0 => ${($_ = isset($this->services['form.type_extension.repeated.validator']) ? $this->services['form.type_extension.repeated.validator'] : $this->getForm_TypeExtension_Repeated_ValidatorService()) && false ?: '_'};
        }, 1), 'Symfony\\Component\\Form\\Extension\\Core\\Type\\SubmitType' => new RewindableGenerator(function () {
            yield 0 => ${($_ = isset($this->services['form.type_extension.submit.validator']) ? $this->services['form.type_extension.submit.validator'] : $this->getForm_TypeExtension_Submit_ValidatorService()) && false ?: '_'};
        }, 1)), new RewindableGenerator(function () {
            yield 0 => ${($_ = isset($this->services['form.type_guesser.validator']) ? $this->services['form.type_guesser.validator'] : $this->getForm_TypeGuesser_ValidatorService()) && false ?: '_'};
            yield 1 => ${($_ = isset($this->services['form.type_guesser.doctrine']) ? $this->services['form.type_guesser.doctrine'] : $this->get('form.type_guesser.doctrine')) && false ?: '_'};
        }, 2), NULL)), ${($_ = isset($this->services['form.resolved_type_factory']) ? $this->services['form.resolved_type_factory'] : $this->get('form.resolved_type_factory')) && false ?: '_'});
    }

    /**
     * Gets the 'form.resolved_type_factory' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\Form\ResolvedFormTypeFactory A Symfony\Component\Form\ResolvedFormTypeFactory instance
     */
    protected function getForm_ResolvedTypeFactoryService()
    {
        return $this->services['form.resolved_type_factory'] = new \Symfony\Component\Form\ResolvedFormTypeFactory();
    }

    /**
     * Gets the 'form.type.birthday' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\BirthdayType A Symfony\Component\Form\Extension\Core\Type\BirthdayType instance
     *
     * @deprecated The "form.type.birthday" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getForm_Type_BirthdayService()
    {
        @trigger_error('The "form.type.birthday" service is deprecated since Symfony 3.1 and will be removed in 4.0.', E_USER_DEPRECATED);

        return $this->services['form.type.birthday'] = new \Symfony\Component\Form\Extension\Core\Type\BirthdayType();
    }

    /**
     * Gets the 'form.type.button' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\ButtonType A Symfony\Component\Form\Extension\Core\Type\ButtonType instance
     *
     * @deprecated The "form.type.button" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getForm_Type_ButtonService()
    {
        @trigger_error('The "form.type.button" service is deprecated since Symfony 3.1 and will be removed in 4.0.', E_USER_DEPRECATED);

        return $this->services['form.type.button'] = new \Symfony\Component\Form\Extension\Core\Type\ButtonType();
    }

    /**
     * Gets the 'form.type.checkbox' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\CheckboxType A Symfony\Component\Form\Extension\Core\Type\CheckboxType instance
     *
     * @deprecated The "form.type.checkbox" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getForm_Type_CheckboxService()
    {
        @trigger_error('The "form.type.checkbox" service is deprecated since Symfony 3.1 and will be removed in 4.0.', E_USER_DEPRECATED);

        return $this->services['form.type.checkbox'] = new \Symfony\Component\Form\Extension\Core\Type\CheckboxType();
    }

    /**
     * Gets the 'form.type.collection' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\CollectionType A Symfony\Component\Form\Extension\Core\Type\CollectionType instance
     *
     * @deprecated The "form.type.collection" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getForm_Type_CollectionService()
    {
        @trigger_error('The "form.type.collection" service is deprecated since Symfony 3.1 and will be removed in 4.0.', E_USER_DEPRECATED);

        return $this->services['form.type.collection'] = new \Symfony\Component\Form\Extension\Core\Type\CollectionType();
    }

    /**
     * Gets the 'form.type.country' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\CountryType A Symfony\Component\Form\Extension\Core\Type\CountryType instance
     *
     * @deprecated The "form.type.country" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getForm_Type_CountryService()
    {
        @trigger_error('The "form.type.country" service is deprecated since Symfony 3.1 and will be removed in 4.0.', E_USER_DEPRECATED);

        return $this->services['form.type.country'] = new \Symfony\Component\Form\Extension\Core\Type\CountryType();
    }

    /**
     * Gets the 'form.type.currency' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\CurrencyType A Symfony\Component\Form\Extension\Core\Type\CurrencyType instance
     *
     * @deprecated The "form.type.currency" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getForm_Type_CurrencyService()
    {
        @trigger_error('The "form.type.currency" service is deprecated since Symfony 3.1 and will be removed in 4.0.', E_USER_DEPRECATED);

        return $this->services['form.type.currency'] = new \Symfony\Component\Form\Extension\Core\Type\CurrencyType();
    }

    /**
     * Gets the 'form.type.date' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\DateType A Symfony\Component\Form\Extension\Core\Type\DateType instance
     *
     * @deprecated The "form.type.date" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getForm_Type_DateService()
    {
        @trigger_error('The "form.type.date" service is deprecated since Symfony 3.1 and will be removed in 4.0.', E_USER_DEPRECATED);

        return $this->services['form.type.date'] = new \Symfony\Component\Form\Extension\Core\Type\DateType();
    }

    /**
     * Gets the 'form.type.datetime' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\DateTimeType A Symfony\Component\Form\Extension\Core\Type\DateTimeType instance
     *
     * @deprecated The "form.type.datetime" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getForm_Type_DatetimeService()
    {
        @trigger_error('The "form.type.datetime" service is deprecated since Symfony 3.1 and will be removed in 4.0.', E_USER_DEPRECATED);

        return $this->services['form.type.datetime'] = new \Symfony\Component\Form\Extension\Core\Type\DateTimeType();
    }

    /**
     * Gets the 'form.type.email' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\EmailType A Symfony\Component\Form\Extension\Core\Type\EmailType instance
     *
     * @deprecated The "form.type.email" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getForm_Type_EmailService()
    {
        @trigger_error('The "form.type.email" service is deprecated since Symfony 3.1 and will be removed in 4.0.', E_USER_DEPRECATED);

        return $this->services['form.type.email'] = new \Symfony\Component\Form\Extension\Core\Type\EmailType();
    }

    /**
     * Gets the 'form.type.entity' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Bridge\Doctrine\Form\Type\EntityType A Symfony\Bridge\Doctrine\Form\Type\EntityType instance
     */
    protected function getForm_Type_EntityService()
    {
        return $this->services['form.type.entity'] = new \Symfony\Bridge\Doctrine\Form\Type\EntityType(${($_ = isset($this->services['doctrine']) ? $this->services['doctrine'] : $this->get('doctrine')) && false ?: '_'});
    }

    /**
     * Gets the 'form.type.file' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\FileType A Symfony\Component\Form\Extension\Core\Type\FileType instance
     *
     * @deprecated The "form.type.file" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getForm_Type_FileService()
    {
        @trigger_error('The "form.type.file" service is deprecated since Symfony 3.1 and will be removed in 4.0.', E_USER_DEPRECATED);

        return $this->services['form.type.file'] = new \Symfony\Component\Form\Extension\Core\Type\FileType();
    }

    /**
     * Gets the 'form.type.hidden' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\HiddenType A Symfony\Component\Form\Extension\Core\Type\HiddenType instance
     *
     * @deprecated The "form.type.hidden" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getForm_Type_HiddenService()
    {
        @trigger_error('The "form.type.hidden" service is deprecated since Symfony 3.1 and will be removed in 4.0.', E_USER_DEPRECATED);

        return $this->services['form.type.hidden'] = new \Symfony\Component\Form\Extension\Core\Type\HiddenType();
    }

    /**
     * Gets the 'form.type.integer' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\IntegerType A Symfony\Component\Form\Extension\Core\Type\IntegerType instance
     *
     * @deprecated The "form.type.integer" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getForm_Type_IntegerService()
    {
        @trigger_error('The "form.type.integer" service is deprecated since Symfony 3.1 and will be removed in 4.0.', E_USER_DEPRECATED);

        return $this->services['form.type.integer'] = new \Symfony\Component\Form\Extension\Core\Type\IntegerType();
    }

    /**
     * Gets the 'form.type.language' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\LanguageType A Symfony\Component\Form\Extension\Core\Type\LanguageType instance
     *
     * @deprecated The "form.type.language" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getForm_Type_LanguageService()
    {
        @trigger_error('The "form.type.language" service is deprecated since Symfony 3.1 and will be removed in 4.0.', E_USER_DEPRECATED);

        return $this->services['form.type.language'] = new \Symfony\Component\Form\Extension\Core\Type\LanguageType();
    }

    /**
     * Gets the 'form.type.locale' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\LocaleType A Symfony\Component\Form\Extension\Core\Type\LocaleType instance
     *
     * @deprecated The "form.type.locale" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getForm_Type_LocaleService()
    {
        @trigger_error('The "form.type.locale" service is deprecated since Symfony 3.1 and will be removed in 4.0.', E_USER_DEPRECATED);

        return $this->services['form.type.locale'] = new \Symfony\Component\Form\Extension\Core\Type\LocaleType();
    }

    /**
     * Gets the 'form.type.money' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\MoneyType A Symfony\Component\Form\Extension\Core\Type\MoneyType instance
     *
     * @deprecated The "form.type.money" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getForm_Type_MoneyService()
    {
        @trigger_error('The "form.type.money" service is deprecated since Symfony 3.1 and will be removed in 4.0.', E_USER_DEPRECATED);

        return $this->services['form.type.money'] = new \Symfony\Component\Form\Extension\Core\Type\MoneyType();
    }

    /**
     * Gets the 'form.type.number' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\NumberType A Symfony\Component\Form\Extension\Core\Type\NumberType instance
     *
     * @deprecated The "form.type.number" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getForm_Type_NumberService()
    {
        @trigger_error('The "form.type.number" service is deprecated since Symfony 3.1 and will be removed in 4.0.', E_USER_DEPRECATED);

        return $this->services['form.type.number'] = new \Symfony\Component\Form\Extension\Core\Type\NumberType();
    }

    /**
     * Gets the 'form.type.password' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\PasswordType A Symfony\Component\Form\Extension\Core\Type\PasswordType instance
     *
     * @deprecated The "form.type.password" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getForm_Type_PasswordService()
    {
        @trigger_error('The "form.type.password" service is deprecated since Symfony 3.1 and will be removed in 4.0.', E_USER_DEPRECATED);

        return $this->services['form.type.password'] = new \Symfony\Component\Form\Extension\Core\Type\PasswordType();
    }

    /**
     * Gets the 'form.type.percent' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\PercentType A Symfony\Component\Form\Extension\Core\Type\PercentType instance
     *
     * @deprecated The "form.type.percent" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getForm_Type_PercentService()
    {
        @trigger_error('The "form.type.percent" service is deprecated since Symfony 3.1 and will be removed in 4.0.', E_USER_DEPRECATED);

        return $this->services['form.type.percent'] = new \Symfony\Component\Form\Extension\Core\Type\PercentType();
    }

    /**
     * Gets the 'form.type.radio' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\RadioType A Symfony\Component\Form\Extension\Core\Type\RadioType instance
     *
     * @deprecated The "form.type.radio" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getForm_Type_RadioService()
    {
        @trigger_error('The "form.type.radio" service is deprecated since Symfony 3.1 and will be removed in 4.0.', E_USER_DEPRECATED);

        return $this->services['form.type.radio'] = new \Symfony\Component\Form\Extension\Core\Type\RadioType();
    }

    /**
     * Gets the 'form.type.range' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\RangeType A Symfony\Component\Form\Extension\Core\Type\RangeType instance
     *
     * @deprecated The "form.type.range" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getForm_Type_RangeService()
    {
        @trigger_error('The "form.type.range" service is deprecated since Symfony 3.1 and will be removed in 4.0.', E_USER_DEPRECATED);

        return $this->services['form.type.range'] = new \Symfony\Component\Form\Extension\Core\Type\RangeType();
    }

    /**
     * Gets the 'form.type.repeated' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\RepeatedType A Symfony\Component\Form\Extension\Core\Type\RepeatedType instance
     *
     * @deprecated The "form.type.repeated" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getForm_Type_RepeatedService()
    {
        @trigger_error('The "form.type.repeated" service is deprecated since Symfony 3.1 and will be removed in 4.0.', E_USER_DEPRECATED);

        return $this->services['form.type.repeated'] = new \Symfony\Component\Form\Extension\Core\Type\RepeatedType();
    }

    /**
     * Gets the 'form.type.reset' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\ResetType A Symfony\Component\Form\Extension\Core\Type\ResetType instance
     *
     * @deprecated The "form.type.reset" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getForm_Type_ResetService()
    {
        @trigger_error('The "form.type.reset" service is deprecated since Symfony 3.1 and will be removed in 4.0.', E_USER_DEPRECATED);

        return $this->services['form.type.reset'] = new \Symfony\Component\Form\Extension\Core\Type\ResetType();
    }

    /**
     * Gets the 'form.type.search' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\SearchType A Symfony\Component\Form\Extension\Core\Type\SearchType instance
     *
     * @deprecated The "form.type.search" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getForm_Type_SearchService()
    {
        @trigger_error('The "form.type.search" service is deprecated since Symfony 3.1 and will be removed in 4.0.', E_USER_DEPRECATED);

        return $this->services['form.type.search'] = new \Symfony\Component\Form\Extension\Core\Type\SearchType();
    }

    /**
     * Gets the 'form.type.submit' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\SubmitType A Symfony\Component\Form\Extension\Core\Type\SubmitType instance
     *
     * @deprecated The "form.type.submit" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getForm_Type_SubmitService()
    {
        @trigger_error('The "form.type.submit" service is deprecated since Symfony 3.1 and will be removed in 4.0.', E_USER_DEPRECATED);

        return $this->services['form.type.submit'] = new \Symfony\Component\Form\Extension\Core\Type\SubmitType();
    }

    /**
     * Gets the 'form.type.text' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\TextType A Symfony\Component\Form\Extension\Core\Type\TextType instance
     *
     * @deprecated The "form.type.text" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getForm_Type_TextService()
    {
        @trigger_error('The "form.type.text" service is deprecated since Symfony 3.1 and will be removed in 4.0.', E_USER_DEPRECATED);

        return $this->services['form.type.text'] = new \Symfony\Component\Form\Extension\Core\Type\TextType();
    }

    /**
     * Gets the 'form.type.textarea' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\TextareaType A Symfony\Component\Form\Extension\Core\Type\TextareaType instance
     *
     * @deprecated The "form.type.textarea" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getForm_Type_TextareaService()
    {
        @trigger_error('The "form.type.textarea" service is deprecated since Symfony 3.1 and will be removed in 4.0.', E_USER_DEPRECATED);

        return $this->services['form.type.textarea'] = new \Symfony\Component\Form\Extension\Core\Type\TextareaType();
    }

    /**
     * Gets the 'form.type.time' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\TimeType A Symfony\Component\Form\Extension\Core\Type\TimeType instance
     *
     * @deprecated The "form.type.time" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getForm_Type_TimeService()
    {
        @trigger_error('The "form.type.time" service is deprecated since Symfony 3.1 and will be removed in 4.0.', E_USER_DEPRECATED);

        return $this->services['form.type.time'] = new \Symfony\Component\Form\Extension\Core\Type\TimeType();
    }

    /**
     * Gets the 'form.type.timezone' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\TimezoneType A Symfony\Component\Form\Extension\Core\Type\TimezoneType instance
     *
     * @deprecated The "form.type.timezone" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getForm_Type_TimezoneService()
    {
        @trigger_error('The "form.type.timezone" service is deprecated since Symfony 3.1 and will be removed in 4.0.', E_USER_DEPRECATED);

        return $this->services['form.type.timezone'] = new \Symfony\Component\Form\Extension\Core\Type\TimezoneType();
    }

    /**
     * Gets the 'form.type.url' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\UrlType A Symfony\Component\Form\Extension\Core\Type\UrlType instance
     *
     * @deprecated The "form.type.url" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getForm_Type_UrlService()
    {
        @trigger_error('The "form.type.url" service is deprecated since Symfony 3.1 and will be removed in 4.0.', E_USER_DEPRECATED);

        return $this->services['form.type.url'] = new \Symfony\Component\Form\Extension\Core\Type\UrlType();
    }

    /**
     * Gets the 'form.type_guesser.doctrine' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Bridge\Doctrine\Form\DoctrineOrmTypeGuesser A Symfony\Bridge\Doctrine\Form\DoctrineOrmTypeGuesser instance
     */
    protected function getForm_TypeGuesser_DoctrineService()
    {
        return $this->services['form.type_guesser.doctrine'] = new \Symfony\Bridge\Doctrine\Form\DoctrineOrmTypeGuesser(${($_ = isset($this->services['doctrine']) ? $this->services['doctrine'] : $this->get('doctrine')) && false ?: '_'});
    }

    /**
     * Gets the 'fragment.handler' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\HttpKernel\DependencyInjection\LazyLoadingFragmentHandler A Symfony\Component\HttpKernel\DependencyInjection\LazyLoadingFragmentHandler instance
     */
    protected function getFragment_HandlerService()
    {
        return $this->services['fragment.handler'] = new \Symfony\Component\HttpKernel\DependencyInjection\LazyLoadingFragmentHandler(${($_ = isset($this->services['service_locator.f3e4af76f32830cd048a9a0d4662e709']) ? $this->services['service_locator.f3e4af76f32830cd048a9a0d4662e709'] : $this->getServiceLocator_F3e4af76f32830cd048a9a0d4662e709Service()) && false ?: '_'}, ${($_ = isset($this->services['request_stack']) ? $this->services['request_stack'] : $this->get('request_stack')) && false ?: '_'}, true);
    }

    /**
     * Gets the 'fragment.listener' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\HttpKernel\EventListener\FragmentListener A Symfony\Component\HttpKernel\EventListener\FragmentListener instance
     */
    protected function getFragment_ListenerService()
    {
        return $this->services['fragment.listener'] = new \Symfony\Component\HttpKernel\EventListener\FragmentListener(${($_ = isset($this->services['uri_signer']) ? $this->services['uri_signer'] : $this->get('uri_signer')) && false ?: '_'}, '/_fragment');
    }

    /**
     * Gets the 'fragment.renderer.esi' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\HttpKernel\Fragment\EsiFragmentRenderer A Symfony\Component\HttpKernel\Fragment\EsiFragmentRenderer instance
     */
    protected function getFragment_Renderer_EsiService()
    {
        $this->services['fragment.renderer.esi'] = $instance = new \Symfony\Component\HttpKernel\Fragment\EsiFragmentRenderer(NULL, ${($_ = isset($this->services['fragment.renderer.inline']) ? $this->services['fragment.renderer.inline'] : $this->get('fragment.renderer.inline')) && false ?: '_'}, ${($_ = isset($this->services['uri_signer']) ? $this->services['uri_signer'] : $this->get('uri_signer')) && false ?: '_'});

        $instance->setFragmentPath('/_fragment');

        return $instance;
    }

    /**
     * Gets the 'fragment.renderer.hinclude' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\HttpKernel\Fragment\HIncludeFragmentRenderer A Symfony\Component\HttpKernel\Fragment\HIncludeFragmentRenderer instance
     */
    protected function getFragment_Renderer_HincludeService()
    {
        $this->services['fragment.renderer.hinclude'] = $instance = new \Symfony\Component\HttpKernel\Fragment\HIncludeFragmentRenderer('', ${($_ = isset($this->services['uri_signer']) ? $this->services['uri_signer'] : $this->get('uri_signer')) && false ?: '_'}, '');

        $instance->setFragmentPath('/_fragment');

        return $instance;
    }

    /**
     * Gets the 'fragment.renderer.inline' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\HttpKernel\Fragment\InlineFragmentRenderer A Symfony\Component\HttpKernel\Fragment\InlineFragmentRenderer instance
     */
    protected function getFragment_Renderer_InlineService()
    {
        $this->services['fragment.renderer.inline'] = $instance = new \Symfony\Component\HttpKernel\Fragment\InlineFragmentRenderer(${($_ = isset($this->services['http_kernel']) ? $this->services['http_kernel'] : $this->get('http_kernel')) && false ?: '_'}, ${($_ = isset($this->services['debug.event_dispatcher']) ? $this->services['debug.event_dispatcher'] : $this->get('debug.event_dispatcher')) && false ?: '_'});

        $instance->setFragmentPath('/_fragment');

        return $instance;
    }

    /**
     * Gets the 'fragment.renderer.ssi' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\HttpKernel\Fragment\SsiFragmentRenderer A Symfony\Component\HttpKernel\Fragment\SsiFragmentRenderer instance
     */
    protected function getFragment_Renderer_SsiService()
    {
        $this->services['fragment.renderer.ssi'] = $instance = new \Symfony\Component\HttpKernel\Fragment\SsiFragmentRenderer(NULL, ${($_ = isset($this->services['fragment.renderer.inline']) ? $this->services['fragment.renderer.inline'] : $this->get('fragment.renderer.inline')) && false ?: '_'}, ${($_ = isset($this->services['uri_signer']) ? $this->services['uri_signer'] : $this->get('uri_signer')) && false ?: '_'});

        $instance->setFragmentPath('/_fragment');

        return $instance;
    }

    /**
     * Gets the 'http_kernel' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\HttpKernel\HttpKernel A Symfony\Component\HttpKernel\HttpKernel instance
     */
    protected function getHttpKernelService()
    {
        return $this->services['http_kernel'] = new \Symfony\Component\HttpKernel\HttpKernel(${($_ = isset($this->services['debug.event_dispatcher']) ? $this->services['debug.event_dispatcher'] : $this->get('debug.event_dispatcher')) && false ?: '_'}, ${($_ = isset($this->services['debug.controller_resolver']) ? $this->services['debug.controller_resolver'] : $this->get('debug.controller_resolver')) && false ?: '_'}, ${($_ = isset($this->services['request_stack']) ? $this->services['request_stack'] : $this->get('request_stack')) && false ?: '_'}, ${($_ = isset($this->services['debug.argument_resolver']) ? $this->services['debug.argument_resolver'] : $this->get('debug.argument_resolver')) && false ?: '_'});
    }

    /**
     * Gets the 'kernel.class_cache.cache_warmer' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Bundle\FrameworkBundle\CacheWarmer\ClassCacheCacheWarmer A Symfony\Bundle\FrameworkBundle\CacheWarmer\ClassCacheCacheWarmer instance
     */
    protected function getKernel_ClassCache_CacheWarmerService()
    {
        return $this->services['kernel.class_cache.cache_warmer'] = new \Symfony\Bundle\FrameworkBundle\CacheWarmer\ClassCacheCacheWarmer(array(0 => 'Symfony\\Component\\HttpFoundation\\ParameterBag', 1 => 'Symfony\\Component\\HttpFoundation\\HeaderBag', 2 => 'Symfony\\Component\\HttpFoundation\\FileBag', 3 => 'Symfony\\Component\\HttpFoundation\\ServerBag', 4 => 'Symfony\\Component\\HttpFoundation\\Request', 5 => 'Symfony\\Component\\HttpKernel\\Kernel'));
    }

    /**
     * Gets the 'locale_listener' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\HttpKernel\EventListener\LocaleListener A Symfony\Component\HttpKernel\EventListener\LocaleListener instance
     */
    protected function getLocaleListenerService()
    {
        return $this->services['locale_listener'] = new \Symfony\Component\HttpKernel\EventListener\LocaleListener(${($_ = isset($this->services['request_stack']) ? $this->services['request_stack'] : $this->get('request_stack')) && false ?: '_'}, 'ru', ${($_ = isset($this->services['router']) ? $this->services['router'] : $this->get('router', ContainerInterface::NULL_ON_INVALID_REFERENCE)) && false ?: '_'});
    }

    /**
     * Gets the 'logger' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Bridge\Monolog\Logger A Symfony\Bridge\Monolog\Logger instance
     */
    protected function getLoggerService()
    {
        $this->services['logger'] = $instance = new \Symfony\Bridge\Monolog\Logger('app');

        $instance->useMicrosecondTimestamps(true);
        $instance->pushHandler(${($_ = isset($this->services['monolog.handler.main']) ? $this->services['monolog.handler.main'] : $this->get('monolog.handler.main')) && false ?: '_'});

        return $instance;
    }

    /**
     * Gets the 'monolog.handler.buffered' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Monolog\Handler\BufferHandler A Monolog\Handler\BufferHandler instance
     */
    protected function getMonolog_Handler_BufferedService()
    {
        $this->services['monolog.handler.buffered'] = $instance = new \Monolog\Handler\BufferHandler(${($_ = isset($this->services['easycorp.easylog.handler']) ? $this->services['easycorp.easylog.handler'] : $this->get('easycorp.easylog.handler')) && false ?: '_'}, 0, 100, true, false);

        $instance->pushProcessor(${($_ = isset($this->services['monolog.processor.psr_log_message']) ? $this->services['monolog.processor.psr_log_message'] : $this->getMonolog_Processor_PsrLogMessageService()) && false ?: '_'});

        return $instance;
    }

    /**
     * Gets the 'monolog.handler.main' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Monolog\Handler\FingersCrossedHandler A Monolog\Handler\FingersCrossedHandler instance
     */
    protected function getMonolog_Handler_MainService()
    {
        $this->services['monolog.handler.main'] = $instance = new \Monolog\Handler\FingersCrossedHandler(${($_ = isset($this->services['monolog.handler.buffered']) ? $this->services['monolog.handler.buffered'] : $this->get('monolog.handler.buffered')) && false ?: '_'}, 400, 0, true, true, NULL);

        $instance->pushProcessor(${($_ = isset($this->services['monolog.processor.psr_log_message']) ? $this->services['monolog.processor.psr_log_message'] : $this->getMonolog_Processor_PsrLogMessageService()) && false ?: '_'});

        return $instance;
    }

    /**
     * Gets the 'monolog.handler.null_internal' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Monolog\Handler\NullHandler A Monolog\Handler\NullHandler instance
     */
    protected function getMonolog_Handler_NullInternalService()
    {
        return $this->services['monolog.handler.null_internal'] = new \Monolog\Handler\NullHandler();
    }

    /**
     * Gets the 'monolog.logger.cache' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Bridge\Monolog\Logger A Symfony\Bridge\Monolog\Logger instance
     */
    protected function getMonolog_Logger_CacheService()
    {
        $this->services['monolog.logger.cache'] = $instance = new \Symfony\Bridge\Monolog\Logger('cache');

        $instance->pushHandler(${($_ = isset($this->services['monolog.handler.main']) ? $this->services['monolog.handler.main'] : $this->get('monolog.handler.main')) && false ?: '_'});

        return $instance;
    }

    /**
     * Gets the 'monolog.logger.console' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Bridge\Monolog\Logger A Symfony\Bridge\Monolog\Logger instance
     */
    protected function getMonolog_Logger_ConsoleService()
    {
        $this->services['monolog.logger.console'] = $instance = new \Symfony\Bridge\Monolog\Logger('console');

        $instance->pushHandler(${($_ = isset($this->services['monolog.handler.main']) ? $this->services['monolog.handler.main'] : $this->get('monolog.handler.main')) && false ?: '_'});

        return $instance;
    }

    /**
     * Gets the 'monolog.logger.doctrine' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Bridge\Monolog\Logger A Symfony\Bridge\Monolog\Logger instance
     */
    protected function getMonolog_Logger_DoctrineService()
    {
        $this->services['monolog.logger.doctrine'] = $instance = new \Symfony\Bridge\Monolog\Logger('doctrine');

        $instance->pushHandler(${($_ = isset($this->services['monolog.handler.main']) ? $this->services['monolog.handler.main'] : $this->get('monolog.handler.main')) && false ?: '_'});

        return $instance;
    }

    /**
     * Gets the 'monolog.logger.event' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Bridge\Monolog\Logger A Symfony\Bridge\Monolog\Logger instance
     */
    protected function getMonolog_Logger_EventService()
    {
        $this->services['monolog.logger.event'] = $instance = new \Symfony\Bridge\Monolog\Logger('event');

        $instance->pushHandler(${($_ = isset($this->services['monolog.handler.main']) ? $this->services['monolog.handler.main'] : $this->get('monolog.handler.main')) && false ?: '_'});

        return $instance;
    }

    /**
     * Gets the 'monolog.logger.php' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Bridge\Monolog\Logger A Symfony\Bridge\Monolog\Logger instance
     */
    protected function getMonolog_Logger_PhpService()
    {
        $this->services['monolog.logger.php'] = $instance = new \Symfony\Bridge\Monolog\Logger('php');

        $instance->pushHandler(${($_ = isset($this->services['monolog.handler.main']) ? $this->services['monolog.handler.main'] : $this->get('monolog.handler.main')) && false ?: '_'});

        return $instance;
    }

    /**
     * Gets the 'monolog.logger.request' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Bridge\Monolog\Logger A Symfony\Bridge\Monolog\Logger instance
     */
    protected function getMonolog_Logger_RequestService()
    {
        $this->services['monolog.logger.request'] = $instance = new \Symfony\Bridge\Monolog\Logger('request');

        $instance->pushHandler(${($_ = isset($this->services['monolog.handler.main']) ? $this->services['monolog.handler.main'] : $this->get('monolog.handler.main')) && false ?: '_'});

        return $instance;
    }

    /**
     * Gets the 'monolog.logger.router' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Bridge\Monolog\Logger A Symfony\Bridge\Monolog\Logger instance
     */
    protected function getMonolog_Logger_RouterService()
    {
        $this->services['monolog.logger.router'] = $instance = new \Symfony\Bridge\Monolog\Logger('router');

        $instance->pushHandler(${($_ = isset($this->services['monolog.handler.main']) ? $this->services['monolog.handler.main'] : $this->get('monolog.handler.main')) && false ?: '_'});

        return $instance;
    }

    /**
     * Gets the 'monolog.logger.security' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Bridge\Monolog\Logger A Symfony\Bridge\Monolog\Logger instance
     */
    protected function getMonolog_Logger_SecurityService()
    {
        $this->services['monolog.logger.security'] = $instance = new \Symfony\Bridge\Monolog\Logger('security');

        $instance->pushHandler(${($_ = isset($this->services['monolog.handler.main']) ? $this->services['monolog.handler.main'] : $this->get('monolog.handler.main')) && false ?: '_'});

        return $instance;
    }

    /**
     * Gets the 'property_accessor' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\PropertyAccess\PropertyAccessor A Symfony\Component\PropertyAccess\PropertyAccessor instance
     */
    protected function getPropertyAccessorService()
    {
        return $this->services['property_accessor'] = new \Symfony\Component\PropertyAccess\PropertyAccessor(false, false, new \Symfony\Component\Cache\Adapter\ArrayAdapter(0, false));
    }

    /**
     * Gets the 'request.add_request_formats_listener' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\HttpKernel\EventListener\AddRequestFormatsListener A Symfony\Component\HttpKernel\EventListener\AddRequestFormatsListener instance
     */
    protected function getRequest_AddRequestFormatsListenerService()
    {
        return $this->services['request.add_request_formats_listener'] = new \Symfony\Component\HttpKernel\EventListener\AddRequestFormatsListener(array('jsonapi' => array(0 => 'application/vnd.api+json')));
    }

    /**
     * Gets the 'request_stack' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\HttpFoundation\RequestStack A Symfony\Component\HttpFoundation\RequestStack instance
     */
    protected function getRequestStackService()
    {
        return $this->services['request_stack'] = new \Symfony\Component\HttpFoundation\RequestStack();
    }

    /**
     * Gets the 'response_listener' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\HttpKernel\EventListener\ResponseListener A Symfony\Component\HttpKernel\EventListener\ResponseListener instance
     */
    protected function getResponseListenerService()
    {
        return $this->services['response_listener'] = new \Symfony\Component\HttpKernel\EventListener\ResponseListener('UTF-8');
    }

    /**
     * Gets the 'router' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Bundle\FrameworkBundle\Routing\Router A Symfony\Bundle\FrameworkBundle\Routing\Router instance
     */
    protected function getRouterService()
    {
        $this->services['router'] = $instance = new \Symfony\Bundle\FrameworkBundle\Routing\Router($this, ($this->targetDirs[3].'/app/config/routing.yml'), array('cache_dir' => __DIR__, 'debug' => true, 'generator_class' => 'Symfony\\Component\\Routing\\Generator\\UrlGenerator', 'generator_base_class' => 'Symfony\\Component\\Routing\\Generator\\UrlGenerator', 'generator_dumper_class' => 'Symfony\\Component\\Routing\\Generator\\Dumper\\PhpGeneratorDumper', 'generator_cache_class' => 'appSyncDebugProjectContainerUrlGenerator', 'matcher_class' => 'Symfony\\Bundle\\FrameworkBundle\\Routing\\RedirectableUrlMatcher', 'matcher_base_class' => 'Symfony\\Bundle\\FrameworkBundle\\Routing\\RedirectableUrlMatcher', 'matcher_dumper_class' => 'Symfony\\Component\\Routing\\Matcher\\Dumper\\PhpMatcherDumper', 'matcher_cache_class' => 'appSyncDebugProjectContainerUrlMatcher', 'strict_requirements' => NULL), ${($_ = isset($this->services['router.request_context']) ? $this->services['router.request_context'] : $this->getRouter_RequestContextService()) && false ?: '_'}, ${($_ = isset($this->services['monolog.logger.router']) ? $this->services['monolog.logger.router'] : $this->get('monolog.logger.router', ContainerInterface::NULL_ON_INVALID_REFERENCE)) && false ?: '_'});

        $instance->setConfigCacheFactory(${($_ = isset($this->services['config_cache_factory']) ? $this->services['config_cache_factory'] : $this->get('config_cache_factory')) && false ?: '_'});

        return $instance;
    }

    /**
     * Gets the 'router_listener' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\HttpKernel\EventListener\RouterListener A Symfony\Component\HttpKernel\EventListener\RouterListener instance
     */
    protected function getRouterListenerService()
    {
        return $this->services['router_listener'] = new \Symfony\Component\HttpKernel\EventListener\RouterListener(${($_ = isset($this->services['router']) ? $this->services['router'] : $this->get('router')) && false ?: '_'}, ${($_ = isset($this->services['request_stack']) ? $this->services['request_stack'] : $this->get('request_stack')) && false ?: '_'}, ${($_ = isset($this->services['router.request_context']) ? $this->services['router.request_context'] : $this->getRouter_RequestContextService()) && false ?: '_'}, ${($_ = isset($this->services['monolog.logger.request']) ? $this->services['monolog.logger.request'] : $this->get('monolog.logger.request', ContainerInterface::NULL_ON_INVALID_REFERENCE)) && false ?: '_'});
    }

    /**
     * Gets the 'routing.loader' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Bundle\FrameworkBundle\Routing\DelegatingLoader A Symfony\Bundle\FrameworkBundle\Routing\DelegatingLoader instance
     */
    protected function getRouting_LoaderService()
    {
        $a = ${($_ = isset($this->services['file_locator']) ? $this->services['file_locator'] : $this->get('file_locator')) && false ?: '_'};
        $b = ${($_ = isset($this->services['annotation_reader']) ? $this->services['annotation_reader'] : $this->get('annotation_reader')) && false ?: '_'};

        $c = new \Sensio\Bundle\FrameworkExtraBundle\Routing\AnnotatedRouteControllerLoader($b);

        $d = new \Symfony\Component\Config\Loader\LoaderResolver();
        $d->addLoader(new \Symfony\Component\Routing\Loader\XmlFileLoader($a));
        $d->addLoader(new \Symfony\Component\Routing\Loader\YamlFileLoader($a));
        $d->addLoader(new \Symfony\Component\Routing\Loader\PhpFileLoader($a));
        $d->addLoader(new \Symfony\Component\Config\Loader\GlobFileLoader($a));
        $d->addLoader(new \Symfony\Component\Routing\Loader\DirectoryLoader($a));
        $d->addLoader(new \Symfony\Component\Routing\Loader\DependencyInjection\ServiceRouterLoader($this));
        $d->addLoader(new \Symfony\Component\Routing\Loader\AnnotationDirectoryLoader($a, $c));
        $d->addLoader(new \Symfony\Component\Routing\Loader\AnnotationFileLoader($a, $c));
        $d->addLoader($c);

        return $this->services['routing.loader'] = new \Symfony\Bundle\FrameworkBundle\Routing\DelegatingLoader(${($_ = isset($this->services['controller_name_converter']) ? $this->services['controller_name_converter'] : $this->getControllerNameConverterService()) && false ?: '_'}, $d);
    }

    /**
     * Gets the 'security.authentication.guard_handler' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\Security\Guard\GuardAuthenticatorHandler A Symfony\Component\Security\Guard\GuardAuthenticatorHandler instance
     */
    protected function getSecurity_Authentication_GuardHandlerService()
    {
        return $this->services['security.authentication.guard_handler'] = new \Symfony\Component\Security\Guard\GuardAuthenticatorHandler(${($_ = isset($this->services['security.token_storage']) ? $this->services['security.token_storage'] : $this->get('security.token_storage')) && false ?: '_'}, ${($_ = isset($this->services['debug.event_dispatcher']) ? $this->services['debug.event_dispatcher'] : $this->get('debug.event_dispatcher', ContainerInterface::NULL_ON_INVALID_REFERENCE)) && false ?: '_'});
    }

    /**
     * Gets the 'security.authentication_utils' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\Security\Http\Authentication\AuthenticationUtils A Symfony\Component\Security\Http\Authentication\AuthenticationUtils instance
     */
    protected function getSecurity_AuthenticationUtilsService()
    {
        return $this->services['security.authentication_utils'] = new \Symfony\Component\Security\Http\Authentication\AuthenticationUtils(${($_ = isset($this->services['request_stack']) ? $this->services['request_stack'] : $this->get('request_stack')) && false ?: '_'});
    }

    /**
     * Gets the 'security.authorization_checker' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\Security\Core\Authorization\AuthorizationChecker A Symfony\Component\Security\Core\Authorization\AuthorizationChecker instance
     */
    protected function getSecurity_AuthorizationCheckerService()
    {
        return $this->services['security.authorization_checker'] = new \Symfony\Component\Security\Core\Authorization\AuthorizationChecker(${($_ = isset($this->services['security.token_storage']) ? $this->services['security.token_storage'] : $this->get('security.token_storage')) && false ?: '_'}, ${($_ = isset($this->services['security.authentication.manager']) ? $this->services['security.authentication.manager'] : $this->getSecurity_Authentication_ManagerService()) && false ?: '_'}, ${($_ = isset($this->services['debug.security.access.decision_manager']) ? $this->services['debug.security.access.decision_manager'] : $this->getDebug_Security_Access_DecisionManagerService()) && false ?: '_'}, false);
    }

    /**
     * Gets the 'security.encoder_factory' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\Security\Core\Encoder\EncoderFactory A Symfony\Component\Security\Core\Encoder\EncoderFactory instance
     */
    protected function getSecurity_EncoderFactoryService()
    {
        return $this->services['security.encoder_factory'] = new \Symfony\Component\Security\Core\Encoder\EncoderFactory(array());
    }

    /**
     * Gets the 'security.firewall' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Bundle\SecurityBundle\EventListener\FirewallListener A Symfony\Bundle\SecurityBundle\EventListener\FirewallListener instance
     */
    protected function getSecurity_FirewallService()
    {
        return $this->services['security.firewall'] = new \Symfony\Bundle\SecurityBundle\EventListener\FirewallListener(new \Symfony\Bundle\SecurityBundle\Security\FirewallMap(new \Symfony\Component\DependencyInjection\ServiceLocator(array('security.firewall.map.context.api' => function () {
            return ${($_ = isset($this->services['security.firewall.map.context.api']) ? $this->services['security.firewall.map.context.api'] : $this->get('security.firewall.map.context.api')) && false ?: '_'};
        }, 'security.firewall.map.context.dev' => function () {
            return ${($_ = isset($this->services['security.firewall.map.context.dev']) ? $this->services['security.firewall.map.context.dev'] : $this->get('security.firewall.map.context.dev')) && false ?: '_'};
        }, 'security.firewall.map.context.swagger' => function () {
            return ${($_ = isset($this->services['security.firewall.map.context.swagger']) ? $this->services['security.firewall.map.context.swagger'] : $this->get('security.firewall.map.context.swagger')) && false ?: '_'};
        })), new RewindableGenerator(function () {
            yield 'security.firewall.map.context.dev' => ${($_ = isset($this->services['security.request_matcher.5314eeb91110adf24b9b678372bb11bbe00e8858c519c088bfb65f525181ad3bf573fd1d']) ? $this->services['security.request_matcher.5314eeb91110adf24b9b678372bb11bbe00e8858c519c088bfb65f525181ad3bf573fd1d'] : $this->getSecurity_RequestMatcher_5314eeb91110adf24b9b678372bb11bbe00e8858c519c088bfb65f525181ad3bf573fd1dService()) && false ?: '_'};
            yield 'security.firewall.map.context.swagger' => ${($_ = isset($this->services['security.request_matcher.5233d5f86c1e2076eac36f9cc15152586d6330c2cd3da54fdb761a92d2bb3b59b468e25d']) ? $this->services['security.request_matcher.5233d5f86c1e2076eac36f9cc15152586d6330c2cd3da54fdb761a92d2bb3b59b468e25d'] : $this->getSecurity_RequestMatcher_5233d5f86c1e2076eac36f9cc15152586d6330c2cd3da54fdb761a92d2bb3b59b468e25dService()) && false ?: '_'};
            yield 'security.firewall.map.context.api' => ${($_ = isset($this->services['security.request_matcher.c9679edbbb26b9639a697ddca8184f648b310f5623db2680a8f22b3b5426f010053cae6b']) ? $this->services['security.request_matcher.c9679edbbb26b9639a697ddca8184f648b310f5623db2680a8f22b3b5426f010053cae6b'] : $this->getSecurity_RequestMatcher_C9679edbbb26b9639a697ddca8184f648b310f5623db2680a8f22b3b5426f010053cae6bService()) && false ?: '_'};
        }, 3)), ${($_ = isset($this->services['debug.event_dispatcher']) ? $this->services['debug.event_dispatcher'] : $this->get('debug.event_dispatcher')) && false ?: '_'}, ${($_ = isset($this->services['security.logout_url_generator']) ? $this->services['security.logout_url_generator'] : $this->getSecurity_LogoutUrlGeneratorService()) && false ?: '_'});
    }

    /**
     * Gets the 'security.firewall.map.context.api' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Bundle\SecurityBundle\Security\FirewallContext A Symfony\Bundle\SecurityBundle\Security\FirewallContext instance
     */
    protected function getSecurity_Firewall_Map_Context_ApiService()
    {
        $a = ${($_ = isset($this->services['monolog.logger.security']) ? $this->services['monolog.logger.security'] : $this->get('monolog.logger.security', ContainerInterface::NULL_ON_INVALID_REFERENCE)) && false ?: '_'};
        $b = ${($_ = isset($this->services['security.authentication.manager']) ? $this->services['security.authentication.manager'] : $this->getSecurity_Authentication_ManagerService()) && false ?: '_'};
        $c = ${($_ = isset($this->services['security.token_storage']) ? $this->services['security.token_storage'] : $this->get('security.token_storage')) && false ?: '_'};
        $d = ${($_ = isset($this->services['router']) ? $this->services['router'] : $this->get('router', ContainerInterface::NULL_ON_INVALID_REFERENCE)) && false ?: '_'};

        $e = new \Symfony\Component\Security\Http\AccessMap();

        return $this->services['security.firewall.map.context.api'] = new \Symfony\Bundle\SecurityBundle\Security\FirewallContext(array(0 => new \Symfony\Component\Security\Http\Firewall\ChannelListener($e, new \Symfony\Component\Security\Http\EntryPoint\RetryAuthenticationEntryPoint(80, 443), $a), 1 => new \Symfony\Component\Security\Guard\Firewall\GuardAuthenticationListener(${($_ = isset($this->services['security.authentication.guard_handler']) ? $this->services['security.authentication.guard_handler'] : $this->get('security.authentication.guard_handler')) && false ?: '_'}, $b, 'api', new RewindableGenerator(function () {
            yield 0 => ${($_ = isset($this->services['api.security.jws_guard_authenticator']) ? $this->services['api.security.jws_guard_authenticator'] : $this->get('api.security.jws_guard_authenticator')) && false ?: '_'};
        }, 1), $a), 2 => new \Symfony\Component\Security\Http\Firewall\AccessListener($c, ${($_ = isset($this->services['debug.security.access.decision_manager']) ? $this->services['debug.security.access.decision_manager'] : $this->getDebug_Security_Access_DecisionManagerService()) && false ?: '_'}, $e, $b)), new \Symfony\Component\Security\Http\Firewall\ExceptionListener($c, ${($_ = isset($this->services['security.authentication.trust_resolver']) ? $this->services['security.authentication.trust_resolver'] : $this->getSecurity_Authentication_TrustResolverService()) && false ?: '_'}, new \Symfony\Component\Security\Http\HttpUtils($d, $d), 'api', ${($_ = isset($this->services['api.security.jws_guard_authenticator']) ? $this->services['api.security.jws_guard_authenticator'] : $this->get('api.security.jws_guard_authenticator')) && false ?: '_'}, NULL, NULL, $a, true), new \Symfony\Bundle\SecurityBundle\Security\FirewallConfig('api', 'security.user_checker', 'security.request_matcher.c9679edbbb26b9639a697ddca8184f648b310f5623db2680a8f22b3b5426f010053cae6b', true, true, 'security.user.provider.concrete.applications', NULL, 'api.security.jws_guard_authenticator', NULL, NULL, array(0 => 'guard')));
    }

    /**
     * Gets the 'security.firewall.map.context.dev' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Bundle\SecurityBundle\Security\FirewallContext A Symfony\Bundle\SecurityBundle\Security\FirewallContext instance
     */
    protected function getSecurity_Firewall_Map_Context_DevService()
    {
        return $this->services['security.firewall.map.context.dev'] = new \Symfony\Bundle\SecurityBundle\Security\FirewallContext(array(), NULL, new \Symfony\Bundle\SecurityBundle\Security\FirewallConfig('dev', 'security.user_checker', 'security.request_matcher.5314eeb91110adf24b9b678372bb11bbe00e8858c519c088bfb65f525181ad3bf573fd1d', false, '', '', '', '', '', '', array()));
    }

    /**
     * Gets the 'security.firewall.map.context.swagger' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Bundle\SecurityBundle\Security\FirewallContext A Symfony\Bundle\SecurityBundle\Security\FirewallContext instance
     */
    protected function getSecurity_Firewall_Map_Context_SwaggerService()
    {
        return $this->services['security.firewall.map.context.swagger'] = new \Symfony\Bundle\SecurityBundle\Security\FirewallContext(array(), NULL, new \Symfony\Bundle\SecurityBundle\Security\FirewallConfig('swagger', 'security.user_checker', 'security.request_matcher.5233d5f86c1e2076eac36f9cc15152586d6330c2cd3da54fdb761a92d2bb3b59b468e25d', false, '', '', '', '', '', '', array()));
    }

    /**
     * Gets the 'security.password_encoder' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\Security\Core\Encoder\UserPasswordEncoder A Symfony\Component\Security\Core\Encoder\UserPasswordEncoder instance
     */
    protected function getSecurity_PasswordEncoderService()
    {
        return $this->services['security.password_encoder'] = new \Symfony\Component\Security\Core\Encoder\UserPasswordEncoder(${($_ = isset($this->services['security.encoder_factory']) ? $this->services['security.encoder_factory'] : $this->get('security.encoder_factory')) && false ?: '_'});
    }

    /**
     * Gets the 'security.rememberme.response_listener' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\Security\Http\RememberMe\ResponseListener A Symfony\Component\Security\Http\RememberMe\ResponseListener instance
     */
    protected function getSecurity_Rememberme_ResponseListenerService()
    {
        return $this->services['security.rememberme.response_listener'] = new \Symfony\Component\Security\Http\RememberMe\ResponseListener();
    }

    /**
     * Gets the 'security.token_storage' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage A Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage instance
     */
    protected function getSecurity_TokenStorageService()
    {
        return $this->services['security.token_storage'] = new \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage();
    }

    /**
     * Gets the 'security.validator.user_password' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\Security\Core\Validator\Constraints\UserPasswordValidator A Symfony\Component\Security\Core\Validator\Constraints\UserPasswordValidator instance
     */
    protected function getSecurity_Validator_UserPasswordService()
    {
        return $this->services['security.validator.user_password'] = new \Symfony\Component\Security\Core\Validator\Constraints\UserPasswordValidator(${($_ = isset($this->services['security.token_storage']) ? $this->services['security.token_storage'] : $this->get('security.token_storage')) && false ?: '_'}, ${($_ = isset($this->services['security.encoder_factory']) ? $this->services['security.encoder_factory'] : $this->get('security.encoder_factory')) && false ?: '_'});
    }

    /**
     * Gets the 'sensio_framework_extra.cache.listener' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Sensio\Bundle\FrameworkExtraBundle\EventListener\HttpCacheListener A Sensio\Bundle\FrameworkExtraBundle\EventListener\HttpCacheListener instance
     */
    protected function getSensioFrameworkExtra_Cache_ListenerService()
    {
        return $this->services['sensio_framework_extra.cache.listener'] = new \Sensio\Bundle\FrameworkExtraBundle\EventListener\HttpCacheListener();
    }

    /**
     * Gets the 'sensio_framework_extra.controller.listener' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Sensio\Bundle\FrameworkExtraBundle\EventListener\ControllerListener A Sensio\Bundle\FrameworkExtraBundle\EventListener\ControllerListener instance
     */
    protected function getSensioFrameworkExtra_Controller_ListenerService()
    {
        return $this->services['sensio_framework_extra.controller.listener'] = new \Sensio\Bundle\FrameworkExtraBundle\EventListener\ControllerListener(${($_ = isset($this->services['annotation_reader']) ? $this->services['annotation_reader'] : $this->get('annotation_reader')) && false ?: '_'});
    }

    /**
     * Gets the 'sensio_framework_extra.converter.datetime' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\DateTimeParamConverter A Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\DateTimeParamConverter instance
     */
    protected function getSensioFrameworkExtra_Converter_DatetimeService()
    {
        return $this->services['sensio_framework_extra.converter.datetime'] = new \Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\DateTimeParamConverter();
    }

    /**
     * Gets the 'sensio_framework_extra.converter.doctrine.orm' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\DoctrineParamConverter A Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\DoctrineParamConverter instance
     */
    protected function getSensioFrameworkExtra_Converter_Doctrine_OrmService()
    {
        return $this->services['sensio_framework_extra.converter.doctrine.orm'] = new \Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\DoctrineParamConverter(${($_ = isset($this->services['doctrine']) ? $this->services['doctrine'] : $this->get('doctrine', ContainerInterface::NULL_ON_INVALID_REFERENCE)) && false ?: '_'});
    }

    /**
     * Gets the 'sensio_framework_extra.converter.listener' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Sensio\Bundle\FrameworkExtraBundle\EventListener\ParamConverterListener A Sensio\Bundle\FrameworkExtraBundle\EventListener\ParamConverterListener instance
     */
    protected function getSensioFrameworkExtra_Converter_ListenerService()
    {
        return $this->services['sensio_framework_extra.converter.listener'] = new \Sensio\Bundle\FrameworkExtraBundle\EventListener\ParamConverterListener(${($_ = isset($this->services['sensio_framework_extra.converter.manager']) ? $this->services['sensio_framework_extra.converter.manager'] : $this->get('sensio_framework_extra.converter.manager')) && false ?: '_'}, true);
    }

    /**
     * Gets the 'sensio_framework_extra.converter.manager' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterManager A Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterManager instance
     */
    protected function getSensioFrameworkExtra_Converter_ManagerService()
    {
        $this->services['sensio_framework_extra.converter.manager'] = $instance = new \Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterManager();

        $instance->add(${($_ = isset($this->services['sensio_framework_extra.converter.doctrine.orm']) ? $this->services['sensio_framework_extra.converter.doctrine.orm'] : $this->get('sensio_framework_extra.converter.doctrine.orm')) && false ?: '_'}, 0, 'doctrine.orm');
        $instance->add(${($_ = isset($this->services['sensio_framework_extra.converter.datetime']) ? $this->services['sensio_framework_extra.converter.datetime'] : $this->get('sensio_framework_extra.converter.datetime')) && false ?: '_'}, 0, 'datetime');

        return $instance;
    }

    /**
     * Gets the 'sensio_framework_extra.security.listener' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Sensio\Bundle\FrameworkExtraBundle\EventListener\SecurityListener A Sensio\Bundle\FrameworkExtraBundle\EventListener\SecurityListener instance
     */
    protected function getSensioFrameworkExtra_Security_ListenerService()
    {
        return $this->services['sensio_framework_extra.security.listener'] = new \Sensio\Bundle\FrameworkExtraBundle\EventListener\SecurityListener(NULL, new \Sensio\Bundle\FrameworkExtraBundle\Security\ExpressionLanguage(), ${($_ = isset($this->services['security.authentication.trust_resolver']) ? $this->services['security.authentication.trust_resolver'] : $this->getSecurity_Authentication_TrustResolverService()) && false ?: '_'}, ${($_ = isset($this->services['security.role_hierarchy']) ? $this->services['security.role_hierarchy'] : $this->getSecurity_RoleHierarchyService()) && false ?: '_'}, ${($_ = isset($this->services['security.token_storage']) ? $this->services['security.token_storage'] : $this->get('security.token_storage', ContainerInterface::NULL_ON_INVALID_REFERENCE)) && false ?: '_'}, ${($_ = isset($this->services['security.authorization_checker']) ? $this->services['security.authorization_checker'] : $this->get('security.authorization_checker', ContainerInterface::NULL_ON_INVALID_REFERENCE)) && false ?: '_'});
    }

    /**
     * Gets the 'sensio_framework_extra.view.guesser' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Sensio\Bundle\FrameworkExtraBundle\Templating\TemplateGuesser A Sensio\Bundle\FrameworkExtraBundle\Templating\TemplateGuesser instance
     */
    protected function getSensioFrameworkExtra_View_GuesserService()
    {
        return $this->services['sensio_framework_extra.view.guesser'] = new \Sensio\Bundle\FrameworkExtraBundle\Templating\TemplateGuesser(${($_ = isset($this->services['kernel']) ? $this->services['kernel'] : $this->get('kernel')) && false ?: '_'});
    }

    /**
     * Gets the 'sensio_framework_extra.view.listener' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Sensio\Bundle\FrameworkExtraBundle\EventListener\TemplateListener A Sensio\Bundle\FrameworkExtraBundle\EventListener\TemplateListener instance
     */
    protected function getSensioFrameworkExtra_View_ListenerService()
    {
        return $this->services['sensio_framework_extra.view.listener'] = new \Sensio\Bundle\FrameworkExtraBundle\EventListener\TemplateListener($this);
    }

    /**
     * Gets the 'sonata.notification.backend.doctrine' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Sonata\NotificationBundle\Backend\MessageManagerBackendDispatcher A Sonata\NotificationBundle\Backend\MessageManagerBackendDispatcher instance
     */
    protected function getSonata_Notification_Backend_DoctrineService()
    {
        $a = ${($_ = isset($this->services['sonata.notification.manager.message.default']) ? $this->services['sonata.notification.manager.message.default'] : $this->get('sonata.notification.manager.message.default')) && false ?: '_'};

        return $this->services['sonata.notification.backend.doctrine'] = new \Sonata\NotificationBundle\Backend\MessageManagerBackendDispatcher($a, array(0 => array('queue' => 'default', 'default' => true, 'types' => array())), 'default', array(0 => array('types' => array(), 'backend' => new \Sonata\NotificationBundle\Backend\MessageManagerBackend($a, array(2 => 10000, -1 => 20, 1 => 10, 0 => 100), 500000, 86400, 10, array()))));
    }

    /**
     * Gets the 'sonata.notification.backend.postpone' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Sonata\NotificationBundle\Backend\PostponeRuntimeBackend A Sonata\NotificationBundle\Backend\PostponeRuntimeBackend instance
     */
    protected function getSonata_Notification_Backend_PostponeService()
    {
        return $this->services['sonata.notification.backend.postpone'] = new \Sonata\NotificationBundle\Backend\PostponeRuntimeBackend(${($_ = isset($this->services['sonata.notification.dispatcher']) ? $this->services['sonata.notification.dispatcher'] : $this->get('sonata.notification.dispatcher')) && false ?: '_'});
    }

    /**
     * Gets the 'sonata.notification.backend.runtime' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Sonata\NotificationBundle\Backend\RuntimeBackend A Sonata\NotificationBundle\Backend\RuntimeBackend instance
     */
    protected function getSonata_Notification_Backend_RuntimeService()
    {
        return $this->services['sonata.notification.backend.runtime'] = new \Sonata\NotificationBundle\Backend\RuntimeBackend(${($_ = isset($this->services['sonata.notification.dispatcher']) ? $this->services['sonata.notification.dispatcher'] : $this->get('sonata.notification.dispatcher')) && false ?: '_'});
    }

    /**
     * Gets the 'sonata.notification.consumer.metadata' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Sonata\NotificationBundle\Consumer\Metadata A Sonata\NotificationBundle\Consumer\Metadata instance
     */
    protected function getSonata_Notification_Consumer_MetadataService()
    {
        return $this->services['sonata.notification.consumer.metadata'] = new \Sonata\NotificationBundle\Consumer\Metadata(array('sync' => array(0 => 'synchronization.consumer')));
    }

    /**
     * Gets the 'sonata.notification.dispatcher' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\EventDispatcher\ContainerAwareEventDispatcher A Symfony\Component\EventDispatcher\ContainerAwareEventDispatcher instance
     */
    protected function getSonata_Notification_DispatcherService()
    {
        $this->services['sonata.notification.dispatcher'] = $instance = new \Symfony\Component\EventDispatcher\ContainerAwareEventDispatcher($this);

        $instance->addListenerService('sync', array(0 => 'synchronization.consumer', 1 => 'process'), 0);

        return $instance;
    }

    /**
     * Gets the 'sonata.notification.erroneous_messages_selector' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Sonata\NotificationBundle\Selector\ErroneousMessagesSelector A Sonata\NotificationBundle\Selector\ErroneousMessagesSelector instance
     */
    protected function getSonata_Notification_ErroneousMessagesSelectorService()
    {
        return $this->services['sonata.notification.erroneous_messages_selector'] = new \Sonata\NotificationBundle\Selector\ErroneousMessagesSelector(${($_ = isset($this->services['doctrine']) ? $this->services['doctrine'] : $this->get('doctrine')) && false ?: '_'}, 'Illuzion\\SynchronizationBundle\\Entity\\Message');
    }

    /**
     * Gets the 'sonata.notification.event.doctrine_backend_optimize' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Sonata\NotificationBundle\Event\DoctrineBackendOptimizeListener A Sonata\NotificationBundle\Event\DoctrineBackendOptimizeListener instance
     */
    protected function getSonata_Notification_Event_DoctrineBackendOptimizeService()
    {
        return $this->services['sonata.notification.event.doctrine_backend_optimize'] = new \Sonata\NotificationBundle\Event\DoctrineBackendOptimizeListener(${($_ = isset($this->services['doctrine']) ? $this->services['doctrine'] : $this->get('doctrine')) && false ?: '_'});
    }

    /**
     * Gets the 'sonata.notification.event.doctrine_optimize' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Sonata\NotificationBundle\Event\DoctrineOptimizeListener A Sonata\NotificationBundle\Event\DoctrineOptimizeListener instance
     */
    protected function getSonata_Notification_Event_DoctrineOptimizeService()
    {
        return $this->services['sonata.notification.event.doctrine_optimize'] = new \Sonata\NotificationBundle\Event\DoctrineOptimizeListener(${($_ = isset($this->services['doctrine']) ? $this->services['doctrine'] : $this->get('doctrine')) && false ?: '_'});
    }

    /**
     * Gets the 'sonata.notification.manager.message.default' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Sonata\NotificationBundle\Entity\MessageManager A Sonata\NotificationBundle\Entity\MessageManager instance
     */
    protected function getSonata_Notification_Manager_Message_DefaultService()
    {
        return $this->services['sonata.notification.manager.message.default'] = new \Sonata\NotificationBundle\Entity\MessageManager('Illuzion\\SynchronizationBundle\\Entity\\Message', ${($_ = isset($this->services['doctrine']) ? $this->services['doctrine'] : $this->get('doctrine')) && false ?: '_'});
    }

    /**
     * Gets the 'streamed_response_listener' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\HttpKernel\EventListener\StreamedResponseListener A Symfony\Component\HttpKernel\EventListener\StreamedResponseListener instance
     */
    protected function getStreamedResponseListenerService()
    {
        return $this->services['streamed_response_listener'] = new \Symfony\Component\HttpKernel\EventListener\StreamedResponseListener();
    }

    /**
     * Gets the 'swiftmailer.email_sender.listener' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Bundle\SwiftmailerBundle\EventListener\EmailSenderListener A Symfony\Bundle\SwiftmailerBundle\EventListener\EmailSenderListener instance
     */
    protected function getSwiftmailer_EmailSender_ListenerService()
    {
        return $this->services['swiftmailer.email_sender.listener'] = new \Symfony\Bundle\SwiftmailerBundle\EventListener\EmailSenderListener($this, ${($_ = isset($this->services['logger']) ? $this->services['logger'] : $this->get('logger', ContainerInterface::NULL_ON_INVALID_REFERENCE)) && false ?: '_'});
    }

    /**
     * Gets the 'swiftmailer.mailer.default' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Swift_Mailer A Swift_Mailer instance
     */
    protected function getSwiftmailer_Mailer_DefaultService()
    {
        return $this->services['swiftmailer.mailer.default'] = new \Swift_Mailer(${($_ = isset($this->services['swiftmailer.mailer.default.transport']) ? $this->services['swiftmailer.mailer.default.transport'] : $this->get('swiftmailer.mailer.default.transport')) && false ?: '_'});
    }

    /**
     * Gets the 'swiftmailer.mailer.default.plugin.messagelogger' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Swift_Plugins_MessageLogger A Swift_Plugins_MessageLogger instance
     */
    protected function getSwiftmailer_Mailer_Default_Plugin_MessageloggerService()
    {
        return $this->services['swiftmailer.mailer.default.plugin.messagelogger'] = new \Swift_Plugins_MessageLogger();
    }

    /**
     * Gets the 'swiftmailer.mailer.default.transport' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Swift_Transport_EsmtpTransport A Swift_Transport_EsmtpTransport instance
     */
    protected function getSwiftmailer_Mailer_Default_TransportService()
    {
        $a = new \Swift_Transport_Esmtp_AuthHandler(array(0 => new \Swift_Transport_Esmtp_Auth_CramMd5Authenticator(), 1 => new \Swift_Transport_Esmtp_Auth_LoginAuthenticator(), 2 => new \Swift_Transport_Esmtp_Auth_PlainAuthenticator()));
        $a->setUsername(NULL);
        $a->setPassword(NULL);
        $a->setAuthMode(NULL);

        $this->services['swiftmailer.mailer.default.transport'] = $instance = new \Swift_Transport_EsmtpTransport(new \Swift_Transport_StreamBuffer(new \Swift_StreamFilters_StringReplacementFilterFactory()), array(0 => $a), new \Swift_Events_SimpleEventDispatcher());

        $instance->setHost('localhost');
        $instance->setPort(25);
        $instance->setEncryption(NULL);
        $instance->setTimeout(30);
        $instance->setSourceIp(NULL);
        $instance->registerPlugin(${($_ = isset($this->services['swiftmailer.mailer.default.plugin.messagelogger']) ? $this->services['swiftmailer.mailer.default.plugin.messagelogger'] : $this->get('swiftmailer.mailer.default.plugin.messagelogger')) && false ?: '_'});
        (new \Symfony\Bundle\SwiftmailerBundle\DependencyInjection\SmtpTransportConfigurator(NULL, ${($_ = isset($this->services['router.request_context']) ? $this->services['router.request_context'] : $this->getRouter_RequestContextService()) && false ?: '_'}))->configure($instance);

        return $instance;
    }

    /**
     * Gets the 'synchronization.consumer' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\SynchronizationBundle\Service\SyncConsumer A Illuzion\SynchronizationBundle\Service\SyncConsumer instance
     */
    protected function getSynchronization_ConsumerService()
    {
        return $this->services['synchronization.consumer'] = new \Illuzion\SynchronizationBundle\Service\SyncConsumer(${($_ = isset($this->services['synchronization.handler']) ? $this->services['synchronization.handler'] : $this->get('synchronization.handler')) && false ?: '_'}, ${($_ = isset($this->services['api.provider.em']) ? $this->services['api.provider.em'] : $this->get('api.provider.em')) && false ?: '_'});
    }

    /**
     * Gets the 'synchronization.handler' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\SynchronizationBundle\Service\SyncHandler A Illuzion\SynchronizationBundle\Service\SyncHandler instance
     */
    protected function getSynchronization_HandlerService()
    {
        $this->services['synchronization.handler'] = $instance = new \Illuzion\SynchronizationBundle\Service\SyncHandler();

        $instance->registerEntityHandler(${($_ = isset($this->services['synchronization.handler.program']) ? $this->services['synchronization.handler.program'] : $this->get('synchronization.handler.program')) && false ?: '_'});
        $instance->registerEntityHandler(${($_ = isset($this->services['synchronization.handler.movie']) ? $this->services['synchronization.handler.movie'] : $this->get('synchronization.handler.movie')) && false ?: '_'});
        $instance->registerEntityHandler(${($_ = isset($this->services['synchronization.handler.distributor']) ? $this->services['synchronization.handler.distributor'] : $this->get('synchronization.handler.distributor')) && false ?: '_'});

        return $instance;
    }

    /**
     * Gets the 'synchronization.handler.distributor' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\SynchronizationBundle\Hanlder\DistributorSyncHandler A Illuzion\SynchronizationBundle\Hanlder\DistributorSyncHandler instance
     */
    protected function getSynchronization_Handler_DistributorService()
    {
        return $this->services['synchronization.handler.distributor'] = new \Illuzion\SynchronizationBundle\Hanlder\DistributorSyncHandler(${($_ = isset($this->services['api.provider.em']) ? $this->services['api.provider.em'] : $this->get('api.provider.em')) && false ?: '_'}, ${($_ = isset($this->services['logger']) ? $this->services['logger'] : $this->get('logger')) && false ?: '_'});
    }

    /**
     * Gets the 'synchronization.handler.movie' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\SynchronizationBundle\Hanlder\MovieSyncHandler A Illuzion\SynchronizationBundle\Hanlder\MovieSyncHandler instance
     */
    protected function getSynchronization_Handler_MovieService()
    {
        return $this->services['synchronization.handler.movie'] = new \Illuzion\SynchronizationBundle\Hanlder\MovieSyncHandler(${($_ = isset($this->services['api.provider.em']) ? $this->services['api.provider.em'] : $this->get('api.provider.em')) && false ?: '_'}, ${($_ = isset($this->services['logger']) ? $this->services['logger'] : $this->get('logger')) && false ?: '_'});
    }

    /**
     * Gets the 'synchronization.handler.program' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\SynchronizationBundle\Hanlder\ProgramSyncHandler A Illuzion\SynchronizationBundle\Hanlder\ProgramSyncHandler instance
     */
    protected function getSynchronization_Handler_ProgramService()
    {
        return $this->services['synchronization.handler.program'] = new \Illuzion\SynchronizationBundle\Hanlder\ProgramSyncHandler(${($_ = isset($this->services['api.provider.em']) ? $this->services['api.provider.em'] : $this->get('api.provider.em')) && false ?: '_'}, ${($_ = isset($this->services['logger']) ? $this->services['logger'] : $this->get('logger')) && false ?: '_'});
    }

    /**
     * Gets the 'synchronization.listener.entity_update' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Illuzion\SynchronizationBundle\Listener\EntityUpdateListener A Illuzion\SynchronizationBundle\Listener\EntityUpdateListener instance
     */
    protected function getSynchronization_Listener_EntityUpdateService()
    {
        return $this->services['synchronization.listener.entity_update'] = new \Illuzion\SynchronizationBundle\Listener\EntityUpdateListener(${($_ = isset($this->services['synchronization.handler']) ? $this->services['synchronization.handler'] : $this->get('synchronization.handler')) && false ?: '_'}, ${($_ = isset($this->services['sonata.notification.backend.doctrine']) ? $this->services['sonata.notification.backend.doctrine'] : $this->get('sonata.notification.backend.doctrine')) && false ?: '_'}, array(0 => 'uss', 1 => 'oc', 2 => 'ill', 3 => 'che', 4 => 'nmega'));
    }

    /**
     * Gets the 'templating.helper.logout_url' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Bundle\SecurityBundle\Templating\Helper\LogoutUrlHelper A Symfony\Bundle\SecurityBundle\Templating\Helper\LogoutUrlHelper instance
     */
    protected function getTemplating_Helper_LogoutUrlService()
    {
        return $this->services['templating.helper.logout_url'] = new \Symfony\Bundle\SecurityBundle\Templating\Helper\LogoutUrlHelper(${($_ = isset($this->services['security.logout_url_generator']) ? $this->services['security.logout_url_generator'] : $this->getSecurity_LogoutUrlGeneratorService()) && false ?: '_'});
    }

    /**
     * Gets the 'templating.helper.security' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Bundle\SecurityBundle\Templating\Helper\SecurityHelper A Symfony\Bundle\SecurityBundle\Templating\Helper\SecurityHelper instance
     */
    protected function getTemplating_Helper_SecurityService()
    {
        return $this->services['templating.helper.security'] = new \Symfony\Bundle\SecurityBundle\Templating\Helper\SecurityHelper(${($_ = isset($this->services['security.authorization_checker']) ? $this->services['security.authorization_checker'] : $this->get('security.authorization_checker', ContainerInterface::NULL_ON_INVALID_REFERENCE)) && false ?: '_'});
    }

    /**
     * Gets the 'translator' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\Translation\IdentityTranslator A Symfony\Component\Translation\IdentityTranslator instance
     */
    protected function getTranslatorService()
    {
        return $this->services['translator'] = new \Symfony\Component\Translation\IdentityTranslator(new \Symfony\Component\Translation\MessageSelector());
    }

    /**
     * Gets the 'uri_signer' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\HttpKernel\UriSigner A Symfony\Component\HttpKernel\UriSigner instance
     */
    protected function getUriSignerService()
    {
        return $this->services['uri_signer'] = new \Symfony\Component\HttpKernel\UriSigner('da08e547r7319955bg3d7465866619dfd0fbe478');
    }

    /**
     * Gets the 'validate_request_listener' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\HttpKernel\EventListener\ValidateRequestListener A Symfony\Component\HttpKernel\EventListener\ValidateRequestListener instance
     */
    protected function getValidateRequestListenerService()
    {
        return $this->services['validate_request_listener'] = new \Symfony\Component\HttpKernel\EventListener\ValidateRequestListener();
    }

    /**
     * Gets the 'validator' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\Validator\Validator\ValidatorInterface A Symfony\Component\Validator\Validator\ValidatorInterface instance
     */
    protected function getValidatorService()
    {
        return $this->services['validator'] = ${($_ = isset($this->services['validator.builder']) ? $this->services['validator.builder'] : $this->get('validator.builder')) && false ?: '_'}->getValidator();
    }

    /**
     * Gets the 'validator.builder' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\Validator\ValidatorBuilderInterface A Symfony\Component\Validator\ValidatorBuilderInterface instance
     */
    protected function getValidator_BuilderService()
    {
        $this->services['validator.builder'] = $instance = \Symfony\Component\Validator\Validation::createValidatorBuilder();

        $instance->setConstraintValidatorFactory(new \Symfony\Component\Validator\ContainerConstraintValidatorFactory(new \Symfony\Component\DependencyInjection\ServiceLocator(array('Symfony\\Bridge\\Doctrine\\Validator\\Constraints\\UniqueEntityValidator' => function () {
            return ${($_ = isset($this->services['doctrine.orm.validator.unique']) ? $this->services['doctrine.orm.validator.unique'] : $this->get('doctrine.orm.validator.unique')) && false ?: '_'};
        }, 'Symfony\\Component\\Security\\Core\\Validator\\Constraints\\UserPasswordValidator' => function () {
            return ${($_ = isset($this->services['security.validator.user_password']) ? $this->services['security.validator.user_password'] : $this->get('security.validator.user_password')) && false ?: '_'};
        }, 'Symfony\\Component\\Validator\\Constraints\\EmailValidator' => function () {
            return ${($_ = isset($this->services['validator.email']) ? $this->services['validator.email'] : $this->get('validator.email')) && false ?: '_'};
        }, 'Symfony\\Component\\Validator\\Constraints\\ExpressionValidator' => function () {
            return ${($_ = isset($this->services['validator.expression']) ? $this->services['validator.expression'] : $this->get('validator.expression')) && false ?: '_'};
        }, 'doctrine.orm.validator.unique' => function () {
            return ${($_ = isset($this->services['doctrine.orm.validator.unique']) ? $this->services['doctrine.orm.validator.unique'] : $this->get('doctrine.orm.validator.unique')) && false ?: '_'};
        }, 'security.validator.user_password' => function () {
            return ${($_ = isset($this->services['security.validator.user_password']) ? $this->services['security.validator.user_password'] : $this->get('security.validator.user_password')) && false ?: '_'};
        }, 'validator.expression' => function () {
            return ${($_ = isset($this->services['validator.expression']) ? $this->services['validator.expression'] : $this->get('validator.expression')) && false ?: '_'};
        }))));
        $instance->setTranslator(${($_ = isset($this->services['translator']) ? $this->services['translator'] : $this->get('translator')) && false ?: '_'});
        $instance->setTranslationDomain('validators');
        $instance->addXmlMappings(array(0 => ($this->targetDirs[3].'/vendor/symfony/symfony/src/Symfony/Component/Form/Resources/config/validation.xml'), 1 => ($this->targetDirs[3].'/vendor/sonata-project/notification-bundle/Resources/config/validation.xml')));
        $instance->enableAnnotationMapping(${($_ = isset($this->services['annotation_reader']) ? $this->services['annotation_reader'] : $this->get('annotation_reader')) && false ?: '_'});
        $instance->addMethodMapping('loadValidatorMetadata');
        $instance->addObjectInitializers(array(0 => ${($_ = isset($this->services['doctrine.orm.validator_initializer']) ? $this->services['doctrine.orm.validator_initializer'] : $this->get('doctrine.orm.validator_initializer')) && false ?: '_'}));

        return $instance;
    }

    /**
     * Gets the 'validator.email' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\Validator\Constraints\EmailValidator A Symfony\Component\Validator\Constraints\EmailValidator instance
     */
    protected function getValidator_EmailService()
    {
        return $this->services['validator.email'] = new \Symfony\Component\Validator\Constraints\EmailValidator(false);
    }

    /**
     * Gets the 'validator.expression' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * @return \Symfony\Component\Validator\Constraints\ExpressionValidator A Symfony\Component\Validator\Constraints\ExpressionValidator instance
     */
    protected function getValidator_ExpressionService()
    {
        return $this->services['validator.expression'] = new \Symfony\Component\Validator\Constraints\ExpressionValidator();
    }

    /**
     * Gets the '1_5bc971da440047f3dbeaf372c109d1b5c64861934e6dbd155bbae3c432cf847d' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * This service is private.
     * If you want to be able to request this service from the container directly,
     * make it public, otherwise you might end up with broken code.
     *
     * @return \Symfony\Component\DependencyInjection\Config\ContainerParametersResourceChecker A Symfony\Component\DependencyInjection\Config\ContainerParametersResourceChecker instance
     */
    protected function get15bc971da440047f3dbeaf372c109d1b5c64861934e6dbd155bbae3c432cf847dService()
    {
        return $this->services['1_5bc971da440047f3dbeaf372c109d1b5c64861934e6dbd155bbae3c432cf847d'] = new \Symfony\Component\DependencyInjection\Config\ContainerParametersResourceChecker($this);
    }

    /**
     * Gets the '2_5bc971da440047f3dbeaf372c109d1b5c64861934e6dbd155bbae3c432cf847d' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * This service is private.
     * If you want to be able to request this service from the container directly,
     * make it public, otherwise you might end up with broken code.
     *
     * @return \Symfony\Component\Config\Resource\SelfCheckingResourceChecker A Symfony\Component\Config\Resource\SelfCheckingResourceChecker instance
     */
    protected function get25bc971da440047f3dbeaf372c109d1b5c64861934e6dbd155bbae3c432cf847dService()
    {
        return $this->services['2_5bc971da440047f3dbeaf372c109d1b5c64861934e6dbd155bbae3c432cf847d'] = new \Symfony\Component\Config\Resource\SelfCheckingResourceChecker();
    }

    /**
     * Gets the '8464c6758298cf75d30c4f689fb7886d' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * This service is private.
     * If you want to be able to request this service from the container directly,
     * make it public, otherwise you might end up with broken code.
     *
     * @return \Symfony\Component\Cache\Adapter\AbstractAdapter A Symfony\Component\Cache\Adapter\AbstractAdapter instance
     */
    protected function get8464c6758298cf75d30c4f689fb7886dService()
    {
        return $this->services['8464c6758298cf75d30c4f689fb7886d'] = \Symfony\Component\Cache\Adapter\AbstractAdapter::createConnection('memcached://localhost');
    }

    /**
     * Gets the 'annotations.reader' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * This service is private.
     * If you want to be able to request this service from the container directly,
     * make it public, otherwise you might end up with broken code.
     *
     * @return \Doctrine\Common\Annotations\AnnotationReader A Doctrine\Common\Annotations\AnnotationReader instance
     */
    protected function getAnnotations_ReaderService()
    {
        $a = new \Doctrine\Common\Annotations\AnnotationRegistry();
        $a->registerLoader('class_exists');

        $this->services['annotations.reader'] = $instance = new \Doctrine\Common\Annotations\AnnotationReader();

        $instance->addGlobalIgnoredName('required', $a);

        return $instance;
    }

    /**
     * Gets the 'api.params.transformer_factory' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * This service is private.
     * If you want to be able to request this service from the container directly,
     * make it public, otherwise you might end up with broken code.
     *
     * @return \Illuzion\ApiBundle\Request\Params\ParamTransformerFactory A Illuzion\ApiBundle\Request\Params\ParamTransformerFactory instance
     */
    protected function getApi_Params_TransformerFactoryService()
    {
        return $this->services['api.params.transformer_factory'] = new \Illuzion\ApiBundle\Request\Params\ParamTransformerFactory($this);
    }

    /**
     * Gets the 'argument_resolver.default' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * This service is private.
     * If you want to be able to request this service from the container directly,
     * make it public, otherwise you might end up with broken code.
     *
     * @return \Symfony\Component\HttpKernel\Controller\ArgumentResolver\DefaultValueResolver A Symfony\Component\HttpKernel\Controller\ArgumentResolver\DefaultValueResolver instance
     */
    protected function getArgumentResolver_DefaultService()
    {
        return $this->services['argument_resolver.default'] = new \Symfony\Component\HttpKernel\Controller\ArgumentResolver\DefaultValueResolver();
    }

    /**
     * Gets the 'argument_resolver.request' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * This service is private.
     * If you want to be able to request this service from the container directly,
     * make it public, otherwise you might end up with broken code.
     *
     * @return \Symfony\Component\HttpKernel\Controller\ArgumentResolver\RequestValueResolver A Symfony\Component\HttpKernel\Controller\ArgumentResolver\RequestValueResolver instance
     */
    protected function getArgumentResolver_RequestService()
    {
        return $this->services['argument_resolver.request'] = new \Symfony\Component\HttpKernel\Controller\ArgumentResolver\RequestValueResolver();
    }

    /**
     * Gets the 'argument_resolver.request_attribute' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * This service is private.
     * If you want to be able to request this service from the container directly,
     * make it public, otherwise you might end up with broken code.
     *
     * @return \Symfony\Component\HttpKernel\Controller\ArgumentResolver\RequestAttributeValueResolver A Symfony\Component\HttpKernel\Controller\ArgumentResolver\RequestAttributeValueResolver instance
     */
    protected function getArgumentResolver_RequestAttributeService()
    {
        return $this->services['argument_resolver.request_attribute'] = new \Symfony\Component\HttpKernel\Controller\ArgumentResolver\RequestAttributeValueResolver();
    }

    /**
     * Gets the 'argument_resolver.service' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * This service is private.
     * If you want to be able to request this service from the container directly,
     * make it public, otherwise you might end up with broken code.
     *
     * @return \Symfony\Component\HttpKernel\Controller\ArgumentResolver\ServiceValueResolver A Symfony\Component\HttpKernel\Controller\ArgumentResolver\ServiceValueResolver instance
     */
    protected function getArgumentResolver_ServiceService()
    {
        return $this->services['argument_resolver.service'] = new \Symfony\Component\HttpKernel\Controller\ArgumentResolver\ServiceValueResolver(new \Symfony\Component\DependencyInjection\ServiceLocator(array()));
    }

    /**
     * Gets the 'argument_resolver.session' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * This service is private.
     * If you want to be able to request this service from the container directly,
     * make it public, otherwise you might end up with broken code.
     *
     * @return \Symfony\Component\HttpKernel\Controller\ArgumentResolver\SessionValueResolver A Symfony\Component\HttpKernel\Controller\ArgumentResolver\SessionValueResolver instance
     */
    protected function getArgumentResolver_SessionService()
    {
        return $this->services['argument_resolver.session'] = new \Symfony\Component\HttpKernel\Controller\ArgumentResolver\SessionValueResolver();
    }

    /**
     * Gets the 'argument_resolver.variadic' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * This service is private.
     * If you want to be able to request this service from the container directly,
     * make it public, otherwise you might end up with broken code.
     *
     * @return \Symfony\Component\HttpKernel\Controller\ArgumentResolver\VariadicValueResolver A Symfony\Component\HttpKernel\Controller\ArgumentResolver\VariadicValueResolver instance
     */
    protected function getArgumentResolver_VariadicService()
    {
        return $this->services['argument_resolver.variadic'] = new \Symfony\Component\HttpKernel\Controller\ArgumentResolver\VariadicValueResolver();
    }

    /**
     * Gets the 'cache.annotations' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * This service is private.
     * If you want to be able to request this service from the container directly,
     * make it public, otherwise you might end up with broken code.
     *
     * @param bool    $lazyLoad whether to try lazy-loading the service with a proxy
     *
     * @return \Symfony\Component\Cache\Adapter\MemcachedAdapter A Symfony\Component\Cache\Adapter\MemcachedAdapter instance
     */
    public function getCache_AnnotationsService($lazyLoad = true)
    {
        if ($lazyLoad) {

            return $this->services['cache.annotations'] = new SymfonyComponentCacheAdapterMemcachedAdapter_000000002084051500000000634d02687471c441876336872902bf356897607e(
                function (&$wrappedInstance, \ProxyManager\Proxy\LazyLoadingInterface $proxy) {
                    $wrappedInstance = $this->getCache_AnnotationsService(false);

                    $proxy->setProxyInitializer(null);

                    return true;
                }
            );
        }

        $instance = new \Symfony\Component\Cache\Adapter\MemcachedAdapter(${($_ = isset($this->services['8464c6758298cf75d30c4f689fb7886d']) ? $this->services['8464c6758298cf75d30c4f689fb7886d'] : $this->get8464c6758298cf75d30c4f689fb7886dService()) && false ?: '_'}, 'F7eoCMthtN', 0);

        if ($this->has('monolog.logger.cache')) {
            $instance->setLogger(${($_ = isset($this->services['monolog.logger.cache']) ? $this->services['monolog.logger.cache'] : $this->get('monolog.logger.cache', ContainerInterface::NULL_ON_INVALID_REFERENCE)) && false ?: '_'});
        }

        return $instance;
    }

    /**
     * Gets the 'cache.validator' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * This service is private.
     * If you want to be able to request this service from the container directly,
     * make it public, otherwise you might end up with broken code.
     *
     * @param bool    $lazyLoad whether to try lazy-loading the service with a proxy
     *
     * @return \Symfony\Component\Cache\Adapter\MemcachedAdapter A Symfony\Component\Cache\Adapter\MemcachedAdapter instance
     */
    public function getCache_ValidatorService($lazyLoad = true)
    {
        if ($lazyLoad) {

            return $this->services['cache.validator'] = new SymfonyComponentCacheAdapterMemcachedAdapter_000000002084051700000000634d02687471c441876336872902bf356897607e(
                function (&$wrappedInstance, \ProxyManager\Proxy\LazyLoadingInterface $proxy) {
                    $wrappedInstance = $this->getCache_ValidatorService(false);

                    $proxy->setProxyInitializer(null);

                    return true;
                }
            );
        }

        $instance = new \Symfony\Component\Cache\Adapter\MemcachedAdapter(${($_ = isset($this->services['8464c6758298cf75d30c4f689fb7886d']) ? $this->services['8464c6758298cf75d30c4f689fb7886d'] : $this->get8464c6758298cf75d30c4f689fb7886dService()) && false ?: '_'}, '4c7d9Ditqd', 0);

        if ($this->has('monolog.logger.cache')) {
            $instance->setLogger(${($_ = isset($this->services['monolog.logger.cache']) ? $this->services['monolog.logger.cache'] : $this->get('monolog.logger.cache', ContainerInterface::NULL_ON_INVALID_REFERENCE)) && false ?: '_'});
        }

        return $instance;
    }

    /**
     * Gets the 'console.error_listener' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * This service is private.
     * If you want to be able to request this service from the container directly,
     * make it public, otherwise you might end up with broken code.
     *
     * @return \Symfony\Component\Console\EventListener\ErrorListener A Symfony\Component\Console\EventListener\ErrorListener instance
     */
    protected function getConsole_ErrorListenerService()
    {
        return $this->services['console.error_listener'] = new \Symfony\Component\Console\EventListener\ErrorListener(${($_ = isset($this->services['monolog.logger.console']) ? $this->services['monolog.logger.console'] : $this->get('monolog.logger.console', ContainerInterface::NULL_ON_INVALID_REFERENCE)) && false ?: '_'});
    }

    /**
     * Gets the 'controller_name_converter' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * This service is private.
     * If you want to be able to request this service from the container directly,
     * make it public, otherwise you might end up with broken code.
     *
     * @return \Symfony\Bundle\FrameworkBundle\Controller\ControllerNameParser A Symfony\Bundle\FrameworkBundle\Controller\ControllerNameParser instance
     */
    protected function getControllerNameConverterService()
    {
        return $this->services['controller_name_converter'] = new \Symfony\Bundle\FrameworkBundle\Controller\ControllerNameParser(${($_ = isset($this->services['kernel']) ? $this->services['kernel'] : $this->get('kernel')) && false ?: '_'});
    }

    /**
     * Gets the 'debug.security.access.decision_manager' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * This service is private.
     * If you want to be able to request this service from the container directly,
     * make it public, otherwise you might end up with broken code.
     *
     * @return \Symfony\Component\Security\Core\Authorization\TraceableAccessDecisionManager A Symfony\Component\Security\Core\Authorization\TraceableAccessDecisionManager instance
     */
    protected function getDebug_Security_Access_DecisionManagerService()
    {
        return $this->services['debug.security.access.decision_manager'] = new \Symfony\Component\Security\Core\Authorization\TraceableAccessDecisionManager(new \Symfony\Component\Security\Core\Authorization\AccessDecisionManager(new RewindableGenerator(function () {
            yield 0 => ${($_ = isset($this->services['security.access.authenticated_voter']) ? $this->services['security.access.authenticated_voter'] : $this->getSecurity_Access_AuthenticatedVoterService()) && false ?: '_'};
            yield 1 => ${($_ = isset($this->services['security.access.role_hierarchy_voter']) ? $this->services['security.access.role_hierarchy_voter'] : $this->getSecurity_Access_RoleHierarchyVoterService()) && false ?: '_'};
            yield 2 => ${($_ = isset($this->services['security.access.expression_voter']) ? $this->services['security.access.expression_voter'] : $this->getSecurity_Access_ExpressionVoterService()) && false ?: '_'};
        }, 3), 'affirmative', false, true));
    }

    /**
     * Gets the 'doctrine.dbal.logger' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * This service is private.
     * If you want to be able to request this service from the container directly,
     * make it public, otherwise you might end up with broken code.
     *
     * @return \Symfony\Bridge\Doctrine\Logger\DbalLogger A Symfony\Bridge\Doctrine\Logger\DbalLogger instance
     */
    protected function getDoctrine_Dbal_LoggerService()
    {
        return $this->services['doctrine.dbal.logger'] = new \Symfony\Bridge\Doctrine\Logger\DbalLogger(${($_ = isset($this->services['monolog.logger.doctrine']) ? $this->services['monolog.logger.doctrine'] : $this->get('monolog.logger.doctrine', ContainerInterface::NULL_ON_INVALID_REFERENCE)) && false ?: '_'}, ${($_ = isset($this->services['debug.stopwatch']) ? $this->services['debug.stopwatch'] : $this->get('debug.stopwatch', ContainerInterface::NULL_ON_INVALID_REFERENCE)) && false ?: '_'});
    }

    /**
     * Gets the 'doctrine.orm.naming_strategy.default' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * This service is private.
     * If you want to be able to request this service from the container directly,
     * make it public, otherwise you might end up with broken code.
     *
     * @return \Doctrine\ORM\Mapping\DefaultNamingStrategy A Doctrine\ORM\Mapping\DefaultNamingStrategy instance
     */
    protected function getDoctrine_Orm_NamingStrategy_DefaultService()
    {
        return $this->services['doctrine.orm.naming_strategy.default'] = new \Doctrine\ORM\Mapping\DefaultNamingStrategy();
    }

    /**
     * Gets the 'doctrine.orm.quote_strategy.default' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * This service is private.
     * If you want to be able to request this service from the container directly,
     * make it public, otherwise you might end up with broken code.
     *
     * @return \Doctrine\ORM\Mapping\DefaultQuoteStrategy A Doctrine\ORM\Mapping\DefaultQuoteStrategy instance
     */
    protected function getDoctrine_Orm_QuoteStrategy_DefaultService()
    {
        return $this->services['doctrine.orm.quote_strategy.default'] = new \Doctrine\ORM\Mapping\DefaultQuoteStrategy();
    }

    /**
     * Gets the 'form.type.choice' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * This service is private.
     * If you want to be able to request this service from the container directly,
     * make it public, otherwise you might end up with broken code.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\ChoiceType A Symfony\Component\Form\Extension\Core\Type\ChoiceType instance
     */
    protected function getForm_Type_ChoiceService()
    {
        return $this->services['form.type.choice'] = new \Symfony\Component\Form\Extension\Core\Type\ChoiceType(new \Symfony\Component\Form\ChoiceList\Factory\CachingFactoryDecorator(new \Symfony\Component\Form\ChoiceList\Factory\PropertyAccessDecorator(new \Symfony\Component\Form\ChoiceList\Factory\DefaultChoiceListFactory(), ${($_ = isset($this->services['property_accessor']) ? $this->services['property_accessor'] : $this->get('property_accessor')) && false ?: '_'})));
    }

    /**
     * Gets the 'form.type.form' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * This service is private.
     * If you want to be able to request this service from the container directly,
     * make it public, otherwise you might end up with broken code.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\FormType A Symfony\Component\Form\Extension\Core\Type\FormType instance
     */
    protected function getForm_Type_FormService()
    {
        return $this->services['form.type.form'] = new \Symfony\Component\Form\Extension\Core\Type\FormType(${($_ = isset($this->services['property_accessor']) ? $this->services['property_accessor'] : $this->get('property_accessor')) && false ?: '_'});
    }

    /**
     * Gets the 'form.type_extension.form.http_foundation' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * This service is private.
     * If you want to be able to request this service from the container directly,
     * make it public, otherwise you might end up with broken code.
     *
     * @return \Symfony\Component\Form\Extension\HttpFoundation\Type\FormTypeHttpFoundationExtension A Symfony\Component\Form\Extension\HttpFoundation\Type\FormTypeHttpFoundationExtension instance
     */
    protected function getForm_TypeExtension_Form_HttpFoundationService()
    {
        return $this->services['form.type_extension.form.http_foundation'] = new \Symfony\Component\Form\Extension\HttpFoundation\Type\FormTypeHttpFoundationExtension(new \Symfony\Component\Form\Extension\HttpFoundation\HttpFoundationRequestHandler(new \Symfony\Component\Form\Util\ServerParams(${($_ = isset($this->services['request_stack']) ? $this->services['request_stack'] : $this->get('request_stack')) && false ?: '_'})));
    }

    /**
     * Gets the 'form.type_extension.form.validator' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * This service is private.
     * If you want to be able to request this service from the container directly,
     * make it public, otherwise you might end up with broken code.
     *
     * @return \Symfony\Component\Form\Extension\Validator\Type\FormTypeValidatorExtension A Symfony\Component\Form\Extension\Validator\Type\FormTypeValidatorExtension instance
     */
    protected function getForm_TypeExtension_Form_ValidatorService()
    {
        return $this->services['form.type_extension.form.validator'] = new \Symfony\Component\Form\Extension\Validator\Type\FormTypeValidatorExtension(${($_ = isset($this->services['validator']) ? $this->services['validator'] : $this->get('validator')) && false ?: '_'});
    }

    /**
     * Gets the 'form.type_extension.repeated.validator' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * This service is private.
     * If you want to be able to request this service from the container directly,
     * make it public, otherwise you might end up with broken code.
     *
     * @return \Symfony\Component\Form\Extension\Validator\Type\RepeatedTypeValidatorExtension A Symfony\Component\Form\Extension\Validator\Type\RepeatedTypeValidatorExtension instance
     */
    protected function getForm_TypeExtension_Repeated_ValidatorService()
    {
        return $this->services['form.type_extension.repeated.validator'] = new \Symfony\Component\Form\Extension\Validator\Type\RepeatedTypeValidatorExtension();
    }

    /**
     * Gets the 'form.type_extension.submit.validator' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * This service is private.
     * If you want to be able to request this service from the container directly,
     * make it public, otherwise you might end up with broken code.
     *
     * @return \Symfony\Component\Form\Extension\Validator\Type\SubmitTypeValidatorExtension A Symfony\Component\Form\Extension\Validator\Type\SubmitTypeValidatorExtension instance
     */
    protected function getForm_TypeExtension_Submit_ValidatorService()
    {
        return $this->services['form.type_extension.submit.validator'] = new \Symfony\Component\Form\Extension\Validator\Type\SubmitTypeValidatorExtension();
    }

    /**
     * Gets the 'form.type_extension.upload.validator' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * This service is private.
     * If you want to be able to request this service from the container directly,
     * make it public, otherwise you might end up with broken code.
     *
     * @return \Symfony\Component\Form\Extension\Validator\Type\UploadValidatorExtension A Symfony\Component\Form\Extension\Validator\Type\UploadValidatorExtension instance
     */
    protected function getForm_TypeExtension_Upload_ValidatorService()
    {
        return $this->services['form.type_extension.upload.validator'] = new \Symfony\Component\Form\Extension\Validator\Type\UploadValidatorExtension(${($_ = isset($this->services['translator']) ? $this->services['translator'] : $this->get('translator')) && false ?: '_'}, 'validators');
    }

    /**
     * Gets the 'form.type_guesser.validator' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * This service is private.
     * If you want to be able to request this service from the container directly,
     * make it public, otherwise you might end up with broken code.
     *
     * @return \Symfony\Component\Form\Extension\Validator\ValidatorTypeGuesser A Symfony\Component\Form\Extension\Validator\ValidatorTypeGuesser instance
     */
    protected function getForm_TypeGuesser_ValidatorService()
    {
        return $this->services['form.type_guesser.validator'] = new \Symfony\Component\Form\Extension\Validator\ValidatorTypeGuesser(${($_ = isset($this->services['validator']) ? $this->services['validator'] : $this->get('validator')) && false ?: '_'});
    }

    /**
     * Gets the 'monolog.processor.psr_log_message' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * This service is private.
     * If you want to be able to request this service from the container directly,
     * make it public, otherwise you might end up with broken code.
     *
     * @return \Monolog\Processor\PsrLogMessageProcessor A Monolog\Processor\PsrLogMessageProcessor instance
     */
    protected function getMonolog_Processor_PsrLogMessageService()
    {
        return $this->services['monolog.processor.psr_log_message'] = new \Monolog\Processor\PsrLogMessageProcessor();
    }

    /**
     * Gets the 'router.request_context' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * This service is private.
     * If you want to be able to request this service from the container directly,
     * make it public, otherwise you might end up with broken code.
     *
     * @return \Symfony\Component\Routing\RequestContext A Symfony\Component\Routing\RequestContext instance
     */
    protected function getRouter_RequestContextService()
    {
        return $this->services['router.request_context'] = new \Symfony\Component\Routing\RequestContext('', 'GET', 'localhost', 'http', 80, 443);
    }

    /**
     * Gets the 'security.access.authenticated_voter' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * This service is private.
     * If you want to be able to request this service from the container directly,
     * make it public, otherwise you might end up with broken code.
     *
     * @return \Symfony\Component\Security\Core\Authorization\Voter\AuthenticatedVoter A Symfony\Component\Security\Core\Authorization\Voter\AuthenticatedVoter instance
     */
    protected function getSecurity_Access_AuthenticatedVoterService()
    {
        return $this->services['security.access.authenticated_voter'] = new \Symfony\Component\Security\Core\Authorization\Voter\AuthenticatedVoter(${($_ = isset($this->services['security.authentication.trust_resolver']) ? $this->services['security.authentication.trust_resolver'] : $this->getSecurity_Authentication_TrustResolverService()) && false ?: '_'});
    }

    /**
     * Gets the 'security.access.expression_voter' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * This service is private.
     * If you want to be able to request this service from the container directly,
     * make it public, otherwise you might end up with broken code.
     *
     * @return \Symfony\Component\Security\Core\Authorization\Voter\ExpressionVoter A Symfony\Component\Security\Core\Authorization\Voter\ExpressionVoter instance
     */
    protected function getSecurity_Access_ExpressionVoterService()
    {
        return $this->services['security.access.expression_voter'] = new \Symfony\Component\Security\Core\Authorization\Voter\ExpressionVoter(new \Symfony\Component\Security\Core\Authorization\ExpressionLanguage(), ${($_ = isset($this->services['security.authentication.trust_resolver']) ? $this->services['security.authentication.trust_resolver'] : $this->getSecurity_Authentication_TrustResolverService()) && false ?: '_'}, ${($_ = isset($this->services['security.role_hierarchy']) ? $this->services['security.role_hierarchy'] : $this->getSecurity_RoleHierarchyService()) && false ?: '_'});
    }

    /**
     * Gets the 'security.access.role_hierarchy_voter' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * This service is private.
     * If you want to be able to request this service from the container directly,
     * make it public, otherwise you might end up with broken code.
     *
     * @return \Symfony\Component\Security\Core\Authorization\Voter\RoleHierarchyVoter A Symfony\Component\Security\Core\Authorization\Voter\RoleHierarchyVoter instance
     */
    protected function getSecurity_Access_RoleHierarchyVoterService()
    {
        return $this->services['security.access.role_hierarchy_voter'] = new \Symfony\Component\Security\Core\Authorization\Voter\RoleHierarchyVoter(${($_ = isset($this->services['security.role_hierarchy']) ? $this->services['security.role_hierarchy'] : $this->getSecurity_RoleHierarchyService()) && false ?: '_'});
    }

    /**
     * Gets the 'security.authentication.manager' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * This service is private.
     * If you want to be able to request this service from the container directly,
     * make it public, otherwise you might end up with broken code.
     *
     * @return \Symfony\Component\Security\Core\Authentication\AuthenticationProviderManager A Symfony\Component\Security\Core\Authentication\AuthenticationProviderManager instance
     */
    protected function getSecurity_Authentication_ManagerService()
    {
        $this->services['security.authentication.manager'] = $instance = new \Symfony\Component\Security\Core\Authentication\AuthenticationProviderManager(new RewindableGenerator(function () {
            yield 0 => ${($_ = isset($this->services['security.authentication.provider.guard.api']) ? $this->services['security.authentication.provider.guard.api'] : $this->getSecurity_Authentication_Provider_Guard_ApiService()) && false ?: '_'};
        }, 1), true);

        $instance->setEventDispatcher(${($_ = isset($this->services['debug.event_dispatcher']) ? $this->services['debug.event_dispatcher'] : $this->get('debug.event_dispatcher')) && false ?: '_'});

        return $instance;
    }

    /**
     * Gets the 'security.authentication.provider.guard.api' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * This service is private.
     * If you want to be able to request this service from the container directly,
     * make it public, otherwise you might end up with broken code.
     *
     * @return \Symfony\Component\Security\Guard\Provider\GuardAuthenticationProvider A Symfony\Component\Security\Guard\Provider\GuardAuthenticationProvider instance
     */
    protected function getSecurity_Authentication_Provider_Guard_ApiService()
    {
        return $this->services['security.authentication.provider.guard.api'] = new \Symfony\Component\Security\Guard\Provider\GuardAuthenticationProvider(new RewindableGenerator(function () {
            yield 0 => ${($_ = isset($this->services['api.security.jws_guard_authenticator']) ? $this->services['api.security.jws_guard_authenticator'] : $this->get('api.security.jws_guard_authenticator')) && false ?: '_'};
        }, 1), ${($_ = isset($this->services['api.security.user_provider']) ? $this->services['api.security.user_provider'] : $this->get('api.security.user_provider')) && false ?: '_'}, 'api', new \Symfony\Component\Security\Core\User\UserChecker());
    }

    /**
     * Gets the 'security.authentication.trust_resolver' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * This service is private.
     * If you want to be able to request this service from the container directly,
     * make it public, otherwise you might end up with broken code.
     *
     * @return \Symfony\Component\Security\Core\Authentication\AuthenticationTrustResolver A Symfony\Component\Security\Core\Authentication\AuthenticationTrustResolver instance
     */
    protected function getSecurity_Authentication_TrustResolverService()
    {
        return $this->services['security.authentication.trust_resolver'] = new \Symfony\Component\Security\Core\Authentication\AuthenticationTrustResolver('Symfony\\Component\\Security\\Core\\Authentication\\Token\\AnonymousToken', 'Symfony\\Component\\Security\\Core\\Authentication\\Token\\RememberMeToken');
    }

    /**
     * Gets the 'security.logout_url_generator' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * This service is private.
     * If you want to be able to request this service from the container directly,
     * make it public, otherwise you might end up with broken code.
     *
     * @return \Symfony\Component\Security\Http\Logout\LogoutUrlGenerator A Symfony\Component\Security\Http\Logout\LogoutUrlGenerator instance
     */
    protected function getSecurity_LogoutUrlGeneratorService()
    {
        return $this->services['security.logout_url_generator'] = new \Symfony\Component\Security\Http\Logout\LogoutUrlGenerator(${($_ = isset($this->services['request_stack']) ? $this->services['request_stack'] : $this->get('request_stack', ContainerInterface::NULL_ON_INVALID_REFERENCE)) && false ?: '_'}, ${($_ = isset($this->services['router']) ? $this->services['router'] : $this->get('router', ContainerInterface::NULL_ON_INVALID_REFERENCE)) && false ?: '_'}, ${($_ = isset($this->services['security.token_storage']) ? $this->services['security.token_storage'] : $this->get('security.token_storage', ContainerInterface::NULL_ON_INVALID_REFERENCE)) && false ?: '_'});
    }

    /**
     * Gets the 'security.request_matcher.5233d5f86c1e2076eac36f9cc15152586d6330c2cd3da54fdb761a92d2bb3b59b468e25d' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * This service is private.
     * If you want to be able to request this service from the container directly,
     * make it public, otherwise you might end up with broken code.
     *
     * @return \Symfony\Component\HttpFoundation\RequestMatcher A Symfony\Component\HttpFoundation\RequestMatcher instance
     */
    protected function getSecurity_RequestMatcher_5233d5f86c1e2076eac36f9cc15152586d6330c2cd3da54fdb761a92d2bb3b59b468e25dService()
    {
        return $this->services['security.request_matcher.5233d5f86c1e2076eac36f9cc15152586d6330c2cd3da54fdb761a92d2bb3b59b468e25d'] = new \Symfony\Component\HttpFoundation\RequestMatcher('/swagger/');
    }

    /**
     * Gets the 'security.request_matcher.5314eeb91110adf24b9b678372bb11bbe00e8858c519c088bfb65f525181ad3bf573fd1d' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * This service is private.
     * If you want to be able to request this service from the container directly,
     * make it public, otherwise you might end up with broken code.
     *
     * @return \Symfony\Component\HttpFoundation\RequestMatcher A Symfony\Component\HttpFoundation\RequestMatcher instance
     */
    protected function getSecurity_RequestMatcher_5314eeb91110adf24b9b678372bb11bbe00e8858c519c088bfb65f525181ad3bf573fd1dService()
    {
        return $this->services['security.request_matcher.5314eeb91110adf24b9b678372bb11bbe00e8858c519c088bfb65f525181ad3bf573fd1d'] = new \Symfony\Component\HttpFoundation\RequestMatcher('^/(_(profiler|wdt)|css|images|js)/');
    }

    /**
     * Gets the 'security.request_matcher.c9679edbbb26b9639a697ddca8184f648b310f5623db2680a8f22b3b5426f010053cae6b' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * This service is private.
     * If you want to be able to request this service from the container directly,
     * make it public, otherwise you might end up with broken code.
     *
     * @return \Symfony\Component\HttpFoundation\RequestMatcher A Symfony\Component\HttpFoundation\RequestMatcher instance
     */
    protected function getSecurity_RequestMatcher_C9679edbbb26b9639a697ddca8184f648b310f5623db2680a8f22b3b5426f010053cae6bService()
    {
        return $this->services['security.request_matcher.c9679edbbb26b9639a697ddca8184f648b310f5623db2680a8f22b3b5426f010053cae6b'] = new \Symfony\Component\HttpFoundation\RequestMatcher('^/v4');
    }

    /**
     * Gets the 'security.role_hierarchy' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * This service is private.
     * If you want to be able to request this service from the container directly,
     * make it public, otherwise you might end up with broken code.
     *
     * @return \Symfony\Component\Security\Core\Role\RoleHierarchy A Symfony\Component\Security\Core\Role\RoleHierarchy instance
     */
    protected function getSecurity_RoleHierarchyService()
    {
        return $this->services['security.role_hierarchy'] = new \Symfony\Component\Security\Core\Role\RoleHierarchy(array('admin_role' => array(0 => 'ADMIN'), 'tysa_role' => array(0 => 'CINEMA_VIEW', 1 => 'CITY_VIEW', 2 => 'CUSTOMER_VIEW', 3 => 'CUSTOMER_LOGIN', 4 => 'DISTRIBUTOR_VIEW', 5 => 'GENRE_VIEW', 6 => 'HALL_VIEW', 7 => 'MOVIE_VIEW', 8 => 'ORDER_VIEW', 9 => 'ORDER_CREATE', 10 => 'ORDER_UPDATE', 11 => 'SEAT_VIEW', 12 => 'SHOW_VIEW'), 'vlru_role' => array(0 => 'CINEMA_VIEW', 1 => 'CITY_VIEW', 2 => 'CUSTOMER_VIEW', 3 => 'CUSTOMER_LOGIN', 4 => 'DISTRIBUTOR_VIEW', 5 => 'GENRE_VIEW', 6 => 'HALL_VIEW', 7 => 'MOVIE_VIEW', 8 => 'ORDER_VIEW', 9 => 'ORDER_CREATE', 10 => 'ORDER_UPDATE', 11 => 'SEAT_VIEW', 12 => 'SHOW_VIEW'), 'illru_role' => array(0 => 'CINEMA_VIEW', 1 => 'CITY_VIEW', 2 => 'CUSTOMER_VIEW', 3 => 'CUSTOMER_LOGIN', 4 => 'DISTRIBUTOR_VIEW', 5 => 'GENRE_VIEW', 6 => 'HALL_VIEW', 7 => 'MOVIE_VIEW', 8 => 'ORDER_VIEW', 9 => 'ORDER_CREATE', 10 => 'ORDER_UPDATE', 11 => 'SEAT_VIEW', 12 => 'SHOW_VIEW'), 'erp_role' => array(0 => 'GENRE_VIEW', 1 => 'MOVIE_ALL', 2 => 'DISTRIBUTOR_ALL', 3 => 'PROGRAM_ALL', 4 => 'NEWS_ALL', 5 => 'CITY_VIEW'), 'rpi_role' => array(0 => 'CINEMA_VIEW', 1 => 'MOVIE_VIEW', 2 => 'SHOW_VIEW', 3 => 'GENRE_VIEW', 4 => 'HALL_VIEW', 5 => 'CITY_VIEW', 6 => 'PROGRAM_VIEW', 7 => 'DISTRIBUTOR_VIEW'), 'ADMIN' => array(0 => 'CINEMA_ALL', 1 => 'CITY_ALL', 2 => 'CUSTOMER_ALL', 3 => 'DISTRIBUTOR_ALL', 4 => 'GENRE_ALL', 5 => 'HALL_ALL', 6 => 'MOVIE_ALL', 7 => 'NEWS_ALL', 8 => 'ORDER_ALL', 9 => 'PROGRAM_ALL', 10 => 'SEAT_ALL', 11 => 'SHOW_ALL'), 'CINEMA_ALL' => array(0 => 'CINEMA_VIEW'), 'CITY_ALL' => array(0 => 'CITY_VIEW'), 'CUSTOMER_ALL' => array(0 => 'CUSTOMER_VIEW', 1 => 'CUSTOMER_LOGIN', 2 => 'CUSTOMER_UPDATE'), 'DISTRIBUTOR_ALL' => array(0 => 'DISTRIBUTOR_VIEW'), 'GENRE_ALL' => array(0 => 'GENRE_VIEW'), 'HALL_ALL' => array(0 => 'HALL_VIEW'), 'MOVIE_ALL' => array(0 => 'MOVIE_VIEW', 1 => 'MOVIE_CREATE', 2 => 'MOVIE_UPDATE', 3 => 'MOVIE_DELETE'), 'NEWS_ALL' => array(0 => 'NEWS_VIEW', 1 => 'NEWS_CREATE', 2 => 'NEWS_UPDATE', 3 => 'NEWS_DELETE'), 'ORDER_ALL' => array(0 => 'ORDER_VIEW', 1 => 'ORDER_CREATE', 2 => 'ORDER_UPDATE'), 'PROGRAM_ALL' => array(0 => 'PROGRAM_VIEW', 1 => 'PROGRAM_CREATE', 2 => 'PROGRAM_UPDATE', 3 => 'PROGRAM_DELETE'), 'SEAT_ALL' => array(0 => 'SEAT_VIEW'), 'SHOW_ALL' => array(0 => 'SHOW_VIEW')));
    }

    /**
     * Gets the 'security.user_value_resolver' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * This service is private.
     * If you want to be able to request this service from the container directly,
     * make it public, otherwise you might end up with broken code.
     *
     * @return \Symfony\Bundle\SecurityBundle\SecurityUserValueResolver A Symfony\Bundle\SecurityBundle\SecurityUserValueResolver instance
     */
    protected function getSecurity_UserValueResolverService()
    {
        return $this->services['security.user_value_resolver'] = new \Symfony\Bundle\SecurityBundle\SecurityUserValueResolver(${($_ = isset($this->services['security.token_storage']) ? $this->services['security.token_storage'] : $this->get('security.token_storage')) && false ?: '_'});
    }

    /**
     * Gets the 'service_locator.f3e4af76f32830cd048a9a0d4662e709' service.
     *
     * This service is shared.
     * This method always returns the same instance of the service.
     *
     * This service is private.
     * If you want to be able to request this service from the container directly,
     * make it public, otherwise you might end up with broken code.
     *
     * @return \Symfony\Component\DependencyInjection\ServiceLocator A Symfony\Component\DependencyInjection\ServiceLocator instance
     */
    protected function getServiceLocator_F3e4af76f32830cd048a9a0d4662e709Service()
    {
        return $this->services['service_locator.f3e4af76f32830cd048a9a0d4662e709'] = new \Symfony\Component\DependencyInjection\ServiceLocator(array('esi' => function () {
            return ${($_ = isset($this->services['fragment.renderer.esi']) ? $this->services['fragment.renderer.esi'] : $this->get('fragment.renderer.esi')) && false ?: '_'};
        }, 'inline' => function () {
            return ${($_ = isset($this->services['fragment.renderer.inline']) ? $this->services['fragment.renderer.inline'] : $this->get('fragment.renderer.inline')) && false ?: '_'};
        }, 'ssi' => function () {
            return ${($_ = isset($this->services['fragment.renderer.ssi']) ? $this->services['fragment.renderer.ssi'] : $this->get('fragment.renderer.ssi')) && false ?: '_'};
        }));
    }

    /**
     * {@inheritdoc}
     */
    public function getParameter($name)
    {
        $name = strtolower($name);

        if (!(isset($this->parameters[$name]) || array_key_exists($name, $this->parameters) || isset($this->loadedDynamicParameters[$name]))) {
            throw new InvalidArgumentException(sprintf('The parameter "%s" must be defined.', $name));
        }
        if (isset($this->loadedDynamicParameters[$name])) {
            return $this->loadedDynamicParameters[$name] ? $this->dynamicParameters[$name] : $this->getDynamicParameter($name);
        }

        return $this->parameters[$name];
    }

    /**
     * {@inheritdoc}
     */
    public function hasParameter($name)
    {
        $name = strtolower($name);

        return isset($this->parameters[$name]) || array_key_exists($name, $this->parameters) || isset($this->loadedDynamicParameters[$name]);
    }

    /**
     * {@inheritdoc}
     */
    public function setParameter($name, $value)
    {
        throw new LogicException('Impossible to call set() on a frozen ParameterBag.');
    }

    /**
     * {@inheritdoc}
     */
    public function getParameterBag()
    {
        if (null === $this->parameterBag) {
            $parameters = $this->parameters;
            foreach ($this->loadedDynamicParameters as $name => $loaded) {
                $parameters[$name] = $loaded ? $this->dynamicParameters[$name] : $this->getDynamicParameter($name);
            }
            $this->parameterBag = new FrozenParameterBag($parameters);
        }

        return $this->parameterBag;
    }

    private $loadedDynamicParameters = array(
        'kernel.root_dir' => false,
        'kernel.project_dir' => false,
        'kernel.logs_dir' => false,
        'kernel.bundles_metadata' => false,
        'router.resource' => false,
        'doctrine_migrations.dir_name' => false,
    );
    private $dynamicParameters = array();

    /**
     * Computes a dynamic parameter.
     *
     * @param string The name of the dynamic parameter to load
     *
     * @return mixed The value of the dynamic parameter
     *
     * @throws InvalidArgumentException When the dynamic parameter does not exist
     */
    private function getDynamicParameter($name)
    {
        switch ($name) {
            case 'kernel.root_dir': $value = ($this->targetDirs[3].'/app'); break;
            case 'kernel.project_dir': $value = $this->targetDirs[3]; break;
            case 'kernel.logs_dir': $value = ($this->targetDirs[5].'/logs'); break;
            case 'kernel.bundles_metadata': $value = array(
                'FrameworkBundle' => array(
                    'parent' => NULL,
                    'path' => ($this->targetDirs[3].'/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle'),
                    'namespace' => 'Symfony\\Bundle\\FrameworkBundle',
                ),
                'SecurityBundle' => array(
                    'parent' => NULL,
                    'path' => ($this->targetDirs[3].'/vendor/symfony/symfony/src/Symfony/Bundle/SecurityBundle'),
                    'namespace' => 'Symfony\\Bundle\\SecurityBundle',
                ),
                'MonologBundle' => array(
                    'parent' => NULL,
                    'path' => ($this->targetDirs[3].'/vendor/symfony/monolog-bundle'),
                    'namespace' => 'Symfony\\Bundle\\MonologBundle',
                ),
                'DoctrineBundle' => array(
                    'parent' => NULL,
                    'path' => ($this->targetDirs[3].'/vendor/doctrine/doctrine-bundle'),
                    'namespace' => 'Doctrine\\Bundle\\DoctrineBundle',
                ),
                'SensioFrameworkExtraBundle' => array(
                    'parent' => NULL,
                    'path' => ($this->targetDirs[3].'/vendor/sensio/framework-extra-bundle'),
                    'namespace' => 'Sensio\\Bundle\\FrameworkExtraBundle',
                ),
                'DoctrineMigrationsBundle' => array(
                    'parent' => NULL,
                    'path' => ($this->targetDirs[3].'/vendor/doctrine/doctrine-migrations-bundle'),
                    'namespace' => 'Doctrine\\Bundle\\MigrationsBundle',
                ),
                'SwiftmailerBundle' => array(
                    'parent' => NULL,
                    'path' => ($this->targetDirs[3].'/vendor/symfony/swiftmailer-bundle'),
                    'namespace' => 'Symfony\\Bundle\\SwiftmailerBundle',
                ),
                'SonataNotificationBundle' => array(
                    'parent' => NULL,
                    'path' => ($this->targetDirs[3].'/vendor/sonata-project/notification-bundle'),
                    'namespace' => 'Sonata\\NotificationBundle',
                ),
                'ApiBundle' => array(
                    'parent' => NULL,
                    'path' => ($this->targetDirs[3].'/src/Illuzion/ApiBundle'),
                    'namespace' => 'Illuzion\\ApiBundle',
                ),
                'SynchronizationBundle' => array(
                    'parent' => NULL,
                    'path' => ($this->targetDirs[3].'/src/Illuzion/SynchronizationBundle'),
                    'namespace' => 'Illuzion\\SynchronizationBundle',
                ),
            ); break;
            case 'router.resource': $value = ($this->targetDirs[3].'/app/config/routing.yml'); break;
            case 'doctrine_migrations.dir_name': $value = ($this->targetDirs[3].'/app/DoctrineMigrations'); break;
            default: throw new InvalidArgumentException(sprintf('The dynamic parameter "%s" must be defined.', $name));
        }
        $this->loadedDynamicParameters[$name] = true;

        return $this->dynamicParameters[$name] = $value;
    }

    /**
     * Gets the default parameters.
     *
     * @return array An array of the default parameters
     */
    protected function getDefaultParameters()
    {
        return array(
            'kernel.environment' => 'sync',
            'kernel.debug' => true,
            'kernel.name' => 'app',
            'kernel.cache_dir' => __DIR__,
            'kernel.bundles' => array(
                'FrameworkBundle' => 'Symfony\\Bundle\\FrameworkBundle\\FrameworkBundle',
                'SecurityBundle' => 'Symfony\\Bundle\\SecurityBundle\\SecurityBundle',
                'MonologBundle' => 'Symfony\\Bundle\\MonologBundle\\MonologBundle',
                'DoctrineBundle' => 'Doctrine\\Bundle\\DoctrineBundle\\DoctrineBundle',
                'SensioFrameworkExtraBundle' => 'Sensio\\Bundle\\FrameworkExtraBundle\\SensioFrameworkExtraBundle',
                'DoctrineMigrationsBundle' => 'Doctrine\\Bundle\\MigrationsBundle\\DoctrineMigrationsBundle',
                'SwiftmailerBundle' => 'Symfony\\Bundle\\SwiftmailerBundle\\SwiftmailerBundle',
                'SonataNotificationBundle' => 'Sonata\\NotificationBundle\\SonataNotificationBundle',
                'ApiBundle' => 'Illuzion\\ApiBundle\\ApiBundle',
                'SynchronizationBundle' => 'Illuzion\\SynchronizationBundle\\SynchronizationBundle',
            ),
            'kernel.charset' => 'UTF-8',
            'kernel.container_class' => 'appSyncDebugProjectContainer',
            'security.application_users' => array(
                'base' => array(
                    'roles' => array(
                        0 => 'admin_role',
                    ),
                ),
                'tysa' => array(
                    'roles' => array(
                        0 => 'tysa_role',
                    ),
                ),
                'vlru' => array(
                    'roles' => array(
                        0 => 'vlru_role',
                    ),
                ),
                'rpi' => array(
                    'roles' => array(
                        0 => 'rpi_role',
                    ),
                ),
                'erp' => array(
                    'roles' => array(
                        0 => 'admin_role',
                    ),
                ),
                'illru' => array(
                    'roles' => array(
                        0 => 'admin_role',
                    ),
                ),
            ),
            'jws_encoder.algo' => 'HS512',
            'jws_encoder.keychain' => array(
                'key' => 'da08e547r7319955bg3d7465866619dfd0fbe478',
            ),
            'doctrine.supported_cinemas' => array(
                0 => 'uss',
                1 => 'oc',
                2 => 'ill',
                3 => 'che',
                4 => 'nmega',
            ),
            'doctrine.ibase_options' => array(
                'doctrineTransactionWait' => -1,
                'doctrineTransactionIsolationLevel' => 3,
            ),
            'doctrine.ibase_dql' => array(
                'numeric_functions' => array(
                    'bitand' => 'Illuzion\\Common\\Doctrine\\BitAndFunction',
                    'rupper' => 'Illuzion\\Common\\Doctrine\\RupperFunction',
                ),
            ),
            'doctrine.ibase.result_cache_driver' => array(
                'type' => 'void',
            ),
            'doctrine.ibase.main.mappings' => array(
                'ApiBundle' => array(
                    'prefix' => 'Illuzion\\ApiBundle\\Entity\\Main',
                    'alias' => 'Main',
                ),
            ),
            'doctrine.ibase.customer.mappings' => array(
                'ApiBundle' => array(
                    'prefix' => 'Illuzion\\ApiBundle\\Entity\\Customer',
                    'alias' => 'Customer',
                ),
            ),
            'api_prefix' => 'http://ussuri.illuzion.loc:7023',
            'url_prefix' => '/v4',
            'secret' => 'da08e547r7319955bg3d7465866619dfd0fbe478',
            'default_sales_close_time' => '30 minutes',
            'db_ussuri_main_host' => '192.168.22.90',
            'db_ussuri_main_port' => NULL,
            'db_ussuri_main_name' => 'c:\\kassa\\uss_db.gdb',
            'db_ussuri_main_user' => 'internet',
            'db_ussuri_main_password' => 12345678,
            'db_ussuri_customer_host' => '192.168.22.90',
            'db_ussuri_customer_port' => NULL,
            'db_ussuri_customer_name' => 'c:\\kassa\\people_cinema.gdb',
            'db_ussuri_customer_user' => 'internet',
            'db_ussuri_customer_password' => 12345678,
            'db_okean_main_host' => '192.168.21.59',
            'db_okean_main_port' => NULL,
            'db_okean_main_name' => 'c:\\cinema\\db\\ocean_db.gdb',
            'db_okean_main_user' => 'internet',
            'db_okean_main_password' => 12345678,
            'db_okean_customer_host' => '192.168.21.59',
            'db_okean_customer_port' => NULL,
            'db_okean_customer_name' => 'c:\\cinema\\db\\people_cinema.gdb',
            'db_okean_customer_user' => 'internet',
            'db_okean_customer_password' => 12345678,
            'db_illuzion_main_host' => '192.168.20.1',
            'db_illuzion_main_port' => NULL,
            'db_illuzion_main_name' => 'c:\\illuzion\\db\\illusion_db.gdb',
            'db_illuzion_main_user' => 'internet',
            'db_illuzion_main_password' => 12345678,
            'db_illuzion_customer_host' => '192.168.20.1',
            'db_illuzion_customer_port' => NULL,
            'db_illuzion_customer_name' => 'c:\\illuzion\\db\\people_cinema.gdb',
            'db_illuzion_customer_user' => 'internet',
            'db_illuzion_customer_password' => 12345678,
            'db_cheremushki_main_host' => '192.168.26.4',
            'db_cheremushki_main_port' => NULL,
            'db_cheremushki_main_name' => 'c:\\cher\\cher_db.gdb',
            'db_cheremushki_main_user' => 'internet',
            'db_cheremushki_main_password' => 12345678,
            'db_cheremushki_customer_host' => '192.168.26.4',
            'db_cheremushki_customer_port' => NULL,
            'db_cheremushki_customer_name' => 'c:\\cher\\people_cinema.gdb',
            'db_cheremushki_customer_user' => 'internet',
            'db_cheremushki_customer_password' => 12345678,
            'db_nmega_main_host' => '192.168.40.8',
            'db_nmega_main_port' => NULL,
            'db_nmega_main_name' => 'c:\\nmega\\nmega_db.gdb',
            'db_nmega_main_user' => 'internet',
            'db_nmega_main_password' => 12345678,
            'db_nmega_customer_host' => '192.168.40.8',
            'db_nmega_customer_port' => NULL,
            'db_nmega_customer_name' => 'c:\\nmega\\people_cinema.gdb',
            'db_nmega_customer_user' => 'internet',
            'db_nmega_customer_password' => 12345678,
            'db_shared_host' => 'localhost',
            'db_shared_port' => NULL,
            'db_shared_name' => 'api_production',
            'db_shared_user' => 'api',
            'db_shared_password' => '1990api',
            'emailer.host' => 'http://service.illuzion.ru/mailer/web/',
            'emailer.token' => 19901990,
            'emailer.confirmation_link_pattern' => 'http://illuzion.ru/customer-activate/__hash__',
            'locale' => 'ru',
            'fragment.renderer.hinclude.global_template' => '',
            'fragment.path' => '/_fragment',
            'kernel.secret' => 'da08e547r7319955bg3d7465866619dfd0fbe478',
            'kernel.http_method_override' => true,
            'kernel.trusted_hosts' => array(

            ),
            'kernel.default_locale' => 'ru',
            'templating.helper.code.file_link_format' => NULL,
            'debug.file_link_format' => NULL,
            'form.type_extension.csrf.enabled' => false,
            'validator.mapping.cache.prefix' => '',
            'validator.mapping.cache.file' => (__DIR__.'/validation.php'),
            'validator.translation_domain' => 'validators',
            'data_collector.templates' => array(

            ),
            'debug.error_handler.throw_at' => -1,
            'debug.container.dump' => (__DIR__.'/appSyncDebugProjectContainer.xml'),
            'router.options.generator_class' => 'Symfony\\Component\\Routing\\Generator\\UrlGenerator',
            'router.options.generator_base_class' => 'Symfony\\Component\\Routing\\Generator\\UrlGenerator',
            'router.options.generator_dumper_class' => 'Symfony\\Component\\Routing\\Generator\\Dumper\\PhpGeneratorDumper',
            'router.options.matcher_class' => 'Symfony\\Bundle\\FrameworkBundle\\Routing\\RedirectableUrlMatcher',
            'router.options.matcher_base_class' => 'Symfony\\Bundle\\FrameworkBundle\\Routing\\RedirectableUrlMatcher',
            'router.options.matcher_dumper_class' => 'Symfony\\Component\\Routing\\Matcher\\Dumper\\PhpMatcherDumper',
            'router.options.matcher.cache_class' => 'appSyncDebugProjectContainerUrlMatcher',
            'router.options.generator.cache_class' => 'appSyncDebugProjectContainerUrlGenerator',
            'router.request_context.host' => 'localhost',
            'router.request_context.scheme' => 'http',
            'router.request_context.base_url' => '',
            'router.cache_class_prefix' => 'appSyncDebugProjectContainer',
            'request_listener.http_port' => 80,
            'request_listener.https_port' => 443,
            'security.authentication.trust_resolver.anonymous_class' => 'Symfony\\Component\\Security\\Core\\Authentication\\Token\\AnonymousToken',
            'security.authentication.trust_resolver.rememberme_class' => 'Symfony\\Component\\Security\\Core\\Authentication\\Token\\RememberMeToken',
            'security.role_hierarchy.roles' => array(
                'admin_role' => array(
                    0 => 'ADMIN',
                ),
                'tysa_role' => array(
                    0 => 'CINEMA_VIEW',
                    1 => 'CITY_VIEW',
                    2 => 'CUSTOMER_VIEW',
                    3 => 'CUSTOMER_LOGIN',
                    4 => 'DISTRIBUTOR_VIEW',
                    5 => 'GENRE_VIEW',
                    6 => 'HALL_VIEW',
                    7 => 'MOVIE_VIEW',
                    8 => 'ORDER_VIEW',
                    9 => 'ORDER_CREATE',
                    10 => 'ORDER_UPDATE',
                    11 => 'SEAT_VIEW',
                    12 => 'SHOW_VIEW',
                ),
                'vlru_role' => array(
                    0 => 'CINEMA_VIEW',
                    1 => 'CITY_VIEW',
                    2 => 'CUSTOMER_VIEW',
                    3 => 'CUSTOMER_LOGIN',
                    4 => 'DISTRIBUTOR_VIEW',
                    5 => 'GENRE_VIEW',
                    6 => 'HALL_VIEW',
                    7 => 'MOVIE_VIEW',
                    8 => 'ORDER_VIEW',
                    9 => 'ORDER_CREATE',
                    10 => 'ORDER_UPDATE',
                    11 => 'SEAT_VIEW',
                    12 => 'SHOW_VIEW',
                ),
                'illru_role' => array(
                    0 => 'CINEMA_VIEW',
                    1 => 'CITY_VIEW',
                    2 => 'CUSTOMER_VIEW',
                    3 => 'CUSTOMER_LOGIN',
                    4 => 'DISTRIBUTOR_VIEW',
                    5 => 'GENRE_VIEW',
                    6 => 'HALL_VIEW',
                    7 => 'MOVIE_VIEW',
                    8 => 'ORDER_VIEW',
                    9 => 'ORDER_CREATE',
                    10 => 'ORDER_UPDATE',
                    11 => 'SEAT_VIEW',
                    12 => 'SHOW_VIEW',
                ),
                'erp_role' => array(
                    0 => 'GENRE_VIEW',
                    1 => 'MOVIE_ALL',
                    2 => 'DISTRIBUTOR_ALL',
                    3 => 'PROGRAM_ALL',
                    4 => 'NEWS_ALL',
                    5 => 'CITY_VIEW',
                ),
                'rpi_role' => array(
                    0 => 'CINEMA_VIEW',
                    1 => 'MOVIE_VIEW',
                    2 => 'SHOW_VIEW',
                    3 => 'GENRE_VIEW',
                    4 => 'HALL_VIEW',
                    5 => 'CITY_VIEW',
                    6 => 'PROGRAM_VIEW',
                    7 => 'DISTRIBUTOR_VIEW',
                ),
                'ADMIN' => array(
                    0 => 'CINEMA_ALL',
                    1 => 'CITY_ALL',
                    2 => 'CUSTOMER_ALL',
                    3 => 'DISTRIBUTOR_ALL',
                    4 => 'GENRE_ALL',
                    5 => 'HALL_ALL',
                    6 => 'MOVIE_ALL',
                    7 => 'NEWS_ALL',
                    8 => 'ORDER_ALL',
                    9 => 'PROGRAM_ALL',
                    10 => 'SEAT_ALL',
                    11 => 'SHOW_ALL',
                ),
                'CINEMA_ALL' => array(
                    0 => 'CINEMA_VIEW',
                ),
                'CITY_ALL' => array(
                    0 => 'CITY_VIEW',
                ),
                'CUSTOMER_ALL' => array(
                    0 => 'CUSTOMER_VIEW',
                    1 => 'CUSTOMER_LOGIN',
                    2 => 'CUSTOMER_UPDATE',
                ),
                'DISTRIBUTOR_ALL' => array(
                    0 => 'DISTRIBUTOR_VIEW',
                ),
                'GENRE_ALL' => array(
                    0 => 'GENRE_VIEW',
                ),
                'HALL_ALL' => array(
                    0 => 'HALL_VIEW',
                ),
                'MOVIE_ALL' => array(
                    0 => 'MOVIE_VIEW',
                    1 => 'MOVIE_CREATE',
                    2 => 'MOVIE_UPDATE',
                    3 => 'MOVIE_DELETE',
                ),
                'NEWS_ALL' => array(
                    0 => 'NEWS_VIEW',
                    1 => 'NEWS_CREATE',
                    2 => 'NEWS_UPDATE',
                    3 => 'NEWS_DELETE',
                ),
                'ORDER_ALL' => array(
                    0 => 'ORDER_VIEW',
                    1 => 'ORDER_CREATE',
                    2 => 'ORDER_UPDATE',
                ),
                'PROGRAM_ALL' => array(
                    0 => 'PROGRAM_VIEW',
                    1 => 'PROGRAM_CREATE',
                    2 => 'PROGRAM_UPDATE',
                    3 => 'PROGRAM_DELETE',
                ),
                'SEAT_ALL' => array(
                    0 => 'SEAT_VIEW',
                ),
                'SHOW_ALL' => array(
                    0 => 'SHOW_VIEW',
                ),
            ),
            'security.access.denied_url' => NULL,
            'security.authentication.manager.erase_credentials' => true,
            'security.authentication.session_strategy.strategy' => 'migrate',
            'security.access.always_authenticate_before_granting' => false,
            'security.authentication.hide_user_not_found' => true,
            'monolog.logger.class' => 'Symfony\\Bridge\\Monolog\\Logger',
            'monolog.gelf.publisher.class' => 'Gelf\\MessagePublisher',
            'monolog.gelfphp.publisher.class' => 'Gelf\\Publisher',
            'monolog.handler.stream.class' => 'Monolog\\Handler\\StreamHandler',
            'monolog.handler.console.class' => 'Symfony\\Bridge\\Monolog\\Handler\\ConsoleHandler',
            'monolog.handler.group.class' => 'Monolog\\Handler\\GroupHandler',
            'monolog.handler.buffer.class' => 'Monolog\\Handler\\BufferHandler',
            'monolog.handler.deduplication.class' => 'Monolog\\Handler\\DeduplicationHandler',
            'monolog.handler.rotating_file.class' => 'Monolog\\Handler\\RotatingFileHandler',
            'monolog.handler.syslog.class' => 'Monolog\\Handler\\SyslogHandler',
            'monolog.handler.syslogudp.class' => 'Monolog\\Handler\\SyslogUdpHandler',
            'monolog.handler.null.class' => 'Monolog\\Handler\\NullHandler',
            'monolog.handler.test.class' => 'Monolog\\Handler\\TestHandler',
            'monolog.handler.gelf.class' => 'Monolog\\Handler\\GelfHandler',
            'monolog.handler.rollbar.class' => 'Monolog\\Handler\\RollbarHandler',
            'monolog.handler.flowdock.class' => 'Monolog\\Handler\\FlowdockHandler',
            'monolog.handler.browser_console.class' => 'Monolog\\Handler\\BrowserConsoleHandler',
            'monolog.handler.firephp.class' => 'Symfony\\Bridge\\Monolog\\Handler\\FirePHPHandler',
            'monolog.handler.chromephp.class' => 'Symfony\\Bridge\\Monolog\\Handler\\ChromePhpHandler',
            'monolog.handler.debug.class' => 'Symfony\\Bridge\\Monolog\\Handler\\DebugHandler',
            'monolog.handler.swift_mailer.class' => 'Symfony\\Bridge\\Monolog\\Handler\\SwiftMailerHandler',
            'monolog.handler.native_mailer.class' => 'Monolog\\Handler\\NativeMailerHandler',
            'monolog.handler.socket.class' => 'Monolog\\Handler\\SocketHandler',
            'monolog.handler.pushover.class' => 'Monolog\\Handler\\PushoverHandler',
            'monolog.handler.raven.class' => 'Monolog\\Handler\\RavenHandler',
            'monolog.handler.newrelic.class' => 'Monolog\\Handler\\NewRelicHandler',
            'monolog.handler.hipchat.class' => 'Monolog\\Handler\\HipChatHandler',
            'monolog.handler.slack.class' => 'Monolog\\Handler\\SlackHandler',
            'monolog.handler.cube.class' => 'Monolog\\Handler\\CubeHandler',
            'monolog.handler.amqp.class' => 'Monolog\\Handler\\AmqpHandler',
            'monolog.handler.error_log.class' => 'Monolog\\Handler\\ErrorLogHandler',
            'monolog.handler.loggly.class' => 'Monolog\\Handler\\LogglyHandler',
            'monolog.handler.logentries.class' => 'Monolog\\Handler\\LogEntriesHandler',
            'monolog.handler.whatfailuregroup.class' => 'Monolog\\Handler\\WhatFailureGroupHandler',
            'monolog.activation_strategy.not_found.class' => 'Symfony\\Bundle\\MonologBundle\\NotFoundActivationStrategy',
            'monolog.handler.fingers_crossed.class' => 'Monolog\\Handler\\FingersCrossedHandler',
            'monolog.handler.fingers_crossed.error_level_activation_strategy.class' => 'Monolog\\Handler\\FingersCrossed\\ErrorLevelActivationStrategy',
            'monolog.handler.filter.class' => 'Monolog\\Handler\\FilterHandler',
            'monolog.handler.mongo.class' => 'Monolog\\Handler\\MongoDBHandler',
            'monolog.mongo.client.class' => 'MongoClient',
            'monolog.handler.elasticsearch.class' => 'Monolog\\Handler\\ElasticSearchHandler',
            'monolog.elastica.client.class' => 'Elastica\\Client',
            'monolog.use_microseconds' => true,
            'monolog.swift_mailer.handlers' => array(

            ),
            'monolog.handlers_to_channels' => array(
                'monolog.handler.main' => NULL,
            ),
            'doctrine_cache.apc.class' => 'Doctrine\\Common\\Cache\\ApcCache',
            'doctrine_cache.apcu.class' => 'Doctrine\\Common\\Cache\\ApcuCache',
            'doctrine_cache.array.class' => 'Doctrine\\Common\\Cache\\ArrayCache',
            'doctrine_cache.chain.class' => 'Doctrine\\Common\\Cache\\ChainCache',
            'doctrine_cache.couchbase.class' => 'Doctrine\\Common\\Cache\\CouchbaseCache',
            'doctrine_cache.couchbase.connection.class' => 'Couchbase',
            'doctrine_cache.couchbase.hostnames' => 'localhost:8091',
            'doctrine_cache.file_system.class' => 'Doctrine\\Common\\Cache\\FilesystemCache',
            'doctrine_cache.php_file.class' => 'Doctrine\\Common\\Cache\\PhpFileCache',
            'doctrine_cache.memcache.class' => 'Doctrine\\Common\\Cache\\MemcacheCache',
            'doctrine_cache.memcache.connection.class' => 'Memcache',
            'doctrine_cache.memcache.host' => 'localhost',
            'doctrine_cache.memcache.port' => 11211,
            'doctrine_cache.memcached.class' => 'Doctrine\\Common\\Cache\\MemcachedCache',
            'doctrine_cache.memcached.connection.class' => 'Memcached',
            'doctrine_cache.memcached.host' => 'localhost',
            'doctrine_cache.memcached.port' => 11211,
            'doctrine_cache.mongodb.class' => 'Doctrine\\Common\\Cache\\MongoDBCache',
            'doctrine_cache.mongodb.collection.class' => 'MongoCollection',
            'doctrine_cache.mongodb.connection.class' => 'MongoClient',
            'doctrine_cache.mongodb.server' => 'localhost:27017',
            'doctrine_cache.predis.client.class' => 'Predis\\Client',
            'doctrine_cache.predis.scheme' => 'tcp',
            'doctrine_cache.predis.host' => 'localhost',
            'doctrine_cache.predis.port' => 6379,
            'doctrine_cache.redis.class' => 'Doctrine\\Common\\Cache\\RedisCache',
            'doctrine_cache.redis.connection.class' => 'Redis',
            'doctrine_cache.redis.host' => 'localhost',
            'doctrine_cache.redis.port' => 6379,
            'doctrine_cache.riak.class' => 'Doctrine\\Common\\Cache\\RiakCache',
            'doctrine_cache.riak.bucket.class' => 'Riak\\Bucket',
            'doctrine_cache.riak.connection.class' => 'Riak\\Connection',
            'doctrine_cache.riak.bucket_property_list.class' => 'Riak\\BucketPropertyList',
            'doctrine_cache.riak.host' => 'localhost',
            'doctrine_cache.riak.port' => 8087,
            'doctrine_cache.sqlite3.class' => 'Doctrine\\Common\\Cache\\SQLite3Cache',
            'doctrine_cache.sqlite3.connection.class' => 'SQLite3',
            'doctrine_cache.void.class' => 'Doctrine\\Common\\Cache\\VoidCache',
            'doctrine_cache.wincache.class' => 'Doctrine\\Common\\Cache\\WinCacheCache',
            'doctrine_cache.xcache.class' => 'Doctrine\\Common\\Cache\\XcacheCache',
            'doctrine_cache.zenddata.class' => 'Doctrine\\Common\\Cache\\ZendDataCache',
            'doctrine_cache.security.acl.cache.class' => 'Doctrine\\Bundle\\DoctrineCacheBundle\\Acl\\Model\\AclCache',
            'doctrine.dbal.logger.chain.class' => 'Doctrine\\DBAL\\Logging\\LoggerChain',
            'doctrine.dbal.logger.profiling.class' => 'Doctrine\\DBAL\\Logging\\DebugStack',
            'doctrine.dbal.logger.class' => 'Symfony\\Bridge\\Doctrine\\Logger\\DbalLogger',
            'doctrine.dbal.configuration.class' => 'Doctrine\\DBAL\\Configuration',
            'doctrine.data_collector.class' => 'Doctrine\\Bundle\\DoctrineBundle\\DataCollector\\DoctrineDataCollector',
            'doctrine.dbal.connection.event_manager.class' => 'Symfony\\Bridge\\Doctrine\\ContainerAwareEventManager',
            'doctrine.dbal.connection_factory.class' => 'Doctrine\\Bundle\\DoctrineBundle\\ConnectionFactory',
            'doctrine.dbal.events.mysql_session_init.class' => 'Doctrine\\DBAL\\Event\\Listeners\\MysqlSessionInit',
            'doctrine.dbal.events.oracle_session_init.class' => 'Doctrine\\DBAL\\Event\\Listeners\\OracleSessionInit',
            'doctrine.class' => 'Doctrine\\Bundle\\DoctrineBundle\\Registry',
            'doctrine.entity_managers' => array(
                'shared' => 'doctrine.orm.shared_entity_manager',
                'uss_main' => 'doctrine.orm.uss_main_entity_manager',
                'uss_customer' => 'doctrine.orm.uss_customer_entity_manager',
                'oc_main' => 'doctrine.orm.oc_main_entity_manager',
                'oc_customer' => 'doctrine.orm.oc_customer_entity_manager',
                'ill_main' => 'doctrine.orm.ill_main_entity_manager',
                'ill_customer' => 'doctrine.orm.ill_customer_entity_manager',
                'che_main' => 'doctrine.orm.che_main_entity_manager',
                'che_customer' => 'doctrine.orm.che_customer_entity_manager',
                'nmega_main' => 'doctrine.orm.nmega_main_entity_manager',
                'nmega_customer' => 'doctrine.orm.nmega_customer_entity_manager',
            ),
            'doctrine.default_entity_manager' => 'shared',
            'doctrine.dbal.connection_factory.types' => array(
                'string_win1251' => array(
                    'class' => 'Illuzion\\Common\\Doctrine\\Windows1251String',
                    'commented' => true,
                ),
                'text_win1251' => array(
                    'class' => 'Illuzion\\Common\\Doctrine\\Windows1251Text',
                    'commented' => true,
                ),
                'movie_format' => array(
                    'class' => 'Illuzion\\Common\\Doctrine\\MovieFormat',
                    'commented' => true,
                ),
                'seat_type' => array(
                    'class' => 'Illuzion\\Common\\Doctrine\\SeatType',
                    'commented' => true,
                ),
                'json' => array(
                    'class' => 'Sonata\\Doctrine\\Types\\JsonType',
                    'commented' => true,
                ),
            ),
            'doctrine.connections' => array(
                'shared' => 'doctrine.dbal.shared_connection',
                'uss_main' => 'doctrine.dbal.uss_main_connection',
                'uss_customer' => 'doctrine.dbal.uss_customer_connection',
                'oc_main' => 'doctrine.dbal.oc_main_connection',
                'oc_customer' => 'doctrine.dbal.oc_customer_connection',
                'ill_main' => 'doctrine.dbal.ill_main_connection',
                'ill_customer' => 'doctrine.dbal.ill_customer_connection',
                'che_main' => 'doctrine.dbal.che_main_connection',
                'che_customer' => 'doctrine.dbal.che_customer_connection',
                'nmega_main' => 'doctrine.dbal.nmega_main_connection',
                'nmega_customer' => 'doctrine.dbal.nmega_customer_connection',
            ),
            'doctrine.default_connection' => 'shared',
            'doctrine.orm.configuration.class' => 'Doctrine\\ORM\\Configuration',
            'doctrine.orm.entity_manager.class' => 'Doctrine\\ORM\\EntityManager',
            'doctrine.orm.manager_configurator.class' => 'Doctrine\\Bundle\\DoctrineBundle\\ManagerConfigurator',
            'doctrine.orm.cache.array.class' => 'Doctrine\\Common\\Cache\\ArrayCache',
            'doctrine.orm.cache.apc.class' => 'Doctrine\\Common\\Cache\\ApcCache',
            'doctrine.orm.cache.memcache.class' => 'Doctrine\\Common\\Cache\\MemcacheCache',
            'doctrine.orm.cache.memcache_host' => 'localhost',
            'doctrine.orm.cache.memcache_port' => 11211,
            'doctrine.orm.cache.memcache_instance.class' => 'Memcache',
            'doctrine.orm.cache.memcached.class' => 'Doctrine\\Common\\Cache\\MemcachedCache',
            'doctrine.orm.cache.memcached_host' => 'localhost',
            'doctrine.orm.cache.memcached_port' => 11211,
            'doctrine.orm.cache.memcached_instance.class' => 'Memcached',
            'doctrine.orm.cache.redis.class' => 'Doctrine\\Common\\Cache\\RedisCache',
            'doctrine.orm.cache.redis_host' => 'localhost',
            'doctrine.orm.cache.redis_port' => 6379,
            'doctrine.orm.cache.redis_instance.class' => 'Redis',
            'doctrine.orm.cache.xcache.class' => 'Doctrine\\Common\\Cache\\XcacheCache',
            'doctrine.orm.cache.wincache.class' => 'Doctrine\\Common\\Cache\\WinCacheCache',
            'doctrine.orm.cache.zenddata.class' => 'Doctrine\\Common\\Cache\\ZendDataCache',
            'doctrine.orm.metadata.driver_chain.class' => 'Doctrine\\Common\\Persistence\\Mapping\\Driver\\MappingDriverChain',
            'doctrine.orm.metadata.annotation.class' => 'Doctrine\\ORM\\Mapping\\Driver\\AnnotationDriver',
            'doctrine.orm.metadata.xml.class' => 'Doctrine\\ORM\\Mapping\\Driver\\SimplifiedXmlDriver',
            'doctrine.orm.metadata.yml.class' => 'Doctrine\\ORM\\Mapping\\Driver\\SimplifiedYamlDriver',
            'doctrine.orm.metadata.php.class' => 'Doctrine\\ORM\\Mapping\\Driver\\PHPDriver',
            'doctrine.orm.metadata.staticphp.class' => 'Doctrine\\ORM\\Mapping\\Driver\\StaticPHPDriver',
            'doctrine.orm.proxy_cache_warmer.class' => 'Symfony\\Bridge\\Doctrine\\CacheWarmer\\ProxyCacheWarmer',
            'form.type_guesser.doctrine.class' => 'Symfony\\Bridge\\Doctrine\\Form\\DoctrineOrmTypeGuesser',
            'doctrine.orm.validator.unique.class' => 'Symfony\\Bridge\\Doctrine\\Validator\\Constraints\\UniqueEntityValidator',
            'doctrine.orm.validator_initializer.class' => 'Symfony\\Bridge\\Doctrine\\Validator\\DoctrineInitializer',
            'doctrine.orm.security.user.provider.class' => 'Symfony\\Bridge\\Doctrine\\Security\\User\\EntityUserProvider',
            'doctrine.orm.listeners.resolve_target_entity.class' => 'Doctrine\\ORM\\Tools\\ResolveTargetEntityListener',
            'doctrine.orm.listeners.attach_entity_listeners.class' => 'Doctrine\\ORM\\Tools\\AttachEntityListenersListener',
            'doctrine.orm.naming_strategy.default.class' => 'Doctrine\\ORM\\Mapping\\DefaultNamingStrategy',
            'doctrine.orm.naming_strategy.underscore.class' => 'Doctrine\\ORM\\Mapping\\UnderscoreNamingStrategy',
            'doctrine.orm.quote_strategy.default.class' => 'Doctrine\\ORM\\Mapping\\DefaultQuoteStrategy',
            'doctrine.orm.quote_strategy.ansi.class' => 'Doctrine\\ORM\\Mapping\\AnsiQuoteStrategy',
            'doctrine.orm.entity_listener_resolver.class' => 'Doctrine\\Bundle\\DoctrineBundle\\Mapping\\ContainerAwareEntityListenerResolver',
            'doctrine.orm.second_level_cache.default_cache_factory.class' => 'Doctrine\\ORM\\Cache\\DefaultCacheFactory',
            'doctrine.orm.second_level_cache.default_region.class' => 'Doctrine\\ORM\\Cache\\Region\\DefaultRegion',
            'doctrine.orm.second_level_cache.filelock_region.class' => 'Doctrine\\ORM\\Cache\\Region\\FileLockRegion',
            'doctrine.orm.second_level_cache.logger_chain.class' => 'Doctrine\\ORM\\Cache\\Logging\\CacheLoggerChain',
            'doctrine.orm.second_level_cache.logger_statistics.class' => 'Doctrine\\ORM\\Cache\\Logging\\StatisticsCacheLogger',
            'doctrine.orm.second_level_cache.cache_configuration.class' => 'Doctrine\\ORM\\Cache\\CacheConfiguration',
            'doctrine.orm.second_level_cache.regions_configuration.class' => 'Doctrine\\ORM\\Cache\\RegionsConfiguration',
            'doctrine.orm.auto_generate_proxy_classes' => true,
            'doctrine.orm.proxy_dir' => (__DIR__.'/doctrine/orm/Proxies'),
            'doctrine.orm.proxy_namespace' => 'Proxies',
            'sensio_framework_extra.view.guesser.class' => 'Sensio\\Bundle\\FrameworkExtraBundle\\Templating\\TemplateGuesser',
            'sensio_framework_extra.controller.listener.class' => 'Sensio\\Bundle\\FrameworkExtraBundle\\EventListener\\ControllerListener',
            'sensio_framework_extra.routing.loader.annot_dir.class' => 'Symfony\\Component\\Routing\\Loader\\AnnotationDirectoryLoader',
            'sensio_framework_extra.routing.loader.annot_file.class' => 'Symfony\\Component\\Routing\\Loader\\AnnotationFileLoader',
            'sensio_framework_extra.routing.loader.annot_class.class' => 'Sensio\\Bundle\\FrameworkExtraBundle\\Routing\\AnnotatedRouteControllerLoader',
            'sensio_framework_extra.converter.listener.class' => 'Sensio\\Bundle\\FrameworkExtraBundle\\EventListener\\ParamConverterListener',
            'sensio_framework_extra.converter.manager.class' => 'Sensio\\Bundle\\FrameworkExtraBundle\\Request\\ParamConverter\\ParamConverterManager',
            'sensio_framework_extra.converter.doctrine.class' => 'Sensio\\Bundle\\FrameworkExtraBundle\\Request\\ParamConverter\\DoctrineParamConverter',
            'sensio_framework_extra.converter.datetime.class' => 'Sensio\\Bundle\\FrameworkExtraBundle\\Request\\ParamConverter\\DateTimeParamConverter',
            'sensio_framework_extra.view.listener.class' => 'Sensio\\Bundle\\FrameworkExtraBundle\\EventListener\\TemplateListener',
            'doctrine_migrations.namespace' => 'Application\\Migrations',
            'doctrine_migrations.table_name' => 'migration_versions',
            'doctrine_migrations.name' => 'Application Migrations',
            'doctrine_migrations.organize_migrations' => false,
            'swiftmailer.class' => 'Swift_Mailer',
            'swiftmailer.transport.sendmail.class' => 'Swift_Transport_SendmailTransport',
            'swiftmailer.transport.mail.class' => 'Swift_Transport_MailTransport',
            'swiftmailer.transport.failover.class' => 'Swift_Transport_FailoverTransport',
            'swiftmailer.plugin.redirecting.class' => 'Swift_Plugins_RedirectingPlugin',
            'swiftmailer.plugin.impersonate.class' => 'Swift_Plugins_ImpersonatePlugin',
            'swiftmailer.plugin.messagelogger.class' => 'Swift_Plugins_MessageLogger',
            'swiftmailer.plugin.antiflood.class' => 'Swift_Plugins_AntiFloodPlugin',
            'swiftmailer.transport.smtp.class' => 'Swift_Transport_EsmtpTransport',
            'swiftmailer.plugin.blackhole.class' => 'Swift_Plugins_BlackholePlugin',
            'swiftmailer.spool.file.class' => 'Swift_FileSpool',
            'swiftmailer.spool.memory.class' => 'Swift_MemorySpool',
            'swiftmailer.email_sender.listener.class' => 'Symfony\\Bundle\\SwiftmailerBundle\\EventListener\\EmailSenderListener',
            'swiftmailer.data_collector.class' => 'Symfony\\Bundle\\SwiftmailerBundle\\DataCollector\\MessageDataCollector',
            'swiftmailer.mailer.default.transport.name' => 'smtp',
            'swiftmailer.mailer.default.transport.smtp.encryption' => NULL,
            'swiftmailer.mailer.default.transport.smtp.port' => 25,
            'swiftmailer.mailer.default.transport.smtp.host' => 'localhost',
            'swiftmailer.mailer.default.transport.smtp.username' => NULL,
            'swiftmailer.mailer.default.transport.smtp.password' => NULL,
            'swiftmailer.mailer.default.transport.smtp.auth_mode' => NULL,
            'swiftmailer.mailer.default.transport.smtp.timeout' => 30,
            'swiftmailer.mailer.default.transport.smtp.source_ip' => NULL,
            'swiftmailer.mailer.default.transport.smtp.local_domain' => NULL,
            'swiftmailer.mailer.default.spool.enabled' => false,
            'swiftmailer.mailer.default.plugin.impersonate' => NULL,
            'swiftmailer.mailer.default.single_address' => NULL,
            'swiftmailer.mailer.default.delivery.enabled' => true,
            'swiftmailer.spool.enabled' => false,
            'swiftmailer.delivery.enabled' => true,
            'swiftmailer.single_address' => NULL,
            'swiftmailer.mailers' => array(
                'default' => 'swiftmailer.mailer.default',
            ),
            'swiftmailer.default_mailer' => 'default',
            'sonata.notification.backend' => 'sonata.notification.backend.doctrine',
            'sonata.notification.message.class' => 'Illuzion\\SynchronizationBundle\\Entity\\Message',
            'sonata.notification.admin.message.entity' => 'Illuzion\\SynchronizationBundle\\Entity\\Message',
            'sonata.notification.manager.message.entity' => 'Illuzion\\SynchronizationBundle\\Entity\\Message',
            'sonata.notification.event.iteration_listeners' => array(
                0 => 'sonata.notification.event.doctrine_backend_optimize',
            ),
            'sonata.notification.admin.message.class' => 'Sonata\\NotificationBundle\\Admin\\MessageAdmin',
            'sonata.notification.admin.message.controller' => 'SonataNotificationBundle:MessageAdmin',
            'sonata.notification.admin.message.translation_domain' => 'SonataNotificationBundle',
            'serializer.ecoder_option.json_options' => 320,
            'serializer.ecoder_option.url_prefix' => 'http://ussuri.illuzion.loc:7023/v4',
            'serializer.ecoder_option.depth' => 512,
            'serializer.image_url_prefix' => 'http://service.illuzion.ru/media',
            'api.seats_cache_in_secs' => 0,
            'console.command.ids' => array(
                'console.command.symfony_bundle_securitybundle_command_userpasswordencodercommand' => 'console.command.symfony_bundle_securitybundle_command_userpasswordencodercommand',
                'console.command.illuzion_apibundle_command_generatejwstokencommand' => 'api.command.generate_jws_token',
                'console.command.illuzion_apibundle_command_moviesscanforarchivedattributecommand' => 'api.command.movie_scan_for_archived',
                'console.command.illuzion_apibundle_command_canceltempreservationscommand' => 'api.command.cancel_temp_reservations',
            ),
        );
    }
}

class SymfonyComponentCacheAdapterMemcachedAdapter_000000002084051700000000634d02687471c441876336872902bf356897607e extends \Symfony\Component\Cache\Adapter\MemcachedAdapter implements \ProxyManager\Proxy\VirtualProxyInterface
{

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $valueHolder5acaafb3c4c09384722631 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer5acaafb3c4c29146485166 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties5acaafb3c4bb5291380403 = array(
        
    );

    /**
     * {@inheritDoc}
     */
    public function getItem($key)
    {
        $this->initializer5acaafb3c4c29146485166 && $this->initializer5acaafb3c4c29146485166->__invoke($this->valueHolder5acaafb3c4c09384722631, $this, 'getItem', array('key' => $key), $this->initializer5acaafb3c4c29146485166);

        return $this->valueHolder5acaafb3c4c09384722631->getItem($key);
    }

    /**
     * {@inheritDoc}
     */
    public function getItems(array $keys = array())
    {
        $this->initializer5acaafb3c4c29146485166 && $this->initializer5acaafb3c4c29146485166->__invoke($this->valueHolder5acaafb3c4c09384722631, $this, 'getItems', array('keys' => $keys), $this->initializer5acaafb3c4c29146485166);

        return $this->valueHolder5acaafb3c4c09384722631->getItems($keys);
    }

    /**
     * {@inheritDoc}
     */
    public function save(\Psr\Cache\CacheItemInterface $item)
    {
        $this->initializer5acaafb3c4c29146485166 && $this->initializer5acaafb3c4c29146485166->__invoke($this->valueHolder5acaafb3c4c09384722631, $this, 'save', array('item' => $item), $this->initializer5acaafb3c4c29146485166);

        return $this->valueHolder5acaafb3c4c09384722631->save($item);
    }

    /**
     * {@inheritDoc}
     */
    public function saveDeferred(\Psr\Cache\CacheItemInterface $item)
    {
        $this->initializer5acaafb3c4c29146485166 && $this->initializer5acaafb3c4c29146485166->__invoke($this->valueHolder5acaafb3c4c09384722631, $this, 'saveDeferred', array('item' => $item), $this->initializer5acaafb3c4c29146485166);

        return $this->valueHolder5acaafb3c4c09384722631->saveDeferred($item);
    }

    /**
     * {@inheritDoc}
     */
    public function commit()
    {
        $this->initializer5acaafb3c4c29146485166 && $this->initializer5acaafb3c4c29146485166->__invoke($this->valueHolder5acaafb3c4c09384722631, $this, 'commit', array(), $this->initializer5acaafb3c4c29146485166);

        return $this->valueHolder5acaafb3c4c09384722631->commit();
    }

    /**
     * {@inheritDoc}
     */
    public function __destruct()
    {
        $this->initializer5acaafb3c4c29146485166 && $this->initializer5acaafb3c4c29146485166->__invoke($this->valueHolder5acaafb3c4c09384722631, $this, '__destruct', array(), $this->initializer5acaafb3c4c29146485166);

        return $this->valueHolder5acaafb3c4c09384722631->__destruct();
    }

    /**
     * {@inheritDoc}
     */
    public function hasItem($key)
    {
        $this->initializer5acaafb3c4c29146485166 && $this->initializer5acaafb3c4c29146485166->__invoke($this->valueHolder5acaafb3c4c09384722631, $this, 'hasItem', array('key' => $key), $this->initializer5acaafb3c4c29146485166);

        return $this->valueHolder5acaafb3c4c09384722631->hasItem($key);
    }

    /**
     * {@inheritDoc}
     */
    public function clear()
    {
        $this->initializer5acaafb3c4c29146485166 && $this->initializer5acaafb3c4c29146485166->__invoke($this->valueHolder5acaafb3c4c09384722631, $this, 'clear', array(), $this->initializer5acaafb3c4c29146485166);

        return $this->valueHolder5acaafb3c4c09384722631->clear();
    }

    /**
     * {@inheritDoc}
     */
    public function deleteItem($key)
    {
        $this->initializer5acaafb3c4c29146485166 && $this->initializer5acaafb3c4c29146485166->__invoke($this->valueHolder5acaafb3c4c09384722631, $this, 'deleteItem', array('key' => $key), $this->initializer5acaafb3c4c29146485166);

        return $this->valueHolder5acaafb3c4c09384722631->deleteItem($key);
    }

    /**
     * {@inheritDoc}
     */
    public function deleteItems(array $keys)
    {
        $this->initializer5acaafb3c4c29146485166 && $this->initializer5acaafb3c4c29146485166->__invoke($this->valueHolder5acaafb3c4c09384722631, $this, 'deleteItems', array('keys' => $keys), $this->initializer5acaafb3c4c29146485166);

        return $this->valueHolder5acaafb3c4c09384722631->deleteItems($keys);
    }

    /**
     * {@inheritDoc}
     */
    public function setLogger(\Psr\Log\LoggerInterface $logger)
    {
        $this->initializer5acaafb3c4c29146485166 && $this->initializer5acaafb3c4c29146485166->__invoke($this->valueHolder5acaafb3c4c09384722631, $this, 'setLogger', array('logger' => $logger), $this->initializer5acaafb3c4c29146485166);

        return $this->valueHolder5acaafb3c4c09384722631->setLogger($logger);
    }

    /**
     * @override constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public function __construct($initializer)
    {
        $this->initializer5acaafb3c4c29146485166 = $initializer;
    }

    /**
     * @param string $name
     */
    public function & __get($name)
    {
        $this->initializer5acaafb3c4c29146485166 && $this->initializer5acaafb3c4c29146485166->__invoke($this->valueHolder5acaafb3c4c09384722631, $this, '__get', array('name' => $name), $this->initializer5acaafb3c4c29146485166);

        if (isset(self::$publicProperties5acaafb3c4bb5291380403[$name])) {
            return $this->valueHolder5acaafb3c4c09384722631->$name;
        }

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5acaafb3c4c09384722631;

            $backtrace = debug_backtrace(false);
            trigger_error('Undefined property: ' . get_parent_class($this) . '::$' . $name . ' in ' . $backtrace[0]['file'] . ' on line ' . $backtrace[0]['line'], \E_USER_NOTICE);
            return $targetObject->$name;;
            return;
        }

        $targetObject = $this->valueHolder5acaafb3c4c09384722631;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    /**
     * @param string $name
     * @param mixed $value
     */
    public function __set($name, $value)
    {
        $this->initializer5acaafb3c4c29146485166 && $this->initializer5acaafb3c4c29146485166->__invoke($this->valueHolder5acaafb3c4c09384722631, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer5acaafb3c4c29146485166);

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5acaafb3c4c09384722631;

            return $targetObject->$name = $value;;
            return;
        }

        $targetObject = $this->valueHolder5acaafb3c4c09384722631;
        $accessor = function & () use ($targetObject, $name, $value) {
            return $targetObject->$name = $value;
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    /**
     * @param string $name
     */
    public function __isset($name)
    {
        $this->initializer5acaafb3c4c29146485166 && $this->initializer5acaafb3c4c29146485166->__invoke($this->valueHolder5acaafb3c4c09384722631, $this, '__isset', array('name' => $name), $this->initializer5acaafb3c4c29146485166);

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5acaafb3c4c09384722631;

            return isset($targetObject->$name);;
            return;
        }

        $targetObject = $this->valueHolder5acaafb3c4c09384722631;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    /**
     * @param string $name
     */
    public function __unset($name)
    {
        $this->initializer5acaafb3c4c29146485166 && $this->initializer5acaafb3c4c29146485166->__invoke($this->valueHolder5acaafb3c4c09384722631, $this, '__unset', array('name' => $name), $this->initializer5acaafb3c4c29146485166);

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5acaafb3c4c09384722631;

            unset($targetObject->$name);;
            return;
        }

        $targetObject = $this->valueHolder5acaafb3c4c09384722631;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __clone()
    {
        $this->initializer5acaafb3c4c29146485166 && $this->initializer5acaafb3c4c29146485166->__invoke($this->valueHolder5acaafb3c4c09384722631, $this, '__clone', array(), $this->initializer5acaafb3c4c29146485166);

        $this->valueHolder5acaafb3c4c09384722631 = clone $this->valueHolder5acaafb3c4c09384722631;
    }

    public function __sleep()
    {
        $this->initializer5acaafb3c4c29146485166 && $this->initializer5acaafb3c4c29146485166->__invoke($this->valueHolder5acaafb3c4c09384722631, $this, '__sleep', array(), $this->initializer5acaafb3c4c29146485166);

        return array('valueHolder5acaafb3c4c09384722631');
    }

    public function __wakeup()
    {
    }

    /**
     * {@inheritDoc}
     */
    public function setProxyInitializer(\Closure $initializer = null)
    {
        $this->initializer5acaafb3c4c29146485166 = $initializer;
    }

    /**
     * {@inheritDoc}
     */
    public function getProxyInitializer()
    {
        return $this->initializer5acaafb3c4c29146485166;
    }

    /**
     * {@inheritDoc}
     */
    public function initializeProxy()
    {
        return $this->initializer5acaafb3c4c29146485166 && $this->initializer5acaafb3c4c29146485166->__invoke($this->valueHolder5acaafb3c4c09384722631, $this, 'initializeProxy', array(), $this->initializer5acaafb3c4c29146485166);
    }

    /**
     * {@inheritDoc}
     */
    public function isProxyInitialized()
    {
        return null !== $this->valueHolder5acaafb3c4c09384722631;
    }

    /**
     * {@inheritDoc}
     */
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder5acaafb3c4c09384722631;
    }


}

class SymfonyComponentCacheAdapterMemcachedAdapter_000000002084051500000000634d02687471c441876336872902bf356897607e extends \Symfony\Component\Cache\Adapter\MemcachedAdapter implements \ProxyManager\Proxy\VirtualProxyInterface
{

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $valueHolder5acaafb3ca912287923809 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer5acaafb3ca92c584545519 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties5acaafb3ca8d4593213114 = array(
        
    );

    /**
     * {@inheritDoc}
     */
    public function getItem($key)
    {
        $this->initializer5acaafb3ca92c584545519 && $this->initializer5acaafb3ca92c584545519->__invoke($this->valueHolder5acaafb3ca912287923809, $this, 'getItem', array('key' => $key), $this->initializer5acaafb3ca92c584545519);

        return $this->valueHolder5acaafb3ca912287923809->getItem($key);
    }

    /**
     * {@inheritDoc}
     */
    public function getItems(array $keys = array())
    {
        $this->initializer5acaafb3ca92c584545519 && $this->initializer5acaafb3ca92c584545519->__invoke($this->valueHolder5acaafb3ca912287923809, $this, 'getItems', array('keys' => $keys), $this->initializer5acaafb3ca92c584545519);

        return $this->valueHolder5acaafb3ca912287923809->getItems($keys);
    }

    /**
     * {@inheritDoc}
     */
    public function save(\Psr\Cache\CacheItemInterface $item)
    {
        $this->initializer5acaafb3ca92c584545519 && $this->initializer5acaafb3ca92c584545519->__invoke($this->valueHolder5acaafb3ca912287923809, $this, 'save', array('item' => $item), $this->initializer5acaafb3ca92c584545519);

        return $this->valueHolder5acaafb3ca912287923809->save($item);
    }

    /**
     * {@inheritDoc}
     */
    public function saveDeferred(\Psr\Cache\CacheItemInterface $item)
    {
        $this->initializer5acaafb3ca92c584545519 && $this->initializer5acaafb3ca92c584545519->__invoke($this->valueHolder5acaafb3ca912287923809, $this, 'saveDeferred', array('item' => $item), $this->initializer5acaafb3ca92c584545519);

        return $this->valueHolder5acaafb3ca912287923809->saveDeferred($item);
    }

    /**
     * {@inheritDoc}
     */
    public function commit()
    {
        $this->initializer5acaafb3ca92c584545519 && $this->initializer5acaafb3ca92c584545519->__invoke($this->valueHolder5acaafb3ca912287923809, $this, 'commit', array(), $this->initializer5acaafb3ca92c584545519);

        return $this->valueHolder5acaafb3ca912287923809->commit();
    }

    /**
     * {@inheritDoc}
     */
    public function __destruct()
    {
        $this->initializer5acaafb3ca92c584545519 && $this->initializer5acaafb3ca92c584545519->__invoke($this->valueHolder5acaafb3ca912287923809, $this, '__destruct', array(), $this->initializer5acaafb3ca92c584545519);

        return $this->valueHolder5acaafb3ca912287923809->__destruct();
    }

    /**
     * {@inheritDoc}
     */
    public function hasItem($key)
    {
        $this->initializer5acaafb3ca92c584545519 && $this->initializer5acaafb3ca92c584545519->__invoke($this->valueHolder5acaafb3ca912287923809, $this, 'hasItem', array('key' => $key), $this->initializer5acaafb3ca92c584545519);

        return $this->valueHolder5acaafb3ca912287923809->hasItem($key);
    }

    /**
     * {@inheritDoc}
     */
    public function clear()
    {
        $this->initializer5acaafb3ca92c584545519 && $this->initializer5acaafb3ca92c584545519->__invoke($this->valueHolder5acaafb3ca912287923809, $this, 'clear', array(), $this->initializer5acaafb3ca92c584545519);

        return $this->valueHolder5acaafb3ca912287923809->clear();
    }

    /**
     * {@inheritDoc}
     */
    public function deleteItem($key)
    {
        $this->initializer5acaafb3ca92c584545519 && $this->initializer5acaafb3ca92c584545519->__invoke($this->valueHolder5acaafb3ca912287923809, $this, 'deleteItem', array('key' => $key), $this->initializer5acaafb3ca92c584545519);

        return $this->valueHolder5acaafb3ca912287923809->deleteItem($key);
    }

    /**
     * {@inheritDoc}
     */
    public function deleteItems(array $keys)
    {
        $this->initializer5acaafb3ca92c584545519 && $this->initializer5acaafb3ca92c584545519->__invoke($this->valueHolder5acaafb3ca912287923809, $this, 'deleteItems', array('keys' => $keys), $this->initializer5acaafb3ca92c584545519);

        return $this->valueHolder5acaafb3ca912287923809->deleteItems($keys);
    }

    /**
     * {@inheritDoc}
     */
    public function setLogger(\Psr\Log\LoggerInterface $logger)
    {
        $this->initializer5acaafb3ca92c584545519 && $this->initializer5acaafb3ca92c584545519->__invoke($this->valueHolder5acaafb3ca912287923809, $this, 'setLogger', array('logger' => $logger), $this->initializer5acaafb3ca92c584545519);

        return $this->valueHolder5acaafb3ca912287923809->setLogger($logger);
    }

    /**
     * @override constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public function __construct($initializer)
    {
        $this->initializer5acaafb3ca92c584545519 = $initializer;
    }

    /**
     * @param string $name
     */
    public function & __get($name)
    {
        $this->initializer5acaafb3ca92c584545519 && $this->initializer5acaafb3ca92c584545519->__invoke($this->valueHolder5acaafb3ca912287923809, $this, '__get', array('name' => $name), $this->initializer5acaafb3ca92c584545519);

        if (isset(self::$publicProperties5acaafb3ca8d4593213114[$name])) {
            return $this->valueHolder5acaafb3ca912287923809->$name;
        }

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5acaafb3ca912287923809;

            $backtrace = debug_backtrace(false);
            trigger_error('Undefined property: ' . get_parent_class($this) . '::$' . $name . ' in ' . $backtrace[0]['file'] . ' on line ' . $backtrace[0]['line'], \E_USER_NOTICE);
            return $targetObject->$name;;
            return;
        }

        $targetObject = $this->valueHolder5acaafb3ca912287923809;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    /**
     * @param string $name
     * @param mixed $value
     */
    public function __set($name, $value)
    {
        $this->initializer5acaafb3ca92c584545519 && $this->initializer5acaafb3ca92c584545519->__invoke($this->valueHolder5acaafb3ca912287923809, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer5acaafb3ca92c584545519);

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5acaafb3ca912287923809;

            return $targetObject->$name = $value;;
            return;
        }

        $targetObject = $this->valueHolder5acaafb3ca912287923809;
        $accessor = function & () use ($targetObject, $name, $value) {
            return $targetObject->$name = $value;
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    /**
     * @param string $name
     */
    public function __isset($name)
    {
        $this->initializer5acaafb3ca92c584545519 && $this->initializer5acaafb3ca92c584545519->__invoke($this->valueHolder5acaafb3ca912287923809, $this, '__isset', array('name' => $name), $this->initializer5acaafb3ca92c584545519);

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5acaafb3ca912287923809;

            return isset($targetObject->$name);;
            return;
        }

        $targetObject = $this->valueHolder5acaafb3ca912287923809;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    /**
     * @param string $name
     */
    public function __unset($name)
    {
        $this->initializer5acaafb3ca92c584545519 && $this->initializer5acaafb3ca92c584545519->__invoke($this->valueHolder5acaafb3ca912287923809, $this, '__unset', array('name' => $name), $this->initializer5acaafb3ca92c584545519);

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5acaafb3ca912287923809;

            unset($targetObject->$name);;
            return;
        }

        $targetObject = $this->valueHolder5acaafb3ca912287923809;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __clone()
    {
        $this->initializer5acaafb3ca92c584545519 && $this->initializer5acaafb3ca92c584545519->__invoke($this->valueHolder5acaafb3ca912287923809, $this, '__clone', array(), $this->initializer5acaafb3ca92c584545519);

        $this->valueHolder5acaafb3ca912287923809 = clone $this->valueHolder5acaafb3ca912287923809;
    }

    public function __sleep()
    {
        $this->initializer5acaafb3ca92c584545519 && $this->initializer5acaafb3ca92c584545519->__invoke($this->valueHolder5acaafb3ca912287923809, $this, '__sleep', array(), $this->initializer5acaafb3ca92c584545519);

        return array('valueHolder5acaafb3ca912287923809');
    }

    public function __wakeup()
    {
    }

    /**
     * {@inheritDoc}
     */
    public function setProxyInitializer(\Closure $initializer = null)
    {
        $this->initializer5acaafb3ca92c584545519 = $initializer;
    }

    /**
     * {@inheritDoc}
     */
    public function getProxyInitializer()
    {
        return $this->initializer5acaafb3ca92c584545519;
    }

    /**
     * {@inheritDoc}
     */
    public function initializeProxy()
    {
        return $this->initializer5acaafb3ca92c584545519 && $this->initializer5acaafb3ca92c584545519->__invoke($this->valueHolder5acaafb3ca912287923809, $this, 'initializeProxy', array(), $this->initializer5acaafb3ca92c584545519);
    }

    /**
     * {@inheritDoc}
     */
    public function isProxyInitialized()
    {
        return null !== $this->valueHolder5acaafb3ca912287923809;
    }

    /**
     * {@inheritDoc}
     */
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder5acaafb3ca912287923809;
    }


}

class DoctrineORMEntityManager_000000002084056000000000634d02687471c441876336872902bf356897607e extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $valueHolder5acaafb3d0450721347842 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer5acaafb3d0469289557090 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties5acaafb3d0412565179754 = array(
        
    );

    /**
     * {@inheritDoc}
     */
    public function getConnection()
    {
        $this->initializer5acaafb3d0469289557090 && $this->initializer5acaafb3d0469289557090->__invoke($this->valueHolder5acaafb3d0450721347842, $this, 'getConnection', array(), $this->initializer5acaafb3d0469289557090);

        return $this->valueHolder5acaafb3d0450721347842->getConnection();
    }

    /**
     * {@inheritDoc}
     */
    public function getMetadataFactory()
    {
        $this->initializer5acaafb3d0469289557090 && $this->initializer5acaafb3d0469289557090->__invoke($this->valueHolder5acaafb3d0450721347842, $this, 'getMetadataFactory', array(), $this->initializer5acaafb3d0469289557090);

        return $this->valueHolder5acaafb3d0450721347842->getMetadataFactory();
    }

    /**
     * {@inheritDoc}
     */
    public function getExpressionBuilder()
    {
        $this->initializer5acaafb3d0469289557090 && $this->initializer5acaafb3d0469289557090->__invoke($this->valueHolder5acaafb3d0450721347842, $this, 'getExpressionBuilder', array(), $this->initializer5acaafb3d0469289557090);

        return $this->valueHolder5acaafb3d0450721347842->getExpressionBuilder();
    }

    /**
     * {@inheritDoc}
     */
    public function beginTransaction()
    {
        $this->initializer5acaafb3d0469289557090 && $this->initializer5acaafb3d0469289557090->__invoke($this->valueHolder5acaafb3d0450721347842, $this, 'beginTransaction', array(), $this->initializer5acaafb3d0469289557090);

        return $this->valueHolder5acaafb3d0450721347842->beginTransaction();
    }

    /**
     * {@inheritDoc}
     */
    public function getCache()
    {
        $this->initializer5acaafb3d0469289557090 && $this->initializer5acaafb3d0469289557090->__invoke($this->valueHolder5acaafb3d0450721347842, $this, 'getCache', array(), $this->initializer5acaafb3d0469289557090);

        return $this->valueHolder5acaafb3d0450721347842->getCache();
    }

    /**
     * {@inheritDoc}
     */
    public function transactional($func)
    {
        $this->initializer5acaafb3d0469289557090 && $this->initializer5acaafb3d0469289557090->__invoke($this->valueHolder5acaafb3d0450721347842, $this, 'transactional', array('func' => $func), $this->initializer5acaafb3d0469289557090);

        return $this->valueHolder5acaafb3d0450721347842->transactional($func);
    }

    /**
     * {@inheritDoc}
     */
    public function commit()
    {
        $this->initializer5acaafb3d0469289557090 && $this->initializer5acaafb3d0469289557090->__invoke($this->valueHolder5acaafb3d0450721347842, $this, 'commit', array(), $this->initializer5acaafb3d0469289557090);

        return $this->valueHolder5acaafb3d0450721347842->commit();
    }

    /**
     * {@inheritDoc}
     */
    public function rollback()
    {
        $this->initializer5acaafb3d0469289557090 && $this->initializer5acaafb3d0469289557090->__invoke($this->valueHolder5acaafb3d0450721347842, $this, 'rollback', array(), $this->initializer5acaafb3d0469289557090);

        return $this->valueHolder5acaafb3d0450721347842->rollback();
    }

    /**
     * {@inheritDoc}
     */
    public function getClassMetadata($className)
    {
        $this->initializer5acaafb3d0469289557090 && $this->initializer5acaafb3d0469289557090->__invoke($this->valueHolder5acaafb3d0450721347842, $this, 'getClassMetadata', array('className' => $className), $this->initializer5acaafb3d0469289557090);

        return $this->valueHolder5acaafb3d0450721347842->getClassMetadata($className);
    }

    /**
     * {@inheritDoc}
     */
    public function createQuery($dql = '')
    {
        $this->initializer5acaafb3d0469289557090 && $this->initializer5acaafb3d0469289557090->__invoke($this->valueHolder5acaafb3d0450721347842, $this, 'createQuery', array('dql' => $dql), $this->initializer5acaafb3d0469289557090);

        return $this->valueHolder5acaafb3d0450721347842->createQuery($dql);
    }

    /**
     * {@inheritDoc}
     */
    public function createNamedQuery($name)
    {
        $this->initializer5acaafb3d0469289557090 && $this->initializer5acaafb3d0469289557090->__invoke($this->valueHolder5acaafb3d0450721347842, $this, 'createNamedQuery', array('name' => $name), $this->initializer5acaafb3d0469289557090);

        return $this->valueHolder5acaafb3d0450721347842->createNamedQuery($name);
    }

    /**
     * {@inheritDoc}
     */
    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializer5acaafb3d0469289557090 && $this->initializer5acaafb3d0469289557090->__invoke($this->valueHolder5acaafb3d0450721347842, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializer5acaafb3d0469289557090);

        return $this->valueHolder5acaafb3d0450721347842->createNativeQuery($sql, $rsm);
    }

    /**
     * {@inheritDoc}
     */
    public function createNamedNativeQuery($name)
    {
        $this->initializer5acaafb3d0469289557090 && $this->initializer5acaafb3d0469289557090->__invoke($this->valueHolder5acaafb3d0450721347842, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializer5acaafb3d0469289557090);

        return $this->valueHolder5acaafb3d0450721347842->createNamedNativeQuery($name);
    }

    /**
     * {@inheritDoc}
     */
    public function createQueryBuilder()
    {
        $this->initializer5acaafb3d0469289557090 && $this->initializer5acaafb3d0469289557090->__invoke($this->valueHolder5acaafb3d0450721347842, $this, 'createQueryBuilder', array(), $this->initializer5acaafb3d0469289557090);

        return $this->valueHolder5acaafb3d0450721347842->createQueryBuilder();
    }

    /**
     * {@inheritDoc}
     */
    public function flush($entity = null)
    {
        $this->initializer5acaafb3d0469289557090 && $this->initializer5acaafb3d0469289557090->__invoke($this->valueHolder5acaafb3d0450721347842, $this, 'flush', array('entity' => $entity), $this->initializer5acaafb3d0469289557090);

        return $this->valueHolder5acaafb3d0450721347842->flush($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function find($entityName, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer5acaafb3d0469289557090 && $this->initializer5acaafb3d0469289557090->__invoke($this->valueHolder5acaafb3d0450721347842, $this, 'find', array('entityName' => $entityName, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer5acaafb3d0469289557090);

        return $this->valueHolder5acaafb3d0450721347842->find($entityName, $id, $lockMode, $lockVersion);
    }

    /**
     * {@inheritDoc}
     */
    public function getReference($entityName, $id)
    {
        $this->initializer5acaafb3d0469289557090 && $this->initializer5acaafb3d0469289557090->__invoke($this->valueHolder5acaafb3d0450721347842, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializer5acaafb3d0469289557090);

        return $this->valueHolder5acaafb3d0450721347842->getReference($entityName, $id);
    }

    /**
     * {@inheritDoc}
     */
    public function getPartialReference($entityName, $identifier)
    {
        $this->initializer5acaafb3d0469289557090 && $this->initializer5acaafb3d0469289557090->__invoke($this->valueHolder5acaafb3d0450721347842, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializer5acaafb3d0469289557090);

        return $this->valueHolder5acaafb3d0450721347842->getPartialReference($entityName, $identifier);
    }

    /**
     * {@inheritDoc}
     */
    public function clear($entityName = null)
    {
        $this->initializer5acaafb3d0469289557090 && $this->initializer5acaafb3d0469289557090->__invoke($this->valueHolder5acaafb3d0450721347842, $this, 'clear', array('entityName' => $entityName), $this->initializer5acaafb3d0469289557090);

        return $this->valueHolder5acaafb3d0450721347842->clear($entityName);
    }

    /**
     * {@inheritDoc}
     */
    public function close()
    {
        $this->initializer5acaafb3d0469289557090 && $this->initializer5acaafb3d0469289557090->__invoke($this->valueHolder5acaafb3d0450721347842, $this, 'close', array(), $this->initializer5acaafb3d0469289557090);

        return $this->valueHolder5acaafb3d0450721347842->close();
    }

    /**
     * {@inheritDoc}
     */
    public function persist($entity)
    {
        $this->initializer5acaafb3d0469289557090 && $this->initializer5acaafb3d0469289557090->__invoke($this->valueHolder5acaafb3d0450721347842, $this, 'persist', array('entity' => $entity), $this->initializer5acaafb3d0469289557090);

        return $this->valueHolder5acaafb3d0450721347842->persist($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function remove($entity)
    {
        $this->initializer5acaafb3d0469289557090 && $this->initializer5acaafb3d0469289557090->__invoke($this->valueHolder5acaafb3d0450721347842, $this, 'remove', array('entity' => $entity), $this->initializer5acaafb3d0469289557090);

        return $this->valueHolder5acaafb3d0450721347842->remove($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function refresh($entity)
    {
        $this->initializer5acaafb3d0469289557090 && $this->initializer5acaafb3d0469289557090->__invoke($this->valueHolder5acaafb3d0450721347842, $this, 'refresh', array('entity' => $entity), $this->initializer5acaafb3d0469289557090);

        return $this->valueHolder5acaafb3d0450721347842->refresh($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function detach($entity)
    {
        $this->initializer5acaafb3d0469289557090 && $this->initializer5acaafb3d0469289557090->__invoke($this->valueHolder5acaafb3d0450721347842, $this, 'detach', array('entity' => $entity), $this->initializer5acaafb3d0469289557090);

        return $this->valueHolder5acaafb3d0450721347842->detach($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function merge($entity)
    {
        $this->initializer5acaafb3d0469289557090 && $this->initializer5acaafb3d0469289557090->__invoke($this->valueHolder5acaafb3d0450721347842, $this, 'merge', array('entity' => $entity), $this->initializer5acaafb3d0469289557090);

        return $this->valueHolder5acaafb3d0450721347842->merge($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function copy($entity, $deep = false)
    {
        $this->initializer5acaafb3d0469289557090 && $this->initializer5acaafb3d0469289557090->__invoke($this->valueHolder5acaafb3d0450721347842, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializer5acaafb3d0469289557090);

        return $this->valueHolder5acaafb3d0450721347842->copy($entity, $deep);
    }

    /**
     * {@inheritDoc}
     */
    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializer5acaafb3d0469289557090 && $this->initializer5acaafb3d0469289557090->__invoke($this->valueHolder5acaafb3d0450721347842, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer5acaafb3d0469289557090);

        return $this->valueHolder5acaafb3d0450721347842->lock($entity, $lockMode, $lockVersion);
    }

    /**
     * {@inheritDoc}
     */
    public function getRepository($entityName)
    {
        $this->initializer5acaafb3d0469289557090 && $this->initializer5acaafb3d0469289557090->__invoke($this->valueHolder5acaafb3d0450721347842, $this, 'getRepository', array('entityName' => $entityName), $this->initializer5acaafb3d0469289557090);

        return $this->valueHolder5acaafb3d0450721347842->getRepository($entityName);
    }

    /**
     * {@inheritDoc}
     */
    public function contains($entity)
    {
        $this->initializer5acaafb3d0469289557090 && $this->initializer5acaafb3d0469289557090->__invoke($this->valueHolder5acaafb3d0450721347842, $this, 'contains', array('entity' => $entity), $this->initializer5acaafb3d0469289557090);

        return $this->valueHolder5acaafb3d0450721347842->contains($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function getEventManager()
    {
        $this->initializer5acaafb3d0469289557090 && $this->initializer5acaafb3d0469289557090->__invoke($this->valueHolder5acaafb3d0450721347842, $this, 'getEventManager', array(), $this->initializer5acaafb3d0469289557090);

        return $this->valueHolder5acaafb3d0450721347842->getEventManager();
    }

    /**
     * {@inheritDoc}
     */
    public function getConfiguration()
    {
        $this->initializer5acaafb3d0469289557090 && $this->initializer5acaafb3d0469289557090->__invoke($this->valueHolder5acaafb3d0450721347842, $this, 'getConfiguration', array(), $this->initializer5acaafb3d0469289557090);

        return $this->valueHolder5acaafb3d0450721347842->getConfiguration();
    }

    /**
     * {@inheritDoc}
     */
    public function isOpen()
    {
        $this->initializer5acaafb3d0469289557090 && $this->initializer5acaafb3d0469289557090->__invoke($this->valueHolder5acaafb3d0450721347842, $this, 'isOpen', array(), $this->initializer5acaafb3d0469289557090);

        return $this->valueHolder5acaafb3d0450721347842->isOpen();
    }

    /**
     * {@inheritDoc}
     */
    public function getUnitOfWork()
    {
        $this->initializer5acaafb3d0469289557090 && $this->initializer5acaafb3d0469289557090->__invoke($this->valueHolder5acaafb3d0450721347842, $this, 'getUnitOfWork', array(), $this->initializer5acaafb3d0469289557090);

        return $this->valueHolder5acaafb3d0450721347842->getUnitOfWork();
    }

    /**
     * {@inheritDoc}
     */
    public function getHydrator($hydrationMode)
    {
        $this->initializer5acaafb3d0469289557090 && $this->initializer5acaafb3d0469289557090->__invoke($this->valueHolder5acaafb3d0450721347842, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializer5acaafb3d0469289557090);

        return $this->valueHolder5acaafb3d0450721347842->getHydrator($hydrationMode);
    }

    /**
     * {@inheritDoc}
     */
    public function newHydrator($hydrationMode)
    {
        $this->initializer5acaafb3d0469289557090 && $this->initializer5acaafb3d0469289557090->__invoke($this->valueHolder5acaafb3d0450721347842, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializer5acaafb3d0469289557090);

        return $this->valueHolder5acaafb3d0450721347842->newHydrator($hydrationMode);
    }

    /**
     * {@inheritDoc}
     */
    public function getProxyFactory()
    {
        $this->initializer5acaafb3d0469289557090 && $this->initializer5acaafb3d0469289557090->__invoke($this->valueHolder5acaafb3d0450721347842, $this, 'getProxyFactory', array(), $this->initializer5acaafb3d0469289557090);

        return $this->valueHolder5acaafb3d0450721347842->getProxyFactory();
    }

    /**
     * {@inheritDoc}
     */
    public function initializeObject($obj)
    {
        $this->initializer5acaafb3d0469289557090 && $this->initializer5acaafb3d0469289557090->__invoke($this->valueHolder5acaafb3d0450721347842, $this, 'initializeObject', array('obj' => $obj), $this->initializer5acaafb3d0469289557090);

        return $this->valueHolder5acaafb3d0450721347842->initializeObject($obj);
    }

    /**
     * {@inheritDoc}
     */
    public function getFilters()
    {
        $this->initializer5acaafb3d0469289557090 && $this->initializer5acaafb3d0469289557090->__invoke($this->valueHolder5acaafb3d0450721347842, $this, 'getFilters', array(), $this->initializer5acaafb3d0469289557090);

        return $this->valueHolder5acaafb3d0450721347842->getFilters();
    }

    /**
     * {@inheritDoc}
     */
    public function isFiltersStateClean()
    {
        $this->initializer5acaafb3d0469289557090 && $this->initializer5acaafb3d0469289557090->__invoke($this->valueHolder5acaafb3d0450721347842, $this, 'isFiltersStateClean', array(), $this->initializer5acaafb3d0469289557090);

        return $this->valueHolder5acaafb3d0450721347842->isFiltersStateClean();
    }

    /**
     * {@inheritDoc}
     */
    public function hasFilters()
    {
        $this->initializer5acaafb3d0469289557090 && $this->initializer5acaafb3d0469289557090->__invoke($this->valueHolder5acaafb3d0450721347842, $this, 'hasFilters', array(), $this->initializer5acaafb3d0469289557090);

        return $this->valueHolder5acaafb3d0450721347842->hasFilters();
    }

    /**
     * @override constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public function __construct($initializer)
    {
        $this->initializer5acaafb3d0469289557090 = $initializer;
    }

    /**
     * @param string $name
     */
    public function & __get($name)
    {
        $this->initializer5acaafb3d0469289557090 && $this->initializer5acaafb3d0469289557090->__invoke($this->valueHolder5acaafb3d0450721347842, $this, '__get', array('name' => $name), $this->initializer5acaafb3d0469289557090);

        if (isset(self::$publicProperties5acaafb3d0412565179754[$name])) {
            return $this->valueHolder5acaafb3d0450721347842->$name;
        }

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5acaafb3d0450721347842;

            $backtrace = debug_backtrace(false);
            trigger_error('Undefined property: ' . get_parent_class($this) . '::$' . $name . ' in ' . $backtrace[0]['file'] . ' on line ' . $backtrace[0]['line'], \E_USER_NOTICE);
            return $targetObject->$name;;
            return;
        }

        $targetObject = $this->valueHolder5acaafb3d0450721347842;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    /**
     * @param string $name
     * @param mixed $value
     */
    public function __set($name, $value)
    {
        $this->initializer5acaafb3d0469289557090 && $this->initializer5acaafb3d0469289557090->__invoke($this->valueHolder5acaafb3d0450721347842, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer5acaafb3d0469289557090);

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5acaafb3d0450721347842;

            return $targetObject->$name = $value;;
            return;
        }

        $targetObject = $this->valueHolder5acaafb3d0450721347842;
        $accessor = function & () use ($targetObject, $name, $value) {
            return $targetObject->$name = $value;
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    /**
     * @param string $name
     */
    public function __isset($name)
    {
        $this->initializer5acaafb3d0469289557090 && $this->initializer5acaafb3d0469289557090->__invoke($this->valueHolder5acaafb3d0450721347842, $this, '__isset', array('name' => $name), $this->initializer5acaafb3d0469289557090);

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5acaafb3d0450721347842;

            return isset($targetObject->$name);;
            return;
        }

        $targetObject = $this->valueHolder5acaafb3d0450721347842;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    /**
     * @param string $name
     */
    public function __unset($name)
    {
        $this->initializer5acaafb3d0469289557090 && $this->initializer5acaafb3d0469289557090->__invoke($this->valueHolder5acaafb3d0450721347842, $this, '__unset', array('name' => $name), $this->initializer5acaafb3d0469289557090);

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5acaafb3d0450721347842;

            unset($targetObject->$name);;
            return;
        }

        $targetObject = $this->valueHolder5acaafb3d0450721347842;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __clone()
    {
        $this->initializer5acaafb3d0469289557090 && $this->initializer5acaafb3d0469289557090->__invoke($this->valueHolder5acaafb3d0450721347842, $this, '__clone', array(), $this->initializer5acaafb3d0469289557090);

        $this->valueHolder5acaafb3d0450721347842 = clone $this->valueHolder5acaafb3d0450721347842;
    }

    public function __sleep()
    {
        $this->initializer5acaafb3d0469289557090 && $this->initializer5acaafb3d0469289557090->__invoke($this->valueHolder5acaafb3d0450721347842, $this, '__sleep', array(), $this->initializer5acaafb3d0469289557090);

        return array('valueHolder5acaafb3d0450721347842');
    }

    public function __wakeup()
    {
    }

    /**
     * {@inheritDoc}
     */
    public function setProxyInitializer(\Closure $initializer = null)
    {
        $this->initializer5acaafb3d0469289557090 = $initializer;
    }

    /**
     * {@inheritDoc}
     */
    public function getProxyInitializer()
    {
        return $this->initializer5acaafb3d0469289557090;
    }

    /**
     * {@inheritDoc}
     */
    public function initializeProxy()
    {
        return $this->initializer5acaafb3d0469289557090 && $this->initializer5acaafb3d0469289557090->__invoke($this->valueHolder5acaafb3d0450721347842, $this, 'initializeProxy', array(), $this->initializer5acaafb3d0469289557090);
    }

    /**
     * {@inheritDoc}
     */
    public function isProxyInitialized()
    {
        return null !== $this->valueHolder5acaafb3d0450721347842;
    }

    /**
     * {@inheritDoc}
     */
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder5acaafb3d0450721347842;
    }


}

class DoctrineORMEntityManager_000000002084057a00000000634d02687471c441876336872902bf356897607e extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $valueHolder5acaafb3e8e9f605099207 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer5acaafb3e8ebd022935843 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties5acaafb3e8e5a640015446 = array(
        
    );

    /**
     * {@inheritDoc}
     */
    public function getConnection()
    {
        $this->initializer5acaafb3e8ebd022935843 && $this->initializer5acaafb3e8ebd022935843->__invoke($this->valueHolder5acaafb3e8e9f605099207, $this, 'getConnection', array(), $this->initializer5acaafb3e8ebd022935843);

        return $this->valueHolder5acaafb3e8e9f605099207->getConnection();
    }

    /**
     * {@inheritDoc}
     */
    public function getMetadataFactory()
    {
        $this->initializer5acaafb3e8ebd022935843 && $this->initializer5acaafb3e8ebd022935843->__invoke($this->valueHolder5acaafb3e8e9f605099207, $this, 'getMetadataFactory', array(), $this->initializer5acaafb3e8ebd022935843);

        return $this->valueHolder5acaafb3e8e9f605099207->getMetadataFactory();
    }

    /**
     * {@inheritDoc}
     */
    public function getExpressionBuilder()
    {
        $this->initializer5acaafb3e8ebd022935843 && $this->initializer5acaafb3e8ebd022935843->__invoke($this->valueHolder5acaafb3e8e9f605099207, $this, 'getExpressionBuilder', array(), $this->initializer5acaafb3e8ebd022935843);

        return $this->valueHolder5acaafb3e8e9f605099207->getExpressionBuilder();
    }

    /**
     * {@inheritDoc}
     */
    public function beginTransaction()
    {
        $this->initializer5acaafb3e8ebd022935843 && $this->initializer5acaafb3e8ebd022935843->__invoke($this->valueHolder5acaafb3e8e9f605099207, $this, 'beginTransaction', array(), $this->initializer5acaafb3e8ebd022935843);

        return $this->valueHolder5acaafb3e8e9f605099207->beginTransaction();
    }

    /**
     * {@inheritDoc}
     */
    public function getCache()
    {
        $this->initializer5acaafb3e8ebd022935843 && $this->initializer5acaafb3e8ebd022935843->__invoke($this->valueHolder5acaafb3e8e9f605099207, $this, 'getCache', array(), $this->initializer5acaafb3e8ebd022935843);

        return $this->valueHolder5acaafb3e8e9f605099207->getCache();
    }

    /**
     * {@inheritDoc}
     */
    public function transactional($func)
    {
        $this->initializer5acaafb3e8ebd022935843 && $this->initializer5acaafb3e8ebd022935843->__invoke($this->valueHolder5acaafb3e8e9f605099207, $this, 'transactional', array('func' => $func), $this->initializer5acaafb3e8ebd022935843);

        return $this->valueHolder5acaafb3e8e9f605099207->transactional($func);
    }

    /**
     * {@inheritDoc}
     */
    public function commit()
    {
        $this->initializer5acaafb3e8ebd022935843 && $this->initializer5acaafb3e8ebd022935843->__invoke($this->valueHolder5acaafb3e8e9f605099207, $this, 'commit', array(), $this->initializer5acaafb3e8ebd022935843);

        return $this->valueHolder5acaafb3e8e9f605099207->commit();
    }

    /**
     * {@inheritDoc}
     */
    public function rollback()
    {
        $this->initializer5acaafb3e8ebd022935843 && $this->initializer5acaafb3e8ebd022935843->__invoke($this->valueHolder5acaafb3e8e9f605099207, $this, 'rollback', array(), $this->initializer5acaafb3e8ebd022935843);

        return $this->valueHolder5acaafb3e8e9f605099207->rollback();
    }

    /**
     * {@inheritDoc}
     */
    public function getClassMetadata($className)
    {
        $this->initializer5acaafb3e8ebd022935843 && $this->initializer5acaafb3e8ebd022935843->__invoke($this->valueHolder5acaafb3e8e9f605099207, $this, 'getClassMetadata', array('className' => $className), $this->initializer5acaafb3e8ebd022935843);

        return $this->valueHolder5acaafb3e8e9f605099207->getClassMetadata($className);
    }

    /**
     * {@inheritDoc}
     */
    public function createQuery($dql = '')
    {
        $this->initializer5acaafb3e8ebd022935843 && $this->initializer5acaafb3e8ebd022935843->__invoke($this->valueHolder5acaafb3e8e9f605099207, $this, 'createQuery', array('dql' => $dql), $this->initializer5acaafb3e8ebd022935843);

        return $this->valueHolder5acaafb3e8e9f605099207->createQuery($dql);
    }

    /**
     * {@inheritDoc}
     */
    public function createNamedQuery($name)
    {
        $this->initializer5acaafb3e8ebd022935843 && $this->initializer5acaafb3e8ebd022935843->__invoke($this->valueHolder5acaafb3e8e9f605099207, $this, 'createNamedQuery', array('name' => $name), $this->initializer5acaafb3e8ebd022935843);

        return $this->valueHolder5acaafb3e8e9f605099207->createNamedQuery($name);
    }

    /**
     * {@inheritDoc}
     */
    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializer5acaafb3e8ebd022935843 && $this->initializer5acaafb3e8ebd022935843->__invoke($this->valueHolder5acaafb3e8e9f605099207, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializer5acaafb3e8ebd022935843);

        return $this->valueHolder5acaafb3e8e9f605099207->createNativeQuery($sql, $rsm);
    }

    /**
     * {@inheritDoc}
     */
    public function createNamedNativeQuery($name)
    {
        $this->initializer5acaafb3e8ebd022935843 && $this->initializer5acaafb3e8ebd022935843->__invoke($this->valueHolder5acaafb3e8e9f605099207, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializer5acaafb3e8ebd022935843);

        return $this->valueHolder5acaafb3e8e9f605099207->createNamedNativeQuery($name);
    }

    /**
     * {@inheritDoc}
     */
    public function createQueryBuilder()
    {
        $this->initializer5acaafb3e8ebd022935843 && $this->initializer5acaafb3e8ebd022935843->__invoke($this->valueHolder5acaafb3e8e9f605099207, $this, 'createQueryBuilder', array(), $this->initializer5acaafb3e8ebd022935843);

        return $this->valueHolder5acaafb3e8e9f605099207->createQueryBuilder();
    }

    /**
     * {@inheritDoc}
     */
    public function flush($entity = null)
    {
        $this->initializer5acaafb3e8ebd022935843 && $this->initializer5acaafb3e8ebd022935843->__invoke($this->valueHolder5acaafb3e8e9f605099207, $this, 'flush', array('entity' => $entity), $this->initializer5acaafb3e8ebd022935843);

        return $this->valueHolder5acaafb3e8e9f605099207->flush($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function find($entityName, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer5acaafb3e8ebd022935843 && $this->initializer5acaafb3e8ebd022935843->__invoke($this->valueHolder5acaafb3e8e9f605099207, $this, 'find', array('entityName' => $entityName, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer5acaafb3e8ebd022935843);

        return $this->valueHolder5acaafb3e8e9f605099207->find($entityName, $id, $lockMode, $lockVersion);
    }

    /**
     * {@inheritDoc}
     */
    public function getReference($entityName, $id)
    {
        $this->initializer5acaafb3e8ebd022935843 && $this->initializer5acaafb3e8ebd022935843->__invoke($this->valueHolder5acaafb3e8e9f605099207, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializer5acaafb3e8ebd022935843);

        return $this->valueHolder5acaafb3e8e9f605099207->getReference($entityName, $id);
    }

    /**
     * {@inheritDoc}
     */
    public function getPartialReference($entityName, $identifier)
    {
        $this->initializer5acaafb3e8ebd022935843 && $this->initializer5acaafb3e8ebd022935843->__invoke($this->valueHolder5acaafb3e8e9f605099207, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializer5acaafb3e8ebd022935843);

        return $this->valueHolder5acaafb3e8e9f605099207->getPartialReference($entityName, $identifier);
    }

    /**
     * {@inheritDoc}
     */
    public function clear($entityName = null)
    {
        $this->initializer5acaafb3e8ebd022935843 && $this->initializer5acaafb3e8ebd022935843->__invoke($this->valueHolder5acaafb3e8e9f605099207, $this, 'clear', array('entityName' => $entityName), $this->initializer5acaafb3e8ebd022935843);

        return $this->valueHolder5acaafb3e8e9f605099207->clear($entityName);
    }

    /**
     * {@inheritDoc}
     */
    public function close()
    {
        $this->initializer5acaafb3e8ebd022935843 && $this->initializer5acaafb3e8ebd022935843->__invoke($this->valueHolder5acaafb3e8e9f605099207, $this, 'close', array(), $this->initializer5acaafb3e8ebd022935843);

        return $this->valueHolder5acaafb3e8e9f605099207->close();
    }

    /**
     * {@inheritDoc}
     */
    public function persist($entity)
    {
        $this->initializer5acaafb3e8ebd022935843 && $this->initializer5acaafb3e8ebd022935843->__invoke($this->valueHolder5acaafb3e8e9f605099207, $this, 'persist', array('entity' => $entity), $this->initializer5acaafb3e8ebd022935843);

        return $this->valueHolder5acaafb3e8e9f605099207->persist($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function remove($entity)
    {
        $this->initializer5acaafb3e8ebd022935843 && $this->initializer5acaafb3e8ebd022935843->__invoke($this->valueHolder5acaafb3e8e9f605099207, $this, 'remove', array('entity' => $entity), $this->initializer5acaafb3e8ebd022935843);

        return $this->valueHolder5acaafb3e8e9f605099207->remove($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function refresh($entity)
    {
        $this->initializer5acaafb3e8ebd022935843 && $this->initializer5acaafb3e8ebd022935843->__invoke($this->valueHolder5acaafb3e8e9f605099207, $this, 'refresh', array('entity' => $entity), $this->initializer5acaafb3e8ebd022935843);

        return $this->valueHolder5acaafb3e8e9f605099207->refresh($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function detach($entity)
    {
        $this->initializer5acaafb3e8ebd022935843 && $this->initializer5acaafb3e8ebd022935843->__invoke($this->valueHolder5acaafb3e8e9f605099207, $this, 'detach', array('entity' => $entity), $this->initializer5acaafb3e8ebd022935843);

        return $this->valueHolder5acaafb3e8e9f605099207->detach($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function merge($entity)
    {
        $this->initializer5acaafb3e8ebd022935843 && $this->initializer5acaafb3e8ebd022935843->__invoke($this->valueHolder5acaafb3e8e9f605099207, $this, 'merge', array('entity' => $entity), $this->initializer5acaafb3e8ebd022935843);

        return $this->valueHolder5acaafb3e8e9f605099207->merge($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function copy($entity, $deep = false)
    {
        $this->initializer5acaafb3e8ebd022935843 && $this->initializer5acaafb3e8ebd022935843->__invoke($this->valueHolder5acaafb3e8e9f605099207, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializer5acaafb3e8ebd022935843);

        return $this->valueHolder5acaafb3e8e9f605099207->copy($entity, $deep);
    }

    /**
     * {@inheritDoc}
     */
    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializer5acaafb3e8ebd022935843 && $this->initializer5acaafb3e8ebd022935843->__invoke($this->valueHolder5acaafb3e8e9f605099207, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer5acaafb3e8ebd022935843);

        return $this->valueHolder5acaafb3e8e9f605099207->lock($entity, $lockMode, $lockVersion);
    }

    /**
     * {@inheritDoc}
     */
    public function getRepository($entityName)
    {
        $this->initializer5acaafb3e8ebd022935843 && $this->initializer5acaafb3e8ebd022935843->__invoke($this->valueHolder5acaafb3e8e9f605099207, $this, 'getRepository', array('entityName' => $entityName), $this->initializer5acaafb3e8ebd022935843);

        return $this->valueHolder5acaafb3e8e9f605099207->getRepository($entityName);
    }

    /**
     * {@inheritDoc}
     */
    public function contains($entity)
    {
        $this->initializer5acaafb3e8ebd022935843 && $this->initializer5acaafb3e8ebd022935843->__invoke($this->valueHolder5acaafb3e8e9f605099207, $this, 'contains', array('entity' => $entity), $this->initializer5acaafb3e8ebd022935843);

        return $this->valueHolder5acaafb3e8e9f605099207->contains($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function getEventManager()
    {
        $this->initializer5acaafb3e8ebd022935843 && $this->initializer5acaafb3e8ebd022935843->__invoke($this->valueHolder5acaafb3e8e9f605099207, $this, 'getEventManager', array(), $this->initializer5acaafb3e8ebd022935843);

        return $this->valueHolder5acaafb3e8e9f605099207->getEventManager();
    }

    /**
     * {@inheritDoc}
     */
    public function getConfiguration()
    {
        $this->initializer5acaafb3e8ebd022935843 && $this->initializer5acaafb3e8ebd022935843->__invoke($this->valueHolder5acaafb3e8e9f605099207, $this, 'getConfiguration', array(), $this->initializer5acaafb3e8ebd022935843);

        return $this->valueHolder5acaafb3e8e9f605099207->getConfiguration();
    }

    /**
     * {@inheritDoc}
     */
    public function isOpen()
    {
        $this->initializer5acaafb3e8ebd022935843 && $this->initializer5acaafb3e8ebd022935843->__invoke($this->valueHolder5acaafb3e8e9f605099207, $this, 'isOpen', array(), $this->initializer5acaafb3e8ebd022935843);

        return $this->valueHolder5acaafb3e8e9f605099207->isOpen();
    }

    /**
     * {@inheritDoc}
     */
    public function getUnitOfWork()
    {
        $this->initializer5acaafb3e8ebd022935843 && $this->initializer5acaafb3e8ebd022935843->__invoke($this->valueHolder5acaafb3e8e9f605099207, $this, 'getUnitOfWork', array(), $this->initializer5acaafb3e8ebd022935843);

        return $this->valueHolder5acaafb3e8e9f605099207->getUnitOfWork();
    }

    /**
     * {@inheritDoc}
     */
    public function getHydrator($hydrationMode)
    {
        $this->initializer5acaafb3e8ebd022935843 && $this->initializer5acaafb3e8ebd022935843->__invoke($this->valueHolder5acaafb3e8e9f605099207, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializer5acaafb3e8ebd022935843);

        return $this->valueHolder5acaafb3e8e9f605099207->getHydrator($hydrationMode);
    }

    /**
     * {@inheritDoc}
     */
    public function newHydrator($hydrationMode)
    {
        $this->initializer5acaafb3e8ebd022935843 && $this->initializer5acaafb3e8ebd022935843->__invoke($this->valueHolder5acaafb3e8e9f605099207, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializer5acaafb3e8ebd022935843);

        return $this->valueHolder5acaafb3e8e9f605099207->newHydrator($hydrationMode);
    }

    /**
     * {@inheritDoc}
     */
    public function getProxyFactory()
    {
        $this->initializer5acaafb3e8ebd022935843 && $this->initializer5acaafb3e8ebd022935843->__invoke($this->valueHolder5acaafb3e8e9f605099207, $this, 'getProxyFactory', array(), $this->initializer5acaafb3e8ebd022935843);

        return $this->valueHolder5acaafb3e8e9f605099207->getProxyFactory();
    }

    /**
     * {@inheritDoc}
     */
    public function initializeObject($obj)
    {
        $this->initializer5acaafb3e8ebd022935843 && $this->initializer5acaafb3e8ebd022935843->__invoke($this->valueHolder5acaafb3e8e9f605099207, $this, 'initializeObject', array('obj' => $obj), $this->initializer5acaafb3e8ebd022935843);

        return $this->valueHolder5acaafb3e8e9f605099207->initializeObject($obj);
    }

    /**
     * {@inheritDoc}
     */
    public function getFilters()
    {
        $this->initializer5acaafb3e8ebd022935843 && $this->initializer5acaafb3e8ebd022935843->__invoke($this->valueHolder5acaafb3e8e9f605099207, $this, 'getFilters', array(), $this->initializer5acaafb3e8ebd022935843);

        return $this->valueHolder5acaafb3e8e9f605099207->getFilters();
    }

    /**
     * {@inheritDoc}
     */
    public function isFiltersStateClean()
    {
        $this->initializer5acaafb3e8ebd022935843 && $this->initializer5acaafb3e8ebd022935843->__invoke($this->valueHolder5acaafb3e8e9f605099207, $this, 'isFiltersStateClean', array(), $this->initializer5acaafb3e8ebd022935843);

        return $this->valueHolder5acaafb3e8e9f605099207->isFiltersStateClean();
    }

    /**
     * {@inheritDoc}
     */
    public function hasFilters()
    {
        $this->initializer5acaafb3e8ebd022935843 && $this->initializer5acaafb3e8ebd022935843->__invoke($this->valueHolder5acaafb3e8e9f605099207, $this, 'hasFilters', array(), $this->initializer5acaafb3e8ebd022935843);

        return $this->valueHolder5acaafb3e8e9f605099207->hasFilters();
    }

    /**
     * @override constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public function __construct($initializer)
    {
        $this->initializer5acaafb3e8ebd022935843 = $initializer;
    }

    /**
     * @param string $name
     */
    public function & __get($name)
    {
        $this->initializer5acaafb3e8ebd022935843 && $this->initializer5acaafb3e8ebd022935843->__invoke($this->valueHolder5acaafb3e8e9f605099207, $this, '__get', array('name' => $name), $this->initializer5acaafb3e8ebd022935843);

        if (isset(self::$publicProperties5acaafb3e8e5a640015446[$name])) {
            return $this->valueHolder5acaafb3e8e9f605099207->$name;
        }

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5acaafb3e8e9f605099207;

            $backtrace = debug_backtrace(false);
            trigger_error('Undefined property: ' . get_parent_class($this) . '::$' . $name . ' in ' . $backtrace[0]['file'] . ' on line ' . $backtrace[0]['line'], \E_USER_NOTICE);
            return $targetObject->$name;;
            return;
        }

        $targetObject = $this->valueHolder5acaafb3e8e9f605099207;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    /**
     * @param string $name
     * @param mixed $value
     */
    public function __set($name, $value)
    {
        $this->initializer5acaafb3e8ebd022935843 && $this->initializer5acaafb3e8ebd022935843->__invoke($this->valueHolder5acaafb3e8e9f605099207, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer5acaafb3e8ebd022935843);

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5acaafb3e8e9f605099207;

            return $targetObject->$name = $value;;
            return;
        }

        $targetObject = $this->valueHolder5acaafb3e8e9f605099207;
        $accessor = function & () use ($targetObject, $name, $value) {
            return $targetObject->$name = $value;
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    /**
     * @param string $name
     */
    public function __isset($name)
    {
        $this->initializer5acaafb3e8ebd022935843 && $this->initializer5acaafb3e8ebd022935843->__invoke($this->valueHolder5acaafb3e8e9f605099207, $this, '__isset', array('name' => $name), $this->initializer5acaafb3e8ebd022935843);

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5acaafb3e8e9f605099207;

            return isset($targetObject->$name);;
            return;
        }

        $targetObject = $this->valueHolder5acaafb3e8e9f605099207;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    /**
     * @param string $name
     */
    public function __unset($name)
    {
        $this->initializer5acaafb3e8ebd022935843 && $this->initializer5acaafb3e8ebd022935843->__invoke($this->valueHolder5acaafb3e8e9f605099207, $this, '__unset', array('name' => $name), $this->initializer5acaafb3e8ebd022935843);

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5acaafb3e8e9f605099207;

            unset($targetObject->$name);;
            return;
        }

        $targetObject = $this->valueHolder5acaafb3e8e9f605099207;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __clone()
    {
        $this->initializer5acaafb3e8ebd022935843 && $this->initializer5acaafb3e8ebd022935843->__invoke($this->valueHolder5acaafb3e8e9f605099207, $this, '__clone', array(), $this->initializer5acaafb3e8ebd022935843);

        $this->valueHolder5acaafb3e8e9f605099207 = clone $this->valueHolder5acaafb3e8e9f605099207;
    }

    public function __sleep()
    {
        $this->initializer5acaafb3e8ebd022935843 && $this->initializer5acaafb3e8ebd022935843->__invoke($this->valueHolder5acaafb3e8e9f605099207, $this, '__sleep', array(), $this->initializer5acaafb3e8ebd022935843);

        return array('valueHolder5acaafb3e8e9f605099207');
    }

    public function __wakeup()
    {
    }

    /**
     * {@inheritDoc}
     */
    public function setProxyInitializer(\Closure $initializer = null)
    {
        $this->initializer5acaafb3e8ebd022935843 = $initializer;
    }

    /**
     * {@inheritDoc}
     */
    public function getProxyInitializer()
    {
        return $this->initializer5acaafb3e8ebd022935843;
    }

    /**
     * {@inheritDoc}
     */
    public function initializeProxy()
    {
        return $this->initializer5acaafb3e8ebd022935843 && $this->initializer5acaafb3e8ebd022935843->__invoke($this->valueHolder5acaafb3e8e9f605099207, $this, 'initializeProxy', array(), $this->initializer5acaafb3e8ebd022935843);
    }

    /**
     * {@inheritDoc}
     */
    public function isProxyInitialized()
    {
        return null !== $this->valueHolder5acaafb3e8e9f605099207;
    }

    /**
     * {@inheritDoc}
     */
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder5acaafb3e8e9f605099207;
    }


}

class DoctrineORMEntityManager_000000002084057c00000000634d02687471c441876336872902bf356897607e extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $valueHolder5acaafb40b70c423078331 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer5acaafb40b728032179573 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties5acaafb40b6cc087424933 = array(
        
    );

    /**
     * {@inheritDoc}
     */
    public function getConnection()
    {
        $this->initializer5acaafb40b728032179573 && $this->initializer5acaafb40b728032179573->__invoke($this->valueHolder5acaafb40b70c423078331, $this, 'getConnection', array(), $this->initializer5acaafb40b728032179573);

        return $this->valueHolder5acaafb40b70c423078331->getConnection();
    }

    /**
     * {@inheritDoc}
     */
    public function getMetadataFactory()
    {
        $this->initializer5acaafb40b728032179573 && $this->initializer5acaafb40b728032179573->__invoke($this->valueHolder5acaafb40b70c423078331, $this, 'getMetadataFactory', array(), $this->initializer5acaafb40b728032179573);

        return $this->valueHolder5acaafb40b70c423078331->getMetadataFactory();
    }

    /**
     * {@inheritDoc}
     */
    public function getExpressionBuilder()
    {
        $this->initializer5acaafb40b728032179573 && $this->initializer5acaafb40b728032179573->__invoke($this->valueHolder5acaafb40b70c423078331, $this, 'getExpressionBuilder', array(), $this->initializer5acaafb40b728032179573);

        return $this->valueHolder5acaafb40b70c423078331->getExpressionBuilder();
    }

    /**
     * {@inheritDoc}
     */
    public function beginTransaction()
    {
        $this->initializer5acaafb40b728032179573 && $this->initializer5acaafb40b728032179573->__invoke($this->valueHolder5acaafb40b70c423078331, $this, 'beginTransaction', array(), $this->initializer5acaafb40b728032179573);

        return $this->valueHolder5acaafb40b70c423078331->beginTransaction();
    }

    /**
     * {@inheritDoc}
     */
    public function getCache()
    {
        $this->initializer5acaafb40b728032179573 && $this->initializer5acaafb40b728032179573->__invoke($this->valueHolder5acaafb40b70c423078331, $this, 'getCache', array(), $this->initializer5acaafb40b728032179573);

        return $this->valueHolder5acaafb40b70c423078331->getCache();
    }

    /**
     * {@inheritDoc}
     */
    public function transactional($func)
    {
        $this->initializer5acaafb40b728032179573 && $this->initializer5acaafb40b728032179573->__invoke($this->valueHolder5acaafb40b70c423078331, $this, 'transactional', array('func' => $func), $this->initializer5acaafb40b728032179573);

        return $this->valueHolder5acaafb40b70c423078331->transactional($func);
    }

    /**
     * {@inheritDoc}
     */
    public function commit()
    {
        $this->initializer5acaafb40b728032179573 && $this->initializer5acaafb40b728032179573->__invoke($this->valueHolder5acaafb40b70c423078331, $this, 'commit', array(), $this->initializer5acaafb40b728032179573);

        return $this->valueHolder5acaafb40b70c423078331->commit();
    }

    /**
     * {@inheritDoc}
     */
    public function rollback()
    {
        $this->initializer5acaafb40b728032179573 && $this->initializer5acaafb40b728032179573->__invoke($this->valueHolder5acaafb40b70c423078331, $this, 'rollback', array(), $this->initializer5acaafb40b728032179573);

        return $this->valueHolder5acaafb40b70c423078331->rollback();
    }

    /**
     * {@inheritDoc}
     */
    public function getClassMetadata($className)
    {
        $this->initializer5acaafb40b728032179573 && $this->initializer5acaafb40b728032179573->__invoke($this->valueHolder5acaafb40b70c423078331, $this, 'getClassMetadata', array('className' => $className), $this->initializer5acaafb40b728032179573);

        return $this->valueHolder5acaafb40b70c423078331->getClassMetadata($className);
    }

    /**
     * {@inheritDoc}
     */
    public function createQuery($dql = '')
    {
        $this->initializer5acaafb40b728032179573 && $this->initializer5acaafb40b728032179573->__invoke($this->valueHolder5acaafb40b70c423078331, $this, 'createQuery', array('dql' => $dql), $this->initializer5acaafb40b728032179573);

        return $this->valueHolder5acaafb40b70c423078331->createQuery($dql);
    }

    /**
     * {@inheritDoc}
     */
    public function createNamedQuery($name)
    {
        $this->initializer5acaafb40b728032179573 && $this->initializer5acaafb40b728032179573->__invoke($this->valueHolder5acaafb40b70c423078331, $this, 'createNamedQuery', array('name' => $name), $this->initializer5acaafb40b728032179573);

        return $this->valueHolder5acaafb40b70c423078331->createNamedQuery($name);
    }

    /**
     * {@inheritDoc}
     */
    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializer5acaafb40b728032179573 && $this->initializer5acaafb40b728032179573->__invoke($this->valueHolder5acaafb40b70c423078331, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializer5acaafb40b728032179573);

        return $this->valueHolder5acaafb40b70c423078331->createNativeQuery($sql, $rsm);
    }

    /**
     * {@inheritDoc}
     */
    public function createNamedNativeQuery($name)
    {
        $this->initializer5acaafb40b728032179573 && $this->initializer5acaafb40b728032179573->__invoke($this->valueHolder5acaafb40b70c423078331, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializer5acaafb40b728032179573);

        return $this->valueHolder5acaafb40b70c423078331->createNamedNativeQuery($name);
    }

    /**
     * {@inheritDoc}
     */
    public function createQueryBuilder()
    {
        $this->initializer5acaafb40b728032179573 && $this->initializer5acaafb40b728032179573->__invoke($this->valueHolder5acaafb40b70c423078331, $this, 'createQueryBuilder', array(), $this->initializer5acaafb40b728032179573);

        return $this->valueHolder5acaafb40b70c423078331->createQueryBuilder();
    }

    /**
     * {@inheritDoc}
     */
    public function flush($entity = null)
    {
        $this->initializer5acaafb40b728032179573 && $this->initializer5acaafb40b728032179573->__invoke($this->valueHolder5acaafb40b70c423078331, $this, 'flush', array('entity' => $entity), $this->initializer5acaafb40b728032179573);

        return $this->valueHolder5acaafb40b70c423078331->flush($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function find($entityName, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer5acaafb40b728032179573 && $this->initializer5acaafb40b728032179573->__invoke($this->valueHolder5acaafb40b70c423078331, $this, 'find', array('entityName' => $entityName, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer5acaafb40b728032179573);

        return $this->valueHolder5acaafb40b70c423078331->find($entityName, $id, $lockMode, $lockVersion);
    }

    /**
     * {@inheritDoc}
     */
    public function getReference($entityName, $id)
    {
        $this->initializer5acaafb40b728032179573 && $this->initializer5acaafb40b728032179573->__invoke($this->valueHolder5acaafb40b70c423078331, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializer5acaafb40b728032179573);

        return $this->valueHolder5acaafb40b70c423078331->getReference($entityName, $id);
    }

    /**
     * {@inheritDoc}
     */
    public function getPartialReference($entityName, $identifier)
    {
        $this->initializer5acaafb40b728032179573 && $this->initializer5acaafb40b728032179573->__invoke($this->valueHolder5acaafb40b70c423078331, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializer5acaafb40b728032179573);

        return $this->valueHolder5acaafb40b70c423078331->getPartialReference($entityName, $identifier);
    }

    /**
     * {@inheritDoc}
     */
    public function clear($entityName = null)
    {
        $this->initializer5acaafb40b728032179573 && $this->initializer5acaafb40b728032179573->__invoke($this->valueHolder5acaafb40b70c423078331, $this, 'clear', array('entityName' => $entityName), $this->initializer5acaafb40b728032179573);

        return $this->valueHolder5acaafb40b70c423078331->clear($entityName);
    }

    /**
     * {@inheritDoc}
     */
    public function close()
    {
        $this->initializer5acaafb40b728032179573 && $this->initializer5acaafb40b728032179573->__invoke($this->valueHolder5acaafb40b70c423078331, $this, 'close', array(), $this->initializer5acaafb40b728032179573);

        return $this->valueHolder5acaafb40b70c423078331->close();
    }

    /**
     * {@inheritDoc}
     */
    public function persist($entity)
    {
        $this->initializer5acaafb40b728032179573 && $this->initializer5acaafb40b728032179573->__invoke($this->valueHolder5acaafb40b70c423078331, $this, 'persist', array('entity' => $entity), $this->initializer5acaafb40b728032179573);

        return $this->valueHolder5acaafb40b70c423078331->persist($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function remove($entity)
    {
        $this->initializer5acaafb40b728032179573 && $this->initializer5acaafb40b728032179573->__invoke($this->valueHolder5acaafb40b70c423078331, $this, 'remove', array('entity' => $entity), $this->initializer5acaafb40b728032179573);

        return $this->valueHolder5acaafb40b70c423078331->remove($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function refresh($entity)
    {
        $this->initializer5acaafb40b728032179573 && $this->initializer5acaafb40b728032179573->__invoke($this->valueHolder5acaafb40b70c423078331, $this, 'refresh', array('entity' => $entity), $this->initializer5acaafb40b728032179573);

        return $this->valueHolder5acaafb40b70c423078331->refresh($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function detach($entity)
    {
        $this->initializer5acaafb40b728032179573 && $this->initializer5acaafb40b728032179573->__invoke($this->valueHolder5acaafb40b70c423078331, $this, 'detach', array('entity' => $entity), $this->initializer5acaafb40b728032179573);

        return $this->valueHolder5acaafb40b70c423078331->detach($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function merge($entity)
    {
        $this->initializer5acaafb40b728032179573 && $this->initializer5acaafb40b728032179573->__invoke($this->valueHolder5acaafb40b70c423078331, $this, 'merge', array('entity' => $entity), $this->initializer5acaafb40b728032179573);

        return $this->valueHolder5acaafb40b70c423078331->merge($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function copy($entity, $deep = false)
    {
        $this->initializer5acaafb40b728032179573 && $this->initializer5acaafb40b728032179573->__invoke($this->valueHolder5acaafb40b70c423078331, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializer5acaafb40b728032179573);

        return $this->valueHolder5acaafb40b70c423078331->copy($entity, $deep);
    }

    /**
     * {@inheritDoc}
     */
    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializer5acaafb40b728032179573 && $this->initializer5acaafb40b728032179573->__invoke($this->valueHolder5acaafb40b70c423078331, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer5acaafb40b728032179573);

        return $this->valueHolder5acaafb40b70c423078331->lock($entity, $lockMode, $lockVersion);
    }

    /**
     * {@inheritDoc}
     */
    public function getRepository($entityName)
    {
        $this->initializer5acaafb40b728032179573 && $this->initializer5acaafb40b728032179573->__invoke($this->valueHolder5acaafb40b70c423078331, $this, 'getRepository', array('entityName' => $entityName), $this->initializer5acaafb40b728032179573);

        return $this->valueHolder5acaafb40b70c423078331->getRepository($entityName);
    }

    /**
     * {@inheritDoc}
     */
    public function contains($entity)
    {
        $this->initializer5acaafb40b728032179573 && $this->initializer5acaafb40b728032179573->__invoke($this->valueHolder5acaafb40b70c423078331, $this, 'contains', array('entity' => $entity), $this->initializer5acaafb40b728032179573);

        return $this->valueHolder5acaafb40b70c423078331->contains($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function getEventManager()
    {
        $this->initializer5acaafb40b728032179573 && $this->initializer5acaafb40b728032179573->__invoke($this->valueHolder5acaafb40b70c423078331, $this, 'getEventManager', array(), $this->initializer5acaafb40b728032179573);

        return $this->valueHolder5acaafb40b70c423078331->getEventManager();
    }

    /**
     * {@inheritDoc}
     */
    public function getConfiguration()
    {
        $this->initializer5acaafb40b728032179573 && $this->initializer5acaafb40b728032179573->__invoke($this->valueHolder5acaafb40b70c423078331, $this, 'getConfiguration', array(), $this->initializer5acaafb40b728032179573);

        return $this->valueHolder5acaafb40b70c423078331->getConfiguration();
    }

    /**
     * {@inheritDoc}
     */
    public function isOpen()
    {
        $this->initializer5acaafb40b728032179573 && $this->initializer5acaafb40b728032179573->__invoke($this->valueHolder5acaafb40b70c423078331, $this, 'isOpen', array(), $this->initializer5acaafb40b728032179573);

        return $this->valueHolder5acaafb40b70c423078331->isOpen();
    }

    /**
     * {@inheritDoc}
     */
    public function getUnitOfWork()
    {
        $this->initializer5acaafb40b728032179573 && $this->initializer5acaafb40b728032179573->__invoke($this->valueHolder5acaafb40b70c423078331, $this, 'getUnitOfWork', array(), $this->initializer5acaafb40b728032179573);

        return $this->valueHolder5acaafb40b70c423078331->getUnitOfWork();
    }

    /**
     * {@inheritDoc}
     */
    public function getHydrator($hydrationMode)
    {
        $this->initializer5acaafb40b728032179573 && $this->initializer5acaafb40b728032179573->__invoke($this->valueHolder5acaafb40b70c423078331, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializer5acaafb40b728032179573);

        return $this->valueHolder5acaafb40b70c423078331->getHydrator($hydrationMode);
    }

    /**
     * {@inheritDoc}
     */
    public function newHydrator($hydrationMode)
    {
        $this->initializer5acaafb40b728032179573 && $this->initializer5acaafb40b728032179573->__invoke($this->valueHolder5acaafb40b70c423078331, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializer5acaafb40b728032179573);

        return $this->valueHolder5acaafb40b70c423078331->newHydrator($hydrationMode);
    }

    /**
     * {@inheritDoc}
     */
    public function getProxyFactory()
    {
        $this->initializer5acaafb40b728032179573 && $this->initializer5acaafb40b728032179573->__invoke($this->valueHolder5acaafb40b70c423078331, $this, 'getProxyFactory', array(), $this->initializer5acaafb40b728032179573);

        return $this->valueHolder5acaafb40b70c423078331->getProxyFactory();
    }

    /**
     * {@inheritDoc}
     */
    public function initializeObject($obj)
    {
        $this->initializer5acaafb40b728032179573 && $this->initializer5acaafb40b728032179573->__invoke($this->valueHolder5acaafb40b70c423078331, $this, 'initializeObject', array('obj' => $obj), $this->initializer5acaafb40b728032179573);

        return $this->valueHolder5acaafb40b70c423078331->initializeObject($obj);
    }

    /**
     * {@inheritDoc}
     */
    public function getFilters()
    {
        $this->initializer5acaafb40b728032179573 && $this->initializer5acaafb40b728032179573->__invoke($this->valueHolder5acaafb40b70c423078331, $this, 'getFilters', array(), $this->initializer5acaafb40b728032179573);

        return $this->valueHolder5acaafb40b70c423078331->getFilters();
    }

    /**
     * {@inheritDoc}
     */
    public function isFiltersStateClean()
    {
        $this->initializer5acaafb40b728032179573 && $this->initializer5acaafb40b728032179573->__invoke($this->valueHolder5acaafb40b70c423078331, $this, 'isFiltersStateClean', array(), $this->initializer5acaafb40b728032179573);

        return $this->valueHolder5acaafb40b70c423078331->isFiltersStateClean();
    }

    /**
     * {@inheritDoc}
     */
    public function hasFilters()
    {
        $this->initializer5acaafb40b728032179573 && $this->initializer5acaafb40b728032179573->__invoke($this->valueHolder5acaafb40b70c423078331, $this, 'hasFilters', array(), $this->initializer5acaafb40b728032179573);

        return $this->valueHolder5acaafb40b70c423078331->hasFilters();
    }

    /**
     * @override constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public function __construct($initializer)
    {
        $this->initializer5acaafb40b728032179573 = $initializer;
    }

    /**
     * @param string $name
     */
    public function & __get($name)
    {
        $this->initializer5acaafb40b728032179573 && $this->initializer5acaafb40b728032179573->__invoke($this->valueHolder5acaafb40b70c423078331, $this, '__get', array('name' => $name), $this->initializer5acaafb40b728032179573);

        if (isset(self::$publicProperties5acaafb40b6cc087424933[$name])) {
            return $this->valueHolder5acaafb40b70c423078331->$name;
        }

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5acaafb40b70c423078331;

            $backtrace = debug_backtrace(false);
            trigger_error('Undefined property: ' . get_parent_class($this) . '::$' . $name . ' in ' . $backtrace[0]['file'] . ' on line ' . $backtrace[0]['line'], \E_USER_NOTICE);
            return $targetObject->$name;;
            return;
        }

        $targetObject = $this->valueHolder5acaafb40b70c423078331;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    /**
     * @param string $name
     * @param mixed $value
     */
    public function __set($name, $value)
    {
        $this->initializer5acaafb40b728032179573 && $this->initializer5acaafb40b728032179573->__invoke($this->valueHolder5acaafb40b70c423078331, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer5acaafb40b728032179573);

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5acaafb40b70c423078331;

            return $targetObject->$name = $value;;
            return;
        }

        $targetObject = $this->valueHolder5acaafb40b70c423078331;
        $accessor = function & () use ($targetObject, $name, $value) {
            return $targetObject->$name = $value;
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    /**
     * @param string $name
     */
    public function __isset($name)
    {
        $this->initializer5acaafb40b728032179573 && $this->initializer5acaafb40b728032179573->__invoke($this->valueHolder5acaafb40b70c423078331, $this, '__isset', array('name' => $name), $this->initializer5acaafb40b728032179573);

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5acaafb40b70c423078331;

            return isset($targetObject->$name);;
            return;
        }

        $targetObject = $this->valueHolder5acaafb40b70c423078331;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    /**
     * @param string $name
     */
    public function __unset($name)
    {
        $this->initializer5acaafb40b728032179573 && $this->initializer5acaafb40b728032179573->__invoke($this->valueHolder5acaafb40b70c423078331, $this, '__unset', array('name' => $name), $this->initializer5acaafb40b728032179573);

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5acaafb40b70c423078331;

            unset($targetObject->$name);;
            return;
        }

        $targetObject = $this->valueHolder5acaafb40b70c423078331;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __clone()
    {
        $this->initializer5acaafb40b728032179573 && $this->initializer5acaafb40b728032179573->__invoke($this->valueHolder5acaafb40b70c423078331, $this, '__clone', array(), $this->initializer5acaafb40b728032179573);

        $this->valueHolder5acaafb40b70c423078331 = clone $this->valueHolder5acaafb40b70c423078331;
    }

    public function __sleep()
    {
        $this->initializer5acaafb40b728032179573 && $this->initializer5acaafb40b728032179573->__invoke($this->valueHolder5acaafb40b70c423078331, $this, '__sleep', array(), $this->initializer5acaafb40b728032179573);

        return array('valueHolder5acaafb40b70c423078331');
    }

    public function __wakeup()
    {
    }

    /**
     * {@inheritDoc}
     */
    public function setProxyInitializer(\Closure $initializer = null)
    {
        $this->initializer5acaafb40b728032179573 = $initializer;
    }

    /**
     * {@inheritDoc}
     */
    public function getProxyInitializer()
    {
        return $this->initializer5acaafb40b728032179573;
    }

    /**
     * {@inheritDoc}
     */
    public function initializeProxy()
    {
        return $this->initializer5acaafb40b728032179573 && $this->initializer5acaafb40b728032179573->__invoke($this->valueHolder5acaafb40b70c423078331, $this, 'initializeProxy', array(), $this->initializer5acaafb40b728032179573);
    }

    /**
     * {@inheritDoc}
     */
    public function isProxyInitialized()
    {
        return null !== $this->valueHolder5acaafb40b70c423078331;
    }

    /**
     * {@inheritDoc}
     */
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder5acaafb40b70c423078331;
    }


}

class DoctrineORMEntityManager_000000002084057600000000634d02687471c441876336872902bf356897607e extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $valueHolder5acaafb420d42706900702 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer5acaafb420d63917566210 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties5acaafb420c9d717299721 = array(
        
    );

    /**
     * {@inheritDoc}
     */
    public function getConnection()
    {
        $this->initializer5acaafb420d63917566210 && $this->initializer5acaafb420d63917566210->__invoke($this->valueHolder5acaafb420d42706900702, $this, 'getConnection', array(), $this->initializer5acaafb420d63917566210);

        return $this->valueHolder5acaafb420d42706900702->getConnection();
    }

    /**
     * {@inheritDoc}
     */
    public function getMetadataFactory()
    {
        $this->initializer5acaafb420d63917566210 && $this->initializer5acaafb420d63917566210->__invoke($this->valueHolder5acaafb420d42706900702, $this, 'getMetadataFactory', array(), $this->initializer5acaafb420d63917566210);

        return $this->valueHolder5acaafb420d42706900702->getMetadataFactory();
    }

    /**
     * {@inheritDoc}
     */
    public function getExpressionBuilder()
    {
        $this->initializer5acaafb420d63917566210 && $this->initializer5acaafb420d63917566210->__invoke($this->valueHolder5acaafb420d42706900702, $this, 'getExpressionBuilder', array(), $this->initializer5acaafb420d63917566210);

        return $this->valueHolder5acaafb420d42706900702->getExpressionBuilder();
    }

    /**
     * {@inheritDoc}
     */
    public function beginTransaction()
    {
        $this->initializer5acaafb420d63917566210 && $this->initializer5acaafb420d63917566210->__invoke($this->valueHolder5acaafb420d42706900702, $this, 'beginTransaction', array(), $this->initializer5acaafb420d63917566210);

        return $this->valueHolder5acaafb420d42706900702->beginTransaction();
    }

    /**
     * {@inheritDoc}
     */
    public function getCache()
    {
        $this->initializer5acaafb420d63917566210 && $this->initializer5acaafb420d63917566210->__invoke($this->valueHolder5acaafb420d42706900702, $this, 'getCache', array(), $this->initializer5acaafb420d63917566210);

        return $this->valueHolder5acaafb420d42706900702->getCache();
    }

    /**
     * {@inheritDoc}
     */
    public function transactional($func)
    {
        $this->initializer5acaafb420d63917566210 && $this->initializer5acaafb420d63917566210->__invoke($this->valueHolder5acaafb420d42706900702, $this, 'transactional', array('func' => $func), $this->initializer5acaafb420d63917566210);

        return $this->valueHolder5acaafb420d42706900702->transactional($func);
    }

    /**
     * {@inheritDoc}
     */
    public function commit()
    {
        $this->initializer5acaafb420d63917566210 && $this->initializer5acaafb420d63917566210->__invoke($this->valueHolder5acaafb420d42706900702, $this, 'commit', array(), $this->initializer5acaafb420d63917566210);

        return $this->valueHolder5acaafb420d42706900702->commit();
    }

    /**
     * {@inheritDoc}
     */
    public function rollback()
    {
        $this->initializer5acaafb420d63917566210 && $this->initializer5acaafb420d63917566210->__invoke($this->valueHolder5acaafb420d42706900702, $this, 'rollback', array(), $this->initializer5acaafb420d63917566210);

        return $this->valueHolder5acaafb420d42706900702->rollback();
    }

    /**
     * {@inheritDoc}
     */
    public function getClassMetadata($className)
    {
        $this->initializer5acaafb420d63917566210 && $this->initializer5acaafb420d63917566210->__invoke($this->valueHolder5acaafb420d42706900702, $this, 'getClassMetadata', array('className' => $className), $this->initializer5acaafb420d63917566210);

        return $this->valueHolder5acaafb420d42706900702->getClassMetadata($className);
    }

    /**
     * {@inheritDoc}
     */
    public function createQuery($dql = '')
    {
        $this->initializer5acaafb420d63917566210 && $this->initializer5acaafb420d63917566210->__invoke($this->valueHolder5acaafb420d42706900702, $this, 'createQuery', array('dql' => $dql), $this->initializer5acaafb420d63917566210);

        return $this->valueHolder5acaafb420d42706900702->createQuery($dql);
    }

    /**
     * {@inheritDoc}
     */
    public function createNamedQuery($name)
    {
        $this->initializer5acaafb420d63917566210 && $this->initializer5acaafb420d63917566210->__invoke($this->valueHolder5acaafb420d42706900702, $this, 'createNamedQuery', array('name' => $name), $this->initializer5acaafb420d63917566210);

        return $this->valueHolder5acaafb420d42706900702->createNamedQuery($name);
    }

    /**
     * {@inheritDoc}
     */
    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializer5acaafb420d63917566210 && $this->initializer5acaafb420d63917566210->__invoke($this->valueHolder5acaafb420d42706900702, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializer5acaafb420d63917566210);

        return $this->valueHolder5acaafb420d42706900702->createNativeQuery($sql, $rsm);
    }

    /**
     * {@inheritDoc}
     */
    public function createNamedNativeQuery($name)
    {
        $this->initializer5acaafb420d63917566210 && $this->initializer5acaafb420d63917566210->__invoke($this->valueHolder5acaafb420d42706900702, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializer5acaafb420d63917566210);

        return $this->valueHolder5acaafb420d42706900702->createNamedNativeQuery($name);
    }

    /**
     * {@inheritDoc}
     */
    public function createQueryBuilder()
    {
        $this->initializer5acaafb420d63917566210 && $this->initializer5acaafb420d63917566210->__invoke($this->valueHolder5acaafb420d42706900702, $this, 'createQueryBuilder', array(), $this->initializer5acaafb420d63917566210);

        return $this->valueHolder5acaafb420d42706900702->createQueryBuilder();
    }

    /**
     * {@inheritDoc}
     */
    public function flush($entity = null)
    {
        $this->initializer5acaafb420d63917566210 && $this->initializer5acaafb420d63917566210->__invoke($this->valueHolder5acaafb420d42706900702, $this, 'flush', array('entity' => $entity), $this->initializer5acaafb420d63917566210);

        return $this->valueHolder5acaafb420d42706900702->flush($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function find($entityName, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer5acaafb420d63917566210 && $this->initializer5acaafb420d63917566210->__invoke($this->valueHolder5acaafb420d42706900702, $this, 'find', array('entityName' => $entityName, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer5acaafb420d63917566210);

        return $this->valueHolder5acaafb420d42706900702->find($entityName, $id, $lockMode, $lockVersion);
    }

    /**
     * {@inheritDoc}
     */
    public function getReference($entityName, $id)
    {
        $this->initializer5acaafb420d63917566210 && $this->initializer5acaafb420d63917566210->__invoke($this->valueHolder5acaafb420d42706900702, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializer5acaafb420d63917566210);

        return $this->valueHolder5acaafb420d42706900702->getReference($entityName, $id);
    }

    /**
     * {@inheritDoc}
     */
    public function getPartialReference($entityName, $identifier)
    {
        $this->initializer5acaafb420d63917566210 && $this->initializer5acaafb420d63917566210->__invoke($this->valueHolder5acaafb420d42706900702, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializer5acaafb420d63917566210);

        return $this->valueHolder5acaafb420d42706900702->getPartialReference($entityName, $identifier);
    }

    /**
     * {@inheritDoc}
     */
    public function clear($entityName = null)
    {
        $this->initializer5acaafb420d63917566210 && $this->initializer5acaafb420d63917566210->__invoke($this->valueHolder5acaafb420d42706900702, $this, 'clear', array('entityName' => $entityName), $this->initializer5acaafb420d63917566210);

        return $this->valueHolder5acaafb420d42706900702->clear($entityName);
    }

    /**
     * {@inheritDoc}
     */
    public function close()
    {
        $this->initializer5acaafb420d63917566210 && $this->initializer5acaafb420d63917566210->__invoke($this->valueHolder5acaafb420d42706900702, $this, 'close', array(), $this->initializer5acaafb420d63917566210);

        return $this->valueHolder5acaafb420d42706900702->close();
    }

    /**
     * {@inheritDoc}
     */
    public function persist($entity)
    {
        $this->initializer5acaafb420d63917566210 && $this->initializer5acaafb420d63917566210->__invoke($this->valueHolder5acaafb420d42706900702, $this, 'persist', array('entity' => $entity), $this->initializer5acaafb420d63917566210);

        return $this->valueHolder5acaafb420d42706900702->persist($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function remove($entity)
    {
        $this->initializer5acaafb420d63917566210 && $this->initializer5acaafb420d63917566210->__invoke($this->valueHolder5acaafb420d42706900702, $this, 'remove', array('entity' => $entity), $this->initializer5acaafb420d63917566210);

        return $this->valueHolder5acaafb420d42706900702->remove($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function refresh($entity)
    {
        $this->initializer5acaafb420d63917566210 && $this->initializer5acaafb420d63917566210->__invoke($this->valueHolder5acaafb420d42706900702, $this, 'refresh', array('entity' => $entity), $this->initializer5acaafb420d63917566210);

        return $this->valueHolder5acaafb420d42706900702->refresh($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function detach($entity)
    {
        $this->initializer5acaafb420d63917566210 && $this->initializer5acaafb420d63917566210->__invoke($this->valueHolder5acaafb420d42706900702, $this, 'detach', array('entity' => $entity), $this->initializer5acaafb420d63917566210);

        return $this->valueHolder5acaafb420d42706900702->detach($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function merge($entity)
    {
        $this->initializer5acaafb420d63917566210 && $this->initializer5acaafb420d63917566210->__invoke($this->valueHolder5acaafb420d42706900702, $this, 'merge', array('entity' => $entity), $this->initializer5acaafb420d63917566210);

        return $this->valueHolder5acaafb420d42706900702->merge($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function copy($entity, $deep = false)
    {
        $this->initializer5acaafb420d63917566210 && $this->initializer5acaafb420d63917566210->__invoke($this->valueHolder5acaafb420d42706900702, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializer5acaafb420d63917566210);

        return $this->valueHolder5acaafb420d42706900702->copy($entity, $deep);
    }

    /**
     * {@inheritDoc}
     */
    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializer5acaafb420d63917566210 && $this->initializer5acaafb420d63917566210->__invoke($this->valueHolder5acaafb420d42706900702, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer5acaafb420d63917566210);

        return $this->valueHolder5acaafb420d42706900702->lock($entity, $lockMode, $lockVersion);
    }

    /**
     * {@inheritDoc}
     */
    public function getRepository($entityName)
    {
        $this->initializer5acaafb420d63917566210 && $this->initializer5acaafb420d63917566210->__invoke($this->valueHolder5acaafb420d42706900702, $this, 'getRepository', array('entityName' => $entityName), $this->initializer5acaafb420d63917566210);

        return $this->valueHolder5acaafb420d42706900702->getRepository($entityName);
    }

    /**
     * {@inheritDoc}
     */
    public function contains($entity)
    {
        $this->initializer5acaafb420d63917566210 && $this->initializer5acaafb420d63917566210->__invoke($this->valueHolder5acaafb420d42706900702, $this, 'contains', array('entity' => $entity), $this->initializer5acaafb420d63917566210);

        return $this->valueHolder5acaafb420d42706900702->contains($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function getEventManager()
    {
        $this->initializer5acaafb420d63917566210 && $this->initializer5acaafb420d63917566210->__invoke($this->valueHolder5acaafb420d42706900702, $this, 'getEventManager', array(), $this->initializer5acaafb420d63917566210);

        return $this->valueHolder5acaafb420d42706900702->getEventManager();
    }

    /**
     * {@inheritDoc}
     */
    public function getConfiguration()
    {
        $this->initializer5acaafb420d63917566210 && $this->initializer5acaafb420d63917566210->__invoke($this->valueHolder5acaafb420d42706900702, $this, 'getConfiguration', array(), $this->initializer5acaafb420d63917566210);

        return $this->valueHolder5acaafb420d42706900702->getConfiguration();
    }

    /**
     * {@inheritDoc}
     */
    public function isOpen()
    {
        $this->initializer5acaafb420d63917566210 && $this->initializer5acaafb420d63917566210->__invoke($this->valueHolder5acaafb420d42706900702, $this, 'isOpen', array(), $this->initializer5acaafb420d63917566210);

        return $this->valueHolder5acaafb420d42706900702->isOpen();
    }

    /**
     * {@inheritDoc}
     */
    public function getUnitOfWork()
    {
        $this->initializer5acaafb420d63917566210 && $this->initializer5acaafb420d63917566210->__invoke($this->valueHolder5acaafb420d42706900702, $this, 'getUnitOfWork', array(), $this->initializer5acaafb420d63917566210);

        return $this->valueHolder5acaafb420d42706900702->getUnitOfWork();
    }

    /**
     * {@inheritDoc}
     */
    public function getHydrator($hydrationMode)
    {
        $this->initializer5acaafb420d63917566210 && $this->initializer5acaafb420d63917566210->__invoke($this->valueHolder5acaafb420d42706900702, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializer5acaafb420d63917566210);

        return $this->valueHolder5acaafb420d42706900702->getHydrator($hydrationMode);
    }

    /**
     * {@inheritDoc}
     */
    public function newHydrator($hydrationMode)
    {
        $this->initializer5acaafb420d63917566210 && $this->initializer5acaafb420d63917566210->__invoke($this->valueHolder5acaafb420d42706900702, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializer5acaafb420d63917566210);

        return $this->valueHolder5acaafb420d42706900702->newHydrator($hydrationMode);
    }

    /**
     * {@inheritDoc}
     */
    public function getProxyFactory()
    {
        $this->initializer5acaafb420d63917566210 && $this->initializer5acaafb420d63917566210->__invoke($this->valueHolder5acaafb420d42706900702, $this, 'getProxyFactory', array(), $this->initializer5acaafb420d63917566210);

        return $this->valueHolder5acaafb420d42706900702->getProxyFactory();
    }

    /**
     * {@inheritDoc}
     */
    public function initializeObject($obj)
    {
        $this->initializer5acaafb420d63917566210 && $this->initializer5acaafb420d63917566210->__invoke($this->valueHolder5acaafb420d42706900702, $this, 'initializeObject', array('obj' => $obj), $this->initializer5acaafb420d63917566210);

        return $this->valueHolder5acaafb420d42706900702->initializeObject($obj);
    }

    /**
     * {@inheritDoc}
     */
    public function getFilters()
    {
        $this->initializer5acaafb420d63917566210 && $this->initializer5acaafb420d63917566210->__invoke($this->valueHolder5acaafb420d42706900702, $this, 'getFilters', array(), $this->initializer5acaafb420d63917566210);

        return $this->valueHolder5acaafb420d42706900702->getFilters();
    }

    /**
     * {@inheritDoc}
     */
    public function isFiltersStateClean()
    {
        $this->initializer5acaafb420d63917566210 && $this->initializer5acaafb420d63917566210->__invoke($this->valueHolder5acaafb420d42706900702, $this, 'isFiltersStateClean', array(), $this->initializer5acaafb420d63917566210);

        return $this->valueHolder5acaafb420d42706900702->isFiltersStateClean();
    }

    /**
     * {@inheritDoc}
     */
    public function hasFilters()
    {
        $this->initializer5acaafb420d63917566210 && $this->initializer5acaafb420d63917566210->__invoke($this->valueHolder5acaafb420d42706900702, $this, 'hasFilters', array(), $this->initializer5acaafb420d63917566210);

        return $this->valueHolder5acaafb420d42706900702->hasFilters();
    }

    /**
     * @override constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public function __construct($initializer)
    {
        $this->initializer5acaafb420d63917566210 = $initializer;
    }

    /**
     * @param string $name
     */
    public function & __get($name)
    {
        $this->initializer5acaafb420d63917566210 && $this->initializer5acaafb420d63917566210->__invoke($this->valueHolder5acaafb420d42706900702, $this, '__get', array('name' => $name), $this->initializer5acaafb420d63917566210);

        if (isset(self::$publicProperties5acaafb420c9d717299721[$name])) {
            return $this->valueHolder5acaafb420d42706900702->$name;
        }

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5acaafb420d42706900702;

            $backtrace = debug_backtrace(false);
            trigger_error('Undefined property: ' . get_parent_class($this) . '::$' . $name . ' in ' . $backtrace[0]['file'] . ' on line ' . $backtrace[0]['line'], \E_USER_NOTICE);
            return $targetObject->$name;;
            return;
        }

        $targetObject = $this->valueHolder5acaafb420d42706900702;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    /**
     * @param string $name
     * @param mixed $value
     */
    public function __set($name, $value)
    {
        $this->initializer5acaafb420d63917566210 && $this->initializer5acaafb420d63917566210->__invoke($this->valueHolder5acaafb420d42706900702, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer5acaafb420d63917566210);

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5acaafb420d42706900702;

            return $targetObject->$name = $value;;
            return;
        }

        $targetObject = $this->valueHolder5acaafb420d42706900702;
        $accessor = function & () use ($targetObject, $name, $value) {
            return $targetObject->$name = $value;
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    /**
     * @param string $name
     */
    public function __isset($name)
    {
        $this->initializer5acaafb420d63917566210 && $this->initializer5acaafb420d63917566210->__invoke($this->valueHolder5acaafb420d42706900702, $this, '__isset', array('name' => $name), $this->initializer5acaafb420d63917566210);

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5acaafb420d42706900702;

            return isset($targetObject->$name);;
            return;
        }

        $targetObject = $this->valueHolder5acaafb420d42706900702;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    /**
     * @param string $name
     */
    public function __unset($name)
    {
        $this->initializer5acaafb420d63917566210 && $this->initializer5acaafb420d63917566210->__invoke($this->valueHolder5acaafb420d42706900702, $this, '__unset', array('name' => $name), $this->initializer5acaafb420d63917566210);

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5acaafb420d42706900702;

            unset($targetObject->$name);;
            return;
        }

        $targetObject = $this->valueHolder5acaafb420d42706900702;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __clone()
    {
        $this->initializer5acaafb420d63917566210 && $this->initializer5acaafb420d63917566210->__invoke($this->valueHolder5acaafb420d42706900702, $this, '__clone', array(), $this->initializer5acaafb420d63917566210);

        $this->valueHolder5acaafb420d42706900702 = clone $this->valueHolder5acaafb420d42706900702;
    }

    public function __sleep()
    {
        $this->initializer5acaafb420d63917566210 && $this->initializer5acaafb420d63917566210->__invoke($this->valueHolder5acaafb420d42706900702, $this, '__sleep', array(), $this->initializer5acaafb420d63917566210);

        return array('valueHolder5acaafb420d42706900702');
    }

    public function __wakeup()
    {
    }

    /**
     * {@inheritDoc}
     */
    public function setProxyInitializer(\Closure $initializer = null)
    {
        $this->initializer5acaafb420d63917566210 = $initializer;
    }

    /**
     * {@inheritDoc}
     */
    public function getProxyInitializer()
    {
        return $this->initializer5acaafb420d63917566210;
    }

    /**
     * {@inheritDoc}
     */
    public function initializeProxy()
    {
        return $this->initializer5acaafb420d63917566210 && $this->initializer5acaafb420d63917566210->__invoke($this->valueHolder5acaafb420d42706900702, $this, 'initializeProxy', array(), $this->initializer5acaafb420d63917566210);
    }

    /**
     * {@inheritDoc}
     */
    public function isProxyInitialized()
    {
        return null !== $this->valueHolder5acaafb420d42706900702;
    }

    /**
     * {@inheritDoc}
     */
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder5acaafb420d42706900702;
    }


}

class DoctrineORMEntityManager_000000002084058800000000634d02687471c441876336872902bf356897607e extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $valueHolder5acaafb436abc099870777 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer5acaafb436ad7570218559 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties5acaafb436a79923185644 = array(
        
    );

    /**
     * {@inheritDoc}
     */
    public function getConnection()
    {
        $this->initializer5acaafb436ad7570218559 && $this->initializer5acaafb436ad7570218559->__invoke($this->valueHolder5acaafb436abc099870777, $this, 'getConnection', array(), $this->initializer5acaafb436ad7570218559);

        return $this->valueHolder5acaafb436abc099870777->getConnection();
    }

    /**
     * {@inheritDoc}
     */
    public function getMetadataFactory()
    {
        $this->initializer5acaafb436ad7570218559 && $this->initializer5acaafb436ad7570218559->__invoke($this->valueHolder5acaafb436abc099870777, $this, 'getMetadataFactory', array(), $this->initializer5acaafb436ad7570218559);

        return $this->valueHolder5acaafb436abc099870777->getMetadataFactory();
    }

    /**
     * {@inheritDoc}
     */
    public function getExpressionBuilder()
    {
        $this->initializer5acaafb436ad7570218559 && $this->initializer5acaafb436ad7570218559->__invoke($this->valueHolder5acaafb436abc099870777, $this, 'getExpressionBuilder', array(), $this->initializer5acaafb436ad7570218559);

        return $this->valueHolder5acaafb436abc099870777->getExpressionBuilder();
    }

    /**
     * {@inheritDoc}
     */
    public function beginTransaction()
    {
        $this->initializer5acaafb436ad7570218559 && $this->initializer5acaafb436ad7570218559->__invoke($this->valueHolder5acaafb436abc099870777, $this, 'beginTransaction', array(), $this->initializer5acaafb436ad7570218559);

        return $this->valueHolder5acaafb436abc099870777->beginTransaction();
    }

    /**
     * {@inheritDoc}
     */
    public function getCache()
    {
        $this->initializer5acaafb436ad7570218559 && $this->initializer5acaafb436ad7570218559->__invoke($this->valueHolder5acaafb436abc099870777, $this, 'getCache', array(), $this->initializer5acaafb436ad7570218559);

        return $this->valueHolder5acaafb436abc099870777->getCache();
    }

    /**
     * {@inheritDoc}
     */
    public function transactional($func)
    {
        $this->initializer5acaafb436ad7570218559 && $this->initializer5acaafb436ad7570218559->__invoke($this->valueHolder5acaafb436abc099870777, $this, 'transactional', array('func' => $func), $this->initializer5acaafb436ad7570218559);

        return $this->valueHolder5acaafb436abc099870777->transactional($func);
    }

    /**
     * {@inheritDoc}
     */
    public function commit()
    {
        $this->initializer5acaafb436ad7570218559 && $this->initializer5acaafb436ad7570218559->__invoke($this->valueHolder5acaafb436abc099870777, $this, 'commit', array(), $this->initializer5acaafb436ad7570218559);

        return $this->valueHolder5acaafb436abc099870777->commit();
    }

    /**
     * {@inheritDoc}
     */
    public function rollback()
    {
        $this->initializer5acaafb436ad7570218559 && $this->initializer5acaafb436ad7570218559->__invoke($this->valueHolder5acaafb436abc099870777, $this, 'rollback', array(), $this->initializer5acaafb436ad7570218559);

        return $this->valueHolder5acaafb436abc099870777->rollback();
    }

    /**
     * {@inheritDoc}
     */
    public function getClassMetadata($className)
    {
        $this->initializer5acaafb436ad7570218559 && $this->initializer5acaafb436ad7570218559->__invoke($this->valueHolder5acaafb436abc099870777, $this, 'getClassMetadata', array('className' => $className), $this->initializer5acaafb436ad7570218559);

        return $this->valueHolder5acaafb436abc099870777->getClassMetadata($className);
    }

    /**
     * {@inheritDoc}
     */
    public function createQuery($dql = '')
    {
        $this->initializer5acaafb436ad7570218559 && $this->initializer5acaafb436ad7570218559->__invoke($this->valueHolder5acaafb436abc099870777, $this, 'createQuery', array('dql' => $dql), $this->initializer5acaafb436ad7570218559);

        return $this->valueHolder5acaafb436abc099870777->createQuery($dql);
    }

    /**
     * {@inheritDoc}
     */
    public function createNamedQuery($name)
    {
        $this->initializer5acaafb436ad7570218559 && $this->initializer5acaafb436ad7570218559->__invoke($this->valueHolder5acaafb436abc099870777, $this, 'createNamedQuery', array('name' => $name), $this->initializer5acaafb436ad7570218559);

        return $this->valueHolder5acaafb436abc099870777->createNamedQuery($name);
    }

    /**
     * {@inheritDoc}
     */
    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializer5acaafb436ad7570218559 && $this->initializer5acaafb436ad7570218559->__invoke($this->valueHolder5acaafb436abc099870777, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializer5acaafb436ad7570218559);

        return $this->valueHolder5acaafb436abc099870777->createNativeQuery($sql, $rsm);
    }

    /**
     * {@inheritDoc}
     */
    public function createNamedNativeQuery($name)
    {
        $this->initializer5acaafb436ad7570218559 && $this->initializer5acaafb436ad7570218559->__invoke($this->valueHolder5acaafb436abc099870777, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializer5acaafb436ad7570218559);

        return $this->valueHolder5acaafb436abc099870777->createNamedNativeQuery($name);
    }

    /**
     * {@inheritDoc}
     */
    public function createQueryBuilder()
    {
        $this->initializer5acaafb436ad7570218559 && $this->initializer5acaafb436ad7570218559->__invoke($this->valueHolder5acaafb436abc099870777, $this, 'createQueryBuilder', array(), $this->initializer5acaafb436ad7570218559);

        return $this->valueHolder5acaafb436abc099870777->createQueryBuilder();
    }

    /**
     * {@inheritDoc}
     */
    public function flush($entity = null)
    {
        $this->initializer5acaafb436ad7570218559 && $this->initializer5acaafb436ad7570218559->__invoke($this->valueHolder5acaafb436abc099870777, $this, 'flush', array('entity' => $entity), $this->initializer5acaafb436ad7570218559);

        return $this->valueHolder5acaafb436abc099870777->flush($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function find($entityName, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer5acaafb436ad7570218559 && $this->initializer5acaafb436ad7570218559->__invoke($this->valueHolder5acaafb436abc099870777, $this, 'find', array('entityName' => $entityName, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer5acaafb436ad7570218559);

        return $this->valueHolder5acaafb436abc099870777->find($entityName, $id, $lockMode, $lockVersion);
    }

    /**
     * {@inheritDoc}
     */
    public function getReference($entityName, $id)
    {
        $this->initializer5acaafb436ad7570218559 && $this->initializer5acaafb436ad7570218559->__invoke($this->valueHolder5acaafb436abc099870777, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializer5acaafb436ad7570218559);

        return $this->valueHolder5acaafb436abc099870777->getReference($entityName, $id);
    }

    /**
     * {@inheritDoc}
     */
    public function getPartialReference($entityName, $identifier)
    {
        $this->initializer5acaafb436ad7570218559 && $this->initializer5acaafb436ad7570218559->__invoke($this->valueHolder5acaafb436abc099870777, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializer5acaafb436ad7570218559);

        return $this->valueHolder5acaafb436abc099870777->getPartialReference($entityName, $identifier);
    }

    /**
     * {@inheritDoc}
     */
    public function clear($entityName = null)
    {
        $this->initializer5acaafb436ad7570218559 && $this->initializer5acaafb436ad7570218559->__invoke($this->valueHolder5acaafb436abc099870777, $this, 'clear', array('entityName' => $entityName), $this->initializer5acaafb436ad7570218559);

        return $this->valueHolder5acaafb436abc099870777->clear($entityName);
    }

    /**
     * {@inheritDoc}
     */
    public function close()
    {
        $this->initializer5acaafb436ad7570218559 && $this->initializer5acaafb436ad7570218559->__invoke($this->valueHolder5acaafb436abc099870777, $this, 'close', array(), $this->initializer5acaafb436ad7570218559);

        return $this->valueHolder5acaafb436abc099870777->close();
    }

    /**
     * {@inheritDoc}
     */
    public function persist($entity)
    {
        $this->initializer5acaafb436ad7570218559 && $this->initializer5acaafb436ad7570218559->__invoke($this->valueHolder5acaafb436abc099870777, $this, 'persist', array('entity' => $entity), $this->initializer5acaafb436ad7570218559);

        return $this->valueHolder5acaafb436abc099870777->persist($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function remove($entity)
    {
        $this->initializer5acaafb436ad7570218559 && $this->initializer5acaafb436ad7570218559->__invoke($this->valueHolder5acaafb436abc099870777, $this, 'remove', array('entity' => $entity), $this->initializer5acaafb436ad7570218559);

        return $this->valueHolder5acaafb436abc099870777->remove($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function refresh($entity)
    {
        $this->initializer5acaafb436ad7570218559 && $this->initializer5acaafb436ad7570218559->__invoke($this->valueHolder5acaafb436abc099870777, $this, 'refresh', array('entity' => $entity), $this->initializer5acaafb436ad7570218559);

        return $this->valueHolder5acaafb436abc099870777->refresh($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function detach($entity)
    {
        $this->initializer5acaafb436ad7570218559 && $this->initializer5acaafb436ad7570218559->__invoke($this->valueHolder5acaafb436abc099870777, $this, 'detach', array('entity' => $entity), $this->initializer5acaafb436ad7570218559);

        return $this->valueHolder5acaafb436abc099870777->detach($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function merge($entity)
    {
        $this->initializer5acaafb436ad7570218559 && $this->initializer5acaafb436ad7570218559->__invoke($this->valueHolder5acaafb436abc099870777, $this, 'merge', array('entity' => $entity), $this->initializer5acaafb436ad7570218559);

        return $this->valueHolder5acaafb436abc099870777->merge($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function copy($entity, $deep = false)
    {
        $this->initializer5acaafb436ad7570218559 && $this->initializer5acaafb436ad7570218559->__invoke($this->valueHolder5acaafb436abc099870777, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializer5acaafb436ad7570218559);

        return $this->valueHolder5acaafb436abc099870777->copy($entity, $deep);
    }

    /**
     * {@inheritDoc}
     */
    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializer5acaafb436ad7570218559 && $this->initializer5acaafb436ad7570218559->__invoke($this->valueHolder5acaafb436abc099870777, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer5acaafb436ad7570218559);

        return $this->valueHolder5acaafb436abc099870777->lock($entity, $lockMode, $lockVersion);
    }

    /**
     * {@inheritDoc}
     */
    public function getRepository($entityName)
    {
        $this->initializer5acaafb436ad7570218559 && $this->initializer5acaafb436ad7570218559->__invoke($this->valueHolder5acaafb436abc099870777, $this, 'getRepository', array('entityName' => $entityName), $this->initializer5acaafb436ad7570218559);

        return $this->valueHolder5acaafb436abc099870777->getRepository($entityName);
    }

    /**
     * {@inheritDoc}
     */
    public function contains($entity)
    {
        $this->initializer5acaafb436ad7570218559 && $this->initializer5acaafb436ad7570218559->__invoke($this->valueHolder5acaafb436abc099870777, $this, 'contains', array('entity' => $entity), $this->initializer5acaafb436ad7570218559);

        return $this->valueHolder5acaafb436abc099870777->contains($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function getEventManager()
    {
        $this->initializer5acaafb436ad7570218559 && $this->initializer5acaafb436ad7570218559->__invoke($this->valueHolder5acaafb436abc099870777, $this, 'getEventManager', array(), $this->initializer5acaafb436ad7570218559);

        return $this->valueHolder5acaafb436abc099870777->getEventManager();
    }

    /**
     * {@inheritDoc}
     */
    public function getConfiguration()
    {
        $this->initializer5acaafb436ad7570218559 && $this->initializer5acaafb436ad7570218559->__invoke($this->valueHolder5acaafb436abc099870777, $this, 'getConfiguration', array(), $this->initializer5acaafb436ad7570218559);

        return $this->valueHolder5acaafb436abc099870777->getConfiguration();
    }

    /**
     * {@inheritDoc}
     */
    public function isOpen()
    {
        $this->initializer5acaafb436ad7570218559 && $this->initializer5acaafb436ad7570218559->__invoke($this->valueHolder5acaafb436abc099870777, $this, 'isOpen', array(), $this->initializer5acaafb436ad7570218559);

        return $this->valueHolder5acaafb436abc099870777->isOpen();
    }

    /**
     * {@inheritDoc}
     */
    public function getUnitOfWork()
    {
        $this->initializer5acaafb436ad7570218559 && $this->initializer5acaafb436ad7570218559->__invoke($this->valueHolder5acaafb436abc099870777, $this, 'getUnitOfWork', array(), $this->initializer5acaafb436ad7570218559);

        return $this->valueHolder5acaafb436abc099870777->getUnitOfWork();
    }

    /**
     * {@inheritDoc}
     */
    public function getHydrator($hydrationMode)
    {
        $this->initializer5acaafb436ad7570218559 && $this->initializer5acaafb436ad7570218559->__invoke($this->valueHolder5acaafb436abc099870777, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializer5acaafb436ad7570218559);

        return $this->valueHolder5acaafb436abc099870777->getHydrator($hydrationMode);
    }

    /**
     * {@inheritDoc}
     */
    public function newHydrator($hydrationMode)
    {
        $this->initializer5acaafb436ad7570218559 && $this->initializer5acaafb436ad7570218559->__invoke($this->valueHolder5acaafb436abc099870777, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializer5acaafb436ad7570218559);

        return $this->valueHolder5acaafb436abc099870777->newHydrator($hydrationMode);
    }

    /**
     * {@inheritDoc}
     */
    public function getProxyFactory()
    {
        $this->initializer5acaafb436ad7570218559 && $this->initializer5acaafb436ad7570218559->__invoke($this->valueHolder5acaafb436abc099870777, $this, 'getProxyFactory', array(), $this->initializer5acaafb436ad7570218559);

        return $this->valueHolder5acaafb436abc099870777->getProxyFactory();
    }

    /**
     * {@inheritDoc}
     */
    public function initializeObject($obj)
    {
        $this->initializer5acaafb436ad7570218559 && $this->initializer5acaafb436ad7570218559->__invoke($this->valueHolder5acaafb436abc099870777, $this, 'initializeObject', array('obj' => $obj), $this->initializer5acaafb436ad7570218559);

        return $this->valueHolder5acaafb436abc099870777->initializeObject($obj);
    }

    /**
     * {@inheritDoc}
     */
    public function getFilters()
    {
        $this->initializer5acaafb436ad7570218559 && $this->initializer5acaafb436ad7570218559->__invoke($this->valueHolder5acaafb436abc099870777, $this, 'getFilters', array(), $this->initializer5acaafb436ad7570218559);

        return $this->valueHolder5acaafb436abc099870777->getFilters();
    }

    /**
     * {@inheritDoc}
     */
    public function isFiltersStateClean()
    {
        $this->initializer5acaafb436ad7570218559 && $this->initializer5acaafb436ad7570218559->__invoke($this->valueHolder5acaafb436abc099870777, $this, 'isFiltersStateClean', array(), $this->initializer5acaafb436ad7570218559);

        return $this->valueHolder5acaafb436abc099870777->isFiltersStateClean();
    }

    /**
     * {@inheritDoc}
     */
    public function hasFilters()
    {
        $this->initializer5acaafb436ad7570218559 && $this->initializer5acaafb436ad7570218559->__invoke($this->valueHolder5acaafb436abc099870777, $this, 'hasFilters', array(), $this->initializer5acaafb436ad7570218559);

        return $this->valueHolder5acaafb436abc099870777->hasFilters();
    }

    /**
     * @override constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public function __construct($initializer)
    {
        $this->initializer5acaafb436ad7570218559 = $initializer;
    }

    /**
     * @param string $name
     */
    public function & __get($name)
    {
        $this->initializer5acaafb436ad7570218559 && $this->initializer5acaafb436ad7570218559->__invoke($this->valueHolder5acaafb436abc099870777, $this, '__get', array('name' => $name), $this->initializer5acaafb436ad7570218559);

        if (isset(self::$publicProperties5acaafb436a79923185644[$name])) {
            return $this->valueHolder5acaafb436abc099870777->$name;
        }

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5acaafb436abc099870777;

            $backtrace = debug_backtrace(false);
            trigger_error('Undefined property: ' . get_parent_class($this) . '::$' . $name . ' in ' . $backtrace[0]['file'] . ' on line ' . $backtrace[0]['line'], \E_USER_NOTICE);
            return $targetObject->$name;;
            return;
        }

        $targetObject = $this->valueHolder5acaafb436abc099870777;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    /**
     * @param string $name
     * @param mixed $value
     */
    public function __set($name, $value)
    {
        $this->initializer5acaafb436ad7570218559 && $this->initializer5acaafb436ad7570218559->__invoke($this->valueHolder5acaafb436abc099870777, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer5acaafb436ad7570218559);

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5acaafb436abc099870777;

            return $targetObject->$name = $value;;
            return;
        }

        $targetObject = $this->valueHolder5acaafb436abc099870777;
        $accessor = function & () use ($targetObject, $name, $value) {
            return $targetObject->$name = $value;
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    /**
     * @param string $name
     */
    public function __isset($name)
    {
        $this->initializer5acaafb436ad7570218559 && $this->initializer5acaafb436ad7570218559->__invoke($this->valueHolder5acaafb436abc099870777, $this, '__isset', array('name' => $name), $this->initializer5acaafb436ad7570218559);

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5acaafb436abc099870777;

            return isset($targetObject->$name);;
            return;
        }

        $targetObject = $this->valueHolder5acaafb436abc099870777;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    /**
     * @param string $name
     */
    public function __unset($name)
    {
        $this->initializer5acaafb436ad7570218559 && $this->initializer5acaafb436ad7570218559->__invoke($this->valueHolder5acaafb436abc099870777, $this, '__unset', array('name' => $name), $this->initializer5acaafb436ad7570218559);

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5acaafb436abc099870777;

            unset($targetObject->$name);;
            return;
        }

        $targetObject = $this->valueHolder5acaafb436abc099870777;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __clone()
    {
        $this->initializer5acaafb436ad7570218559 && $this->initializer5acaafb436ad7570218559->__invoke($this->valueHolder5acaafb436abc099870777, $this, '__clone', array(), $this->initializer5acaafb436ad7570218559);

        $this->valueHolder5acaafb436abc099870777 = clone $this->valueHolder5acaafb436abc099870777;
    }

    public function __sleep()
    {
        $this->initializer5acaafb436ad7570218559 && $this->initializer5acaafb436ad7570218559->__invoke($this->valueHolder5acaafb436abc099870777, $this, '__sleep', array(), $this->initializer5acaafb436ad7570218559);

        return array('valueHolder5acaafb436abc099870777');
    }

    public function __wakeup()
    {
    }

    /**
     * {@inheritDoc}
     */
    public function setProxyInitializer(\Closure $initializer = null)
    {
        $this->initializer5acaafb436ad7570218559 = $initializer;
    }

    /**
     * {@inheritDoc}
     */
    public function getProxyInitializer()
    {
        return $this->initializer5acaafb436ad7570218559;
    }

    /**
     * {@inheritDoc}
     */
    public function initializeProxy()
    {
        return $this->initializer5acaafb436ad7570218559 && $this->initializer5acaafb436ad7570218559->__invoke($this->valueHolder5acaafb436abc099870777, $this, 'initializeProxy', array(), $this->initializer5acaafb436ad7570218559);
    }

    /**
     * {@inheritDoc}
     */
    public function isProxyInitialized()
    {
        return null !== $this->valueHolder5acaafb436abc099870777;
    }

    /**
     * {@inheritDoc}
     */
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder5acaafb436abc099870777;
    }


}

class DoctrineORMEntityManager_000000002084058200000000634d02687471c441876336872902bf356897607e extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $valueHolder5acaafb44f422127025819 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer5acaafb44f471098593856 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties5acaafb44f3e1825496449 = array(
        
    );

    /**
     * {@inheritDoc}
     */
    public function getConnection()
    {
        $this->initializer5acaafb44f471098593856 && $this->initializer5acaafb44f471098593856->__invoke($this->valueHolder5acaafb44f422127025819, $this, 'getConnection', array(), $this->initializer5acaafb44f471098593856);

        return $this->valueHolder5acaafb44f422127025819->getConnection();
    }

    /**
     * {@inheritDoc}
     */
    public function getMetadataFactory()
    {
        $this->initializer5acaafb44f471098593856 && $this->initializer5acaafb44f471098593856->__invoke($this->valueHolder5acaafb44f422127025819, $this, 'getMetadataFactory', array(), $this->initializer5acaafb44f471098593856);

        return $this->valueHolder5acaafb44f422127025819->getMetadataFactory();
    }

    /**
     * {@inheritDoc}
     */
    public function getExpressionBuilder()
    {
        $this->initializer5acaafb44f471098593856 && $this->initializer5acaafb44f471098593856->__invoke($this->valueHolder5acaafb44f422127025819, $this, 'getExpressionBuilder', array(), $this->initializer5acaafb44f471098593856);

        return $this->valueHolder5acaafb44f422127025819->getExpressionBuilder();
    }

    /**
     * {@inheritDoc}
     */
    public function beginTransaction()
    {
        $this->initializer5acaafb44f471098593856 && $this->initializer5acaafb44f471098593856->__invoke($this->valueHolder5acaafb44f422127025819, $this, 'beginTransaction', array(), $this->initializer5acaafb44f471098593856);

        return $this->valueHolder5acaafb44f422127025819->beginTransaction();
    }

    /**
     * {@inheritDoc}
     */
    public function getCache()
    {
        $this->initializer5acaafb44f471098593856 && $this->initializer5acaafb44f471098593856->__invoke($this->valueHolder5acaafb44f422127025819, $this, 'getCache', array(), $this->initializer5acaafb44f471098593856);

        return $this->valueHolder5acaafb44f422127025819->getCache();
    }

    /**
     * {@inheritDoc}
     */
    public function transactional($func)
    {
        $this->initializer5acaafb44f471098593856 && $this->initializer5acaafb44f471098593856->__invoke($this->valueHolder5acaafb44f422127025819, $this, 'transactional', array('func' => $func), $this->initializer5acaafb44f471098593856);

        return $this->valueHolder5acaafb44f422127025819->transactional($func);
    }

    /**
     * {@inheritDoc}
     */
    public function commit()
    {
        $this->initializer5acaafb44f471098593856 && $this->initializer5acaafb44f471098593856->__invoke($this->valueHolder5acaafb44f422127025819, $this, 'commit', array(), $this->initializer5acaafb44f471098593856);

        return $this->valueHolder5acaafb44f422127025819->commit();
    }

    /**
     * {@inheritDoc}
     */
    public function rollback()
    {
        $this->initializer5acaafb44f471098593856 && $this->initializer5acaafb44f471098593856->__invoke($this->valueHolder5acaafb44f422127025819, $this, 'rollback', array(), $this->initializer5acaafb44f471098593856);

        return $this->valueHolder5acaafb44f422127025819->rollback();
    }

    /**
     * {@inheritDoc}
     */
    public function getClassMetadata($className)
    {
        $this->initializer5acaafb44f471098593856 && $this->initializer5acaafb44f471098593856->__invoke($this->valueHolder5acaafb44f422127025819, $this, 'getClassMetadata', array('className' => $className), $this->initializer5acaafb44f471098593856);

        return $this->valueHolder5acaafb44f422127025819->getClassMetadata($className);
    }

    /**
     * {@inheritDoc}
     */
    public function createQuery($dql = '')
    {
        $this->initializer5acaafb44f471098593856 && $this->initializer5acaafb44f471098593856->__invoke($this->valueHolder5acaafb44f422127025819, $this, 'createQuery', array('dql' => $dql), $this->initializer5acaafb44f471098593856);

        return $this->valueHolder5acaafb44f422127025819->createQuery($dql);
    }

    /**
     * {@inheritDoc}
     */
    public function createNamedQuery($name)
    {
        $this->initializer5acaafb44f471098593856 && $this->initializer5acaafb44f471098593856->__invoke($this->valueHolder5acaafb44f422127025819, $this, 'createNamedQuery', array('name' => $name), $this->initializer5acaafb44f471098593856);

        return $this->valueHolder5acaafb44f422127025819->createNamedQuery($name);
    }

    /**
     * {@inheritDoc}
     */
    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializer5acaafb44f471098593856 && $this->initializer5acaafb44f471098593856->__invoke($this->valueHolder5acaafb44f422127025819, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializer5acaafb44f471098593856);

        return $this->valueHolder5acaafb44f422127025819->createNativeQuery($sql, $rsm);
    }

    /**
     * {@inheritDoc}
     */
    public function createNamedNativeQuery($name)
    {
        $this->initializer5acaafb44f471098593856 && $this->initializer5acaafb44f471098593856->__invoke($this->valueHolder5acaafb44f422127025819, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializer5acaafb44f471098593856);

        return $this->valueHolder5acaafb44f422127025819->createNamedNativeQuery($name);
    }

    /**
     * {@inheritDoc}
     */
    public function createQueryBuilder()
    {
        $this->initializer5acaafb44f471098593856 && $this->initializer5acaafb44f471098593856->__invoke($this->valueHolder5acaafb44f422127025819, $this, 'createQueryBuilder', array(), $this->initializer5acaafb44f471098593856);

        return $this->valueHolder5acaafb44f422127025819->createQueryBuilder();
    }

    /**
     * {@inheritDoc}
     */
    public function flush($entity = null)
    {
        $this->initializer5acaafb44f471098593856 && $this->initializer5acaafb44f471098593856->__invoke($this->valueHolder5acaafb44f422127025819, $this, 'flush', array('entity' => $entity), $this->initializer5acaafb44f471098593856);

        return $this->valueHolder5acaafb44f422127025819->flush($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function find($entityName, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer5acaafb44f471098593856 && $this->initializer5acaafb44f471098593856->__invoke($this->valueHolder5acaafb44f422127025819, $this, 'find', array('entityName' => $entityName, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer5acaafb44f471098593856);

        return $this->valueHolder5acaafb44f422127025819->find($entityName, $id, $lockMode, $lockVersion);
    }

    /**
     * {@inheritDoc}
     */
    public function getReference($entityName, $id)
    {
        $this->initializer5acaafb44f471098593856 && $this->initializer5acaafb44f471098593856->__invoke($this->valueHolder5acaafb44f422127025819, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializer5acaafb44f471098593856);

        return $this->valueHolder5acaafb44f422127025819->getReference($entityName, $id);
    }

    /**
     * {@inheritDoc}
     */
    public function getPartialReference($entityName, $identifier)
    {
        $this->initializer5acaafb44f471098593856 && $this->initializer5acaafb44f471098593856->__invoke($this->valueHolder5acaafb44f422127025819, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializer5acaafb44f471098593856);

        return $this->valueHolder5acaafb44f422127025819->getPartialReference($entityName, $identifier);
    }

    /**
     * {@inheritDoc}
     */
    public function clear($entityName = null)
    {
        $this->initializer5acaafb44f471098593856 && $this->initializer5acaafb44f471098593856->__invoke($this->valueHolder5acaafb44f422127025819, $this, 'clear', array('entityName' => $entityName), $this->initializer5acaafb44f471098593856);

        return $this->valueHolder5acaafb44f422127025819->clear($entityName);
    }

    /**
     * {@inheritDoc}
     */
    public function close()
    {
        $this->initializer5acaafb44f471098593856 && $this->initializer5acaafb44f471098593856->__invoke($this->valueHolder5acaafb44f422127025819, $this, 'close', array(), $this->initializer5acaafb44f471098593856);

        return $this->valueHolder5acaafb44f422127025819->close();
    }

    /**
     * {@inheritDoc}
     */
    public function persist($entity)
    {
        $this->initializer5acaafb44f471098593856 && $this->initializer5acaafb44f471098593856->__invoke($this->valueHolder5acaafb44f422127025819, $this, 'persist', array('entity' => $entity), $this->initializer5acaafb44f471098593856);

        return $this->valueHolder5acaafb44f422127025819->persist($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function remove($entity)
    {
        $this->initializer5acaafb44f471098593856 && $this->initializer5acaafb44f471098593856->__invoke($this->valueHolder5acaafb44f422127025819, $this, 'remove', array('entity' => $entity), $this->initializer5acaafb44f471098593856);

        return $this->valueHolder5acaafb44f422127025819->remove($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function refresh($entity)
    {
        $this->initializer5acaafb44f471098593856 && $this->initializer5acaafb44f471098593856->__invoke($this->valueHolder5acaafb44f422127025819, $this, 'refresh', array('entity' => $entity), $this->initializer5acaafb44f471098593856);

        return $this->valueHolder5acaafb44f422127025819->refresh($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function detach($entity)
    {
        $this->initializer5acaafb44f471098593856 && $this->initializer5acaafb44f471098593856->__invoke($this->valueHolder5acaafb44f422127025819, $this, 'detach', array('entity' => $entity), $this->initializer5acaafb44f471098593856);

        return $this->valueHolder5acaafb44f422127025819->detach($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function merge($entity)
    {
        $this->initializer5acaafb44f471098593856 && $this->initializer5acaafb44f471098593856->__invoke($this->valueHolder5acaafb44f422127025819, $this, 'merge', array('entity' => $entity), $this->initializer5acaafb44f471098593856);

        return $this->valueHolder5acaafb44f422127025819->merge($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function copy($entity, $deep = false)
    {
        $this->initializer5acaafb44f471098593856 && $this->initializer5acaafb44f471098593856->__invoke($this->valueHolder5acaafb44f422127025819, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializer5acaafb44f471098593856);

        return $this->valueHolder5acaafb44f422127025819->copy($entity, $deep);
    }

    /**
     * {@inheritDoc}
     */
    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializer5acaafb44f471098593856 && $this->initializer5acaafb44f471098593856->__invoke($this->valueHolder5acaafb44f422127025819, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer5acaafb44f471098593856);

        return $this->valueHolder5acaafb44f422127025819->lock($entity, $lockMode, $lockVersion);
    }

    /**
     * {@inheritDoc}
     */
    public function getRepository($entityName)
    {
        $this->initializer5acaafb44f471098593856 && $this->initializer5acaafb44f471098593856->__invoke($this->valueHolder5acaafb44f422127025819, $this, 'getRepository', array('entityName' => $entityName), $this->initializer5acaafb44f471098593856);

        return $this->valueHolder5acaafb44f422127025819->getRepository($entityName);
    }

    /**
     * {@inheritDoc}
     */
    public function contains($entity)
    {
        $this->initializer5acaafb44f471098593856 && $this->initializer5acaafb44f471098593856->__invoke($this->valueHolder5acaafb44f422127025819, $this, 'contains', array('entity' => $entity), $this->initializer5acaafb44f471098593856);

        return $this->valueHolder5acaafb44f422127025819->contains($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function getEventManager()
    {
        $this->initializer5acaafb44f471098593856 && $this->initializer5acaafb44f471098593856->__invoke($this->valueHolder5acaafb44f422127025819, $this, 'getEventManager', array(), $this->initializer5acaafb44f471098593856);

        return $this->valueHolder5acaafb44f422127025819->getEventManager();
    }

    /**
     * {@inheritDoc}
     */
    public function getConfiguration()
    {
        $this->initializer5acaafb44f471098593856 && $this->initializer5acaafb44f471098593856->__invoke($this->valueHolder5acaafb44f422127025819, $this, 'getConfiguration', array(), $this->initializer5acaafb44f471098593856);

        return $this->valueHolder5acaafb44f422127025819->getConfiguration();
    }

    /**
     * {@inheritDoc}
     */
    public function isOpen()
    {
        $this->initializer5acaafb44f471098593856 && $this->initializer5acaafb44f471098593856->__invoke($this->valueHolder5acaafb44f422127025819, $this, 'isOpen', array(), $this->initializer5acaafb44f471098593856);

        return $this->valueHolder5acaafb44f422127025819->isOpen();
    }

    /**
     * {@inheritDoc}
     */
    public function getUnitOfWork()
    {
        $this->initializer5acaafb44f471098593856 && $this->initializer5acaafb44f471098593856->__invoke($this->valueHolder5acaafb44f422127025819, $this, 'getUnitOfWork', array(), $this->initializer5acaafb44f471098593856);

        return $this->valueHolder5acaafb44f422127025819->getUnitOfWork();
    }

    /**
     * {@inheritDoc}
     */
    public function getHydrator($hydrationMode)
    {
        $this->initializer5acaafb44f471098593856 && $this->initializer5acaafb44f471098593856->__invoke($this->valueHolder5acaafb44f422127025819, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializer5acaafb44f471098593856);

        return $this->valueHolder5acaafb44f422127025819->getHydrator($hydrationMode);
    }

    /**
     * {@inheritDoc}
     */
    public function newHydrator($hydrationMode)
    {
        $this->initializer5acaafb44f471098593856 && $this->initializer5acaafb44f471098593856->__invoke($this->valueHolder5acaafb44f422127025819, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializer5acaafb44f471098593856);

        return $this->valueHolder5acaafb44f422127025819->newHydrator($hydrationMode);
    }

    /**
     * {@inheritDoc}
     */
    public function getProxyFactory()
    {
        $this->initializer5acaafb44f471098593856 && $this->initializer5acaafb44f471098593856->__invoke($this->valueHolder5acaafb44f422127025819, $this, 'getProxyFactory', array(), $this->initializer5acaafb44f471098593856);

        return $this->valueHolder5acaafb44f422127025819->getProxyFactory();
    }

    /**
     * {@inheritDoc}
     */
    public function initializeObject($obj)
    {
        $this->initializer5acaafb44f471098593856 && $this->initializer5acaafb44f471098593856->__invoke($this->valueHolder5acaafb44f422127025819, $this, 'initializeObject', array('obj' => $obj), $this->initializer5acaafb44f471098593856);

        return $this->valueHolder5acaafb44f422127025819->initializeObject($obj);
    }

    /**
     * {@inheritDoc}
     */
    public function getFilters()
    {
        $this->initializer5acaafb44f471098593856 && $this->initializer5acaafb44f471098593856->__invoke($this->valueHolder5acaafb44f422127025819, $this, 'getFilters', array(), $this->initializer5acaafb44f471098593856);

        return $this->valueHolder5acaafb44f422127025819->getFilters();
    }

    /**
     * {@inheritDoc}
     */
    public function isFiltersStateClean()
    {
        $this->initializer5acaafb44f471098593856 && $this->initializer5acaafb44f471098593856->__invoke($this->valueHolder5acaafb44f422127025819, $this, 'isFiltersStateClean', array(), $this->initializer5acaafb44f471098593856);

        return $this->valueHolder5acaafb44f422127025819->isFiltersStateClean();
    }

    /**
     * {@inheritDoc}
     */
    public function hasFilters()
    {
        $this->initializer5acaafb44f471098593856 && $this->initializer5acaafb44f471098593856->__invoke($this->valueHolder5acaafb44f422127025819, $this, 'hasFilters', array(), $this->initializer5acaafb44f471098593856);

        return $this->valueHolder5acaafb44f422127025819->hasFilters();
    }

    /**
     * @override constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public function __construct($initializer)
    {
        $this->initializer5acaafb44f471098593856 = $initializer;
    }

    /**
     * @param string $name
     */
    public function & __get($name)
    {
        $this->initializer5acaafb44f471098593856 && $this->initializer5acaafb44f471098593856->__invoke($this->valueHolder5acaafb44f422127025819, $this, '__get', array('name' => $name), $this->initializer5acaafb44f471098593856);

        if (isset(self::$publicProperties5acaafb44f3e1825496449[$name])) {
            return $this->valueHolder5acaafb44f422127025819->$name;
        }

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5acaafb44f422127025819;

            $backtrace = debug_backtrace(false);
            trigger_error('Undefined property: ' . get_parent_class($this) . '::$' . $name . ' in ' . $backtrace[0]['file'] . ' on line ' . $backtrace[0]['line'], \E_USER_NOTICE);
            return $targetObject->$name;;
            return;
        }

        $targetObject = $this->valueHolder5acaafb44f422127025819;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    /**
     * @param string $name
     * @param mixed $value
     */
    public function __set($name, $value)
    {
        $this->initializer5acaafb44f471098593856 && $this->initializer5acaafb44f471098593856->__invoke($this->valueHolder5acaafb44f422127025819, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer5acaafb44f471098593856);

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5acaafb44f422127025819;

            return $targetObject->$name = $value;;
            return;
        }

        $targetObject = $this->valueHolder5acaafb44f422127025819;
        $accessor = function & () use ($targetObject, $name, $value) {
            return $targetObject->$name = $value;
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    /**
     * @param string $name
     */
    public function __isset($name)
    {
        $this->initializer5acaafb44f471098593856 && $this->initializer5acaafb44f471098593856->__invoke($this->valueHolder5acaafb44f422127025819, $this, '__isset', array('name' => $name), $this->initializer5acaafb44f471098593856);

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5acaafb44f422127025819;

            return isset($targetObject->$name);;
            return;
        }

        $targetObject = $this->valueHolder5acaafb44f422127025819;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    /**
     * @param string $name
     */
    public function __unset($name)
    {
        $this->initializer5acaafb44f471098593856 && $this->initializer5acaafb44f471098593856->__invoke($this->valueHolder5acaafb44f422127025819, $this, '__unset', array('name' => $name), $this->initializer5acaafb44f471098593856);

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5acaafb44f422127025819;

            unset($targetObject->$name);;
            return;
        }

        $targetObject = $this->valueHolder5acaafb44f422127025819;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __clone()
    {
        $this->initializer5acaafb44f471098593856 && $this->initializer5acaafb44f471098593856->__invoke($this->valueHolder5acaafb44f422127025819, $this, '__clone', array(), $this->initializer5acaafb44f471098593856);

        $this->valueHolder5acaafb44f422127025819 = clone $this->valueHolder5acaafb44f422127025819;
    }

    public function __sleep()
    {
        $this->initializer5acaafb44f471098593856 && $this->initializer5acaafb44f471098593856->__invoke($this->valueHolder5acaafb44f422127025819, $this, '__sleep', array(), $this->initializer5acaafb44f471098593856);

        return array('valueHolder5acaafb44f422127025819');
    }

    public function __wakeup()
    {
    }

    /**
     * {@inheritDoc}
     */
    public function setProxyInitializer(\Closure $initializer = null)
    {
        $this->initializer5acaafb44f471098593856 = $initializer;
    }

    /**
     * {@inheritDoc}
     */
    public function getProxyInitializer()
    {
        return $this->initializer5acaafb44f471098593856;
    }

    /**
     * {@inheritDoc}
     */
    public function initializeProxy()
    {
        return $this->initializer5acaafb44f471098593856 && $this->initializer5acaafb44f471098593856->__invoke($this->valueHolder5acaafb44f422127025819, $this, 'initializeProxy', array(), $this->initializer5acaafb44f471098593856);
    }

    /**
     * {@inheritDoc}
     */
    public function isProxyInitialized()
    {
        return null !== $this->valueHolder5acaafb44f422127025819;
    }

    /**
     * {@inheritDoc}
     */
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder5acaafb44f422127025819;
    }


}

class DoctrineORMEntityManager_000000002084058400000000634d02687471c441876336872902bf356897607e extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $valueHolder5acaafb46726b276218504 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer5acaafb467289887786546 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties5acaafb467224282486606 = array(
        
    );

    /**
     * {@inheritDoc}
     */
    public function getConnection()
    {
        $this->initializer5acaafb467289887786546 && $this->initializer5acaafb467289887786546->__invoke($this->valueHolder5acaafb46726b276218504, $this, 'getConnection', array(), $this->initializer5acaafb467289887786546);

        return $this->valueHolder5acaafb46726b276218504->getConnection();
    }

    /**
     * {@inheritDoc}
     */
    public function getMetadataFactory()
    {
        $this->initializer5acaafb467289887786546 && $this->initializer5acaafb467289887786546->__invoke($this->valueHolder5acaafb46726b276218504, $this, 'getMetadataFactory', array(), $this->initializer5acaafb467289887786546);

        return $this->valueHolder5acaafb46726b276218504->getMetadataFactory();
    }

    /**
     * {@inheritDoc}
     */
    public function getExpressionBuilder()
    {
        $this->initializer5acaafb467289887786546 && $this->initializer5acaafb467289887786546->__invoke($this->valueHolder5acaafb46726b276218504, $this, 'getExpressionBuilder', array(), $this->initializer5acaafb467289887786546);

        return $this->valueHolder5acaafb46726b276218504->getExpressionBuilder();
    }

    /**
     * {@inheritDoc}
     */
    public function beginTransaction()
    {
        $this->initializer5acaafb467289887786546 && $this->initializer5acaafb467289887786546->__invoke($this->valueHolder5acaafb46726b276218504, $this, 'beginTransaction', array(), $this->initializer5acaafb467289887786546);

        return $this->valueHolder5acaafb46726b276218504->beginTransaction();
    }

    /**
     * {@inheritDoc}
     */
    public function getCache()
    {
        $this->initializer5acaafb467289887786546 && $this->initializer5acaafb467289887786546->__invoke($this->valueHolder5acaafb46726b276218504, $this, 'getCache', array(), $this->initializer5acaafb467289887786546);

        return $this->valueHolder5acaafb46726b276218504->getCache();
    }

    /**
     * {@inheritDoc}
     */
    public function transactional($func)
    {
        $this->initializer5acaafb467289887786546 && $this->initializer5acaafb467289887786546->__invoke($this->valueHolder5acaafb46726b276218504, $this, 'transactional', array('func' => $func), $this->initializer5acaafb467289887786546);

        return $this->valueHolder5acaafb46726b276218504->transactional($func);
    }

    /**
     * {@inheritDoc}
     */
    public function commit()
    {
        $this->initializer5acaafb467289887786546 && $this->initializer5acaafb467289887786546->__invoke($this->valueHolder5acaafb46726b276218504, $this, 'commit', array(), $this->initializer5acaafb467289887786546);

        return $this->valueHolder5acaafb46726b276218504->commit();
    }

    /**
     * {@inheritDoc}
     */
    public function rollback()
    {
        $this->initializer5acaafb467289887786546 && $this->initializer5acaafb467289887786546->__invoke($this->valueHolder5acaafb46726b276218504, $this, 'rollback', array(), $this->initializer5acaafb467289887786546);

        return $this->valueHolder5acaafb46726b276218504->rollback();
    }

    /**
     * {@inheritDoc}
     */
    public function getClassMetadata($className)
    {
        $this->initializer5acaafb467289887786546 && $this->initializer5acaafb467289887786546->__invoke($this->valueHolder5acaafb46726b276218504, $this, 'getClassMetadata', array('className' => $className), $this->initializer5acaafb467289887786546);

        return $this->valueHolder5acaafb46726b276218504->getClassMetadata($className);
    }

    /**
     * {@inheritDoc}
     */
    public function createQuery($dql = '')
    {
        $this->initializer5acaafb467289887786546 && $this->initializer5acaafb467289887786546->__invoke($this->valueHolder5acaafb46726b276218504, $this, 'createQuery', array('dql' => $dql), $this->initializer5acaafb467289887786546);

        return $this->valueHolder5acaafb46726b276218504->createQuery($dql);
    }

    /**
     * {@inheritDoc}
     */
    public function createNamedQuery($name)
    {
        $this->initializer5acaafb467289887786546 && $this->initializer5acaafb467289887786546->__invoke($this->valueHolder5acaafb46726b276218504, $this, 'createNamedQuery', array('name' => $name), $this->initializer5acaafb467289887786546);

        return $this->valueHolder5acaafb46726b276218504->createNamedQuery($name);
    }

    /**
     * {@inheritDoc}
     */
    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializer5acaafb467289887786546 && $this->initializer5acaafb467289887786546->__invoke($this->valueHolder5acaafb46726b276218504, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializer5acaafb467289887786546);

        return $this->valueHolder5acaafb46726b276218504->createNativeQuery($sql, $rsm);
    }

    /**
     * {@inheritDoc}
     */
    public function createNamedNativeQuery($name)
    {
        $this->initializer5acaafb467289887786546 && $this->initializer5acaafb467289887786546->__invoke($this->valueHolder5acaafb46726b276218504, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializer5acaafb467289887786546);

        return $this->valueHolder5acaafb46726b276218504->createNamedNativeQuery($name);
    }

    /**
     * {@inheritDoc}
     */
    public function createQueryBuilder()
    {
        $this->initializer5acaafb467289887786546 && $this->initializer5acaafb467289887786546->__invoke($this->valueHolder5acaafb46726b276218504, $this, 'createQueryBuilder', array(), $this->initializer5acaafb467289887786546);

        return $this->valueHolder5acaafb46726b276218504->createQueryBuilder();
    }

    /**
     * {@inheritDoc}
     */
    public function flush($entity = null)
    {
        $this->initializer5acaafb467289887786546 && $this->initializer5acaafb467289887786546->__invoke($this->valueHolder5acaafb46726b276218504, $this, 'flush', array('entity' => $entity), $this->initializer5acaafb467289887786546);

        return $this->valueHolder5acaafb46726b276218504->flush($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function find($entityName, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer5acaafb467289887786546 && $this->initializer5acaafb467289887786546->__invoke($this->valueHolder5acaafb46726b276218504, $this, 'find', array('entityName' => $entityName, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer5acaafb467289887786546);

        return $this->valueHolder5acaafb46726b276218504->find($entityName, $id, $lockMode, $lockVersion);
    }

    /**
     * {@inheritDoc}
     */
    public function getReference($entityName, $id)
    {
        $this->initializer5acaafb467289887786546 && $this->initializer5acaafb467289887786546->__invoke($this->valueHolder5acaafb46726b276218504, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializer5acaafb467289887786546);

        return $this->valueHolder5acaafb46726b276218504->getReference($entityName, $id);
    }

    /**
     * {@inheritDoc}
     */
    public function getPartialReference($entityName, $identifier)
    {
        $this->initializer5acaafb467289887786546 && $this->initializer5acaafb467289887786546->__invoke($this->valueHolder5acaafb46726b276218504, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializer5acaafb467289887786546);

        return $this->valueHolder5acaafb46726b276218504->getPartialReference($entityName, $identifier);
    }

    /**
     * {@inheritDoc}
     */
    public function clear($entityName = null)
    {
        $this->initializer5acaafb467289887786546 && $this->initializer5acaafb467289887786546->__invoke($this->valueHolder5acaafb46726b276218504, $this, 'clear', array('entityName' => $entityName), $this->initializer5acaafb467289887786546);

        return $this->valueHolder5acaafb46726b276218504->clear($entityName);
    }

    /**
     * {@inheritDoc}
     */
    public function close()
    {
        $this->initializer5acaafb467289887786546 && $this->initializer5acaafb467289887786546->__invoke($this->valueHolder5acaafb46726b276218504, $this, 'close', array(), $this->initializer5acaafb467289887786546);

        return $this->valueHolder5acaafb46726b276218504->close();
    }

    /**
     * {@inheritDoc}
     */
    public function persist($entity)
    {
        $this->initializer5acaafb467289887786546 && $this->initializer5acaafb467289887786546->__invoke($this->valueHolder5acaafb46726b276218504, $this, 'persist', array('entity' => $entity), $this->initializer5acaafb467289887786546);

        return $this->valueHolder5acaafb46726b276218504->persist($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function remove($entity)
    {
        $this->initializer5acaafb467289887786546 && $this->initializer5acaafb467289887786546->__invoke($this->valueHolder5acaafb46726b276218504, $this, 'remove', array('entity' => $entity), $this->initializer5acaafb467289887786546);

        return $this->valueHolder5acaafb46726b276218504->remove($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function refresh($entity)
    {
        $this->initializer5acaafb467289887786546 && $this->initializer5acaafb467289887786546->__invoke($this->valueHolder5acaafb46726b276218504, $this, 'refresh', array('entity' => $entity), $this->initializer5acaafb467289887786546);

        return $this->valueHolder5acaafb46726b276218504->refresh($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function detach($entity)
    {
        $this->initializer5acaafb467289887786546 && $this->initializer5acaafb467289887786546->__invoke($this->valueHolder5acaafb46726b276218504, $this, 'detach', array('entity' => $entity), $this->initializer5acaafb467289887786546);

        return $this->valueHolder5acaafb46726b276218504->detach($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function merge($entity)
    {
        $this->initializer5acaafb467289887786546 && $this->initializer5acaafb467289887786546->__invoke($this->valueHolder5acaafb46726b276218504, $this, 'merge', array('entity' => $entity), $this->initializer5acaafb467289887786546);

        return $this->valueHolder5acaafb46726b276218504->merge($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function copy($entity, $deep = false)
    {
        $this->initializer5acaafb467289887786546 && $this->initializer5acaafb467289887786546->__invoke($this->valueHolder5acaafb46726b276218504, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializer5acaafb467289887786546);

        return $this->valueHolder5acaafb46726b276218504->copy($entity, $deep);
    }

    /**
     * {@inheritDoc}
     */
    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializer5acaafb467289887786546 && $this->initializer5acaafb467289887786546->__invoke($this->valueHolder5acaafb46726b276218504, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer5acaafb467289887786546);

        return $this->valueHolder5acaafb46726b276218504->lock($entity, $lockMode, $lockVersion);
    }

    /**
     * {@inheritDoc}
     */
    public function getRepository($entityName)
    {
        $this->initializer5acaafb467289887786546 && $this->initializer5acaafb467289887786546->__invoke($this->valueHolder5acaafb46726b276218504, $this, 'getRepository', array('entityName' => $entityName), $this->initializer5acaafb467289887786546);

        return $this->valueHolder5acaafb46726b276218504->getRepository($entityName);
    }

    /**
     * {@inheritDoc}
     */
    public function contains($entity)
    {
        $this->initializer5acaafb467289887786546 && $this->initializer5acaafb467289887786546->__invoke($this->valueHolder5acaafb46726b276218504, $this, 'contains', array('entity' => $entity), $this->initializer5acaafb467289887786546);

        return $this->valueHolder5acaafb46726b276218504->contains($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function getEventManager()
    {
        $this->initializer5acaafb467289887786546 && $this->initializer5acaafb467289887786546->__invoke($this->valueHolder5acaafb46726b276218504, $this, 'getEventManager', array(), $this->initializer5acaafb467289887786546);

        return $this->valueHolder5acaafb46726b276218504->getEventManager();
    }

    /**
     * {@inheritDoc}
     */
    public function getConfiguration()
    {
        $this->initializer5acaafb467289887786546 && $this->initializer5acaafb467289887786546->__invoke($this->valueHolder5acaafb46726b276218504, $this, 'getConfiguration', array(), $this->initializer5acaafb467289887786546);

        return $this->valueHolder5acaafb46726b276218504->getConfiguration();
    }

    /**
     * {@inheritDoc}
     */
    public function isOpen()
    {
        $this->initializer5acaafb467289887786546 && $this->initializer5acaafb467289887786546->__invoke($this->valueHolder5acaafb46726b276218504, $this, 'isOpen', array(), $this->initializer5acaafb467289887786546);

        return $this->valueHolder5acaafb46726b276218504->isOpen();
    }

    /**
     * {@inheritDoc}
     */
    public function getUnitOfWork()
    {
        $this->initializer5acaafb467289887786546 && $this->initializer5acaafb467289887786546->__invoke($this->valueHolder5acaafb46726b276218504, $this, 'getUnitOfWork', array(), $this->initializer5acaafb467289887786546);

        return $this->valueHolder5acaafb46726b276218504->getUnitOfWork();
    }

    /**
     * {@inheritDoc}
     */
    public function getHydrator($hydrationMode)
    {
        $this->initializer5acaafb467289887786546 && $this->initializer5acaafb467289887786546->__invoke($this->valueHolder5acaafb46726b276218504, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializer5acaafb467289887786546);

        return $this->valueHolder5acaafb46726b276218504->getHydrator($hydrationMode);
    }

    /**
     * {@inheritDoc}
     */
    public function newHydrator($hydrationMode)
    {
        $this->initializer5acaafb467289887786546 && $this->initializer5acaafb467289887786546->__invoke($this->valueHolder5acaafb46726b276218504, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializer5acaafb467289887786546);

        return $this->valueHolder5acaafb46726b276218504->newHydrator($hydrationMode);
    }

    /**
     * {@inheritDoc}
     */
    public function getProxyFactory()
    {
        $this->initializer5acaafb467289887786546 && $this->initializer5acaafb467289887786546->__invoke($this->valueHolder5acaafb46726b276218504, $this, 'getProxyFactory', array(), $this->initializer5acaafb467289887786546);

        return $this->valueHolder5acaafb46726b276218504->getProxyFactory();
    }

    /**
     * {@inheritDoc}
     */
    public function initializeObject($obj)
    {
        $this->initializer5acaafb467289887786546 && $this->initializer5acaafb467289887786546->__invoke($this->valueHolder5acaafb46726b276218504, $this, 'initializeObject', array('obj' => $obj), $this->initializer5acaafb467289887786546);

        return $this->valueHolder5acaafb46726b276218504->initializeObject($obj);
    }

    /**
     * {@inheritDoc}
     */
    public function getFilters()
    {
        $this->initializer5acaafb467289887786546 && $this->initializer5acaafb467289887786546->__invoke($this->valueHolder5acaafb46726b276218504, $this, 'getFilters', array(), $this->initializer5acaafb467289887786546);

        return $this->valueHolder5acaafb46726b276218504->getFilters();
    }

    /**
     * {@inheritDoc}
     */
    public function isFiltersStateClean()
    {
        $this->initializer5acaafb467289887786546 && $this->initializer5acaafb467289887786546->__invoke($this->valueHolder5acaafb46726b276218504, $this, 'isFiltersStateClean', array(), $this->initializer5acaafb467289887786546);

        return $this->valueHolder5acaafb46726b276218504->isFiltersStateClean();
    }

    /**
     * {@inheritDoc}
     */
    public function hasFilters()
    {
        $this->initializer5acaafb467289887786546 && $this->initializer5acaafb467289887786546->__invoke($this->valueHolder5acaafb46726b276218504, $this, 'hasFilters', array(), $this->initializer5acaafb467289887786546);

        return $this->valueHolder5acaafb46726b276218504->hasFilters();
    }

    /**
     * @override constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public function __construct($initializer)
    {
        $this->initializer5acaafb467289887786546 = $initializer;
    }

    /**
     * @param string $name
     */
    public function & __get($name)
    {
        $this->initializer5acaafb467289887786546 && $this->initializer5acaafb467289887786546->__invoke($this->valueHolder5acaafb46726b276218504, $this, '__get', array('name' => $name), $this->initializer5acaafb467289887786546);

        if (isset(self::$publicProperties5acaafb467224282486606[$name])) {
            return $this->valueHolder5acaafb46726b276218504->$name;
        }

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5acaafb46726b276218504;

            $backtrace = debug_backtrace(false);
            trigger_error('Undefined property: ' . get_parent_class($this) . '::$' . $name . ' in ' . $backtrace[0]['file'] . ' on line ' . $backtrace[0]['line'], \E_USER_NOTICE);
            return $targetObject->$name;;
            return;
        }

        $targetObject = $this->valueHolder5acaafb46726b276218504;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    /**
     * @param string $name
     * @param mixed $value
     */
    public function __set($name, $value)
    {
        $this->initializer5acaafb467289887786546 && $this->initializer5acaafb467289887786546->__invoke($this->valueHolder5acaafb46726b276218504, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer5acaafb467289887786546);

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5acaafb46726b276218504;

            return $targetObject->$name = $value;;
            return;
        }

        $targetObject = $this->valueHolder5acaafb46726b276218504;
        $accessor = function & () use ($targetObject, $name, $value) {
            return $targetObject->$name = $value;
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    /**
     * @param string $name
     */
    public function __isset($name)
    {
        $this->initializer5acaafb467289887786546 && $this->initializer5acaafb467289887786546->__invoke($this->valueHolder5acaafb46726b276218504, $this, '__isset', array('name' => $name), $this->initializer5acaafb467289887786546);

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5acaafb46726b276218504;

            return isset($targetObject->$name);;
            return;
        }

        $targetObject = $this->valueHolder5acaafb46726b276218504;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    /**
     * @param string $name
     */
    public function __unset($name)
    {
        $this->initializer5acaafb467289887786546 && $this->initializer5acaafb467289887786546->__invoke($this->valueHolder5acaafb46726b276218504, $this, '__unset', array('name' => $name), $this->initializer5acaafb467289887786546);

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5acaafb46726b276218504;

            unset($targetObject->$name);;
            return;
        }

        $targetObject = $this->valueHolder5acaafb46726b276218504;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __clone()
    {
        $this->initializer5acaafb467289887786546 && $this->initializer5acaafb467289887786546->__invoke($this->valueHolder5acaafb46726b276218504, $this, '__clone', array(), $this->initializer5acaafb467289887786546);

        $this->valueHolder5acaafb46726b276218504 = clone $this->valueHolder5acaafb46726b276218504;
    }

    public function __sleep()
    {
        $this->initializer5acaafb467289887786546 && $this->initializer5acaafb467289887786546->__invoke($this->valueHolder5acaafb46726b276218504, $this, '__sleep', array(), $this->initializer5acaafb467289887786546);

        return array('valueHolder5acaafb46726b276218504');
    }

    public function __wakeup()
    {
    }

    /**
     * {@inheritDoc}
     */
    public function setProxyInitializer(\Closure $initializer = null)
    {
        $this->initializer5acaafb467289887786546 = $initializer;
    }

    /**
     * {@inheritDoc}
     */
    public function getProxyInitializer()
    {
        return $this->initializer5acaafb467289887786546;
    }

    /**
     * {@inheritDoc}
     */
    public function initializeProxy()
    {
        return $this->initializer5acaafb467289887786546 && $this->initializer5acaafb467289887786546->__invoke($this->valueHolder5acaafb46726b276218504, $this, 'initializeProxy', array(), $this->initializer5acaafb467289887786546);
    }

    /**
     * {@inheritDoc}
     */
    public function isProxyInitialized()
    {
        return null !== $this->valueHolder5acaafb46726b276218504;
    }

    /**
     * {@inheritDoc}
     */
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder5acaafb46726b276218504;
    }


}

class DoctrineORMEntityManager_000000002084059e00000000634d02687471c441876336872902bf356897607e extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $valueHolder5acaafb47f479164142711 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer5acaafb47f498537203639 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties5acaafb47f435667603235 = array(
        
    );

    /**
     * {@inheritDoc}
     */
    public function getConnection()
    {
        $this->initializer5acaafb47f498537203639 && $this->initializer5acaafb47f498537203639->__invoke($this->valueHolder5acaafb47f479164142711, $this, 'getConnection', array(), $this->initializer5acaafb47f498537203639);

        return $this->valueHolder5acaafb47f479164142711->getConnection();
    }

    /**
     * {@inheritDoc}
     */
    public function getMetadataFactory()
    {
        $this->initializer5acaafb47f498537203639 && $this->initializer5acaafb47f498537203639->__invoke($this->valueHolder5acaafb47f479164142711, $this, 'getMetadataFactory', array(), $this->initializer5acaafb47f498537203639);

        return $this->valueHolder5acaafb47f479164142711->getMetadataFactory();
    }

    /**
     * {@inheritDoc}
     */
    public function getExpressionBuilder()
    {
        $this->initializer5acaafb47f498537203639 && $this->initializer5acaafb47f498537203639->__invoke($this->valueHolder5acaafb47f479164142711, $this, 'getExpressionBuilder', array(), $this->initializer5acaafb47f498537203639);

        return $this->valueHolder5acaafb47f479164142711->getExpressionBuilder();
    }

    /**
     * {@inheritDoc}
     */
    public function beginTransaction()
    {
        $this->initializer5acaafb47f498537203639 && $this->initializer5acaafb47f498537203639->__invoke($this->valueHolder5acaafb47f479164142711, $this, 'beginTransaction', array(), $this->initializer5acaafb47f498537203639);

        return $this->valueHolder5acaafb47f479164142711->beginTransaction();
    }

    /**
     * {@inheritDoc}
     */
    public function getCache()
    {
        $this->initializer5acaafb47f498537203639 && $this->initializer5acaafb47f498537203639->__invoke($this->valueHolder5acaafb47f479164142711, $this, 'getCache', array(), $this->initializer5acaafb47f498537203639);

        return $this->valueHolder5acaafb47f479164142711->getCache();
    }

    /**
     * {@inheritDoc}
     */
    public function transactional($func)
    {
        $this->initializer5acaafb47f498537203639 && $this->initializer5acaafb47f498537203639->__invoke($this->valueHolder5acaafb47f479164142711, $this, 'transactional', array('func' => $func), $this->initializer5acaafb47f498537203639);

        return $this->valueHolder5acaafb47f479164142711->transactional($func);
    }

    /**
     * {@inheritDoc}
     */
    public function commit()
    {
        $this->initializer5acaafb47f498537203639 && $this->initializer5acaafb47f498537203639->__invoke($this->valueHolder5acaafb47f479164142711, $this, 'commit', array(), $this->initializer5acaafb47f498537203639);

        return $this->valueHolder5acaafb47f479164142711->commit();
    }

    /**
     * {@inheritDoc}
     */
    public function rollback()
    {
        $this->initializer5acaafb47f498537203639 && $this->initializer5acaafb47f498537203639->__invoke($this->valueHolder5acaafb47f479164142711, $this, 'rollback', array(), $this->initializer5acaafb47f498537203639);

        return $this->valueHolder5acaafb47f479164142711->rollback();
    }

    /**
     * {@inheritDoc}
     */
    public function getClassMetadata($className)
    {
        $this->initializer5acaafb47f498537203639 && $this->initializer5acaafb47f498537203639->__invoke($this->valueHolder5acaafb47f479164142711, $this, 'getClassMetadata', array('className' => $className), $this->initializer5acaafb47f498537203639);

        return $this->valueHolder5acaafb47f479164142711->getClassMetadata($className);
    }

    /**
     * {@inheritDoc}
     */
    public function createQuery($dql = '')
    {
        $this->initializer5acaafb47f498537203639 && $this->initializer5acaafb47f498537203639->__invoke($this->valueHolder5acaafb47f479164142711, $this, 'createQuery', array('dql' => $dql), $this->initializer5acaafb47f498537203639);

        return $this->valueHolder5acaafb47f479164142711->createQuery($dql);
    }

    /**
     * {@inheritDoc}
     */
    public function createNamedQuery($name)
    {
        $this->initializer5acaafb47f498537203639 && $this->initializer5acaafb47f498537203639->__invoke($this->valueHolder5acaafb47f479164142711, $this, 'createNamedQuery', array('name' => $name), $this->initializer5acaafb47f498537203639);

        return $this->valueHolder5acaafb47f479164142711->createNamedQuery($name);
    }

    /**
     * {@inheritDoc}
     */
    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializer5acaafb47f498537203639 && $this->initializer5acaafb47f498537203639->__invoke($this->valueHolder5acaafb47f479164142711, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializer5acaafb47f498537203639);

        return $this->valueHolder5acaafb47f479164142711->createNativeQuery($sql, $rsm);
    }

    /**
     * {@inheritDoc}
     */
    public function createNamedNativeQuery($name)
    {
        $this->initializer5acaafb47f498537203639 && $this->initializer5acaafb47f498537203639->__invoke($this->valueHolder5acaafb47f479164142711, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializer5acaafb47f498537203639);

        return $this->valueHolder5acaafb47f479164142711->createNamedNativeQuery($name);
    }

    /**
     * {@inheritDoc}
     */
    public function createQueryBuilder()
    {
        $this->initializer5acaafb47f498537203639 && $this->initializer5acaafb47f498537203639->__invoke($this->valueHolder5acaafb47f479164142711, $this, 'createQueryBuilder', array(), $this->initializer5acaafb47f498537203639);

        return $this->valueHolder5acaafb47f479164142711->createQueryBuilder();
    }

    /**
     * {@inheritDoc}
     */
    public function flush($entity = null)
    {
        $this->initializer5acaafb47f498537203639 && $this->initializer5acaafb47f498537203639->__invoke($this->valueHolder5acaafb47f479164142711, $this, 'flush', array('entity' => $entity), $this->initializer5acaafb47f498537203639);

        return $this->valueHolder5acaafb47f479164142711->flush($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function find($entityName, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer5acaafb47f498537203639 && $this->initializer5acaafb47f498537203639->__invoke($this->valueHolder5acaafb47f479164142711, $this, 'find', array('entityName' => $entityName, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer5acaafb47f498537203639);

        return $this->valueHolder5acaafb47f479164142711->find($entityName, $id, $lockMode, $lockVersion);
    }

    /**
     * {@inheritDoc}
     */
    public function getReference($entityName, $id)
    {
        $this->initializer5acaafb47f498537203639 && $this->initializer5acaafb47f498537203639->__invoke($this->valueHolder5acaafb47f479164142711, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializer5acaafb47f498537203639);

        return $this->valueHolder5acaafb47f479164142711->getReference($entityName, $id);
    }

    /**
     * {@inheritDoc}
     */
    public function getPartialReference($entityName, $identifier)
    {
        $this->initializer5acaafb47f498537203639 && $this->initializer5acaafb47f498537203639->__invoke($this->valueHolder5acaafb47f479164142711, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializer5acaafb47f498537203639);

        return $this->valueHolder5acaafb47f479164142711->getPartialReference($entityName, $identifier);
    }

    /**
     * {@inheritDoc}
     */
    public function clear($entityName = null)
    {
        $this->initializer5acaafb47f498537203639 && $this->initializer5acaafb47f498537203639->__invoke($this->valueHolder5acaafb47f479164142711, $this, 'clear', array('entityName' => $entityName), $this->initializer5acaafb47f498537203639);

        return $this->valueHolder5acaafb47f479164142711->clear($entityName);
    }

    /**
     * {@inheritDoc}
     */
    public function close()
    {
        $this->initializer5acaafb47f498537203639 && $this->initializer5acaafb47f498537203639->__invoke($this->valueHolder5acaafb47f479164142711, $this, 'close', array(), $this->initializer5acaafb47f498537203639);

        return $this->valueHolder5acaafb47f479164142711->close();
    }

    /**
     * {@inheritDoc}
     */
    public function persist($entity)
    {
        $this->initializer5acaafb47f498537203639 && $this->initializer5acaafb47f498537203639->__invoke($this->valueHolder5acaafb47f479164142711, $this, 'persist', array('entity' => $entity), $this->initializer5acaafb47f498537203639);

        return $this->valueHolder5acaafb47f479164142711->persist($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function remove($entity)
    {
        $this->initializer5acaafb47f498537203639 && $this->initializer5acaafb47f498537203639->__invoke($this->valueHolder5acaafb47f479164142711, $this, 'remove', array('entity' => $entity), $this->initializer5acaafb47f498537203639);

        return $this->valueHolder5acaafb47f479164142711->remove($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function refresh($entity)
    {
        $this->initializer5acaafb47f498537203639 && $this->initializer5acaafb47f498537203639->__invoke($this->valueHolder5acaafb47f479164142711, $this, 'refresh', array('entity' => $entity), $this->initializer5acaafb47f498537203639);

        return $this->valueHolder5acaafb47f479164142711->refresh($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function detach($entity)
    {
        $this->initializer5acaafb47f498537203639 && $this->initializer5acaafb47f498537203639->__invoke($this->valueHolder5acaafb47f479164142711, $this, 'detach', array('entity' => $entity), $this->initializer5acaafb47f498537203639);

        return $this->valueHolder5acaafb47f479164142711->detach($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function merge($entity)
    {
        $this->initializer5acaafb47f498537203639 && $this->initializer5acaafb47f498537203639->__invoke($this->valueHolder5acaafb47f479164142711, $this, 'merge', array('entity' => $entity), $this->initializer5acaafb47f498537203639);

        return $this->valueHolder5acaafb47f479164142711->merge($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function copy($entity, $deep = false)
    {
        $this->initializer5acaafb47f498537203639 && $this->initializer5acaafb47f498537203639->__invoke($this->valueHolder5acaafb47f479164142711, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializer5acaafb47f498537203639);

        return $this->valueHolder5acaafb47f479164142711->copy($entity, $deep);
    }

    /**
     * {@inheritDoc}
     */
    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializer5acaafb47f498537203639 && $this->initializer5acaafb47f498537203639->__invoke($this->valueHolder5acaafb47f479164142711, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer5acaafb47f498537203639);

        return $this->valueHolder5acaafb47f479164142711->lock($entity, $lockMode, $lockVersion);
    }

    /**
     * {@inheritDoc}
     */
    public function getRepository($entityName)
    {
        $this->initializer5acaafb47f498537203639 && $this->initializer5acaafb47f498537203639->__invoke($this->valueHolder5acaafb47f479164142711, $this, 'getRepository', array('entityName' => $entityName), $this->initializer5acaafb47f498537203639);

        return $this->valueHolder5acaafb47f479164142711->getRepository($entityName);
    }

    /**
     * {@inheritDoc}
     */
    public function contains($entity)
    {
        $this->initializer5acaafb47f498537203639 && $this->initializer5acaafb47f498537203639->__invoke($this->valueHolder5acaafb47f479164142711, $this, 'contains', array('entity' => $entity), $this->initializer5acaafb47f498537203639);

        return $this->valueHolder5acaafb47f479164142711->contains($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function getEventManager()
    {
        $this->initializer5acaafb47f498537203639 && $this->initializer5acaafb47f498537203639->__invoke($this->valueHolder5acaafb47f479164142711, $this, 'getEventManager', array(), $this->initializer5acaafb47f498537203639);

        return $this->valueHolder5acaafb47f479164142711->getEventManager();
    }

    /**
     * {@inheritDoc}
     */
    public function getConfiguration()
    {
        $this->initializer5acaafb47f498537203639 && $this->initializer5acaafb47f498537203639->__invoke($this->valueHolder5acaafb47f479164142711, $this, 'getConfiguration', array(), $this->initializer5acaafb47f498537203639);

        return $this->valueHolder5acaafb47f479164142711->getConfiguration();
    }

    /**
     * {@inheritDoc}
     */
    public function isOpen()
    {
        $this->initializer5acaafb47f498537203639 && $this->initializer5acaafb47f498537203639->__invoke($this->valueHolder5acaafb47f479164142711, $this, 'isOpen', array(), $this->initializer5acaafb47f498537203639);

        return $this->valueHolder5acaafb47f479164142711->isOpen();
    }

    /**
     * {@inheritDoc}
     */
    public function getUnitOfWork()
    {
        $this->initializer5acaafb47f498537203639 && $this->initializer5acaafb47f498537203639->__invoke($this->valueHolder5acaafb47f479164142711, $this, 'getUnitOfWork', array(), $this->initializer5acaafb47f498537203639);

        return $this->valueHolder5acaafb47f479164142711->getUnitOfWork();
    }

    /**
     * {@inheritDoc}
     */
    public function getHydrator($hydrationMode)
    {
        $this->initializer5acaafb47f498537203639 && $this->initializer5acaafb47f498537203639->__invoke($this->valueHolder5acaafb47f479164142711, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializer5acaafb47f498537203639);

        return $this->valueHolder5acaafb47f479164142711->getHydrator($hydrationMode);
    }

    /**
     * {@inheritDoc}
     */
    public function newHydrator($hydrationMode)
    {
        $this->initializer5acaafb47f498537203639 && $this->initializer5acaafb47f498537203639->__invoke($this->valueHolder5acaafb47f479164142711, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializer5acaafb47f498537203639);

        return $this->valueHolder5acaafb47f479164142711->newHydrator($hydrationMode);
    }

    /**
     * {@inheritDoc}
     */
    public function getProxyFactory()
    {
        $this->initializer5acaafb47f498537203639 && $this->initializer5acaafb47f498537203639->__invoke($this->valueHolder5acaafb47f479164142711, $this, 'getProxyFactory', array(), $this->initializer5acaafb47f498537203639);

        return $this->valueHolder5acaafb47f479164142711->getProxyFactory();
    }

    /**
     * {@inheritDoc}
     */
    public function initializeObject($obj)
    {
        $this->initializer5acaafb47f498537203639 && $this->initializer5acaafb47f498537203639->__invoke($this->valueHolder5acaafb47f479164142711, $this, 'initializeObject', array('obj' => $obj), $this->initializer5acaafb47f498537203639);

        return $this->valueHolder5acaafb47f479164142711->initializeObject($obj);
    }

    /**
     * {@inheritDoc}
     */
    public function getFilters()
    {
        $this->initializer5acaafb47f498537203639 && $this->initializer5acaafb47f498537203639->__invoke($this->valueHolder5acaafb47f479164142711, $this, 'getFilters', array(), $this->initializer5acaafb47f498537203639);

        return $this->valueHolder5acaafb47f479164142711->getFilters();
    }

    /**
     * {@inheritDoc}
     */
    public function isFiltersStateClean()
    {
        $this->initializer5acaafb47f498537203639 && $this->initializer5acaafb47f498537203639->__invoke($this->valueHolder5acaafb47f479164142711, $this, 'isFiltersStateClean', array(), $this->initializer5acaafb47f498537203639);

        return $this->valueHolder5acaafb47f479164142711->isFiltersStateClean();
    }

    /**
     * {@inheritDoc}
     */
    public function hasFilters()
    {
        $this->initializer5acaafb47f498537203639 && $this->initializer5acaafb47f498537203639->__invoke($this->valueHolder5acaafb47f479164142711, $this, 'hasFilters', array(), $this->initializer5acaafb47f498537203639);

        return $this->valueHolder5acaafb47f479164142711->hasFilters();
    }

    /**
     * @override constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public function __construct($initializer)
    {
        $this->initializer5acaafb47f498537203639 = $initializer;
    }

    /**
     * @param string $name
     */
    public function & __get($name)
    {
        $this->initializer5acaafb47f498537203639 && $this->initializer5acaafb47f498537203639->__invoke($this->valueHolder5acaafb47f479164142711, $this, '__get', array('name' => $name), $this->initializer5acaafb47f498537203639);

        if (isset(self::$publicProperties5acaafb47f435667603235[$name])) {
            return $this->valueHolder5acaafb47f479164142711->$name;
        }

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5acaafb47f479164142711;

            $backtrace = debug_backtrace(false);
            trigger_error('Undefined property: ' . get_parent_class($this) . '::$' . $name . ' in ' . $backtrace[0]['file'] . ' on line ' . $backtrace[0]['line'], \E_USER_NOTICE);
            return $targetObject->$name;;
            return;
        }

        $targetObject = $this->valueHolder5acaafb47f479164142711;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    /**
     * @param string $name
     * @param mixed $value
     */
    public function __set($name, $value)
    {
        $this->initializer5acaafb47f498537203639 && $this->initializer5acaafb47f498537203639->__invoke($this->valueHolder5acaafb47f479164142711, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer5acaafb47f498537203639);

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5acaafb47f479164142711;

            return $targetObject->$name = $value;;
            return;
        }

        $targetObject = $this->valueHolder5acaafb47f479164142711;
        $accessor = function & () use ($targetObject, $name, $value) {
            return $targetObject->$name = $value;
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    /**
     * @param string $name
     */
    public function __isset($name)
    {
        $this->initializer5acaafb47f498537203639 && $this->initializer5acaafb47f498537203639->__invoke($this->valueHolder5acaafb47f479164142711, $this, '__isset', array('name' => $name), $this->initializer5acaafb47f498537203639);

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5acaafb47f479164142711;

            return isset($targetObject->$name);;
            return;
        }

        $targetObject = $this->valueHolder5acaafb47f479164142711;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    /**
     * @param string $name
     */
    public function __unset($name)
    {
        $this->initializer5acaafb47f498537203639 && $this->initializer5acaafb47f498537203639->__invoke($this->valueHolder5acaafb47f479164142711, $this, '__unset', array('name' => $name), $this->initializer5acaafb47f498537203639);

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5acaafb47f479164142711;

            unset($targetObject->$name);;
            return;
        }

        $targetObject = $this->valueHolder5acaafb47f479164142711;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __clone()
    {
        $this->initializer5acaafb47f498537203639 && $this->initializer5acaafb47f498537203639->__invoke($this->valueHolder5acaafb47f479164142711, $this, '__clone', array(), $this->initializer5acaafb47f498537203639);

        $this->valueHolder5acaafb47f479164142711 = clone $this->valueHolder5acaafb47f479164142711;
    }

    public function __sleep()
    {
        $this->initializer5acaafb47f498537203639 && $this->initializer5acaafb47f498537203639->__invoke($this->valueHolder5acaafb47f479164142711, $this, '__sleep', array(), $this->initializer5acaafb47f498537203639);

        return array('valueHolder5acaafb47f479164142711');
    }

    public function __wakeup()
    {
    }

    /**
     * {@inheritDoc}
     */
    public function setProxyInitializer(\Closure $initializer = null)
    {
        $this->initializer5acaafb47f498537203639 = $initializer;
    }

    /**
     * {@inheritDoc}
     */
    public function getProxyInitializer()
    {
        return $this->initializer5acaafb47f498537203639;
    }

    /**
     * {@inheritDoc}
     */
    public function initializeProxy()
    {
        return $this->initializer5acaafb47f498537203639 && $this->initializer5acaafb47f498537203639->__invoke($this->valueHolder5acaafb47f479164142711, $this, 'initializeProxy', array(), $this->initializer5acaafb47f498537203639);
    }

    /**
     * {@inheritDoc}
     */
    public function isProxyInitialized()
    {
        return null !== $this->valueHolder5acaafb47f479164142711;
    }

    /**
     * {@inheritDoc}
     */
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder5acaafb47f479164142711;
    }


}

class DoctrineORMEntityManager_000000002084059000000000634d02687471c441876336872902bf356897607e extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $valueHolder5acaafb495435560554157 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer5acaafb495450594208540 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties5acaafb4953f4437177620 = array(
        
    );

    /**
     * {@inheritDoc}
     */
    public function getConnection()
    {
        $this->initializer5acaafb495450594208540 && $this->initializer5acaafb495450594208540->__invoke($this->valueHolder5acaafb495435560554157, $this, 'getConnection', array(), $this->initializer5acaafb495450594208540);

        return $this->valueHolder5acaafb495435560554157->getConnection();
    }

    /**
     * {@inheritDoc}
     */
    public function getMetadataFactory()
    {
        $this->initializer5acaafb495450594208540 && $this->initializer5acaafb495450594208540->__invoke($this->valueHolder5acaafb495435560554157, $this, 'getMetadataFactory', array(), $this->initializer5acaafb495450594208540);

        return $this->valueHolder5acaafb495435560554157->getMetadataFactory();
    }

    /**
     * {@inheritDoc}
     */
    public function getExpressionBuilder()
    {
        $this->initializer5acaafb495450594208540 && $this->initializer5acaafb495450594208540->__invoke($this->valueHolder5acaafb495435560554157, $this, 'getExpressionBuilder', array(), $this->initializer5acaafb495450594208540);

        return $this->valueHolder5acaafb495435560554157->getExpressionBuilder();
    }

    /**
     * {@inheritDoc}
     */
    public function beginTransaction()
    {
        $this->initializer5acaafb495450594208540 && $this->initializer5acaafb495450594208540->__invoke($this->valueHolder5acaafb495435560554157, $this, 'beginTransaction', array(), $this->initializer5acaafb495450594208540);

        return $this->valueHolder5acaafb495435560554157->beginTransaction();
    }

    /**
     * {@inheritDoc}
     */
    public function getCache()
    {
        $this->initializer5acaafb495450594208540 && $this->initializer5acaafb495450594208540->__invoke($this->valueHolder5acaafb495435560554157, $this, 'getCache', array(), $this->initializer5acaafb495450594208540);

        return $this->valueHolder5acaafb495435560554157->getCache();
    }

    /**
     * {@inheritDoc}
     */
    public function transactional($func)
    {
        $this->initializer5acaafb495450594208540 && $this->initializer5acaafb495450594208540->__invoke($this->valueHolder5acaafb495435560554157, $this, 'transactional', array('func' => $func), $this->initializer5acaafb495450594208540);

        return $this->valueHolder5acaafb495435560554157->transactional($func);
    }

    /**
     * {@inheritDoc}
     */
    public function commit()
    {
        $this->initializer5acaafb495450594208540 && $this->initializer5acaafb495450594208540->__invoke($this->valueHolder5acaafb495435560554157, $this, 'commit', array(), $this->initializer5acaafb495450594208540);

        return $this->valueHolder5acaafb495435560554157->commit();
    }

    /**
     * {@inheritDoc}
     */
    public function rollback()
    {
        $this->initializer5acaafb495450594208540 && $this->initializer5acaafb495450594208540->__invoke($this->valueHolder5acaafb495435560554157, $this, 'rollback', array(), $this->initializer5acaafb495450594208540);

        return $this->valueHolder5acaafb495435560554157->rollback();
    }

    /**
     * {@inheritDoc}
     */
    public function getClassMetadata($className)
    {
        $this->initializer5acaafb495450594208540 && $this->initializer5acaafb495450594208540->__invoke($this->valueHolder5acaafb495435560554157, $this, 'getClassMetadata', array('className' => $className), $this->initializer5acaafb495450594208540);

        return $this->valueHolder5acaafb495435560554157->getClassMetadata($className);
    }

    /**
     * {@inheritDoc}
     */
    public function createQuery($dql = '')
    {
        $this->initializer5acaafb495450594208540 && $this->initializer5acaafb495450594208540->__invoke($this->valueHolder5acaafb495435560554157, $this, 'createQuery', array('dql' => $dql), $this->initializer5acaafb495450594208540);

        return $this->valueHolder5acaafb495435560554157->createQuery($dql);
    }

    /**
     * {@inheritDoc}
     */
    public function createNamedQuery($name)
    {
        $this->initializer5acaafb495450594208540 && $this->initializer5acaafb495450594208540->__invoke($this->valueHolder5acaafb495435560554157, $this, 'createNamedQuery', array('name' => $name), $this->initializer5acaafb495450594208540);

        return $this->valueHolder5acaafb495435560554157->createNamedQuery($name);
    }

    /**
     * {@inheritDoc}
     */
    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializer5acaafb495450594208540 && $this->initializer5acaafb495450594208540->__invoke($this->valueHolder5acaafb495435560554157, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializer5acaafb495450594208540);

        return $this->valueHolder5acaafb495435560554157->createNativeQuery($sql, $rsm);
    }

    /**
     * {@inheritDoc}
     */
    public function createNamedNativeQuery($name)
    {
        $this->initializer5acaafb495450594208540 && $this->initializer5acaafb495450594208540->__invoke($this->valueHolder5acaafb495435560554157, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializer5acaafb495450594208540);

        return $this->valueHolder5acaafb495435560554157->createNamedNativeQuery($name);
    }

    /**
     * {@inheritDoc}
     */
    public function createQueryBuilder()
    {
        $this->initializer5acaafb495450594208540 && $this->initializer5acaafb495450594208540->__invoke($this->valueHolder5acaafb495435560554157, $this, 'createQueryBuilder', array(), $this->initializer5acaafb495450594208540);

        return $this->valueHolder5acaafb495435560554157->createQueryBuilder();
    }

    /**
     * {@inheritDoc}
     */
    public function flush($entity = null)
    {
        $this->initializer5acaafb495450594208540 && $this->initializer5acaafb495450594208540->__invoke($this->valueHolder5acaafb495435560554157, $this, 'flush', array('entity' => $entity), $this->initializer5acaafb495450594208540);

        return $this->valueHolder5acaafb495435560554157->flush($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function find($entityName, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer5acaafb495450594208540 && $this->initializer5acaafb495450594208540->__invoke($this->valueHolder5acaafb495435560554157, $this, 'find', array('entityName' => $entityName, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer5acaafb495450594208540);

        return $this->valueHolder5acaafb495435560554157->find($entityName, $id, $lockMode, $lockVersion);
    }

    /**
     * {@inheritDoc}
     */
    public function getReference($entityName, $id)
    {
        $this->initializer5acaafb495450594208540 && $this->initializer5acaafb495450594208540->__invoke($this->valueHolder5acaafb495435560554157, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializer5acaafb495450594208540);

        return $this->valueHolder5acaafb495435560554157->getReference($entityName, $id);
    }

    /**
     * {@inheritDoc}
     */
    public function getPartialReference($entityName, $identifier)
    {
        $this->initializer5acaafb495450594208540 && $this->initializer5acaafb495450594208540->__invoke($this->valueHolder5acaafb495435560554157, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializer5acaafb495450594208540);

        return $this->valueHolder5acaafb495435560554157->getPartialReference($entityName, $identifier);
    }

    /**
     * {@inheritDoc}
     */
    public function clear($entityName = null)
    {
        $this->initializer5acaafb495450594208540 && $this->initializer5acaafb495450594208540->__invoke($this->valueHolder5acaafb495435560554157, $this, 'clear', array('entityName' => $entityName), $this->initializer5acaafb495450594208540);

        return $this->valueHolder5acaafb495435560554157->clear($entityName);
    }

    /**
     * {@inheritDoc}
     */
    public function close()
    {
        $this->initializer5acaafb495450594208540 && $this->initializer5acaafb495450594208540->__invoke($this->valueHolder5acaafb495435560554157, $this, 'close', array(), $this->initializer5acaafb495450594208540);

        return $this->valueHolder5acaafb495435560554157->close();
    }

    /**
     * {@inheritDoc}
     */
    public function persist($entity)
    {
        $this->initializer5acaafb495450594208540 && $this->initializer5acaafb495450594208540->__invoke($this->valueHolder5acaafb495435560554157, $this, 'persist', array('entity' => $entity), $this->initializer5acaafb495450594208540);

        return $this->valueHolder5acaafb495435560554157->persist($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function remove($entity)
    {
        $this->initializer5acaafb495450594208540 && $this->initializer5acaafb495450594208540->__invoke($this->valueHolder5acaafb495435560554157, $this, 'remove', array('entity' => $entity), $this->initializer5acaafb495450594208540);

        return $this->valueHolder5acaafb495435560554157->remove($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function refresh($entity)
    {
        $this->initializer5acaafb495450594208540 && $this->initializer5acaafb495450594208540->__invoke($this->valueHolder5acaafb495435560554157, $this, 'refresh', array('entity' => $entity), $this->initializer5acaafb495450594208540);

        return $this->valueHolder5acaafb495435560554157->refresh($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function detach($entity)
    {
        $this->initializer5acaafb495450594208540 && $this->initializer5acaafb495450594208540->__invoke($this->valueHolder5acaafb495435560554157, $this, 'detach', array('entity' => $entity), $this->initializer5acaafb495450594208540);

        return $this->valueHolder5acaafb495435560554157->detach($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function merge($entity)
    {
        $this->initializer5acaafb495450594208540 && $this->initializer5acaafb495450594208540->__invoke($this->valueHolder5acaafb495435560554157, $this, 'merge', array('entity' => $entity), $this->initializer5acaafb495450594208540);

        return $this->valueHolder5acaafb495435560554157->merge($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function copy($entity, $deep = false)
    {
        $this->initializer5acaafb495450594208540 && $this->initializer5acaafb495450594208540->__invoke($this->valueHolder5acaafb495435560554157, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializer5acaafb495450594208540);

        return $this->valueHolder5acaafb495435560554157->copy($entity, $deep);
    }

    /**
     * {@inheritDoc}
     */
    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializer5acaafb495450594208540 && $this->initializer5acaafb495450594208540->__invoke($this->valueHolder5acaafb495435560554157, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer5acaafb495450594208540);

        return $this->valueHolder5acaafb495435560554157->lock($entity, $lockMode, $lockVersion);
    }

    /**
     * {@inheritDoc}
     */
    public function getRepository($entityName)
    {
        $this->initializer5acaafb495450594208540 && $this->initializer5acaafb495450594208540->__invoke($this->valueHolder5acaafb495435560554157, $this, 'getRepository', array('entityName' => $entityName), $this->initializer5acaafb495450594208540);

        return $this->valueHolder5acaafb495435560554157->getRepository($entityName);
    }

    /**
     * {@inheritDoc}
     */
    public function contains($entity)
    {
        $this->initializer5acaafb495450594208540 && $this->initializer5acaafb495450594208540->__invoke($this->valueHolder5acaafb495435560554157, $this, 'contains', array('entity' => $entity), $this->initializer5acaafb495450594208540);

        return $this->valueHolder5acaafb495435560554157->contains($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function getEventManager()
    {
        $this->initializer5acaafb495450594208540 && $this->initializer5acaafb495450594208540->__invoke($this->valueHolder5acaafb495435560554157, $this, 'getEventManager', array(), $this->initializer5acaafb495450594208540);

        return $this->valueHolder5acaafb495435560554157->getEventManager();
    }

    /**
     * {@inheritDoc}
     */
    public function getConfiguration()
    {
        $this->initializer5acaafb495450594208540 && $this->initializer5acaafb495450594208540->__invoke($this->valueHolder5acaafb495435560554157, $this, 'getConfiguration', array(), $this->initializer5acaafb495450594208540);

        return $this->valueHolder5acaafb495435560554157->getConfiguration();
    }

    /**
     * {@inheritDoc}
     */
    public function isOpen()
    {
        $this->initializer5acaafb495450594208540 && $this->initializer5acaafb495450594208540->__invoke($this->valueHolder5acaafb495435560554157, $this, 'isOpen', array(), $this->initializer5acaafb495450594208540);

        return $this->valueHolder5acaafb495435560554157->isOpen();
    }

    /**
     * {@inheritDoc}
     */
    public function getUnitOfWork()
    {
        $this->initializer5acaafb495450594208540 && $this->initializer5acaafb495450594208540->__invoke($this->valueHolder5acaafb495435560554157, $this, 'getUnitOfWork', array(), $this->initializer5acaafb495450594208540);

        return $this->valueHolder5acaafb495435560554157->getUnitOfWork();
    }

    /**
     * {@inheritDoc}
     */
    public function getHydrator($hydrationMode)
    {
        $this->initializer5acaafb495450594208540 && $this->initializer5acaafb495450594208540->__invoke($this->valueHolder5acaafb495435560554157, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializer5acaafb495450594208540);

        return $this->valueHolder5acaafb495435560554157->getHydrator($hydrationMode);
    }

    /**
     * {@inheritDoc}
     */
    public function newHydrator($hydrationMode)
    {
        $this->initializer5acaafb495450594208540 && $this->initializer5acaafb495450594208540->__invoke($this->valueHolder5acaafb495435560554157, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializer5acaafb495450594208540);

        return $this->valueHolder5acaafb495435560554157->newHydrator($hydrationMode);
    }

    /**
     * {@inheritDoc}
     */
    public function getProxyFactory()
    {
        $this->initializer5acaafb495450594208540 && $this->initializer5acaafb495450594208540->__invoke($this->valueHolder5acaafb495435560554157, $this, 'getProxyFactory', array(), $this->initializer5acaafb495450594208540);

        return $this->valueHolder5acaafb495435560554157->getProxyFactory();
    }

    /**
     * {@inheritDoc}
     */
    public function initializeObject($obj)
    {
        $this->initializer5acaafb495450594208540 && $this->initializer5acaafb495450594208540->__invoke($this->valueHolder5acaafb495435560554157, $this, 'initializeObject', array('obj' => $obj), $this->initializer5acaafb495450594208540);

        return $this->valueHolder5acaafb495435560554157->initializeObject($obj);
    }

    /**
     * {@inheritDoc}
     */
    public function getFilters()
    {
        $this->initializer5acaafb495450594208540 && $this->initializer5acaafb495450594208540->__invoke($this->valueHolder5acaafb495435560554157, $this, 'getFilters', array(), $this->initializer5acaafb495450594208540);

        return $this->valueHolder5acaafb495435560554157->getFilters();
    }

    /**
     * {@inheritDoc}
     */
    public function isFiltersStateClean()
    {
        $this->initializer5acaafb495450594208540 && $this->initializer5acaafb495450594208540->__invoke($this->valueHolder5acaafb495435560554157, $this, 'isFiltersStateClean', array(), $this->initializer5acaafb495450594208540);

        return $this->valueHolder5acaafb495435560554157->isFiltersStateClean();
    }

    /**
     * {@inheritDoc}
     */
    public function hasFilters()
    {
        $this->initializer5acaafb495450594208540 && $this->initializer5acaafb495450594208540->__invoke($this->valueHolder5acaafb495435560554157, $this, 'hasFilters', array(), $this->initializer5acaafb495450594208540);

        return $this->valueHolder5acaafb495435560554157->hasFilters();
    }

    /**
     * @override constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public function __construct($initializer)
    {
        $this->initializer5acaafb495450594208540 = $initializer;
    }

    /**
     * @param string $name
     */
    public function & __get($name)
    {
        $this->initializer5acaafb495450594208540 && $this->initializer5acaafb495450594208540->__invoke($this->valueHolder5acaafb495435560554157, $this, '__get', array('name' => $name), $this->initializer5acaafb495450594208540);

        if (isset(self::$publicProperties5acaafb4953f4437177620[$name])) {
            return $this->valueHolder5acaafb495435560554157->$name;
        }

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5acaafb495435560554157;

            $backtrace = debug_backtrace(false);
            trigger_error('Undefined property: ' . get_parent_class($this) . '::$' . $name . ' in ' . $backtrace[0]['file'] . ' on line ' . $backtrace[0]['line'], \E_USER_NOTICE);
            return $targetObject->$name;;
            return;
        }

        $targetObject = $this->valueHolder5acaafb495435560554157;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    /**
     * @param string $name
     * @param mixed $value
     */
    public function __set($name, $value)
    {
        $this->initializer5acaafb495450594208540 && $this->initializer5acaafb495450594208540->__invoke($this->valueHolder5acaafb495435560554157, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer5acaafb495450594208540);

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5acaafb495435560554157;

            return $targetObject->$name = $value;;
            return;
        }

        $targetObject = $this->valueHolder5acaafb495435560554157;
        $accessor = function & () use ($targetObject, $name, $value) {
            return $targetObject->$name = $value;
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    /**
     * @param string $name
     */
    public function __isset($name)
    {
        $this->initializer5acaafb495450594208540 && $this->initializer5acaafb495450594208540->__invoke($this->valueHolder5acaafb495435560554157, $this, '__isset', array('name' => $name), $this->initializer5acaafb495450594208540);

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5acaafb495435560554157;

            return isset($targetObject->$name);;
            return;
        }

        $targetObject = $this->valueHolder5acaafb495435560554157;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    /**
     * @param string $name
     */
    public function __unset($name)
    {
        $this->initializer5acaafb495450594208540 && $this->initializer5acaafb495450594208540->__invoke($this->valueHolder5acaafb495435560554157, $this, '__unset', array('name' => $name), $this->initializer5acaafb495450594208540);

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5acaafb495435560554157;

            unset($targetObject->$name);;
            return;
        }

        $targetObject = $this->valueHolder5acaafb495435560554157;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __clone()
    {
        $this->initializer5acaafb495450594208540 && $this->initializer5acaafb495450594208540->__invoke($this->valueHolder5acaafb495435560554157, $this, '__clone', array(), $this->initializer5acaafb495450594208540);

        $this->valueHolder5acaafb495435560554157 = clone $this->valueHolder5acaafb495435560554157;
    }

    public function __sleep()
    {
        $this->initializer5acaafb495450594208540 && $this->initializer5acaafb495450594208540->__invoke($this->valueHolder5acaafb495435560554157, $this, '__sleep', array(), $this->initializer5acaafb495450594208540);

        return array('valueHolder5acaafb495435560554157');
    }

    public function __wakeup()
    {
    }

    /**
     * {@inheritDoc}
     */
    public function setProxyInitializer(\Closure $initializer = null)
    {
        $this->initializer5acaafb495450594208540 = $initializer;
    }

    /**
     * {@inheritDoc}
     */
    public function getProxyInitializer()
    {
        return $this->initializer5acaafb495450594208540;
    }

    /**
     * {@inheritDoc}
     */
    public function initializeProxy()
    {
        return $this->initializer5acaafb495450594208540 && $this->initializer5acaafb495450594208540->__invoke($this->valueHolder5acaafb495435560554157, $this, 'initializeProxy', array(), $this->initializer5acaafb495450594208540);
    }

    /**
     * {@inheritDoc}
     */
    public function isProxyInitialized()
    {
        return null !== $this->valueHolder5acaafb495435560554157;
    }

    /**
     * {@inheritDoc}
     */
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder5acaafb495435560554157;
    }


}

class DoctrineORMEntityManager_00000000208405aa00000000634d02687471c441876336872902bf356897607e extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $valueHolder5acaafb4abdaa247457062 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer5acaafb4abdc7044369510 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties5acaafb4abd67271711748 = array(
        
    );

    /**
     * {@inheritDoc}
     */
    public function getConnection()
    {
        $this->initializer5acaafb4abdc7044369510 && $this->initializer5acaafb4abdc7044369510->__invoke($this->valueHolder5acaafb4abdaa247457062, $this, 'getConnection', array(), $this->initializer5acaafb4abdc7044369510);

        return $this->valueHolder5acaafb4abdaa247457062->getConnection();
    }

    /**
     * {@inheritDoc}
     */
    public function getMetadataFactory()
    {
        $this->initializer5acaafb4abdc7044369510 && $this->initializer5acaafb4abdc7044369510->__invoke($this->valueHolder5acaafb4abdaa247457062, $this, 'getMetadataFactory', array(), $this->initializer5acaafb4abdc7044369510);

        return $this->valueHolder5acaafb4abdaa247457062->getMetadataFactory();
    }

    /**
     * {@inheritDoc}
     */
    public function getExpressionBuilder()
    {
        $this->initializer5acaafb4abdc7044369510 && $this->initializer5acaafb4abdc7044369510->__invoke($this->valueHolder5acaafb4abdaa247457062, $this, 'getExpressionBuilder', array(), $this->initializer5acaafb4abdc7044369510);

        return $this->valueHolder5acaafb4abdaa247457062->getExpressionBuilder();
    }

    /**
     * {@inheritDoc}
     */
    public function beginTransaction()
    {
        $this->initializer5acaafb4abdc7044369510 && $this->initializer5acaafb4abdc7044369510->__invoke($this->valueHolder5acaafb4abdaa247457062, $this, 'beginTransaction', array(), $this->initializer5acaafb4abdc7044369510);

        return $this->valueHolder5acaafb4abdaa247457062->beginTransaction();
    }

    /**
     * {@inheritDoc}
     */
    public function getCache()
    {
        $this->initializer5acaafb4abdc7044369510 && $this->initializer5acaafb4abdc7044369510->__invoke($this->valueHolder5acaafb4abdaa247457062, $this, 'getCache', array(), $this->initializer5acaafb4abdc7044369510);

        return $this->valueHolder5acaafb4abdaa247457062->getCache();
    }

    /**
     * {@inheritDoc}
     */
    public function transactional($func)
    {
        $this->initializer5acaafb4abdc7044369510 && $this->initializer5acaafb4abdc7044369510->__invoke($this->valueHolder5acaafb4abdaa247457062, $this, 'transactional', array('func' => $func), $this->initializer5acaafb4abdc7044369510);

        return $this->valueHolder5acaafb4abdaa247457062->transactional($func);
    }

    /**
     * {@inheritDoc}
     */
    public function commit()
    {
        $this->initializer5acaafb4abdc7044369510 && $this->initializer5acaafb4abdc7044369510->__invoke($this->valueHolder5acaafb4abdaa247457062, $this, 'commit', array(), $this->initializer5acaafb4abdc7044369510);

        return $this->valueHolder5acaafb4abdaa247457062->commit();
    }

    /**
     * {@inheritDoc}
     */
    public function rollback()
    {
        $this->initializer5acaafb4abdc7044369510 && $this->initializer5acaafb4abdc7044369510->__invoke($this->valueHolder5acaafb4abdaa247457062, $this, 'rollback', array(), $this->initializer5acaafb4abdc7044369510);

        return $this->valueHolder5acaafb4abdaa247457062->rollback();
    }

    /**
     * {@inheritDoc}
     */
    public function getClassMetadata($className)
    {
        $this->initializer5acaafb4abdc7044369510 && $this->initializer5acaafb4abdc7044369510->__invoke($this->valueHolder5acaafb4abdaa247457062, $this, 'getClassMetadata', array('className' => $className), $this->initializer5acaafb4abdc7044369510);

        return $this->valueHolder5acaafb4abdaa247457062->getClassMetadata($className);
    }

    /**
     * {@inheritDoc}
     */
    public function createQuery($dql = '')
    {
        $this->initializer5acaafb4abdc7044369510 && $this->initializer5acaafb4abdc7044369510->__invoke($this->valueHolder5acaafb4abdaa247457062, $this, 'createQuery', array('dql' => $dql), $this->initializer5acaafb4abdc7044369510);

        return $this->valueHolder5acaafb4abdaa247457062->createQuery($dql);
    }

    /**
     * {@inheritDoc}
     */
    public function createNamedQuery($name)
    {
        $this->initializer5acaafb4abdc7044369510 && $this->initializer5acaafb4abdc7044369510->__invoke($this->valueHolder5acaafb4abdaa247457062, $this, 'createNamedQuery', array('name' => $name), $this->initializer5acaafb4abdc7044369510);

        return $this->valueHolder5acaafb4abdaa247457062->createNamedQuery($name);
    }

    /**
     * {@inheritDoc}
     */
    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializer5acaafb4abdc7044369510 && $this->initializer5acaafb4abdc7044369510->__invoke($this->valueHolder5acaafb4abdaa247457062, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializer5acaafb4abdc7044369510);

        return $this->valueHolder5acaafb4abdaa247457062->createNativeQuery($sql, $rsm);
    }

    /**
     * {@inheritDoc}
     */
    public function createNamedNativeQuery($name)
    {
        $this->initializer5acaafb4abdc7044369510 && $this->initializer5acaafb4abdc7044369510->__invoke($this->valueHolder5acaafb4abdaa247457062, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializer5acaafb4abdc7044369510);

        return $this->valueHolder5acaafb4abdaa247457062->createNamedNativeQuery($name);
    }

    /**
     * {@inheritDoc}
     */
    public function createQueryBuilder()
    {
        $this->initializer5acaafb4abdc7044369510 && $this->initializer5acaafb4abdc7044369510->__invoke($this->valueHolder5acaafb4abdaa247457062, $this, 'createQueryBuilder', array(), $this->initializer5acaafb4abdc7044369510);

        return $this->valueHolder5acaafb4abdaa247457062->createQueryBuilder();
    }

    /**
     * {@inheritDoc}
     */
    public function flush($entity = null)
    {
        $this->initializer5acaafb4abdc7044369510 && $this->initializer5acaafb4abdc7044369510->__invoke($this->valueHolder5acaafb4abdaa247457062, $this, 'flush', array('entity' => $entity), $this->initializer5acaafb4abdc7044369510);

        return $this->valueHolder5acaafb4abdaa247457062->flush($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function find($entityName, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer5acaafb4abdc7044369510 && $this->initializer5acaafb4abdc7044369510->__invoke($this->valueHolder5acaafb4abdaa247457062, $this, 'find', array('entityName' => $entityName, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer5acaafb4abdc7044369510);

        return $this->valueHolder5acaafb4abdaa247457062->find($entityName, $id, $lockMode, $lockVersion);
    }

    /**
     * {@inheritDoc}
     */
    public function getReference($entityName, $id)
    {
        $this->initializer5acaafb4abdc7044369510 && $this->initializer5acaafb4abdc7044369510->__invoke($this->valueHolder5acaafb4abdaa247457062, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializer5acaafb4abdc7044369510);

        return $this->valueHolder5acaafb4abdaa247457062->getReference($entityName, $id);
    }

    /**
     * {@inheritDoc}
     */
    public function getPartialReference($entityName, $identifier)
    {
        $this->initializer5acaafb4abdc7044369510 && $this->initializer5acaafb4abdc7044369510->__invoke($this->valueHolder5acaafb4abdaa247457062, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializer5acaafb4abdc7044369510);

        return $this->valueHolder5acaafb4abdaa247457062->getPartialReference($entityName, $identifier);
    }

    /**
     * {@inheritDoc}
     */
    public function clear($entityName = null)
    {
        $this->initializer5acaafb4abdc7044369510 && $this->initializer5acaafb4abdc7044369510->__invoke($this->valueHolder5acaafb4abdaa247457062, $this, 'clear', array('entityName' => $entityName), $this->initializer5acaafb4abdc7044369510);

        return $this->valueHolder5acaafb4abdaa247457062->clear($entityName);
    }

    /**
     * {@inheritDoc}
     */
    public function close()
    {
        $this->initializer5acaafb4abdc7044369510 && $this->initializer5acaafb4abdc7044369510->__invoke($this->valueHolder5acaafb4abdaa247457062, $this, 'close', array(), $this->initializer5acaafb4abdc7044369510);

        return $this->valueHolder5acaafb4abdaa247457062->close();
    }

    /**
     * {@inheritDoc}
     */
    public function persist($entity)
    {
        $this->initializer5acaafb4abdc7044369510 && $this->initializer5acaafb4abdc7044369510->__invoke($this->valueHolder5acaafb4abdaa247457062, $this, 'persist', array('entity' => $entity), $this->initializer5acaafb4abdc7044369510);

        return $this->valueHolder5acaafb4abdaa247457062->persist($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function remove($entity)
    {
        $this->initializer5acaafb4abdc7044369510 && $this->initializer5acaafb4abdc7044369510->__invoke($this->valueHolder5acaafb4abdaa247457062, $this, 'remove', array('entity' => $entity), $this->initializer5acaafb4abdc7044369510);

        return $this->valueHolder5acaafb4abdaa247457062->remove($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function refresh($entity)
    {
        $this->initializer5acaafb4abdc7044369510 && $this->initializer5acaafb4abdc7044369510->__invoke($this->valueHolder5acaafb4abdaa247457062, $this, 'refresh', array('entity' => $entity), $this->initializer5acaafb4abdc7044369510);

        return $this->valueHolder5acaafb4abdaa247457062->refresh($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function detach($entity)
    {
        $this->initializer5acaafb4abdc7044369510 && $this->initializer5acaafb4abdc7044369510->__invoke($this->valueHolder5acaafb4abdaa247457062, $this, 'detach', array('entity' => $entity), $this->initializer5acaafb4abdc7044369510);

        return $this->valueHolder5acaafb4abdaa247457062->detach($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function merge($entity)
    {
        $this->initializer5acaafb4abdc7044369510 && $this->initializer5acaafb4abdc7044369510->__invoke($this->valueHolder5acaafb4abdaa247457062, $this, 'merge', array('entity' => $entity), $this->initializer5acaafb4abdc7044369510);

        return $this->valueHolder5acaafb4abdaa247457062->merge($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function copy($entity, $deep = false)
    {
        $this->initializer5acaafb4abdc7044369510 && $this->initializer5acaafb4abdc7044369510->__invoke($this->valueHolder5acaafb4abdaa247457062, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializer5acaafb4abdc7044369510);

        return $this->valueHolder5acaafb4abdaa247457062->copy($entity, $deep);
    }

    /**
     * {@inheritDoc}
     */
    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializer5acaafb4abdc7044369510 && $this->initializer5acaafb4abdc7044369510->__invoke($this->valueHolder5acaafb4abdaa247457062, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer5acaafb4abdc7044369510);

        return $this->valueHolder5acaafb4abdaa247457062->lock($entity, $lockMode, $lockVersion);
    }

    /**
     * {@inheritDoc}
     */
    public function getRepository($entityName)
    {
        $this->initializer5acaafb4abdc7044369510 && $this->initializer5acaafb4abdc7044369510->__invoke($this->valueHolder5acaafb4abdaa247457062, $this, 'getRepository', array('entityName' => $entityName), $this->initializer5acaafb4abdc7044369510);

        return $this->valueHolder5acaafb4abdaa247457062->getRepository($entityName);
    }

    /**
     * {@inheritDoc}
     */
    public function contains($entity)
    {
        $this->initializer5acaafb4abdc7044369510 && $this->initializer5acaafb4abdc7044369510->__invoke($this->valueHolder5acaafb4abdaa247457062, $this, 'contains', array('entity' => $entity), $this->initializer5acaafb4abdc7044369510);

        return $this->valueHolder5acaafb4abdaa247457062->contains($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function getEventManager()
    {
        $this->initializer5acaafb4abdc7044369510 && $this->initializer5acaafb4abdc7044369510->__invoke($this->valueHolder5acaafb4abdaa247457062, $this, 'getEventManager', array(), $this->initializer5acaafb4abdc7044369510);

        return $this->valueHolder5acaafb4abdaa247457062->getEventManager();
    }

    /**
     * {@inheritDoc}
     */
    public function getConfiguration()
    {
        $this->initializer5acaafb4abdc7044369510 && $this->initializer5acaafb4abdc7044369510->__invoke($this->valueHolder5acaafb4abdaa247457062, $this, 'getConfiguration', array(), $this->initializer5acaafb4abdc7044369510);

        return $this->valueHolder5acaafb4abdaa247457062->getConfiguration();
    }

    /**
     * {@inheritDoc}
     */
    public function isOpen()
    {
        $this->initializer5acaafb4abdc7044369510 && $this->initializer5acaafb4abdc7044369510->__invoke($this->valueHolder5acaafb4abdaa247457062, $this, 'isOpen', array(), $this->initializer5acaafb4abdc7044369510);

        return $this->valueHolder5acaafb4abdaa247457062->isOpen();
    }

    /**
     * {@inheritDoc}
     */
    public function getUnitOfWork()
    {
        $this->initializer5acaafb4abdc7044369510 && $this->initializer5acaafb4abdc7044369510->__invoke($this->valueHolder5acaafb4abdaa247457062, $this, 'getUnitOfWork', array(), $this->initializer5acaafb4abdc7044369510);

        return $this->valueHolder5acaafb4abdaa247457062->getUnitOfWork();
    }

    /**
     * {@inheritDoc}
     */
    public function getHydrator($hydrationMode)
    {
        $this->initializer5acaafb4abdc7044369510 && $this->initializer5acaafb4abdc7044369510->__invoke($this->valueHolder5acaafb4abdaa247457062, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializer5acaafb4abdc7044369510);

        return $this->valueHolder5acaafb4abdaa247457062->getHydrator($hydrationMode);
    }

    /**
     * {@inheritDoc}
     */
    public function newHydrator($hydrationMode)
    {
        $this->initializer5acaafb4abdc7044369510 && $this->initializer5acaafb4abdc7044369510->__invoke($this->valueHolder5acaafb4abdaa247457062, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializer5acaafb4abdc7044369510);

        return $this->valueHolder5acaafb4abdaa247457062->newHydrator($hydrationMode);
    }

    /**
     * {@inheritDoc}
     */
    public function getProxyFactory()
    {
        $this->initializer5acaafb4abdc7044369510 && $this->initializer5acaafb4abdc7044369510->__invoke($this->valueHolder5acaafb4abdaa247457062, $this, 'getProxyFactory', array(), $this->initializer5acaafb4abdc7044369510);

        return $this->valueHolder5acaafb4abdaa247457062->getProxyFactory();
    }

    /**
     * {@inheritDoc}
     */
    public function initializeObject($obj)
    {
        $this->initializer5acaafb4abdc7044369510 && $this->initializer5acaafb4abdc7044369510->__invoke($this->valueHolder5acaafb4abdaa247457062, $this, 'initializeObject', array('obj' => $obj), $this->initializer5acaafb4abdc7044369510);

        return $this->valueHolder5acaafb4abdaa247457062->initializeObject($obj);
    }

    /**
     * {@inheritDoc}
     */
    public function getFilters()
    {
        $this->initializer5acaafb4abdc7044369510 && $this->initializer5acaafb4abdc7044369510->__invoke($this->valueHolder5acaafb4abdaa247457062, $this, 'getFilters', array(), $this->initializer5acaafb4abdc7044369510);

        return $this->valueHolder5acaafb4abdaa247457062->getFilters();
    }

    /**
     * {@inheritDoc}
     */
    public function isFiltersStateClean()
    {
        $this->initializer5acaafb4abdc7044369510 && $this->initializer5acaafb4abdc7044369510->__invoke($this->valueHolder5acaafb4abdaa247457062, $this, 'isFiltersStateClean', array(), $this->initializer5acaafb4abdc7044369510);

        return $this->valueHolder5acaafb4abdaa247457062->isFiltersStateClean();
    }

    /**
     * {@inheritDoc}
     */
    public function hasFilters()
    {
        $this->initializer5acaafb4abdc7044369510 && $this->initializer5acaafb4abdc7044369510->__invoke($this->valueHolder5acaafb4abdaa247457062, $this, 'hasFilters', array(), $this->initializer5acaafb4abdc7044369510);

        return $this->valueHolder5acaafb4abdaa247457062->hasFilters();
    }

    /**
     * @override constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public function __construct($initializer)
    {
        $this->initializer5acaafb4abdc7044369510 = $initializer;
    }

    /**
     * @param string $name
     */
    public function & __get($name)
    {
        $this->initializer5acaafb4abdc7044369510 && $this->initializer5acaafb4abdc7044369510->__invoke($this->valueHolder5acaafb4abdaa247457062, $this, '__get', array('name' => $name), $this->initializer5acaafb4abdc7044369510);

        if (isset(self::$publicProperties5acaafb4abd67271711748[$name])) {
            return $this->valueHolder5acaafb4abdaa247457062->$name;
        }

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5acaafb4abdaa247457062;

            $backtrace = debug_backtrace(false);
            trigger_error('Undefined property: ' . get_parent_class($this) . '::$' . $name . ' in ' . $backtrace[0]['file'] . ' on line ' . $backtrace[0]['line'], \E_USER_NOTICE);
            return $targetObject->$name;;
            return;
        }

        $targetObject = $this->valueHolder5acaafb4abdaa247457062;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    /**
     * @param string $name
     * @param mixed $value
     */
    public function __set($name, $value)
    {
        $this->initializer5acaafb4abdc7044369510 && $this->initializer5acaafb4abdc7044369510->__invoke($this->valueHolder5acaafb4abdaa247457062, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer5acaafb4abdc7044369510);

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5acaafb4abdaa247457062;

            return $targetObject->$name = $value;;
            return;
        }

        $targetObject = $this->valueHolder5acaafb4abdaa247457062;
        $accessor = function & () use ($targetObject, $name, $value) {
            return $targetObject->$name = $value;
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    /**
     * @param string $name
     */
    public function __isset($name)
    {
        $this->initializer5acaafb4abdc7044369510 && $this->initializer5acaafb4abdc7044369510->__invoke($this->valueHolder5acaafb4abdaa247457062, $this, '__isset', array('name' => $name), $this->initializer5acaafb4abdc7044369510);

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5acaafb4abdaa247457062;

            return isset($targetObject->$name);;
            return;
        }

        $targetObject = $this->valueHolder5acaafb4abdaa247457062;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    /**
     * @param string $name
     */
    public function __unset($name)
    {
        $this->initializer5acaafb4abdc7044369510 && $this->initializer5acaafb4abdc7044369510->__invoke($this->valueHolder5acaafb4abdaa247457062, $this, '__unset', array('name' => $name), $this->initializer5acaafb4abdc7044369510);

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5acaafb4abdaa247457062;

            unset($targetObject->$name);;
            return;
        }

        $targetObject = $this->valueHolder5acaafb4abdaa247457062;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __clone()
    {
        $this->initializer5acaafb4abdc7044369510 && $this->initializer5acaafb4abdc7044369510->__invoke($this->valueHolder5acaafb4abdaa247457062, $this, '__clone', array(), $this->initializer5acaafb4abdc7044369510);

        $this->valueHolder5acaafb4abdaa247457062 = clone $this->valueHolder5acaafb4abdaa247457062;
    }

    public function __sleep()
    {
        $this->initializer5acaafb4abdc7044369510 && $this->initializer5acaafb4abdc7044369510->__invoke($this->valueHolder5acaafb4abdaa247457062, $this, '__sleep', array(), $this->initializer5acaafb4abdc7044369510);

        return array('valueHolder5acaafb4abdaa247457062');
    }

    public function __wakeup()
    {
    }

    /**
     * {@inheritDoc}
     */
    public function setProxyInitializer(\Closure $initializer = null)
    {
        $this->initializer5acaafb4abdc7044369510 = $initializer;
    }

    /**
     * {@inheritDoc}
     */
    public function getProxyInitializer()
    {
        return $this->initializer5acaafb4abdc7044369510;
    }

    /**
     * {@inheritDoc}
     */
    public function initializeProxy()
    {
        return $this->initializer5acaafb4abdc7044369510 && $this->initializer5acaafb4abdc7044369510->__invoke($this->valueHolder5acaafb4abdaa247457062, $this, 'initializeProxy', array(), $this->initializer5acaafb4abdc7044369510);
    }

    /**
     * {@inheritDoc}
     */
    public function isProxyInitialized()
    {
        return null !== $this->valueHolder5acaafb4abdaa247457062;
    }

    /**
     * {@inheritDoc}
     */
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder5acaafb4abdaa247457062;
    }


}

class DoctrineORMEntityManager_00000000208405ac00000000634d02687471c441876336872902bf356897607e extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $valueHolder5acaafb4c57bc361927955 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer5acaafb4c57db576188216 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties5acaafb4c5773426853229 = array(
        
    );

    /**
     * {@inheritDoc}
     */
    public function getConnection()
    {
        $this->initializer5acaafb4c57db576188216 && $this->initializer5acaafb4c57db576188216->__invoke($this->valueHolder5acaafb4c57bc361927955, $this, 'getConnection', array(), $this->initializer5acaafb4c57db576188216);

        return $this->valueHolder5acaafb4c57bc361927955->getConnection();
    }

    /**
     * {@inheritDoc}
     */
    public function getMetadataFactory()
    {
        $this->initializer5acaafb4c57db576188216 && $this->initializer5acaafb4c57db576188216->__invoke($this->valueHolder5acaafb4c57bc361927955, $this, 'getMetadataFactory', array(), $this->initializer5acaafb4c57db576188216);

        return $this->valueHolder5acaafb4c57bc361927955->getMetadataFactory();
    }

    /**
     * {@inheritDoc}
     */
    public function getExpressionBuilder()
    {
        $this->initializer5acaafb4c57db576188216 && $this->initializer5acaafb4c57db576188216->__invoke($this->valueHolder5acaafb4c57bc361927955, $this, 'getExpressionBuilder', array(), $this->initializer5acaafb4c57db576188216);

        return $this->valueHolder5acaafb4c57bc361927955->getExpressionBuilder();
    }

    /**
     * {@inheritDoc}
     */
    public function beginTransaction()
    {
        $this->initializer5acaafb4c57db576188216 && $this->initializer5acaafb4c57db576188216->__invoke($this->valueHolder5acaafb4c57bc361927955, $this, 'beginTransaction', array(), $this->initializer5acaafb4c57db576188216);

        return $this->valueHolder5acaafb4c57bc361927955->beginTransaction();
    }

    /**
     * {@inheritDoc}
     */
    public function getCache()
    {
        $this->initializer5acaafb4c57db576188216 && $this->initializer5acaafb4c57db576188216->__invoke($this->valueHolder5acaafb4c57bc361927955, $this, 'getCache', array(), $this->initializer5acaafb4c57db576188216);

        return $this->valueHolder5acaafb4c57bc361927955->getCache();
    }

    /**
     * {@inheritDoc}
     */
    public function transactional($func)
    {
        $this->initializer5acaafb4c57db576188216 && $this->initializer5acaafb4c57db576188216->__invoke($this->valueHolder5acaafb4c57bc361927955, $this, 'transactional', array('func' => $func), $this->initializer5acaafb4c57db576188216);

        return $this->valueHolder5acaafb4c57bc361927955->transactional($func);
    }

    /**
     * {@inheritDoc}
     */
    public function commit()
    {
        $this->initializer5acaafb4c57db576188216 && $this->initializer5acaafb4c57db576188216->__invoke($this->valueHolder5acaafb4c57bc361927955, $this, 'commit', array(), $this->initializer5acaafb4c57db576188216);

        return $this->valueHolder5acaafb4c57bc361927955->commit();
    }

    /**
     * {@inheritDoc}
     */
    public function rollback()
    {
        $this->initializer5acaafb4c57db576188216 && $this->initializer5acaafb4c57db576188216->__invoke($this->valueHolder5acaafb4c57bc361927955, $this, 'rollback', array(), $this->initializer5acaafb4c57db576188216);

        return $this->valueHolder5acaafb4c57bc361927955->rollback();
    }

    /**
     * {@inheritDoc}
     */
    public function getClassMetadata($className)
    {
        $this->initializer5acaafb4c57db576188216 && $this->initializer5acaafb4c57db576188216->__invoke($this->valueHolder5acaafb4c57bc361927955, $this, 'getClassMetadata', array('className' => $className), $this->initializer5acaafb4c57db576188216);

        return $this->valueHolder5acaafb4c57bc361927955->getClassMetadata($className);
    }

    /**
     * {@inheritDoc}
     */
    public function createQuery($dql = '')
    {
        $this->initializer5acaafb4c57db576188216 && $this->initializer5acaafb4c57db576188216->__invoke($this->valueHolder5acaafb4c57bc361927955, $this, 'createQuery', array('dql' => $dql), $this->initializer5acaafb4c57db576188216);

        return $this->valueHolder5acaafb4c57bc361927955->createQuery($dql);
    }

    /**
     * {@inheritDoc}
     */
    public function createNamedQuery($name)
    {
        $this->initializer5acaafb4c57db576188216 && $this->initializer5acaafb4c57db576188216->__invoke($this->valueHolder5acaafb4c57bc361927955, $this, 'createNamedQuery', array('name' => $name), $this->initializer5acaafb4c57db576188216);

        return $this->valueHolder5acaafb4c57bc361927955->createNamedQuery($name);
    }

    /**
     * {@inheritDoc}
     */
    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializer5acaafb4c57db576188216 && $this->initializer5acaafb4c57db576188216->__invoke($this->valueHolder5acaafb4c57bc361927955, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializer5acaafb4c57db576188216);

        return $this->valueHolder5acaafb4c57bc361927955->createNativeQuery($sql, $rsm);
    }

    /**
     * {@inheritDoc}
     */
    public function createNamedNativeQuery($name)
    {
        $this->initializer5acaafb4c57db576188216 && $this->initializer5acaafb4c57db576188216->__invoke($this->valueHolder5acaafb4c57bc361927955, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializer5acaafb4c57db576188216);

        return $this->valueHolder5acaafb4c57bc361927955->createNamedNativeQuery($name);
    }

    /**
     * {@inheritDoc}
     */
    public function createQueryBuilder()
    {
        $this->initializer5acaafb4c57db576188216 && $this->initializer5acaafb4c57db576188216->__invoke($this->valueHolder5acaafb4c57bc361927955, $this, 'createQueryBuilder', array(), $this->initializer5acaafb4c57db576188216);

        return $this->valueHolder5acaafb4c57bc361927955->createQueryBuilder();
    }

    /**
     * {@inheritDoc}
     */
    public function flush($entity = null)
    {
        $this->initializer5acaafb4c57db576188216 && $this->initializer5acaafb4c57db576188216->__invoke($this->valueHolder5acaafb4c57bc361927955, $this, 'flush', array('entity' => $entity), $this->initializer5acaafb4c57db576188216);

        return $this->valueHolder5acaafb4c57bc361927955->flush($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function find($entityName, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer5acaafb4c57db576188216 && $this->initializer5acaafb4c57db576188216->__invoke($this->valueHolder5acaafb4c57bc361927955, $this, 'find', array('entityName' => $entityName, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer5acaafb4c57db576188216);

        return $this->valueHolder5acaafb4c57bc361927955->find($entityName, $id, $lockMode, $lockVersion);
    }

    /**
     * {@inheritDoc}
     */
    public function getReference($entityName, $id)
    {
        $this->initializer5acaafb4c57db576188216 && $this->initializer5acaafb4c57db576188216->__invoke($this->valueHolder5acaafb4c57bc361927955, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializer5acaafb4c57db576188216);

        return $this->valueHolder5acaafb4c57bc361927955->getReference($entityName, $id);
    }

    /**
     * {@inheritDoc}
     */
    public function getPartialReference($entityName, $identifier)
    {
        $this->initializer5acaafb4c57db576188216 && $this->initializer5acaafb4c57db576188216->__invoke($this->valueHolder5acaafb4c57bc361927955, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializer5acaafb4c57db576188216);

        return $this->valueHolder5acaafb4c57bc361927955->getPartialReference($entityName, $identifier);
    }

    /**
     * {@inheritDoc}
     */
    public function clear($entityName = null)
    {
        $this->initializer5acaafb4c57db576188216 && $this->initializer5acaafb4c57db576188216->__invoke($this->valueHolder5acaafb4c57bc361927955, $this, 'clear', array('entityName' => $entityName), $this->initializer5acaafb4c57db576188216);

        return $this->valueHolder5acaafb4c57bc361927955->clear($entityName);
    }

    /**
     * {@inheritDoc}
     */
    public function close()
    {
        $this->initializer5acaafb4c57db576188216 && $this->initializer5acaafb4c57db576188216->__invoke($this->valueHolder5acaafb4c57bc361927955, $this, 'close', array(), $this->initializer5acaafb4c57db576188216);

        return $this->valueHolder5acaafb4c57bc361927955->close();
    }

    /**
     * {@inheritDoc}
     */
    public function persist($entity)
    {
        $this->initializer5acaafb4c57db576188216 && $this->initializer5acaafb4c57db576188216->__invoke($this->valueHolder5acaafb4c57bc361927955, $this, 'persist', array('entity' => $entity), $this->initializer5acaafb4c57db576188216);

        return $this->valueHolder5acaafb4c57bc361927955->persist($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function remove($entity)
    {
        $this->initializer5acaafb4c57db576188216 && $this->initializer5acaafb4c57db576188216->__invoke($this->valueHolder5acaafb4c57bc361927955, $this, 'remove', array('entity' => $entity), $this->initializer5acaafb4c57db576188216);

        return $this->valueHolder5acaafb4c57bc361927955->remove($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function refresh($entity)
    {
        $this->initializer5acaafb4c57db576188216 && $this->initializer5acaafb4c57db576188216->__invoke($this->valueHolder5acaafb4c57bc361927955, $this, 'refresh', array('entity' => $entity), $this->initializer5acaafb4c57db576188216);

        return $this->valueHolder5acaafb4c57bc361927955->refresh($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function detach($entity)
    {
        $this->initializer5acaafb4c57db576188216 && $this->initializer5acaafb4c57db576188216->__invoke($this->valueHolder5acaafb4c57bc361927955, $this, 'detach', array('entity' => $entity), $this->initializer5acaafb4c57db576188216);

        return $this->valueHolder5acaafb4c57bc361927955->detach($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function merge($entity)
    {
        $this->initializer5acaafb4c57db576188216 && $this->initializer5acaafb4c57db576188216->__invoke($this->valueHolder5acaafb4c57bc361927955, $this, 'merge', array('entity' => $entity), $this->initializer5acaafb4c57db576188216);

        return $this->valueHolder5acaafb4c57bc361927955->merge($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function copy($entity, $deep = false)
    {
        $this->initializer5acaafb4c57db576188216 && $this->initializer5acaafb4c57db576188216->__invoke($this->valueHolder5acaafb4c57bc361927955, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializer5acaafb4c57db576188216);

        return $this->valueHolder5acaafb4c57bc361927955->copy($entity, $deep);
    }

    /**
     * {@inheritDoc}
     */
    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializer5acaafb4c57db576188216 && $this->initializer5acaafb4c57db576188216->__invoke($this->valueHolder5acaafb4c57bc361927955, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer5acaafb4c57db576188216);

        return $this->valueHolder5acaafb4c57bc361927955->lock($entity, $lockMode, $lockVersion);
    }

    /**
     * {@inheritDoc}
     */
    public function getRepository($entityName)
    {
        $this->initializer5acaafb4c57db576188216 && $this->initializer5acaafb4c57db576188216->__invoke($this->valueHolder5acaafb4c57bc361927955, $this, 'getRepository', array('entityName' => $entityName), $this->initializer5acaafb4c57db576188216);

        return $this->valueHolder5acaafb4c57bc361927955->getRepository($entityName);
    }

    /**
     * {@inheritDoc}
     */
    public function contains($entity)
    {
        $this->initializer5acaafb4c57db576188216 && $this->initializer5acaafb4c57db576188216->__invoke($this->valueHolder5acaafb4c57bc361927955, $this, 'contains', array('entity' => $entity), $this->initializer5acaafb4c57db576188216);

        return $this->valueHolder5acaafb4c57bc361927955->contains($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function getEventManager()
    {
        $this->initializer5acaafb4c57db576188216 && $this->initializer5acaafb4c57db576188216->__invoke($this->valueHolder5acaafb4c57bc361927955, $this, 'getEventManager', array(), $this->initializer5acaafb4c57db576188216);

        return $this->valueHolder5acaafb4c57bc361927955->getEventManager();
    }

    /**
     * {@inheritDoc}
     */
    public function getConfiguration()
    {
        $this->initializer5acaafb4c57db576188216 && $this->initializer5acaafb4c57db576188216->__invoke($this->valueHolder5acaafb4c57bc361927955, $this, 'getConfiguration', array(), $this->initializer5acaafb4c57db576188216);

        return $this->valueHolder5acaafb4c57bc361927955->getConfiguration();
    }

    /**
     * {@inheritDoc}
     */
    public function isOpen()
    {
        $this->initializer5acaafb4c57db576188216 && $this->initializer5acaafb4c57db576188216->__invoke($this->valueHolder5acaafb4c57bc361927955, $this, 'isOpen', array(), $this->initializer5acaafb4c57db576188216);

        return $this->valueHolder5acaafb4c57bc361927955->isOpen();
    }

    /**
     * {@inheritDoc}
     */
    public function getUnitOfWork()
    {
        $this->initializer5acaafb4c57db576188216 && $this->initializer5acaafb4c57db576188216->__invoke($this->valueHolder5acaafb4c57bc361927955, $this, 'getUnitOfWork', array(), $this->initializer5acaafb4c57db576188216);

        return $this->valueHolder5acaafb4c57bc361927955->getUnitOfWork();
    }

    /**
     * {@inheritDoc}
     */
    public function getHydrator($hydrationMode)
    {
        $this->initializer5acaafb4c57db576188216 && $this->initializer5acaafb4c57db576188216->__invoke($this->valueHolder5acaafb4c57bc361927955, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializer5acaafb4c57db576188216);

        return $this->valueHolder5acaafb4c57bc361927955->getHydrator($hydrationMode);
    }

    /**
     * {@inheritDoc}
     */
    public function newHydrator($hydrationMode)
    {
        $this->initializer5acaafb4c57db576188216 && $this->initializer5acaafb4c57db576188216->__invoke($this->valueHolder5acaafb4c57bc361927955, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializer5acaafb4c57db576188216);

        return $this->valueHolder5acaafb4c57bc361927955->newHydrator($hydrationMode);
    }

    /**
     * {@inheritDoc}
     */
    public function getProxyFactory()
    {
        $this->initializer5acaafb4c57db576188216 && $this->initializer5acaafb4c57db576188216->__invoke($this->valueHolder5acaafb4c57bc361927955, $this, 'getProxyFactory', array(), $this->initializer5acaafb4c57db576188216);

        return $this->valueHolder5acaafb4c57bc361927955->getProxyFactory();
    }

    /**
     * {@inheritDoc}
     */
    public function initializeObject($obj)
    {
        $this->initializer5acaafb4c57db576188216 && $this->initializer5acaafb4c57db576188216->__invoke($this->valueHolder5acaafb4c57bc361927955, $this, 'initializeObject', array('obj' => $obj), $this->initializer5acaafb4c57db576188216);

        return $this->valueHolder5acaafb4c57bc361927955->initializeObject($obj);
    }

    /**
     * {@inheritDoc}
     */
    public function getFilters()
    {
        $this->initializer5acaafb4c57db576188216 && $this->initializer5acaafb4c57db576188216->__invoke($this->valueHolder5acaafb4c57bc361927955, $this, 'getFilters', array(), $this->initializer5acaafb4c57db576188216);

        return $this->valueHolder5acaafb4c57bc361927955->getFilters();
    }

    /**
     * {@inheritDoc}
     */
    public function isFiltersStateClean()
    {
        $this->initializer5acaafb4c57db576188216 && $this->initializer5acaafb4c57db576188216->__invoke($this->valueHolder5acaafb4c57bc361927955, $this, 'isFiltersStateClean', array(), $this->initializer5acaafb4c57db576188216);

        return $this->valueHolder5acaafb4c57bc361927955->isFiltersStateClean();
    }

    /**
     * {@inheritDoc}
     */
    public function hasFilters()
    {
        $this->initializer5acaafb4c57db576188216 && $this->initializer5acaafb4c57db576188216->__invoke($this->valueHolder5acaafb4c57bc361927955, $this, 'hasFilters', array(), $this->initializer5acaafb4c57db576188216);

        return $this->valueHolder5acaafb4c57bc361927955->hasFilters();
    }

    /**
     * @override constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public function __construct($initializer)
    {
        $this->initializer5acaafb4c57db576188216 = $initializer;
    }

    /**
     * @param string $name
     */
    public function & __get($name)
    {
        $this->initializer5acaafb4c57db576188216 && $this->initializer5acaafb4c57db576188216->__invoke($this->valueHolder5acaafb4c57bc361927955, $this, '__get', array('name' => $name), $this->initializer5acaafb4c57db576188216);

        if (isset(self::$publicProperties5acaafb4c5773426853229[$name])) {
            return $this->valueHolder5acaafb4c57bc361927955->$name;
        }

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5acaafb4c57bc361927955;

            $backtrace = debug_backtrace(false);
            trigger_error('Undefined property: ' . get_parent_class($this) . '::$' . $name . ' in ' . $backtrace[0]['file'] . ' on line ' . $backtrace[0]['line'], \E_USER_NOTICE);
            return $targetObject->$name;;
            return;
        }

        $targetObject = $this->valueHolder5acaafb4c57bc361927955;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    /**
     * @param string $name
     * @param mixed $value
     */
    public function __set($name, $value)
    {
        $this->initializer5acaafb4c57db576188216 && $this->initializer5acaafb4c57db576188216->__invoke($this->valueHolder5acaafb4c57bc361927955, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer5acaafb4c57db576188216);

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5acaafb4c57bc361927955;

            return $targetObject->$name = $value;;
            return;
        }

        $targetObject = $this->valueHolder5acaafb4c57bc361927955;
        $accessor = function & () use ($targetObject, $name, $value) {
            return $targetObject->$name = $value;
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    /**
     * @param string $name
     */
    public function __isset($name)
    {
        $this->initializer5acaafb4c57db576188216 && $this->initializer5acaafb4c57db576188216->__invoke($this->valueHolder5acaafb4c57bc361927955, $this, '__isset', array('name' => $name), $this->initializer5acaafb4c57db576188216);

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5acaafb4c57bc361927955;

            return isset($targetObject->$name);;
            return;
        }

        $targetObject = $this->valueHolder5acaafb4c57bc361927955;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    /**
     * @param string $name
     */
    public function __unset($name)
    {
        $this->initializer5acaafb4c57db576188216 && $this->initializer5acaafb4c57db576188216->__invoke($this->valueHolder5acaafb4c57bc361927955, $this, '__unset', array('name' => $name), $this->initializer5acaafb4c57db576188216);

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5acaafb4c57bc361927955;

            unset($targetObject->$name);;
            return;
        }

        $targetObject = $this->valueHolder5acaafb4c57bc361927955;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __clone()
    {
        $this->initializer5acaafb4c57db576188216 && $this->initializer5acaafb4c57db576188216->__invoke($this->valueHolder5acaafb4c57bc361927955, $this, '__clone', array(), $this->initializer5acaafb4c57db576188216);

        $this->valueHolder5acaafb4c57bc361927955 = clone $this->valueHolder5acaafb4c57bc361927955;
    }

    public function __sleep()
    {
        $this->initializer5acaafb4c57db576188216 && $this->initializer5acaafb4c57db576188216->__invoke($this->valueHolder5acaafb4c57bc361927955, $this, '__sleep', array(), $this->initializer5acaafb4c57db576188216);

        return array('valueHolder5acaafb4c57bc361927955');
    }

    public function __wakeup()
    {
    }

    /**
     * {@inheritDoc}
     */
    public function setProxyInitializer(\Closure $initializer = null)
    {
        $this->initializer5acaafb4c57db576188216 = $initializer;
    }

    /**
     * {@inheritDoc}
     */
    public function getProxyInitializer()
    {
        return $this->initializer5acaafb4c57db576188216;
    }

    /**
     * {@inheritDoc}
     */
    public function initializeProxy()
    {
        return $this->initializer5acaafb4c57db576188216 && $this->initializer5acaafb4c57db576188216->__invoke($this->valueHolder5acaafb4c57bc361927955, $this, 'initializeProxy', array(), $this->initializer5acaafb4c57db576188216);
    }

    /**
     * {@inheritDoc}
     */
    public function isProxyInitialized()
    {
        return null !== $this->valueHolder5acaafb4c57bc361927955;
    }

    /**
     * {@inheritDoc}
     */
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder5acaafb4c57bc361927955;
    }


}

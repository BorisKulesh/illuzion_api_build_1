<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appProdProjectContainerUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $trimmedPathinfo = rtrim($pathinfo, '/');
        $context = $this->context;
        $request = $this->request;
        $requestMethod = $canonicalMethod = $context->getMethod();
        $scheme = $context->getScheme();

        if ('HEAD' === $requestMethod) {
            $canonicalMethod = 'GET';
        }


        if (0 === strpos($pathinfo, '/v4')) {
            if (0 === strpos($pathinfo, '/v4/c')) {
                if (0 === strpos($pathinfo, '/v4/cinemas')) {
                    // api.v10.cinema.list
                    if ('/v4/cinemas' === $pathinfo) {
                        if ('GET' !== $canonicalMethod) {
                            $allow[] = 'GET';
                            goto not_apiv10cinemalist;
                        }

                        return array (  '_controller' => 'api.controller.cinema:listAction',  '_route' => 'api.v10.cinema.list',);
                    }
                    not_apiv10cinemalist:

                    // api.v10.cinema.get
                    if (preg_match('#^/v4/cinemas/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if ('GET' !== $canonicalMethod) {
                            $allow[] = 'GET';
                            goto not_apiv10cinemaget;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'api.v10.cinema.get')), array (  '_controller' => 'api.controller.cinema:getAction',));
                    }
                    not_apiv10cinemaget:

                }

                elseif (0 === strpos($pathinfo, '/v4/cities')) {
                    // api.v10.city.list
                    if ('/v4/cities' === $pathinfo) {
                        if ('GET' !== $canonicalMethod) {
                            $allow[] = 'GET';
                            goto not_apiv10citylist;
                        }

                        return array (  '_controller' => 'api.controller.city:listAction',  '_route' => 'api.v10.city.list',);
                    }
                    not_apiv10citylist:

                    // api.v10.city.get
                    if (preg_match('#^/v4/cities/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if ('GET' !== $canonicalMethod) {
                            $allow[] = 'GET';
                            goto not_apiv10cityget;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'api.v10.city.get')), array (  '_controller' => 'api.controller.city:getAction',));
                    }
                    not_apiv10cityget:

                }

                elseif (0 === strpos($pathinfo, '/v4/customers')) {
                    // api.v10.customer.list
                    if ('/v4/customers' === $pathinfo) {
                        if ('GET' !== $canonicalMethod) {
                            $allow[] = 'GET';
                            goto not_apiv10customerlist;
                        }

                        return array (  '_controller' => 'api.controller.customer:listAction',  '_route' => 'api.v10.customer.list',);
                    }
                    not_apiv10customerlist:

                    // api.v10.customer.get
                    if (preg_match('#^/v4/customers/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if ('GET' !== $canonicalMethod) {
                            $allow[] = 'GET';
                            goto not_apiv10customerget;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'api.v10.customer.get')), array (  '_controller' => 'api.controller.customer:getAction',));
                    }
                    not_apiv10customerget:

                    // api.v10.customer.update
                    if (preg_match('#^/v4/customers/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if ('PATCH' !== $canonicalMethod) {
                            $allow[] = 'PATCH';
                            goto not_apiv10customerupdate;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'api.v10.customer.update')), array (  '_controller' => 'api.controller.customer:updateAction',));
                    }
                    not_apiv10customerupdate:

                    // api.v10.customer.login
                    if ('/v4/customers/login' === $pathinfo) {
                        if ('POST' !== $canonicalMethod) {
                            $allow[] = 'POST';
                            goto not_apiv10customerlogin;
                        }

                        return array (  '_controller' => 'api.controller.customer:loginAction',  '_route' => 'api.v10.customer.login',);
                    }
                    not_apiv10customerlogin:

                    // api.v10.customer.reset-password
                    if ('/v4/customers/reset-password' === $pathinfo) {
                        if ('POST' !== $canonicalMethod) {
                            $allow[] = 'POST';
                            goto not_apiv10customerresetpassword;
                        }

                        return array (  '_controller' => 'api.controller.customer:resetPasswordAction',  '_route' => 'api.v10.customer.reset-password',);
                    }
                    not_apiv10customerresetpassword:

                    // api.v10.customer.change-password
                    if ('/v4/customers/changePassword' === $pathinfo) {
                        if ('POST' !== $canonicalMethod) {
                            $allow[] = 'POST';
                            goto not_apiv10customerchangepassword;
                        }

                        return array (  '_controller' => 'api.controller.customer:changePasswordAction',  '_route' => 'api.v10.customer.change-password',);
                    }
                    not_apiv10customerchangepassword:

                }

            }

            elseif (0 === strpos($pathinfo, '/v4/distributors')) {
                // api.v10.distributor.list
                if ('/v4/distributors' === $pathinfo) {
                    if ('GET' !== $canonicalMethod) {
                        $allow[] = 'GET';
                        goto not_apiv10distributorlist;
                    }

                    return array (  '_controller' => 'api.controller.distributor:listAction',  '_route' => 'api.v10.distributor.list',);
                }
                not_apiv10distributorlist:

                // api.v10.distributor.get
                if (preg_match('#^/v4/distributors/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ('GET' !== $canonicalMethod) {
                        $allow[] = 'GET';
                        goto not_apiv10distributorget;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'api.v10.distributor.get')), array (  '_controller' => 'api.controller.distributor:getAction',));
                }
                not_apiv10distributorget:

            }

            elseif (0 === strpos($pathinfo, '/v4/genres')) {
                // api.v10.genre.list
                if ('/v4/genres' === $pathinfo) {
                    if ('GET' !== $canonicalMethod) {
                        $allow[] = 'GET';
                        goto not_apiv10genrelist;
                    }

                    return array (  '_controller' => 'api.controller.genre:listAction',  '_route' => 'api.v10.genre.list',);
                }
                not_apiv10genrelist:

                // api.v10.genre.get
                if (preg_match('#^/v4/genres/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ('GET' !== $canonicalMethod) {
                        $allow[] = 'GET';
                        goto not_apiv10genreget;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'api.v10.genre.get')), array (  '_controller' => 'api.controller.genre:getAction',));
                }
                not_apiv10genreget:

            }

            elseif (0 === strpos($pathinfo, '/v4/halls')) {
                // api.v10.hall.list
                if ('/v4/halls' === $pathinfo) {
                    if ('GET' !== $canonicalMethod) {
                        $allow[] = 'GET';
                        goto not_apiv10halllist;
                    }

                    return array (  '_controller' => 'api.controller.hall:listAction',  '_route' => 'api.v10.hall.list',);
                }
                not_apiv10halllist:

                // api.v10.hall.get
                if (preg_match('#^/v4/halls/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ('GET' !== $canonicalMethod) {
                        $allow[] = 'GET';
                        goto not_apiv10hallget;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'api.v10.hall.get')), array (  '_controller' => 'api.controller.hall:getAction',));
                }
                not_apiv10hallget:

            }

            elseif (0 === strpos($pathinfo, '/v4/movies')) {
                // api.v10.movie.list
                if ('/v4/movies' === $pathinfo) {
                    if ('GET' !== $canonicalMethod) {
                        $allow[] = 'GET';
                        goto not_apiv10movielist;
                    }

                    return array (  '_controller' => 'api.controller.movie:listAction',  '_route' => 'api.v10.movie.list',);
                }
                not_apiv10movielist:

                // api.v10.movie.get
                if (preg_match('#^/v4/movies/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ('GET' !== $canonicalMethod) {
                        $allow[] = 'GET';
                        goto not_apiv10movieget;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'api.v10.movie.get')), array (  '_controller' => 'api.controller.movie:getAction',));
                }
                not_apiv10movieget:

                // api.v10.movie.create
                if ('/v4/movies' === $pathinfo) {
                    if ('POST' !== $canonicalMethod) {
                        $allow[] = 'POST';
                        goto not_apiv10moviecreate;
                    }

                    return array (  '_controller' => 'api.controller.movie:createAction',  '_route' => 'api.v10.movie.create',);
                }
                not_apiv10moviecreate:

                // api.v10.movie.update
                if (preg_match('#^/v4/movies/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ('PATCH' !== $canonicalMethod) {
                        $allow[] = 'PATCH';
                        goto not_apiv10movieupdate;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'api.v10.movie.update')), array (  '_controller' => 'api.controller.movie:updateAction',));
                }
                not_apiv10movieupdate:

                // api.v10.movie.delete
                if (preg_match('#^/v4/movies/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ('DELETE' !== $canonicalMethod) {
                        $allow[] = 'DELETE';
                        goto not_apiv10moviedelete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'api.v10.movie.delete')), array (  '_controller' => 'api.controller.movie:deleteAction',));
                }
                not_apiv10moviedelete:

            }

            elseif (0 === strpos($pathinfo, '/v4/news')) {
                // api.v10.article.list
                if ('/v4/news' === $pathinfo) {
                    if ('GET' !== $canonicalMethod) {
                        $allow[] = 'GET';
                        goto not_apiv10articlelist;
                    }

                    return array (  '_controller' => 'api.controller.news:listAction',  '_route' => 'api.v10.article.list',);
                }
                not_apiv10articlelist:

                // api.v10.article.get
                if (preg_match('#^/v4/news/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ('GET' !== $canonicalMethod) {
                        $allow[] = 'GET';
                        goto not_apiv10articleget;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'api.v10.article.get')), array (  '_controller' => 'api.controller.news:getAction',));
                }
                not_apiv10articleget:

                // api.v10.article.create
                if ('/v4/news' === $pathinfo) {
                    if ('POST' !== $canonicalMethod) {
                        $allow[] = 'POST';
                        goto not_apiv10articlecreate;
                    }

                    return array (  '_controller' => 'api.controller.news:createAction',  '_route' => 'api.v10.article.create',);
                }
                not_apiv10articlecreate:

                // api.v10.article.update
                if (preg_match('#^/v4/news/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ('PATCH' !== $canonicalMethod) {
                        $allow[] = 'PATCH';
                        goto not_apiv10articleupdate;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'api.v10.article.update')), array (  '_controller' => 'api.controller.news:updateAction',));
                }
                not_apiv10articleupdate:

                // api.v10.article.delete
                if (preg_match('#^/v4/news/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ('DELETE' !== $canonicalMethod) {
                        $allow[] = 'DELETE';
                        goto not_apiv10articledelete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'api.v10.article.delete')), array (  '_controller' => 'api.controller.news:deleteAction',));
                }
                not_apiv10articledelete:

            }

            elseif (0 === strpos($pathinfo, '/v4/orders')) {
                // api.v10.order.list
                if ('/v4/orders' === $pathinfo) {
                    if ('GET' !== $canonicalMethod) {
                        $allow[] = 'GET';
                        goto not_apiv10orderlist;
                    }

                    return array (  '_controller' => 'api.controller.order:listAction',  '_route' => 'api.v10.order.list',);
                }
                not_apiv10orderlist:

                // api.v10.order.get
                if (preg_match('#^/v4/orders/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ('GET' !== $canonicalMethod) {
                        $allow[] = 'GET';
                        goto not_apiv10orderget;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'api.v10.order.get')), array (  '_controller' => 'api.controller.order:getAction',));
                }
                not_apiv10orderget:

                // api.v10.order.create
                if ('/v4/orders' === $pathinfo) {
                    if ('POST' !== $canonicalMethod) {
                        $allow[] = 'POST';
                        goto not_apiv10ordercreate;
                    }

                    return array (  '_controller' => 'api.controller.order:createAction',  '_route' => 'api.v10.order.create',);
                }
                not_apiv10ordercreate:

                // api.v10.order.update
                if (preg_match('#^/v4/orders/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ('PATCH' !== $canonicalMethod) {
                        $allow[] = 'PATCH';
                        goto not_apiv10orderupdate;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'api.v10.order.update')), array (  '_controller' => 'api.controller.order:updateAction',));
                }
                not_apiv10orderupdate:

            }

            elseif (0 === strpos($pathinfo, '/v4/programs')) {
                // api.v10.program.list
                if ('/v4/programs' === $pathinfo) {
                    if ('GET' !== $canonicalMethod) {
                        $allow[] = 'GET';
                        goto not_apiv10programlist;
                    }

                    return array (  '_controller' => 'api.controller.program:listAction',  '_route' => 'api.v10.program.list',);
                }
                not_apiv10programlist:

                // api.v10.program.get
                if (preg_match('#^/v4/programs/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ('GET' !== $canonicalMethod) {
                        $allow[] = 'GET';
                        goto not_apiv10programget;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'api.v10.program.get')), array (  '_controller' => 'api.controller.program:getAction',));
                }
                not_apiv10programget:

                // api.v10.program.create
                if ('/v4/programs' === $pathinfo) {
                    if ('POST' !== $canonicalMethod) {
                        $allow[] = 'POST';
                        goto not_apiv10programcreate;
                    }

                    return array (  '_controller' => 'api.controller.program:createAction',  '_route' => 'api.v10.program.create',);
                }
                not_apiv10programcreate:

                // api.v10.program.update
                if (preg_match('#^/v4/programs/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ('PATCH' !== $canonicalMethod) {
                        $allow[] = 'PATCH';
                        goto not_apiv10programupdate;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'api.v10.program.update')), array (  '_controller' => 'api.controller.program:updateAction',));
                }
                not_apiv10programupdate:

                // api.v10.program.delete
                if (preg_match('#^/v4/programs/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ('DELETE' !== $canonicalMethod) {
                        $allow[] = 'DELETE';
                        goto not_apiv10programdelete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'api.v10.program.delete')), array (  '_controller' => 'api.controller.program:deleteAction',));
                }
                not_apiv10programdelete:

            }

            elseif (0 === strpos($pathinfo, '/v4/s')) {
                // api.v10.seat.list
                if ('/v4/seats' === $pathinfo) {
                    if ('GET' !== $canonicalMethod) {
                        $allow[] = 'GET';
                        goto not_apiv10seatlist;
                    }

                    return array (  '_controller' => 'api.controller.seat:listAction',  '_route' => 'api.v10.seat.list',);
                }
                not_apiv10seatlist:

                if (0 === strpos($pathinfo, '/v4/shows')) {
                    // api.v10.show.list
                    if ('/v4/shows' === $pathinfo) {
                        if ('GET' !== $canonicalMethod) {
                            $allow[] = 'GET';
                            goto not_apiv10showlist;
                        }

                        return array (  '_controller' => 'api.controller.show:listAction',  '_route' => 'api.v10.show.list',);
                    }
                    not_apiv10showlist:

                    // api.v10.show.get
                    if (preg_match('#^/v4/shows/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if ('GET' !== $canonicalMethod) {
                            $allow[] = 'GET';
                            goto not_apiv10showget;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'api.v10.show.get')), array (  '_controller' => 'api.controller.show:getAction',));
                    }
                    not_apiv10showget:

                }

                // swagger
                if (0 === strpos($pathinfo, '/v4/swagger') && preg_match('#^/v4/swagger/(?P<path>.*)$#s', $pathinfo, $matches)) {
                    if ('GET' !== $canonicalMethod) {
                        $allow[] = 'GET';
                        goto not_swagger;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'swagger')), array (  '_controller' => 'api.controller.swagger:listAction',));
                }
                not_swagger:

            }

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}

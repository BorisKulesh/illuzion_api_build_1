<?php

use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Psr\Log\LoggerInterface;

/**
 * appProdProjectContainerUrlGenerator
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdProjectContainerUrlGenerator extends Symfony\Component\Routing\Generator\UrlGenerator
{
    private static $declaredRoutes;

    /**
     * Constructor.
     */
    public function __construct(RequestContext $context, LoggerInterface $logger = null)
    {
        $this->context = $context;
        $this->logger = $logger;
        if (null === self::$declaredRoutes) {
            self::$declaredRoutes = array(
        'api.v10.cinema.list' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'api.controller.cinema:listAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/v4/cinemas',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'api.v10.cinema.get' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'api.controller.cinema:getAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'id',    ),    1 =>     array (      0 => 'text',      1 => '/v4/cinemas',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'api.v10.city.list' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'api.controller.city:listAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/v4/cities',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'api.v10.city.get' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'api.controller.city:getAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'id',    ),    1 =>     array (      0 => 'text',      1 => '/v4/cities',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'api.v10.customer.list' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'api.controller.customer:listAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/v4/customers',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'api.v10.customer.get' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'api.controller.customer:getAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'id',    ),    1 =>     array (      0 => 'text',      1 => '/v4/customers',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'api.v10.customer.update' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'api.controller.customer:updateAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'id',    ),    1 =>     array (      0 => 'text',      1 => '/v4/customers',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'api.v10.customer.login' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'api.controller.customer:loginAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/v4/customers/login',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'api.v10.customer.reset-password' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'api.controller.customer:resetPasswordAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/v4/customers/reset-password',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'api.v10.customer.change-password' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'api.controller.customer:changePasswordAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/v4/customers/changePassword',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'api.v10.distributor.list' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'api.controller.distributor:listAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/v4/distributors',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'api.v10.distributor.get' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'api.controller.distributor:getAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'id',    ),    1 =>     array (      0 => 'text',      1 => '/v4/distributors',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'api.v10.genre.list' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'api.controller.genre:listAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/v4/genres',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'api.v10.genre.get' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'api.controller.genre:getAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'id',    ),    1 =>     array (      0 => 'text',      1 => '/v4/genres',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'api.v10.hall.list' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'api.controller.hall:listAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/v4/halls',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'api.v10.hall.get' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'api.controller.hall:getAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'id',    ),    1 =>     array (      0 => 'text',      1 => '/v4/halls',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'api.v10.movie.list' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'api.controller.movie:listAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/v4/movies',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'api.v10.movie.get' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'api.controller.movie:getAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'id',    ),    1 =>     array (      0 => 'text',      1 => '/v4/movies',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'api.v10.movie.create' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'api.controller.movie:createAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/v4/movies',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'api.v10.movie.update' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'api.controller.movie:updateAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'id',    ),    1 =>     array (      0 => 'text',      1 => '/v4/movies',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'api.v10.movie.delete' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'api.controller.movie:deleteAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'id',    ),    1 =>     array (      0 => 'text',      1 => '/v4/movies',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'api.v10.article.list' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'api.controller.news:listAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/v4/news',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'api.v10.article.get' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'api.controller.news:getAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'id',    ),    1 =>     array (      0 => 'text',      1 => '/v4/news',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'api.v10.article.create' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'api.controller.news:createAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/v4/news',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'api.v10.article.update' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'api.controller.news:updateAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'id',    ),    1 =>     array (      0 => 'text',      1 => '/v4/news',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'api.v10.article.delete' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'api.controller.news:deleteAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'id',    ),    1 =>     array (      0 => 'text',      1 => '/v4/news',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'api.v10.order.list' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'api.controller.order:listAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/v4/orders',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'api.v10.order.get' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'api.controller.order:getAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'id',    ),    1 =>     array (      0 => 'text',      1 => '/v4/orders',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'api.v10.order.create' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'api.controller.order:createAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/v4/orders',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'api.v10.order.update' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'api.controller.order:updateAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'id',    ),    1 =>     array (      0 => 'text',      1 => '/v4/orders',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'api.v10.program.list' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'api.controller.program:listAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/v4/programs',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'api.v10.program.get' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'api.controller.program:getAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'id',    ),    1 =>     array (      0 => 'text',      1 => '/v4/programs',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'api.v10.program.create' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'api.controller.program:createAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/v4/programs',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'api.v10.program.update' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'api.controller.program:updateAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'id',    ),    1 =>     array (      0 => 'text',      1 => '/v4/programs',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'api.v10.program.delete' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'api.controller.program:deleteAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'id',    ),    1 =>     array (      0 => 'text',      1 => '/v4/programs',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'api.v10.seat.list' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'api.controller.seat:listAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/v4/seats',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'api.v10.show.list' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'api.controller.show:listAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/v4/shows',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'api.v10.show.get' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'api.controller.show:getAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'id',    ),    1 =>     array (      0 => 'text',      1 => '/v4/shows',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'swagger' => array (  0 =>   array (    0 => 'path',  ),  1 =>   array (    '_controller' => 'api.controller.swagger:listAction',  ),  2 =>   array (    'path' => '.*',  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '.*',      3 => 'path',    ),    1 =>     array (      0 => 'text',      1 => '/v4/swagger',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
    );
        }
    }

    public function generate($name, $parameters = array(), $referenceType = self::ABSOLUTE_PATH)
    {
        if (!isset(self::$declaredRoutes[$name])) {
            throw new RouteNotFoundException(sprintf('Unable to generate a URL for the named route "%s" as such route does not exist.', $name));
        }

        list($variables, $defaults, $requirements, $tokens, $hostTokens, $requiredSchemes) = self::$declaredRoutes[$name];

        return $this->doGenerate($variables, $defaults, $requirements, $tokens, $parameters, $name, $referenceType, $hostTokens, $requiredSchemes);
    }
}

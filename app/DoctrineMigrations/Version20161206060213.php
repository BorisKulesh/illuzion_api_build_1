<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161206060213 extends AbstractMigration
{
    public function up(Schema $schema)
    {
    }

    public function down(Schema $schema)
    {
    }

    /**
     * @param Schema $schema
     */
    public function postUp(Schema $schema)
    {
        $this->connection->insert('cinema', [
            'id' => 'che',
            'title' => 'Черемушки',
            'page' => '<h1>Черемушки</h1>',
            'city_id' => 'vladivostok',
            'alias' => 'cheremushki',
        ]);
    }
}

<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160822060213 extends AbstractMigration
{
    public function up(Schema $schema)
    {
    }

    public function down(Schema $schema)
    {
    }

    /**
     * @param Schema $schema
     */
    public function postUp(Schema $schema)
    {
        $this->connection->insert('cinema', [
            'id' => 'oc',
            'title' => 'Океан',
            'page' => '<h1>Океан</h1>',
            'city_id' => 'vladivostok',
            'alias' => 'ocean',
        ]);

        $this->connection->insert('cinema', [
            'id' => 'ill',
            'title' => 'Иллюзион',
            'page' => '<h1>Иллюзион</h1>',
            'city_id' => 'vladivostok',
            'alias' => 'illuzion',
        ]);

        $this->connection->insert('cinema', [
            'id' => 'uss',
            'title' => 'Уссури',
            'page' => '<h1>Уссури</h1>',
            'city_id' => 'vladivostok',
            'alias' => 'ussuri',
        ]);
    }
}

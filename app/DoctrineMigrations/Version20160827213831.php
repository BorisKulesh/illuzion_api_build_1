<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20160827213831 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $table = $schema->createTable('release_date');
        $table->addColumn('id', 'integer', ['notnull' => true, 'unsigned' => true, 'autoincrement' => true]);
        $table->addColumn('movie_id', 'integer', ['notnull' => true, 'unsigned' => true,]);
        $table->addColumn('city_id', 'string', ['notnull' => true]);
        $table->addColumn('datetime', 'datetime', ['notnull' => true]);
        $table->setPrimaryKey(['id']);
        $table->addForeignKeyConstraint($schema->getTable('city'), ['city_id'], ['id']);
        $table->addUniqueIndex(['city_id', 'movie_id'], 'UNQ_CITY_MOVIE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $schema->dropTable('release_date');
    }
}

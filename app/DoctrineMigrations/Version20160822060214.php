<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160822060214 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $article = $schema->getTable('article');
        $article->changeColumn('text_short', ['notnull' => false]);
        $article->changeColumn('text_full', ['notnull' => false]);
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $article = $schema->getTable('article');
        $article->changeColumn('text_short', ['notnull' => true]);
        $article->changeColumn('text_full', ['notnull' => true]);
    }
}

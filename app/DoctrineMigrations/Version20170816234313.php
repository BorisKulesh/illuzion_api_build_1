<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170816234313 extends AbstractMigration
{
    public function up(Schema $schema)
    {
    }

    public function down(Schema $schema)
    {
    }

    /**
     * @param Schema $schema
     */
    public function postUp(Schema $schema)
    {
        $this->connection->insert('cinema', [
            'id' => 'nmega',
            'title' => 'Находка-Мега',
            'page' => '<h1>Находка-Мегcimа</h1>',
            'city_id' => 'vladivostok',
            'alias' => 'nahodka-mega',
        ]);
    }
}

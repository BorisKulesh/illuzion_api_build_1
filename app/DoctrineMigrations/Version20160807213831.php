<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20160807213831 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $city = $schema->createTable('city');
        $city->addColumn('id', 'string', ['notnull' => true]);
        $city->addColumn('name', 'string', ['notnull' => true]);
        $city->setPrimaryKey(['id']);

        $cinema = $schema->createTable('cinema');
        $cinema->addColumn('id', 'string', ['notnull' => true]);
        $cinema->addColumn('title', 'string', ['notnull' => true]);
        $cinema->addColumn('page', 'text', ['notnull' => false]);
        $cinema->addColumn('city_id', 'string', ['notnull' => true]);
        $cinema->setPrimaryKey(['id']);
        $cinema->addForeignKeyConstraint($city, ['city_id'], ['id']);
    }

    /**
     * @param Schema $schema
     */
    public function postUp(Schema $schema)
    {
        $this->connection->insert('city', [
            'id' => 'vladivostok',
            'name' => 'Владивосток',
        ]);
        $this->connection->insert('city', [
            'id' => 'nakhodka',
            'name' => 'Находка',
        ]);

        $this->connection->insert('cinema', [
            'id' => 'uss',
            'title' => 'Уссури',
            'page' => '<h1>Уссури</h1>',
            'city_id' => 'vladivostok',
        ]);
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $schema->dropTable('city');
        $schema->dropTable('cinema');
    }
}

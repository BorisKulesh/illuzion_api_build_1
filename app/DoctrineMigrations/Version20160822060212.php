<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160822060212 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $article = $schema->createTable('article');
        $article->addColumn('id', 'integer', ['notnull' => true, 'unsigned' => true, 'autoincrement' => true]);
        $article->addColumn('city_id', 'string', ['notnull' => true]);
        $article->addColumn('title', 'string', ['notnull' => true]);
        $article->addColumn('alias', 'string', ['notnull' => true]);
        $article->addColumn('datetime', 'datetime', ['notnull' => true]);
        $article->addColumn('text_short', 'text', ['notnull' => true]);
        $article->addColumn('text_full', 'text', ['notnull' => true]);
        $article->addColumn('published', 'boolean', ['notnull' => true]);
        $article->addColumn('views', 'integer', ['notnull' => true, 'unsigned' => true]);
        $article->setPrimaryKey(['id']);
        $article->addUniqueIndex(['alias']);
        $article->addForeignKeyConstraint($schema->getTable('city'), ['city_id'], ['id']);
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $schema->dropTable('article');
    }
}
